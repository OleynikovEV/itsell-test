const ENTER = 13;

var app = {

    layout: 'desktop',

    init : function() {
        app.initHeaderSearch();
        app.initMainMenu();

        app.initMobileSidebar();

        app.initAddToCart();

        app.initAdaptiveLayout();
        app.initUIElements();

        app.initFeedbackForm();

        app.ReadmoreSeo();

        app.initTouch();

        app.initWarrantyPopup();
        app.initProductsColors();

        app.eventCartFocus();

        $(function () {
            app.eventChangePositionElements();
            
            $('#profile-menu').click(function() {
              $('.mobile-profile-menu').toggle();
              $(this).toggleClass('arrow_down_icon');
            });

            // Header Popup Block
            $('.header-contacts-block').mouseenter(function() {
                if ($(window).width() >= 850) $('.header-popup-bg').stop(true).fadeIn();
            });
            $('.header-contacts-block').mouseleave(function() {
                if ($(window).width() >= 850) $('.header-popup-bg').stop(true).fadeOut();
            });

            $('.header-navbar-block .client-link, .header-navbar-block .profile-menu').mouseenter(function() {
                if ($(window).width() >= 850) $('.header-popup-bg').stop(true).fadeIn();
            });
            $('.header-navbar-block .client-link, .header-navbar-block .profile-menu').mouseleave(function() {
                if ($(window).width() >= 850) $('.header-popup-bg').stop(true).fadeOut();
            });

            // Sorting
            $('.cart-sorting-wrapper').click(function(){
                $(this).toggleClass('active');
                $(this).find('.cart-sorting-popup').toggle();
            });

            // subscribe
            const hrefViber = document.getElementById('hrefViber');
            const hrefTelegram = document.getElementById('hrefTelegram');
            const hrefEmail = document.getElementById('hrefEmail');

            if (hrefViber) hrefViber.addEventListener('click', app.subscribeClick);
            if (hrefTelegram) hrefTelegram.addEventListener('click', app.subscribeClick);
            if (hrefEmail) hrefEmail.addEventListener('click', app.subscribeClick);

            // Sorting Buttons in Cart Page
            $(window).resize(function() {
              app.eventChangePositionElements();
            });

            $('.tooltip-event').tooltip();
        });

        // Кнопка Наверх
        var scrollBlockIsDisplayed = false;

        $(window).on('scroll', function(){
            var scrollTop = $(window).scrollTop();

            if ( ! scrollBlockIsDisplayed && scrollTop > window.innerHeight) {
                $('.btn-page-up').css({opacity: 0}).show();
                scrollBlockIsDisplayed = true;
            }

            if (scrollBlockIsDisplayed && window.innerHeight > 0) {
                var opacity = Math.min((scrollTop - window.innerHeight) / window.innerHeight, 1);
                $('.btn-page-up').css({opacity: opacity});
            }
        });

        $('.btn-page-up').on('click', function(ev){
            ev.preventDefault();
            $('body, html').animate({scrollTop: 0}, 500);
        });

        setTimeout(function(){
            $(window).resize();
        }, 100);
    },

    /**
     * Блок Гарантии
     */
    initWarrantyPopup : function() {
      $('.page-button').on('click', function(ev){
        ev.preventDefault();

        var popup = $('.static-page-popup').html($('<a>').addClass('btn-close').attr('href', '#'));
        const page = $(this).data('page-name');

        $.ajax({
          url: `/pages/${page}?type=json`,
          dataType: 'json',
          success: function(res) {
            popup.append($('<div>').addClass('popup-title').html(res.title))
              .append(res.content);

            $('.popup-bg').fadeIn(300);
            popup.addClass('active').css({top: $(window).scrollTop() + 50}).fadeIn(300);
          }
        })
      });
    },

    eventChangePositionElements: function() {
      if($(window).width() <= 1319) {
        $('._cart-table-title').after($('.sorting-buttons-wrapper'));

        // Availability Block
        $('.cart-product-block').each(function(){
          $(this).find('.product-availability-block').appendTo($(this).find('.cart-product-labels'));
        });
      } else {
        $('.sorting-buttons-wrapper').prependTo('.cart-sorting-buttons');

        // Availability Block
        $('.cart-product-block').each(function(){
          $(this).find('.product-availability-block').appendTo($(this).find('.cart-col-7'));
        });
      }
    },

    subscribeClick: function() {
      const method = this.dataset.event;

      fetch('/v1/wishlist/subscribe', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
          method: method
        })
      })
        .catch( error => {
          console.error(error)
        })
        .finally(() => {
          const modal = document.getElementById('subscribeModal');
          modal.remove();
        });
    },

    initMainMenu : function() {
        var showSubmenuTimeout;
        var hideSubmenuTimeout;

        $('.menu-models-block > ul > li').on('mouseenter', function(){
            if (app.layout === 'mobile') return;

            var submenu = $(this).find('.submenu-category-block');
            const submenuContent = $(this).find('ul');

            if (submenu.find('.submenu-search-block').length) {
                $('.submenu-search-block .search-input').val(null);
                app.clearFilter($(this).find('ul'));
            } else {
                // добавляем поле поиска по меню
                submenuContent.prepend(
                    $('<div>').addClass('submenu-search-block').append(
                        $('<input>').attr('type', 'text').attr('placeholder', __('Поиск по моделям')).addClass('search-input')
                    )
                );
            }

            if (submenu.hasClass('active')) {
                clearTimeout(hideSubmenuTimeout);
                submenu.show().addClass('active');
                return;
            }

            showSubmenuTimeout = setTimeout(function(){
                $('.menu-models-block .submenu-category-block').hide().removeClass('active');
                submenu.show().addClass('active');
            }, 300);
        }).on('mouseleave', function(el){
            if (app.layout == 'mobile') return;

            clearInterval(showSubmenuTimeout);

            var submenu = $(this).find('.submenu-category-block');
            if ( ! submenu.hasClass('active')) return;

            hideSubmenuTimeout = setTimeout(function(){
                submenu.hide().removeClass('active');
            }, 300);
        });

        $('body').on('keyup', '.search-input', function() {
            const filter = $(this).val();
            const elements = $(this).closest('.submenu-category-block').children('ul').children('li');

            if (filter) {
                elements.each(function() {
                    const link = $(this).find('a');
                    if ( ! link) return;

                    const itemText = link.text();
                    if (itemText.match(new RegExp(filter, 'gi'))) {
                        $(this).addClass('submenu-filter__active');
                    } else {
                        $(this).removeClass('submenu-filter__active');
                    }
                });
            } else {
                app.clearFilter($(this).closest('.submenu-category-block').children('ul'));
            }
        });

        // Переход по ссылкам первого уровня выпадающего меню только после второго нажатия на touch-устройствах
        // var dropdownsSelector = '.submenu-block > li > a:before';
        var dropdownsSelector = '.submenu-block > li > a';

        $('.header-menu-block').on('touchstart', dropdownsSelector, function() {
            $(this).addClass('touch-started touched');
        }).on('click', dropdownsSelector, function(ev) {
            if ($(this).hasClass('touch-started') && ! $(this).hasClass('touched')) {
                ev.preventDefault();
                $(this).addClass('touched');
            }
        });
    },

    /**
     * Снять выделения и подменю
     */
    clearFilter: function(ul) {
        const elements = ul.children('li');

        elements.each(function() {
            $(this).removeClass('submenu-filter__active');
        });
    },

    /**
     * Скрипты для работы главной страницы
     */
    initMainPage : function() {
        // Слайдер
        var bannerBlock = $('.main-banner-block');
        if ($(window).width() <= 500) {
            // bannerBlock.css({height: bannerBlock.width() * 0.53})
            bannerBlock.css({height: bannerBlock.width()})
        }

        const wmrSlider = $('.main-banner-content').wmrSlider({
            height: bannerBlock.height(),
            speed: 5000,
            transitionSpeed: 10,
            autoSize: true,
            touchSupport: true,
            shiftOnTouch : true,
            dotsWrapper: $('.main-banner-dots')
        });

        $('.main-banner-content a img').each(function(){
            var self = $(this);
            var imageSrc = $(this).data($(window).width() > 500 ? 'src' : 'src-mobile');

            var image = new Image();
            image.onload = function() {
                self.attr('src', imageSrc).removeClass('loading').css({marginLeft: -self.width() / 2});
            }

            image.src = imageSrc;
        });

        // Переключение видео
        $('.video-list-item a').on('click', function(ev){
            ev.preventDefault();

            var videoCode = $(this).closest('.video-list-item').data('code');
            $('.full-video-block iframe').attr('src', 'https://www.youtube.com/embed/' + videoCode);
        });

        // Вкладки в блоке Сервисы
        $('.advantages-tabs-block li').on('click', function(ev){
            ev.preventDefault();

            const images = document.querySelectorAll('.lazy');
            images.forEach(function(image) {
              image.src = image.dataset.src;
              image.classList.remove('width-a');
              image.classList.remove('opacity');
            });

            if (app.layout == 'desktop') {
                $(this).closest('.tabs-title-block').find('li').removeClass('active');
                $(this).addClass('active');

                var selector = $(this).find('a').attr('href');
                $(this).closest('.tabs-wrapper-block').find('.tabs-info-block').hide();
                $(selector).show();
            } else {
                var popup = $('.advantages-popup').html($('<a>').addClass('btn-close').attr('href', '#'));
                popup.append($('<div>').addClass('popup-title').html($(this).find('a').text()));

                var textBlock = $($(this).find('a').attr('href'));
                popup.append(textBlock.clone());

                $('.popup-bg').fadeIn(300);
                popup.addClass('active').css({top: $(window).scrollTop() + 50}).fadeIn(300);
            }
        });

        $('body').on('layout:rebuild', function(){
            if (app.layout == 'mobile') {
                $('.advantages-tabs-block li.active').removeClass('active')
            }
        });

        // Перестройка блока Сервисы
        $(window).resize(function() {
            if($(window).width() >= 1020) {
                // Advantages Block
                $('.advantages-tabs-block .tabs-info-block').each(function(){
                    var block = $(this).find('.advantages-title-block');

                    $(this).show();
                    block.css('margin-top', -block.height() / 2).show();
                    $(this).hide();
                });

                $('.advantages-tabs-block .tabs-info-block').first().show();
            }
        });

      setTimeout(function(){
        $(window).resize();
      }, 100);
    },

    /**
     * Работа боковой шторки на мобильных
     */
    initMobileSidebar : function() {
        // Открытие и закрытие боковой шторки
        $('.mobile-btn-menu').click(function(){
            $('.popup-bg').fadeIn();
            $('.mobile-sidebar-menu').addClass('active').animate({left: '0'}, 300);
        });

        $('.mobile-sidebar-header .btn-close').click(function(){
            $('.popup-bg').fadeOut();
            $('.mobile-sidebar-menu').removeClass('active').animate({left: '-100%'}, 300);
        });

        $('.popup-bg').on('click', function(){
            if ($('.mobile-sidebar-menu.active').length) {
                $('.mobile-sidebar-header .btn-back:not(.hidden)').click();
                $('.mobile-sidebar-header .btn-close').click();
            }
        });

        // Открытие пунктов подменю
        // $('.menu-models-block > ul > li > .show-more, .menu-models-block > ul > li > a .arrow, .menu-title-block > ul > li > a').off('click').on('click', function(ev){
        $('.menu-models-block > ul > li > a, .menu-title-block > ul > li > a').off('click').on('click', function(ev){
            if (app.layout != 'mobile') return;

            ev.preventDefault();

            var submenu = $(this).parent().children('.submenu-category-block').clone()
              .addClass('mobile-sidebar-active-submenu');
            // var submenu = $(this).closest('a').parent().children('.submenu-category-block').clone()
            //     .addClass('mobile-sidebar-active-submenu');

            var submenuBanner = submenu.find('.submenu-banner-block');
            if (submenuBanner.length && submenuBanner.index() == 0) {
                submenu.find('ul').after(submenuBanner);
            }

            if ( ! submenu.find('.mobile-search-block').length) {
                submenu.prepend(
                    $('<div>').addClass('mobile-search-block').append(
                        $('<input>').attr('type', 'text').attr('placeholder', __('Поиск по моделям'))
                    ).append(
                        $('<button>')
                    )
                );
            }

            $('.mobile-sidebar-content').append(submenu);
            $('.mobile-sidebar-content-inner').animate({left: '-100%'}, 200);
            submenu.animate({left: 0}, 200);

            $('.mobile-sidebar-header .btn-close').hide();
            $('.mobile-sidebar-header .btn-back').removeClass('hidden').show();
        });

        // Закрытие подменю
        $('.mobile-sidebar-header .btn-back').off('click').on('click', function(ev){
            ev.preventDefault();

            var submenu = $('.mobile-sidebar-active-submenu');
            submenu.animate({left: '100%'}, 200, function(){
                submenu.remove();
            });

            $('.mobile-sidebar-content-inner').animate({left: 0}, 200);

            $(this).hide();
            $('.mobile-sidebar-header .btn-close').show();
        });

        // Фильтрация списка моделей в подменю
        $('.mobile-sidebar-menu').on('keyup', '.mobile-search-block input', function(){
            var filter = $(this).val();
            var elements = $(this).closest('.submenu-category-block').children('ul').children('li');

            if (filter) {
                elements.each(function(){
                    var link = $(this).find('a');
                    if ( ! link) return;

                    var itemText = link.text();
                    if (itemText.match(new RegExp(filter, 'gi'))) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            } else {
                elements.show();
            }
        });
    },

    /**
     * Поле поиска в шапке
     */
    initHeaderSearch : function() {
        // Превью результатов поиска
        let searchTimeout = null;
        let xhr = null;
        $('.header-search-form input').keyup(function(ev){
            const phrase = $(this).val();

            if (searchTimeout != null) {
              clearTimeout(searchTimeout);
              if (xhr !== null) xhr.abort();
            }
            if (ev.key === 13 || ev.key == 'Enter') {
              clearTimeout(searchTimeout);
              xhr.abort();
              return;
            }

            if (phrase.length > 2 && ev.which != 40 && ev.which != 38 && ev.which != 13) {
                searchTimeout = setTimeout(function(){
                    xhr = $.ajax({
                        url: '/search/' + phrase.replace(/[\%\/]/gi, ' '),
                        data: {type: 'preview'},
                        dataType: 'json',
                        success: function(data) {
                          if (data.success && data.response.length > 0) {
                            const searchPreviewWrapper = $('.header-search-popup').empty();
                            searchPreviewWrapper.append(data.response);
                              searchPreviewWrapper.show();
                          } else {
                            $('.header-search-popup').empty();
                            $('.header-search-popup').hide();
                          }
                        }
                    });
                }, 200);
            } else if (phrase.length < 3 && $(window).width() > 649) {
                $('.header-search-popup').hide();
            }
        })
        .focus(function(){
            if ($(window).width() <= 649) {
                $('.header-search-popup').show();
                $('header').addClass('header-search-active');

                $('body').css({overflow: 'hidden', position: 'fixed'});
            } else {
                $(this).addClass('header-search-input-focused');

                var fieldRightAnchor = '.login-link';
                if ($(window).width() < 1020) fieldRightAnchor = '.header-contacts-block';
                if ($(window).width() < 830) fieldRightAnchor = '.header-cart-block';

                $(this).css({width: $(fieldRightAnchor).offset().left - $(this).offset().left - 10});
            }
        })
        .focusout(function() {
            var self = $(this);

            setTimeout(function() {
                if ($(window).width() > 649) {
                    $('.header-search-popup').hide();
                    $('header').removeClass('header-search-active');

                    self.removeClass('header-search-input-focused').css({width: ''});
                }
            }, 300);
        });

        $('.header-search-close').on('click', function(ev){
            ev.preventDefault();

            $('.header-search-popup').hide();
            $('header').removeClass('header-search-active');

            $('body').css({overflow: 'auto', position: 'relative'});
        })

        $('.header-search-form').on('submit', function(ev){
            ev.preventDefault();

            const phrase = $(this).find('input[name=search_phrase]').val();
            if (phrase) document.location.href = '/search/' + phrase.replace(/[\%\/]/gi, ' ');
        });
    },

    /**
     * Добавление товаров в корзину
     */
    initAddToCart : function() {
        // Добавление товара в корзину
        $('body').on('click', '.add-to-cart', function(ev){
            ev.preventDefault();

            var isProductPage = $(this).is('.add-to-cart-product-page') || $(this).is('.add-to-cart-product-page-modal');
            var productBlockClass = $(this).is('.add-to-cart-product-page') ? '.product-info-block' : '.product-info-block-modal';
            if (isProductPage) $('.product-info-block .product-colors-block').removeClass('highlighted');

            var colorsQty = {};
            var qtyInCart = 0;
            var materialVal = '';

            var productBlock = $(this).closest(
              $(this).is('.add-to-cart-product-page')
                ? '.product-info-block'
                : $(this).is('.add-to-cart-product-page-modal')
                  ? '.photo-viewer-right-block'
                  : '.product-block');

            var colorProduct = $(this).data('type') === 'modal' ? '.product-color-qty-modal' : '.product-color-qty';
            productBlock.find(colorProduct).each(function(){
                var qty = parseInt($(this).val());
                if (qty) {
                    colorsQty[$(this).data('id')] = qty;
                    qtyInCart += qty;
                    materialVal = $('#material').val();
                }
            });

            if (isProductPage) {
              if ($('#material').length > 0 && $('#material').val().length === 0) {
                app.showPopupMessage(__('выберите материал чехла'));
                return;
              } else if ($('#material').length > 0 && $('#material').val().length !== 0 && $('#product-qty').val() > 0) {
                colorsQty[$('#product-qty').data('id')] = parseInt($('#product-qty').val());
                materialVal = $('#material').val();
              }
            } else {
              const block = $(this).closest('.product-block');
              let material = block.find('.product-material .select-block');

              if (material.length > 0) {
                material = material.val();
                if (material.length === 0) {
                  app.showPopupMessage(__('выберите материал чехла'));
                  return;
                } else {
                  let qty = block.find('.product-material .product-qty');
                  let productId = qty.data('id');
                  qty = parseInt(qty.val());
                  if (qty > 0) {

                    colorsQty[productId] = qty;
                    qtyInCart += qty;
                    materialVal = material;
                  }
                }
              }
            }

            if (Object.keys(colorsQty).length) {
                $.ajax({
                    url: '/cart',
                    type: 'post',
                    data: {
                      products: colorsQty,
                      material: materialVal,
                    },
                    dataType: 'json',
                    success: function(data){
                        if (typeof isCartPage !== 'undefined') {
                            window.scrollTo(0, 0);
                            document.location.reload();
                            return;
                        }

                        app.showPopupMessage(__('товары добавлены в') + ' <a href="' + $('.header-cart-block a').attr('href')
                            + '">' + __('вашу корзину') + '</a>!');

                        app.updateHeaderCartState(data.response);

                        // Если товар добавлен со страницы списка - обновляем статус карточки товара
                        if ( ! isProductPage) {
                            if ( ! productBlock.hasClass('product-in-cart')) {
                                productBlock.addClass('product-in-cart');
                                productBlock.find('.add-to-cart').append($('<b>'));
                            } else {
                                qtyInCart += parseInt(productBlock.find('.add-to-cart b').html());
                            }

                            productBlock.find('.add-to-cart b').html(qtyInCart + ' ' + __('ед.'));
                        }

                        productBlock.find(colorProduct).each(function(){
                            $(this).val(0)
                        });
                        // Вызов конверсий
                        fbq('track', 'Add to cart');
                    }
                });
            } else {
                app.showPopupMessage(__('введите количество для добавления в корзину'));

                if (isProductPage) {
                    $('.product-colors-block').addClass('highlighted');
                    //$('.product-color-qty-modal').addClass('highlighted');
                    app.shakeElement($('.product-colors-block'));

                    $('.product-qty-block').addClass('highlighted');
                    app.shakeElement($('.product-qty-block'));
                } else {
                  const block = $(this).closest('.product-wrapper');
                  const colorsBlock = $(block).find('.product-colors .product-colors-wrapper');
                  colorsBlock.addClass('highlighted');
                  app.shakeElement(colorsBlock);

                  const material = block.find('.product-qty-block').addClass('highlighted');
                  app.shakeElement(material);
                }
            }
        });

        // Удаление нуля в поле количества при установке курсора
        $('body').on('focus', '.product-color-qty, .product-qty', function(){
            if ($(this).val() == '0') $(this).val('');
        }).on('focusout', '.product-color-qty, .product-qty', function(){
            if ($(this).val() == '') $(this).val('0');
        });

        // запрет вводить отрицательное количество
        $('body').on('input', '.product-color-qty, .product-qty', function(){
          const value = $(this).val();
          if (value < 0) {
            app.showPopupMessage(__('Количество не должно быть отрицательным'));
            $(this).val('');
            $('#product-qty2').val('');
            $('#product-qty').val('');
          }

          // if ($(this).data('isPrint')) {
          //   console.log( priceTypeSite, priceRangeForPrint[priceTypeSite] );
          //   console.log( priceRangeForPrint );
          // }
        });
    },

    updateHeaderCartState : function(data) {
        if ( ! $('.header-cart-block a span').length) {
            $('.header-cart-block a').append($('<span>'));
        }

        $('.header-cart-block a span').html(data.count);
    },

    showPopupMessage : function(text){
        var messageBlock = null;
        var messageTimeout = null;

        if ( ! messageBlock) {
            messageBlock = $('<div>').addClass('orange-popup product-added').append($('<p>'));
            $('body').append(messageBlock);
        }

        messageBlock.find('p').html(text);
        messageBlock.css({bottom: -100}).animate({bottom: 15}, 300);
        clearTimeout(messageTimeout);

        messageTimeout = setTimeout(function(){
            messageBlock.animate({bottom: -100}, 300);
        }, 5000);
    },

    /**
     * Переключение изображений при нажатии на цвета товаров
     */
    initProductsColors : function() {
        // Переключение фото товара при нажатии на иконки цветов
        $('body').on('click', '.product-colors a', function(ev){
            ev.preventDefault();
            if (priceTypeSiteShow === true) return;

            // Выделение активного цвета и замена названия товара
            $(this).closest('.product-colors-wrapper').find('a').removeClass('active');
            $(this).addClass('active');

            const titleBlock = $(this).closest('.product-wrapper').find('.product-name a');
            if ( ! titleBlock.data('title')) titleBlock.data('title', titleBlock.text());
            let title = titleBlock.data('title');
            title = title.replace(/Серия/gi, '<span class="text-orange bold">Серия</span>');
            title = title.replace(/Группа товаров/gi, '<span class="text-orange bold">Группа товаров</span>');
            titleBlock.html(title + ' (' + $(this).attr('title') + ')');

            // Переключение изображения товара
            const src = $(this).data('image');
            const id = $(this).data('id');

            const image = $(this).closest('.product-block').find('.product-image img');
            image.attr('src', '/template/images/ajax-loader.gif');

            const wishBtn = this.closest('.product-block').querySelector('.wish-button');
            wishList.setElement(wishBtn, this);

            const img = new Image();
            img.onload = function(){image.attr('src', src)};
            img.src = src;

            // Отображение даты ожидания цвета
            $(this).closest('.product-colors').find('.product-availability-block').remove();

            if ($(this).data('when-appear')) {
                $(this).closest('.product-colors').append(
                    $('<div>').addClass('product-availability-block soon-available').html(__('ожидается'))
                );

              wishList.changeProductId(id);
              wishList.show();
            } else {
              wishList.hidden();
            }

            // выводим цену выбранного товара
            const parentWrapper = $(this).closest('.product-wrapper');

            $(parentWrapper).find('a img').removeClass('in');

            if ($(this).data('byseries') == true) return; // не меняем серию если товар сгруппирован

            const price = priceModal.init(`price-block-${$(this).data('parent')}`);
            const currentOptPriceRange = $(this).data('price-opt-range');
            const setPriceDefaultUah = $(this).data('price-default-uah');
            const currentOptPrice = $(this).data('price-opt');
            const currentPriceOptUsd = $(this).data('price-opt-usd');
            const currentVipPrice = $(this).data('price-vip');
            const currentDealerPrice = $(this).data('price-dealer');
            const currentDropUahPrice = $(this).data('price-drop-uah');
            const currentPPCPrice = $(this).data('price-ppc');
            const currentPriceOld = $(this).data('price-old');
            const defaultPrice = $(this).data('price-default');

            if (currentOptPriceRange === undefined || currentOptPrice === undefined
                || currentPriceOptUsd === undefined || currentVipPrice === undefined
                || currentDealerPrice === undefined || currentDropUahPrice === undefined
                || currentPPCPrice === undefined) {
                console.log('No change');
            }
            else {
              price.setPriceRange(currentOptPriceRange);
              price.setPriceSale(currentPriceOld);
              price.setPriceRrc(currentPPCPrice);
              price.setPriceDiler(currentDealerPrice);
              price.setPriceDrop(currentDropUahPrice);
              price.setPriceOpt(currentOptPrice);
              price.setPriceVip(currentVipPrice);
              price.setPriceDefault(defaultPrice, '$');
              price.setPriceDefaultUah(setPriceDefaultUah, '₴');
            }
        });

        $('body').on('focusin keypress', '.product-colors-wrapper div input', function(e) {
            $(this).prev().trigger('click');
            const parentWrapper = $(this).closest('.product-wrapper');
            const cart = $(parentWrapper).find('.add-to-cart');

            if (e.keyCode == ENTER) {
                $(cart).trigger('click');
            }
        });

        $('body').on('keypress', '.product-qty', function(e) {
        if (e.keyCode === ENTER) {
          const parentWrapper = $(this).closest('.product-wrapper');
          const cart = $(parentWrapper).find('.add-to-cart');

          $(cart[0]).trigger('click');
        }
      });

        // Разворачивание списка с иконками цветов
        $('body').on('click', '.product-colors .btn-more-color', function(ev){
            ev.preventDefault();

            const productBlock = $(this).closest('.product-block');
            productBlock.toggleClass('active');

            $(this).text(productBlock.is('.active') ? __('Скрыть цвета') : __('Еще') + ' +' + $(this).data('amount'));
        });

        $('body').on('click', '.product-colors .mobile-more-colors', function(ev){
            const productBlock = $(this).closest('.product-block');

            productBlock.toggleClass('active');

            if (productBlock.hasClass('active')) {
                const fixHeightParent = $(this).closest('.product-list-block');
                if (fixHeightParent.length && ! fixHeightParent.hasClass('active')) fixHeightParent.find('.btn-show-more').click();
            }
        });
    },

    initFeedbackForm : function() {
        $('.feedback-form-open').on('click', function(ev){
            ev.preventDefault();

            const isGuest = $(this).data('guest');

            if (isGuest) {
              ajaxGetCity();
            }

            $('.popup-bg').fadeIn(300);
            $('.feedback-popup').addClass('active').css({top: $(window).scrollTop() + 50}).fadeIn(300);
        });

        // Отправка формы
        var inAction = false;

        $('.feedback-popup form').on('submit', function(ev){
            ev.preventDefault();

            if (inAction) return;

            const self = $(this);
            self.find('.success-message, .error-message').remove();

            var data = {};
            self.find('input, textarea').each(function(){
                data[$(this).attr('name')] = $(this).val();
            });

            const purchase_type = $('.contact-director input[name="purchase_type[]"]');
          if ((purchase_type[0] && purchase_type[0].checked === false) && (purchase_type[1] && purchase_type[1].checked === false)) {
            $('.checkbox-block').css('border', '1px solid #d81b07');
            return;
          } else if (purchase_type[0] && purchase_type[1]) {
            delete data['purchase_type[]'];
            data.purchase_type = [];
            if ((purchase_type[0].checked)) {
              data.purchase_type.push($(purchase_type[0]).val());
            }
            if ((purchase_type[1].checked)) {
              data.purchase_type.push($(purchase_type[1]).val());
            }
            // data.purchase_type += (purchase_type[0].checked) ? ' Опт' : '';
            // data.purchase_type += (purchase_type[1].checked) ? ' Дропшиппинг' : '';
          } else {
            // data.purchase_type = data.purchase_type.replace('wholesale', 'Опт').replace('dropshipping', 'Дропшиппинг');
            data.purchase_type = data.purchase_type.split(',');
          }
          $('#send-form-director').hide();
          $('#loader1').removeClass('hidden');

            $.ajax({
                url: '/feedback',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function(res) {
                  inAction = true;
                    if (res.success) {
                        self.prepend($('<div>').addClass('success-message').html(res.message));
                        self.find('input[type=text], textarea').val('');
                    } else {
                        self.prepend($('<div>').addClass('error-message').html(res.messages.join('<br>')));
                    }

                    inAction = false;
                },
              complete: function() {
                $('#loader1').addClass('hidden');
                $('#send-form-director').show();
              }
            })
        });

        mask.init();
        //$('input[name="phone"]').inputmask('mask', {mask: '+38 (999) 999-99-99', clearMaskOnLostFocus: false});
    },

    initUIElements : function() {
        // Выпадающие списки
        $('.custom-select').on('click', function(){
            $(this).toggleClass('active');
            $(this).find('.custom-select-list').toggle();
        });

        $('.custom-select').on('click', '.custom-select-list div:not(.custom-select-search)', function(){
            var selectBlock = $(this).closest('.custom-select');
            var value = $(this).data('value');

            selectBlock.find('.custom-select-active-value').html($(this).clone());
            selectBlock.data('value', value).trigger('select:change');

            if (selectBlock.find('.custom-select-input').length) selectBlock.find('.custom-select-input').val(value).change();
        });

        $('.custom-select-search').on('click', function(ev){
            ev.stopPropagation();
        });

        $('.custom-select-search input').on('keyup', function(){
            var filter = $(this).val();
            var elements = $(this).closest('.custom-select-search').siblings();

            if (filter) {
                elements.each(function(){
                    var itemText = $(this).text();
                    if (itemText.match(new RegExp(filter, 'gi'))) {
                        $(this).removeClass('hidden');
                    } else {
                        $(this).addClass('hidden');
                    }
                });
            } else {
                elements.removeClass('hidden');
            }
        });

        $('.custom-select').each(function(){
            var input = $(this).find('.custom-select-input');
            if (input.length && input.val()) {
                var activeValue = $(this).find('.custom-select-list > *[data-value="' + input.val() + '"]');
                if (activeValue.length) {
                    $(this).find('.custom-select-active-value').html(activeValue.clone());
                }
            }
        });

        // Чекбоксы и радиобаттоны
        // $('.radio input, .checkbox input, .checkbox-revers input').on('change', function(){
        $(document).on('change', '.radio input, .checkbox input, .checkbox-revers input', function(){
            if ($(this).attr('type') == 'radio') {
                $('.radio input[name=' + $(this).attr('name') + ']').each(function(){
                    $(this).closest('label').removeClass('active');
                });

                $(this).closest('label').addClass('active');
            } else {
                $(this).closest('label').toggleClass('active');
            }
        });

        $('.radio input:checked, .checkbox input:checked, .checkbox-revers input:checked').each(function(){
            $(this).closest('label').addClass('active');
        });

        // Разворачивание блоков со скрытыми товарами
        $('.product-list-block .btn-show-more').on('click', function(ev){
            ev.preventDefault();

            var productsBlock = $(this).parent().children('.product-list-wrapper');
            productsBlock.css({height: productsBlock.height()});
            productsBlock.animate({height: productsBlock.get(0).scrollHeight}, 400);

            $(this).parent().addClass('active');
            $(this).hide();
        })

        // Скрытие кнопок разворачивания в блоках без скрытых товаров
        $('.product-list-block .btn-show-more').each(function(){
            var productsBlock = $(this).parent().children('.product-list-wrapper');
            if (productsBlock.get(0).scrollHeight < productsBlock.height() * 1.2) $(this).click();
        });

        // Попапы
        $('.popup-bg').on('click', function(){
            $(this).fadeOut(300);
            $('.popup-block.active').removeClass('active').fadeOut(300);
            $('.category-filters-block.active .mobile-btn-close').click();
        });

        $('.popup-block').on('click', '.btn-close', function(ev){
            ev.preventDefault();
            $('.popup-bg').click();
        });
    },

    initAdaptiveLayout : function() {
        var currentState = 'desktop';

        $('body').on('layout:rebuild', function(){
            app.rebuildLayout();
        });

        $(window).on('resize', function(){
            var width = $(window).width();

            if (width <= 829 && currentState != 'mobile') {
                app.layout = 'mobile';
                $('body').data('layout', app.layout).trigger('layout:rebuild');
            }

            if (width > 829 && app.layout != 'desktop') {
                app.layout = 'desktop';
                $('body').data('layout', app.layout).trigger('layout:rebuild');
            }

            if (app.layout == 'desktop') {
                var menuWidth = $('.header-menu-block .wrapper').width() - $('menu').width() - 6;
                var menuWidthFull = $('.header-menu-block .wrapper').width();

                $('menu .submenu-category-block').css({width: menuWidth});
                $('.menu-models-block .submenu-category-block').css({width: width <= 1790 ? menuWidthFull : menuWidth});

                app.setMainMenuSize();
            }
        });
    },

    rebuildLayout : function() {
        var layout = $('body').data('layout');

        if (layout == 'desktop') {
            // Мобильная боковая панель
            $('menu').appendTo('.header-menu-block .wrapper');
            $('.header-fast-link').appendTo('.header-menu-block .wrapper');
            $('.header-fast-link').after($('.menu-models-block'));
            $('.header-contacts-block').after($('.header-navbar-block'));

            // Блок с ценой в карточке товара
            $('.product-big-block').each(function(){
                $(this).find('.product-colors').after($(this).find('.product-price'));
            });

            // Верхний баннер шапке
            var headerBanner = $('.header-banner-block');
            if (headerBanner.length) {
                $('header').prepend(headerBanner.show());

                if (headerBanner.data('animation')) {
                    clearInterval(headerBanner.data('animation'));
                    headerBanner.data('animation', null);
                }
            }

            // Кнопка Продолжить покупки в корзине
            $('.breadcrumbs-block').before($('.cart-buttons-block'));
        }

        if (layout == 'mobile') {
            // Мобильная боковая панель
            $('.menu-models-block').appendTo('.mobile-sidebar-content-inner');
            $('.header-fast-link').appendTo('.mobile-sidebar-content-inner');
            $('menu').appendTo('.mobile-sidebar-content-inner');
            $('.header-navbar-block').appendTo('.mobile-sidebar-content-inner');

            // Блок с ценой в карточке товара
            $('.product-big-block').each(function(){
                $(this).find('.product-colors').before($(this).find('.product-price'));
            }); // TODO: перестраивать товары, подгружаемые при нажатии Показать еще

            // Кнопка Продолжить покупки в корзине
            $('.breadcrumbs-block').after($('.cart-buttons-block'));

            // Верхний баннер шапке
            var headerBanner = $('.header-banner-block');
            if (headerBanner.length) {
                $('header').after(headerBanner.show());

                headerBanner.data('animation', setInterval(app._animateHeaderBanner, 5000));
                app._animateHeaderBanner();
            }

            app.initMobileSidebar();
        }
    },

    setMainMenuSize : function() {
        if (app.layout != 'desktop') return;

        setTimeout(function(){
            var menuBlock = $('.menu-models-block');
            var menuItems = menuBlock.children('ul').children('li:visible');

            var menuWidth = menuBlock.children('ul').width();
            var menuItemsWidth = 0;

            var menuItemsCount = menuItems.length;

            menuItems.each(function(){
                menuItemsWidth += Math.ceil($(this).outerWidth());
            });

            // Если пунктов меню больше чем влезает - переносим последний из них в пункт Другое
            if (menuWidth < menuItemsWidth) {
                var menuItem = menuItems.eq(menuItemsCount - 2).off('mouseenter mouseleave');
                menuItem.find('.submenu-category-block').remove();
                menuItems.eq(menuItemsCount - 1).find('.submenu-category-block > ul').first().prepend(menuItem);

                return app.setMainMenuSize();
            }

            menuBlock.css({overflow: 'visible'});
            menuBlock.children('ul').css({opacity: 1});
        }, 100);
    },

    /**
     * Анимация траски для заданного блока
     */
    shakeElement : function(element) {
        element.css({position: 'relative'}).animate({left: -10}, 100, function(){
            $(this).animate({left: 10}, 100, function(){
                $(this).animate({left: -10}, 100, function(){
                    $(this).animate({left: 10}, 100, function(){
                        $(this).animate({left: 0}, 100);
                    });
                });
            });
        });
    },

    ReadmoreSeo: function() {
        $('.btn-seo-readmore').on('click', function() {
            $(this).hide();
            $('.seo-description').removeClass('seo-description-hidden');
        });
    },

    /** раскрыть список цветов товара */
    eventCartFocus: function() {
      $('.category-content-block, .product-list-wrapper').on('mouseenter','.product-block', function() {
        if ($(this).find('.btn-more-color').length) {
          if (! $(this).is('.active')) {
            $(this).find('.btn-more-color').trigger('click');
          }
        }
      });

      $('.category-content-block, .product-list-wrapper').on('mouseleave','.product-block', function() {
        if ($(this).find('.btn-more-color').length) {
          if ($(this).is('.active')) {
            $(this).find('.btn-more-color').trigger('click');
          }
        }
      });
    },

    /** смена фото при навидении мыши на товаре */
    initTouch: function() {
        $('.category-content-block, .product-list-wrapper').on('touchstart', '.product-wrapper .product-image a img', function(el) {
            if ($(this).hasClass('in')) {
                const src = $(this).data('tmp');
                if (src.length > 0) {
                    $(this).attr('src', src);
                    $(this).removeClass('in');
                }
            } else {
                const src = $(this).data('in');
                if (src.length > 0) {
                    $(this).data('tmp',  $(this).attr('src'));
                    $(this).attr('src', src);
                    $(this).addClass('in');
                }
            }
        })
            .on('mouseout', '.product-wrapper .product-image a img', function(el) {
                const src = $(this).data('tmp');
                if (src.length > 0) {
                    $(this).attr('src', src);
                }
            })
            .on('mouseover', '.product-wrapper .product-image a img', function(el) {
                const src = $(this).data('in');
                if (src.length > 0) {
                    $(this).data('tmp',  $(this).attr('src'));
                    $(this).attr('src', src);
                }
            });
    },

    /**
     * Анимация бегущей строки для верхнего баннера в шапке
     */
    _animateHeaderBanner : function() {
        var banner = $('.header-banner-block img');

        var animation = banner.hasClass('header-banner-left')
            ? {left: 0}
            : {left: -banner.width() + $(window).width()};

        banner.toggleClass('header-banner-left').animate(animation, 5000, 'linear');
    },

    /**
     * Подстановка данных в HTML-шаблон
     */
    _template : function(html, data) {
        var re = /<%([^%>]+)?%>/g, reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g, code = 'var r=[];\n', cursor = 0, match;
        var add = function(line, js) {
            js? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
                (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
            return add;
        }
        while (match = re.exec(html)) {
            add(html.slice(cursor, match.index))(match[1], true);
            cursor = match.index + match[0].length;
        }
        add(html.substr(cursor, html.length - cursor));
        code += 'return r.join("");';
        return new Function(code.replace(/[\r\t\n]/g, '')).apply(data);
    }

};

function ajaxGetCity()
{
  $.ajax({
    url: '/v1/cities',
    dataType: 'json',
    data: {search: ''},
    success: function(data) {
      const selectBlock = $('.city-select');
      let selectList = selectBlock.find('.custom-select-list');

      for (let i in data) {
        const option = $('<div>').data('value', data[i].id).html(data[i][getLocaleCity]);
        selectList.append(option);
      }
    }
  });
}

function isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }

function __(phrase) {
    return typeof localeStrings !== 'undefined' && localeStrings[phrase] ? localeStrings[phrase] : phrase;
}

function wordDeclension(word, count) {
    var endings = {
        'модел' : ['ь', 'и', 'ей'],
        'цвет' : ['', 'а', 'ов'],
        'наименовани' : ['е', 'я', 'й'],
        'единиц' : ['а', 'ы', ''],
    }

    var wordEndings = endings[word.toLowerCase()];
    if ( ! wordEndings) return word;

    var ending = wordEndings[2];

    if (count < 10 || count > 20) {
        count = count % 10;
        if (count == 1) ending = wordEndings[0];
        if (count > 1 && count < 5) ending = wordEndings[1];
    }

    return word + ending;
}

app.init();
