'use strict'

const videoBaner = {
  init() {
    this.findYoutubeBlock();
  },

  findYoutubeBlock() {
    let block = document.querySelectorAll('.youtube-block');

    for (let i = 0; i < block.length; i++) {
      this.initBlock(block[i]);
    }
  },

  initBlock(block) {
    const youtubeId = block.dataset.youtubeId;
    const videoUrl = block.dataset.videoUrl;

    block.addEventListener('click', () => {
      let iframe = null;
      if (youtubeId.length > 0) {
        iframe = this.createIframeYoutube(youtubeId);
      } else if (videoUrl.length > 0) {
        iframe = this.createIframeVideo(videoUrl);
      }

      if (iframe === null) return;

      this.showVideo(iframe);
    });

    block.classList.add('youtube-block--enabled');
  },

  createIframeYoutube(id) {
    let iframe = document.createElement('iframe');

    iframe.setAttribute('id', 'iframeYoutube');
    iframe.setAttribute('allowfullscreen', '');
    iframe.setAttribute('webkitallowfullscreen', '');
    iframe.setAttribute('mozallowfullscreen', '');
    iframe.setAttribute('allow', 'autoplay');
    iframe.setAttribute('src', this.generateURL(id));
    iframe.classList.add('youtube__iframe');

    return iframe;
  },

  createIframeVideo(url) {
    const video = document.createElement('video');

    video.setAttribute('autoplay', '');
    video.setAttribute('muted', '');
    video.setAttribute('controls', '');
    video.setAttribute('src', url);
    video.classList.add('youtube__iframe');

    return video;
  },

  showVideo(iframe) {
    const body = document.querySelector('.main-page');
    let bg = document.createElement('div');
    bg.setAttribute('id', 'youtubeBg')
    bg.classList.add('popup-bg');
    bg.addEventListener('click', this.closeEvent);
    body.append(bg);

    bg.appendChild(iframe);
    bg.style.display = 'block';
  },
  generateURL(id) {
    let query = '?rel=0&showinfo=0&autoplay=1&muted=1&enablejsapi=1';
    return 'https://www.youtube.com/embed/' + id + query;
  },
  closeEvent() {
    document.getElementById('youtubeBg').remove();
  }
}

videoBaner.init();