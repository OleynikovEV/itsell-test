const cart = {
  messageCourierDelivery: __('Добавь для бесплатной доставки курьером по адресу'),
  messageDelivery: __('Добавь для бесплатной доставки'),
  selectedProducts: [],
  rowId: null,

  init : function() {
    this.eventPositionElement();
    this.changePositionElement();

    this.eventChangeQty();
    this.eventDelete();
    this.eventGroup();
    this.eventExpandGroup();
    this.eventChangeFullBorder();
    this.eventUnfold();
    this.eventGroupControl();
    this.eventSelectGroupControl();
    this.deliveryInit();
    this.eventModal();
    this.eventChangeColor();
    this.eventChangeMaterial();
    this.eventPriceTypeChange();
    this.eventChangeCart();

    $('.cart-item-qty').each(function(){
      $(this).data('prev-qty', $(this).val());
    });

    this.eventSort();
  },

  eventPositionElement: function() {
    cart.positionElementPromoCodeBlock();
    let resize = true;
    $(window).resize(function() {
      cart.changePositionElement();
      cart.positionElementPromoCodeBlock();
      resize = cart.positionElementPriceTypeBlock(resize);
    });
  },
  changePositionElement: function() {
    if($(window).width() <= 990) {
      $('#title-block-info').css({'display': 'block', 'margin': 'auto', 'width': 'auto'});
      $('#title-block-info').insertBefore($('.order-block'));
    } else {
      $('#title-block-info').css({'display': 'inline-block', 'width': '220px'});
      $('.before-title-block-info').before($('#title-block-info'));
    }
  },
  positionElementPriceTypeBlock: function(resize) {
    if ( $(window).width() <= 500 ) {
      $('#price-type').css({'margin': '13px 11px 0 0'});
      $('#panel-block').removeClass('justify-flex-end');
      $('#panel-block').addClass('justify-between');
      $('#inform-ico').removeClass('m-5');
      $('#price-type').detach().prependTo($('#panel-block'));

      return false;
    } else if(resize === false) {
      $($('#price-type')).detach().prependTo($('.sorting-buttons-wrapper '));
      $('#price-type').css({'margin': '0'});
      $('#panel-block').addClass('justify-flex-end');
      $('#panel-block').removeClass('justify-between');
      $('#inform-ico').addClass('m-5');

      return true;
    }
  },
  positionElementPromoCodeBlock: function() {
    if( $(window).width() <= 600 ) {
      $('#promo-block').insertBefore($('#panel-block'));
      $('#promo-block').css('width', '100%');
      $('#promocode').css('width', '100%');
      $('#panel-block').removeClass('justify-between');
      $('#panel-block').addClass('justify-flex-end');
    } else {
      $('#panel-block').prepend($('#promo-block'));
      $('#promo-block').css('width', '20%');
      $('#promocode').css('width', '150px');
      $('#panel-block').addClass('justify-between');
      $('#panel-block').removeClass('justify-flex-end');
    }
  },

  // обновляем текст по доставке
  deliveryInit: function() {
    setTimeout(function() {
      const cartTotal = parseFloat($('.total-sum').text());
      const message = cart.deliveryMessage(cartTotal);
      cart.setDeliveryMessage(message);
    }, 1000);
  },
  // текст для мотивации
  deliveryMessage: function(totalSum) {
    let message = false;
    let diff = 0;

    if (totalSum > 100 && totalSum < 200) {
      diff = 200 - totalSum;
      message = cart.messageCourierDelivery.replace(/sum/gi, diff);
    } else if (totalSum < 100) {
      diff = 100 - totalSum;
      message = cart.messageDelivery.replace(/sum/gi, diff);
    }
    message = (message !== false) ? `<div><p>$ ${diff.toFixed(1)}</p><span>${message}</span></div>` : false;

    return message;
  },
  // устанавливаем сообщение
  setDeliveryMessage: function(message) {
    const messageBlock = $('#title-block-info');
    if (message === false || message === null || message.length === 0) {
      messageBlock.html('');
      messageBlock.hide();
    } else {
      messageBlock.html(message);
      messageBlock.show();
    }
  },

  // смена корзины
  eventChangeCart: function() {
    const cartSelector = $('#default_cart');

    cartSelector.on('change', function() {
      const value = $(this).val();
      Cookies.set('selected_cart', value);

      document.location.reload();
    });
  },

  // событие сортировки
  eventPriceTypeChange: function () {
    $('.select-price-type').change(function() {
      Cookies.set('price_type', $(this).val());
      document.location.href = '/cart';
    });
  },
  eventSort: function() {
    $('.cart-sorting-popup div').click(function() {
      if (cart.isGrouped()) return;

      const sortBy = $(this).data('attr-name');
      const sortCaption = $(this).text();

      cart.setSortCaption(sortCaption);
      cart.sorted('.cart-product-block', sortBy, 'asc');
    });

    $('.sorted').click(function() {
      if (cart.isGrouped()) return;

      const attrName = $(this).data('attr-name');
      const sort = $(this).data('sort');
      const sortInvert = (sort === 'asc') ? 'desc' : 'asc';

      $('.sorted').each(function() {
        $(this).css('fill', '#000');
      });
      $(this).css('fill', '#ff9f15');
      // $(this).attr('data-sort', sortInvert);
      $(this).data('sort', sortInvert);

      cart.sorted('.cart-product-block', attrName, sort);
    });
  },
  // развернуть\свернуть список
  eventUnfold: function() {
    $('#btn-unfold-list').click(function() {
      cart.unfload();
    });
  },
  // развернуть\свернуть список
  unfload: function() {
    const wrapper = $('.cart-content-block');
    const isShow = $('#btn-unfold-list').attr('data-show');

    if (isShow === 'false') {
      wrapper.children('.cart-group-products').each(function (index, item) {
        $(item).find('.body-group-block').show();
      });
      $('#btn-unfold-list').attr('data-show', true);
    } else {
      wrapper.children('.cart-group-products').each(function (index, item) {
        $(item).find('.body-group-block').hide();
      });
      $('#btn-unfold-list').attr('data-show', false);
    }
  },
  // Изменение количества товара
  eventChangeQty: function() {
    $('.spinner-block .plus, .spinner-block .minus').on('click', function(ev){
      ev.preventDefault();
      ev.stopPropagation();

      let qtyInput = $(this).parent().find('input');
      let qty = qtyInput.val();

      if ($(this).is('.plus')) qty++; else qty--;
      qtyInput.val(qty).change();
    });

    // блокируем всплытие события дальше к родителю
    $('.cart-item-qty, .spinner-block').on('click', function(ev) {
      ev.stopPropagation();
    });

    $('.cart-item-qty').on('change', function(){
      const self = $(this);
      const qty = parseInt(self.val());

      if ( ! qty || qty < 0) {
        self.val(self.data('prev-qty'));
        return;
      }

      let data = {};

      if (! cart.isActiveGroupControl()) {
        const rowId = $(this).closest('.cart-product-block').data('row-id');
        data[rowId] = qty;
      } else {
        if (cart.selectedProducts.length === 0) return;

        cart.selectedProducts.forEach(function (rowId) {
          data[rowId] = qty;
        });
      }

      cart.ajaxChangeQty(data, self, qty);

      if (cart.isGrouped()) {
        const parent = $(this).closest('.cart-product-block').data('group-parent');
        cart.updateGroupAmount(parent);
      }
    });
  },
  // Удаление товара из корзины
  eventDelete: function() {
    $('.cart-item-delete').on('click', function(ev){
      ev.preventDefault();

      if (!confirm(__('Действительно хотите удалить?'))) return;

      const rowBlock = $(this).closest('.cart-product-block');
      const rowId = rowBlock.data('row-id');
      cart.ajaxDelete(rowBlock, rowId);
      cart.updateGroupAmount(rowBlock.data('group-parent'));
      cart.deleteEmptyGroup();
    });
  },
  // группировка по цветам
  eventGroup: function() {
    const el = $('#group-color');

    el.click(function() {
      if (!el.hasClass('active')) {
        $('#btn-unfold-list').removeClass('hidden');
        cart.sorted('.cart-product-block', 'sort-title', 'asc');
        cart.groupBy();

        el.addClass('active');
        el.text(__('Показать цвета'));
      } else {
        $('#btn-unfold-list').addClass('hidden');
        cart.ungroup();
        el.removeClass('active');
        el.text(__('Собрать цвета'));
      }
    });
  },
  // раскрыть список сгруппированных товаров
  eventExpandGroup: function() {
    $(document).on('click', '.btn-cart-group', function() {
      const id = $(this).data('id');
      $(`#${id} .body-group-block`).toggle();
    });
  },
  // групповое управление количеством
  eventGroupControl: function() {
    $('#btn-group-control').click(function() {
      cart.selectedProducts = [];
      if (!cart.isActiveGroupControl()) {
        $(this).addClass('active');
        $('.cart-content-block').addClass('cursor_crosshair');

        $('.cart-product-block a').on('click', cart.preventDefault);
      } else {
        $(this).removeClass('active');
        $('.cart-content-block').removeClass('cursor_crosshair');
        $('.cart-product-block').each(function(index, item) {
          $(item).removeClass('bg-orange-select');
        });

        $('.cart-product-block a').off('click', cart.preventDefault);
      }
    });
  },
  // событие нажатия на строку продукта
  eventSelectGroupControl: function() {
    $(document).on('click', '.cart-product-block', function() {
      if (cart.isActiveGroupControl()) {
        if (! cart.isSelected(this)) {
          cart.selected(this);
        } else {
          cart.unSelected(this);
        }
      }
    });
  },
  // событие отображения доступных цветов в модальном окне
  eventModal: function() {
    $('.select-color').click(function() {
      const id = $(this).data('id');
      cart.rowId = $(this).data('rowid');
      modal.show('#change-color-modal');

      cart.ajaxGetColors(id);
    });
  },
  // событие смены цвета
  eventChangeColor: function() {
    $(document).on('click','.color-item', function() {
      const id = $(this).data('id');
      cart.ajaxChangeColor(cart.rowId, id);
    });
  },
  // сменить материал чехла
  eventChangeMaterial: function() {
    $('.select-material').on('change', function() {
      const rowId = $(this).data('rowid');
      const value = $(this).val();

      cart.ajaxChangeMaterial(rowId, value);
    });
  },
  // закраска бортов
  eventChangeFullBorder: function() {
    $('.full-border').on('change', function() {
      const rowId = $(this).data('rowid');
      const value = $(this).is(':checked');
      cart.ajaxChangeBordercolor(rowId, value);
    });
  },

  // отключаем событие
  preventDefault: function(ev) {
    ev.preventDefault();
  },

  // включена ли группировка
  isGrouped: function() {
    return $('#group-color').hasClass('active');
  },
  // включено ли групповое управление
  isActiveGroupControl: function() {
    return $('#btn-group-control').hasClass('active');
  },
  // проверка выбран ли продукт
  isSelected: function(item) {
    return $(item).hasClass('bg-orange-select');
  },

  // группировка товаров
  groupBy: function(attrName = 'data-group-parent') {
    const wrapper = $('.cart-content-block');
    wrapper.children(`.cart-product-block[${attrName}]`).each(function(index, item) {
      const id = $(item).data('group-parent');
      const hasElem = document.getElementById(id);
      let groupBlock;

      if (hasElem === null) {
        groupBlock = cart.createFieldset(item);
        wrapper.append(groupBlock);
        groupBlock = groupBlock.find('.body-group-block');
      } else {
        groupBlock = $(`#${id} .body-group-block`);
        cart.updateGroupAmount(id, item);
      }

      groupBlock.append(item);
    });
  },
  // удаляем пустые группы
  deleteEmptyGroup: function() {
    $('.cart-group-products').each(function(index, item) {
      const amount = Number.parseInt($(item).find('.amount-group-block').text());

      if (amount === 0) {
        const id = $(item).attr('id');
        $(`#${id}`).remove();
      }
    });
  },
  // отключать группировку
  ungroup: function() {
    const wrapper = $('.cart-content-block');
    wrapper.children('.cart-group-products').each(function(index, item) {
      $(item).children('.body-group-block').find('.cart-product-block').each(function(index2, product) {
        wrapper.append(product);
      });
    }).remove();

  },
  // метод сортировки
  sorted: function(selector, attrName, sort) {
    attrName = `data-${attrName}`;
    if (sort === 'asc') {
      $(selector).sortElements(function (a, b) {
        if (isNumber($(a).attr(attrName))) {
          return $(a).attr(attrName)*1 > $(b).attr(attrName)*1 ? 1 : -1;
        } else {
          return $(a).attr(attrName) > $(b).attr(attrName) ? 1 : -1;
        }
      });
    } else {
      $(selector).sortElements(function (a, b) {
        if (isNumber($(a).attr(attrName))) {
          return $(a).attr(attrName)*1 > $(b).attr(attrName)*1 ? -1 : 1;
        } else {
          return $(a).attr(attrName) > $(b).attr(attrName) ? -1 : 1;
        }
      });
    }
  },
  // делаем выбор
  selected: function(item) {
    const id = $(item).data('row-id');
    $(item).addClass('bg-orange-select');
    cart.selectedProducts.push(id);
  },
  // удаляем выбор
  unSelected: function(item) {
    const id = $(item).data('row-id');
    $(item).removeClass('bg-orange-select');
    cart.selectedProducts.splice(id, 1);
  },

  // создаем элемент для группировки
  createFieldset: function(item) {
    const id = $(item).data('group-parent');
    const text = $(item).data('sort-title');
    const brand = $(item).data('sort-brand');
    const model = $(item).data('sort-model');
    const amount = $(item).data('sort-amount');

    // cart-table-title
    const element = $(`<fieldset class="cart-group-products" id="${id}">
          <div class="wrapper clearfix">  
            <div class="bg-gray cart-col-1 title-group-block cursor_p btn-cart-group" data-id="${id}">${text}</div>
            <div class="bg-gray cart-col-2 brand-group-block">${brand}</div>
            <div class="bg-gray cart-col-3 model-group-block">${model}</div>
<!--            <div class="bg-gray cart-col-4">&nbsp;&nbsp;&nbsp;</div>-->
            <div class="bg-gray cart-col-4"></div>
            <div class="bg-gray cart-col-5 amount-group-block">${amount}</div>
<!--            <div class="bg-gray cart-col-6">&nbsp;&nbsp;&nbsp;</div>-->
            <div class="bg-gray cart-col-6"></div>
<!--            <div class="bg-gray cart-col-7">&nbsp;&nbsp;&nbsp;</div>-->
            <div class="bg-gray cart-col-7"></div>
            <div class="bg-gray cart-col-8">
                <div class="arrow-down cursor_p btn-cart-group" data-id="${id}"></div>
                </div>
            <div class="clearfix"></div>
          </div>
          <div class="body-group-block"></div>
        </fieldset>`);

    element.find('.body-group-block').hide();

    return element;
  },
  // обновление количества и стоимости
  updateTotalInfo : function(data) {
        $('.cart-count b').html(data.count);
        $('.cart-count span').html(wordDeclension(__('Единиц'), data.count) + ' ' + __('товара'));

        $('.cart-count-sku b').html(data.count_sku);
        $('.cart-count-sku span').html(wordDeclension(__('Наименовани'), data.count_sku));

        $('.cart-total').html(data.total);

        if (data.items && Object.keys(data.items).length) {
            $('.cart-product-block').each(function(){
                const rowId = $(this).data('row-id');
                if ( ! rowId) return;

                if (data.items[rowId]) {
                    if (data.items[rowId].promocode) {
                        const promo = $(this).find('.product-label-orange');
                        promo.html(`промо -${data.items[rowId].discountRate}%`).removeClass('hidden');
                        $(this).find('.cart-col-4 p').html(`$${data.items[rowId].price} / ₴${data.items[rowId].price_uah}`);
                    } else {
                        $(this).find('.product-label-orange').addClass('hidden');
                    }
                    $(this).find('.cart-col-4 p').html(`$${data.items[rowId].price} / ₴${data.items[rowId].price_uah}`);
                    $(this).find('.cart-col-6 b').html('$' + data.items[rowId].total + ' / ₴' + data.items[rowId].total_uah);
                    $(this).find('.cart-item-qty').val(data.items[rowId].qty);
                    cart.changeDataInCartProductBlock($(this), data.items[rowId].qty, data.items[rowId].total);
                } else {
                    $(this).remove();
                }
            });
        }

        cart.deliveryInit();

        app.updateHeaderCartState(data);
    },
  // обновляем количество при группировка
  updateGroupAmount: function(groupId, item = null) {
    const groupBlock = $(`#${groupId}`);
    let amount = 0;

    if (item !== null) {
      const amountGroupBlock = Number.parseInt(groupBlock.find('.amount-group-block').text());
      const amountItem = Number.parseInt($(item).data('sort-amount'));
      amount = amountGroupBlock + amountItem;
    } else {
      groupBlock.find('.body-group-block').children('.cart-product-block').each(function(index, item) {
        amount += Number.parseInt($(item).attr('data-sort-amount'));
      });
    }

    groupBlock.find('.amount-group-block').text(amount);
  },
  // записываем в блок .cart-product-block
  // чтоб правильно работала сортировка при изменении количества
  changeDataInCartProductBlock: function(self, qty, total) {
    self.attr('data-sort-amount', qty);
    self.attr('data-sort-cost', total);
  },
  // установить текст по какому полю сортируем
  setSortCaption: function(text) {
    $('#sorted-text').text(text)
  },
  // отображаем список доступных цветов
  addColors: function(data) {
    const template = $('#product-template').html();
    const wrapper = $('.modal-wrapper');

    wrapper.empty();

    data.forEach(function(item) {
      wrapper.append(app._template(template, item));
    });
  },

  // запрос на обновление количества
  ajaxChangeQty: function(data, self, qty) {
    $.ajax({
      async: false,
      url: '/cart/update',
      type: 'put',
      data: {products: data},
      dataType: 'json',
      success: function(data) {
        if (data.success) {
          self.data('prev-qty', qty);
          cart.updateTotalInfo(data.response);
        }
      }
    });
  },
  // запрос на удаление товара из корзины
  ajaxDelete: function(rowBlock, rowId) {
    $.ajax({
      async: false,
      url: '/cart/' + rowId,
      type: 'delete',
      dataType: 'json',
      success: function(data) {
        if (data.success) {
          rowBlock.remove();
          cart.updateTotalInfo(data.response);

          // Если в списке не осталось товаров - отображаем сообщение о пустой корзине
          if ( ! $('.cart-item-delete').length) {
            $('.order-block').after(
              $('<div>').addClass('title-block text-center cart-empty-message').html(__('Ваша корзина пуста'))
            );

            $('.cart-sorting-block, .cart-content-block, .order-block').remove();
          }
        }
      }
    })
  },
  // получение списка цветов
  ajaxGetColors: function(parentId) {
    $.ajax({
      async: false,
      type: 'post',
      dataType: 'json',
      url: '/get-product-colors',
      data: {
        id: parentId
      },
      success: function(data) {
        if (data.success === true && data.colors.length > 0) {
          cart.addColors(data.colors);
        } else {
          console.error(data);
        }
      },
      error: function (error) {
        console.error(error);
      }
    });
  },
  // сменить цвет
  ajaxChangeColor: function(rowId, newProductId) {
    $.ajax({
      async: false,
      type: 'post',
      dataType: 'json',
      url: 'cart/changeColor',
      data: {
        rowId: rowId,
        newId: newProductId
      },
      success: function(data) {
        if (data.success === true) {
          location.reload();
        } else {
          console.error(data);
        }
      },
      error: function (error) {
        console.error(error);
      }
    });
  },
  // смена материала
  ajaxChangeMaterial: function(rowId, material) {
    $.ajax({
      async: false,
      type: 'post',
      dataType: 'json',
      url: 'cart/changeMaterial',
      data: {
        rowId: rowId,
        material: material
      },
      success: function(data) {
        if (data.success === true) {
          location.reload();
        } else {
          console.error(data);
        }
      },
      error: function (error) {
        console.error(error);
      }
    });
  },
  // смена материала
  ajaxChangeBordercolor: function(rowId, value) {
    $.ajax({
      async: false,
      type: 'post',
      dataType: 'json',
      url: 'cart/changeBorderColor',
      data: {
        rowId: rowId,
        value: value
      },
      success: function(data) {
        if (data.success === true) {
          location.reload();
        } else {
          console.error(data);
        }
      },
      error: function (error) {
        console.error(error);
      }
    });
  }
};

const promo = {
    id: 'promocode',
    btnId: 'apply',
    loaderId: 'loader',
    input: null,
    loader: null,
    btn: null,
    init: function() {
        this.input = document.getElementById(this.id); // получаем элемент поля ввода
        this.loader = document.getElementById(this.loaderId);
        this.btn = document.getElementById(this.btnId); // получаем элемент кнопка

        // вешаем события на поле
        this.btn.addEventListener('click', this.eventChange);
    },
    eventChange: function() {
        // жадержка перед отправкой запроса на промокод
        if (promo.input.value.length > 0) {
            promo.update(promo.input.value);
        }
    },
    getProducts: function() {
        const products = document.querySelectorAll('.cart-product-block');
        let productIds = [];

        if (products.length > 0) {
            Array.from(products).forEach(function(item) {
                let id = item.getAttribute('data-row-id');
                if (id !== null) {
                    productIds[id] = item.value;
                }
            });
        }

        return productIds;
    },
    update: function(code = null) {
        this.btn.classList.add('hidden');
        this.loader.classList.remove('hidden');
        $.ajax({
            url: '/cart/updatePromocode',
            type: 'POST',
            data: {
                promocode: code,
            },
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    cart.updateTotalInfo(data.response);
                    alert('Промокод применен успешно');
                } else if(data !== false) {
                    alert(data);
                }

                promo.loader.classList.add('hidden');
                promo.btn.classList.remove('hidden');
            },
            error: function(error) {
                console.error(error);
                promo.loader.classList.add('hidden');
                promo.btn.classList.remove('hidden');
            }
        });
    }
};

cart.init();
promo.init();
