if (document.querySelector('.cancel-button')){
    document.querySelector('.cancel-button').addEventListener('click', function(e) {
        window.location.href = e.target.getAttribute('data-url');
    })
}

$(function() {
  $('.link__onclick').click(function() {
    const popup = $('.static-page-popup').html($('<a>').addClass('btn-close').attr('href', '#'));
    const image = $(this).data('image');
    const title = $(this).data('title');
    const imgElm = $('<img>').attr('src', image);

    popup.append($('<div>').addClass('popup-title').html(title))
      .append(imgElm);

    $('.popup-bg').fadeIn(300);

    const options = {
      top: window.scrollY,
      width: window.innerWidth <= 360 ? '90%' : '320px',
      textAlign: 'center'
    };

    if (window.innerWidth >= 830) {
      options.right = 0;
      options.margin = '-125px';
      popup.removeAttr('left');
    } else {
      options.left = '5%';
      options.margin = '0 auto';
    }

    popup.addClass('active').css(options).fadeIn(300);
  });
});
