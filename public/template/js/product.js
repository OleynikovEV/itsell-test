
var colorActive = false;

var product = {
    // сохраняем изначальные цены на общие категории
    selectImage: 0,
    bigSlider: null,
    modalSlider: null,
    wishTextStatus: [__('Сообщить, когда появится'), __('Сообщим о наличии')],
    wishButton: null,
    html: null,
    scrollPosition: 0,

    init : function() {
        product.initGallery();
        product.initModalGallery();
        product.lazyLoad();
        product.initColorsSwitch();
        product.initTabAction();
        product.initDescriptionTabs();

        product.initAdaptiveLayout();

        product.initFoundCheaperPopup();
        //product.initWarrantyPopup();

        product.inputEventChange();

        product.bannersChange();

      $(function() {
        product.html = document.documentElement;
        product.scrollPosition = window.pageYOffset;

        product.wishButton = wishList.initElement('wish').element;

        // product.swipeModalInit();

        const selectToSync = document.getElementById('material');
        if (selectToSync) {
          selectToSync.onchange = function(){
            product.sync(this,'material2');
          };
        }

        const selectToSync2 = document.getElementById('material2');
        if (selectToSync2) {
          selectToSync2.onchange = function(){
            product.sync(this,'material');
          };
        }

        const count = document.getElementById('product-qty');
        const count2 = document.getElementById('product-qty2');
        if (count) {
          count.addEventListener('input', function() {
            count2.value = this.value;
          });
        }
        if (count2) {
          count2.addEventListener('input', function () {
            count.value = this.value;
          });
        }
      });
    },

    swipeModalInit: function() {
      const modal = $('#modal-photo-viewer');
      let positionTop = 0;
      let opacity = 1;
      let updatPosition = 0, currentPosition = 0, direction;

      modal.on('touchmove touchend', function(event) {
        if (event.type === 'touchend') { // если отпустили пальцем
          positionTop = 0;
          updatPosition = 0;
          currentPosition = 0;
          modal.css({'top': positionTop});

          return;
        }

        currentPosition = modal.scrollTop();
        const scrollHeight = modal.prop('scrollHeight');
        const clientHeight = modal.prop('clientHeight');
        const scrollEnd = (scrollHeight - currentPosition);

        direction = (currentPosition > updatPosition) ? 'up' : 'down'; // направление скрола
        updatPosition = currentPosition;

        if (direction === 'down' && currentPosition === 0) {
          positionTop = positionTop + 2;
          opacity = opacity - 0.01;
        }

        if (clientHeight === scrollEnd && direction === 'up') {
          positionTop = positionTop - 2;
          opacity = opacity - 0.01;
        }

        // if (opacity <= 0.5) {
        //   $('.btn-close').trigger('click');
        // }

        modal.css({'top': positionTop});
        // modal.css({opacity: opacity});
      });
    },

    lazyLoad() {
      $(function() {
        const images = document.querySelectorAll('[data-src]');
        setTimeout(function() {
          images.forEach(function(image) {
            image.src = image.dataset.src;
            image.classList.remove('opacity');
            image.classList.remove('width-a');
          });
        }, 1000);
      });
    },

    bannersChange: function() {
      if (bannersList !== null) {
        const amount = bannersList.length - 1;
        let i = 0;

        setInterval(function() {
          if (i < amount) {
            i++;
          } else {
            i = 0;
          }

          if (bannersList[i].link.length > 0) {
            $('#banners').attr('src', `${content_server}${bannersList[i].image}`);
            $('#banners').attr('alt', bannersList[i].alt);
            $('#banners_link').attr('href', bannersList[i].link);
          }
        }, 10000);
      }
    },

    /**
     * Галерея с фотографиями товара
     */
    initGallery : function() {
        const mainSlider = $('.product-photo-block .product-big-photo-content');
        const previewsSlider = $('.product-photo-preview');
        let previewsMode = $(window).width() >= 1320 ? 'vertical' : 'horizontal';
        const productColorsBlock = $('.product-colors-block-top');
        const modalPreview = $('.product-photo-preview-bottom');

        product.bigSlider = mainSlider;

        mainSlider.wmrSlider({
            autoSize: true,
            autoSwitch: false,
            touchSupport: true,
            shiftOnTouch: true,
            speed: 0,
            transitionSpeed: 0,
            dotsWrapper: $('.product-photo-block .mobile-photo-dots')
        });

        // показать модальное окно со слайдером
        mainSlider.on('click', function() {
            product.beforeOpenModal();
            $('.photo-viewer-block').show(1);
            modalPreview.wmrSlidingContent('update-settings', {vertical: 'vertical'});
            $('.photo-viewer-content .product-big-photo-content').wmrSlider('reload');
            product.modalSlider.wmrSlider('show-slide', product.selectImage);
        });

        previewsSlider.wmrSlidingContent({
            vertical: previewsMode === 'vertical',
            switchOnArrow: true,
            hideArrows: false,
            wrapperSelector: '.product-preview-wrapper',
            arrowLeftSelector: '.btn-switch-up',
            arrowRightSelector: '.btn-switch-down'
        });

        mainSlider.on('slider-change', function(ev, activeSlide){
            const productID = $('.product-big-photo-content-top div').eq(activeSlide).data('product-id');

            // Снятие активного цвета
            if (!colorActive){
                const activeColor = productColorsBlock.find('a.active');
                if (activeColor.length) activeColor.removeClass('active');
            }

            //выделение активного цвета
            productColorsBlock.find('a[data-id=' + productID + ']').addClass('active');

            previewsSlider.wmrSlidingContent('show-slide', activeSlide);
            product.selectImage = activeSlide;
        });

        previewsSlider.on('click mouseenter', '.product-preview-slider a', function(ev){
            ev.preventDefault();

            if ($(this).hasClass('active')) return true;

            $(this).removeClass('active');
            $(this).addClass('active');
            let activeImage = $('.product-preview-slider a:visible').index($(this));
            product.selectImage = activeImage;
            mainSlider.wmrSlider('show-slide', activeImage);
        });

        previewsSlider.find('.product-preview-slider a').first().addClass('active');

        $(window).resize(function(){
            const newPreviewsMode = $(window).width() >= 1320 ? 'vertical' : 'horizontal';
            if (newPreviewsMode !== previewsMode) {
                previewsMode = newPreviewsMode;
                previewsSlider.wmrSlidingContent('update-settings', {vertical: previewsMode === 'vertical'})

                const activeImage = $('.product-preview-slider a:visible').index($('.product-preview-slider a.active'));
                previewsSlider.wmrSlidingContent('show-slide', activeImage);
            }
        });
        // показать все фото
        $('.product-show-all-photos-no-modal').on('click', function(ev){
            ev.preventDefault();
            product.showAllPhoto();
            // product._showAllPhotoFull();
            product.selectImage = 0;
            wishList.setElement(product.wishButton, this)
              .changeProductId(this.dataset.parentId)
              .setText(product.wishTextStatus)
              .changeStyle('wish-button-inform__select', 'wish-button-inform');
        });
    },

    beforeOpenModal: function() {
      $('#modal-photo-viewer').css({
        top: 0,
        opacity: 1
      });
      // $('#modal-shadow').addClass('modal__shadow--show');
      product.html.style.top = -product.scrollPosition + "px";
      product.html.classList.add("hystmodal__opened");
    },

    beforeCloseModal: function() {
      // $('#modal-shadow').removeClass('modal__shadow--show');
      product.html.classList.remove("hystmodal__opened");
      $('#modal-photo-viewer').scrollTop(0);
      window.scrollTo(0, product.scrollPosition);
      product.html.style.top = "";
    },

    initModalGallery : function() {
        const mainContainer = $('.photo-viewer-block');
        const mainSlider = mainContainer.find('.photo-viewer-content .product-big-photo-content');
        const previewsSlider = mainContainer.find('.product-photo-preview');
        let previewsMode = $(window).width() >= 1320 ? 'vertical' : 'horizontal';
        const mobileColorsBlock = $('.mobile-colors-block');

        product.modalSlider = mainSlider;
        mainContainer.find('.btn-close').on('click', function() {
            product.beforeCloseModal();
            product.bigSlider.wmrSlider('show-slide', product.selectImage);
            mainContainer.hide(10);
        })

        $(document).keyup(function(e) {
            if (e.keyCode === 27) mainContainer.hide(1);
        });

        mainSlider.wmrSlider({
            autoSize: true,
            autoSwitch: false,
            touchSupport: true,
            shiftOnTouch: true,
            speed: 0,
            transitionSpeed: 0,
            dotsWrapper: mainContainer.find('.photo-viewer-content .mobile-photo-dots'),
            arrowLeft: mainContainer.find('.left-arrow-block'),
            arrowRight: mainContainer.find('.right-arrow-block')
        });

        previewsSlider.wmrSlidingContent({
            vertical: previewsMode === 'vertical',
            switchOnArrow: true,
            hideArrows: false,
            wrapperSelector: mainContainer.find('.product-preview-wrapper'),
            arrowLeftSelector: mainContainer.find('.btn-switch-up'),
            arrowRightSelector: mainContainer.find('.btn-switch-down')
        });

        mainSlider.on('slider-change', function(ev, activeSlide){

            let productID = $('.product-big-photo-content-bottom div').eq(activeSlide).data('product-id');

            // Снятие активного цвета
            if (!colorActive){
                if (mobileColorsBlock.find('a.active').length) {
                    let activeColor = mobileColorsBlock.find('a.active');
                    if (activeColor.length) activeColor.removeClass('active');
                }
            }

            //выделение активного цвета
            mobileColorsBlock.find('a[data-id=' + productID + ']').addClass('active');

            previewsSlider.wmrSlidingContent('show-slide', activeSlide);
            product.selectImage = activeSlide;
        });

        previewsSlider.on('click mouseenter', '.product-preview-slider a', function(ev){
            ev.preventDefault();

            mainContainer.find('.product-preview-slider a').removeClass('active');
            $(this).addClass('active');

            let activeImage = mainContainer.find('.product-preview-slider a:visible').index($(this));
            product.selectImage = activeImage;
            mainSlider.wmrSlider('show-slide', activeImage);
            wishList.setElement(product.wishButton, this)
              .changeProductId(this.dataset.parentId)
              .setText(product.wishTextStatus)
              .changeStyle('wish-button-inform__select', 'wish-button-inform');
        });

        previewsSlider.find('.product-preview-slider a').first().addClass('active');

        $(window).resize(function(){
            const newPreviewsMode = $(window).width() >= 1320 ? 'vertical' : 'horizontal';
            if (newPreviewsMode !== previewsMode) {
                previewsMode = newPreviewsMode;
                previewsSlider.wmrSlidingContent('update-settings', {vertical: previewsMode === 'vertical'})

                const activeImage = $('.product-preview-slider a:visible').index($('.product-preview-slider a.active'));
                previewsSlider.wmrSlidingContent('show-slide', activeImage);
            }
        });

        $('.product-show-all-photos-modal').on('click', function(ev){
            ev.preventDefault();
            product.selectImage = 0;
            product.showAllPhoto();
            // product._showAllPhotoModal();
            wishList.setElement(product.wishButton, this)
              .changeProductId(this.dataset.parentId)
              .setText(product.wishTextStatus)
              .changeStyle('wish-button-inform__select', 'wish-button-inform');
        });
    },

    showAllPhoto: function() {
        colorActive = false;

        product._showAllPhotoFull();
        product._showAllPhotoModal();
    },

    _showAllPhotoFull: function() {
        const self = $('.product-show-all-photos-no-modal');
        const mainSlider = $('.product-photo-block .product-big-photo-content');
        const previewsSlider = $('.product-photo-preview');

        // восстанавливаем изначальный интервал цен
      product.setPrices(self, self.data('parent-id'));
      // const price = priceModal.init('price-block-' + self.data('parent-id'));
      // const range = self.data('price-opt-range');
      // const def = self.data('price-default');
      // const sale = self.data('price-sale');
      //
      // price.setPriceRange(range);
      // price.setPriceDefault(def);
      // price.setPriceSale(sale);
        // $('.product-price-block p').html(product.priceRange);
        // $('.product-page-price-popup').html(product.productPagePricePopup);
        // $('.product-price-block div').first().html(product.productPriceBlock);

        mainSlider.children('div').show(11);
        previewsSlider.find('.product-preview-slider a').show(1);

        mainSlider.wmrSlider('reload');
        previewsSlider.wmrSlidingContent('reset-size');

        previewsSlider.find('.product-preview-slider a').removeClass('active').first().addClass('active');

        self.addClass('hidden');

        // Снятие выделения с активного цвета и обновление названия товара
        self.closest('.product-content-block').find('.product-colors-block a').removeClass('active');

        let titleBlock = self.closest('.product-content-block').find('.product-title-block');
        titleBlock.html(titleBlock.data('title'));

        // Обновление статуса наличия товара
        // let availabilityBlock = $('.product-code-block .product-availability-block');
        let availabilityBlock = $('.product-code-block .product-availability');
        let whenAppear = availabilityBlock.data('when-appear');

        availabilityBlock.html(whenAppear ? __('ожидается') : __('в наличии'));
        if (whenAppear) availabilityBlock.addClass('soon-available'); else availabilityBlock.removeClass('soon-available');
    },

    _showAllPhotoModal: function() {
        const self = $('.product-show-all-photos-modal');
        const mainContainer = $('.photo-viewer-block');
        const mainSlider = mainContainer.find('.photo-viewer-content .product-big-photo-content');
        const previewsSlider = mainContainer.find('.product-photo-preview');

        // восстанавливаем изначальный интервал цен
        product.setPrices(self, self.data('parent-id'))
        // const price = priceModal.init('price-block-' + self.data('parent-id'));
        // const range = self.data('price-opt-range');
        // const def = self.data('price-default');
        // const sale = self.data('price-sale');
        //
        // price.setPriceRange(range);
        // price.setPriceDefault(def);
        // price.setPriceSale(sale);
        // $('.product-price-block p').html(product.priceRange);
        // $('.product-page-price-popup').html(product.productPagePricePopup);
        // $('.product-price-block div').first().html(product.productPriceBlock);

        mainSlider.children('div').show(1);
        previewsSlider.find('.product-preview-slider a').show(1);
        mainSlider.wmrSlider('reload');
        previewsSlider.wmrSlidingContent('reset-size');

        previewsSlider.find('.product-preview-slider a').removeClass('active').first().addClass('active');

        self.addClass('hidden');

        let titleBlock = self.closest(self.is('.product-show-all-photos-modal') ? '.photo-viewer-wrapper' : '.product-content-block');
        if (self.is('.product-show-all-photos-modal')){
            // Снятие выделения с активного цвета и обновление названия товара
            titleBlock.find('.product-colors-block a').removeClass('active');
            titleBlock = titleBlock.find('.title-block')
        } else {
            // Снятие выделения с активного цвета и обновление названия товара
            titleBlock.find('.product-colors-block a').removeClass('active');
            titleBlock = titleBlock.find('.product-title-block');
        }
        titleBlock.html(titleBlock.data('title'));

        // Обновление статуса наличия товара
        // let availabilityBlock = $('.product-code-block .product-availability-block');
        let availabilityBlock = $('.product-code-block .product-availability');
        let whenAppear = availabilityBlock.data('when-appear');

        availabilityBlock.html(whenAppear ? __('ожидается') : __('в наличии'));
        if (whenAppear) availabilityBlock.addClass('soon-available'); else availabilityBlock.removeClass('soon-available');
    },

    initTabAction: function() {
        $('.product-colors-block-top div input, .product-qty').on('focusin', function(e){
            $(this).prev().trigger('click');
        }).keypress(function(e) {
            if (e.keyCode === ENTER) {
                $('.add-to-cart-product-page').trigger('click');
            }
        });

        $('.product-color-qty-modal').on('focusin', function(e){
          $(this).prev().trigger('click');
        }).keypress(function(e) {
          if (e.keyCode === ENTER) {
            $('.add-to-cart-product-page-modal').trigger('click');
          }
        });
    },

  /**
   * синхронизация двух селектов
   */
    sync: function(el1, el2) {
    if (!el1) {
      return false;
    }
    else {
      let val = el1.value;
      let syncWith = document.getElementById(el2);
      let options = syncWith.getElementsByTagName('option');
      for (let i = 0, len = options.length; i < len; i++) {
        if (options[i].value === val) {
          options[i].selected = true;
        }
      }
    }
  },
  /**
     * Переключение цветов товара
     */
    initColorsSwitch : function() {
        $('.product-colors-block a').on('click', function(ev){
            ev.preventDefault();

            const self = $(this);
            const colorId = self.data('id');
            const colors = $('.product-colors-block').find('a');

            colors.each(function (index,item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            })

            // Выделение активного цвета и изменение названия товара
            setTimeout(function() {
                $(`.product-colors-block a[data-id='${colorId}']`).addClass('active');
            }, 10);

            let titleBlock = self.closest(self.is('.cart-color') ? '.product-content-block' : '.photo-viewer-wrapper');

            if (self.is('.cart-color')){
                titleBlock = titleBlock.find('.product-title-block');
            } else {
                titleBlock = titleBlock.find('.title-block')
            }

            if ( ! titleBlock.data('title')) titleBlock.data('title', titleBlock.text());
            titleBlock.html(titleBlock.data('title') + ' (' + self.attr('title') + ')');

            // выводим цену выбранного товара
            product.setPrices(self, self.data('parent'));

            // Скрытие в галерее фотографий остальных цветов
            $('.product-photo-preview .product-preview-slider a, .product-big-photo-content > div').each(function(){
                if ($(this).data('product-id') === colorId) $(this).removeClass('hidden').show(); else $(this).hide();
            });

            $('.product-photo-preview').wmrSlidingContent('reset-size');

            // Скрытие в галерее фотографий остальных цветов
            // модальное окно галерея
            $('.product-photo-preview .product-preview-slider-bottom a, .product-big-photo-content-bottom > div').each(function(){
                if ($(this).data('product-id') === colorId) {
                    $(this).removeClass('hidden').show();
                } else {
                    $(this).hide();
                }
            });
            $('.photo-viewer-content .product-big-photo-content').wmrSlider('reload');

            // Большая галерея
            $('.product-photo-preview .product-preview-slider-top a, .product-big-photo-content-top > div').each(function(){
                if ($(this).data('product-id') === colorId) {
                    $(this).removeClass('hidden').show();
                } else {
                    $(this).hide();
                }
            });
            $('.product-photo-block .product-big-photo-content').wmrSlider('reload');
            $('.product-show-all-photos').removeClass('hidden');

            $('.product-photo-preview .product-preview-slider a:visible').removeClass('active').first().addClass('active');

            // Обновление статуса наличия товара
            //let availavilityBlock = $('.product-code-block .product-availability-block');
            let availavilityBlock = $('.product-code-block .product-availability');
            let whenAppear = $(this).data('when-appear');
            wishList.setElement(product.wishButton, this)
              .changeProductId(colorId)
              .changeStyle('wish-button-inform__select', 'wish-button-inform');

            availavilityBlock.html(whenAppear ? __('ожидается') : __('в наличии'));
            if (whenAppear) {
              availavilityBlock.addClass('soon-available');
              wishList.setText(product.wishTextStatus).show();
            } else {
              availavilityBlock.removeClass('soon-available');
              wishList.setText(product.wishTextStatus).hidden();
            }
        });
    },

    setPrices: function(self, parent) {
      const price = priceModal.init('price-block-' + parent);
      const currentOptPriceRange = self.data('price-opt-range');
      const setPriceDefaultUah = self.data('price-default-uah');
      const currentOptPrice = self.data('price-opt');
      const currentVipPrice = self.data('price-vip');
      const currentDealerPrice = self.data('price-dealer');
      const currentDropUahPrice = self.data('price-drop-uah');
      const currentPPCPrice = self.data('price-ppc');
      const currentPriceOld = self.data('price-old');
      const defaultPrice = self.data('price-default');

      price.setPriceRange(currentOptPriceRange);
      price.setPriceDefaultUah(setPriceDefaultUah);
      price.setPriceOpt(currentOptPrice);
      price.setPriceVip(currentVipPrice);
      price.setPriceDiler(currentDealerPrice);
      price.setPriceRrc(currentPPCPrice);
      price.setPriceDrop(currentDropUahPrice);
      price.setPriceSale(currentPriceOld);
      price.setPriceDefault(defaultPrice);
    },

    initDescriptionTabs : function() {
        $('.tabs-title-block li').click(function(ev){
            ev.preventDefault();

            $(this).closest('.tabs-title-block').find('li').removeClass('active');
            $(this).addClass('active');

            var tabIndex = $(this).index();
            if (tabIndex) {
                var selector = $(this).find('a').attr('href');
                $(this).closest('.tabs-wrapper-block').find('.tabs-info-block').hide();
                $(selector).show();
            } else {
                $(this).closest('.tabs-wrapper-block').find('.tabs-info-block').show();
            }
        });

        $('.tabs-info-block').show();
    },

    /**
     * Блок Нашли дешевле
     */
    initFoundCheaperPopup : function() {
        // Открытие попапа
        $('.cheaper-block a').on('click', function(ev){
            ev.preventDefault();

            $('.popup-bg').fadeIn(300);
            $('.found-cheaper-popup').addClass('active').css({top: $(window).scrollTop() + 50}).fadeIn(300);
        });

        // Отправка формы
        var inAction = false;

        $('.found-cheaper-popup form').on('submit', function(ev){
            ev.preventDefault();

            if (inAction) return;

            $('.custom-select-active-value').css('border', '1px solid #d1d2d5');
            $('.checkbox-block').css('border', '0');

            var self = $(this);
            self.find('.success-message, .error-message').remove();

            var data = {};
            self.find('input, textarea').each(function(){
                data[$(this).attr('name')] = $(this).val();
            });

            const city = $('input[name="city"]').val();
            const purchase_type = $('input[name="purchase_type[]"]');

            if (city.length === 0) {
              $('.custom-select-active-value').css('border', '1px solid #d81b07');
              return;
            } else if ( $('.custom-select-active-value').is('div') ) {
              data.city = $('.custom-select-active-value div').text();
            }

            if ((purchase_type[0] && purchase_type[0].checked === false) && (purchase_type[1] && purchase_type[1].checked === false)) {
              $('.checkbox-block').css('border', '1px solid #d81b07');
              return;
            } else if (purchase_type[0] && purchase_type[1]) {
              delete data['purchase_type[]'];
              data.purchase_type = [];
              if ((purchase_type[0].checked)) {
                data.purchase_type.push($(purchase_type[0]).val());
              }
              if ((purchase_type[1].checked)) {
                data.purchase_type.push($(purchase_type[1]).val());
              }
              // data.purchase_type = '';
              // data.purchase_type += (purchase_type[0].checked) ? ' Опт' : '';
              // data.purchase_type += (purchase_type[1].checked) ? ' Дропшиппинг' : '';
            } else {
              data.purchase_type = data.purchase_type.split(',');
              // data.purchase_type = data.purchase_type.replace('wholesale', 'Опт').replace('dropshipping', 'Дропшиппинг');
            }

            $('#send-form').hide();
            $('#loader').removeClass('hidden');
            inAction = true;
            $.ajax({
                url: '/feedback/sendMessage',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function(res) {
                    if (res.success) {
                        self.prepend($('<div>').addClass('success-message').html(res.message));
                        self.find('input[type=text], textarea').val('');
                    } else {
                        self.prepend($('<div>').addClass('error-message').html(res.messages.join('<br>')));
                    }

                    inAction = false;
                },
                complete: function() {
                  $('#loader').addClass('hidden');
                  $('#send-form').show();
                }
            })
        });
    },

    // /**
    //  * Блок Гарантии
    //  */
    // initWarrantyPopup : function() {
    //     $('.page-button').on('click', function(ev){
    //         ev.preventDefault();
    //
    //         var popup = $('.static-page-popup').html($('<a>').addClass('btn-close').attr('href', '#'));
    //         const page = $(this).data('page-name');
    //
    //         $.ajax({
    //             url: `/pages/${page}?type=json`,
    //             dataType: 'json',
    //             success: function(res) {
    //                 popup.append($('<div>').addClass('popup-title').html(res.title))
    //                     .append(res.content);
    //
    //                 $('.popup-bg').fadeIn(300);
    //                 popup.addClass('active').css({top: $(window).scrollTop() + 50}).fadeIn(300);
    //             }
    //         })
    //     });
    // },

    initAdaptiveLayout : function() {
        $('body').on('layout:rebuild', function(){
            product.rebuildLayout();
        });

        product.rebuildLayout();
    },

    rebuildLayout : function() {
        const layout = $('body').data('layout');

        if (layout === 'desktop') {
            $('.product-banner-block').prependTo('.product-info-block');
            $('.product-code-block').prependTo('.product-info-block');
            $('.product-title-block').prependTo('.product-info-block');
            $('.product-description-block').before($('.fast-category-link'));
            $('.product-colors-block').before($('.info-blocks'));
            $('.cross-products-block').before($('.product-description-block'));

            $('.photo-viewer-block').find('.photo-viewer-right-block').appendTo('.photo-viewer-description');
        }

        if (layout === 'mobile') {
            $('.product-photo-block').before($('.product-title-block'));
            $('.product-photo-block').before($('.product-code-block'));
            if ($('.cross-products-block').length) {
              $('.cross-products-block').after($('.info-blocks'));
            } else {
              $('.tabs-content-block').after($('.info-blocks'));
            }
            // $('.tabs-info-block').after($('.cross-products-block'));

            $('.product-photo-block').before($('.product-banner-block'));
            // $('.product-description-block').before($('.info-blocks'));
            // $('.found-cheaper-popup').after($('.cross-products-block'));

            // if ($('.fast-category-link').length) {
            //     $('.fast-category-link').after($('.info-blocks'));
            // } else {
            //     $('.add-to-cart-product-page').after($('.info-blocks'));
            // }
            // $('.info-blocks').after($('.product-description-block'));
            // $('.photo-viewer-block').find('.photo-viewer-content').after($('.photo-viewer-right-block'));
        }
    },

  inputEventChange() {
    const inputs = document.querySelectorAll('.product-colors-block input');

    inputs.forEach(function(input) {
      input.addEventListener('input', product.__inputQty);
    });
  },

  __inputQty: function() {
    const value = this.value;
    const id = this.dataset.id;
    const inputSecond = document.querySelectorAll(`input[data-id='${id}']`);

    inputSecond.forEach(function(item) {
      item.value = value;
    });
  }
}

product.init();
