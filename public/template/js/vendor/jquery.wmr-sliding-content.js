(function($){

    var wmrSlidingContent = function(element, settings) {
        this.init(element, settings);
    };

    wmrSlidingContent.prototype = {

        element : null,
        wrapper : null,
        slider : null,
        slides : null,

        dotsWrapper : null,
        dots: null,

        settings : {},

        slideWidth : 0,

        activeSlide : 0,

        lastSwipePosition : 0,

        enabled : true,

        init : function(element, settings){
            var self = this;

            // Расширяем настройки
            self.settings = jQuery.extend({
                slidesPerScreen : 0,
                transitionSpeed : 500,
                rows : 1,

                autoWidth: false,
                touchSupport : false,

                vertical: false,
                hideArrows: true,

                wrapperSelector : '.wmr-sliding-content-wrapper',
                arrowLeftSelector : '.wmr-sliding-content-left',
                arrowRightSelector : '.wmr-sliding-content-right',
                dotsWrapperSelector: false,

                switchOnArrow : false
            }, settings);

            self.element = element;
            self.wrapper = element.children(self.settings.wrapperSelector);
            self.slider = self.wrapper.children();

            self.setSliderSizes();

            self.initEventsListeners();

            if (self.settings.touchSupport) self.initTouchSupport();
        },

        // Инициализация слушателей событий
        initEventsListeners : function(){
            var self = this;

            // Пересчет размеров слайдера при изменении размеров окна
            $(window).resize(function(){
                self.setSliderSizes();
            });

            // Нажатия на стрелки для переключения слайдов
            self.element.find(self.settings.arrowRightSelector).on('click', function(ev){
                ev.preventDefault();

                self.showNextSlide();
            });

            self.element.find(self.settings.arrowLeftSelector).on('click', function(ev){
                ev.preventDefault();

                self.showPrevSlide();
            });

            // Переключение слайдера при нажатии на точки статуса
            if (self.dots) {
                self.dots.on('click', function(ev){
                    ev.preventDefault();

                    if (self.activeSlide == $(this).index()) return;

                    self.activeSlide = $(this).index();
                    self.showActiveSlide();
                });
            }
        },

        // Установка размеров слайдера
        setSliderSizes : function(resetActiveSlide){
            var self = this;
            if ( ! self.enabled) return;

            self.settings.slidesPerScreen = 0;

            self.getSliderSizes();

            var styles = {
                top: 0,
                left: 0
            };

            if (self.settings.autoWidth) {
                self.slides.each(function(){
                   styles.width += $(this).outerWidth(true) + 1;
                });
            } else {
                if (self.settings.vertical) {
                    styles.height = Math.ceil(self.slides.length / self.settings.rows) * self.slideWidth;
                    styles.width = 'auto';
                } else {
                    styles.width = Math.ceil(self.slides.length / self.settings.rows) * self.slideWidth;
                    styles.height = 'auto';
                }
            }

            self.slider.css(styles);

            if (self.settings.dotsWrapperSelector) self.initStatusDots();

            if (self.settings.hideArrows) {
                if ( ! self.settings.slidesPerScreen || self.settings.slidesPerScreen >= self.slides.length) {
                    self.element.find(self.settings.arrowLeftSelector).hide();
                    self.element.find(self.settings.arrowRightSelector).hide();
                } else {
                    self.element.find(self.settings.arrowLeftSelector).show();
                    self.element.find(self.settings.arrowRightSelector).show();
                }
            }

            if (resetActiveSlide) self.activeSlide = 0;
        },

        // Получение размеров слайда
        getSliderSizes : function(){
            var self = this;

            self.slides = self.slider.children(':visible');
            self.slideWidth = self.settings.vertical ? self.slides.eq(0).outerHeight(true) : self.slides.eq(0).outerWidth(true);

            if ( ! self.settings.autoWidth && ! self.settings.slidesPerScreen) {
                self.settings.slidesPerScreen = Math.max(self.settings.vertical ? Math.floor(self.wrapper.height() / (self.slideWidth - 1))
                    : Math.floor(self.wrapper.width() / (self.slideWidth - 1)), 1);
            }
        },

        // Инициализация точек со статусом слайдера
        initStatusDots : function(){
            var self = this;

            if ( ! self.settings.dotsWrapperSelector) return;

            self.dotsWrapper = $(self.settings.dotsWrapperSelector).empty();
            if ( ! self.dotsWrapper) return;

            if ( ! self.settings.customDots) {
                for (var i = 0; i < self.slides.length; i++){
                    self.dotsWrapper.append($('<div>').addClass('wmr-slider-dot'));
                }
            }

            self.dots = self.dotsWrapper.children('*');
            self.dots.eq(0).addClass('active');
        },

        initTouchSupport : function(){
            var self = this;

            self.slider.swipe({
                allowPageScroll : 'vertical',
                threshold : 5,
                swipeStatus : function(ev, phase, direction, distance){
                    if (direction == 'top' || direction == 'bottom') return;

                    switch (phase){
                        case 'start':
                            self.lastSwipePosition = 0;

                            break;

                        case 'move':
                            if (direction != 'left' && direction != 'right') break;
                            var sign = '+=';
                            if (direction == 'left') sign = '-=';

                            distance -= self.lastSwipePosition;
                            self.lastSwipePosition += distance;

                            self.slider.css({'left': sign + distance + 'px'});

                            break;

                        case 'end':
                            if ( ! self.settings.autoWidth) {
                                if (distance >= 50) {
                                    if (direction == 'left'){
                                        self.showNextSlide();
                                    }

                                    if (direction == 'right'){
                                        self.showPrevSlide();
                                    }
                                } else {
                                    // Если размер свайпа слишком маленький - возв возвращаем слайд на оригинальную позицию
                                    self.showActiveSlide();
                                }
                            } else {
                                var closestSlide = 0;
                                var minDistance = -1;

                                var currentLeft = parseInt(self.slider.css('left'));
                                self.slides.each(function(index){
                                    var distance = Math.abs(currentLeft + $(this).position().left);

                                    if (minDistance < 0 || distance < minDistance) {
                                        minDistance = distance;
                                        closestSlide = index;
                                    }
                                });

                                if (closestSlide != self.activeSlide) {
                                    self.showSlide(closestSlide, false);
                                } else if (distance >= 50) {
                                    if (direction == 'left'){
                                        self.showNextSlide();
                                    }

                                    if (direction == 'right'){
                                        self.showPrevSlide();
                                    }
                                } else {
                                    self.showActiveSlide();
                                }
                            }

                            break;
                    }
                }
            });
        },

        // Переключение на следующий слайд
        showNextSlide : function(){
            var self = this;

            if ( ! self.settings.switchOnArrow) {
                if ( ! self.settings.autoWidth) {
                    if (self.activeSlide + self.settings.slidesPerScreen * 2 > Math.ceil(self.slides.length / self.settings.rows)){
                        self.activeSlide = Math.ceil(self.slides.length / self.settings.rows) - self.settings.slidesPerScreen;
                        if (self.activeSlide < 0) self.activeSlide = 0;
                    } else {
                        self.activeSlide += self.settings.slidesPerScreen;
                    }
                } else {
                    var wrapperWidth = self.wrapper.width();
                    var sliderWidth = self.slider.width();
                    var sliderLeft = parseInt(self.slider.css('left'));

                    if (sliderWidth + sliderLeft > wrapperWidth)
                    {
                        self.activeSlide += self.settings.slidesPerScreen;
                        if (self.activeSlide > self.slides.length - 1) self.activeSlide = self.slides.length - 1;
                    }
                }
            } else {
                var index = self.slides.index(self.slides.filter('.active')) + 1;
                if (index >= self.slides.length) index = 0;

                self.activeSlide = index < self.slides.length - self.settings.slidesPerScreen ? index
                    : self.slides.length - self.settings.slidesPerScreen;

                self.slides.eq(index).click();
            }

            self.showActiveSlide();
        },

        // Переключение на предыдущий слайд
        showPrevSlide : function(){
            var self = this;

            if ( ! self.settings.switchOnArrow) {
                if (self.activeSlide - self.settings.slidesPerScreen < 0){
                    self.activeSlide = 0;
                } else {
                    self.activeSlide -= self.settings.slidesPerScreen;
                }
            } else {
                var index = self.slides.filter('.active').index() - 1;
                if (index < 0) index = self.slides.length - 1;

                self.activeSlide = index < self.slides.length - self.settings.slidesPerScreen ? index
                    : self.slides.length - self.settings.slidesPerScreen;

                self.slides.eq(index).click();
            }

            self.showActiveSlide();
        },

        // Отображение определенного слайда
        showSlide : function(index, showMinLeftSlide){
            var self = this;

            if (typeof(showMinLeftSlide) === 'undefined') showMinLeftSlide = true;

            self.activeSlide = index;

            var activeSlide = self.slides.eq(self.activeSlide);
            var activeSlidePosition = activeSlide.length ? (self.settings.vertical ? activeSlide.position().top : activeSlide.position().left) : 0;
            var activeSlideWidth = activeSlide.length ? (self.settings.vertical ? activeSlide.outerHeight(true) : activeSlide.outerWidth(true)) : 0;

            var wrapperWidth = self.settings.vertical ? self.wrapper.height() : self.wrapper.width();
            var minLeft = activeSlidePosition + activeSlideWidth - wrapperWidth;

            if (showMinLeftSlide) {
                for (var i = 0; i < self.slides.length; i++) {
                    var currentPosition = self.settings.vertical ? self.slides.eq(i).position().top : self.slides.eq(i).position().left;
                    if (currentPosition > minLeft) {
                        self.activeSlide = i;
                        break;
                    }
                }
            } else {
                var sliderWidth = self.slider.width();
                if (sliderWidth - activeSlidePosition < wrapperWidth) {
                    for (var i = self.activeSlide; i >= 0; i--) {
                        if (sliderWidth - self.slides.eq(i).position().left < wrapperWidth) {
                            self.activeSlide = i;
                        }
                        else break;
                    }
                }
            }

            // авто прокрутка слайдера вниз/вверх
            // self.showActiveSlide();

            self.slides.removeClass('active').eq(index).addClass('active');
        },

        // Отображение активного слайда
        showActiveSlide : function(){
            var self = this;

            var animation = {};

            if (self.settings.autoWidth) {
                animation.left = -1 * self.slides.eq(self.activeSlide).position().left;
            } else {
                // листает слайдер вверх - вниз
                if (self.settings.vertical) {
                    animation.top = -1 * self.slideWidth * self.activeSlide;
                } else {
                    animation.left = -1 * self.slideWidth * self.activeSlide;
                }
            }

            self.slider.stop(true).animate(animation, self.settings.transitionSpeed);

            if (self.dots) self.dots.removeClass('active').eq(self.activeSlide).addClass('active');
        }

    };

    $.fn.wmrSlidingContent = function(settings, value){
        if (typeof(settings) === 'string') {
            this.each(function() {
                if ( ! $(this).data('wmr-sliding-content')) return;
                var slidingContent = $(this).data('wmr-sliding-content');

                switch (settings) {
                    case 'show-slide':
                        slidingContent.showSlide(value);
                        break;

                    case 'reset-size':
                        slidingContent.setSliderSizes(true);
                        break;

                    case 'disable':
                        slidingContent.enabled = false;
                        break;

                    case 'enable':
                        slidingContent.enabled = true;
                        break;

                    case 'update-settings':
                        for (var setting in value) slidingContent.settings[setting] = value[setting];
                        slidingContent.setSliderSizes(true);
                        break;
                }
            });
        } else {
            this.each(function(){
                if ($(this).data('wmr-sliding-content')) return;

                var carousel = new wmrSlidingContent($(this), settings);
                $(this).data('wmr-sliding-content', carousel);
            });
        }

        return this;
    }

})(jQuery);
