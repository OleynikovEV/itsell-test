(function($){

    var wmrSlider = function(element, settings){
        this.init(element, settings);
    };

    wmrSlider.prototype = {

        slider: null,
        slides: null,
        dots : null,

        settings : {},

        isHover : false,

        activeSlide : 0,

        progress : 0,
        step : 50,

        lastSwipePosition : 0,

        directions : {
            LEFT : 'left',
            RIGHT : 'right'
        },

        // Инициализация слайдера
        init : function(element, settings) {
            var self = this;

            self.slider = element;
            self.slides = self.slider.children();

            // Расширяем настройки
            self.settings = jQuery.extend({
                width: 500,
                height: 200,

                autoSize: false,
                fullWidth: false,

                speed: 5000,
                transitionSpeed: 500,
                rightKey: true,
                leftKey: true,
                autoSwitch : true,

                touchSupport: false,
                shiftOnTouch : false,

                arrowLeft: null,
                arrowRight: null,

                dotsWrapper: null,
                customDots: false
            }, settings);

            self.setElementsStyles();
            self.recountSliderSize();

            if (self.settings.dotsWrapper) self.initStatusDots();

            self.initEventsListeners();

            if (self.settings.autoSwitch && self.slides.length > 1) self.initAutoSwitch();
            if (self.settings.touchSupport) self.initTouchSwipes();
        },

        // Перезагрузка слайдера
        reload : function(){
            var self = this;

            self.activeSlide = 0;
            self.slides = self.slider.children(':visible');

            self.setElementsStyles();
            self.recountSliderSize();

            if (self.settings.dotsWrapper) self.initStatusDots();

            // self.slider.trigger('slider-change', self.activeSlide);
            self.slider.trigger('slider-reload', self.activeSlide);
        },

        // Инициализация слушателей событий
        initEventsListeners : function(){
            var self = this;

            // Наведение курсора на слайдер
            self.slider.mouseenter(function(){
                self.isHover = true;
            });
            self.slider.mouseleave(function(){
                self.isHover = false;
            });

            // Нажатия на стрелки
            if (self.settings.arrowRight) {
                self.settings.arrowRight.on('click', function(ev) {
                    ev.preventDefault();

                    self.showNextSlide();
                });
            }

            if (self.settings.arrowLeft) {
                self.settings.arrowLeft.on('click', function(ev) {
                    ev.preventDefault();

                    self.showPrevSlide();
                });
            }

            if (self.settings.rightKey) {
                $(document).keyup(function(e) {
                    if (e.keyCode === 37) {
                        self.showPrevSlide();
                    }
                });
            }

            if (self.settings.leftKey) {
                $(document).keyup(function(e) {
                    if (e.keyCode === 39) {
                        self.showNextSlide();
                    }
                });
            }

            // Переключение слайдера при нажатии на точки статуса
            if (self.settings.dotsWrapper) {
                self.settings.dotsWrapper.on('click', '*', function(ev){
                    ev.preventDefault();

                    if (self.activeSlide == $(this).index()) return;

                    var direction = $(this).index() > self.activeSlide ? self.directions.RIGHT : self.directions.LEFT;

                    self.activeSlide = $(this).index();
                    self.showActiveSlide(direction);
                });
            }

            // Пересчитываем размеры слайдера при изменении размеров окна
            $(window).resize(function(){
                self.recountSliderSize();
            });
        },

        // Инициализация автоматического переключения слайдов
        initAutoSwitch : function(){
            var self = this;

            setInterval(function(){
                if (self.slides.length == 1) return;

                if ( ! self.isHover){
                    self.progress += self.step;
                }

                if (self.progress >= self.settings.speed){
                    self.showNextSlide();
                }
            }, self.step);
        },

        // Инициализация переключения по свайпам
        initTouchSwipes : function(){
            const self = this;

            self.slider.swipe({
                allowPageScroll : 'vertical',
                threshold : 5,
                tap : function(ev) {
                    $(this).trigger('slider-tap');
                },
                swipeStatus : function(ev, phase, direction, distance){
                    if (direction == 'top' || direction == 'bottom') return;

                    switch (phase){
                        case 'start':
                            self.lastSwipePosition = 0;

                            break;

                        case 'move':
                            break;
                            if (self.settings.shiftOnTouch) {
                                if (direction != 'left' && direction != 'right') break;
                                var sign = '+=';
                                if (direction == 'left') sign = '-=';

                                distance -= self.lastSwipePosition;
                                self.lastSwipePosition += distance;

                                self.slides.eq(self.activeSlide).css({'left': sign + distance + 'px'});
                            }

                            break;

                        case 'end':
                            if (distance >= 1 && self.slides.length > 1) {
                                if (direction == 'left'){
                                    self.showNextSlide();
                                }

                                if (direction == 'right'){
                                    self.showPrevSlide();
                                }
                            } else {
                                // Если размер свайпа слишком маленький - возв возвращаем слайд на оригинальную позицию
                                if (self.settings.shiftOnTouch) self.slides.eq(self.activeSlide).animate({left: 0}, 150);
                            }

                            break;
                    }
                }
            });
        },

        // Переключение на заданный слайд
        showSlide : function(index) {
            var self = this;

            if (index < self.slides.length) {
                self.activeSlide = index;
                self.showActiveSlide();
            }
        },

        // Переключение на следующий слайд
        showNextSlide : function() {
            var self = this;
            if (self.slides.length == 1) return;

            self.activeSlide++;
            if (self.activeSlide == self.slides.length) self.activeSlide = 0;

            self.showActiveSlide(self.directions.RIGHT);
        },

        // Переключение на предыдущий слайд
        showPrevSlide : function() {
            var self = this;
            if (self.slides.length == 1) return;

            self.activeSlide--;
            if (self.activeSlide < 0) self.activeSlide = self.slides.length - 1;

            self.showActiveSlide(self.directions.LEFT);
        },

        // Отображение текущего слайда
        showActiveSlide : function(direction) {
            var self = this;

            if ( ! direction) direction = self.directions.RIGHT;

            self.progress = 0;

            var currentSlide = self.dots.filter('.active').index();

            var currentSlideAnimation = {left: '-100%'};
            var newSlideStyles = {top: 0, left: '100%'};
            var newSlideAnimation = {left: 0};

            if (direction == self.directions.LEFT){
                currentSlideAnimation = {left: '100%'};
                newSlideStyles = {top: 0, left: '-100%'};
                newSlideAnimation = {left: 0};
            }

            if (currentSlide >= 0) self.slides.eq(currentSlide).animate(currentSlideAnimation, self.settings.transitionSpeed);
            self.slides.eq(self.activeSlide).css(newSlideStyles).animate(newSlideAnimation, self.settings.transitionSpeed);

            self.dots.removeClass('active').eq(self.activeSlide).addClass('active');

            self.slider.trigger('slider-change', self.activeSlide);
        },

        // Установка базовых стилей для элементов слайдера
        setElementsStyles : function(){
            var self = this;

            self.slider.css({
                position: 'relative',
                width: self.settings.fullWidth ? '100%' : self.settings.width,
                height: self.settings.height,
                overflow: 'hidden'
            });

            self.slides.css({
                position: 'absolute',
                top: 0,
                left: '100%'
            });

            self.slides.eq(0).css({
                top: 0,
                left: 0
            });
        },

        // Пересчет размеров слайдера
        recountSliderSize : function(){
            var self = this;

            var slideWidth = self.settings.width;
            var slideHeight = self.settings.height;

            if (self.settings.fullWidth) {
                slideWidth = self.slider.width();
                slideHeight = Math.round(slideWidth / self.settings.width * self.settings.height);
            } else if (self.settings.autoSize) {
                slideWidth = self.slider.parent().width();
                slideHeight = self.slider.parent().height();
            }

            self.slides.css({
                width: slideWidth,
                height: slideHeight,
                lineHeight: slideHeight + 'px'
            });

            self.slider.css({
                height: slideHeight
            });

            if (self.settings.autoSize) {
                self.slider.css({
                    width: slideWidth
                });
            }
        },

        // Инициализация точек со статусом слайдера
        initStatusDots : function(){
            var self = this;

            if ( ! self.settings.dotsWrapper) return;

            if ( ! self.settings.customDots) {
                self.settings.dotsWrapper.empty();
                for (var i = 0; i < self.slides.length; i++){
                    self.settings.dotsWrapper.append($('<div>').addClass('wmr-slider-dot'));
                }
            }

            self.dots = self.settings.dotsWrapper.children('*');
            self.dots.eq(0).addClass('active');
        }

    };

    $.fn.wmrSlider = function(settings, value) {
        if (typeof(settings) === 'string') {
            if ($(this).data('wmr-slider')) {
                this.each(function() {
                    switch (settings) {
                        case 'show-slide':
                            $(this).data('wmr-slider').showSlide(value);
                            break;

                        case 'reload':
                            $(this).data('wmr-slider').reload();
                            break;
                    }
                });
            }
        } else {
            this.each(function () {
                var slider = new wmrSlider($(this), settings);
                $(this).data('wmr-slider', slider);
            });
        }

        return this;
    }

})(jQuery);
