const register = {

    init : function() {
        // Отображение дополнительных блоков при выборе чекбоксок
        $('input[data-toggle-block]').on('change checkbox:toggle-block', function(){
            const selector = $(this).data('toggle-block');
            const state = $(this).is(':checked');

            if (state) $(selector).removeClass('hidden').show(); else $(selector).hide();
        }).trigger('checkbox:toggle-block');

        // Отображение поля для своего значения Как вы нас нашли
        $('input[name=how_found]').on('change', function(){
            if ($(this).val() == 'custom') {
                $('.how-found-custom-block').removeClass('hidden').show();
            } else {
                $('.how-found-custom-block').hide();
            }
        }).change();

        // Маска для поля номера телефона
        mask.init();
        //$('input[name=phone]').inputmask('mask', {mask: '+38 (999) 999-99-99', clearMaskOnLostFocus: false});
    },

}

register.init();
