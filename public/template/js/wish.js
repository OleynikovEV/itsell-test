const wishList = {
  classHidden: 'hidden',
  classSelected: 'wish-button__selected',
  element: null,
  elementColor: null,
  productId: null,
  callback: null,

  initElement: function(selector) {
    this.element = document.getElementById(selector);
    if (this.element) {
      this.getProductId();
      this.callback = function() {
        wishList.setText([__('Сообщить, когда появится'), __('Сообщим о наличии')]);
        wishList.changeStyle('wish-button-inform__select', 'wish-button-inform');
      };
      this.element.onclick = this._eventClick;
    }
    return this;
  },

  init: function() {
    $(document).on('click','.wish-button', wishList._eventClick);
    return this;
  },

  _eventClick: function() {
    wishList.element = this;
    wishList.productId = parseInt(this.dataset.productId);
    let result = null;
    let action = null;
    if (wishList.isSelected()) {
      result = wishList._deleteFromStor(wishList.productId);
      action = 'delete';
    } else {
      let comment = wishList.showPrompt(__('Здесь вы можете ввести комментарий к товару'));

      if (comment === null) comment = '';
      result = wishList._addToStor(wishList.productId, comment.slice(0, 255));
      action = 'add';
    }

    result.then( response => {
      if (response.status === 401) {
        app.showPopupMessage(__('Необходимо авторизоваться. ' + '<a href="/login">' + __('Вход') + '</a>' ));
      } else if (response.status === 200) {
        return response.json();
      } else {
        switch (action) {
          case 'add':
            app.showPopupMessage(__('Не удалось добавить товар в лист ожиданий.'));
            break;
          case 'delete':
            app.showPopupMessage(__('Не удалось удалить товар из листа ожиданий.'));
            break;
        }
      }

      return false;
    }).then(data => {

      if (data === false) return;

      switch (action) {
        case 'add':
          wishList.select();
          app.showPopupMessage(__('Товар добавлен в лист ожиданий. ' + '<a href="/profile/wish_list">' + __('Лист ожиданий') + '</a>'));

          // если первый контакт с формой
          let answer = JSON.parse(data);
          if (answer.first_touch === null) {
            console.log('Первый контакт с формой');
            wishList.showModal();
          }
          break;
        case 'delete':
          wishList.unSelect();
          app.showPopupMessage(__('Товар удален из листа ожиданий.'));

          if (wishList.needReload()) {
            document.location.reload();
          }
          break;
      }
    }).finally(() => {
      if (typeof wishList.callback === "function") {
        wishList.callback();
      }
    });
   },

  showModal: function() {
    const modal = document.getElementById('subscribeModal');
    modal.classList.remove('hidden');
  },

  showPrompt: function(msg) {
    return prompt(msg, '');
  },

  _addToStor: async function (productId, comment) {
    return await fetch('/v1/wishlist/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        productId: productId,
        comment: comment
      })
    });
  },

  _deleteFromStor: async function(productId) {
    return await fetch('/v1/wishlist/delete', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        productId: productId
      })
    });
  },

  changeStyle: function(select, unselect) {
    if (wishList.getWish()) {
      wishList.element.classList.remove(unselect);
      wishList.element.classList.add(select);
    } else {
      wishList.element.classList.remove(select);
      wishList.element.classList.add(unselect);
    }
  },

  findElementById(id) {
    this.element = document.querySelector(`[data-product-id="${id}"]`);
    return this.element;
  },

  needReload: function() {
    if (this.element) {
      return this.element.dataset.reload === 'true';
    }

    return false;
  },

  setElement: function(element, elementColor) {
    this.element = element;
    this.elementColor = elementColor;
    if (this.getWish()) {
      this.select();
    } else {
      this.unSelect();
    }
    return this;
  },

  getProductId: function() {
    if (this.element) {
      this.productId = parseInt(this.element.dataset.productId);
      return this.productId;
    }

    return null;
  },

  setText: function(textStatus) {
    if (this.element) {
      this.element.innerHTML = !this.getWish() ? textStatus[0] : textStatus[1];
    }
    return this;
  },

  getWish: function() {
    if (this.elementColor) {
      return this.elementColor.dataset.wish === 'true';
    }
    if (this.element) {
      return this.element.dataset.wish === 'true';
    }

    return false;
  },

  setWish: function(val) {
    if (this.elementColor) {
      this.elementColor.dataset.wish = val;
    }
    if (this.element) {
      this.element.dataset.wish = val;
    }
    return this;
  },

  isSelected: function() {
    if (this.element) {
      return this.element.classList.contains(this.classSelected) || this.element.dataset.wish === 'true';
    }

    return false;
  },

  select: function() {
    if (this.element) {
      this.element.classList.add(this.classSelected);
      this.setWish(true);
    }
    return this;
  },

  unSelect: function() {
    if (this.element) {
      this.element.classList.remove(this.classSelected);
      this.setWish(false);
    }
    return this;
  },

  changeProductId: function(id) {
    if (this.element) {
      this.element.dataset.productId = id;
    }
    return this;
  },

  hidden: function() {
    if (this.element) {
      this.element.classList.add(this.classHidden);
    }
    return this;
  },

  show: function() {
    if (this.element) {
      this.element.classList.remove(this.classHidden);
    }
    return this;
  },

  destroy: function() {
    this.element = null;
    this.elementColor = null;
    this.productId = null;
    return this;
  }
}

wishList.init();