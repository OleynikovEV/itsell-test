const listItems = {
  ids: [],

  init: function() {
    this.eventButtonClick(); // событие для вызуального выбора кнопок
    this.eventSelectAll(); // выделить все позиции
    this.eventSelectSingle(); // выделить отдельные позиции
    this.eventButtonDelete();
  },

  eventButtonClick: function() {
    $('#selectAll, #select').click(function() {
      if (! listItems.__isActive(this) ) {
        $('#selectAll, #select').removeClass('active');
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }
    });
  },
  // выбрать все позиции
  eventSelectSingle: function() {
    $('#select').click(function() {
      listItems.__unselectAll();

      if (listItems.__isActive(this)) {
        $('.multi-column-list li').addClass('cursor_crosshair');
        $('.multi-column-list li a').on('click', listItems.__preventDefault);
        $('.multi-column-list li').on('click', listItems.eventSelectItem);
      } else {
        $('.multi-column-list li').removeClass('cursor_crosshair');
        $('.multi-column-list li a').off('click', listItems.__preventDefault);
        $('.multi-column-list li').off('click', listItems.eventSelectItem);
      }
    });
  },
  // выбирать отдельные позиции
  eventSelectAll: function() {
    $('#selectAll').click(function() {
      if (listItems.__isActive(this)) {
        listItems.__selectAll();
      } else {
        listItems.__unselectAll();
      }
    });
  },
  // событие выбора конкретного товара в списке
  eventSelectItem: function() {
    listItems.__selectItem(this);
  },
  // событие кнопки Удалить
  eventButtonDelete: function() {
    $('#buttonDelete').on('click', listItems.__eventDelete);
  },

  // отключаем событие
  __preventDefault: function(ev) {
    ev.preventDefault();
  },
  // если на событие нажали
  __selectItem: function(selector) {
    const id = $(selector).data('itemId');

    if (listItems.__isActive(selector)) { // снимаем выдиление
      $(selector).removeClass('active');
      const index = listItems.ids.indexOf(id);
      listItems.ids.splice(index, 1);
    } else { // выделяем позицию
      $(selector).addClass('active');
      listItems.ids.push(id);
    }

    // проверка нужно ли активировать кнопку удаление
    if (listItems.ids.length > 0) {
      listItems.__activateButtonDelete();
    } else {
      listItems.__disableButtonDelete();
    }
  },
  // выбрать все эллементы
  __selectAll: function() {
    listItems.ids = [];

    $('.multi-column-list li').each(function() {
      listItems.ids.push( $(this).data('itemId') );
      $(this).addClass('active');
    });

    if (listItems.ids.length > 0) {
      listItems.__activateButtonDelete();
    } else {
      listItems.__disableButtonDelete();
    }
  },
  // снять выделения со всех элементов
  __unselectAll: function() {
    listItems.ids = [];
    listItems.__disableButtonDelete();

    $('.multi-column-list li').each(function() {
      $(this).removeClass('active');
    });
  },
  // проверяем активный элемент или нет
  __isActive: function(selector) {
    return $(selector).hasClass('active');
  },
  // активируем кнопку
  __activateButtonDelete: function() {
    $('#buttonDelete').removeClass('btn-disable');
    $('#buttonDelete').addClass('btn-outline-orange');
  },
  // деактивируем кнопку
  __disableButtonDelete: function() {
    $('#buttonDelete').removeClass('btn-outline-orange');
    $('#buttonDelete').addClass('btn-disable');
  },

  // событие удаления из листа ожиданий
  __eventDelete: function() {

    if (listItems.ids.length === 0) return false;

    $.ajax({
      async: false,
      url: '/v1/wishlist/deleteItems',
      type: 'post',
      dataType: 'json',
      data: {ids: listItems.ids},
      success: function(response) {
        if (response === true) {
          app.showPopupMessage(__('Выбранные позиции удалены из листа ожиданий'));
          document.location.reload();
        }
        console.log(response);
      },
      error: function(error) {
        app.showPopupMessage(__('Не удалось удалить позиции из листа ожиданий'));
        console.error(error);
      }
    });
  }
}