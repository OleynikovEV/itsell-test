const priceModal = {
  element: null,

  init: function(element) {
    this.element = `[data-id=${element}]`;
    return this;
  },

  setPriceRange: function(value) {
    this.changePrice('range', value);
  },
  setPriceOpt: function(value) {
    this.changePrice('opt', value);
  },
  setPriceVip: function(value) {
    this.changePrice('vip', value);
  },
  setPriceDiler: function(value) {
    this.changePrice('diler', value);
  },
  setPriceRrc: function(value) {
    this.changePrice('rrc', value);
  },
  setPriceDrop: function(value) {
    this.changePrice('drop', value);
  },
  setPriceSale: function(value) {
    if (value > 0) {
      this.getElement(this, 'sale').show();
      this.changePrice('sale', `${value}`);
    } else {
      this.getElement(this, 'sale').hide();
    }
  },
  setPriceDefault: function(value, cur = '') {
    this.changePrice('price-default', value, cur);
  },
  setPriceDefaultUah: function(value, cur = '') {
    this.changePrice('price-default-uah', value, cur);
  },

  changePrice: function(name, value, cur = '') {
    if (value === undefined) return;
    const self = document.querySelectorAll(this.element);

    self.forEach(function(item) {
      this.setText(
        this.getElement(item, name),
        value,
        cur
      );
    }, this);
  },
  getElement: function(item, name) {
    return $(item).find(`.${name}`);
  },
  setText: function(item, value, cur) {
    item.html(`${cur} ${value}`);
  }
}