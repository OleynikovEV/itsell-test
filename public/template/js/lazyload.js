'use strict'

document.addEventListener("DOMContentLoaded", function() {
  const lazyLoadElements = document.querySelectorAll(".lazy");

  if ("IntersectionObserver" in window) {
    let imageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let image = entry.target;
          image.src = image.dataset.src;
          image.classList.remove("lazy");
          imageObserver.unobserve(image);
        }
      });
    });

    lazyLoadElements.forEach(function(image) {
      imageObserver.observe(image);
    });
  } else {
   setTimeout(function() {
    lazyLoadElements.forEach(function(img) {
        img.src = img.dataset.src;
        img.classList.remove('lazy');
      });
    }, 1000);
  }
});
