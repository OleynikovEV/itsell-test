var order = {

  init : function() {
    order.initForm();

    order.initDropshippingFields();

    order.initCityInput();
    order.initWarehouseSelect();
    order.initStreetSelect();

    order.initDeliveryTypes();
    order.initPaymentTypes();

    order.initDeliveryPriceCalculator();

    order.dropshipOther();

    // Маска для поля номера телефона
    $('input[name=phone]').inputmask('mask', {mask: '+38 (999) 999-99-99', clearMaskOnLostFocus: false});
    // mask.init();
  },

  initForm : function() {
    let inAction = false;

    $('.order-form-main').on('submit', function(ev){
      ev.preventDefault();

      if (inAction) return;
      inAction = true;

      $('.order-form-wrapper .error-message').remove();

      let data = {};
      $('.order-form-main input').each(function(){
        switch ($(this).attr('type')) {
          case 'checkbox':
            value = $(this).is(':checked') ? 1 : 0;
            break;

          case 'radio':
            value = $('input[name=' + $(this).attr('name') + ']:checked').val();
            break;

          default:
            value = $(this).val();
        }
        data[$(this).attr('name')] = value;
      });

      data['price-type'] = $('#price-type').val();

      products = [];
      hydrogelPlenca = false;
      $('.cart-product-block').each(function( index, item) {
        const price = Number.parseFloat( $(item).data('sort-price') );
        const from = Number.parseInt( $(item).data('from') );

        if (price === 0) {
          products.push({
            id: $(item).data('row-id'),
            price: price,
            from: from
          });
        }

        if (from === 2 && price === 0) {
          hydrogelPlenca = true;
        }
      });

      if (products.length > 0) {
        let msg = order.__hydrogelMessage(hydrogelPlenca, $('#price-type option:selected').text());
        let question = __('Обратите внимание, в заказе есть товары с нулевой ценой.');
        question += '\n' + __('Такие товары не попадут в заказ.');
        question += '\n' + __('Вы можете обратиться к менеджеру для решения этого вопроса.');

        if (msg !== null) {
          question += '\n';
          question += '\n' + msg;
          question += '\n';
        }

        question += '\n' + __('Продолжить оформление заказа?');

        if (! confirm(question) ) {
          inAction = false;
          $('#preload').addClass('hidden');
          return;
        }
      }

      $.ajax({
        url: '/order',
        type: 'post',
        data: data,
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            document.location.href = data.redirect_url;
          } else {
            $('.order-form-wrapper').prepend($('<div>').addClass('error-message').html(data.messages.join('<br>')));

            const formStart = $('.order-form-wrapper').offset().top + (app.layout == 'mobile' ? -30 : -15);
            if ($(window).scrollTop() > formStart) $('body, html').animate({scrollTop: formStart}, 300);

            $('#preload').addClass('hidden');
          }

          inAction = false;
        }
      })
    });

    $('.cart-go-to-order').on('click', function(ev){
      ev.preventDefault();

      var formStart = $('.order-form-wrapper').offset().top;
      $('body, html').animate({scrollTop: formStart}, 300);
    });

    $('.cart-submit-order-form').on('click', function(ev){
      ev.preventDefault();

      $('#preload').removeClass('hidden');
      $('.order-form-main').submit();
    });
  },

  __hydrogelMessage: function(data, priceType) {
    if (data === true) return __('Защитная Гидрогелевая Пленка SKLO не продается по типу цен ') + priceType + '.';

    return null;
  },

  initDropshippingFields : function() {
    $('input[name=dropshipping]').on('change', function(){

      order.dropshipShow();
      order.dropshipOther();

      if ($(this).is(':checked')) {
        $('.order-form-main input, .order-form-main select').each(function(){
          var value = $(this).val();

          if ($(this).attr('name') == 'phone') value = value.replace(/[^0-9+]/g, '');

          if ($(this).data('preset') && $(this).data('preset') == value) {
            $(this).val('').change();

            if ($(this).attr('name') == 'city_id') {
              $(this).closest('.custom-select').find('.custom-select-active-value').html(__('Выберите город'));
            }

            if ($(this).attr('name') == 'delivery_type_id') {
              $('.delivery-type-btn').removeClass('active').eq(0).click();
            }
          }
        });

        if ( ! $('.delivery-type-btn.active').is(':visible')) $('.delivery-type-btn').eq(0).click();
      }
    });

    $('input[name=dropshipping_price]').on('change', function(){
      $('.order-delivery-price').trigger('delivery-price:calculate');
    });
  },

  initDeliveryTypes : function() {
    // Кнопки для переключения активного способа доставки
    $('.delivery-type-btn').on('click', function(ev){
      ev.preventDefault();

      if ( ! $(this).is('.active')) {
        $('input[name=delivery_type_id]').val($(this).data('value')).change();

        $('.delivery-type-btn').removeClass('active');
        $(this).addClass('active');
      }
      order.dropshipShow();
      order.dropshipOther();
    });

    // Переключение блоков с деталями доставки в зависимости от выбранного способа
    $('input[name=delivery_type_id]').on('change', function(){
      var deliveryTypeId = $(this).val();

      $('.order-delivery-details').each(function(){
        var blockDeliveryTypes = $(this).data('delivery-types').toString().split(',');

        if (blockDeliveryTypes.indexOf(deliveryTypeId) !== -1) {
          $(this).show().trigger('delivery:update-data');
        } else {
          $(this).hide();
        }
      });

      $('.order-delivery-price').trigger('delivery-price:calculate');
    });

    $('input[name=delivery_type_id]').change();
  },

  initPaymentTypes : function() {
    $('input[name=payment_type_id]').on('change', function(){
      var paymentTypeId = $('input[name=payment_type_id]:checked').val();
      if (paymentTypeId == 1) {
        $('.order-dropshipping-cod-fields').show();

        $('input[name=package_payer]').each(function(){
          $(this).removeAttr('disabled');
          $(this).closest('label').removeClass('disabled')
        });
      } else {
        $('.order-dropshipping-cod-fields').hide();

        $('input[name=package_payer]').first().click();
        $('input[name=package_payer]').each(function(){
          $(this).attr('disabled', true);
          $(this).closest('label').addClass('disabled')
        });
      }

      $('.order-delivery-price').trigger('delivery-price:calculate');
    });
  },

  initCityInput : function() {
    // Изменение активного города
    $('input[name=city_id]').on('change', function(){
      $('.order-delivery-details:visible').trigger('delivery:update-data')
      $('.order-delivery-price').trigger('delivery-price:calculate');
    });

    // Пресеты для быстрого выбора городов
    $('.order-city-presets a').on('click', function(ev){
      ev.preventDefault();

      var cityName = $(this).text();
      $('.city-select .custom-select-list > div').each(function(){
        if ($(this).text() == cityName) {
          $(this).click();
          $('.city-select').click();
        }
      });
    });
  },

  initWarehouseSelect : function() {
    var warehousesCityId = 0;
    var warehousesDeliveryTypeId = 0;

    $('.warehouse-select').on('delivery:update-data', function(){
      var cityId = $('input[name=city_id]').val();
      var deliveryTypeId = $('input[name=delivery_type_id]').val();

      if (cityId && (warehousesCityId != cityId || warehousesDeliveryTypeId != deliveryTypeId)) {
        warehousesCityId = cityId;
        warehousesDeliveryTypeId = deliveryTypeId;

        $.ajax({
          url: 'order/warehouses',
          data: {city_id: cityId, delivery_type_id: deliveryTypeId},
          dataType: 'json',
          success: function(data) {
            if (data.success) {
              var selectBlock = $('.warehouse-select');
              var selectList = selectBlock.find('.custom-select-list');
              selectList.children(':not(.custom-select-search)').remove();

              var lastValue = selectBlock.find('input').val();
              var lastValueOption = null;

              for (var i = 0; i < data.response.length; i++) {
                var option = $('<div>').data('value', data.response[i].id).html(data.response[i].name);
                if (data.response[i].id == lastValue) lastValueOption = option;

                selectList.append(option);
              }

              if ( ! lastValueOption) {
                selectBlock.find('.custom-select-active-value').html(__('Выберите отделение'));
                selectBlock.find('input').val('');
              } else {
                selectBlock.find('.custom-select-active-value').html(lastValueOption.text());
              }
            }
          }
        })
      }
      else if ( ! cityId) {
        var selectBlock = $('.warehouse-select');
        var selectList = selectBlock.find('.custom-select-list');
        selectList.children(':not(.custom-select-search)').remove();
        selectBlock.find('.custom-select-active-value').html(__('Выберите отделение'));

        warehousesCityId = 0;
      }
    });
  },

  initStreetSelect : function() {
    var streetsCityId = 0;

    $('.street-select').on('delivery:update-data', function(){
      var cityId = $('input[name=city_id]').val();
      if (cityId && streetsCityId != cityId) {
        streetsCityId = cityId;

        $.ajax({
          url: 'order/streets',
          data: {city_id: cityId},
          dataType: 'json',
          success: function(data) {
            if (data.success) {
              var selectBlock = $('.street-select');
              var selectList = selectBlock.find('.custom-select-list');
              selectList.children(':not(.custom-select-search)').remove();

              for (var i = 0; i < data.response.length; i++) {
                selectList.append($('<div>').data('value', data.response[i].id).html(data.response[i].name));
              }

              selectBlock.find('.custom-select-active-value').html(__('Выберите улицу'));
              selectBlock.find('input').val('');
            }
          }
        })
      }
    });
  },

  initDeliveryPriceCalculator : function() {
    var deliveryPriceCityId = 0;
    var deliveryPriceDeliveryTypeId = 0;
    var deliveryPricePaymentTypeId = 0;
    var deliveryPriceRedeliverySumm = 0;

    $('.order-delivery-price').on('delivery-price:calculate', function(){
      var cityId = $('input[name=city_id]').val() || 0;
      var deliveryTypeId = $('input[name=delivery_type_id]').val() || 0;
      var paymentTypeId = $('input[name=payment_type_id]:checked').val() || 0;
      var redeliverySumm = $('input[name=dropshipping_price]').val() || 0;

      if (cityId != deliveryPriceCityId || deliveryTypeId != deliveryPriceDeliveryTypeId
        || paymentTypeId != deliveryPricePaymentTypeId || redeliverySumm != deliveryPriceRedeliverySumm) {

        deliveryPriceCityId = cityId;
        deliveryPriceDeliveryTypeId = deliveryTypeId;
        deliveryPricePaymentTypeId = paymentTypeId;
        deliveryPriceRedeliverySumm = redeliverySumm;

        if (cityId && deliveryTypeId <= 2) {
          $.ajax({
            url: '/order/delivery-price/' + cityId + '/' + deliveryTypeId + '/' + (paymentTypeId == 1
              ? redeliverySumm : 0),
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                $('.order-delivery-price').removeClass('hidden').find('span').html(data.response + ' грн');
              } else {
                $('.order-delivery-price').addClass('hidden');
              }
            }
          })
        } else $(this).addClass('hidden');
      }
    });
  },

  // проверка выбран дропшипинг или нет
  dropshipShow: function() {
    const active =  $('input[name=dropshipping]').is(':checked');

    $('.order-dropshipping-field').each(function(){
      if (active) {
        if ($(this).data('show') == 1) $(this).removeClass('hidden').show(); else $(this).hide();
      } else {
        if ($(this).data('show') == 1) $(this).hide(); else $(this).show();
      }
    });
  },

  dropshipOther: function() {
    const active =  $('input[name=dropshipping]').is(':checked');
    const deliveryType = $('#delivery-type-other').hasClass('active');

    if (active && deliveryType) {
      $('input[name="city_id"]').val(0);
      $('.order-dropshipping-field').each(function(){
        $(this).hide();
      });
      $('.city-select').hide();
      $('.order-city-presets').hide();
      $('.address').hide();
      $('input[name="comment"]').hide();
    } else {
      $('.city-select').show();
      $('.order-city-presets').show();
      $('input[name="comment"]').show();
    }
  }
}

order.init();
