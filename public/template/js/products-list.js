var productsList = {
  init : function() {
    this.initFilters();
    this.initFilters2();

    this.initPagination();
    // this.initLoadMore();

    this.initSortSelect();
    this.initPerPageSelect();
    this.initDisplayType();

    this.groupBySeries();
    this.selectProductColor();
  },

  groupBySeries: function() {
    $('#groupBySeries').click(function() {
      if( $(this).is(':checked') === true) {
        Cookies.set('groupBySeries', true);
      } else {
        Cookies.set('groupBySeries', false)
      }

      document.location.href = paginationUrl;
    });
  },

  initFilters : function() {
    var priceRange = $('.price-range-slider');
    var priceMinInput = $('.price-range-block').find('.from input');
    var priceMaxInput = $('.price-range-block').find('.to input');

    priceMinInput.data('previous', priceMinInput.val());
    priceMaxInput.data('previous', priceMaxInput.val());

    // применение фильтров
    $('.filter-values-list input[type=checkbox], .filter-color-block input[type=checkbox]').on('change', function(){
      productsList.applyFilters(this);
    });

    $('.filter-color-chackbox').click(function() {
      const checkbox = $(this).find('input[type=checkbox]');
      $(checkbox).attr('checked', !$(checkbox).prop('checked'));
      $(this).find('input[type=checkbox]').trigger('change');
    });

    // Фильтрация по цене
    priceRange.slider({
      range: true,
      min: priceRange.data('min'),
      max: priceRange.data('max'),
      step: 0.01,
      values: [priceMinInput.val(), priceMaxInput.val()],
      slide: function(event, ui){
        priceMinInput.val(ui.values[0]);
        priceMaxInput.val(ui.values[1]);
      },
      change: function() {
        priceMinInput.change();
      }
    });

    $('.price-range-block input').on('change', function(){
      var priceMin = parseFloat(priceRange.data('min'));
      var priceMax = parseFloat(priceRange.data('max'));

      var priceFrom = parseFloat(priceMinInput.val());
      var priceFromPrev = parseFloat(priceMinInput.data('previous'));

      if (priceFrom != priceFromPrev) {
        if (Number.isNaN(priceFrom)) priceMinInput.val(priceMin);
        if (priceFrom < priceMin) priceMinInput.val(priceMin);
        if (priceFrom > priceMax) priceMinInput.val(priceMax);
      }

      var priceTo = parseFloat(priceMaxInput.val());
      var priceToPrev = parseFloat(priceMaxInput.data('previous'));

      if (priceTo != priceToPrev) {
        if (Number.isNaN(priceTo)) priceMaxInput.val(priceMax);
        if (priceTo < priceMin) priceMaxInput.val(priceMin);
        if (priceTo > priceMax) priceMaxInput.val(priceMax);
      }

      if (parseFloat(priceMinInput.val()) != priceFromPrev || parseFloat(priceMaxInput.val()) != priceToPrev) {
        productsList.applyFilters();
      }
    });

    // Сброс фильтров
    $('.filter-reset').on('click', function(ev){
      ev.preventDefault();
      const self = $(this);

      switch (self.data('filter')) {
        case 'gadget':
          let url = document.location.href;
          const filter = self.data('value'); // родительский фильтр или слово поиска
          if (url.indexOf('/search/') !== -1) { // если это поиски, сбрасываем все до фразы поиска
            const index = url.indexOf(filter);
            document.location.href = url.substring(0, index-1);
          } else if (self.data('is-series') == true) {
            document.location.href = url.substring(0, url.indexOf(filter));
          } else { // если категории, нужно сбросить на главную страницу
            document.location.href = window.location.origin;
          }
          break;
        case 'color':
          const parentId = self.data('colorid');
          const inputChildrenColor = $(`input[data-parent='${parentId}'`);
          $(`input[data-id=${parentId}]`).prop('checked', false);
          inputChildrenColor.prop('checked', false);
          productsList.applyFilters();
          break;
        case 'price_from':
          priceMinInput.val(priceRange.data('min')).change();
          break;

        case 'price_to':
          priceMaxInput.val(priceRange.data('max')).change();
          break;

        default:
          let value = self.data('value');

          // Снятие выделения с текущего фильтра
          const id = self.data('id');

          if ($(`#${id}`).length) {
            $(`#${id}`).click();
          } else {
            const input = $('input[name=' + self.data('filter') + '][value*=' + self.data('value') + ']');
            if (input.length === 0) {
              productsList.applyFilters();
            } else {
              input.click();
            }
          }
      }
    });

    $('.filter-reset-all').on('click', function(ev){
      ev.preventDefault();
      document.location.href = productsListBaseUrl;
    });

    $('.filter-values-list input:checked').each(function(){
      $(this).parent().addClass('active');
    });

    // Разворачивание списка всех моделей
    if ($('.filter-subcategories label.hidden').length) $('.filter-subcategories-show-all').removeClass('hidden');

    $('.filter-subcategories-show-all').on('click', function(ev){
      ev.preventDefault();

      $(this).toggleClass('active').text($(this).hasClass('active') ? __('Скрыть модели') : __('Все модели'))

      $('.filter-subcategory-hidden').toggleClass('hidden');
      productsList._updateFiltersColumns(true);
    })

    // Равномерное распределение горизонтальных блоков фильтров по колонкам
    productsList._updateFiltersColumns();
    $(window).resize(function(){productsList._updateFiltersColumns()});

    // Открытие слайдера с фильтрами на мобильных
    $('.mobile-btn-filter, .mobile-filters-counter').click(function(){
      $('.popup-bg').css({top: '0'}).fadeIn();
      $('.mobile-btn-close').show();
      $('.category-filters-block').addClass('active').animate({right: '0'}, 300);
    });

    $('.mobile-btn-close').click(function(){
      $('.popup-bg').fadeOut();
      $('.mobile-btn-close').hide();
      $('.category-filters-block').removeClass('active').animate({right: '-100%'}, 300);
    });

    // Разворачивание списка фильтров при большом количестве значений
    $('.filter-values-show-all').on('click', function(ev){
      ev.preventDefault();

      $(this).closest('.filter-color-block').find('.filter-value-hidden').toggleClass('hidden-mob-only');
      $(this).closest('.filter-values-list').find('.filter-value-hidden').toggleClass('hidden');

      $(this).toggleClass('active');
      $(this).text($(this).is('.active') ? __('Свернуть') : __('Показать все'));
    }).each(function(){
      if ($(this).closest('.filter-values-list').find('.filter-value-hidden input:checked').length) $(this).click();
    });
  },

  // если есть выбранный дочерний цвет, данный цвет делаем активным у товаров
  selectProductColor: function() {
    if (activeFilters.hasOwnProperty('color-child')) {
      if (activeFilters['color-child'].length === 1) {
        const activeColor = activeFilters['color-child'][0];
        const colorsList = $('body').find(`.product-colors a[data-color='${activeColor}']`).not('.active');
        colorsList.trigger('click');
      }
    } else if (activeFilters.hasOwnProperty('color') && activeFilters.color.length === 1) {
      const activeColor = activeFilters.color[0];
      $('body').find(`a[data-color='${activeColor}']`).not('.active').trigger('click');
    }
  },

  // связываем скрытые фильтры и фильтры на странице
  // вывели два фильтра на главную - тут они синхронизируются со скрытыми
  initFilters2: function() {
    $('.filter-values-list-2 input').on('change', function() {
      const isChecked = $(this).is(':checked');
      const value = $(this).val();
      const elm = $(`.filter-values-list input[value='${value}']`).prop('checked', isChecked).change();
    });
  },

  applyFilters : function(self) {
    var selectedFilters = {};
    const parentFilter = $(self).closest('.filter-block').data('parent-filters-values');
    const isParentFilter = parentFilter !== undefined && parentFilter.length === 0;
    const isChecked = $(self).is(':checked');

    // если сброшен родительский фильтр, сбрасываем и дочерние фильтры
    if (isParentFilter === true && isChecked === false) {
      // Снятие значений во всех дочерних фильтрах
      const value = $(self).val();
      productsList._clearChildrenFilter(value.toString());
    }

    $('.filter-values-list input:checked, .filter-color-block input:checked').each(function(){
      var filterName = $(this).attr('name');

      if ( ! selectedFilters[filterName]) {
        selectedFilters[filterName] = [];
      }
      if (selectedFilters[filterName].indexOf($(this).val()) === -1) {
        selectedFilters[filterName].push($(this).val());
      }
    });

    var priceMin = $('.price-range-block').find('.from input').val();
    var priceMax = $('.price-range-block').find('.to input').val();

    if (priceMin != $('.price-range-slider').data('min')) selectedFilters['price_from'] = [priceMin];
    if (priceMax != $('.price-range-slider').data('max')) selectedFilters['price_to'] = [priceMax];

    var result = [];

    // Если задан список уже выбранных ранее фильтров - сохраняем их последовательность
    if (activeFilters) {
      for (var activeFilter in activeFilters) {
        items = (activeFilter == 'price_from' || activeFilter == 'price_to' ? ['price_from', 'price_to'] : [activeFilter]);
        for (var i in items) {
          if (selectedFilters[items[i]]) {
            result.push(items[i] + '=' + selectedFilters[items[i]].join(','));
            delete selectedFilters[items[i]];
          }
        }
      }
    }

    for (var selectedFilter in selectedFilters) result.push(selectedFilter + '=' + selectedFilters[selectedFilter].join(','));

    document.location.href = productsListBaseUrl + (result.length ? '/filter/' + result.join(';') : '');
  },

  // Снятие значений во всех дочерних фильтрах
  _clearChildrenFilter: function(value) {
    $('.filter-block').each(function(){
      let parentValues = $(this).data('parent-filters-values');
      if ( !parentValues) return;

      const parentValuesArray = (parentValues + '').split(',');
      parentValues = parentValues.toString();

      if (parentValuesArray.length > 0 && (parentValuesArray.indexOf(value) !== -1 || parentValues.indexOf(value) !== -1)) {
        $(this).find('input[type=checkbox]').prop('checked', null);
      }
    });
  },

  _updateFiltersColumns : function(forceUpdate) {
    // Определение количества колонок на текущем разрешении
    var totalColumns = 6;
    var totalColumnsResolutions = {
      515: 2,
      830: 3,
      1020: 4,
      1320: 5,
      1791: 6
    };

    var windowWidth = $(window).width();
    for (var resolution in totalColumnsResolutions) {
      if (windowWidth < resolution) {
        totalColumns = totalColumnsResolutions[resolution];
        break;
      }
    }

    $('.filter-device-content').each(function(){
      if ( ! forceUpdate && $(this).data('columns') == totalColumns) return;

      var elements = $(this).find('.left-block > *');

      var totalElements = elements.filter(':visible').length;

      var perColumn = Math.ceil(totalElements / totalColumns);
      var columnsWithMoreElements = totalElements % totalColumns;

      var content = $('<div>');
      var processedElements = 0;

      for (var i = 0; i < totalColumns; i++) {
        var elementsInColumn = 0;

        var column = $('<div>').addClass('left-block');
        content.append(column);

        if (columnsWithMoreElements && columnsWithMoreElements == i) perColumn--;

        while (elementsInColumn < perColumn) {
          var element = elements[processedElements];
          if ( ! element) break;

          if ($(element).is(':visible')) elementsInColumn++;

          column.append(element);
          processedElements++;

          if (elementsInColumn == perColumn && $(element).is('.filter-device-sector')) {
            column.append(elements[processedElements++]);
          }
        }
      }

      // Если в конце списка остались скрытые элементы, которые не были распределены по колонкам - добавляем
      // их в последнюю колонку
      if (processedElements < elements.length) {
        for (; processedElements < elements.length; processedElements++) {
          column.append(elements[processedElements]);
        }
      }

      $(this).html(content.find('.left-block')).data('columns', totalColumns);
    });
  },

  initPagination : function() {
    var links = $('.pagination .page-item');
    links.first().addClass('previous');
    links.last().addClass('next');

    $('.pagination .page-item.disabled:not(.previous):not(.next)').addClass('pagination-spacer').html('. . . . . .');
  },

  initSortSelect : function() {
    $('.products-list-sort').on('select:change', function(){
      var sortType = $(this).data('value');
      if (sortType != 'default') Cookies.set('products_sort', sortType); else Cookies.remove('products_sort');

      document.location.href = paginationUrl;
    });
  },

  initPerPageSelect : function() {
    $('.products-list-per-page').on('select:change', function(){
      Cookies.set('products_per_page', $(this).data('value'));
      document.location.href = productsListBaseUrl;
    });
  },

  initDisplayType : function() {
    $('.product-display-type').on('click', function(ev){
      ev.preventDefault();

      if ($(this).is('.product-display-line')) {
        $('.category-products-block').addClass('display-line');
        Cookies.set('products_display_type', 'list');
      } else {
        $('.category-products-block').removeClass('display-line');
        Cookies.remove('products_display_type');
      }

      $('.product-display-type').removeClass('active');
      $(this).addClass('active');
    });
  },
}

productsList.init();
