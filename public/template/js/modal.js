const modal = {
  init: function() {
    this.eventClose();
  },
  show: function(selector) {
    $(selector).addClass('modal-show');
  },
  hide: function(selector) {
    $(selector).removeClass('modal-show');
  },
  eventClose: function() {
    const self = this;
    $('.close-modal').click(function() {
      self.hide($(this).closest('.modal'));
    });
  }
}

modal.init();