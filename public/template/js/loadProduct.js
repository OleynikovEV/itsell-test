const loadProduct = {
  init: function() {
    $(function() {
      loadProduct.eventLoad();
    });
  },

  eventLoad: function() {
    $('#btn-load-more-products').click(function(ev) {
      ev.preventDefault();

      const loadMoreButton = $('#btn-load-more-products');
      const paginationUrl = loadMoreButton.attr('data-url');
      let currentPage = loadMoreButton.attr('data-page')*1;

      if ($(this).is('.loading')) return;

      $(this).addClass('loading').hide().before($('<img>')
             .addClass('load-more-icon').attr('src', '/template/images/ajax-loader.gif'));

      let url = '';
      if (window.location.protocol === 'https:' && paginationUrl.indexOf('https') === -1) {
        url = paginationUrl.replace(/http/gi, 'https') + '/' + (currentPage + 1);
      } else if (window.location.protocol === 'http:' && paginationUrl.indexOf('https') !== -1) {
        url = paginationUrl.replace(/https/gi, 'http') + '/' + (currentPage + 1);
      } else {
        url = paginationUrl + '/' + (currentPage + 1);
      }

      loadProduct.ajaxLoad(url).then((result) => {
        if (result !== false && result.success && result.html.length) {
          $('.category-content-block').append(result.html);
          $('.tooltip-event').tooltip({});

          currentPage++;
          loadMoreButton.attr('data-page', currentPage);

          if (result.to !== result.total) {
            $('.pagination .page-item').each(function(){
              if ($(this).find('a').text() == result.current_page) $(this).addClass('active');
            });

            const lastLink = $('.pagination .page-item').last();
            if (lastLink && lastLink.find('a').attr('rel') === 'next') {
              lastLink.find('a').attr('href', result.path + '/' + (result.current_page + 1));
            }

            const productsLeft = result.total - result.to;
            loadMoreButton.html(__('Показать еще') + ' ' + Math.min(productsLeft, productsListPerPage)
              + ' ' + __('из') + ' ' + productsLeft);

            $('.pagination-block .load-more-icon').remove();
            loadMoreButton.removeClass('loading').show();
          } else {
            $('.pagination-block .load-more-icon').remove();
            $('.load-more-products, .pagination').remove();
          }
          productsList.selectProductColor();
        } else {
          console.error(result);
        }
      })
    });
  },

  ajaxLoad: async function(url) {
    try {
      return await $.ajax({
        url: url,
        type: 'get',
        async: true,
        dataType: 'json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          type: 'html'
        }
      });
    } catch (error) {
      console.error(error);
      alert(error);
    }
  }
};

loadProduct.init();