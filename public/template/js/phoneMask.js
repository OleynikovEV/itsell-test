const mask = {
  init: function(selector, value) {
    // Маска для поля номера телефона
    if (selector === undefined) {
      selector = 'input[name=phone]';
    }

    $(selector).inputmask('mask', {mask: [
      '+38 (999)-999-99-99',
      '+375 99-999-99-99',
      '+7 99-999-99-999',
      '+373 99-99-99-99',
      ],
      clearMaskOnLostFocus: false,
      keepStatic: false
    });

    if (value !== undefined) {
      $(selector).val(value);
    }
  },

  changeMask: function() {

  }
}