<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'MainPageController@index')->name('main_page');
Route::get('/price_list', 'MainPageController@priceList')->name('price_list');

// Корзина
Route::apiResource('cart', 'CartController')->names(['index' => 'cart']);
Route::post('cart/cover/add', 'CartController@storeCover')->name('cart_add_cover');
Route::post('cart/updatePromocode', 'CartController@updatePromocode');
Route::post('cart/changeColor', 'CartController@changeColor');
Route::post('cart/changeMaterial', 'CartController@updateMaterial');
Route::post('cart/changeBorderColor', 'CartController@setFullBorder');
Route::get('cart-empty', 'CartController@cartEmpty')->middleware('auth')->name('cart.empty');

// Оформление заказа
Route::post('order', 'OrdersController@store');
Route::get('order/confirm', 'OrdersController@success')->name('order_success');

Route::get('order/warehouses', 'OrdersController@warehouses');
Route::get('order/streets', 'OrdersController@streets');
Route::get('order/delivery-price/{cityId}/{deliveryTypeId}/{paymentAmount}', 'OrdersController@deliveryPrice');

// Пользователи
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Password reset routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

// просмотр личного кабинета для тестов
//Route::group(['middleware' => 'auth'], function()
//{
//    Route::get('profile/profile/{id}', 'ProfileController@profile');
//    Route::get('profile/orders/{page}/{id?}', 'ProfileController@orders')->where('page', '[0-9]+');
//});

// Личный кабинет
Route::group(['middleware' => 'auth'], function()
{
    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::post('profile', 'ProfileController@update')->name('profile.update');

    Route::get('profile/orders', 'ProfileController@orders')->name('profile_orders');
    Route::get('profile/wish_list/{pare?}', 'ProfileController@wishList')->name('wish_list');
    Route::get('profile/price', 'ProfileController@price')->name('profile_price');
    Route::get('profile/recently_viewed', 'ProfileController@recentlyViewed')->name('recently_viewed');
    Route::get('profile/orders/{page}', 'ProfileController@orders')->where('page', '[0-9]+');
    Route::get('profile/orders/export', 'ProfileController@export')->name('export_orders');

    /** прайс генератор */
    Route::prefix('price-generator')->group(function() {
      Route::get('/', 'PriceGenerator@index')->name('price-generator');
      Route::get('generate/{slug}/{format?}', 'PriceGenerator@priceGenerator')->name('price-generator.generate');
      Route::get('remove/{id}', 'PriceGenerator@remove')->name('price-generator.remove');
      Route::post('create', 'PriceGenerator@create')->name('price-generator.create');
      Route::get('update/{id}', 'PriceGenerator@update')->name('price-generator.update');
      Route::post('products-list', 'PriceGenerator@productsList')->name('price-generator.products-list');
      Route::get('iprint/all/download', 'PriceGenerator@iPrintDownload')->name('price-generator.iprint-all-download');
      Route::get('test', 'PriceGenerator@test');
    });

    /** обновление цветов товара */
  Route::get('/product/color/update/{id}', 'ProductColorUpdateController@update')->name('product-color.update');
});

// ссылка для скачивания прайса
Route::get('price-generator/price/download/{slug}/{userId}/', 'PriceGenerator@downloadPrice')->name('profile_price.download');

// Товары
Route::get('products/{url}/{id}', 'ProductsController@show')->name('product');

// Серии товаров
Route::get('series/{url}/{params?}', 'SeriesController@show')->where(['params' => '(.*)'])->name('products_series');
//Route::get('by_series/{url}/{params?}', 'SeriesController@series')->where(['params' => '(.*)'])->name('series');
Route::get('by_group/{url}/{params?}', 'SeriesController@group')->where(['params' => '(.*)'])->name('group');

// Поиск
Route::get('search/category/{category_url}/{phrase}/{params?}', 'SearchController@category')->where(['params' => '(.*)'])->name('search_category');
Route::get('search/{phrase}/{params?}', 'SearchController@index')->where(['params' => '(.*)'])->name('search');

// Бренды
Route::get('brands', 'BrandsController@index')->name('brands');
Route::get('brands/{url}/{params?}', 'BrandsController@show')->where(['params' => '(.*)'])->name('brand');

// Список категорий общих аксессуаров
Route::get('others', 'CategoriesController@accessories')->name('accessories');

// Секции товаров
Route::get('{section}/{params?}', 'SectionsController@index')->where(['section' => '(novelties|sale|soon|coming)', 'params' => '(.*)'])->name('section');

// Категории
Route::post('api/filters', 'CategoriesNewController@filters');
Route::get('new/{section}/{url}/{params?}', 'CategoriesNewController@show')->where(['section' => '(phones|tablets|others)', 'params' => '(.*)'])->name('new_category');
Route::get('{section}/{url}/{params?}', 'CategoriesController@show')->where(['section' => '(phones|tablets|others)', 'params' => '(.*)'])->name('category');

// Статические страницы
Route::get('pages/new/{num?}', 'PagesController@functionality')->name('static_page.new');
Route::get('pages/new/functionality/{num}', 'PagesController@page')->name('static_page.new.page');
Route::get('pages/{url}', 'PagesController@show')->name('static_page');

// список дочерних цветов
Route::post('get-product-colors', 'ProductsController@getColors')->name('product.get-color');

// Обратная связь
Route::post('feedback', 'FeedbackController@store');
Route::post('feedback/found-cheaper', 'FeedbackController@foundCheaper');
Route::post('feedback/sendMessage', 'FeedbackController@sendMessage');

// Обмен с 1С
Route::any('onec/exchange', 'OnecExchangeController@index');

// Конструктор своих принтов
Route::get('printcover', 'PrintCoverController@cover')->name('printcover');

Route::prefix('v1')->group(function() {
  Route::get('/cities', 'Api@getCities');
});

// Ответ от производства Вчехле
Route::post('/FGRldi5pdHNlbGxvcHQuY29t/vchehle/webhook', 'WebhookController@index')->name('vchehle-webhook');

Route::prefix('v1')->group(function() {
  Route::post('wishlist/subscribe', 'Api@subscribe')->name('wishlist.subscribe');
  Route::post('wishlist/add', 'Api@addToWishllist')->name('wishlist.add');
  Route::post('wishlist/delete', 'Api@deleteFromWishllist')->name('wishlist.delete');
  Route::post('wishlist/update', 'Api@updateCommentInWishList')->name('wishlist.update');
  Route::post('wishlist/deleteItems', 'Api@deleteItemsFromWishList')->name('wishlist.delete-items');
});

Route::get('/unsubscribe/{userId}/{messenger}', 'Notification@unsubscribe')->name('unsubscribe');

Route::post('/BQAwgZcxCzAJBgNVBAYTAlVB/webhook', 'TelegramController@webhook');
Route::post('/ZXNhMQ8wDQYDVQQKDAZJVHNl/vb-webhook', 'ViberController@webhook');

//Route::get('/test', 'TestController@index');