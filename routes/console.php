<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

/** Ручной импорт прайса Вчехле */
Artisan::command('price', function() {
  $path = 'https://prints.vchehle.ua/storage/price_lists/prints.yml';
//  $path = 'C:\Users\eugen\Desktop\prints.xml';
  $import = new \App\Http\Models\ImportPrintPrice($path, true);
  $import->update();
});

/**
 * Создание миниатюр для товара если нету
 */
Artisan::command('thumb {idProduct?}', function(int $idProduct = null) {

    if ($idProduct == null) {
        $products = \App\Models\Product::where('active', 1)
                                        ->where(function($query) {
                                            $query->where('qty', '>', 0)
                                                  ->orWhere('added_time', '>', 0);
                                        })->get();
    } else {
        $products = [
            \App\Models\Product::find($idProduct)
        ];
    }

    if (count($products) == 0) {
        echo 'No products ' . ' error' . PHP_EOL;
        return;
    }

    foreach ($products as $product) {
        $images = $product->getImages(true); // получаем масив картинок

        if ($images->count() > 0) {
            $content_server = config('custom.content_server');
            $thumbSize = \App\Models\Product::IMAGE_THUMB_SECONDARY; // размер файла
            $storage = Storage::disk('content'); // хранилище файлов
            $exchangeDir = 'app/'; // папка для временного хранения миниатюр

            foreach ($images as $image) {
                try {
                    fopen($content_server . $image['image_thumb'], 'r'); // проверяем есть ли миниатюра на сервере
                    echo $image['image_thumb'] . ' - ' . ' loaded' . PHP_EOL;
                } catch (Exception $e) {
                    echo $image['image_thumb'] . ' - ' . ' error' . PHP_EOL;

                    try {
                        fopen($content_server . $image['image'], 'r'); // проверяем есть ли большое фото
                        // создаем файл с именем _thumb
                        if (File::copy($content_server . $image['image'], storage_path($exchangeDir . $image['image_thumb']))) {
                            echo $image['image_thumb'] . ' - ' . ' save' . PHP_EOL;
                        }

                        // сжимаем картинку
                        if (Image::make(storage_path($exchangeDir . $image['image_thumb']))
                            ->resize($thumbSize, $thumbSize, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            })->save()) {
                            echo $image['image_thumb'] . ' - resize' . ' ' . PHP_EOL;

                            // записываем в хранилище
                            if ($storage->put($image['image_thumb'], File::get(storage_path($exchangeDir . $image['image_thumb'])))) {
                                File::delete(storage_path($exchangeDir . $image['image_thumb'])); // удаляем временную миниатюру
                                echo $image['image_thumb'] . ' - put to storage' . ' ' . PHP_EOL;
                            }
                        }
                    } catch (Exception $ex) {
                        echo $image['image'] . ' - ' . ' does not exist' . PHP_EOL;
                    }
                }
            }
        }
    }
});

Artisan::command('availability {productsId*}', function(array $productsId) {
  $run = new \App\Http\Models\Availability($productsId);
  $run->notify();
});
