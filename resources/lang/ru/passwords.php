<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть не менее 8 символов и соответствовать подтверждению',
    'reset' => 'Ваш пароль успешно изменен',
    'sent' => 'Ссылка для восстановления пароля отправлена на Ваш email',
    'token' => 'Ссылка на восстановление пароля больше не действительна',
    'user' => 'Пользователь с указанным email-адресом не найден',
    'throttled' => 'Частый сброс пароля. Сброс пароля временно недоступен. Попробуйте позже.',
];