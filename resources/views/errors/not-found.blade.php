@extends('layout')

@section('content')
<br><br><br>
<div class="title-block text-center cart-empty-message">{{ __('К сожалению, запрашиваемая страница не найдена') }}</div>
@endsection