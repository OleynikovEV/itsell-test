@extends('emails.layout')

@section('content')
<table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px; padding: 5px 5px 0 5px; border-color: #f8f8f8; border-bottom: none;">
    <tr style="background-color: #fff;">
        <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">
            <!-- Номер заказа -->
            <table style="width:100%;">
                <tr style="background-color:#fff; padding: 5px 8px;">
                    <td style="padding: 5px 8px;"><h3 style="font-family:Verdana; font-size:16px; font-weight:bold; margin: 0;">{{ __('Заказ') . ' №' . $order->id . ($order->dropshipping ? ' (' . __('Дропшиппинг') . ')' : '') }}</h3></td>
                    <td style="padding: 5px 8px;"><p style="font-family:Verdana; text-align:right; font-size: 14px; color: #3e67ab; text-decoration: underline; margin: 0;">{{ date('d.m.Y H:i', $order->time) }}</p></td>
                </tr>
            </table>

            @if ($mailType == 2 && $order->user)
            <!-- Информация о клиенте -->
            <table cellpadding="0" cellspacing="0" style="width:100%; margin: 10px 0 20px 0; border-collapse:collapse;">
                <tr>
                    <td style="padding: 5px 8px;" colspan="2"><h3 style="font-family:Verdana; font-size:16px; font-weight:bold; margin: 0;">{{ __('Клиент') }}</h3></td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Имя') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->user->fio }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Логин клиента') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->user->email }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Телефон') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->user->phone }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Город') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->user->city }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Компания') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->user->company }}</p>
                    </td>
                </tr>
            </table>
            @endif

            <!-- Информация о заказе -->
            <table cellpadding="0" cellspacing="0" style="width:100%; margin: 10px 0 20px 0; border-collapse:collapse;">
                @if ($mailType == 2)
                <tr>
                    <td style="padding: 5px 8px;" colspan="2"><h3 style="font-family:Verdana; font-size:16px; font-weight:bold; margin: 0;">{{ __('Получатель') }}</h3></td>
                </tr>
                @endif
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('ФИО') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->lastname . ' ' . $order->firstname }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Телефон') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->phone }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Способ доставки') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">
                            {{ $order->delivery_type_id ? [1 => __('Отделение Новой Почты'), 2 => __('Курьер Новой Почты'), 3 => __('Другое')][$order->delivery_type_id] : '' }}

                            @if ($order->delivery_type_id == 3 && $order->delivery_comment)
                                ({{ $order->delivery_comment }})
                            @endif
                        </p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Город') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->city }}</p>
                    </td>
                </tr>

                @if ($order->delivery_type_id)
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">
                            {{ [1 => __('Отделение'), 2 => __('Адрес'), 3 => __('Адрес')][$order->delivery_type_id] }}: 
                        </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">
                            @if ($order->delivery_type_id == 1)
                                {{ optional($order->warehouse)->{'address_' . app()->getLocale() } }}
                            @elseif ($order->delivery_type_id == 2)
                                {{ ($order->street ? $order->street . ', ' : '') . $order->street_building }}
                            @else
                                {{ $order->address }}
                            @endif
                        </p>
                    </td>
                </tr>
                @endif

                @if ($order->dropshipping)
                    <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                        <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                            <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Способ оплаты') }}: </p>
                        </td>
                        <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                            <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->payment_type_id ? [1 => __('Наложенный платеж'), 2 => __('Предоплата')][$order->payment_type_id] : '' }}</p>
                        </td>
                    </tr>

                    @if ($order->payment_type_id == 1)
                    <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                        <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                            <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Сумма') }}: </p>
                        </td>
                        <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                            <p style="padding:10px 0 10px 10px; margin: 0;">{{ +$order->dropshipping_price }} грн</p>
                        </td>
                    </tr>
                    @endif

                    <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                        <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                            <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Стоимость доставки') }}: </p>
                        </td>
                        <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                            <p style="padding:10px 0 10px 10px; margin: 0;">{{ +$order->delivery_price }} грн</p>
                        </td>
                    </tr>
                @endif

                @if ($order->user_comment)
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ __('Комментарий') }}: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $order->user_comment }}</p>
                    </td>
                </tr>
                @endif
            </table>
        </td>
    </tr>
</table>

<table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px; padding: 5px; border-color: #f8f8f8; ">
    <tr style="background-color: #fff;">
        <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">

            <!-- Заказ -->
            <table cellpadding="0" cellspacing="0" style="width:100%;">
                <tr style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
                    <td style="width:305px;"><p style="font-size:14px; font-weight:bold; font-family:Verdana; margin-left:10px;">{{ __('Товар') }}</p></td>
                    <td style="width:65px; text-align:center;"><p style="font-size:14px; font-weight:bold; font-family:Verdana;">{{ __('Кол-во') }}</p></td>
                    <td style="width:75px; text-align:center;"><p style="font-size:14px; font-weight:bold; font-family:Verdana;">{{ __('Сумма') }}</p></td>
                </tr>

                @foreach ($order->products as $product)
                @if ($product->product)
                <tr border="1" style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
                    <td style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
                        <img style="float:left; max-width:45px; max-height:45px; display:inline-block; margin-left:10px;" src="{{ $product->product->img ? config('custom.content_server') . $product->product->img : url('/template/images/noimage.jpg') }}">
                        <a href="{{ custom_route('product', [$product->product->url, $product->product->id]) }}" style="float:left; width:75%; font-size:14px; font-family:Verdana; margin-left:10px; color:#3e67ab; display:inline-block;" target="blank">
                            {{ $product->product->title . ($product->product->color_name ? ' (' . $product->product->color_name . ')' : '') }}
                        </a>
                    </td>
                    <td style="border-bottom: 1px solid #ddd; width:65px; text-align:center;"><p style="font-size:14px; font-family:Verdana;">{{ $product->count }}</p></td>
                    <td style="border-bottom: 1px solid #ddd; width:75px; text-align:center;">
                        <p style="font-size:14px; font-family:Verdana;">
                           {{ round($product->price * $product->count, 2) }}$
                        </p>
                    </td>
                </tr>
                @endif
                @endforeach

              @foreach ($order->products as $product)
                @if ($product->productCover)
                  <tr border="1" style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
                    <td style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
                      <img style="float:left; max-width:45px; max-height:45px; display:inline-block;margin-right:10px;margin-left:10px;" src="{{ $product->productCover->image ? $product->productCover->image : url('/template/images/noimage.jpg') }}">
                        {{ $product->productCover->title }}
                      </td>
                    <td style="border-bottom: 1px solid #ddd; width:65px; text-align:center;"><p style="font-size:14px; font-family:Verdana;">{{ $product->count }}</p></td>
                    <td style="border-bottom: 1px solid #ddd; width:75px; text-align:center;">
                      <p style="font-size:14px; font-family:Verdana;">
                        {{ round($product->price * $product->count, 2) }}$
                      </p>
                    </td>
                  </tr>
                @endif
              @endforeach
            </table>
            <!-- #End Заказ -->

            <!-- #End Сумма к оплате -->
            <table style="width:100%; text-align:right;">
                <tbody style="width: 100%">
                    <tr style="background-color:#fff; padding: 5px 8px;">
                        <td style="width:305px"></td>
                        <td style="width:65px; font-size:14px; font-family:Verdana; text-align: right;">{{ __('Сумма') }}:</td>
                        <td style="width:75px; text-align:center; font-size:14px; font-family:Verdana;"><b>{{ $order->total }}$</b></td>
                    </tr>
                </tbody>
            </table>
            <!-- Сумма к оплате -->
        </td>
    </tr>
</table>
@endsection