@extends('emails.layout')

@section('content')
<table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px; padding: 5px 5px 0 5px; border-color: #f8f8f8; border-bottom: none;">
    <tr style="background-color: #fff;">
        <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">
            <p style="margin:5px 0; font-family:Verdana; font-size:14px;">Добрый день, <b>{{ $user->fio }}</b> @if ($mailType === 2) ({{ $user->email }}) @endif</p>
            <p style="margin:15px 0 5px 0; font-family:Verdana; font-size:14px;">
                Недавно Вы добавили товары в корзину ({{ $cartName }}), но не завершили покупку. Вы можете
                <a href="{{ custom_route('cart') }}">оформить заказ</a> нажав на кнопку ниже.
            </p>
            <p style="text-align: center; font-family: Verdana; padding: 7px 0 10px;">
                <a href="{{ custom_route('cart') }}" style="display: inline-block; width: 215px; height: 35px; background: #fecc03; text-align: center; line-height: 35px; text-decoration: none; font-size: 14px; color: #000; border-radius: 6px;" id="order-button">
                    ОФОРМИТЬ ЗАКАЗ
                </a>
            </p>
        </td>
    </tr>
</table>

<table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px; padding: 5px; border-color: #f8f8f8; ">
    <tr style="background-color: #fff;">
        <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">

            <!-- Заказ -->
            <table cellpadding="0" cellspacing="0" style="width:100%;">
                <tr style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
                    <td style="width:305px;"><p style="font-size:14px; font-weight:bold; font-family:Verdana; margin-left:10px;">Товар</p></td>
                    <td style="width:65px; text-align:center;"><p style="font-size:14px; font-weight:bold; font-family:Verdana;">Кол-во</p></td>
                    <td style="width:75px; text-align:center;"><p style="font-size:14px; font-weight:bold; font-family:Verdana;">Сумма</p></td>
                </tr>

                @foreach ($cart as $item)
                @if ($item->product)
                <!-- Товар -->
                <tr border="1" style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
                    <td style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
                        <img style="float:left; max-width:45px; max-height:45px; display:inline-block; margin-left:10px;" src="{{ $item->product->img ? config('custom.content_server') . thumb_name($item->product->img) : url('/') . '/template/images/noimage.jpg' }}">
                        <a href="{{ custom_route('product', [$item->product->url, $item->product->id])}}" style="float:left; width:75%; font-size:14px; font-family:Verdana; margin-left:10px; color:#3e67ab; display:inline-block;" target="blank">
                            {{ $item->product->title . ($item->product->color_name ? ' (' . $item->product->color_name . ')' : '')}}
                        </a>
                    </td>
                    <td style="border-bottom: 1px solid #ddd; width:65px; text-align:center;"><p style="font-size:14px; font-family:Verdana;">{{ $item->qty }}</p></td>
                    <td style="border-bottom: 1px solid #ddd; width:75px; text-align:center;">
                        <p style="font-size:14px; font-family:Verdana;">
                           {{ round($item->price * $item->qty, 2) }}$
                        </p>
                    </td>
                </tr>
                <!-- #End Товар -->
                @endif
                @endforeach

            </table>
            <!-- #End Заказ -->

            <!-- #End Сумма к оплате -->
            <table style="width:100%; text-align:right;">
                <tbody style="width: 100%">
                    <tr style="background-color:#fff; padding: 5px 8px;">
                        <td style="width:305px"></td>
                        <td style="width:65px; font-size:14px; font-family:Verdana; text-align: right;">Сумма:</td>
                        <td style="width:75px; text-align:center; font-size:14px; font-family:Verdana;"><b>{{ $cartTotal }}$</b></td>
                    </tr>
                </tbody>
            </table>
            <!-- Сумма к оплате -->

            <p style="text-align: center; font-family: Verdana; padding: 7px 0 10px;">
                <a href="{{ custom_route('cart') }}" style="display: inline-block; width: 215px; height: 35px; background: #fecc03; text-align: center; line-height: 35px; text-decoration: none; font-size: 14px; color: #000; border-radius: 6px;" id="order-button">
                    ОФОРМИТЬ ЗАКАЗ
                </a>
            </p>
        </td>
    </tr>
</table>
@endsection