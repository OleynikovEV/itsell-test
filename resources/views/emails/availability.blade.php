@extends('emails.layout')

@section('content')

  <table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px;
  padding: 5px 5px 0 5px; border-color: #f8f8f8; border-bottom: none;">
    <tr style="background-color: #fff;">
      <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">
        <!-- Номер заказа -->
        <table style="width:100%;">
          <tr style="background-color:#fff; padding: 5px 8px;">
            <td style="padding: 5px 8px;"><h3 style="font-family:Verdana; font-size:16px; font-weight:bold; margin: 0;">{{ __('Здравствуйте') }} {{ $user->fio }}</h3></td>
          </tr>
          <tr style="background-color:#fff; padding: 5px 8px;font-family:Verdana;">
            <td>{{ __('На сайте поступление товаров которые вы ожидаете') }}</td>
          </tr>
        </table>

        <table style="width:100%;">
          <tr>
            <td style="padding: 5px 8px;"><p style="font-family:Verdana; text-align:left; font-size: 14px; margin: 0;">{{ __('Дата поступления') }}:</p></td>
            <td style="padding: 5px 8px;"><p style="font-family:Verdana; text-align:right; font-size: 14px; color: #3e67ab; text-decoration: underline; margin: 0;">{{ date('d.m.Y H:i', time()) }}</p></td>
          </tr>
        </table>

        <table>
          @foreach ($products as $product)
            <div border="1" style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
              <td style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
                <img style="float:left; max-width:45px; max-height:45px; display:inline-block; margin-left:10px;" src="{{ $product->img ? config('custom.content_server') . $product->img : url('/template/images/noimage.jpg') }}">
                <a href="{{ custom_route('product', [$product->url, $product->id]) }}" style="float:left; width:75%; font-size:14px; font-family:Verdana; margin-left:10px; color:#3e67ab; display:inline-block;" target="blank">
                  {{ $product->title }}
                </a>
                @if ($product->comment)
                <div style="margin-top: 10px;">
                  <b>{{ __('Комментарий') }}:</b>
                  {{ $product->comment }}
                </div>
                @endif
              </td>
            </tr>
          @endforeach
        </table>
      </td>
    </tr>
  </table>
@endsection
