@extends('emails.layout')

@section('content')

  <table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px;
  padding: 5px 5px 0 5px; border-color: #f8f8f8; border-bottom: none;">
    <tr style="background-color: #fff;">
      <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">
        <!-- Номер заказа -->
        <table style="width:100%;">
          <tr style="background-color:#fff; padding: 5px 8px;">
            <td style="padding: 5px 8px;"><h3 style="font-family:Verdana; font-size:16px; font-weight:bold; margin: 0;">{{ __('Сайт заблокировал следующие аккаунты') }}</h3></td>
          </tr>
          <tr style="background-color:#fff; padding: 5px 8px;font-family:Verdana;">
            <td>{{ __('Пользователи которые не совершали заказы более 4 месяцев') }}</td>
          </tr>
        </table>

        <table style="width:100%;">
          <tr>
            <td style="padding: 5px 8px;"><p style="font-family:Verdana; text-align:left; font-size: 14px; margin: 0;">{{ __('Дата блокировки') }}:</p></td>
            <td style="padding: 5px 8px;"><p style="font-family:Verdana; text-align:right; font-size: 14px; color: #3e67ab; text-decoration: underline; margin: 0;">{{ date('d.m.Y H:i', time()) }}</p></td>
          </tr>
        </table>

        <table>
          <tr border="1" style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
            <th style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">{{ 'Email' }}</th>
            <th style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">{{ 'ФИО' }}</th>
            <th style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">{{ 'Телефон' }}</th>
            <th style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">{{ 'Компания' }}</th>
          </tr>
          @foreach ($users as $user)
          <tr border="1" style="background-color:#fff; padding: 5px 8px; border-bottom:1px solid #ddd;">
            <td style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
              {{ $user->login }}
            </td>
            <td style="border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
              {{ $user->fio }}
            </td>
            <td style="text-align: center; border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
              {{ $user->phone }}
            </td>
            <td style="text-align: center; border-bottom: 1px solid #ddd; width:305px; padding: 10px 0;">
              {{ $user->company }}
            </td>
          </tr>
          @endforeach
        </table>
      </td>
    </tr>
  </table>

@endsection