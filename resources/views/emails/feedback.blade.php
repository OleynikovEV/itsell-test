@extends('emails.layout')

@section('content')
<table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px; padding: 5px 5px 0 5px; border-color: #f8f8f8; border-bottom: none;">
    <tr style="background-color: #fff;">
        <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">

            <!-- Информация о заказе -->
            <table cellpadding="0" cellspacing="0" style="width:100%; margin: 10px 0 20px 0; border-collapse:collapse;">
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Имя: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['name'] }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Телефон: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['phone'] }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Email: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['email'] }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">Название компании: </p>
                  </td>
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['company'] }}</p>
                  </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">Город: </p>
                  </td>
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['city'] }}</p>
                  </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">Способ закупки: </p>
                  </td>
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['purchase_type'] }}</p>
                  </td>
                </tr>
              @if (isset($formData['productId']))
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">Товар: </p>
                  </td>
                  <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                    <p style="padding:10px 0 10px 10px; margin: 0;">
                      <a href="{{ custom_route('product', [$formData['productUrl'], $formData['productId']]) }}">{{ $formData['productTitle'] }}</a>
                    </p>
                  </td>
                </tr>
              @endif
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Сообщение: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['message'] }}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@endsection