<body>
<table style="background-color:#afaeae; width:530px; border-radius:8px; margin:0 auto; border-spacing: 0px; border-radius:5px;">
    <tr>
        <td>
            <table style="background-color:#afaeae; width:530px; border-radius:8px 8px 0 0; margin:0 auto; border-spacing: 0px; padding: 2px; border-collapse: collapse;">
                <tr style="border-radius:8px; height:78px; border:8px;">
                    <td style="width: 110px;">
                        <a style="display:block; margin-left:12px;" href="{{ url('/') }}" target="blank"><img src="{{ url('/template/images/logo.png') }}" style="max-width: 110px;"></a>
                    </td>
                </tr>
            </table>

            @yield('content')

            <table style="background-color:#afaeae; width:530px; border-radius:0 0 8px 8px; margin:0 auto; border-spacing: 0px; padding: 0 2px;">
                <tr style="border-radius:8px; height:78px;">
                    <td style="width: 265px; padding-left: 15px; font-size: 12px; color: #000; font-family:Verdana; border-radius:0 0 0 8px;">
                        <p style="margin:5px 0 10px 0; font-size:12px; font-family:Verdana;"><b>{{ __('По вопросам ОПТа') }}:</b></p>
                        <p style="margin:5px 0;"><a href="tel:{{ preg_replace('/\D/', '', settings('wholesale_phone')) }}" style="color: #000; text-decoration: none;">{{ settings('wholesale_phone') }}</a></p>
                        <p style="margin:5px 0;"><a href="mailto:{{ settings('wholesale_email') }}'" style="color: #000; text-decoration: none;">{{ settings('wholesale_email') }}</a></p>
                    </td>
                    <td style="border-radius:0 0 8px 0; width: 265px; font-size: 12px; color: #000; font-family:Verdana;">
                        <p style="margin:5px 0 10px 0; font-size:12px; font-family:Verdana;"><b>{{ __('По вопросам дропшиппинга') }}:</b></p>
                        <p style="margin:5px 0;"><a href="tel:{{ preg_replace('/\D/', '', settings('dropshipping_phone')) }}" style="color: #000; text-decoration: none;">{{ settings('dropshipping_phone') }}</a></p>
                        <p style="margin:5px 0;"><a href="mailto:{{ settings('dropshipping_email') }}" style="color: #000; text-decoration: none;">{{ settings('dropshipping_email') }}</a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>