@extends('emails.layout')

@section('content')
<table border="1" style="background-color:#f8f8f8; width:530px; margin:0 auto; border-spacing: 0px; padding: 5px 5px 0 5px; border-color: #f8f8f8; border-bottom: none;">
    <tr style="background-color: #fff;">
        <td style="background-color: #fff; padding: 5px 8px; border-radius: 5px; border: 1px solid #ddd;">

            <!-- Информация о заказе -->
            <table cellpadding="0" cellspacing="0" style="width:100%; margin: 10px 0 20px 0; border-collapse:collapse;">
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Email: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['email'] }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Название компании: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['company'] }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Товар: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;"><a href="{{ route('product', [$product->url, $product->id]) }}">{{ $product->title }}</a></p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Цена: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['price'] }}</p>
                    </td>
                </tr>
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Поставщик: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['supplier'] }}</p>
                    </td>
                </tr>

                @if ($formData['comment'])
                <tr style="background-color:#fff; padding: 5px 0; width: 100%; border-bottom: 1px solid #ddd;">
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:160px; border-top: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">Комментарий: </p>
                    </td>
                    <td style="padding: 0; font-family:Verdana; font-size:14px; border-spacing: 0px; border-collapse: collapse; width:320px; border-top: 1px solid #ccc; border-left: 1px solid #ccc;">
                        <p style="padding:10px 0 10px 10px; margin: 0;">{{ $formData['comment'] }}</p>
                    </td>
                </tr>
                @endif
            </table>
        </td>
    </tr>
</table>
@endsection