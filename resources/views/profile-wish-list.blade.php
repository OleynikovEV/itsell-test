@extends('layout')

@section('content')
  <div class="profile-block-wrapper wrapper clearfix">

    <div class="profile-content-block right-block">
      <div class="profile-active-tab">{{ __('Лист ожиданий') }}</div>

      <div class="page">
        {{-- Подписка на рассылку --}}
        <div class="title-block">Подписка на рассылку</div>
        <div class="profile-input-block clearfix">
          <div class="input-field">
              <div class="subscribe">
                @if ($user->telegram_chat_id > 0)
                  <i class="check-mark"></i>
                  <a href=" {{ custom_route('unsubscribe', ['userId' => $user->id, 'messenger' => 'telegram']) }}">{{ __('Отписаться от Telegram') }}</a>
                @else
                  <a href="https://telegram.me/{{ config('custom.telegram_bot_url') }}?start={{ $user->id }}"
                     id='hrefTelegram'
                     data-event="telegram"
                     target="_blank">
                    {{ __('Получать уведомления по Telegram') }}
                  </a>
                @endif
              </div>
              <div class="subscribe">
                @if ($user->viber_chat_id)
                  <i class="check-mark"></i>
                  <a href=" {{ custom_route('unsubscribe', ['userId' => $user->id, 'messenger' => 'viber']) }}">{{ __('Отписаться от Viber') }}</a>
                @else
                  <a href="viber://pa?chatURI={{ config('custom.viber_bot_url') }}&context={{ $user->id }}&text=Отправьте это сообщение чтоб подписаться"
                     id="hrefViber"
                     data-event="viber"
                     target="_blank">
                    {{ __('Получать уведомления по Viber') }}
                  </a>
                @endif
              </div>
            </div>
        </div>
        {{--// Подписка на рассылку --}}

        {{-- Список --}}
        @if ($wishList->count())
          <h2 style="font-size: 2em; margin-bottom: 15px">{{ __('Список товаров') }}</h2>

          <div class="menu bg-gray mb-20 flex flex-direction-row justify-left align-center" style="height: 30px; padding: 5px 10px;">
            <div class="btn-sorting btn-outline-orange cursor_p" id="selectAll" style="margin-left: 0px">
              {{ __('Выделить все') }}
            </div>

            <div class="ml-10 btn-sorting btn-outline-orange cursor_p" id="select">
              {{ __('Выделить') }}
            </div>

            <div id="buttonDelete" class="ml-10 btn btn-disable cursor_p" style="font-size: 15px; margin: 0 5px; padding: 7px 15px;">
              {{ __('Удалить') }}
            </div>
          </div>

          <ul class="multi-column-list flex flex-wrap justify-around">
           @forelse ($wishList as $product)
            <li class="p-5 mb-10 border-box box-shadow-hover" data-item-id="{{ $product->item_id }}">
              <div class="title mb-10 flex flex-direction-row">
                {{-- Удалить --}}
                <div class="wish-button" data-reload="true" style="position: unset;"
                     data-product-id="{{ $product->id }}" data-wish="true">
                  <span class="btn-remove"></span>
                </div>

                <a class="text-grey ml-5 font-weight-bold" href="{{ custom_route('product', [$product->url, $product->id]) }}">{{ $product->title }}</a>
              </div>

              <div class="flex flex-direction-row">
                <div class="image">
                  @if ($product->qty > 0)
                    <div class="label bg-in-stock">{{ __('доступен') }}</div>
                  @elseif ($product->when_appear)
                    <div class="label bg-soon">{{ __('ожидается') }}</div>
                  @else
                    <div class="label bg-out-stock">{{ __('нет в наличии') }}</div>
                  @endif

                  <a href="{{ custom_route('product', [$product->url, $product->id]) }}">
                    <img src="{{ config('custom.content_server') . thumb_name($product->img) }}"
                         style="max-height: 100px;"/>
                  </a>
                </div>
                <div class="ml-10">
                  <div class="comment-label font-weight-bold">
                    {{ __('Комментарии') }}:
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"
                         style="fill: #ff9f15;" class="cursor_p comment" data-id="{{ $product->item_id }}">
                      <path d="M7.127 22.562l-7.127 1.438 1.438-7.128 5.689 5.69zm1.414-1.414l11.228-11.225-5.69-5.692-11.227 11.227 5.689 5.69zm9.768-21.148l-2.816 2.817 5.691 5.691 2.816-2.819-5.691-5.689z"/>
                    </svg>
                  </div>
                  <div id="comment-{{ $product->item_id }}">{{ $product->comment }}</div>
                </div>
              </div>
            </li>
          @endforeach
          </ul>
        @else
          <h2 style="font-size: 2em;">{{ __('Ваш список ожидания пуст') }}</h2>
        @endif
        {{--// Список --}}

        {{-- Пагинация --}}
        @if ($wishList->hasPages())
          <div style="clear: both; padding-top: 20px;">
            <div class="pagination-block">
              <ul>{!! str_replace(['?page=1"', '?page='], ['"', '/'], $wishList->links()) !!}</ul>
            </div>
          </div>
        @endif
        {{--// Пагинация --}}
      </div>
    </div>

    <div class="static-page-popup popup-block"></div>

    @include('partials.profile-submenu')

</div>
@endsection

@push('footer-scripts')
  <script src="/template/js/dist/wish-list.min.js?v=1.0.1"></script>
  <script>
    $(function() {
      $('.profile-active-tab').click(function() {
        $('.page').toggle();
      });

      $('.comment').click(function() {
        const el = this;
        const id = $(el).data('id');
        const text = $(`#comment-${id}`).text();
        const comment = prompt(__('Введите новый комментарий'), text);

        if (comment != null) {
          $.ajax({
            url: '/v1/wishlist/update',
            type: 'post',
            data: {
              itemId: id,
              comment: comment.slice(0, 255)
            },
            dataType: 'json',
            success: function(data) {
              if (data === true) {
                $(`#comment-${id}`).text(comment.slice(0, 255));
              } else {
                console.error(data);
                app.showPopupMessage(__('Не удалось обновить комментарий.'));
              }
            },
            error: function(error) {
              console.error(error);
              app.showPopupMessage(__('Не удалось обновить комментарий.'));
            }
          });
        }
      });

      listItems.init();
    });
  </script>
@endpush