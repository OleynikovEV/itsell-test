@if (!($priceTypeSite === 'disable'))
  <div data-id="price-block-{{ $product->id }}">
  @if ($priceTypeSite)
    <p>
      <div class="product-cart-single-price single-price cursor_p" style="display: inline-block">
        <b class="symbol">{{ $product->getCurrencyType($priceTypeSite)['symbol'] ?? null }}</b>
        <span class="price-default">{{ $product->getPrice($priceTypeSite, false) }}</span>
      </div>

      <span class="cross-out">{{ ($salePrice > 0) ? '$' : null }}<span class="cross-out sale">{{ ($salePrice > 0) ? $salePrice : null }}</span></span>

{{--      <p>₴ <span class="price-default-uah">--}}
{{--        {{ round( $product->getPrice($priceTypeSite, false) * settings('dollar') ) }}--}}
{{--        </span>--}}
{{--      </p>--}}

        {{--   всплывающее окно с дополнительными ценами   --}}
        @if ($product->isPrint() && !$isModal)
          <x-popup-price :product="$product" :bySeries="false" class="price-range-width" />
        @else
        <x-popup-price :product="$product" :bySeries="false" class="popup {{ ($isModal) ? 'popup-modal' : '' }}" />
        @endif
    </p>
  @else
    @if ($showPrice)
      @if (!$product->isPrint() || $isModal)
      <p>
        <span class="range"><b>$</b> {!! implode(' - <b>$</b> ', $product->getPriceRange('all', false)) !!} </span>

        <span class="cross-out"><span class="cross-out sale">{{ ($salePrice > 0) ? '$ ' . $salePrice : null }}</span></span>
      </p>

      <div @if($isModal) {{ 'class="mini-prices"' }} @endif>
        <span @if (!auth()->guest() && auth()->user()->getPriceInContract('opt_price')) class="text-orange" @endif>
          {{ __('Опт') }}: $ <span class="opt">{{ $product->getPrice('opt_price') }}</span>
        </span>
        <span @if (!auth()->guest() && auth()->user()->getPriceInContract('opt_vip_usd')) class="text-orange" @endif>
          Vip: $ <span class="vip">{{ $product->getPrice('opt_vip_usd') }}</span>
        </span>
        <span @if (!auth()->guest() && auth()->user()->getPriceInContract('opt_dealer_usd')) class="text-orange" @endif>
          {{ __('Дилер') }}: $ <span class="diler">{{ $product->getPrice('opt_dealer_usd') }}</span>
        </span>
      </div>
      @endif

      @if ($product->isPrint() && !$isModal)
        <x-popup-price :product="$product" :bySeries="false" class="price-range-width" style="margin-top: 5px;" />
      @else
        <div class="product-page-price-popup @if($isModal) {{ 'new-popup-modal' }} @endif"
        @if($isModal) style="margin-top: -46px;" @endif >
          <span @if (!auth()->guest() && auth()->user()->getPriceInContract('price')) class="text-orange" @endif>
            {{ __('РРЦ') }}: ₴ <span class="rrc">{{ $product->getPrice('price') }}</span>
          </span>
          <span @if (!auth()->guest() && auth()->user()->getPriceInContract('drop_uah')) class="text-orange" @endif>
            {{ __('Дропшиппинг') }}: ₴ <span class="drop">{{ $product->getPrice('drop_uah') }}</span>
          </span>
        </div>
      @endif
    @else
      <div class="product-price-wrapper @if(! $isModal) {{ 'main-banner-content wt-70' }} @endif">
        <p class="cursor_p"><b>$ Скрыто <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('вход') }})</a></b></p>
        <div class="product-page-price-popup mt-0">
          Чтобы увидеть цены и купить товар, нужно <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('авторизоваться') }})</a>
        </div>
      </div>
    @endif
  @endif
</div>
@endif