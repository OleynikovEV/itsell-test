<div class="{{ getColorBorderBottom($product, $groupProduct) }} group-color-height">
  <a href="#" style="background-color: #{{ $groupProduct->color }};" title="{{ $groupProduct->name }}" class="{{ colorWhite($groupProduct->color) }}"
     data-image="{{ $groupProduct->image ? config('custom.content_server') . thumb_name($groupProduct->image) : '/template/images/noimage.jpg' }}"
     {!! $groupProduct->when_appear ? 'data-when-appear="' . substr($groupProduct->when_appear, 0, 5) . '"' : '' !!}
     data-color="{{ $groupProduct->color }}"
     data-bySeries="{{$product->bySeries ?? false}}"
     data-id="{{ $groupProduct->id }}"
     data-parent="{{ $product->id }}"
     data-wish="{{ in_array($groupProduct->id, $wishList) ? 'true' : 'false' }}"
     @if ($showPrice)
     data-price-opt-range="<b>$</b> {!! implode(' - <b>$</b> ', min_max_price($groupProduct->min_price, $groupProduct->max_price)) !!}"
     @if (isset($groupProduct->$priceTypeSite))
     data-price-default="{{ ($priceTypeSite == 'price')
                        ? round($groupProduct->$priceTypeSite / settings('dollar'), 2)
                        : $groupProduct->$priceTypeSite
                      }}"
     data-price-default-uah="{{ ($priceTypeSite == 'price')
                        ? $groupProduct->$priceTypeSite
                        : round($groupProduct->$priceTypeSite * settings('dollar'))
                      }}"
     @endif
     data-price-opt="{{ $groupProduct->opt_price }}"
     data-price-opt-usd="{{ implode(' - ₴ ', array_map(function($item) {return round($item * settings('dollar'));}, min_max_price($groupProduct->min_price, $groupProduct->max_price))) }}"
     data-price-vip="{{ $groupProduct->opt_vip_usd }}"
     data-price-dealer="{{ $groupProduct->opt_dealer_usd }}"
     data-price-drop-uah="{{ $groupProduct->drop_uah }}"
     data-price-ppc="{{ $groupProduct->price }}"
     data-price-old="{{ (isset($groupProduct->old_opt_price) && $groupProduct->old_opt_price > 0) ? $groupProduct->old_opt_price : null }}"
    @endif
  >
    @if ($groupProduct->when_appear)
      <span></span>
    @endif
  </a>

  @if ($groupProduct->opt_price > 0 && $showPrice && ! $product->bySeries)
    <input type="number" class="product-color-qty"
           data-id="{{ $groupProduct->id }}"
           tabindex="1"
           value="0"
           inputmode="numeric">
  @endif
</div>