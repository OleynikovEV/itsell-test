<div class="input-field mt-0 mb-20 flex flex-direction-row justify-between ">
  <p class="align-center pt-5 pr-10">{{ __('Имя') }}:*</p>
  @if ( auth()->guest() || empty(auth()->user()->fio) )
    <input type="text" name="name" class="pt-0" value="">
  @else
    <input type="text" readonly class="pt-0" name="name" value="{{ auth()->user()->fio }}">
  @endif
</div>

<div class="input-field mt-0 mb-20 flex flex-direction-rowjustify-between">
  <p class="align-center pt-5 pr-10">{{ __('Телефон') }}:*</p>
  @if ( auth()->guest() || empty(auth()->user()->phone) )
    <input type="text" name="phone" class="pt-0" value="">
  @else
    <input type="text" readonly class="pt-0" name="phone" value="{{ auth()->user()->phone }}">
  @endif
</div>

<div class="input-field mt-0 mb-20 flex flex-direction-row justify-between">
  <p class="align-center pt-5 pr-10">{{ __('Email') }}:*</p>
  @if ( auth()->guest() || empty(auth()->user()->email) )
    <input type="text" class="pt-0" name="email" value="">
  @else
    <input type="text" readonly class="pt-0" name="email" value="{{ auth()->user()->email }}">
  @endif
</div>

<div class="input-field mt-0 mb-20 flex flex-direction-row justify-between">
  <p class="align-center pt-5 pr-10 text-left">{{ __('Название компании') }}:*</p>
  @if ( auth()->guest() || empty(auth()->user()->company) )
      <input type="text" class="pt-0" name="company" value="">
  @else
    <input type="text" readonly class="pt-0" name="company" value="{{ auth()->user()->company }}">
  @endif
</div>

<div class="input-field mt-0 mb-20 flex flex-direction-row justify-between">
  @if ( auth()->guest() || empty(auth()->user()->city) )
    <p class="align-center pt-20 pr-10 text-left">{{ __('Город') }}:*</p>
    <div class="select-block custom-select city-select">
      <input type="hidden" name="city" class="custom-select-input" value="">
      <div class="selected-block custom-select-active-value">{{ __('Выберите город') }}</div>
      <div class="select-list-block custom-select-list">
        <div class="select-list-search custom-select-search"><input type="text" placeholder="{{ __('Поиск') }}"></div>
      </div>
    </div>
  @else
    <p class="align-center pt-5 pr-10 text-left">{{ __('Город') }}:*</p>
    <input type="text" readonly class="pt-0" name="city" value="{{ auth()->user()->city }}">
  @endif
</div>

<div class="input-field mt-0 mb-20">
  <p class="align-center pt-5 pr-10 text-left">{{ __('Cпособ закупки') }}:*</p>
  @if ( auth()->guest() || !isset(auth()->user()->details['purchase_types']) || empty(auth()->user()->details['purchase_types']))
    <div class="input-field mt-0 checkbox-block">
      <label class="checkbox">
        <input type="checkbox" name="purchase_type[]" value="wholesale" style="display: none">{{ __('Опт') }}
      </label>
      <label class="checkbox">
        <input type="checkbox" name="purchase_type[]" value="dropshipping" style="display: none">{{ __('Дропшиппинг') }}
      </label>
      <label class="checkbox">
        <input type="checkbox" name="purchase_type[]" value="retail" style="display: none">{{ __('Розница') }}
      </label>
    </div>
    <input type="hidden" name="purchase_type_null" value="true" />
    @else
      <ul class="mt-20">
        @foreach (auth()->user()->details['purchase_types'] as $type)
          <li style="margin: 10px 0">
          @switch($type)
            @case('dropshipping') {{ __('Дропшиппинг') }} @break
            @case('wholesale') {{ __('Опт') }} @break
            @case('retail') {{ __('Розница') }} @break
          @endswitch
          </li>
        @endforeach
      </ul>
        <input type="hidden" readonly class="pt-0" name="purchase_type" value="{{ implode(',', auth()->user()->details['purchase_types']) }}">
  @endif
</div>

@if (! auth()->guest())
  <a class="text-orange" href="{{ custom_route('profile') }}">*** {{ __('Личные данные Вы можете изменить в своем кабинете') }}</a>
@endif

<div class="input-field">
  <p>{{ $titleText }}:*</p>
  <textarea name="message"></textarea>
</div>