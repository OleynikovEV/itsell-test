@if ($priceTypeSite === 'disable')
  <p>&nbsp;&nbsp;</p>
  <p>&nbsp;&nbsp;</p>
@else
<span data-id="price-block-{{ $product->id }}">
  @if ($priceTypeSite)
    <p>
        <b class="price-default">
          $ {{ $product->getPrice($priceTypeSite, true, $product->bySeries ?? false) }}
        </b>

      @if ($sale > 0 && $priceTypeSite != 'price')
        <span class="line-through sale">$ {{ $sale }}</span>
      @endif

      <p class="price-default-uah">₴
        @if ($product->bySeries)
            {{ $product->getPrice($priceTypeSite, true, true, true) }}
        @else
          {{ round( $product->getPrice($priceTypeSite, true) * settings('dollar') ) }}
        @endif
        </span>
      </p>
    </p>

    {{--   всплывающее окно с дополнительными ценами   --}}
    <x-popup-price :product="$product" :bySeries="$product->bySeries" class="product-price-popup" />

  @else
    @if ($showPrice)
      <p>
        <b class="range">$ {{ implode(' - $ ', $product->getPriceRange('all', $product->bySeries ?? false)) }}</b>
{{--        <b class="range">$ @php $product->getPriceRange('all', $product->bySeries ?? false) @endphp</b>--}}
        @if ($sale > 0)
          <span class="line-through sale">$ {{ $sale }}</span></span>
        @endif
      </p>

      <p>₴ {{ implode(' - ₴ ', array_map(function($item) {return round($item * settings('dollar'));}, $product->getPriceRange('all', $product->bySeries ?? false))) }}</p>

      {{--   всплывающее окно с дополнительными ценами   --}}
      <x-popup-price :product="$product" :bySeries="$product->bySeries" class="product-price-popup" />
    @else
      <p class="cursor_p mt-20 ml-10"><b>$ Скрыто <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('вход') }})</a></b></p>
      <div class="product-price-popup">
        Чтобы увидеть цены и купить товар, нужно <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('авторизоваться') }})</a>
      </div>
    @endif
  @endif
</span>
@endif