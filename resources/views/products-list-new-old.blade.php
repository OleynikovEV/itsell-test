@extends('layout')

@section('content')

  @if ($banner)
    <div class="category-banner-block">
      <div class="wrapper">
        @if ($banner->link) <a href="{{ $banner->link }}" target="_blank"> @endif
          <img src="{{ config('custom.content_server') . $banner->image }}" alt="{{ $banner->alt }}">
          @if ($banner->link) </a> @endif
      </div>
    </div>

    <div class="mobile-category-banner">
      @if ($banner->link) <a href="{{ $banner->link }}" target="_blank"> @endif
        <img src="{{ config('custom.content_server') . $banner->image_mobile }}" alt="{{ $banner->alt }}">
        @if ($banner->link) </a> @endif
    </div>
  @endif

  <div class="wrapper">
    <div class="mobile-btn-filter {{ $activeFilters->except('parent_category')->count() ? 'mobile-btn-filter-number' : '' }}">
      {{ $activeFilters->except('parent_category')->flatten()->count() ?: '' }}
    </div>
    <h1 class="title-block">{{ $h1Text }}</h1>

    @if (optional($parentCategories)->count())
      <div class="filter-device-block">
        <div class="filter-device-title">{{ __('Гаджеты') }}</div>

        <div class="filter-device-content clearfix">
          <div class="left-block">
            @foreach ($parentCategories as $parentCategory)
              <a href="{{ $baseUrl . '/' . $parentCategory->name . ($filtersStringWithoutCategories ? '/filter/' . $filtersStringWithoutCategories : '') }}"
                {!! optional($activeParentCategory)->id == $parentCategory->id ? 'class="active"' : '' !!}>
                {{ $parentCategory->title }} ({{ $categoriesCounters[$parentCategory->id][0] }})

                @if ($categoriesCounters[$parentCategory->id][1])
                  <span>+{{ $categoriesCounters[$parentCategory->id][1] }}</span>
                @endif
              </a>
            @endforeach
          </div>
        </div>
      </div>
    @endif

    @if (optional($categories)->count())
      <div class="filter-device-block filter-subcategories">
        <div class="filter-device-title">{{ __('Подкатегории') }}</div>

        <div class="filter-device-content filter-values-list clearfix">
          <div class="left-block">
            @foreach ($categoriesSectors as $sector)
              @if ($categoriesSectors->count() > 1 || $sector->id)
                <p class="filter-device-sector">{{ $sector->title }}</p>
              @endif

              @foreach ($categories->get($sector->id) as $category)
                <label class="checkbox {{ $categoriesSectors->count() > 1 && $loop->index > 4 ? 'filter-subcategory-hidden hidden' : '' }}">
                  <input type="checkbox" name="categories" value="{{ $category->id }}"
                    {{ $activeFilters->has('categories') && $activeFilters->get('categories')->search($category->id) !== false ? ' checked' : '' }}>
                  {{ trim(str_replace($activeParentCategory->title, '', $category->title)) }}
                  @if (isset($categoriesCounters[$category->id][0])) ({{ $categoriesCounters[$category->id][0] }}) @else * @endif

                  @if ($categoriesCounters[$category->id][1])
                    <span>+{{ $categoriesCounters[$category->id][1] }}</span>
                  @endif
                </label>
              @endforeach
            @endforeach
          </div>
        </div>
        <div class="clearfix">
          <div class="filter-subcategories-show-all btn-red hidden">{{ __('Все модели') }}</div>
        </div>
      </div>
    @endif

    @if (optional($accessoriesCategories)->count())
      <div class="filter-device-block">
        <div class="filter-device-title">{{ __('Аксессуары') }}</div>

        <div class="filter-device-content clearfix">
          <div class="left-block">
            @foreach ($accessoriesCategories as $category)
              <a href="{{ $baseUrl . '/others-' . $category->name . ($filtersString ? '/filter/' . $filtersString : '') }}"
                {!! optional($activeParentCategory)->id == $category->id ? 'class="active"' : '' !!}>
                {{ $category->title }} ({{ $categoriesCounters[$category->id][0] }})

                @if ($categoriesCounters[$category->id][1])
                  <span>+{{ $categoriesCounters[$category->id][1] }}</span>
                @endif
              </a>
            @endforeach
          </div>
        </div>
      </div>
    @endif

    @if ($activeFilters->except('parent_category')->count() || $filters->has('categoriya'))
      <div class="clearfix mobile-filters-counter-wrapper">
        <div class="mobile-filters-categories">
          @if ($filters->has('categoriya'))
            @foreach ($filters->get('categoriya')->availableValues as $filterValue)
              @if (isset($maxFiltersCategories[$filterValue->id]))
                <div class="filter-values-list-2">
                  <label class="checkbox">
                    <input type="checkbox" name="categoriya" value="{{ $filterValue->id }}"
                      {{ $activeFilters->has('categoriya') &&
                          $activeFilters->get('categoriya')->search($filterValue->id) !== false ? ' checked' : '' }}/>
                    {{ $filterValue->has_children ? '(+)' : '' }} {{ $filterValue->name }}
                    ({{ $maxFiltersCategories[$filterValue->id][0] }})
                    @if ($maxFiltersCategories[$filterValue->id][1])
                      <span>+{{ $maxFiltersCategories[$filterValue->id][1] }}</span>
                    @endif
                  </label>
                </div>
              @endif
            @endforeach
          @endif
        </div>
        <div class="mobile-filters-counter">{{ __('Фильтр') }}: {{ $activeFilters->except('parent_category')->flatten()->count() }}</div>
      </div>
    @endif

  </div>

  <!-- Category Panel Block -->
  <div class="category-panel-block">
    <div class="wrapper clearfix">
      @if ($groupBySeries !== null)
      <!--  Группировать по сериям    -->
      <div class="sorting-block mobile-width-100">
        <div class="sorting-content-block">
          <p class="group-button">
            <img class="cursor_p m-5 page-button tooltip-event" src="/template/images/hint_icon.svg"
                 title="{{ __('Группирует товары по сериям, что бы Вам было удобнее изучать широкий ассортимент.') }}"
                 style="vertical-align: middle;">
            <label class="checkbox">{{ __('Группировать') }}
              <input type="checkbox" id="groupBySeries" name="groupBySeries" value="true" @if ($groupBySeries) checked @endif />
            </label>
          </p>
        </div>
      </div>
      @endif

      <div class="mobile-text-center">
        <!-- Sorting Block -->
      <div class="sorting-block custom-select products-list-sort">


        <div class="sorting-content-block">
          <p>{{ __('Выводить') }}:</p>
          <span class="custom-select-active-value">{{ $productsSort ? ['discounts' => __('Акции'), 'cheap' => __('Дешевые'), 'expensive' => __('Дорогие')][$productsSort] : __('Новинки') }}</span>
        </div>
        <div class="sorting-popup-block custom-select-list">
          <div data-value="default" {!! ! $productsSort ? 'class="active"' : '' !!}>{{ __('Новинки') }}</div>
          <div data-value="discounts" {!! $productsSort == 'discounts' ? 'class="active"' : '' !!}>{{ __('Акции') }}</div>
          <div data-value="cheap" {!! $productsSort == 'cheap' ? 'class="active"' : '' !!}>{{ __('Дешевые') }}</div>
          <div data-value="expensive" {!! $productsSort == 'expensive' ? 'class="active"' : '' !!}>{{ __('Дорогие') }}</div>
        </div>
      </div>
        <!-- #End Sorting Block -->

        <!-- Sorting Block -->
        <div class="sorting-block custom-select products-list-per-page">
          <div class="sorting-content-block">
            <p>{{ __('Показать') }}:</p>
            <span class="custom-select-active-value">{{ $perPage }}</span>
{{--            <p>{{ __('на странице') }}</p>--}}
          </div>
          <div class="sorting-popup-block custom-select-list">
            <div data-value="40" {!! $perPage == 40 ? 'class="active"' : '' !!}>40</div>
            <div data-value="60" {!! $perPage == 60 ? 'class="active"' : '' !!}>60</div>
            <div data-value="80" {!! $perPage == 80 ? 'class="active"' : '' !!}>80</div>
          </div>
        </div>
        <div></div>
        <!-- #End Sorting Block -->
      </div>

      <!-- Sorting Block -->
{{--      <div class="sorting-block">--}}
{{--        <div class="sorting-content-block">--}}
{{--          <p>{{ __('Показывать как') }}:</p>--}}
{{--          <span class="product-display-type product-display-block {{ request()->cookie('products_display_type') != 'list' ? 'active' : '' }}"></span>--}}
{{--          <span class="product-display-type product-display-line {{ request()->cookie('products_display_type') == 'list' ? 'active' : '' }}"></span>--}}
{{--          <img src="/template/images/display_block_active_icon.svg" />--}}
{{--          <img src="/template/images/display_line_icon.svg" />--}}
{{--        </div>--}}
{{--      </div>--}}
      <!-- #End Sorting Block -->
    </div>
  </div>
  <!-- #End Category Panel Block -->

  <div class="gradient-block">
    <div class="wrapper">

      <div class="fast-filters-block">
        @if (false)
          <span>Действующие акции</span>
          <a href="#" class="btn-white">Скидки</a>
          <a href="#" class="btn-white">2 по цене 1</a>
          <a href="#" class="btn-white">Вернем до 25%</a>
          <a href="#" class="btn-white">Оплата частями</a>
          <a href="#" class="btn-white">Кредит 0.01%</a>
        @endif
      </div>

      <div class="category-content-block clearfix">

        <!-- Фильтры -->
        <div class="category-filters-block left-block">
          <div class="category-filters-wrapper">

            @if ($activeFilters->except('parent_category')->count() && ($activeParentCategory != null || $gadgetFilter))
              <div class="filter-block filter-values-list">
                <p>{{ __('Вы выбрали') }}:</p>

                @if ($activeParentCategory != null && $gadgetFilter)
                  <div class="filter-selected">
                    {{ $activeParentCategory->title }}
                    <button class="filter-reset"
                            data-filter="gadget"
                            data-is-series="{{ $products_series }}"
                            data-value="{{ $activeParentCategory->name }}">
                    </button>
                  </div>
                @endif

                @foreach ($activeFilters->except('parent_category', 'price_currency') as $activeFilter => $activeFilterValues)
                  @foreach ($activeFilterValues as $activeFilterValue)
                    <div class="filter-selected">
                      @if ($activeFilter == 'brand')
                        {{ __('Бренд') }}: {{ optional($brands->get($activeFilterValue))->title }}
                      @elseif ($activeFilter == 'categories' && $categories !== null)
                        {{ optional($categories->flatten()->keyBy('id')->get($activeFilterValue))->title }}
                      @elseif ($activeFilterValue == '257,258')
                        {{ 'Стекла / Пленки' }}
                      @elseif ($activeFilter == 'price_from')
                        {{ __('Цена от') . ' $' . $activeFilterValue . '' }}
                      @elseif ($activeFilter == 'price_to')
                        {{ __('Цена до') . ' $' . $activeFilterValue }}
                      @elseif ($filters->has($activeFilter) && $filters[$activeFilter]->availableValues->has($activeFilterValue))
                        {{ $filters[$activeFilter]->name}}: {{ $filters[$activeFilter]->availableValues[$activeFilterValue]->name }}
                      @elseif ($activeFilter == 'ucenka')
                        {{ __('Уценка') }}
                      @elseif ($activeFilter == 'sale')
                        {{ __('Скидка') }}
                      @elseif ($activeFilter == 'soon')
                        {{ __('Скоро') }}
                      @elseif ($activeFilter == 'novelty')
                        {{ __('Новинки') }}
                      @elseif ($activeFilter == 'color')
                        <div class="filter-color-block__small {{ colorWhite($activeFilterValue) }}"
                             style="background: #{{ $activeFilterValue  }};"></div>
                      @elseif ($activeFilter == 'color-child')
                        <div class="filter-color-block__small {{ colorWhite($activeFilterValue) }}"
                             style="background: #{{ $activeFilterValue  }};"></div>
                      @endif

                        <button class="filter-reset"
                                data-filter="{{ $activeFilter }}"
                                data-id="{{ $activeFilter }}_{{ str_replace(',', '_', $activeFilterValue) }}"
                                @if ($activeFilter == 'color' && !empty($parentColors))
                                @php $parentColor = $parentColors->where('color', $activeFilterValue)->first() @endphp
                                data-colorId="{{ ($parentColor !== null) ? $parentColors->where('color', $activeFilterValue)->first()->id : 'null' }}"
                                @endif
                                data-value="{{ $activeFilterValue }}">

                        </button>
                    </div>
                  @endforeach
                @endforeach

                <b>{{ __('Подобрано') . ' ' . $totalProducts . ' ' . word_declension(__('товар'), $totalProducts) . ' ' . __('из') . ' ' . $totalProductsBeforeFilters }}</b>
                <div class="filter-reset-all btn-red">{{ __('Очистить все') }}</div>
              </div>
            @endif

            @if ( ! $filters->where('id', 84)->count())
              <div class="filter-block filter-values-list">
                <p>{{ __('Цена') }}</p>
                <div class="price-range-block clearfix">
                  <div class="from left-block">
                    <input type="text" value="{{ $activeFilters->has('price_from') ? $activeFilters->get('price_from')->first() : $filterPriceMin }}">
                  </div>
                  <div class="to right-block">
                    <input type="text" value="{{ $activeFilters->has('price_to') ? $activeFilters->get('price_to')->first() : $filterPriceMax }}">
                  </div>
                </div>
                <div class="price-range-slider-wrapper">
                  <div class="price-range-slider" data-min="{{ $filterPriceMin }}" data-max="{{ $filterPriceMax }}"></div>
                </div>
              </div>
            @endif

            @foreach ($filters as $filter)
                <div class="filter-block filter-values-list" data-parent-filters-values="{{  $filter->parent_filters_values ? implode(',', $filter->parent_filters_values) : '' }}">
                <p>{{ $filter->name }}</p>

                @foreach ($filter->availableValues as $filterValue)
                  <label class="checkbox {{ $loop->index > 4 ? 'filter-value-hidden hidden' : '' }}">
                    <input type="checkbox" name="{{ $filter->url }}" value="{{ $filterValue->id }}"
                      {{ $activeFilters->has($filter->url) && $activeFilters->get($filter->url)->search($filterValue->id) !== false ? ' checked' : '' }}
                      id="{{ $filter->url }}_{{ str_replace(',', '_', $filterValue->id) }}"
                      data-parent-filter="true">
                    {{ $filterValue->has_children ? '(+)' : '' }} {{ $filterValue->name }}
                    ({{ $filtersCounters[$filterValue->id][0] }})

                    @if ($filtersCounters[$filterValue->id][1])
                      <span>+{{ $filtersCounters[$filterValue->id][1] }}</span>
                    @endif
                  </label>
                @endforeach

                @if ($filter->availableValues->count() > 5)
                  <div class="filter-values-show-all btn-red">{{ __('Показать все') }}</div>
                @endif

              </div>

              @if ($filter->id == 84)
                <div class="filter-block filter-values-list">
                  <p>{{ __('Цена') }}</p>
                  <div class="price-range-block clearfix">
                    <div class="from left-block">
                      <input type="text" value="{{ $activeFilters->has('price_from') ? $activeFilters->get('price_from')->first() : $filterPriceMin }}">
                    </div>
                    <div class="to right-block">
                      <input type="text" value="{{ $activeFilters->has('price_to') ? $activeFilters->get('price_to')->first() : $filterPriceMax }}">
                    </div>
                  </div>
                  <div class="price-range-slider-wrapper">
                    <div class="price-range-slider" data-min="{{ $filterPriceMin }}" data-max="{{ $filterPriceMax }}"></div>
                  </div>
                </div>
              @endif

            @endforeach

              @if ($parentColors && count($parentColors) > 0)
                <div class="filter-block filter-color-block">
                  <p>{{ __('Цвет') }}</p>
                  @foreach ($parentColors as $num => $color)
                    <div style="background: #{{ $color->color }}"
                         title="{{ $color->name }}"
                         data-title="{{ $color->name }}"
                         class="filter-color-chackbox {{ in_array($color->color, $colorFilters) ? 'active' : null }}
                         {{ $color->new ? 'new' : '' }}
                         {{ colorWhite($color->color) }}
                         {{ ($num > 4) ? 'filter-value-hidden hidden-mob-only' : null }}">
                      @if ($color->new)
                        <div class="new-color" title="{{ __('Новинка') }}">H</div>
                      @endif
                      <input type="checkbox" class="checkbox-color" name="color"
                             title="{{ $color->name }}"
                             value="{{ $color->color }}"
                             data-id="{{ $color->id }}"
                        {{ in_array($color->color, $colorFilters) ? 'checked' : null }} />
                    </div>
                  @endforeach
                  @if (count($parentColors) > 5)
                    <span class="filter-values-show-all hidden-desktop-only btn-red">{{ __('Показать все') }}</span>
                  @endif
                </div>

                @if ($activeFilters->has('color') && $childrenColorsFilter)
                  <div class="filter-block filter-color-block">
                    <p>{{ __('Дочерние цвета') }}</p>
                    @foreach ($childrenColorsFilter as $num => $childrenColorFilter)
                      <div style="background: #{{ $childrenColorFilter->color }}"
                           data-title="{{ $childrenColorFilter->getColorName() }}"
                           title="{{ $childrenColorFilter->getColorName() }}"
                           class="filter-color-chackbox {{ in_array($childrenColorFilter->color, $colorFilters) ? 'active' : null }}
                           {{ $childrenColorFilter->new ? 'new' : '' }}
                           {{ colorWhite($childrenColorFilter->color) }}
                           {{ ($num > 4) ? 'filter-value-hidden hidden-mob-only' : null }}">
                        @if ($childrenColorFilter->new)
                          <div class="new-color" title="{{ __('Новинка') }}">H</div>
                        @endif
                        <input type="checkbox" class="checkbox-color" name="color-child"
                               data-parent="{{ $childrenColorFilter->parent_id }}"
                               value="{{ $childrenColorFilter->color }}"
                          {{ in_array($childrenColorFilter->color, $colorFilters) ? 'checked' : null }} />
                      </div>
                    @endforeach
                    @if ($childrenColorsFilter->count() > 5)
                      <span class="filter-values-show-all hidden-desktop-only btn-red">{{ __('Показать все') }}</span>
                    @endif
                  </div>
                @endif
              @endif

              {{-- фильтр по Наши бренды --}}
              @if ($brandsFilter && $brandsFilter->has('our') && $brandsFilter->get('our')->count())
                <div class="filter-block filter-values-list">

                  <p>{{ __('Наши бренды') }}</p>

                  @foreach ($brandsFilter->get('our') as $brand)
                    <label class="checkbox {{ $loop->index > 4 ? 'filter-value-hidden hidden' : '' }}">
                      <input type="checkbox" name="brand" value="{{ $brand->id }}"
                        {{ $activeFilters->has('brand') && $activeFilters->get('brand')->search($brand->id) !== false ? ' checked' : '' }}>
                      {{ $brand->title }} ({{ $brand->amount }})

                      @if ($brand->amountNew)
                        <span>+{{ $brand->amountNew }}</span>
                      @endif
                    </label>
                  @endforeach

                  @if ($brandsFilter->get('our')->count() > 5)
                    <div class="filter-values-show-all btn-red">{{ __('Показать все') }}</div>
                  @endif

                </div>
              @endif

              {{-- фильтр по бренду --}}
              @if ($brandsFilter && $brandsFilter->has('data') && $brandsFilter->get('data')->count())
                <div class="filter-block filter-values-list">

                  <p>{{ __('Бренд') }}</p>

                  @foreach ($brandsFilter->get('data') as $brand)
                    <label class="checkbox {{ $loop->index > 4 ? 'filter-value-hidden hidden' : '' }}">
                      <input type="checkbox" name="brand" value="{{ $brand->id }}"
                        {{ $activeFilters->has('brand') && $activeFilters->get('brand')->search($brand->id) !== false ? ' checked' : '' }}>
                      {{ $brand->title }} ({{ $brand->amount }})

                      @if ($brand->amountNew)
                        <span>+{{ $brand->amountNew }}</span>
                      @endif
                    </label>
                  @endforeach

                  @if ($brandsFilter->get('data')->count()   > 5)
                    <div class="filter-values-show-all btn-red">{{ __('Показать все') }}</div>
                  @endif

                </div>
              @endif

              @if ($filter_other)
                <div class="filter-block filter-values-list">
                  <p>{{ __('Другое') }}</p>

                  @if (count($ucenennye_tovary) > 0)
                    <label class="checkbox">
                      <input type="checkbox" name="ucenka" value="true"
                        {{ $activeFilters->has('ucenka') ? ' checked' : '' }}>
                      {{ __('Уценка') }} ({{ $ucenennye_tovary['ucenka'][0] }})

                      @if ($ucenennye_tovary['ucenka'][1] > 0)
                        <span>+{{ $ucenennye_tovary['ucenka'][1] }}</span>
                      @endif
                    </label>
                  @endif

                  @if (count($sale_tovary) > 0)
                    <label class="checkbox">
                      <input type="checkbox" name="sale" value="true"
                        {{ $activeFilters->has('sale') ? ' checked' : '' }}>
                      {{ __('Скидка') }} ({{ $sale_tovary['sale'][0] }})

                      @if ($sale_tovary['sale'][1] > 0)
                        <span>+{{ $sale_tovary['sale'][1] }}</span>
                      @endif
                    </label>
                  @endif

                  @if (count($soon_tovary) > 0)
                    <label class="checkbox">
                      <input type="checkbox" name="soon" value="true"
                        {{ $activeFilters->has('soon') ? ' checked' : '' }}>
                      {{ __('Скоро') }} ({{ $soon_tovary['soon'][0] }})

                      @if ($soon_tovary['soon'][1] > 0)
                        <span>+{{ $soon_tovary['soon'][1] }}</span>
                      @endif
                    </label>
                  @endif

                  @if (count($novelty_tovary) > 0)
                    <label class="checkbox">
                      <input type="checkbox" name="novelty" value="true"
                        {{ $activeFilters->has('novelty') ? ' checked' : '' }}>
                      {{ __('Новинки') }} ({{ $novelty_tovary['novelty'][0] }})

                      @if ($novelty_tovary['novelty'][1] > 0)
                        <span>+{{ $novelty_tovary['novelty'][1] }}</span>
                      @endif
                    </label>
                  @endif
                </div>
              @endif

            <div class="mobile-btn-close"></div>
          </div>
        </div>

{{--        <div class="category-products-block left-block {{ request()->cookie('products_display_type') == 'list' ? 'display-line' : '' }}">--}}
          @each('partials.list-product', $products, 'product')
{{--        </div>--}}
      </div>

      @if ($products->hasPages())
        <div class="pagination-block">
          @if ($products->hasMorePages())
            <a href="#" id="btn-load-more-products" class="btn-load-more btn-white" data-url="{{ $paginationUrl }}" data-page="{{ $products->currentPage() }}">
              {{ __('Показать еще') . ' ' . min($perPage, $totalProducts - $perPage * $products->currentPage()) . ' '
                  . __('из') . ' ' . ($totalProducts - $perPage * $products->currentPage()) }}
            </a>
          @endif

          <ul>{!! str_replace(['?page=1"', '?page='], ['"', '/'], $products->links()) !!}</ul>
        </div>
      @endif
    </div>
  </div>

@if ($seoDescription)
  <div class="wrapper clearfix seo-description static-page">
    {!! $seoDescription !!}
  </div>
@endif

  <div class="static-page-popup popup-block"></div>

@endsection

@push('footer-scripts')

  <script>
    var productsListBaseUrl = '{{ $baseUrl . ($parentCategoryUrl ? '/' . $parentCategoryUrl : '') }}';
    var paginationUrl = @json($paginationUrl);
    var productsListPerPage = {{ $perPage }};

    var activeFilters = {!! json_encode($activeFilters) !!};
  </script>
  <script src="/template/js/dist/products-list.min.js?1.1.78"></script>

{{--  @include('partials.list-product', ['includeOnlyTemplate' => true])--}}

@endpush