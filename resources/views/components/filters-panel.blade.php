@forelse($filters as $filter)
  <div class="filter-block filter-values-list">
    <p>{{ $filter->name }}</p>

    <x-filter-block :parentUrl="$filter->url" :parentId="$filter->id" :values="$filter->values"></x-filter-block>
  </div>
@empty
@endforelse