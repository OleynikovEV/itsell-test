@if($products)
<div class="gradient-block cross-products-block clearfix">
  <div class="wrapper">
    <div class="product-list-block product-list-big oneline">
      <div class="title-block">{{ __('Также интересуются') }}</div>
      <div class="product-list-wrapper clearfix">

        <div class="block" style="display: none"></div>
        @each('partials.list-product', $products, 'product')

      </div>
      <div class="btn-show-more"></div>
    </div>
  </div>
</div>
@endif