  <div {{ $attributes }} style="left:-9px;">
  <div class="table-price-range">
    <div class="tr">
      <div class="th"></div>
      @foreach ($amountRange as $num)
        <div class="th bold">{{ $num }}шт</div>
      @endforeach
    </div>
  @foreach ($priceRangeForPrint as $key => $items)
    @if ($key === 'opt_minopt_usd') @continue @endif
    <div class="tr">
      <div class="th {{ $class[$key] }}">{{ $ru[$key] }}</div>
      @foreach ($items as $num => $item)
        <div class="td {{ $class[$key] }}">{{ $item }}</div>
      @endforeach
    </div>
  @endforeach
  </div>
</div>