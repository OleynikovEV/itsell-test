@if ($prices !== null)
  <div class="filter-block filter-values-list">
    <p>{{ __('Цена') }}</p>
    <div class="price-range-block clearfix">
      <div class="from left-block">
        <input type="text" value="{{ $prices->get('price_from') }}">
      </div>
      <div class="to right-block">
        <input type="text" value="{{ $prices->get('price_to') }}">
      </div>
    </div>
    <div class="price-range-slider-wrapper">
      <div class="price-range-slider" data-min="{{ $prices->get('price_to') }}" data-max="{{ $prices->get('price_to') }}"></div>
    </div>
  </div>
@endif