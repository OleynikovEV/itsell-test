{{--@if($categoriesGadget)--}}
@if(false)
<div class="filter-device-block">
  <div class="filter-device-title">{{ __('Гаджеты') }}</div>

  <div class="filter-device-content clearfix">
    <div class="left-block">
      @foreach($categoriesGadget as $item)
        <a href="">{{ $item->title }}</a>
      @endforeach
    </div>
  </div>

</div>
@endif

@if($sectors)
<div class="filter-device-block filter-subcategories">
  <div class="filter-device-title">{{ __('Подкатегории') }}</div>

  <div class="filter-device-content filter-values-list clearfix">
    <div class="left-block">
      @foreach($sectors as $sector)
        <p class="filter-device-sector">{{ $sector->title }}</p>

        @foreach($sector->models as $model)
          <label class="checkbox {{ $loop->index > 4 ? 'filter-subcategory-hidden hidden' : '' }}">
            <input type="checkbox"
                   {{ $model->selected ?? '' }}
                   data-filter-url="categories"
                   value="{{ $model->id }}"> {{ $model->name }} ({{ $model->amount }})

            @if ($model->novelty)
              <span>+{{ $model->novelty }}</span>
            @endif

            @if ($model->coming)
              <span class="text-orange">+{{ $model->coming }}</span>
            @endif
          </label>
        @endforeach
      @endforeach
    </div>
  </div>

  <div class="clearfix">
    <div class="filter-subcategories-show-all btn-red hidden">{{ __('Все модели') }}</div>
  </div>
</div>
@endif