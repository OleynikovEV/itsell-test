<div>
  @foreach ($products as $product)
    <a href="{{ custom_route('search_category', [$product->url, $searchPhrase]) }}" class="border-bottom header-search-product clearfix">
      <span>
        <span class="header-search-name">{{ $searchPhrase }}</span>
        <span class="header-search-price">
          <i>{{ $product->title }} <b>{{ __('Количество') }}: {{ $product->total }}</b></i>
        </span>
      </span>
    </a>
  @endforeach
  <a href="{{ custom_route('search', [$searchPhrase]) }}" class="header-search-total">
    {{ __('Показать еще') }} {{ $total }} {{ __('товаров') }}
  </a>
</div>