@if($categories !== null)
    <div class="filter-block filter-values-list" data-parent-filters-values="">
      <p>{{ __('Категория') }}</p>

      @foreach($categories as $item)
        <label class="checkbox {{ $loop->index > 4 ? 'filter-value-hidden hidden' : '' }}">
          <input type="checkbox" value="{{ $item->id }}"
                 {{ $item->selected ?? '' }}
                 data-filter-url="categoriya"
                 data-parent-filter="84">
          (+) {{ $item->name }} ({{ $item->amount }})

          @if ($item->novelty)
            <span>+{{ $item->novelty }}</span>
          @endif

          @if ($item->coming)
            <span class="text-orange">+{{ $item->coming }}</span>
          @endif
        </label>
      @endforeach

    </div>
@endif