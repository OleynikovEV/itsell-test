@if ($ourBrands !== null)
  <div class="filter-block filter-values-list">
    <p>{{ __('Наши бренды') }}</p>

    <x-filter-block :parentUrl="'brand'" :parentId="'121'" :values="$ourBrands"></x-filter-block>
  </div>
@endif

@if ($brands !== null)
  <div class="filter-block filter-values-list">
    <p>{{ __('Бренды') }}</p>

    <x-filter-block :parentUrl="'brand'" :parentId="'122'" :values="$brands"></x-filter-block>
  </div>
@endif