@if (isset($values))
  @foreach($values as $item)
    <label class="checkbox {{ $loop->index > 4 ? 'filter-value-hidden hidden' : '' }}">
      <input type="checkbox"
             value="{{ $item->id }}"
             {{ $item->selected ?? '' }}
             data-filter-url="{{ $parentUrl }}"
             data-parent-filter="{{ $parentId }}">
      {{ $item->name }} ({{ $item->amount }})

      @if ($item->novelty)
        <span>+{{ $item->novelty }}</span>
      @endif

      @if ($item->coming)
        <span class="text-orange">+{{ $item->coming }}</span>
      @endif
    </label>
  @endforeach

  @if ($showMore)
    <div class="filter-values-show-all btn-red">{{ __('Показать все') }}</div>
  @endif
@endif