<div {{ $attributes }}>
  <p class="{{ $class['price'] }}">
    {{ __('РРЦ') }}: ₴ <span class="inline-block rrc">{{ $product->getPrice('price', false, $bySeries) }}</span>
  </p>
  <p class="{{ $class['drop_uah'] }}">
    {{ __('Дроп') }}: ₴ <span class="inline-block drop">{{ $product->getPrice('drop_uah', false, $bySeries) }}</span>
  </p>
  <p class="{{ $class['opt_price'] }}">
    {{ __('Опт') }}: $ <span class="inline-block opt">{{ $product->getPrice('opt_price', false, $bySeries) }}</span>
  </p>
  <p class="{{ $class['opt_vip_usd'] }}">
    VIP: $ <span class="inline-block vip">{{ $product->getPrice('opt_vip_usd', false, $bySeries) }}</span>
  </p>
  <p class="{{ $class['opt_dealer_usd'] }}">
    {{ __('Дилер') }}: $ <span class="inline-block diler">{{ $product->getPrice('opt_dealer_usd', false, $bySeries) }}</span>
  </p>
</div>