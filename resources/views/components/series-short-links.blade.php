@if ($productSeries)
  <div class="fast-category-link clearfix">
    <div class="wrapper">
      @foreach ($layoutCategories as $layoutCategory)
        @if ($seriesCounters->get($layoutCategory->id))
          <a href="{{ custom_route('products_series', [$productSeries->url ?? '']) . '/' . $layoutCategory->name }}">
            {{ $layoutCategory->title }} ({{ $seriesCounters->get($layoutCategory->id) }})
          </a>
        @endif
      @endforeach

      <a href="{{ custom_route('products_series', [$productSeries->url]) }}">{{ __('Вce товары серии') }} ({{ $productSeries->products_count }})</a>
    </div>
  </div>
@endif