<div>
  @foreach ($products as $product)
    <a href="{{ $product['url'] }}" class="header-search-product clearfix">
      <span class="left-block">
        <img src="{{ $product['image'] }}" />
      </span>
      <span class="right-block">
        <span class="header-search-name">{{ $product['title'] }}</span>
        <span class="header-search-price">
          <b>$ {{ $product['price'] }}</b>
          ₴ {{ $product['price_uah'] }}
        </span>
      </span>
    </a>
  @endforeach
</div>