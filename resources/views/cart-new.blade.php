@extends('layout')

@section('content')

  @if (\Illuminate\Support\Facades\Session::has('message'))
    <div class="bg-white-red p-10 text-center font-weight-bold">{{ \Illuminate\Support\Facades\Session::get('message') }}</div>
  @endif

  @if ($items && $items->count())
    <!-- Cart Sorting Block -->
    <div class="cart-sorting-block wrapper clearfix">

      <div class="flex justify-between" id="panel-block">
        <div class="cart-row" id="promo-block" style="width: 40%; margin-top: 7px;">
          <div class="flex align-center">
            <label>{{ __('Промокод') }}</label>
            <input type="text" id="promocode" class="input" value="" style="width: 150px" maxlength="50" />
            <input type="button" id="apply" value="OK" />
            <input type="image" id="loader" class="hidden" src="/template/images/ajax-loader.gif" />
          </div>
        </div>

        <div class="cart-sorting-buttons">
          <div class="sorting-buttons-wrapper">

            <a ref="#" id="btn-unfold-list" data-show="false" class="btn-open hidden">{{ __('Расскрыть все') }}</a>

            {{--    Тип цены    --}}
            @if (count($priceList) > 0)
              <span id="price-type">
            <img class="cursor_p m-5 page-button tooltip-event" src="/template/images/hint_icon.svg"
                 title="{{ __('Выберите нужный Вам тип цен') }}"
                 id="inform-ico"
                 style="vertical-align: sub;z-index: 999">

            <select class="select-block select-price-type" id="price-type">
              @foreach ($priceList as $val => $text)
                <option value="{{ $val }}" {{ ($priceTypeDefault == $val) ? 'selected' : null }}>{{ $text }}</option>
              @endforeach
            </select>
          </span>
            @endif

            <div class="btn-sorting btn-outline-orange cursor_p" id="btn-group-control">
              {{ __('Групповое управление количеством') }}
              <span class="hint-popup">
              {{ __('Нажмите на строки, которые вы хотите связать, затем введите необходимое количество товара в любую из строк, он применится ко всем сразу') }}
          </span>
            </div>

            <div id="group-color" class="btn-sorting btn-outline-orange cursor_p">{{ __('Собрать цвета') }}</div>
          </div>


          <div class="cart-sorting-wrapper">
            <div class="cart-sorting-content">
              <p>{{ __('Сортировка') }}:</p>
              <span id="sorted-text">{{ __('По добавлению') }}</span>
            </div>
            <div class="cart-sorting-popup">
              <div data-attr-name="sort-default">{{ __('По добавлению') }}</div>
              <div data-attr-name="sort-title">{{ __('По названию') }}</div>
              <div data-attr-name="sort-price">{{ __('По цене') }}</div>
              <div data-attr-name="sort-model">{{ __('По типу') }}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- #End Cart Sorting Block -->

    <!-- Cart Content Block -->
    <div class="cart-content-block">
      <div class="cart-table-title _cart-table-title">
        <div class="wrapper clearfix">
            <span class="cart-col-1 sorted" data-attr-name="sort-title" data-sort="asc">
              {{ __('Название товара') }}
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M5.5 5.71L2.856 8.354a.5.5 0 0 1-.708-.707L5.617 4.18A.499.499 0 0 1 6 4h.002a.5.5 0 0 1 .354.147l.01.01l3.49 3.49a.5.5 0 0 1-.707.707l-2.65-2.649V15.5a.5.5 0 0 1-1 0V5.71z"/><path d="M14.504 14.29l2.645-2.644a.5.5 0 0 1 .707.707l-3.469 3.468a.5.5 0 0 1-.383.179h-.001a.5.5 0 0 1-.355-.147l-.01-.01l-3.49-3.49a.5.5 0 1 1 .707-.707l2.65 2.649V4.5a.5.5 0 0 1 1 0v9.79z"/></svg>
            </span>
          <div class="cart-col-2 sorted" data-attr-name="sort-brand" data-sort="asc">
            {{ __('Бренд') }}
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M5.5 5.71L2.856 8.354a.5.5 0 0 1-.708-.707L5.617 4.18A.499.499 0 0 1 6 4h.002a.5.5 0 0 1 .354.147l.01.01l3.49 3.49a.5.5 0 0 1-.707.707l-2.65-2.649V15.5a.5.5 0 0 1-1 0V5.71z"/><path d="M14.504 14.29l2.645-2.644a.5.5 0 0 1 .707.707l-3.469 3.468a.5.5 0 0 1-.383.179h-.001a.5.5 0 0 1-.355-.147l-.01-.01l-3.49-3.49a.5.5 0 1 1 .707-.707l2.65 2.649V4.5a.5.5 0 0 1 1 0v9.79z"/></svg>
          </div>
          <div class="cart-col-3 sorted" data-attr-name="sort-model" data-sort="asc">
            {{ __('Модель телефона') }}
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M5.5 5.71L2.856 8.354a.5.5 0 0 1-.708-.707L5.617 4.18A.499.499 0 0 1 6 4h.002a.5.5 0 0 1 .354.147l.01.01l3.49 3.49a.5.5 0 0 1-.707.707l-2.65-2.649V15.5a.5.5 0 0 1-1 0V5.71z"/><path d="M14.504 14.29l2.645-2.644a.5.5 0 0 1 .707.707l-3.469 3.468a.5.5 0 0 1-.383.179h-.001a.5.5 0 0 1-.355-.147l-.01-.01l-3.49-3.49a.5.5 0 1 1 .707-.707l2.65 2.649V4.5a.5.5 0 0 1 1 0v9.79z"/></svg>
          </div>
          <div class="cart-col-4 sorted" data-attr-name="sort-price" data-sort="asc">
            {{ __('Цена') }}
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M5.5 5.71L2.856 8.354a.5.5 0 0 1-.708-.707L5.617 4.18A.499.499 0 0 1 6 4h.002a.5.5 0 0 1 .354.147l.01.01l3.49 3.49a.5.5 0 0 1-.707.707l-2.65-2.649V15.5a.5.5 0 0 1-1 0V5.71z"/><path d="M14.504 14.29l2.645-2.644a.5.5 0 0 1 .707.707l-3.469 3.468a.5.5 0 0 1-.383.179h-.001a.5.5 0 0 1-.355-.147l-.01-.01l-3.49-3.49a.5.5 0 1 1 .707-.707l2.65 2.649V4.5a.5.5 0 0 1 1 0v9.79z"/></svg>
          </div>
          <div class="cart-col-5 sorted" data-attr-name="sort-amount" data-sort="asc">
            {{ __('Количество') }}
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M5.5 5.71L2.856 8.354a.5.5 0 0 1-.708-.707L5.617 4.18A.499.499 0 0 1 6 4h.002a.5.5 0 0 1 .354.147l.01.01l3.49 3.49a.5.5 0 0 1-.707.707l-2.65-2.649V15.5a.5.5 0 0 1-1 0V5.71z"/><path d="M14.504 14.29l2.645-2.644a.5.5 0 0 1 .707.707l-3.469 3.468a.5.5 0 0 1-.383.179h-.001a.5.5 0 0 1-.355-.147l-.01-.01l-3.49-3.49a.5.5 0 1 1 .707-.707l2.65 2.649V4.5a.5.5 0 0 1 1 0v9.79z"/></svg>
          </div>
          <div class="cart-col-6 sorted" data-attr-name="sort-cost" data-sort="asc">
            {{ __('Стоимость') }}
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M5.5 5.71L2.856 8.354a.5.5 0 0 1-.708-.707L5.617 4.18A.499.499 0 0 1 6 4h.002a.5.5 0 0 1 .354.147l.01.01l3.49 3.49a.5.5 0 0 1-.707.707l-2.65-2.649V15.5a.5.5 0 0 1-1 0V5.71z"/><path d="M14.504 14.29l2.645-2.644a.5.5 0 0 1 .707.707l-3.469 3.468a.5.5 0 0 1-.383.179h-.001a.5.5 0 0 1-.355-.147l-.01-.01l-3.49-3.49a.5.5 0 1 1 .707-.707l2.65 2.649V4.5a.5.5 0 0 1 1 0v9.79z"/></svg>
          </div>
          <div class="cart-col-7 sorted" data-attr-name="sort-status" data-sort="asc">
            {{ __('Статус') }}
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M5.5 5.71L2.856 8.354a.5.5 0 0 1-.708-.707L5.617 4.18A.499.499 0 0 1 6 4h.002a.5.5 0 0 1 .354.147l.01.01l3.49 3.49a.5.5 0 0 1-.707.707l-2.65-2.649V15.5a.5.5 0 0 1-1 0V5.71z"/><path d="M14.504 14.29l2.645-2.644a.5.5 0 0 1 .707.707l-3.469 3.468a.5.5 0 0 1-.383.179h-.001a.5.5 0 0 1-.355-.147l-.01-.01l-3.49-3.49a.5.5 0 1 1 .707-.707l2.65 2.649V4.5a.5.5 0 0 1 1 0v9.79z"/></svg>
          </div>
          <div class="cart-col-8">{{ __('Удалить') }}</div>
        </div>
      </div>

      @php $num = 0; @endphp
      @foreach ($products as $product)
        @php $num++; @endphp
        <div class="cart-product-block"
             data-row-id="{{ $product->rowId }}"
             data-sort-default="{{ $num }}"
             data-sort-title="{{ $product->title }}"
             data-sort-brand="{{ $product->brandName }}"
             data-sort-model="{{ $product->modelName }}"
             data-sort-price="{{ $product->price }}"
             data-sort-amount="{{ $product->qty }}"
             data-sort-cost="{{ $product->subtotal( $priceTypeDefault) }}"
             data-sort-status="{{ $product->status() }}"
             data-group-parent="{{ $product->parent }}"
             data-from="{{ $product->from }}"
        >
          <div class="wrapper clearfix {{ ($product->price == 0) ? 'bg-white-red' : '' }}">

            {{-- Image + title + color cart-col-1 --}}
            <div class="cart-col-1 clearfix">
              {{-- Image and label --}}
              <div class="cart-product-image left-block" style="text-align: center;">
                <a href="{{ $product->url }}"><img src="{{ $product->image }}" alt="{{ $product->title }}"></a>
                <div class="product-label-block product-label-block-small {{ $product->label }}"></div>
              </div>
              {{--// End Image and label --}}

              {{-- Title and color --}}
              <div class="cart-product-info right-block">
                <div class="cart-product-labels">
                  @if ($product->baseid) <div class="product-label-grey">#{{ $product->baseid }}</div> @endif
                  @if ($product->discountRate > 0) <div class="product-label-orange">промо -{{ $product->discountRate }}%</div> @endif
                  @if ($product->need_package) <div class="product-label-small">{{ __('техпак') }}</div> @endif
                </div>

                <a href="{{ $product->url }}" class="{{ ($product->outOfStock) ? 'cross-out' : null }}">{{ $product->title }}</a>

                {{-- Color block --}}
                @if ($product->color)
                  <div class="cart-product-color">{{ __('Цвет') }}:
                    <b @if (! $product->isPrint) class="select-color cursor_p" @endif data-id="{{ $product->parent }}" data-rowid="{{ $product->rowId }}">
                      <span style="background-color: #{{ $product->color }};" {!! $product->colorBorder ? 'class="color-white"' : '' !!}></span>
                      {{ $product->color_name }}
                    </b>
                  </div>
                @endif

                {{-- Material block --}}
                @if ($product->isPrint)
                  <div class="cart-product-color">{{ __('Материал') }}:
                    @if ($product->material === null)
                      <b><span>{{ $product->materialName }}</span></b>
                    @else
                      <select class="select-material" data-rowid="{{ $product->rowId }}">
                        @foreach ($product->material as $key => $item)
                          <option value="{{ $key }}" {{ ($product->selectedMaterial === $key) ? 'selected' : '' }}>{{ $item }}</option>
                        @endforeach
                      </select>
                    @endif

                    @if ($product->selectedMaterial === 'silicone')
                      <div style="display: inline-block">
                        <label class="checkbox {{ ($product->fullBorder == true) ? 'active' : '' }}">
                          <input type="checkbox" class="full-border"
                                 {{ ($product->fullBorder == true) ? 'checked' : '' }}
                                 data-rowid="{{ $product->rowId }}">
                          {{ __('Закрашивать борта') }}
                        </label>
                      </div>
                    @endif
                  </div>
                @endif
              </div>
              {{--// End Title and color --}}
            </div>
            {{--// End cart-col-1 --}}

            {{-- Brand cart-col-2 --}}
            <div class="cart-col-2">
              {{ $product->brandName }}
            </div>
            {{--// End cart-col-2 --}}

            {{-- Model cart-col-3 --}}
            <div class="cart-col-3">
              @if ($product->modelUrl)
                <a href="{{ $product->modelUrl }}">{{ $product->modelName }}</a>
              @else
                {{ $product->modelName }}
              @endif
            </div>
            {{--// End cart-col-3 --}}

            {{-- Price cart-col-4 --}}
            <div class="cart-col-4" style="{{ ($product->price == 0) ? 'color: red' : '' }}">
              {{--              <p>${{ $product->getPrice($priceTypeDefault, true) }} / ₴{{ $product->getPriceUAH($priceTypeDefault, true) }}</p>--}}
              <p>${{ $product->price }} / ₴{{ round($product->price * settings('dollar')) }}</p>
              @if ($product->getPrice('old_opt_price') > $product->getPrice('opt_price'))
                <p>
                  <span class="cross-out">${{ $product->getPrice('old_opt_price') }} </span>/
                  <span class="cross-out">₴{{ $product->getPriceUAH('old_opt_price') }}</span>
                </p>
              @endif
            </div>
            {{--// End cart-col-4 --}}

            {{-- Amount block cart-col-5 --}}
            <div class="cart-col-5">
              <div class="spinner-block clearfix">
                <div class="minus">-</div>
                <input type="text" class="cart-item-qty" value="{{ $product->qty }}">
                <div class="plus">+</div>
              </div>
            </div>
            {{--// End cart-col-5 --}}

            {{-- Сost block cart-col-6 --}}
            <div class="cart-col-6" style="{{ ($product->price == 0) ? 'color: red' : '' }}">
              <p><b>${{ $product->subtotal($priceTypeDefault) }} / ₴{{ $product->subtotal($priceTypeDefault, true) }}</b></p>
            </div>
            {{--// End cart-col-6 --}}

            {{-- Status block cart-col-7 --}}
            <div class="cart-col-7">
              @if ($product->qty_product > 0) <div class="product-availability-block"> {{ __('доступен') }} </div>
              @elseif ($product->when_appear) <div class="product-availability-block soon-available"> {{ __('ожидается') }} </div>
              @else <div class="product-availability-block not-available"> {{ __('нет в наличии') }} </div>@endif
            </div>
            {{--// End cart-col-7 --}}

            {{-- cart-col-8 --}}
            <div class="cart-col-8">
              <button class="cart-item-delete btn-remove"></button>
            </div>
            {{--// End cart-col-8 --}}

            {{-- cart-col-sale --}}
            <div class="cart-col-sale">
              @if ($product->getPrice('old_opt_price') > $product->getPrice('opt_price'))
                <p>
                  <span class="cross-out">${{ $product->getPrice('old_opt_price') }} </span>/
                  <span class="cross-out">₴{{ $product->getPriceUAH('old_opt_price') }}</span>
                </p>
              @endif
            </div>
            {{--// End cart-col-sale --}}
          </div>
        </div>
      @endforeach

    </div>
    <!-- #End Cart Product Block -->

    @if ($packages && $packages->count())
      <div class="gradient-block product-packaging-block">
        <div class="wrapper">
          {{--        <div class="product-list-block oneline product-small-block">--}}
          <div class="product-list-block product-list-big oneline">
            <div class="title-block">{{ __('Добавьте к заказу упаковку') }}</div>
            <div class="product-list-wrapper clearfix">
              <div class="block product-block product-big-block" style="display: none;">
                <div class="product-wrapper">
                </div>
              </div>
              @each('partials.list-product', $packages, 'product')
            </div>
            <div class="btn-show-more"></div>
          </div>
        </div>
      </div>
    @endif

    <!-- Order Block -->
    <div class="order-block">
      <div class="order-panel-block order-panel-top">
        <div class="wrapper clearfix">
          <div class="title-block-info" id="title-block-info"></div>
          <div class="order-quantity-block before-title-block-info">
            <p class="cart-count-sku">
              <b>{{ $items->count() }}</b>
              <span>{{ word_declension(__('Наименовани'), $items->count()) }}
                </span>
            </p>
          </div>
          <div class="order-quantity-block">
            <p class="cart-count">
              <b>{{ $cart->count() }}</b>
              <span>{{ word_declension(__('Единиц'), $cart->count()) . ' ' . __('товара') }}</span>
            </p>
          </div>
          <div class="order-total-sum min-order mini-prices order-quantity-block text-orange font-weight-bold">
            <p>
              <b>$</b>
              <i class="font-style-normal">100</i>
              <span>{{ __('Мин. заказ') }}</span>
            </p>
          </div>
          <div class="order-total-sum order-quantity-block btn-green text-white">
            <p>
              <b>$</b>
              <i class="cart-total total-sum">{{ $cart->total($priceTypeDefault) }}</i>
              <span>{{ __('Сумма') }}</span>
            </p>
          </div>
          {{--            <button class="btn-checkout btn-orange square-corner cart-go-to-order">{{ __('Перейти к оформлению') }}</button>--}}
          <button class="btn-checkout btn-orange square-corner cart-go-to-order">{{ __('К оформлению') }}</button>
        </div>
      </div>
      <!-- Order Form Wrapper -->
      <div class="order-form-wrapper wrapper clearfix">
        <form action="" method="post" class="order-form-main">
          <!-- Контактные данные -->
          <div class="order-content-block">
            <div class="title-block"><b>{{ __('Контактные данные') }}</b></div>
            <div class="order-form">
              <label class="checkbox">
                <input type="checkbox" name="dropshipping" value="1">{{ __('Дропшиппинг заказ') }}
              </label>
              <div class="input-field required">
                <input type="text" name="lastname" placeholder="{{ __('Фамилия') }}" value="{{ $lastOrder ? $lastOrder->lastname : '' }}" data-preset="{{ $lastOrder ? $lastOrder->lastname : '' }}">
              </div>
              <div class="input-field required">
                <input type="text" name="firstname" placeholder="{{ __('Имя') }}" value="{{ $lastOrder ? $lastOrder->firstname : '' }}" data-preset="{{ $lastOrder ? $lastOrder->firstname : '' }}">
              </div>
              <div class="input-field required">
                <input type="text" name="phone" placeholder="{{ __('Телефон') }}" value="{{ $lastOrder ? $lastOrder->phone : '' }}" data-preset="{{ $lastOrder ? $lastOrder->phone : '' }}">
              </div>
            </div>
          </div>

          <!-- Доставка -->
          <div class="order-delivery-block order-content-block">
            <div class="title-block"><b>{{ __('Доставка') }}</b></div>
            <div class="order-form">
              <!-- Способ доставки -->
              <input type="hidden" name="delivery_type_id" value="{{ $lastOrder && $lastOrder->delivery_type_id ? $lastOrder->delivery_type_id : 1 }}" data-preset="{{ $lastOrder && $lastOrder->delivery_type_id ? $lastOrder->delivery_type_id : '' }}">

              <button type="button" class="btn-grey post-office-icon delivery-type-btn {{ ! $lastOrder || ! $lastOrder->delivery_type_id || $lastOrder->delivery_type_id == 1 ? 'active' : '' }}" data-value="1">{{ __('Отделение Новой Почты') }}</button type="button">
              <button type="button" class="btn-grey courier-icon delivery-type-btn {{ $lastOrder && $lastOrder->delivery_type_id == 2 ? 'active' : '' }}" data-value="2">{{ __('Курьер Новой Почты') }}</button type="button">
              <button type="button" id="delivery-type-other" class="btn-grey other-icon delivery-type-btn {{ $lastOrder && $lastOrder->delivery_type_id == 3 ? 'active' : '' }}" data-value="3" data-show="0">{{ __('Другое') }}</button type="button">

              <!-- Комментарий к способу доставки -->
              <div class="order-delivery-details input-field" id="delivery_comment" data-delivery-types="3">
                <input type="text" maxlength="255" name="delivery_comment" placeholder="{{ __('Комментарий к доставке') }}">
              </div>

              <!-- Город -->
              <div class="select-block custom-select city-select">
                <p>{{ __('Город') }}</p>
                <input type="hidden" name="city_id" class="custom-select-input" value="{{ $lastOrder && $lastOrder->city_id ? $lastOrder->city_id : '' }}" data-preset="{{ $lastOrder && $lastOrder->city_id ? $lastOrder->city_id : '' }}">
                <div class="selected-block custom-select-active-value">{{ $lastOrder && $lastOrder->city_id && $lastOrder->city ? $lastOrder->city : __('Выберите город') }}</div>
                <div class="select-list-block custom-select-list">
                  <div class="select-list-search custom-select-search"><input autocomplete="off" id="select_search" type="text" placeholder="{{ __('Поиск') }}"></div>
                  {{--                            @foreach ($cities as $cityId => $cityName)--}}
                  {{--                            <div data-value="{{ $cityId }}">{{ $cityName }}</div>--}}
                  {{--                            @endforeach--}}
                </div>
              </div>

              <div class="fast-city-block order-city-presets">
                <a href="#" class="btn-orange-small">{{ __('Киев') }}</a>
                <a href="#" class="btn-orange-small">{{ __('Харьков') }}</a>
                <a href="#" class="btn-orange-small">{{ __('Одесса') }}</a>
                <a href="#" class="btn-orange-small">{{ __('Днепр') }}</a>
              </div>

              <!-- Отделение НП -->
              <div class="order-delivery-details select-block custom-select warehouse-select" data-delivery-types="1">
                <p>{{ __('Отделение') }}</p>
                <input type="hidden" name="warehouse_id" class="custom-select-input" value="{{ $lastOrder ? $lastOrder->warehouse_id : '' }}" data-preset="{{ $lastOrder ? $lastOrder->warehouse_id : '' }}">
                <div class="selected-block custom-select-active-value">{{ __('Выберите отделение') }}</div>
                <div class="select-list-block custom-select-list">
                  <div class="select-list-search custom-select-search"><input type="text" placeholder="{{ __('Поиск') }}"></div>
                </div>
              </div>

              <!-- Улица (Курьер НП) -->
              <div class="order-delivery-details select-block custom-select street-select" data-delivery-types="2">
                <p>{{ __('Улица') }}</p>
                <input type="hidden" name="street_id" class="custom-select-input" value="{{ $lastOrder ? $lastOrder->street_id : '' }}" data-preset="{{ $lastOrder ? $lastOrder->phone : '' }}">
                <div class="selected-block custom-select-active-value">{{ __('Выберите улицу') }}</div>
                <div class="select-list-block custom-select-list">
                  <div class="select-list-search custom-select-search"><input type="text" placeholder="{{ __('Поиск') }}"></div>
                </div>
              </div>

              <!-- Номер дома (Курьер НП) -->
              <div class="order-delivery-details input-field street_building" data-delivery-types="2">
                <input type="text" name="street_building" placeholder="{{ __('Дом') }}" value="{{ $lastOrder ? $lastOrder->street_building : '' }}" data-preset="{{ $lastOrder ? $lastOrder->street_building : '' }}">
              </div>

              <!-- Полный адрес (Другое) -->
              <div class="order-delivery-details input-field address" data-delivery-types="3">
                <input type="text" name="address" placeholder="{{ __('Адрес') }}" value="{{ $lastOrder ? $lastOrder->address : '' }}" data-preset="{{ $lastOrder ? $lastOrder->address : '' }}">
              </div>

              <!-- Плательщик за упаковку -->
              <div id="package_payer" class="order-radio-block radio-block order-dropshipping-field hidden" data-show="1">
                <p>{{ __('Конверт с воздушной подушкой 2 грн за счет') }}:</p>
                <label class="radio active">
                  <input type="radio" name="package_payer" value="1" checked>{{ __('Мой') }}
                </label>
                <label class="radio">
                  <input type="radio" name="package_payer" value="2">{{ __('Покупателя') }}
                </label>
              </div>

              <!-- Комментарий -->
              <div class="input-field">
                {{--                        <input type="text" maxlength="255" name="comment" placeholder="{{ __('Комментарий') }}" value="{{ $lastOrder ? $lastOrder->comment : '' }}" data-preset="{{ $lastOrder ? $lastOrder->comment : '' }}">--}}
                <input type="text" maxlength="255" name="comment" placeholder="{{ __('Комментарий') }}" value="" data-preset="">
              </div>
            </div>
          </div>

          <!-- Оплата -->
          <div class="order-payment-block order-content-block">
            <div class="order-dropshipping-field hidden" data-show="1">
              <div class="title-block"><b>{{ __('Оплата') }}</b></div>
              <div class="order-form">
                <div class="order-radio-block radio-block">
                  <label class="radio">
                    <input type="radio" name="payment_type_id" value="1" checked>{{ __('Наложенный платеж') }}
                  </label>
                  <label class="radio">
                    <input type="radio" name="payment_type_id" value="2">{{ __('Предоплата') }}
                  </label>
                </div>

                <div class="input-field required order-dropshipping-cod-fields">
                  <input type="number" name="dropshipping_price" placeholder="{{ __('Сумма') }}">
                </div>

                <div class="input-field order-delivery-price hidden">
                  <b>{{ __('Доставка') }}: <span></span></b>
                </div>
              </div>
            </div>
          </div>
        </form>

        @guest
          <div class="order-form-overlap">
            <div>
              {{ __('Для оформления заказа') }}
              <a href="{{ custom_route('login', ['return_url' => '/cart']) }}">{{ __('войдите на сайт') }}</a>
              {{ __('или') }}
              <a href="{{ custom_route('register', ['return_url' => '/cart']) }}">{{ __('пройдите регистрацию') }}</a>
            </div>
          </div>
        @endguest
      </div>

      <div class="order-panel-block">
        <div class="wrapper clearfix">
          <div class="order-total-sum order-quantity-block">
            <p>
              <b>$</b> <i class="cart-total">{{ $cart->total($priceTypeDefault) }}</i>
              <span>{{ __('Сумма') }}</span>
            </p>
          </div>
          <button class="btn-checkout btn-orange square-corner cart-submit-order-form {{ ($cart->total($priceTypeDefault) == 0) ? 'pointer-events' : '' }}" >
            <svg class="hidden" style="width: 50px; vertical-align: middle;" version="1.1"
                 id="preload" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                  <path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                    <animateTransform
                      attributeName="transform"
                      attributeType="XML"
                      type="rotate"
                      dur="1s"
                      from="0 50 50"
                      to="360 50 50"
                      repeatCount="indefinite" />
                  </path>
              </svg>
            {{ __('Оформить заказ') }}
          </button>
        </div>
      </div>
    </div>

    @if (isset($saleProducts) && $saleProducts->count())
      <div class="gradient-block cart-cross-products">
        <div class="wrapper">
          <div class="product-list-block oneline">
            <div class="title-block">{{ __('Также интересуются') }}</div>
            <div class="product-list-wrapper clearfix">

              @each('partials.list-product', $saleProducts, 'product')

            </div>
            <div class="btn-show-more"></div>
          </div>
        </div>
      </div>
    @endif

  @else
    <div class="title-block text-center cart-empty-message">{{ __('Ваша корзина пуста') }}</div>
  @endif

  <div class="modal" id="change-color-modal">
    <div class="modal-dialog-centered">
      <div class="modal-content" style="width: 330px">

        <div class="modal-header">
          <div class="modal-title">{{ __('Выберите новый цвет') }}</div>
          <button type="button" class="close-modal"></button>
        </div>

        <div class="modal-body" style="height: 300px">
          <div class="modal-wrapper flex flex-direction-row flex-wrap justify-center">

          </div>
        </div>

      </div>
    </div>
  </div>

  <script type="text/template" id="product-template">
    <div class="color-item mb-10 box-shadow-hover p-5 cursor_p" data-id="<% this.id %>">
      <img class="img-preview mb-10" src="<% this.img %>" />
      <div class="cart-product-color">
        <b>$<% this.opt_price %> / ₴<% this.price_uah %></b>
      </div>
    </div>
  </script>

@endsection

@push('footer-scripts')
  <script>
    var isCartPage = true;

    $(function() {
      $.ajax({
        url: 'v1/cities',
        dataType: 'json',
        // data: {search: search},
        data: {search: ''},
        success: function(data) {
          const selectBlock = $('.city-select');
          let selectList = selectBlock.find('.custom-select-list');
          selectList.children(':not(.custom-select-search)').remove();

          for (let i in data) {
            const option = $('<div>').data('value', data[i].id).html(data[i].name_ru);
            selectList.append(option);
          }
        }
      });
    });
  </script>
  <script src="/template/js/dist/cart.min.js?v=1.0.23"></script>
  <script src="/template/js/dist/order.min.js?v=1.0.6"></script>
@endpush
