@extends('layout')

@section('content')
<div class="static-page">
    <div class="wrapper">
        <div class="title-block text-center">{{ __('Спасибо за Ваш заказ!') }}</div>

        @if ($shortageProducts->count())

        <br><br><br>
        <p class="text-red">{{ __('Вычерк (нехватка) по Вашему заказу') }}:</p>
        <br>

        <div class="cart-content-block order-products">
            <div class="cart-table-title cart-product-block">
                <div class="wrapper clearfix">
                    <div class="cart-col-1">{{ __('Название товара') }}</div>
                    <div class="cart-col-4">{{ __('Цена') }}</div>
                    <div class="cart-col-4">{{ __('Вычерк') }}</div>
                    <div class="cart-col-6">{{ __('Стоимость') }}</div>
                </div>
            </div>

            @foreach ($shortageProducts as $item)
            <div class="cart-product-block">
                <div class="wrapper clearfix">
                    <div class="cart-col-1 clearfix">
                        <div class="cart-product-image left-block">
                            <a href="{{ custom_route('product', [$item->product->url, $item->product->id]) }}">
                                <img src="{{ $item->product->img ? config('custom.content_server') . $item->product->img : '/template/images/nogimage.jpg' }}" alt="{{ $item->product->title }}">
                            </a>
                        </div>
                        <div class="cart-product-info right-block">
                            <div class="cart-product-labels">
                                <div class="product-label-grey">#{{ ltrim(optional($item->product->parentProduct)->baseid, '0') }}</div>
                            </div>
                            <a href="{{ custom_route('product', [$item->product->url, $item->product->id]) }}">{{ $item->product->title }}</a>
                            <div class="cart-product-color">{{ __('Цвет') }}: 
                                <b>
                                    <span style="background-color: #{{ $item->product->color }};" {!! $item->product->color == 'ffffff' ? 'class="active"' : '' !!}></span>{{ $item->product->color_name }}
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="cart-col-4">
                        <p>${{ $item->price }} / ₴{{ round($item->price * settings('dollar')) }}</p>
                    </div>
                    <div class="cart-col-4">
                        <p>{{ $item->qty_shortage . ' ' . __('из') . ' ' . $item->count}}</p>
                    </div>
                    <div class="cart-col-6">
                        <p><b>${{ round($item->price * $item->qty_shortage, 2) }} / ₴{{ round($item->price * $item->qty_shortage * settings('dollar')) }}</b></p>
                    </div>
                </div>
            </div>
            @endforeach

        </div>

        <br><br>
        <p>
            {{ __('Мы принимаем полный заказ от Вас, чтобы узнать Ваши потребности и настроить снабжение под них') }}.<br>
            {{ __('Стараемся стать Вашим самым надежным поставщиком') }}.<br>
            - {{ __('Спасибо за сотрудничество, команда ITsellOpt') }}
        </p>

        @endif

    </div>
</div>
@endsection

@push('footer-scripts')
<script>
    fbq('track', 'SubmitApplication');
</script>
@endpush