@extends('layout')

@section('content')
<div class="static-page">
    <div class="wrapper clearfix">
        <div class="static-content-block right-block">
            <div class="mobile-static-active-tab" id="mobile-static-active-tab">{{ $page->title }}</div>

            <div class="page-content">
              <h1 class="title-block">{{ $page->title }}</h1>
              {!! $page->content !!}
            </div>
        </div>

       @include('partials.static-pages-sidebar', ['activePage' => $page->name])
    </div>
</div>
@endsection

@push('footer-scripts')
  <script>
    $(function() {
      $('#mobile-static-active-tab').click(function() {
        const el = $('#mobile-static-active-tab');
        $('.page-content').toggle();
        if ($('.page-content').is(":visible")) {
          el.removeClass('rotate-0');
        } else {
          el.addClass('rotate-0');
        }
      });
    });
  </script>
@endpush