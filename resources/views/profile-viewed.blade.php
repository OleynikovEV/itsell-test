@extends('layout')

@section('content')

  <div class="profile-block-wrapper wrapper clearfix">

    <div class="profile-content-block right-block">
      <div class="profile-active-tab">{{ __('Недавно просматривали') }}</div>

      <div class="page">
        <div class="title-block">{{ __('Недавно просматривали') }}</div>

        @each('partials.list-product', $products, 'product')

      </div>

    </div>

    @include('partials.profile-submenu')

  </div>

@endsection


@push('footer-scripts')
  <script>
    $(function() {
      $('.profile-active-tab').click(function() {
        $('.page').toggle();
      });
    });
  </script>
@endpush