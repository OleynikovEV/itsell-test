@extends('layout')

@section('content')
  <div class="model-filters">
    <x-model-panel-component :models="$models"></x-model-panel-component>
  </div>

  <div class="gradient-block">
    <div class="wrapper">

      {{--   Контент блок   --}}
      <div class="category-content-block clearfix">

        {{--   Фильтры   --}}
        <div class="category-filters-block left-block">
          <div class="category-filters-wrapper">
            <div id="filter-categories">
              <x-filter-category-component :categories="$categories"></x-filter-category-component>
            </div>

            <div id="filter-price">
              <x-filter-prices-component :prices="$prices"></x-filter-prices-component>
            </div>

            <div id="filters">
              <x-filters-panel-component :filters="$filters"></x-filters-panel-component>
            </div>

            <div id="filter-colors">

            </div>

            <div id="filter-brands">
              <x-filter-brands-component :brands="$brands"></x-filter-brands-component>
            </div>

            <div class="mobile-btn-close"></div>
          </div>
        </div>

        {{--    Отображение карточки товара    --}}
        <x-product-card-component :productIds="$productIds"></x-product-card-component>
      </div>

    </div>
  </div>

  {{-- прелоад --}}
  <x-preload></x-preload>
@endsection

@push('footer-scripts')
{{--  <script src="/template/js/dist/products-list.min.js?1.1.80"></script>--}}
  <script>
    const activeFilters = [];
    const baseUrl = '{{ $baseUrl }}';

    $(function() {
      filters.init();
      filtersShowMoreUI.init();
      panelModelsUi.init();

      $(window).resize(function() { panelModelsUi.updateFiltersColumns() });
    });

    const filters = {
      init: function() {
        this.eventFilterChecked();
      },

      // Events
      eventFilterChecked: function() {
        const self = this;
        $(document).on('change', '.filter-values-list input[type=checkbox]', function() {
          const params = self.getFiltersParams();
          const newUrl = self.getUrl(params);
          window.history.replaceState(null, '', newUrl);

          sendQuery.request(newUrl, params);
        });
      },

      // Getters
      // получаем новый url
      getUrl: function(filters) {
        // объединяем все в одну строку
        let result = [];
        for (let index in filters) {
          let value = filters[index].join(',');
          result.push(`${index}=${value}`);
        }

        let queryFilter = (result.length > 0) ? '/filter/' : '';
        return `${baseUrl}${queryFilter}${result.join(';')}`;
      },

      // получаем все выбранные фильтры
      getFiltersParams: function() {
        let filters = {};

        // сохраняем в массив все выбранные checkbox
        $('.filter-values-list input[type=checkbox]:checked').each(function() {
          const value = $(this).val(); // значение фильтра
          const categories = $(this).data('filter-url'); // урл родителя фильтра

          if (! filters.hasOwnProperty(categories)) {
            filters[categories] = [];
          }

          filters[categories].push(value);
        });

        return filters;
      }
    }

    const sendQuery = {
      request: function(url, data) {
        data['type'] = 'json';

        console.info('... start load ...');

        preload.show();

        $.get(url, data, {type: 'html'})
        .done(function(response) {
          // Вставка фильтров категория
          $('#filter-categories').empty().append(response.categoriesHtml);

          // Вставляемфильтр по цене
          $('#filter-price').empty().append(response.pricesHtml);

          // Вставляем дополнительные фильтры
          $('#filters').empty().append(response.filtersHtml);


          $('#filter-brands').empty().append(response.brandsHtml);

          // Вставка фильтров по моделям
          $('.model-filters').empty().append(response.modelsHtml);
          $( window ).resize();

          $('.checkbox input:checked').each(function(){
            $(this).closest('label').addClass('active');
          });

          // вставка карточек товаров
          $('.product-block').remove();
          $('.category-content-block').append(response.productsHtml);
          $('.tooltip-event').tooltip({});

          preload.hidden();

          console.info('... finish load ...');
        });
      }
    }

    const panelModelsUi = {
      init() {
        this.updateFiltersColumns(false);
        this.showAllModels();
      },

      updateFiltersColumns: function (forceUpdate) {
        // Определение количества колонок на текущем разрешении
        let totalColumns = 6;
        const totalColumnsResolutions = {
          515: 2,
          830: 3,
          1020: 4,
          1320: 5,
          1791: 6
        };

        // получаем кол-во колонок в зависимости от размера экрана
        const index = Object.keys(totalColumnsResolutions).filter( width => $(window).width() < width );
        totalColumns = totalColumnsResolutions[index];

        $('.filter-device-content').each(function(){
          if ( ! forceUpdate && $(this).data('columns') == totalColumns) return;

          var elements = $(this).find('.left-block > *');

          var totalElements = elements.filter(':visible').length;

          var perColumn = Math.ceil(totalElements / totalColumns);
          var columnsWithMoreElements = totalElements % totalColumns;

          var content = $('<div>');
          var processedElements = 0;

          for (var i = 0; i < totalColumns; i++) {
            var elementsInColumn = 0;

            var column = $('<div>').addClass('left-block');
            content.append(column);

            if (columnsWithMoreElements && columnsWithMoreElements == i) perColumn--;

            while (elementsInColumn < perColumn) {
              var element = elements[processedElements];
              if ( ! element) break;

              if ($(element).is(':visible')) elementsInColumn++;

              column.append(element);
              processedElements++;

              if (elementsInColumn == perColumn && $(element).is('.filter-device-sector')) {
                column.append(elements[processedElements++]);
              }
            }
          }

          // Если в конце списка остались скрытые элементы, которые не были распределены по колонкам - добавляем
          // их в последнюю колонку
          if (processedElements < elements.length) {
            for (; processedElements < elements.length; processedElements++) {
              column.append(elements[processedElements]);
            }
          }

          $(this).html(content.find('.left-block')).data('columns', totalColumns);
        });
      },

      showAllModels() {
        const self = this;
        // Разворачивание списка всех моделей
        if ($('.filter-subcategories label.hidden').length) $('.filter-subcategories-show-all').removeClass('hidden');

        $('.filter-subcategories-show-all').on('click', function(ev) {
          ev.preventDefault();

          $(this).toggleClass('active').text($(this).hasClass('active') ? __('Скрыть модели') : __('Все модели'))

          $('.filter-subcategory-hidden').toggleClass('hidden');
          self.showAllModels(true);
        })
      }
    }

    const filtersShowMoreUI = {
      init() {
        // Разворачивание списка фильтров при большом количестве значений
        $('body').on('click', '.filter-values-show-all', function(ev){
          ev.preventDefault();

          $(this).closest('.filter-color-block').find('.filter-value-hidden').toggleClass('hidden-mob-only');
          $(this).closest('.filter-values-list').find('.filter-value-hidden').toggleClass('hidden');

          $(this).toggleClass('active');
          $(this).text($(this).is('.active') ? __('Свернуть') : __('Показать все'));
        }).each(function(){
          if ($(this).closest('.filter-values-list').find('.filter-value-hidden input:checked').length) $(this).click();
        });
      }
    }

    const preload = {
      show() {
        $('#preload').removeClass('hidden');
        $('body').addClass('block-overflow--hidden');
      },
      hidden() {
        $('#preload').addClass('hidden');
        $('body').removeClass('block-overflow--hidden');
      }
    }
  </script>
@endpush