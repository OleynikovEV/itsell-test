@extends('layout')

@section('content')
<div class="wrapper">
    <div class="title-block">{{ $h1Text }}</div>

    <div class="tiles-list clearfix">
        @foreach ($brands as $brand)
        <div class="tile-block">
            <div class="tile-wrapper">
                <div class="tile-image">
                    <a href="{{ custom_route('brand', [$brand->name]) }}">
                        <img src="{{ $brand->logo ? config('custom.content_server') . $brand->logo : '/template/images/noimage.jpg' }}" alt="{{ $brand->title }}">
                    </a>
                </div>
                <div class="tile-title">
                    <a href="{{ custom_route('brand', [$brand->name]) }}">{{ $brand->title }}</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection