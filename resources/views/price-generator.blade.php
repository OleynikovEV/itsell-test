@extends('layout')

@section('content')

@push('css-head')
  <style>
    /* отсавить тут */
    .category-products-block {
      width: 75%;
    }
    .product-block.product-big-block .product-wrapper,
    .product-block.product-big-block .product-price-wrapper {
      min-height: 0!important;
    }
    .product-block.product-big-block {
      height: 390px!important;
    }
    .product-image {
      line-height: 170px!important;
      height: 200px!important;
    }
    .mobile-btn-close {
      z-index: 1;
      top: 5px;
      position: absolute;
    }
    @media (max-width: 829px) {
      .category-filters-block {
        max-width: none;
        width: 100%;
      }
      .category-products-block {
        width: 100%;
      }
    }
  </style>
@endpush
@push('footer-scripts')
    @if(getenv('APP_DEBUG') == true)
      <script type="text/javascript" src="/template/js/vendor/vue.js"></script>
    @else
      <script type="text/javascript" src="/template/js/vendor/vue.min.js"></script>
    @endif
@endpush

<div class="gradient-block">
  <div class="wrapper" id="pg" v-cloak>
    <form action="{{ route('price-generator.create') }}" method="post" class="category-content-block clearfix">
    <input type="hidden" name="action" value="{{ $action }}" />
    <input type="hidden" name="id" value="{{ isset($price->id) ? $price->id : '' }}" />

    {{--  Настройки фильтра выбора товара  --}}
    <div class="category-filters-block left-block">
      <div class="category-filters-wrapper">
        <div class="filter-block">
          <p>Фильтр товаров</p>
          <div class="type-filters">
            <label :class="{'active': tab === 1}">{{ __('Категории') }}
              <input type="radio" name="tab" value="1" v-model.number="tab" />
            </label>
            <label :class="{'active': tab === 4}">{{ __('Бренды') }}
              <input type="radio" name="tab" value="4" v-model.number="tab" />
            </label>
            <label :class="{'active': tab === 2}">{{ __('Серии') }}
              <input type="radio" name="tab" value="2" v-model.number="tab" />
            </label>
            <label :class="{'active': tab === 5}">itsPrint
              <input type="radio" name="tab" value="5" v-model.number="tab" />
            </label>
            <label :class="{'active': tab === 7}">itsPrint {{ __('тематика') }}
              <input type="radio" name="tab" value="7" v-model.number="tab" />
            </label>
            <label :class="{'active': tab === 6}">{{ __('Гидрогелевые пленки SKLO') }}
              <input type="radio" name="tab" value="6" v-model.number="tab" />
            </label>
            <div class="mobile-btn-close" @click="showFilter(false)"></div>
          </div>
          <div>
            <div v-show="tab === 1">
              <div class="menu-ul-title-block">{{ __('Вертикальное меню (Каталог товаров)') }}</div>
              <tree :data="categories" :name="'categories[]'"></tree>
              <div class="menu-ul-title-block">{{ __('Горизонтальное меню (Модель телефона)') }}</div>
              <tree :data="models" :name="'categories[]'"></tree>
            </div>
            <div v-show="tab === 4">
              <ul>
                <li v-for="item in brands">
                  <label :class="[{'active': item.selected}, 'checkbox']">
                    <input type="checkbox" :value="item.id" name="brands[]" v-model="item.selected" />
                    @{{ item.title }}
                  </label>
                </li>
              </ul>
            </div>
            <div v-show="tab === 2">
              <ul>
                <li v-for="item in series">
                  <label :class="[{'active': item.selected}, 'checkbox']">
                    <input type="checkbox" :value="item.id" name="series[]" v-model="item.selected" />
                    @{{ item.name }}
                  </label>
                </li>
              </ul>
            </div>
            <div v-show="tab === 5">
              <div class="menu-ul-title-block">{{ __('Горизонтальное меню (Модель телефона)') }}</div>
              <tree :data="iPrintCategories" :name="'categories[]'"></tree>
            </div>
            <div v-show="tab === 7">
              <div class="menu-ul-title-block">{{ __('Тематика') }}</div>
              <ul>
                <li v-for="item in subjectMatters">
                  <label :class="[{'active': item.selected}, 'checkbox']">
                    <input type="checkbox" :value="item.id" name="subjectMatters[]" v-model="item.selected" />
                    @{{ item.name }}
                  </label>
                </li>
              </ul>
            </div>
            <div v-show="tab === 6">
              <div class="menu-ul-title-block">{{ __('Горизонтальное меню (Модель телефона)') }}</div>
              <tree :data="models" :name="'categories[]'"></tree>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{--  Настройки фильтра товара и настройки формирования прайса  --}}
    <div class="mobile-filters-counter-wrapper">
      <div class="mobile-filters-counter" @click="showFilter(true)">Фильтр товаров</div>
    </div>

    <div class="category-products-block left-block">
      <div>
        <p>
          <input type="text" name="name" minlength="5" class="input" style="width: 50%"
                 value="{{ isset($price->price_name) ? $price->price_name : '' }}" placeholder="{{ __('Введите название прайса') }}" required />
        </p>
        <p class="select-block">
          <label>{{ __('Уровень вложенности прайс листа') }}</label>
          <select name="category_type" v-model="category_type">
            <option value="1">{{ __('Бренд Гаджета + Модель + Категория') }}</option>
            <option value="2">{{ __('Категория + Бренд Гаджета + Модель') }}</option>
            <option value="3">{{ __('Категория + Бренд Гаджета') }}</option>
          </select>
        </p>
        <p class="select-block">
          <label>{{ __('Формат файла') }}
            <select name="format" v-model="format">
              <option value="xml">xml (yml)</option>
              <option value="csv">csv</option>
            </select>
          </label>
        </p>
        <p>
          <label>{{ __('Тип цены') }}
            <select name="price_type" v-model="price_type" @change="onChangePriceType()">
              <option v-for="(item, key) in prices_type" :value="key">@{{ item.name }}</option>
            </select>
          </label>
          <label>{{ __('Валюта') }}
            <select name="currency" v-model="currency" disabled>
              <option value="uah">UAH</option>
              <option value="usd">USD</option>
            </select>
          </label>
        </p>
        <p class="select-block">
          <label>
            <input type="checkbox" name="main_photo" value="1" v-model="main_photo" > <b>{{ __('Выгружать основное фото') }}</b>
          </label>
        </p>
        <p class="select-block">
          <span><b>{{ __('Фильтр товаров') }}:</b></span>
          <label>
              <input type="checkbox" name="finish" value="1" v-model="finish" >+ {{ __('Заканчивается') }}
          </label>
          <label>
            <input type="checkbox" name="sale" value="1" v-model="sale" >+ {{ __('Уценка') }}
          </label>
          <label>
            <input type="checkbox" name="notAvailable" value="1" v-model="notAvailable" >+ {{ __('Нет в наличии') }}
          </label>
        </p>
        <p class="select-block" >
          <button type="submit" :class="['btn btn-green', {'pointer-events': !showBtn}]">Сохранить шаблон</button>
          <button type="button" :class="['btn btn-warning', {'pointer-events': !showBtn}]" @click="showResult(null)">Проверить результат</button>
        </p>
        <p class="select-block">
          <span v-if="total > 0"><b>Отобрано позиций @{{ total }}</b></span>
          <span v-if="preload"><img src="/template/images/preload-bar.svg" /></span>
        <p>
      </div>
      {{--  Список продуктов  --}}
      <div class="products-list">
        <template v-for="product in products">
          <div class="block product-block product-big-block">
            <div class="product-wrapper">
              <div class="product-image"><img :src="`${pathToImg}/${product.img}`" /></div>
              <div class="product-code clearfix">
                <p>Цветов @{{ JSON.parse(product.group_products).length }}</p>
                <a :href="`/products/${product.url}/${product.id}`" target="_blank">#@{{ product.baseid.replace(/^0+/, '') }}</a>
              </div>
              <div class="product-name">
                <a :href="`/products/${product.url}/${product.id}`" target="_blank">@{{ product.title }}</a>
              </div>
              <div class="product-price">
                <div class="product-price-wrapper">
                  <p>₴ @{{ product.price }} - РРЦ</p>
                  <p>₴ @{{ JSON.parse(product.price_types).drop_uah }} - Дропшип</p>
                </div>
              </div>
            </div>
          </div>
        </template>
      </div>
    </div>
  </form>

    <div v-if="pagination.next !== null || pagination.prev !== null" class="pagination-block">
      <ul class="pagination">
        <li v-if="pagination.prev" @click="showResult(pagination.prev)" class="page-item previous"></li>
        <li v-if="pagination.next" @click="showResult(pagination.next)"class="page-item next"></li>
      </ul>
      {{--          <span v-if="pagination.prev" @click="showResult(pagination.first)">&laquo;&laquo; First</span>--}}
      {{--          <span v-if="pagination.prev" @click="showResult(pagination.prev)">&#8249; Prev</span>--}}
      {{--          <span v-if="pagination.next" @click="showResult(pagination.next)">Next &#8250;</span>--}}
      {{--          <span v-if="pagination.next" @click="showResult(pagination.last)">Last &raquo;&raquo;</span>--}}
    </div>
  </div>
</div>
@endsection

@push('footer-scripts')
  <script>
    'use strict'

    Vue.component('tree', {
      props: ['data', 'name'],
      template: `
        <ul class="ul-category-list">
          <li v-for="(item, index) in data">
            <div>
              <label :class="[{'active': item.item.selected}, 'checkbox']">
                <input type="checkbox"
                       :value="item.item.id"
                       v-model="item.item.selected"
                       @click="selectParent(index)"
                />
                @{{ item.item.title }}
              </label>
            </div>
            <div class="open-tree pointer" v-if="!item.item.opened" @click="showChildren(index, true)">+</div>
            <div class="open-tree pointer" v-else @click="showChildren(index, false)">-</div>

            <ul class="ul-style-type-none"
                v-show="item.hasOwnProperty('children') && item.item.opened">
              <li v-for="child in item.children">
                <label :class="[{'active': child.item.selected}, 'checkbox']">
                  <input type="checkbox" :value="child.item.id" :name="name" v-model="child.item.selected" />
                  @{{ child.item.title }}
                </label>
              </li>
            </ul>
          </li>
        </ul>
      `,
      methods: {
        selectParent(parentId) { // выбираем все дочерние элементы
          const selected = !this.data[parentId].item.selected;
          for (let id in this.data[parentId].children) {
            let item = this.data[parentId].children[id];
            item.item.selected = selected;
          }
        },
        showChildren(parentId, show) {
          this.data[parentId].item.opened = show;
        },
      }
    });

    const vm = new Vue({
      el: '#pg',
      data: {
        models: @json($models),
        brands: @json($brands),
        subjectMatters: @json($subjectMatters),
        categories: @json($categories),
        series: @json($series),
        prices_type: @json($prices_type),
        iPrintCategories: @json($iPrint['models']),
        urlGetProducts: @json(route('price-generator.products-list')),
        pathToImg:  @json(config('custom.content_server')),
        format: '{{ isset($price->file_type) ? $price->file_type : 'xml' }}',
        category_type: {{ isset($price->category_type) ? $price->category_type : 3 }},
        notAvailable: {{ $notAvailable }},
        sale: {{ $sale }},
        main_photo: @json($mainPhoto),
        finish: {{ $finish }},
        currency: '{{ $currency }}',
        price_type: '{{ $price_type }}',
        preload: false,
        products: [],
        pagination: [],
        total: [],
        tab: {{ isset($price->tab) ? $price->tab : 1 }},
      },
      computed: {
        showBtn() {
          return this.filterData().length > 0;
        },
      },
      methods: {
        onChangePriceType() {
          this.currency = this.prices_type[this.price_type].currency;
        },
        showFilter(show) {
          if (show) {
            $('.mobile-btn-filter').hide();
            $('.mobile-btn-close').show();
            $('.category-filters-block').css({'right': '0', 'left': '0'});
          } else {
            $('.mobile-btn-close').hide();
            $('.mobile-btn-filter').show();
            $('.category-filters-block').css({'left': '-100%'});
          }
        },
        showResult(url) {
          const self = this;
          const ids = this.filterData();
          self.preload = true;

          if (url === null) {
            url = this.urlGetProducts;
          }

          $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
              ids: ids,
              format: self.format,
              finish: self.finish,
              notAvailable: self.notAvailable,
              sale: self.sale,
              soon: self.soon,
              tab: self.tab,
            },
            success: function(result) {
              self.products = [];
              self.products = result.data;
              self.pagination.next = result.next_page_url;
              self.pagination.prev = result.prev_page_url;
              self.total = result.total;
              self.preload = false;
            },
            errors: function(jqXHR, textStatus) {
              console.error(textStatus);
              console.error(jqXHR);
              self.preload = false;
            }
          });
        },
        filterData() {
          switch (this.tab) {
            case 6:
            case 1:
              return this.filterByCategories(this.categories).concat(this.filterByCategories(this.models));
            case 2:
              return this.filterBySeries();
            case 4:
              return this.filterByBrands();
            case 5:
              return this.filterByCategories(this.iPrintCategories);
            case 7:
              return this.filterBySubjectMatters();
          }
        },
        filterByCategories(data) {
          let selectedElem = [];
          for (const [item, value] of Object.entries(data)) {
            for (const [id, child] of Object.entries(value.children)) {
              if (child.item.selected) {
                selectedElem.push(child.item.id);
              }
            }
          }
          return selectedElem;
        },
        filterBySeries() {
          let selectedElem = [];
          this.series.forEach(function(item) {
            if (item.hasOwnProperty('selected') && item.selected) {
              selectedElem.push(item.id);
            }
          });

          return selectedElem;
        },
        filterBySubjectMatters() {
          let selectedElem = [];
          this.subjectMatters.forEach(function(item) {
            if (item.hasOwnProperty('selected') && item.selected) {
              selectedElem.push(item.id);
            }
          });

          return selectedElem;
        },
        filterByBrands() {
          let selectedElem = [];
          this.brands.forEach(function(item) {
            if (item.hasOwnProperty('selected') && item.selected) {
              selectedElem.push(item.id);
            }
          });

          return selectedElem;
        },
      }
    });
  </script>
@endpush
