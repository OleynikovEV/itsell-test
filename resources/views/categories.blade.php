@extends('layout')

@section('content')
<div class="wrapper">
    <div class="title-block">{{ $h1Text }}</div>

    <div class="tiles-list clearfix">
        @foreach ($categories as $category)
        <div class="tile-block">
            <div class="tile-wrapper">
                <div class="tile-image">
                    <a href="{{ custom_route('category', ['others', $category->name]) }}">
                        <img src="{{ $category->image ? config('custom.content_server') . $category->image : '/template/images/noimage.jpg' }}" alt="{{ $category->title }}">
                    </a>
                </div>
                <div class="tile-title">
                    <a href="{{ custom_route('category', ['others', $category->name]) }}">{{ $category->title }}</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection