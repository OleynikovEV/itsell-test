<div id="advantages-tab-1" class="tabs-info-block">
    <div class="advantages-title-block">
        <h3>{{ __('Стандарт ширины по ассортиментной матрице.') }}</h3>
        <ul>
            <li>{{ __('Проработка ширины по всем ценовым сегментам') }}</li>
            <li>{{ __('Самый широкий ассортимент на Android') }}</li>
            <li>{{ __('11 000 SKU В наличии') }}</li>
        </ul>
    </div>
    <div class="advantages-image-block">
        <img class="lazy" data-src="/template/images/content/advantages_image_1.png" alt="advantages-image">
    </div>
</div>
<div id="advantages-tab-2" class="tabs-info-block">
    <div class="advantages-title-block">
        <h3>{{ __('Ассортимент на вашей полке до выхода модели телефона в продажу') }}</h3>
        <ul>
            <li>{{ __('Отгрузка в день заказа') }}</li>
            <li>{{ __('От $100 доставка бесплатно!') }}</li>
        </ul>
    </div>
    <div class="advantages-image-block">
        <img class="lazy" data-src="/template/images/content/advantages_image_2.png" alt="advantages-image">
    </div>
</div>
<div id="advantages-tab-3" class="tabs-info-block">
    <div class="advantages-title-block">
        <h3>{{ __('Индивидуальные условия для партнера при открытии новой торговой точки.') }}</h3>
    </div>
    <div class="advantages-image-block">
        <img class="lazy" data-src="/template/images/content/advantages_image_3.png" alt="advantages-image">
    </div>
</div>
<div id="advantages-tab-4" class="tabs-info-block clearfix">
    <div class="advantages-title-block">
        <h3>{{ __('Гибкая система ценообразования, привязанная к логике') }}</h3>
        <p>{{ __('Прозрачные правила игры. Больше берешь, платишь меньше') }}</p>
    </div>
    <div class="advantages-image-block">
        <img class="lazy" data-src="/template/images/content/advantages_image_4.png" alt="advantages-image">
    </div>
</div>
<div id="advantages-tab-5" class="tabs-info-block">
    <div class="advantages-title-block">
        <h3>{{ __('Финансовая поддержка партнеров в развитии') }}</h3>
        <p>{{ __('Отсрочки платежей') }}</p>
    </div>
    <div class="advantages-image-block">
        <img class="lazy" data-src="/template/images/content/advantages_image_5.png" alt="advantages-image">
    </div>
</div>
<div id="advantages-tab-6" class="tabs-info-block">
    <div class="advantages-title-block">
        <h3>{!! __('Гибкое подключение <br> xml ссылок с контентом') !!}</h3>
    </div>
    <div class="advantages-image-block">
        <img class="lazy" data-src="/template/images/content/advantages_image_6.png" alt="advantages-image">
    </div>
</div>
<div id="advantages-tab-7" class="tabs-info-block">
    <div class="advantages-title-block">
        <h3>{{ __('Мы выстраиваем только личные отношения, легкие и приятные для обеих сторон.') }}</h3>
        <ul>
            <li>{{ __('Прием заказов через сайт 24/7') }}</li>
            <li>{{ __('Телеграм канал - вся актуальная информация') }}</li>
        </ul>
    </div>
    <div class="advantages-image-block">
        <img class="lazy" data-src="/template/images/content/advantages_image_7.png" alt="advantages-image">
    </div>
</div>
</div>
