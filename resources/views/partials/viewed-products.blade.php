@if ($viewedProducts && $viewedProducts->count())
<div class="gradient-block increased">
    <div class="wrapper">
    <div class="product-list-block product-list-big recently-viewed-block">
        <div class="title-block">{{ __('Вы недавно просматривали') }}</div>
        <div class="product-list-wrapper clearfix">

            <div class="block" style="display: none"></div>
            @each('partials.list-product', $viewedProducts, 'product')

        </div>
        <div class="btn-show-more"></div>

        @if (false)
        <a href="#" class="btn-white btn-recently-viewed">{{ __('Перейти к недавно просмотренным') }}</a>
        @endif
    </div>
    </div>
</div>
@endif
