@if ( ! isset($includeOnlyTemplate))
  @php
    $showPrice = \Illuminate\Support\Facades\Auth::check() || !$product->show_price;
  @endphp
  <div class="block product-block product-big-block {{ ($layoutCartProducts->has($product->id) && $product->bySeries == false) ? 'product-in-cart' : '' }}">
    <div class="product-wrapper">

      <div class="product-image" data-box="{{ $product->getBoxImage() }}">
        {{-- Кнопка для добавления в список ожиданий --}}
        <button class="wish-button {{ in_array($product->id, $wishList) ? 'wish-button__selected' : null }} {{ $product->isAvailability() ? 'hidden' : null }}"
                title="{{ __('Добавить в лист ожиданий') }}"
                data-product-id="{{ $product->id }}">
          <svg width="25" height="22" xmlns="http://www.w3.org/2000/svg">
            <path d="M11.3022 2.7235L12.0004 3.4042L12.6985 2.7233C13.7846 1.664 15.276 1 16.9387 1C20.3433
                          1 23 3.7585 23 7.0376C23 8.7743 22.2631 10.3504 21.0666 11.4595L21.0516 11.4734L21.0372 11.4879L11.9982
                          20.5815L2.908 11.4364L2.8973 11.4256L2.8863 11.4152C1.7177 10.3087 1 8.7517 1 7.0376C1 3.7585 3.6567
                          1 7.0613 1C8.7244 1 10.2154 1.6639 11.3022 2.7235Z">
            </path>
          </svg>
        </button>

        @if ($product->bySeries && $product->series)
          <a href="{{ custom_route('products_series', [$product->series->url, $product->urlCategory]) }}">
        @else
          <a href="{{ custom_route('product', [$product->url, $product->id]) }}">
        @endif
          <img data-in="{{ $product->getBoxImage() }}" data-tmp="" data-out="{{ $product->getDefaultTitleImage() }}"
               src="{{ $product->getDefaultTitleImage() }}" alt="{{ $product->title }}">
        </a>

        <div class="product-label-block {{ $product->getLabel() }}"></div>
      </div>

      @if (!auth()->guest() && auth()->user()->isAdmin())
        <div>
          <a href="{{ config('custom.edit_product_url') . $product->id }}" target="_blank">Редакировать</a>
        </div>
      @endif

      <div class="product-code clearfix">
        @if ($product->series)
          <p>
            <a href="{{ custom_route('products_series', [$product->series->url]) }}">
              {{ $product->series->products_count }} {{ word_declension(__('модел'), $product->series->products_count) }}
            </a>
          </p>
        @else
        @endif

        @if (! $product->bySeries) {{--   общее кол-во серий всех моделей - оранжевый прямоугольник     --}}
          <a href="{{ custom_route('product', [$product->url, $product->id]) }}">#{{ ltrim($product->baseid, '0') }}</a>
        @else {{--   выводим серии и ссылку на серии конкретной модели    --}}
          <a href="{{ custom_route('products_series', [$product->series->url, $product->urlCategory]) }}">
            <span class="text-orange text-bold"
                  data-tooltip="{{ __('Кол-во SKU в контексте вашего запроса') }}">{{ $product->seriesCount }} {{ __('СКУ') }}</span>
          </a>
        @endif
      </div>
      {{--   название товара     --}}
      <div class="product-name">
        @if ($product->bySeries && $product->series)
          <a href="{{ custom_route('products_series', [$product->series->url, $product->urlCategory]) }}">{{ $product->title }}</a>
        @else
          <a href="{{ custom_route('product', [$product->url, $product->id]) }}">{{ $product->title }}</a>
        @endif
      </div>

    @if (!$product->isPrint())
      <!-- #Begin Color block input qty -->
        <div class="product-colors clearfix">
          @if ($product->group_products && count($product->group_products))
            <div class="mobile-more-colors">{{ count($product->group_products) }} {{ word_declension(__('цвет'), count($product->group_products)) }}<b></b></div>
            <div class="product-colors-wrapper clearfix">
              @foreach ($product->group_products as $groupProduct)
{{--                <div class="{{ $product->old_opt_price > $product->opt_price ? 'color-sale' : ($groupProduct->qty > 0 && $groupProduct->qty < 10 ? 'color-last-items' : ($groupProduct->qty <= 0 && $groupProduct->when_appear ? 'color-soon' : '')) }}">--}}
                <div class="{{ getColorBorderBottom($product, $groupProduct) }}">
                  <a href="#" style="background-color: #{{ $groupProduct->color }};" title="{{ $groupProduct->name }}" class="{{ colorWhite($groupProduct->color) }}"
                     data-image="{{ $groupProduct->image ? config('custom.content_server') . thumb_name($groupProduct->image) : '/template/images/noimage.jpg' }}"
                     {!! $groupProduct->when_appear ? 'data-when-appear="' . substr($groupProduct->when_appear, 0, 5) . '"' : '' !!}
                     data-color="{{ $groupProduct->color }}"
                     data-id="{{ $groupProduct->id }}"
                     data-parent="{{ $product->id }}"
                     data-wish="{{ in_array($groupProduct->id, $wishList) ? 'true' : 'false' }}"
                     @if ($showPrice)
                     data-price-opt-range="<b>$</b> {!! implode(' - <b>$</b> ', min_max_price($groupProduct->min_price, $groupProduct->max_price)) !!}"
                     @if (isset($groupProduct->$priceTypeSite))
                      data-price-default="{{ ($priceTypeSite == 'price')
                        ? round($groupProduct->$priceTypeSite / settings('dollar'), 2)
                        : $groupProduct->$priceTypeSite
                      }}"
                      data-price-default-uah="{{ ($priceTypeSite == 'price')
                        ? $groupProduct->$priceTypeSite
                        : round($groupProduct->$priceTypeSite * settings('dollar'))
                      }}"
                     @endif
                     data-price-opt="{{ $groupProduct->opt_price }}"
                     data-price-opt-usd="{{ implode(' - ₴ ', array_map(function($item) {return round($item * settings('dollar'));}, min_max_price($groupProduct->min_price, $groupProduct->max_price))) }}"
                     data-price-vip="{{ $groupProduct->opt_vip_usd }}"
                     data-price-dealer="{{ $groupProduct->opt_dealer_usd }}"
                     data-price-drop-uah="{{ $groupProduct->drop_uah }}"
                     data-price-ppc="{{ $groupProduct->price }}"
                     data-price-old="{{ (isset($groupProduct->old_opt_price) && $groupProduct->old_opt_price > 0) ? $groupProduct->old_opt_price : null }}"
                    @endif
                  >
                    @if ($groupProduct->when_appear)
                      <span></span>
                    @endif
                  </a>

                  @if ($groupProduct->opt_price > 0 && $showPrice && ! $product->bySeries)
                    <input type="text" class="product-color-qty"
                           data-id="{{ $groupProduct->id }}"
                           tabindex="1"
                           value="0"
                           inputmode="numeric">
                  @endif
                </div>
              @endforeach
            </div>

            @if (count($product->group_products) > 6)
              <div class="btn-more-color">{{ __('Еще') }} +{{ count($product->group_products) - 6 }}</div>
            @endif

            @if ($product->qty <= 0 && $product->when_appear)
              <div class="product-availability-block soon-available">{{ __('ожидается') }}</div>
            @endif
          @endif
        </div>
        <!--#End Color block input qty -->
    @elseif ($product->isPrint() && ! $product->bySeries)
      <!--#Begin Material block input qty -->
        <div class="product-material clearfix">
          <select class="select-block material" style="width: 100%">
            <option value="" style="display: none">{{ __('Выберите материал') }}</option>
            @foreach ($product->materialSelect() as $key => $item)
              <option value="{{ $key }}" @if( $product->countMaterial() === 1) selected @endif>{{ strtoupper($item->name_ru) }}</option>
            @endforeach
          </select>

          <div class="product-qty-block">
            <input type="text" class="input-bottom product-qty"
                   style="width: 35px; margin-left: 10px; text-align: center;"
                   data-id="{{ $product->id }}" value="0" tabindex="1" inputmode="numeric">
          </div>
        </div>

        <a href="#" class="btn-buy add-to-cart">
          <span>{{ __('В корзину') }}</span>

          @if ($layoutCartProducts->has($product->id))
            <b>{{ $layoutCartProducts->get($product->id) . ' ' . __('ед.') }}</b>
          @endif
        </a>
        <!--#End Material block input qty -->
      @endif

      <div class="product-price">
        <div class="product-price-wrapper">
          {{--    Вывод цен    --}}
          @include('elements.price-block', [
            'priceTypeSite' => $priceTypeSite,
            'product' => $product,
            'showPrice' => $showPrice,
            'sale' => ($product->old_opt_price > $product->opt_price) ? $product->old_opt_price : 0,
          ])
        </div>
      </div>

      {{--  показать кнопку "В корзину" (если это не принт, можно показать цену или это не серия) --}}
      @if (!$product->isPrint() && $showPrice && ! $product->bySeries)
        <a href="#" class="btn-buy add-to-cart">
          <span>{{ __('В корзину') }}</span>

          @if ($layoutCartProducts->has($product->id))
            <b>{{ $layoutCartProducts->get($product->id) . ' ' . __('ед.') }}</b>
          @endif
        </a>
      @endif

      @if ($product->isPrint() && $product->bySeries == false)
        <a href="#" class="product-material-btn-buy add-to-cart" >
          <span>{{ __('В корзину') }}</span>

          @if ($layoutCartProducts->has($product->id))
            <b>{{ $layoutCartProducts->get($product->id) . ' ' . __('ед.') }}</b>
          @endif
        </a>
      @endif
    </div>
  </div>
@else
  <script>
    var productsColorsLabels = {0: '', 1: 'color-sale', 2: 'color-last-items', 3: 'color-soon'};
  </script>
  <script type="text/template" id="product-template">
    <div class="block product-block product-big-block <% (this.in_cart_qty && this.bySeries == false) ? 'product-in-cart' : ''%>">
      <div class="product-wrapper">

        <div class="product-image">
          <button class="wish-button <%this.inWishList ? 'wish-button__selected' : null %> <%this.isAvailability ? 'hidden' : null %>"
                  data-product-id="<%this.id%>"
                  title="{{ __('Добавить в лист ожиданий') }}">
            <svg width="25" height="22" xmlns="http://www.w3.org/2000/svg">
              <path d="M11.3022 2.7235L12.0004 3.4042L12.6985 2.7233C13.7846 1.664 15.276 1 16.9387 1C20.3433
                      1 23 3.7585 23 7.0376C23 8.7743 22.2631 10.3504 21.0666 11.4595L21.0516 11.4734L21.0372 11.4879L11.9982
                      20.5815L2.908 11.4364L2.8973 11.4256L2.8863 11.4152C1.7177 10.3087 1 8.7517 1 7.0376C1 3.7585 3.6567
                      1 7.0613 1C8.7244 1 10.2154 1.6639 11.3022 2.7235Z">
              </path>
            </svg>
          </button>

          <a href="<%this.url%>">
            <img src="<%this.image%>" alt="<%this.title%>" data-tmp=""
                 data-in="<%this.box_image%>" data-out="<%this.default_title_image%>">
          </a>

          <%if (this.label == 4) {%>
          <div class="product-label-block label-markdown"></div>
          <%}%>

          <%if (this.label == 1) {%>
          <div class="product-label-block label-new"></div>
          <%}%>

          <%if (this.label == 2) {%>
          <div class="product-label-block label-sale"></div>
          <%}%>

          <%if (this.label == 3) {%>
          <div class="product-label-block label-soon"></div>
          <%}%>
        </div>
        <div class="product-code clearfix">
          <%if (this.series_counter) {%>
          <p>
            <a href="<%this.urlSeries%>">
              <%this.series_counter%>
            </a>
          </p>
          <%}%>

          <%if (this.bySeries === false) {%>
            <a href="<%this.url%>">#<%this.code%></a>
          <% } else {%>
            <a href="<%this.url%>"><%this.seriesCount%></a>
          <%}%>
        </div>
        <div class="product-name">
          <a href="<%this.url%>"><%this.title%></a>
        </div>
        <%if (!this.isPrint) {%>
        <div class="product-colors clearfix">
          <%if (this.group_products_counter) {%>
          <div class="mobile-more-colors"><%this.group_products_counter%><b></b></div>
          <div class="product-colors-wrapper clearfix">
            <%for (var i = 0; i < this.group_products.length; i++) {%>
            <div class="<%productsColorsLabels[this.group_products[i].label]%>">
              <a href="#" style="background-color: #<%this.group_products[i].color%>;" title="<%this.group_products[i].name%>"
                 class="<%this.group_products[i].color == 'ffffff' ? 'color-white' : ''%>"
                 data-image="<%this.group_products[i].image%>"
                 data-color="<%this.group_products[i].color%>"
                 data-id="<%this.group_products[i].id%>"
                 data-parent="<%this.id%>"
                 data-wish="<%this.group_products[i].inWishList%>"
                <%if (this.show_price) {%>
                  data-price-opt-range="$ <% this.group_products[i].opt_range.join(' - $ ') %>"
                  data-price-default="<% this.price_default %>"
                  data-price-default-uah="<% this.price_default_type_uah  %>"
                  data-price-opt="<% this.group_products[i].opt_price %>"
                  data-price-opt-usd="<% this.group_products[i].opt_usd.join(' - ₴ ') %>"
                  data-price-vip="<% this.group_products[i].opt_vip_usd %>"
                  data-price-dealer="<% this.group_products[i].opt_dealer_usd %>"
                  data-price-drop-uah="<% this.group_products[i].drop_uah %>"
                  data-price-ppc="<% this.group_products[i].price %>"
                  data-price-old="<% this.group_products[i].old_opt_price %>"
                <% } %>

              <%this.group_products[i].when_appear ? 'data-when-appear="' + this.group_products[i].when_appear + '"' : '' %>>
              <%if (this.group_products[i].when_appear) {%>
              <span></span>
              <%}%>
              </a>

              <% if(this.bySeries === false) {%>
              <input type="text"
                     class="product-color-qty <%if(this.group_products[i].opt_price === 0 || this.show_price === false || this.bySeries === true){%> hidden <%}%>"
                     data-id="<%this.group_products[i].id%>"
                     tabindex="1"
                     value=""
                     inputmode="numeric">
              <% } %>
            </div>
            <%}%>
          </div>

          <%if (this.group_products_hidden_count) {%>
          <div class="btn-more-color">{{ __('Еще') }} +<%this.group_products_hidden_count%></div>
          <%}%>

          <%if (this.when_appear) {%>
          <div class="product-availability-block soon-available">{{ __('ожидается') }}</div>
          <%}%>
          <%}%>
        </div>
        <%} else if (this.isPrint && this.bySeries === false) {%>
        <!--#Begin Material block input qty -->
        <div class="product-material clearfix">
          <select class="select-block material" style="width: 100%">
            <option value="" style="display: none">{{ __('Выберите материал') }}</option>
            <%for(let key in this.material){%>
            <option value="<%key%>"
            <%if(this.amount_material === 1){%> selected <%}%> >
            <% __(this.material[key].name_ru).toUpperCase() %></option>
            <%}%>
          </select>

          <div class="product-qty-block">
            <input type="text" class="input-bottom product-qty"
                   style="width: 35px; margin-left: 10px; text-align: center;"
                   data-id="<%this.id%>" value="0" tabindex="1" inputmode="numeric">
          </div>
        </div>

        <a href="#" class="btn-buy add-to-cart">
          <span>{{ __('В корзину') }}</span>

          <%if (this.in_cart_qty){%>
          <b><%this.in_cart_qty%> {{ __('ед.') }}</b>
          <%}%>
        </a>
        <!--#End Material block input qty -->
        <%}%>

        <div class="product-price">
          <div class="product-price-wrapper">
            @if (!($priceTypeSite === 'disable'))
            <span data-id="price-block-<%this.id%>">
              @if ($priceTypeSite)
                <p>
                  <span class="single-price">
                    <b class="symbol"><% this.symbol %></b>
                    <span class="price-default"><% this.price_default %></span>
                  </span>

                  <%if (this.showSale) {%>
                    <span class="cross-out"><span class="sale"><%if (this.sale) {%> $ <% this.sale %> <%}%></span></span>
                  <%}%>

                  <p>₴ <span class="price-default-uah"><%this.price_default_type_uah%></span></p>
                </p>

                <div class="product-price-popup">
                  <p>{{ __('РРЦ') }}: ₴<%this.prices.price.join(' - ₴')%></p>
                  <p>{{ __('Дропшиппинг') }}: ₴<%this.prices.drop_uah.join(' - ₴')%></p>
                  <p>{{ __('Опт') }}: $<%this.prices.opt_price.join(' - $')%></p>
                  <p>VIP: $<%this.prices.opt_vip_usd.join(' - $')%></p>
                  <p>{{ __('Дилер') }}: $<%this.prices.opt_dealer_usd.join(' - $')%></p>
                </div>
              @else
                <%if (this.show_price) {%>
                <p>
                  <b><%this.symbol%> <%this.prices.base.join(' - $ ')%></b>
                  <%if (this.old_price) {%>
                  <span class="cross-out"><span class="sale">$ <%this.old_price%></span></span>
                  <%}%>
                </p>
                <p>₴ <%this.prices.base_uah.join(' - ₴ ')%></p>

                <div class="product-price-popup">
                  <p <% if(this.priceColor.price) {%> class="text-orange" <%}%> >{{ __('РРЦ') }}: ₴<%this.prices.price.join(' - ₴')%></p>
                  <p <% if(this.priceColor.drop_uah) {%> class="text-orange" <%}%> >{{ __('Дропшиппинг') }}: ₴<%this.prices.drop_uah.join(' - ₴')%></p>
                  <p <% if(this.priceColor.opt_price) {%> class="text-orange" <%}%> >{{ __('Опт') }}: $<%this.prices.opt_price.join(' - $')%></p>
                  <p <% if(this.priceColor.opt_vip_usd) {%> class="text-orange" <%}%> >VIP: $<%this.prices.opt_vip_usd.join(' - $')%></p>
                  <p <% if(this.priceColor.opt_dealer_usd) {%> class="text-orange" <%}%> >{{ __('Дилер') }}: $<%this.prices.opt_dealer_usd.join(' - $')%></p>
                </div>

                <%} else {%>
                <p class="cursor_p mt-20 ml-10"><b>$ Скрыто <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('вход') }})</a></b></p>
                <div class="product-price-popup">
                  Чтобы увидеть цены, нужно <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('авторизоваться') }})</a>
                </div>
                <% } %>
              @endif
            </span>
            @endif
          </div>
        </div>

        <%if (!this.isPrint && this.show_price && this.bySeries === false) {%>
        <a href="#" class="btn-buy add-to-cart">
          <span>{{ __('В корзину') }}</span>

          <%if (this.in_cart_qty){%>
          <b><%this.in_cart_qty%> {{ __('ед.') }}</b>
          <%}%>
        </a>
        <%}%>

        <%if (this.isPrint) {%>
        <a href="#" class="product-material-btn-buy add-to-cart" >
          <span>{{ __('В корзину') }}</span>

          <%if (this.in_cart_qty){%>
          <b><%this.in_cart_qty%> {{ __('ед.') }}</b>
          <%}%>
        </a>
        <%}%>
      </div>
    </div>
  </script>
@endif
