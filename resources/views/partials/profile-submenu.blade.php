<div class="profile-sidebar-block left-block">
    <ul>
        <li><img src="/template/images/profile_photo.svg" alt="{{ __('Личный кабинет') }}"></li>
        @if (isset($user) && $user->debt != 0)
            <li class="profile-descktop__show"><div style="padding: 13px 0; color: #ff9f15">{{ __('Сумма долга') }}: {{ $user->debt }}</div></li>
        @endif
        <li {!! $activeProfileSubmenu == 'profile' ? 'class="active"' : '' !!}>
            <a href="{{ custom_route('profile') }}">{{ __('Моя информация') }}</a>
        </li>
        @if (false)<li><a href="#">{{ __('Мои адреса доставки') }}</a></li>@endif
        <li {!! $activeProfileSubmenu == 'orders' ? 'class="active"' : '' !!}>
            <a href="{{ custom_route('profile_orders') }}">{{ __('Мои заказы') }}</a>
        </li>
        <li {!! $activeProfileSubmenu == 'wish_list' ? 'class="active"' : '' !!}>
          <a href="{{ custom_route('wish_list') }}">{{ __('Лист ожидания') }}</a>
        </li>
        <li {!! $activeProfileSubmenu == 'price' ? 'class="active"' : '' !!}>
          <a href="{{ custom_route('profile_price') }}">{{ __('Экспорт товаров') }}</a>
        </li>
        <li {!! $activeProfileSubmenu == 'recently_viewed' ? 'class="active"' : '' !!}>
          <a href="{{ custom_route('recently_viewed') }}">{{ __('Недавно просматривали') }}</a>
        </li>
        <li>
          <a href="{{ custom_route('logout') }}">{{ __('Выход') }}</a>
        </li>
    </ul>
</div>
