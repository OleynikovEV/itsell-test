@if (isset($breadcrumbs))
<div class="breadcrumbs-block" itemscope itemtype="http://schema.org/BreadcrumbList">
    <ul class="wrapper">
        @foreach ($breadcrumbs as $num => $breadcrumb)
        <li itemscope itemprop="itemListElement" itemtype="http://schema.org/ListItem">
          <a itemprop="item"
            {!! $breadcrumb[1] ? 'href="' . $breadcrumb[1] . '"' : '' !!}>{{ $breadcrumb[0] }}</a>
            <meta itemprop="position" content="{{ $num+1 }}" />
            <meta itemprop="name" content="{{ $breadcrumb[0] }}" />
        </li>
        @endforeach

        @if (isset($cartsList))
          <li>
            <select id="default_cart" class="orange-border">
              @foreach ($cartsList as $key => $item)
                <option value="{{ $key }}"  @if ($cartDefault === $key) {{ 'selected' }} @endif> {{ __($item) }}</option>
              @endforeach
            </select>
          </li>
        @endif
    </ul>
</div>
@endif