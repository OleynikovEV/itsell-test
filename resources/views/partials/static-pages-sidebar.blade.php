<div class="static-sidebar-block left-block clearfix">
    <div>
        <h3>ITsellopt</h3>
        <p>
            <a href="{{ custom_route('static_page', ['contacts']) }}" class="{{ $activePage == 'contacts' ? 'active' : '' }}">
                {{ __('Контакты') }}
            </a>
        </p>
        <p>
            <a href="{{ custom_route('static_page', ['about']) }}" class="{{ $activePage == 'about' ? 'active' : '' }}">
                {{ __('О нас') }}
            </a>
        </p>
        <p>
            <a href="{{ custom_route('static_page', ['mission']) }}" class="{{ $activePage == 'mission' ? 'active' : '' }}">
                {{ __('Миссия и ценности') }}
            </a>
        </p>
    </div>
    <div>
        <h3>{{ __('Клиентам') }}</h3>
        <p>
            <a href="{{ custom_route('static_page', ['service']) }}" class="{{ $activePage == 'service' ? 'active' : '' }}">
                ITsellOPT <br> {{ __('Преимущества и сервис') }}
            </a>
        </p>
        <p>
            <a href="{{ custom_route('static_page', ['delivery']) }}" class="{{ $activePage == 'delivery' ? 'active' : '' }}">
                {{ __('Доставка и оплата') }}
            </a>
        </p>
        <p>
            <a href="{{ custom_route('static_page', ['warranty']) }}" class="{{ $activePage == 'warranty' ? 'active' : '' }}">
                {{ __('Гарантия и возврат') }}
            </a>
        </p>
        <p><a href="#" class="feedback-form-open">{{ __('Написать директору') }}</a></p>
    </div>
    <div>
        <h3>{{ __('Помощь') }}</h3>
        <p>
          <a href="{{ custom_route('static_page.new') }}"
             class="{{ $activePage == 'new' ? 'active' : '' }} {{ ($newFunctionality > 0) ? 'text-orange' : '' }}">
            {{ __('Новый функционал') }} @if ($newFunctionality > 0) ({{ $newFunctionality }}) @endif
          </a>
        </p>
        <p>
            <a href="{{ custom_route('static_page', ['features-faq']) }}" class="{{ $activePage == 'features-faq' ? 'active' : '' }}">
                {{ __('FAQ по функциям сайта') }}
            </a>
        </p>
        <p>
          <a href="{{ custom_route('static_page', ['features-faq']) . '#7.%20Дропшиппинг' }}" class="{{ $activePage == 'drop' ? 'active' : '' }}">
            {{ __('FAQ дропшиппинг') }}
          </a>
        </p>
    </div>
</div>