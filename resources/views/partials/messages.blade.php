@if (isset($errors) && $errors->any())
    <div class="error-message">{!! implode('<br>', $errors->all()) !!}</div>
@endif

@if (session()->has('errorMessage'))
    <div class="error-message">{!! session('errorMessage') !!}</div>
@endif

@if (isset($successMessage) && $successMessage)
    <div class="success-message">{!! $successMessage !!}</div>
@endif

@if (session()->has('successMessage'))
    <div class="success-message">{!! session('successMessage') !!}</div>
@endif