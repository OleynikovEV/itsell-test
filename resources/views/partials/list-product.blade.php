@php
  $showPrice = \Illuminate\Support\Facades\Auth::check() || !$product->show_price;
@endphp
  <div class="block product-block product-big-block {{ ($layoutCartProducts->has($product->id) && $product->bySeries == false) ? 'product-in-cart' : '' }}">
    <div class="product-wrapper {{ ($showPrice === false) ? 'padding-bottom-2-rem' : (($product->isSale()) ? 'padding-bottom' : '') }}">

      <div class="product-image" data-box="{{ $product->getBoxImage() }}">
        @if (false)
          <div style="position: absolute;top: -120px;left: 0;font-size: 11px">{{ date('d-m-Y H:i', $product->added_time) }}</div>
        @endif
        {{-- Кнопка для добавления в список ожиданий --}}
        @if (! $product->bySeries)
        <div class="wish-button tooltip-event {{ in_array($product->id, $wishList) ? 'wish-button__selected' : null }} {{ $product->isAvailability() ? 'hidden' : null }}"
                title="{{ __('Добавить в лист ожиданий') }}"
                data-product-id="{{ $product->id }}">
          <svg width="25" height="22" xmlns="http://www.w3.org/2000/svg">
            <path d="M11.3022 2.7235L12.0004 3.4042L12.6985 2.7233C13.7846 1.664 15.276 1 16.9387 1C20.3433
                          1 23 3.7585 23 7.0376C23 8.7743 22.2631 10.3504 21.0666 11.4595L21.0516 11.4734L21.0372 11.4879L11.9982
                          20.5815L2.908 11.4364L2.8973 11.4256L2.8863 11.4152C1.7177 10.3087 1 8.7517 1 7.0376C1 3.7585 3.6567
                          1 7.0613 1C8.7244 1 10.2154 1.6639 11.3022 2.7235Z">
            </path>
          </svg>
        </div>
        @endif

        @if ($product->bySeries && ($product->series || $product->technical))
          <a href="{{ $product->urlSeries }}">
        @else
          <a href="{{ custom_route('product', [$product->url, $product->id]) }}">
        @endif
          <img data-in="{{ $product->getBoxImage() }}" data-tmp="" data-out="{{ $product->getDefaultTitleImage() }}"
               src="{{ $product->getDefaultTitleImage() }}" alt="{{ $product->title }}">
        </a>

        {{--     вывод лейбы       --}}
        @if ($product->bySeries)
          @foreach ($product->labels as $i => $label)
            <a href="{{ $label['url']  }}">
              <div class="tooltip-event cursor_p {{ $label['class'] }}"
                   style="top: {{ $i * 28 }}px;" title="{{ $label['amountText'] }}">
                  {{ $label['amount'] }} {{ $label['text'] }}
              </div>
            </a>
          @endforeach
        @else
          @php $label = $product->getLabelNew(); @endphp
          <div class="{{ $label['class'] }}" style="top: 0px">{{ $label['text'] }}</div>
        @endif
      </div>

      @if (false)
{{--        !auth()->guest() && auth()->user()->isAdmin())--}}
        <div>
          <a href="{{ config('custom.edit_product_url') . $product->id }}" target="_blank">Редакировать</a>
        </div>
      @endif

      @if (!auth()->guest() && auth()->user()->isAdmin() && $product->priority > 0)
{{--      @php $rating = $product->calcRating(100, 5) @endphp--}}
        <div class="product-rating flex flex-direction-row justify-left align-center">
{{--          @for ($i = 1;$i <= $rating; $i++)--}}
            <div class="rating-line rating-line__active">☆ {{ $product->priority }}</div>
{{--          @endfor--}}
        </div>
      @endif

      <div class="product-code clearfix">
        @if ($product->series)
          <p class="float-left bg-orange">
            <a href="{{ custom_route('products_series', [$product->series->url]) }}"
               class="tooltip-event" title="{{ sprintf('Всего %s %s в серии',
                  $product->series->products_count,
                  word_declension(__('модел'), $product->series->products_count)
               ) }}">
              {{ $product->series->products_count }} {{ word_declension(__('модел'), $product->series->products_count) }}
            </a>
          </p>
        @else
          <p class="float-left">&nbsp;&nbsp;&nbsp;</p>
        @endif

        @if (! $product->bySeries) {{--   общее кол-во серий всех моделей - оранжевый прямоугольник     --}}
          <a href="{{ custom_route('product', [$product->url, $product->id]) }}">#{{ ltrim($product->baseid, '0') }}</a>
        @endif
      </div>
      {{--   название товара     --}}
      <div class="product-name">
        @if ($product->bySeries && ($product->series || $product->technical))
          <a href="{{ $product->urlSeries }}">{!! $product->title !!}</a>
        @else
          <a href="{{ custom_route('product', [$product->url, $product->id]) }}">{!! $product->title !!}</a>
        @endif
      </div>

    @if (!$product->isPrint())
      <!-- #Begin Color block input qty -->
        <div class="product-colors clearfix">
          @if ($product->group_products && count($product->group_products))
            <div class="mobile-more-colors">{{ count($product->group_products) }} {{ word_declension(__('цвет'), count($product->group_products)) }}<b></b></div>
            <div class="product-colors-wrapper clearfix">
              @foreach ($product->group_products as $groupProduct)
                @if ($product->bySeries)
                  @include('elements.color-group')
                @else
                  @include('elements.color-single')
                @endif
              @endforeach
            </div>

            @if (count($product->group_products) > 6)
              <div class="btn-more-color" data-amount="{{ count($product->group_products) - 6 }}">{{ __('Еще') }} +{{ count($product->group_products) - 6 }}</div>
            @endif

            @if ($product->qty <= 0 && $product->when_appear)
              <div class="product-availability-block soon-available">{{ __('ожидается') }}</div>
            @endif
          @endif
        </div>
        <!--#End Color block input qty -->
    @elseif ($product->isPrint() && ! $product->bySeries)
      <!--#Begin Material block input qty -->
        <div class="product-material clearfix">
          <select class="select-block material" style="width: 60%">
            <option value="" style="display: none">{{ __('Выберите материал') }}</option>
            @foreach ($product->materialSelect() as $key => $item)
              <option value="{{ $key }}" @if( $product->countMaterial() === 1) selected @endif>{{ strtoupper($item->name_ru) }}</option>
            @endforeach
          </select>

          <img src="/template/images/hint_icon.svg" class="cursor_p page-button tooltip-event"
               title="{{ __('Описание материала') }}"
               data-page-name="material_chehla_print" title="{{ __('Описание материалов') }}"/>

          <div class="product-qty-block">
            <input type="number" class="input-bottom product-qty"
                   style="width: 25px; margin-left: 0px; text-align: center;"
                   data-id="{{ $product->id }}" value="0" tabindex="1" inputmode="numeric">
          </div>
        </div>

        <a href="#" class="btn-buy add-to-cart">
          <span>{{ __('В корзину') }}</span>

          @if ($layoutCartProducts->has($product->id))
            <b>{{ $layoutCartProducts->get($product->id) . ' ' . __('ед.') }}</b>
          @endif
        </a>
        <!--#End Material block input qty -->
      @endif

      <div class="product-price">
        <div class="product-price-wrapper">
          {{--    Вывод цен    --}}
          @include('elements.price-block', [
            'priceTypeSite' => $priceTypeSite,
            'product' => $product,
            'showPrice' => $showPrice,
            'sale' => ($product->old_opt_price > $product->opt_price) ? $product->old_opt_price : 0,
          ])
        </div>
      </div>

      {{--  показать кнопку "В корзину" (если это не принт, можно показать цену или это не серия) --}}
      @if (!$product->isPrint() && $showPrice && ! $product->bySeries)
        <a href="#" class="btn-buy add-to-cart">
          <span>{{ __('В корзину') }}</span>

          @if ($layoutCartProducts->has($product->id))
            <b>{{ $layoutCartProducts->get($product->id) . ' ' . __('ед.') }}</b>
          @endif
        </a>
      @endif

      @if ($product->isPrint() && $product->bySeries == false)
        <a href="#" class="product-material-btn-buy add-to-cart" >
          <span>{{ __('В корзину') }}</span>

          @if ($layoutCartProducts->has($product->id))
            <b>{{ $layoutCartProducts->get($product->id) . ' ' . __('ед.') }}</b>
          @endif
        </a>
      @endif
    </div>
  </div>