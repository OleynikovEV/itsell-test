@extends('layout')

@section('content')
<div class="profile-block-wrapper wrapper clearfix">
    <div class="profile-content-block right-block">
        <div class="mobile-profile-header">
            <div class="profile-customer-data clearfix">
                <img src="/template/images/profile_photo.svg" alt="profile-photo">
                <b>{{ $user->fio }}</b>
            </div>
            <div class="profile-active-tab">{{ __('Моя информация') }}</div>
        </div>
        <form method="post" action="{{ route('profile.update') }}" class="page">
            <div class="title-block">{{ __('Моя информация') }}</div>

            @include('partials.messages')

            <div class="profile-input-block clearfix">
                <div class="input-field">
                    <p>Email</p>
                    <input type="text" name="email" value="{{ old('email') ?: $user->email }}">
                </div>
                <div class="input-field">
                    <p>{{ __('Телефон') }}</p>
                    <input type="text" name="phone" value="">
                </div>
                <div class="input-field">
                    <p>{{ __('ФИО') }}</p>
                    <input type="text" name="fio" value="{{ old('fio') ?: $user->fio }}">
                </div>
                <div class="clear"></div>

                <div class="input-field">
                    <p></p>
                    <input type="password" name="password" placeholder="{{ __('Новый пароль') }}">
                </div>
                <div class="input-field">
                    <p></p>
                    <input type="password" name="password_confirmation" placeholder="{{ __('Подтвердите пароль') }}">
                </div>
            </div>

            <div class="profile-input-block clearfix select">
              <p>{{ __('Ваш способ закупки') }}:</p>

              <label class="checkbox">
                <input type="checkbox" name="purchase_type[]" value="wholesale"
                  {{ (old('purchase_type') && in_array('wholesale', old('purchase_type'))  !== false) || in_array('wholesale', $purchase_type) ? 'checked' : '' }}>{{ __('Опт') }}
              </label>
              <label class="checkbox">
                <input type="checkbox" name="purchase_type[]" value="dropshipping"
                  {{ (old('purchase_type') && in_array('dropshipping', old('purchase_type')) !== false) || in_array('dropshipping', $purchase_type) ? 'checked' : '' }}>{{ __('Дропшиппинг') }}
              </label>
              <label class="checkbox">
                <input type="checkbox" name="purchase_type[]" value="retail"
                  {{ (old('purchase_type') && in_array('retail', old('purchase_type')) !== false) || in_array('retail', $purchase_type) ? 'checked' : '' }}>{{ __('Розница') }}
              </label>
            </div>

            {{--      Настройки цен      --}}
            <div class="profile-input-block clearfix">
              <p>{{ __('Выбирите какой тип цены будет отображаться по умолчанию') }}:</p>
              <div class="input-field">
                <p>{{ __('На сайте') }}</p>
                <select name="price_type_site" class="orange-border">
                  @foreach ($price_type_site as $key => $item)
                    <option value="{{ $key }}" @if ($price_type_site_default == $key) {{ 'selected' }} @endif>{{ $item }}</option>
                  @endforeach
                </select>
              </div>

              @if (count($price_type) > 0)
              <div class="input-field">
                <p>{{ __('В корзине') }}</p>
                <select name="price_type" class="orange-border">
                  @foreach ($price_type as $key => $item)
                    <option value="{{ $key }}" @if ($price_type_default == $key) {{ 'selected' }} @endif>{{ $item }}</option>
                  @endforeach
                </select>
              </div>
              @endif
            </div>

            {{--     работа с корзинами       --}}
{{--            <div class="title-block">{{ __('Список доступных корзин') }}</div>--}}
{{--            <div class="profile-input-block clearfix">--}}
{{--              <select name="default_cart" class="orange-border">--}}
{{--                @foreach ($cartList as $key => $item)--}}
{{--                <option value="{{ $key }}" @if ($user->default_cart === $key) {{ 'selected' }} @endif> {{ __($item) }}</option>--}}
{{--                @endforeach--}}
{{--              </select>--}}
{{--            </div>--}}

            <div class="title-block">{{ __('Подписка на рассылку') }}</div>
            <div class="profile-input-block clearfix">
                <div class="input-field">
                  <div class="subscribe">
                    @if ($user->telegram_chat_id > 0)
                      <i class="check-mark"></i>
                      <a href=" {{ custom_route('unsubscribe', ['userId' => $user->id, 'messenger' => 'telegram']) }}">{{ __('Отписаться от Telegram') }}</a>
                    @else
                      <a href="https://telegram.me/{{ config('custom.telegram_bot_url') }}?start={{ $user->id }}"
                         id='hrefTelegram'
                         data-event="telegram"
                         target="_blank">
                        {{ __('Получать уведомления по Telegram') }}
                      </a>
                    @endif
                  </div>
                  <div class="subscribe">
                    @if ($user->viber_chat_id)
                      <i class="check-mark"></i>
                      <a href=" {{ custom_route('unsubscribe', ['userId' => $user->id, 'messenger' => 'viber']) }}">{{ __('Отписаться от Viber') }}</a>
                    @else
                      <a href="viber://pa?chatURI={{ config('custom.viber_bot_url') }}&context={{ $user->id }}&text=Отправьте это сообщение чтоб подписаться"
                         id="hrefViber"
                         data-event="viber"
                         target="_blank">
                        {{ __('Получать уведомления по Viber') }}
                      </a>
                    @endif
                  </div>
                </div>
            </div>

            @if (false)
            <div class="title-block">{{ __('Получать новости и предложение на почту') }}</div>
            <div class="radio-block">
                <label class="radio active">
                    <input type="radio" name="group-1">{{ __('Да') }}
                </label>
                <label class="radio">
                    <input type="radio" name="group-1">{{ __('Нет') }}
                </label>
            </div>

            <!-- Profile Info Block -->
            <div class="title-block">{{ __('Информация о продавце') }}</div>
            <!-- Select Block -->
            <div class="select-block">
                <p>{{ __('Что вы продаете') }}</p>
                <div class="selected-block">{{ __('Аксессуары') }}</div>
                <div class="select-list-block">
                    <div class="active">{{ __('Аксессуары') }}</div>
                    <div>{{ __('Чехлы') }}</div>
                    <div>{{ __('Пленки') }}</div>
                </div>
            </div>
            <!-- #End Select Block -->

            <div class="profile-input-block clearfix">
                <div class="input-field">
                    <input type="text" placeholder="Кол-во торговых точек">
                </div>
                <div class="input-field">
                    <input type="text" placeholder="Адрес вашего сайта">
                </div>
            </div>
            <div class="radio-block">
                <p>Отображать цены</p>
                <label class="radio active">
                    <input type="radio" name="group-2">Да
                </label>
                <label class="radio">
                    <input type="radio" name="group-2">Нет
                </label>
            </div>
            <!-- #End Profile Info Block -->
            @endif

            <button class="btn-orange" type="submit">{{ __('Сохранить') }}</button>
        </form>
    </div>
    <!-- #End Profile Content Block -->

    @include('partials.profile-submenu')

</div>
@endsection

@push('footer-scripts')
<script>
    $(function() {
      // Маска для поля номера телефона
      //$('input[name=phone]').inputmask('mask', {mask: '+38 (999) 999-99-99', clearMaskOnLostFocus: false});
      mask.init('input[name=phone]', {{ old('phone') ?: $user->phone }});

      $('.profile-active-tab').click(function() {
        $('.page').toggle();
      });
    });
</script>

@if (session()->has('register_success'))
<script>
    gtag('event', 'conversion', {'send_to': 'AW-869983313/OftrCJ_ZmmsQ0cjrngM'});
    fbq('track', 'Purchase');
</script>
@endif

@endpush
