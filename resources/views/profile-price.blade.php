@extends('layout')

@section('content')
  <div class="profile-block-wrapper wrapper clearfix">
    <div class="profile-content-block right-block">

      <div class="profile-active-tab" style="margin-bottom: 15px;">{{ __('Экспорт товаров') }}</div>

      <div class="page">
        <div>
            <img class="cursor_p page-button tooltip-event" src="/template/images/hint_icon.svg"
                 title="{{ __('Инструкция к модулю по выгрузке товаров') }}" id="inform-ico"
                 style="vertical-align: sub;z-index: 999">
            <a href="https://itsellopt.com.ua/pages/new/functionality/160">
            <span style="font-size: 16px" class="text-orange text-bold">{{ __('Инструкция к модулю по выгрузке товаров') }}</span>
          </a>
        </div>

        <div class="title-block">{{ __('Мои шаблоны') }}
          <a href="{{ custom_route('price-generator') }}" class="btn btn-warning">{{ __('Создать шаблон') }}</a>
        </div>
        <div class="text-orange" style="font-size: 16px; font-weight: bold;">Скачивание прайса по ссылке доступно каждых 15 мин.</div>
        @forelse ($prices as $num => $price)
          <div class="profile-order">
            <div class="profile-order-number">
              <div>
                {{ ++$num }})
                <a href="{{ custom_route('price-generator.update', ['id' => $price->id]) }}">{{ $price->price_name }}</a>
                <p style="margin-top: 10px!important;" class="profile-order-time">{{ __('Обновлен') }}: {{ date('d.m.Y в H:i', strtotime($price->download)) }}</p>
                <div class="mt-10">
                  <a class="btn btn-small btn-default" href="{{ custom_route('price-generator.generate', ['slug' => $price->slug]) }}">{{ __('Скачать')  }}</a>
                  <a class="btn btn-small btn-default" href="{{ custom_route('price-generator.generate',
                    ['slug' => $price->slug, 'format' => 'xml']) }}">{{ 'XML'  }}</a>
                  <a class="btn btn-small btn-default" href="{{ custom_route('price-generator.generate',
                    ['slug' => $price->slug, 'format' => 'csv']) }}">{{ 'CSV'  }}</a>
                  <a class="btn btn-small btn-danger" href="{{ custom_route('price-generator.remove', ['id' => $price->id]) }}"
                     onclick="return confirm('{{ __('Удалить шаблон?') }}');">{{ __('Удалить') }}</a>
                  <div class="mt-10">
                    Ссылка для скачивания - {{ route('profile_price.download', ['userId' => $price->getUserId(), 'slug' => $price->getSlug()]) }}
                    <img src="/template/images/copy.png" alt="Копировать ссылку" title="Копировать ссылку" class="cursor_p"
                         onclick='copyLink("{{ route('profile_price.download', ['userId' => $price->getUserId(), 'slug' => $price->getSlug()]) }}")' />
                  </div>
                </div>
              </div>
            </div>
            @if ($price->download != null)
              <p class="profile-order-time">{{ __('Последнее скачивание') }}: {{ date('d.m.Y в H:i', strtotime($price->download)) }}</p>
            @endif
          </div>
        @empty
          <p>У вас пока нет шаблонов для генерации прайсов</p>
        @endforelse
      </div>
    </div>

    @include('partials.profile-submenu')
  </div>
@endsection

@push('footer-scripts')
  <script src="/template/js/dist/custom.min.js"></script>
  <script>
    function copyLink(link)
    {
      const $temp = $("<input>");
      $("body").append($temp);
      $temp.val(link).select();
      document.execCommand("copy");
      $temp.remove();
    }

    $(function() {
      $('.profile-active-tab').click(function() {
        $('.page').toggle();
      });
    });
  </script>
@endpush
