@extends('layout')

@section('content')

@if ($items && $items->count())
<!-- Cart Sorting Block -->
<div class="cart-sorting-block wrapper clearfix">
    <div class="flex cart-row justify-between">
        <div class="title-block">{{ __('Корзина') }}</div>
        <div class="flex align-center">
            <label>{{ __('Введите промокод') }}</label>
            <input type="text" id="promocode" class="input" value="" maxlength="50" />
            <input type="button" id="apply" value="OK" />
            <input type="image" id="loader" class="hidden" src="/template/images/ajax-loader.gif" />
        </div>
    </div>
    <br><br>
    <div class="cart-sorting-buttons">
        @if (false)
        <div class="sorting-buttons-wrapper">
            <a href="#" class="btn-open">{{ __('Расскрыть все') }}</a>
            <a href="#" class="btn-sorting">
                {{ __('Групповое управление количеством') }}
                <span class="hint-popup">
                    {{ __('Нажмите на строки, которые вы хотите связать, затем введите необходимое количество товара в любую из строк, он применится ко всем сразу') }}
                </span>
            </a>
            <a href="#" class="btn-sorting">{{ __('Собрать цвета') }}</a>
        </div>
        <div class="cart-sorting-wrapper">
            <div class="cart-sorting-content">
                <p>{{ __('Сортировать по') }}:</p>
                <span>{{ __('Порядку добавления') }}</span>
            </div>
            <div class="cart-sorting-popup">
                <div class="active">{{ __('Порядку добавления') }}</div>
                <div>{{ __('По цене') }}</div>
                <div>{{ __('По типу') }}</div>
            </div>
        </div>
        @endif
    </div>
</div>
<!-- #End Cart Sorting Block -->

<!-- Cart Content Block -->
<div class="cart-content-block">
    <div class="cart-table-title cart-product-block">
        <div class="wrapper clearfix">
            <div class="cart-col-1">{{ __('Название товара') }}</div>
            <div class="cart-col-2">{{ __('Бренд') }}</div>
            <div class="cart-col-3">{{ __('Модель телефона') }}</div>
            <div class="cart-col-4">{{ __('Цена') }}</div>
            <div class="cart-col-5">{{ __('Количество') }}</div>
            <div class="cart-col-6">{{ __('Стоимость') }}</div>
            <div class="cart-col-7">{{ __('Статус') }}</div>
            <div class="cart-col-8">{{ __('Удалить') }}</div>
        </div>
    </div>

    @foreach ($items as $item)
    @if ($products->has($item->id))
    <div class="cart-product-block" data-row-id="{{ $item->rowId }}">
        <div class="wrapper clearfix">
            <div class="cart-col-1 clearfix">
                <div class="cart-product-image left-block" style="text-align: center;">
                    @if ($products->get($item->id)->url == null)
                      <img src="{{ $products->get($item->id)->image }}" style="width: 45px" />
                    @else
                    <a href="{{ custom_route('product', [optional($products->get($item->id)->parentProduct)->url ?: $products->get($item->id)->url, optional($products->get($item->id)->parentProduct)->id ?: $item->id]) }}">
                        <img src="{{ $products->get($item->id)->img ? config('custom.content_server') . thumb_name($products->get($item->id)->img) : '/template/images/noimage.jpg' }}" alt="{{ $products->get($item->id)->title }}">
                    </a>
                    @endif
{{--                    @if ($products->get($item->id)->isMarkdown())--}}
{{--                        <div class="product-label-block product-label-block-small label-markdown"></div>--}}
                        <div class="product-label-block product-label-block-small {{ $products->get($item->id)->getLabel() }}"></div>
{{--                    @elseif ($products->get($item->id)->isNovelty())--}}
{{--                        <div class="product-label-block product-label-block-small label-new"></div>--}}
{{--                    @elseif ($products->get($item->id)->old_opt_price > $products->get($item->id)->opt_price)--}}
{{--                        <div class="product-label-block product-label-block-small label-sale"></div>--}}
{{--                    @elseif ($products->get($item->id)->qty <= 0 && $products->get($item->id)->when_appear)--}}
{{--                        <div class="product-label-block product-label-block-small label-soon"></div>--}}
{{--                    @endif--}}
                </div>
                <div class="cart-product-info right-block">
                    <div class="cart-product-labels">
                        @if (optional($products->get($item->id)->parentProduct)->baseid != null || $products->get($item->id)->baseid != null)
                        <div class="product-label-grey">#{{ ltrim(optional($products->get($item->id)->parentProduct)->baseid ?? $products->get($item->id)->baseid, '0') }}</div>
                        @endif
                        <div class="product-label-orange @if ($item->discount == 0) hidden @endif">промо -{{ $item->discountRate }}%</div>
                        @if ($products->get($item->id)->need_package)
                        <div class="product-label-small">{{ __('техпак') }}</div>
                        @endif
                    </div>
                    @if ($products->get($item->id)->url == null)
                      {{ $products->get($item->id)->title }}
                    @else
                    <a href="{{ custom_route('product', [optional($products->get($item->id)->parentProduct)->url ?: $products->get($item->id)->url, optional($products->get($item->id)->parentProduct)->id ?: $item->id]) }}"
                        class="{{ ($products->get($item->id)->qty == 0 && !$products->get($item->id)->when_appear) ? 'cross-out' : null }}"
                    >
                        {{ $products->get($item->id)->title }}
                    </a>
                    @endif
                    @if ($products->get($item->id)->color != null)
                    <div class="cart-product-color">{{ __('Цвет') }}:
                        <b class="select-color" data-id="{{ $products->get($item->id)->parent }}">
                            <span style="background-color: #{{ $products->get($item->id)->color }};" {!! $products->get($item->id)->colorBorder ? 'class="color-white"' : '' !!}></span>{{ $products->get($item->id)->color_name }}
                        </b>
                    </div>
                    @endif
                    @if ($products->get($item->id)->isPrint())
                    <div class="cart-product-color">{{ __('Материал') }}:
                      <b><span>{{ ['silicone' => __('тпу'), 'plastic' => __('3D пластик')][$item->options->material] }}</span></b>
                    </div>
                    @endif
                </div>
            </div>
            <div class="cart-col-2">
                @if ($products->get($item->id)->parentProduct)
                  <p>{{ optional($products->get($item->id)->parentProduct->brandData)->title }}</p>
                @else
                  <p>{{ optional($products->get($item->id)->brandData)->title }}</p>
                @endif
            </div>
            <div class="cart-col-3">
                @if ($products->get($item->id)->categoryData)
                  <a href="{{ custom_route('category', ['phones', $products->get($item->id)->categoryData->name]) }}">{{ $products->get($item->id)->categoryData->title }}</a>
                @else
                  {{ $products->get($item->id)->categoryVchehle->category->title ?? null }}
                @endif
            </div>
            <div class="cart-col-4">
                @if (\Illuminate\Support\Facades\Auth::check() || !$products->get($item->id)->show_price)
{{--                    <p>${{ $item->price }} / ₴{{ round($item->price * settings('dollar')) }}</p>--}}
                    <p>${{ calcSale($item->price, $item->discount) }} / ₴{{ round(calcSale($item->price, $item->discount) * settings('dollar')) }}</p>
                    @if ($products->get($item->id)->old_opt_price > $products->get($item->id)->opt_price)
                    <p>
                        <span class="cross-out">${{ $products->get($item->id)->old_opt_price }} </span>/
                        <span class="cross-out">₴{{ round($products->get($item->id)->old_opt_price * settings('dollar')) }}</span>
                    </p>
                    @endif
                @else
                    <div class="product-price-wrapper position-r wt-70">
                        <p class="cursor_p"><b>$ Скрыто <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('вход') }})</a></b></p>
                        <div class="product-price-popup mt-0">
                            Чтобы увидеть цены, нужно <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('авторизоваться') }})</a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="cart-col-5">
                <div class="spinner-block clearfix">
                    <div class="minus">-</div>
                    <input type="text" class="cart-item-qty" value="{{ $item->qty }}">
                    <div class="plus">+</div>
                </div>
            </div>
            <div class="cart-col-6">
                @if (\Illuminate\Support\Facades\Auth::check() || !$products->get($item->id)->show_price)
{{--                    <p><b>${{ round($item->price * $item->qty, 2) }} / ₴{{ round($item->price * $item->qty * settings('dollar')) }}</b></p>--}}
                    <p><b>${{ $item->subtotal() }} / ₴{{ round($item->subtotal() * settings('dollar')) }}</b></p>
                @else
                    <div class="product-price-wrapper position-r wt-70">
                        <p class="cursor_p"><b>$ Скрыто <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('вход') }})</a></b></p>
                        <div class="product-price-popup mt-0">
                            Чтобы увидеть цены, нужно <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('авторизоваться') }})</a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="cart-col-7">
                @if ($products->get($item->id)->qty > 0)
                <div class="product-availability-block">{{ __('доступен') }}</div>
                @elseif ($products->get($item->id)->when_appear)
                <div class="product-availability-block soon-available">{{ __('ожидается') }}</div>
                @else
                <div class="product-availability-block not-available">{{ __('нет в наличии') }}</div>
                @endif
            </div>
            <div class="cart-col-8">
                <button class="cart-item-delete btn-remove"></button>
            </div>
            <div class="cart-col-sale">
                @if (\Illuminate\Support\Facades\Auth::check() || !$products->get($item->id)->show_price)
                    @if ($products->get($item->id)->old_opt_price > $products->get($item->id)->opt_price)
                        <p>
                            <span>${{ $products->get($item->id)->opt_price }} </span>/
                            <span class="cross-out">${{ $products->get($item->id)->old_opt_price }}</span>
                        </p>
                    @endif
                @endif
            </div>
        </div>
    </div>
    @endif
    @endforeach

</div>
<!-- #End Cart Product Block -->

@if ($packages && $packages->count())
<div class="gradient-block product-packaging-block">
    <div class="wrapper">
        <div class="product-list-block oneline product-small-block">
            <div class="title-block">{{ __('Добавьте к заказу упаковку') }}</div>
            <div class="product-list-wrapper clearfix">
                @each('partials.list-product', $packages, 'product')
            </div>
            <div class="btn-show-more"></div>
        </div>
    </div>
</div>
@endif

<!-- Order Block -->
<div class="order-block">
    <div class="order-panel-block order-panel-top">
        <div class="wrapper clearfix">
            <div class="order-quantity-block"><p class="cart-count-sku"><b>{{ $items->count() }}</b> <span>{{ word_declension(__('Наименовани'), $items->count()) }}</span></p></div>
            <div class="order-quantity-block"><p class="cart-count"><b>{{ Cart::count() }}</b> <span>{{ word_declension(__('Единиц'), Cart::count()) . ' ' . __('товара') }}</span></p></div>
            @if (\Illuminate\Support\Facades\Auth::check() || !$products->get($item->id)->show_price)
                <div class="order-total-sum order-quantity-block"><p><b>$</b> <i class="cart-total">{{ Cart::total() }}</i> <span>{{ __('Сумма') }}</span></p></div>
            @else

                <div class="order-total-sum order-quantity-block"><p class="cursor_p mt-10" title="Чтобы увидеть цены, нужно авторизоваться"><b>$ Скрыто <a href="{{ custom_route('login', ['return_url' => '/cart']) }}" class="orange-link">({{ __('вход') }})</a></b> </div>
            @endif
            <button class="btn-checkout btn-orange square-corner cart-go-to-order">{{ __('Перейти к оформлению') }}</button>
        </div>
    </div>
    <!-- Order Form Wrapper -->
    <div class="order-form-wrapper wrapper clearfix">
        <form action="" method="post" class="order-form-main">
            <!-- Контактные данные -->
            <div class="order-content-block">
                <div class="title-block"><b>{{ __('Контактные данные') }}</b></div>
                <div class="order-form">
                    <label class="checkbox">
                        <input type="checkbox" name="dropshipping" value="1">{{ __('Дропшиппинг заказ') }}
                    </label>
                    <div class="input-field required">
                        <input type="text" name="lastname" placeholder="{{ __('Фамилия') }}" value="{{ $lastOrder ? $lastOrder->lastname : '' }}" data-preset="{{ $lastOrder ? $lastOrder->lastname : '' }}">
                    </div>
                    <div class="input-field required">
                        <input type="text" name="firstname" placeholder="{{ __('Имя') }}" value="{{ $lastOrder ? $lastOrder->firstname : '' }}" data-preset="{{ $lastOrder ? $lastOrder->firstname : '' }}">
                    </div>
                    <div class="input-field required">
                        <input type="text" name="phone" placeholder="{{ __('Телефон') }}" value="{{ $lastOrder ? $lastOrder->phone : '' }}" data-preset="{{ $lastOrder ? $lastOrder->phone : '' }}">
                    </div>
                </div>
            </div>

            <!-- Доставка -->
            <div class="order-delivery-block order-content-block">
                <div class="title-block"><b>{{ __('Доставка') }}</b></div>
                <div class="order-form">
                    <!-- Способ доставки -->
                    <input type="hidden" name="delivery_type_id" value="{{ $lastOrder && $lastOrder->delivery_type_id ? $lastOrder->delivery_type_id : 1 }}" data-preset="{{ $lastOrder && $lastOrder->delivery_type_id ? $lastOrder->delivery_type_id : '' }}">

                    <button type="button" class="btn-grey post-office-icon delivery-type-btn {{ ! $lastOrder || ! $lastOrder->delivery_type_id || $lastOrder->delivery_type_id == 1 ? 'active' : '' }}" data-value="1">{{ __('Отделение Новой Почты') }}</button type="button">
                    <button type="button" class="btn-grey courier-icon delivery-type-btn {{ $lastOrder && $lastOrder->delivery_type_id == 2 ? 'active' : '' }}" data-value="2">{{ __('Курьер Новой Почты') }}</button type="button">
                    <button type="button" id="delivery-type-other" class="btn-grey other-icon delivery-type-btn {{ $lastOrder && $lastOrder->delivery_type_id == 3 ? 'active' : '' }}" data-value="3" data-show="0">{{ __('Другое') }}</button type="button">

                    <!-- Комментарий к способу доставки -->
                    <div class="order-delivery-details input-field" id="delivery_comment" data-delivery-types="3">
                        <input type="text" maxlength="255" name="delivery_comment" placeholder="{{ __('Комментарий к доставке') }}">
                    </div>

                    <!-- Город -->
                    <div class="select-block custom-select city-select">
                        <p>{{ __('Город') }}</p>
                        <input type="hidden" name="city_id" class="custom-select-input" value="{{ $lastOrder && $lastOrder->city_id ? $lastOrder->city_id : '' }}" data-preset="{{ $lastOrder && $lastOrder->city_id ? $lastOrder->city_id : '' }}">
                        <div class="selected-block custom-select-active-value">{{ $lastOrder && $lastOrder->city_id && $lastOrder->city ? $lastOrder->city : __('Выберите город') }}</div>
                        <div class="select-list-block custom-select-list">
                            <div class="select-list-search custom-select-search"><input autocomplete="off" id="select_search" type="text" placeholder="{{ __('Поиск') }}"></div>
{{--                            @foreach ($cities as $cityId => $cityName)--}}
{{--                            <div data-value="{{ $cityId }}">{{ $cityName }}</div>--}}
{{--                            @endforeach--}}
                        </div>
                    </div>

                    <div class="fast-city-block order-city-presets">
                        <a href="#" class="btn-orange-small">{{ __('Киев') }}</a>
                        <a href="#" class="btn-orange-small">{{ __('Харьков') }}</a>
                        <a href="#" class="btn-orange-small">{{ __('Одесса') }}</a>
                        <a href="#" class="btn-orange-small">{{ __('Днепр') }}</a>
                    </div>

                    <!-- Отделение НП -->
                    <div class="order-delivery-details select-block custom-select warehouse-select" data-delivery-types="1">
                        <p>{{ __('Отделение') }}</p>
                        <input type="hidden" name="warehouse_id" class="custom-select-input" value="{{ $lastOrder ? $lastOrder->warehouse_id : '' }}" data-preset="{{ $lastOrder ? $lastOrder->warehouse_id : '' }}">
                        <div class="selected-block custom-select-active-value">{{ __('Выберите отделение') }}</div>
                        <div class="select-list-block custom-select-list">
                            <div class="select-list-search custom-select-search"><input type="text" placeholder="{{ __('Поиск') }}"></div>
                        </div>
                    </div>

                    <!-- Улица (Курьер НП) -->
                    <div class="order-delivery-details select-block custom-select street-select" data-delivery-types="2">
                        <p>{{ __('Улица') }}</p>
                        <input type="hidden" name="street_id" class="custom-select-input" value="{{ $lastOrder ? $lastOrder->street_id : '' }}" data-preset="{{ $lastOrder ? $lastOrder->phone : '' }}">
                        <div class="selected-block custom-select-active-value">{{ __('Выберите улицу') }}</div>
                        <div class="select-list-block custom-select-list">
                            <div class="select-list-search custom-select-search"><input type="text" placeholder="{{ __('Поиск') }}"></div>
                        </div>
                    </div>

                    <!-- Номер дома (Курьер НП) -->
                    <div class="order-delivery-details input-field street_building" data-delivery-types="2">
                        <input type="text" name="street_building" placeholder="{{ __('Дом') }}" value="{{ $lastOrder ? $lastOrder->street_building : '' }}" data-preset="{{ $lastOrder ? $lastOrder->street_building : '' }}">
                    </div>

                    <!-- Полный адрес (Другое) -->
                    <div class="order-delivery-details input-field address" data-delivery-types="3">
                        <input type="text" name="address" placeholder="{{ __('Адрес') }}" value="{{ $lastOrder ? $lastOrder->address : '' }}" data-preset="{{ $lastOrder ? $lastOrder->address : '' }}">
                    </div>

                    <!-- Плательщик за упаковку -->
                    <div id="package_payer" class="order-radio-block radio-block order-dropshipping-field hidden" data-show="1">
                        <p>{{ __('Конверт с воздушной подушкой 2 грн за счет') }}:</p>
                        <label class="radio active">
                            <input type="radio" name="package_payer" value="1" checked>{{ __('Мой') }}
                        </label>
                        <label class="radio">
                            <input type="radio" name="package_payer" value="2">{{ __('Покупателя') }}
                        </label>
                    </div>

                    <!-- Комментарий -->
                    <div class="input-field">
                        <input type="text" maxlength="255" name="comment" placeholder="{{ __('Комментарий') }}" value="{{ $lastOrder ? $lastOrder->comment : '' }}" data-preset="{{ $lastOrder ? $lastOrder->comment : '' }}">
                    </div>
                </div>
            </div>

            <!-- Оплата -->
            <div class="order-payment-block order-content-block">
                <div class="order-dropshipping-field hidden" data-show="1">
                    <div class="title-block"><b>{{ __('Оплата') }}</b></div>
                    <div class="order-form">
                        <div class="order-radio-block radio-block">
                            <label class="radio">
                                <input type="radio" name="payment_type_id" value="1" checked>{{ __('Наложенный платеж') }}
                            </label>
                            <label class="radio">
                                <input type="radio" name="payment_type_id" value="2">{{ __('Предоплата') }}
                            </label>
                        </div>

                        <div class="input-field required order-dropshipping-cod-fields">
                            <input type="text" name="dropshipping_price" placeholder="{{ __('Сумма') }}">
                        </div>

                        <div class="input-field order-delivery-price hidden">
                            <b>{{ __('Доставка') }}: <span></span></b>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        @guest
        <div class="order-form-overlap">
            <div>
                {{ __('Для оформления заказа') }}
                <a href="{{ custom_route('login', ['return_url' => '/cart']) }}">{{ __('войдите на сайт') }}</a>
                {{ __('или') }}
                <a href="{{ custom_route('register', ['return_url' => '/cart']) }}">{{ __('пройдите регистрацию') }}</a>
            </div>
        </div>
        @endguest
    </div>

    <div class="order-panel-block">
        <div class="wrapper clearfix">
            <div class="order-total-sum order-quantity-block"><p><b>$</b> <i class="cart-total">{{ Cart::total() }}</i> <span>{{ __('Сумма') }}</span></p></div>
            <button class="btn-checkout btn-orange square-corner cart-submit-order-form">
              <svg class="hidden" style="width: 50px; vertical-align: middle;"
                   version="1.1" id="preload" xmlns="http://www.w3.org/2000/svg"
                   xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                  <path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                    <animateTransform
                      attributeName="transform"
                      attributeType="XML"
                      type="rotate"
                      dur="1s"
                      from="0 50 50"
                      to="360 50 50"
                      repeatCount="indefinite" />
                  </path>
              </svg>
              {{ __('Оформить заказ') }}
            </button>
        </div>
    </div>
</div>

@if (isset($saleProducts) && $saleProducts->count())
<div class="gradient-block cart-cross-products">
    <div class="wrapper">
        <div class="product-list-block oneline">
            <div class="title-block">{{ __('Также интересуются') }}</div>
            <div class="product-list-wrapper clearfix">

                @each('partials.list-product', $saleProducts, 'product')

            </div>
            <div class="btn-show-more"></div>
        </div>
    </div>
</div>
@endif

@else
<div class="title-block text-center cart-empty-message">{{ __('Ваша корзина пуста') }}</div>
@endif

@endsection

@push('footer-scripts')
<script>
    var isCartPage = true;

    $(function() {
      // let timeout;
      // $('#select_search').on('input', function() {
      //   const self = $(this);
      //   clearTimeout(timeout);
      //   timeout = setTimeout(function() {
      //     const search = self.val();

          $.ajax({
            url: 'v1/cities',
            dataType: 'json',
            // data: {search: search},
            data: {search: ''},
            success: function(data) {
              const selectBlock = $('.city-select');
              let selectList = selectBlock.find('.custom-select-list');
              selectList.children(':not(.custom-select-search)').remove();

              for (let i in data) {
                const option = $('<div>').data('value', data[i].id).html(data[i].name_ru);
                selectList.append(option);
              }
            }
          });
      //   }, 1000);
      // });
    });
</script>
<script src="/template/js/dist/cart.min.js?1.0.3"></script>
<script src="/template/js/dist/order.min.js?1.0.2"></script>
@endpush
