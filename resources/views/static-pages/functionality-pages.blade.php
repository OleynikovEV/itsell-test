@extends('layout')

@section('content')
  <div class="static-page">
    <div class="wrapper clearfix">

      <div class=" right-block">
        <div class="mobile-static-active-tab" id="mobile-static-active-tab">{{ 'Новый функционал' }}</div>

        <div class="page-content">
          <h1 class="title-block">{{ 'Новый функционал' }}</h1>

          <div class="flex flex-direction-row flex-wrap justify-between">
          @forelse ($pages as $page)
            <div class="box-shadow-hover border-box post-block border-bottom">
              <a href="{{ custom_route('static_page.new.page', ['num' => $page->id]) }}"
                 class="flex flex-wrap flex-direction-row justify-between"
                 style="color: #4f515e;">
                <div class="item post-body-text">
                  <h3 class="text-orange bold">{{ $page->title }}</h3>
                  <div class="bold">{{ __('Дата введения') }}: {{ $page->dateStartFunctionality->format('d-m-Y') }}</div>
                  <div class="flex justify-between flex-wrap border-top mt-10">
                    <div class="align-justify border-box"><p>{{ $page->preview }}</p></div>
                  </div>
                </div>
                @if ($page->getCover())
                <div class="item post-body-img border-box"
                  style="background: url('{{ $page->getCover() }}') no-repeat center center;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    ">
                </div>
                @endif
              </a>
            </div>
          @empty
            <p>{{ __('Пока нет публикаций') }}</p>
          @endforelse
          </div>
      </div>

        {{-- Пагинация --}}
        @if ($pages->hasPages())
          <div class="pagination-block">
            <ul>{!! str_replace('?page=', '/', $pages->withQueryString()->links()) !!}</ul>
          </div>
        @endif
        {{--// Пагинация --}}
      </div>

      @include('partials.static-pages-sidebar', ['activePage' => $curPage->name])

    </div>
  </div>
@endsection

@push('footer-scripts')
  <script>
    $(function() {
      $('#mobile-static-active-tab').click(function() {
        const el = $('#mobile-static-active-tab');
        $('.page-content').toggle();
        if ($('.page-content').is(":visible")) {
          el.removeClass('rotate-0');
        } else {
          el.addClass('rotate-0');
        }
      });
    });
  </script>
@endpush