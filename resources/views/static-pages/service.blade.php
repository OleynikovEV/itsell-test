@extends('layout')

@section('content')
<div class="static-page">
    <div class="wrapper clearfix">
        <div class="static-content-block right-block">
            <div class="mobile-static-active-tab" id="mobile-static-active-tab">{{ $page->title }}</div>

            <div class="page-content">
              <h1 class="title-block">{{ $page->title }}</h1>

              <div class="static-advantages-block clearfix">
                  <div class="advantages-icon-1">{{ __('Системная работа с шириной') }}</div>
                  <div class="advantages-icon-2">{{ __('Скорость') }}</div>
                  <div class="advantages-icon-3">{{ __('Новый магазин') }}</div>
                  <div class="advantages-icon-4">{{ __('Система лояльности') }}</div>
                  <div class="advantages-icon-5">{{ __('Отсрочка') }}</div>
                  <div class="advantages-icon-6">{{ __('XML и контент') }}</div>
                  <div class="advantages-icon-7">{{ __('Персональный менеджер') }}</div>
              </div>
            </div>
        </div>

        @include('partials.static-pages-sidebar', ['activePage' => $page->name])
        @include('partials.main-page-advantages')

        <div class="advantages-popup popup-block"></div>
    </div>
</div>
@endsection

@push('footer-scripts')
<script>
    // Открытие всплывающего окна с контентом при выборе пункта
    $('.static-advantages-block > div').on('click', function() {
        var tabNumber = $(this).index() + 1;
        
        var textBlock = $('#advantages-tab-' + tabNumber);
        if ( ! textBlock.length) return;
        
        var popup = $('.advantages-popup').html($('<a>').addClass('btn-close').attr('href', '#'));
        popup.append($('<div>').addClass('popup-title').html($(this).text()));

        popup.append(textBlock.clone());

        $('.popup-bg').fadeIn(300);
        popup.addClass('active').css({top: $(window).scrollTop() + 50}).fadeIn(300);
    });

    // Выбора пункта по-умолчанию на основе параметров из URL'а
    var hash = document.location.hash;
    if (hash.indexOf('advantages-tab') == 1) {
        var tabNumber = parseInt(hash.substr(hash.lastIndexOf('-') + 1));
        if (tabNumber) {
            $('.static-advantages-block > div').eq(tabNumber - 1).click();
        }
    }

    $(function() {
      $('#mobile-static-active-tab').click(function() {
        const el = $('#mobile-static-active-tab');
        $('.page-content').toggle();
        if ($('.page-content').is(":visible")) {
          el.removeClass('rotate-0');
        } else {
          el.addClass('rotate-0');
        }
      });
    });
</script>
@endpush