@extends('layout')

@section('content')
  <div class="static-page">
    <div class="wrapper clearfix">

      <div class="right-block">
        <h2 class="text-center">{{ $page->title }}</h2>

        <div class="page-content">{!! $page->content !!} </div>
      </div>

      @include('partials.static-pages-sidebar', ['activePage' => $curPage->name])
    </div>
  </div>
@endsection