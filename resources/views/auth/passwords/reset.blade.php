@extends('layout')

@section('content')
  <div class="wrapper">
    <div class="flex justify-center align-center flex-direction-column mt-20">
      <div class="title-block">{{ __('Форма сброса пароля') }}</div>

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif

      <form method="POST" action="{{ route('password.update') }}">
        @csrf
        @include('partials.messages')

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="input-field">
          <p>{{ __('Email') }}</p>
          <input type="text" name="email" value="{{ old('email') }}" style="width: 300px" required autofocus>
        </div>

        <div class="input-field">
          <p>{{ __('Новый пароль') }}</p>
          <input type="password" name="password" value="" style="width: 300px" required>
        </div>

        <div class="input-field">
          <p>{{ __('Подтвердите пароль') }}</p>
          <input type="password" name="password_confirmation" value="" style="width: 300px" required>
        </div>

        <button class="btn-orange" type="submit">{{ __('Сохранить') }}</button>
      </form>
    </div>
  </div>
@endsection