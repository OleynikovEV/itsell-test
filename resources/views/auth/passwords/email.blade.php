@extends('layout')

@section('content')
  <div class="wrapper">
    <div class="flex justify-center align-center flex-direction-column mt-20">
      <div class="title-block">{{ __('Форма сброса пароля') }}</div>

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif

      <form method="post" action="{{ route('password.email') }}">
        @csrf
        @include('partials.messages')

        <div class="input-field">
          <p>{{ __('Введите email для сброса пароля') }}</p>
          <input type="text" name="email" value="{{ old('email') }}" style="width: 300px">
        </div>

        <button class="btn-orange" type="submit">{{ __('Сбросить пароль') }}</button>
      </form>

    </div>
  </div>
@endsection
