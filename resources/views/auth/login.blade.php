@extends('layout')

@section('content')
<div class="login-page">
    <div class="wrapper clearfix">
        <div class="left-block">
            <div class="title-block">{{ __('Зарегистрированные покупатели') }}</div>
            <form method="post" action="">

                @include('partials.messages')

                <div class="input-field">
                    <p>{{ __('E-mail (логин)') }}</p>
                    <input type="text" name="email" value="{{ old('email') }}">
                </div>
                <div class="input-field">
                    <p>{{ __('Пароль') }}</p>
                    <input type="password" name="password">
                </div>
                <button class="btn-orange" type="submit">{{ __('Войти') }}</button>

                <a href="{{ custom_route('password.request') }}">{{ __('Забыл пароль') }}</a>

            </form>
        </div>
        <div class="right-block">
            <div class="title-block">{{ __('Новый пользователь') }}</div>
            <p>{{ __('Для отображения цен товаров и оформелния заказа Вам необходимо зарегистрироваться. Сразу после подтверждения Вашей учетной записи Вы сможете приступить к оформлению заказа.') }}</p>
            <a href="{{ custom_route('register') }}" class="btn-orange">{{ __('Регистрация') }}</a>
        </div>
    </div>
</div>
@endsection