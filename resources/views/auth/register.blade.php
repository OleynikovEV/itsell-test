@extends('layout')

@section('content')
<div class="registration-page">
    <div class="wrapper">
        <div class="title-block">{{ __('Регистрация') }}</div>

        <form class="clearfix" method="post" action="">
            @include('partials.messages')

            <div class="left-block">
                <div class="input-field">
                    <p>{{ __('Название компании') }}: *</p>
                    <input type="text" name="company" value="{{ old('company') }}">
                </div>

                <div class="checkbox-block">
                    <p>{{ __('Что вы продаете') }}: *</p>
                    <label class="checkbox">
                        <input type="checkbox" name="products_type[]" value="accessories" {{ old('products_type') && array_search('accessories', old('products_type')) !== false ? 'checked' : '' }}>{{ __('Аксессуары') }}
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="products_type[]" value="technique" {{ old('products_type') && array_search('technique', old('products_type')) !== false ? 'checked' : '' }}>{{ __('Гаджеты') }}
                    </label>
                </div>

                <div class="checkbox-block">
                    <p>{{ __('Ваш способ продажи') }}: *</p>

                    <label class="checkbox">
                        <input type="checkbox" name="sales_type[]" value="retail" data-toggle-block=".retail-type-fields" {{ old('sales_type') && array_search('retail', old('sales_type')) !== false ? 'checked' : '' }}>{{ __('Розница') }}
                    </label>
                    <div class="retail-type-fields clearfix hidden">
                        <label class="checkbox left-block">
                            <input type="checkbox" name="retail_type[]" value="internet" data-toggle-block=".website-url-block" {{ old('retail_type') && array_search('internet', old('retail_type')) !== false ? 'checked' : '' }}>{{ __('Интернет розница') }}
                        </label>
                        <label class="checkbox right-block">
                            <input type="checkbox" name="retail_type[]" value="physical" data-toggle-block=".stores-number-block" {{ old('retail_type') && array_search('physical', old('retail_type')) !== false ? 'checked' : '' }}>{{ __('Физическая розница') }}
                        </label>
                    </div>

                    <label class="checkbox">
                        <input type="checkbox" name="sales_type[]" value="wholesale" data-toggle-block=".wholesale-type-fields" {{ old('sales_type') && array_search('wholesale', old('sales_type')) !== false ? 'checked' : '' }}>{{ __('Опт') }}
                    </label>
                    <div class="wholesale-type-fields clearfix hidden">
                        <label class="checkbox left-block">
                            <input type="checkbox" name="wholesale_type[]" value="small_wholesale" {{ old('wholesale_type') && array_search('small_wholesale', old('wholesale_type')) !== false ? 'checked' : '' }}>{{ __('Мелкий ОПТ') }}
                        </label>
                        <label class="checkbox right-block">
                            <input type="checkbox" name="wholesale_type[]" value="big_wholesale" {{ old('wholesale_type') && array_search('big_wholesale', old('wholesale_type')) !== false ? 'checked' : '' }}>{{ __('Крупный ОПТ') }}
                        </label>
                    </div>
                </div>

                <div class="checkbox-block">
                    <p>{{ __('Ваш способ закупки') }}: *</p>

                    <label class="checkbox">
                        <input type="checkbox" name="purchase_type[]" value="wholesale" {{ old('purchase_type') && array_search('wholesale', old('purchase_type')) !== false ? 'checked' : '' }}>{{ __('Опт') }}
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="purchase_type[]" value="dropshipping" {{ old('purchase_type') && array_search('dropshipping', old('purchase_type')) !== false ? 'checked' : '' }}>{{ __('Дропшиппинг') }}
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="purchase_type[]" value="retail" {{ old('purchase_type') && array_search('retail', old('purchase_type')) !== false ? 'checked' : '' }}>{{ __('Розница') }}
                    </label>
                </div>

                <div class="website-url-block input-field hidden">
                    <p>{{ __('Адрес онлайн магазина') }}: *</p>
                    <input type="text" name="website_url" value="{{ old('website_url') }}">
                </div>

                <div class="stores-number-block input-field hidden">
                    <p>{{ __('Количество магазинов') }}: *</p>
                    <input type="text" name="stores_number" value="{{ old('stores_number') }}">
                </div>

                <div class="select-block custom-select">
                    <p>{{ __('Как вы нас нашли') }}: *</p>
                    <input type="hidden" name="how_found" class="custom-select-input" value="{{ old('how_found') }}">
                    <div class="selected-block custom-select-active-value">{{ __('Выберите вариант') }}</div>
                    <div class="select-list-block custom-select-list">
                        @foreach (config('custom.how_found_options') as $option)
                        <div data-value="{{ $option }}">{{ __($option) }}</div>
                        @endforeach

                        <div data-value="custom">{{ __('Свой вариант') }}</div>
                    </div>
                </div>

                <div class="how-found-custom-block input-field hidden">
                    <input type="text" name="how_found_custom" value="{{ old('how_found_custom') }}" placeholder="{{ __('Введите свой вариант') }}">
                </div>

                <div class="input-field">
                    <p>{{ __('Что еще нам полезно будет знать') }}:</p>
                    <textarea name="additional_info">{{ old('additional_info') }}</textarea>
                </div>
            </div>

            <div class="right-block">
                <div class="input-field">
                    <p>{{ __('ФИО') }}: *</p>
                    <input type="text" name="fio" value="{{ old('fio') }}">
                </div>
                <div class="select-block custom-select country-select">
                  <p>{{ __('Страна') }}: *</p>
                  <input type="hidden" name="country" class="custom-select-input" value="{{ old('country') ?? 'ua' }}">
                  <div class="selected-block custom-select-active-value">{{ __('Выберите страну') }}</div>
                  <div class="select-list-block custom-select-list">
                    <div class="select-list-search custom-select-search"><input type="text" placeholder="{{ __('Поиск') }}"></div>
                    @foreach ($countries as $country)
                      <div data-value="{{ $country->code }}">{{ $country->$NameField }}</div>
                    @endforeach
                  </div>
                </div>
                <div class="select-block custom-select city-select">
                  <p>{{ __('Город') }}: *</p>
                  <input type="hidden" name="city_id" class="custom-select-input" value="{{ old('city_id') }}">
                  <div class="selected-block custom-select-active-value">{{ __('Выберите город') }}</div>
                  <div class="select-list-block custom-select-list">
                    <div class="select-list-search custom-select-search"><input type="text" placeholder="{{ __('Поиск') }}"></div>
                  </div>
                </div>
                <div class="input-field city-block">
                  <p>{{ __('Город') }}: *</p>
                  <input type="text" name="city" class="custom-select-input" value="{{ old('city') }}">
                </div>
                <div class="input-field">
                    <p>{{ __('Телефон') }}: *</p>
                    <input type="text" name="phone" value="{{ old('phone') }}">
                </div>
                <div class="input-field">
                    <p>{{ __('E-mail (логин)') }}: *</p>
                    <input type="text" name="email" value="{{ old('email') }}">
                </div>
                <div class="input-field">
                    <p>{{ __('Пароль') }}: *</p>
                    <input type="password" name="password">
                </div>
                <div class="input-field">
                    <p>{{ __('Повторите пароль') }}: *</p>
                    <input type="password" name="password_confirmation">
                </div>
                <button class="btn-orange">{{ __('Регистрация') }}</button>
            </div>
        </form>
    </div>
</div>
@endsection

@push('footer-scripts')
<script src="/template/js/dist/register.min.js"></script>

<script>
    fbq('track', 'InitiateCheckout');
    const countries = @json($countries);

    $(function() {
      action($('input[name=country]').val());

      $('input[name=country]').on('change', function() {
        action($(this).val());
      });

      $.ajax({
        url: 'v1/cities',
        dataType: 'json',
        data: {search: ''},
        success: function(data) {
          const selectBlock = $('.city-select');
          let selectList = selectBlock.find('.custom-select-list');
          for (let i in data) {
            const option = $('<div>').data('value', data[i].id).html(data[i].name_ru);
            selectList.append(option);
          }
        }
      });

      function action(countryCode) {
        $('input[name=phone]').inputmask({mask: countries[countryCode].mask, clearMaskOnLostFocus: false});

        if (countryCode === 'ua') {
          $('.city-select').show();
          $('.city-block').hide();
        } else {
          $('.city-select').hide();
          $('.city-block').show();
        }
      }
    });
</script>
@endpush
