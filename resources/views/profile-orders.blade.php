@extends('layout')

@section('content')
<div class="profile-block-wrapper wrapper clearfix">
    <div class="profile-content-block right-block">
        @if ($waybills->count())
        <div class="title-block">{{ __('Мои ТТН') }}</div>

        <div class="profile-waybills">
            @foreach ($waybills as $waybill)
            <p>{{ $waybill->created_at->format('d.m.Y') }} - <b>{{ $waybill->waybill }}</b></p>
            @endforeach
        </div>
        @endif

        <div class="profile-active-tab">{{ __('Заказы') }}</div>

        <div class="page">
          <div class="wrapper clearfix w-100">
              <div class="w-100">
                  @if (isset($user) && $user->debt != 0)
                  <div class="profile-mobile__show" style="color: #ff9f15">{{ __('Сумма долга') }}: {{ $user->debt }}</div>
                  @endif

                  <h2 class="title-block">{{ __('Поиск') }}</h2>
                  <form method="get" action="">
                      <div class="search-orders-block">
                          <div class="main-left-block">
                              <div class="input-field float-left">
                                  <p>Дата от:</p>
                                  <input type="date" name="start_date" value="{{ request('start_date') }}">
                              </div>
                              <div class="input-field float-left">
                                  <p>Дата до:</p>
                                  <input type="date" name="end_date" value="{{ request('end_date') }}">
                              </div>
                              <div class="input-field float-right">
                                  <p>ФИО:</p>
                                  <input type="text" name="fio" value="{{ request('fio') }}">
                              </div>
                              <div class="input-field float-left">
  {{--                                    <input type="text" name="status" value="{{ request('status') }}">--}}
                                  <div class="select-block custom-select city-select">
                                    <p>Статус:</p>
                                    <input type="hidden" name="status" class="custom-select-input" value="" data-preset="">
                                    <div class="selected-block custom-select-active-value">
                                      @foreach ($status as $item)
                                      {{ request('status') == $item['status_code'] ? $item['ru'] : '' }}
                                      @endforeach
                                    </div>
                                    <div class="select-list-block custom-select-list">
                                      @foreach ($status as $item)
                                        <div data-value="{{ $item['status_code'] }}">{{ $item['ru'] }}</div>
                                      @endforeach
                                    </div>
                                  </div>
                              </div>
                          </div>
                          <div class="main-right-block">
                              <div class="input-field float-left">
                                  <p>Телефон:</p>
                                  <input type="text" name="phone" value="{{ request('phone') }}">
                              </div>
                              <div class="input-field float-right">
                                  <p>Номер заказа:</p>
                                  <input type="text" name="id" value="{{ request('id') }}">
                              </div>
                              <div class="input-field float-left">
                                  <p>Комментарий:</p>
                                  <input type="text" maxlength="255" name="user_comment" value="{{ request('user_comment') }}">
                              </div>
                              <div class="input-field float-right">
                                  <div class="select-block custom-select city-select">
                                      <p>Способ доставки:</p>
                                      <input type="hidden" name="delivery_type_id" class="custom-select-input" value="" data-preset="">
                                      <div class="selected-block custom-select-active-value">
                                          {{request('delivery_type_id') == 1 ? 'Отделение Новой Почты' :
                                           (request('delivery_type_id') == 2 ? 'Курьер Новой Почты' :
                                           (request('delivery_type_id') == 3 ? 'Другое' : ' Способ доставки'))}}
                                      </div>
                                      <div class="select-list-block custom-select-list">
                                          <div data-value="1"> Отделение Новой Почты</div>
                                          <div data-value="2"> Курьер Новой Почты</div>
                                          <div data-value="3"> Другое</div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="main-left-block">
                              <div class="input-field float-left">
                                  <p>Отделение:</p>
                                  <input type="text" name="branch" value="{{ request('branch') }}">
                              </div>
                          </div>
                          <div class="button-block">
                              <div class="field button-field">
                                  <button type="submit" class="pure-button pure-button-yellow">{{ __('Поиск') }}</button>
                                  <button type="button" class="pure-button pure-button-secondary ml-1 cancel-button" data-url="{{ route('profile_orders') }}">{{ __('Сброс') }}</button>
                                  <button type="button" class="button pure-button btn-green ml-1" id="btn-export-orders">{{ __('Экспорт в excel') }}</button>
{{--                                  <a href="{{ custom_route('export_orders') }}" class="button pure-button btn-green ml-1"--}}
{{--                                     id="btn-export-orders" style="border: 0;">Экспорт в excel</a>--}}
                              </div>
                          </div>
                      </div>
                  </form>
              </div>

          </div>
          <div class="title-block">{{ __('Мои заказы') }}</div>
          @foreach ($orders as $order)
          <div class="profile-order">
              <div class="profile-order-number">
                  <div>{{ __('Заказ') . ' №' . $order->id }}</div>
              </div>
              <p class="profile-order-time">{{ date('d.m.Y в H:i', $order->time) }}</p>

              @isset($order->waybill->scheduled_delivery_date)
                  <p>
                      {{ __('Статус заказа') }} :
                      <span class="text-bold text-latobold">{{ $order->waybill->status }}</span>
                  </p>
                  @if($order->waybill->date_return_cargo)
                      <p>
                          {{ __('Дата возврата посылки') }}:
                          <span class="text-danger text-bold text-latobold">{{ $order->waybill->date_return_cargo->format('d-m-Y') }}</span>
                      </p>
                  @endif
              @endisset

              <div class="clearfix">
                  <div class="profile-order-column">
                      @if ($order->dropshipping)
                      <p><b>{{ __('Дропшиппинг заказ') }}</b></p>
                      @endif

                      <p>
                          <b>{{ __('ФИО') }}:</b> {{ $order->firstname . ' ' . $order->lastname }}<br>
                          <b>{{ __('Телефон') }}:</b> {{ $order->phone }}
                      </p>

                      <p>
                          <b>{{ __('Способ доставки') }}:</b> {{  $order->delivery_type_id ? [1 => __('Отделение Новой Почты'), 2 => __('Курьер Новой Почты'), 3 => __('Другое')][$order->delivery_type_id] : '' }}<br>
                          <b>{{ __('Город') }}: </b> {{ $order->city }}<br>

                          @if ($order->delivery_type_id)
                              <b>{{ [1 => __('Отделение'), 2 => __('Адрес'), 3 => __('Адрес')][$order->delivery_type_id] }}:</b>

                              @if ($order->delivery_type_id == 1)
                                  {{ optional($order->warehouse)->{'address_' . app()->getLocale() } }}
                              @elseif ($order->delivery_type_id == 2)
                                  {{ ($order->street ? $order->street . ', ' : '') . $order->street_building }}
                              @else
                                  {{ $order->address }}
                              @endif
                          @endif
                      </p>

                      @if ($order->dropshipping)
                      <p>
                          <b>{{ __('Способ оплаты') }}:</b> {{ $order->payment_type_id ? [1 => __('Наложенный платеж'), 2 => __('Предоплата')][$order->payment_type_id] : '' }}<br>

                          @if ($order->payment_type_id == 1)
                          <b>{{ __('Сумма') }}:</b> {{ +$order->dropshipping_price }} грн<br>
                          @endif

                          <b>{{ __('Стоимость доставки') }}:</b> {{ +$order->delivery_price }} грн
                      </p>
                      @endif

                      @if ($order->user_comment)
                      <p>
                          <b>{{ __('Комментарий') }}:</b> {{ $order->user_comment }}
                      </p>
                      @endif

                      @if ($order->delivery_comment)
                        <p>
                          <b>{{ __('Комментарий к доставке') }}:</b> {{ $order->delivery_comment }}
                        </p>
                      @endif
                  </div>
                  <div class="profile-order-column">
                      @if (!empty($order->hesPriceType()))
                        <div><b>{{ __('Тип цены') }}:</b> {{ $order->getPriceType() }}</div>
                      @endif
                      @foreach ($order->products as $product)
                      @if ($product->product)
                      <p>
                          {{ $product->count }} x
                          <a href="{{ custom_route('product', [$product->product->url, $product->product->id]) }}" target="_blank">
                              {{ $product->product->title . ($product->product->color_name ? ' (' . $product->product->color_name . ')' : '') }}
                          </a>
                          <span class="text-orange">{{ ($product->full_border) ? __('Закрашивать борта') : '' }}</span>
                          $ {{ $product->price }}
                      </p>
                      @endif
                      @endforeach

                      @foreach ($order->products as $product)
                        @if ($product->productCover)
                          <p>
                              <a ref="#" class="link__onclick"
                                  data-image="{{ $product->productCover->image }}"
                                  data-title="{{ $product->productCover->title }}">
                              {{ $product->count }} x
                              {{ $product->productCover->title }}
                            </a>
                            <span class="text-orange">{{ ($product->full_border) ? __('Закрашивать борта') : '' }}</span>
                            $ {{ $product->price }}
                          </p>
                        @endif
                      @endforeach

                      <br>

                      <p><b>{{ __('Итого') }}: ${{ $order->total }}</b></p>

                      @if ($order->waybills->count())
                      <p><b>{{ __('ТТН') }}:</b> {{ $order->waybills->pluck('waybill')->implode(', ') }}</p>
                      @endif
                  </div>
              </div>
          </div>
          @endforeach

          <div class="pagination-block">
              <ul>{!! str_replace('?page=', '/', $orders->withQueryString()->links()) !!}</ul>
          </div>
        </div>
    </div>

    <div class="static-page-popup popup-block"></div>

    @include('partials.profile-submenu')
</div>

<div class="modal" id="filter-export-orders">
  <div class="modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <div class="modal-title">{{ __('Выберите период для выгрузки') }}</div>
        <button type="button" class="close-modal"></button>
      </div>

      <div class="modal-body">
        <div class="modal-wrapper flex flex-direction-row flex-wrap justify-center" style="align-items: center; width: 320px">
          <div class="input-field" style="margin: 0 10px; width: 100%">
            <p>Дата от:</p>
            <input type="date" id="start_date" value="{{ now()->format('Y-m-d') }}">
          </div>
          <div class="input-field " style="margin: 0 10px; width: 100%">
            <p>Дата до:</p>
            <input type="date" id="end_date" value="{{ now()->format('Y-m-d') }}">
          </div>

          <div style="width: 100%; text-align: center; margin-top: 10px;">
            <a href="/profile/orders/export" class="button btn-green" id="export">{{ __('Выгрузить') }}</a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection

@push('footer-scripts')
<script src="/template/js/dist/custom.min.js?1.0.1"></script>
<script>
  $(function() {
    $('.profile-active-tab').click(function() {
      $('.page').toggle();
    });

    $('#btn-export-orders').click(function() {
      $('#filter-export-orders').addClass('modal-show');
    });

    $('#start_date, #end_date').change(function() {
      const url = $.param({
        start: $('#start_date').val(),
        end: $('#end_date').val()
      });
      $('#export').attr('href', '/profile/orders/export?' + url);
    });
  });
</script>
@endpush
