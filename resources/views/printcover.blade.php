@extends('layout')

@section('content')
{{--  <input type="button" value="отправить" id="test" />--}}
  <div class="design-step">
    <div class="design-step__item">
      <img src="/template/images/design_step_1.png" />
      <div class="text">1. Выбери модель телефона</div>
    </div>
    <div class="design-step__item">
      <img src="/template/images/design_step_2.png" />
      <div class="text">2. Загрузи изображение</div>
    </div>
    <div class="design-step__item">
      <img src="/template/images/design_step_3.png" />
      <div class="text">3. Оформи заказ</div>
    </div>
    <div class="design-step__item">
      <img src="/template/images/design_step_4.png" />
      <div class="text">4. Получи заказ</div>
    </div>
  </div>

  <iframe src="https://prints.vchehle.ua/constructor?partner=YZ5asH&"
          id="construtor-YZ5asH"
          name="constructor"
          data-offset="50"
          width="100%"
          height="800"
          frameborder="0"
          scrolling="0">
  </iframe>

  <div class="information">
    <div class="block__item">
      <span class="img"><img src="/template/images/term_icon.svg" /></span>
      <span class="bold">Срок изготовления</span>&nbsp;- 2 дня
    </div>
    <div class="block__item">
      <span class="img"><img src="/template/images/image_icon.svg" /></span>
      <span><ul>
        <li><span class="bold">Изображение</span>&nbsp;- не менее 1024х768</li>
        <li><span class="bold">Форм-фактор</span>&nbsp;- накладка</li>
        <li><span class="bold">Материал</span>&nbsp;- 3D пластик, силикон</li>
      </ul>
      </span>
    </div>
  </div>
@endsection

@push('footer-scripts')
  <script>
    const constrlYZ5asH = document.getElementById("construtor-YZ5asH");
    const constrlYZ5asHOffset =+ constrlYZ5asH.dataset.offset || 0;

    window.innerHeight < constrlYZ5asH.clientHeight + constrlYZ5asHOffset && (constrlYZ5asH.style.height = window.innerHeight - constrlYZ5asHOffset + "px");
    window.addEventListener("blur", function() {
      document.activeElement === constrlYZ5asH && window.innerWidth < 650 && window.scrollTo(0, constrlYZ5asH.offsetTop - constrlYZ5asHOffset)
    });

    // const btn = document.getElementById('test');
    // btn.addEventListener('click', function() {
    //   $.post('/test')
    //   .done(function(response) {
    //     let data = JSON.parse(response);
    //     cover.addToCart(data);
    //   });
    // });

    window.addEventListener('message', function(e) {
      if (e.data && e.data.event === 'print-add-to-cart' && e.origin === 'https://prints.vchehle.ua') {
        const design = e.data.data;
        // console.log(design);
        cover.addToCart(design);
      }
    });

    const cover = {
      addToCart : function(product) {
        // Добавление товара в корзину
        $.ajax({
          url: '/cart/cover/add',
          type: 'post',
          data: {
            cover: product,
          },
          dataType: 'json',
          success: function(data) {
            // Вызов конверсий
            console.log(data);
            app.showPopupMessage(__('товары добавлены в') + ' <a href="' + $('.header-cart-block a').attr('href')
              + '">' + __('вашу корзину') + '</a>!');
            app.updateHeaderCartState(data.response);
            fbq('track', 'Add to cart');
          },
          error: function(error) {
            console.error(error);
            app.showPopupMessage(__('произошла ошибка. товар не удалось добавить в корзину') );
          }
        });
      }
    };
  </script>
@endpush
