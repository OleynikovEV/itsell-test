@extends('layout')

@section('content')
  <div class="static-page">
    <p class="wrapper">
      <div class="title-block text-center">{{ __('Ваша корзина пуста!') }}</div>
      <p class="text-center">
        {{ __('Возможно вы уже оформили этот заказ ранее') }}.<br>
        {{ __('Вы можете проверить все свои заказы в личном кабинете в разделе ') }} <a class="orange-link" href="{{ route('profile_orders') }}">{{ __('Мои заказы') }}</a>.<br>
        {{ __('Если Вашего заказа нет, обратитесь к своему менеджеру и сообщите об ошибке') }}. <br>
        @if ($lastOrder->products->count() > 0)
          <p class="text-center"><b>{{ __('Ваш последний заказ:') }} {{ '#' . $lastOrder->id }}</b>
          <div class="flex flex-direction-column align-center" style="margin-left: auto; margin-right: auto;">
          @foreach ($lastOrder->products as $product)
            @if ($products->has($product->product_id))
            <div>
              {{ $product->count }} X {{ $products->get($product->product_id)->title  }}
            </div>
            @endif
          @endforeach
          </div></p>
        @endif
      </p>
    </div>
  </div>
@endsection