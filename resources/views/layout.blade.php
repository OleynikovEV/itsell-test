<!DOCTYPE html>
<html lang="ru">
<head>
    <title>{!! isset($layoutMetaTitle) ? $layoutMetaTitle : '' !!}</title>
    <meta name="description" content="{{ isset($layoutMetaDescription) ? $layoutMetaDescription : '' }}">

    <meta property="og:title" content="{{ isset($layoutMetaTitle) ? $layoutMetaTitle : '' }}">
    <meta property="og:description" content="{{ isset($layoutMetaDescription) ? $layoutMetaDescription : '' }}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="google-site-verification" content="5cDhFJSNrjyPgCehYmMs5n02RFlCea2T2Ho074mwmng" />
    <meta name="google-site-verification" content="1Hg84QYVpFfhHmDP0aw0xdxhWVD0yTrfW0tLshs8m-k" />

    <meta name="facebook-domain-verification" content="ykpku1iw7upm1coyto7m5eod0a4tc1" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="/template/images/favicon.png">
    <link rel="stylesheet" href="/template/css/bundle.min.css?v=1.2.11">

    @if (isset($layoutCanonicalLink))
    <link rel="canonical" href="{{ $layoutCanonicalLink }}" />
    @endif

    <!-- Google Tag Manager -->
    <script defer src="https://www.googletagmanager.com/gtag/js?id=UA-46489296-11"></script>
    <script defer>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-46489296-11');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '156285309814542');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=156285309814542&ev=PageView&noscript=1"
      /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Hotjar Tracking Code for https://itsellopt.com.ua -->
    <script>
      (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2094593,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.defer=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <script type="application/ld+json">
      { "@context" : "https://schema.org",
        "@type" : "Organization",
        "name": "ItsellOpt",
        "url" : "https://itsellopt.com.ua/",
        "logo" : "https://itsellopt.com.ua/template/images/logo.png",
        "email": "director@itsellopt.com.ua",
        "address": {
          "@type": "PostalAddress",
          "addressCountry": "Ukraine"
        },
        "sameAs" : [],
        "contactPoint" : [
          {
            "@type" : "ContactPoint",
            "telephone" : "{{ settings('wholesale_phone') }}",
            "email" : "{{ settings('wholesale_email') }}",
            "contactType" : "customer service"
          } ,
          {
            "@type" : "ContactPoint",
            "telephone" : "{{ settings('dropshipping_phone') }}",
            "email" : "{{ settings('dropshipping_email') }}",
            "contactType" : "customer service"
          }
        ] }
    </script>

    @stack('css-head')

    @stack('json-ld')
</head>
<body class="{{ implode(' ', array_filter([
    isset($layoutMainPage) ? 'main-page' : '',
    isset($layoutProductsListPage) ? 'category-page' : '',
    $layoutHeaderBanner ? 'with-header-banner' : ''
])) }}">

    <!-- Header -->
    <header>

        @if (isset($layoutHeaderBanner) && $layoutHeaderBanner)
        <div class="header-banner-block" {!! $layoutHeaderBanner->meta ? 'style="background: #' . $layoutHeaderBanner->meta->bg_left . '"' : '' !!}>
            <a {!! $layoutHeaderBanner->link ? 'href="' . $layoutHeaderBanner->link . '"' : '' !!} target="_blank">
                <img src="{{ config('custom.content_server') . $layoutHeaderBanner->image }}" alt="{{ $layoutHeaderBanner->alt }}">
            </a>
        </div>
        @endif

        <div class="wrapper clearfix">
            <!-- Header Logo Block -->
            <div class="header-logo-block left-block">
                <a href="{{ custom_route('main_page') }}"><img src="/template/images/logo.png" srcset="/template/images/logo@2x.png 2x" alt="Itsell OPT"></a>
            </div>
            <!-- #End Header Logo Block -->

            <!-- Header Search Block -->
            <div class="header-search-block left-block" itemscope itemtype="https://schema.org/WebSite">
                <meta itemprop="url" content="https://itsellopt.com.ua" />

                <form class="header-search-form" method="post"
                      itemprop="potentialAction" itemscope itemtype="https://schema.org/SearchAction">
                    <meta itemprop="target" content="https://itsellopt.com.ua/search/{search_phrase}" />
                    <button></button>
                    <input type="text" name="search_phrase" placeholder="{{ __('Поиск по товарам') }}..."
                           itemprop="query-input" autocomplete="off">
                </form>

                <div class="header-search-popup"></div>
                <a href="#" class="header-search-close btn-close"></a>
            </div>
            <!-- #End Header Search Block -->

            <!-- Header Contacts Block -->
            <div class="header-contacts-block right-block">
                <p><a href="tel:{{ preg_replace('/[^0-9]/', '', settings('wholesale_phone')) }}">{{ settings('wholesale_phone') }}</a></p>
                <a href="mailto:{{ settings('wholesale_email') }}">{{ settings('wholesale_email') }}</a>

                <div class="header-contacts-popup header-popup-block clearfix">
                  <div>
                    <div class="left-block">
                        <h3>{{ __('Информация') }}</h3>
                        {!! settings('contacts_info') !!}
                    </div>
                    <div class="left-block">
                        <h3>{{ __('По вопросам ОПТа') }}</h3>
                        <p>
                          <a href="tel:{{ preg_replace('/[^0-9]/', '', settings('wholesale_phone')) }}">{{ settings('wholesale_phone') }}</a>
                        </p>
                        <p><a href="mailto:{{ settings('wholesale_email') }}">{{ settings('wholesale_email') }}</a></p>
                        <p>
                          <a href="tg://resolve?domain=Ksenia_Itsellopt" target="_blank" class="social-button telegram"></a>
                          <a href="{{ settings('social_vb') }}" target="_blank" class="social-button viber"></a>
                        </p>

                        <p style="margin-top: 30px;">
                          <h3>Руководитель Отдела Продаж</h3>
                          <p>+380635094791 - Александр</p>
                        </p>
                    </div>
                    <div class="left-block">
                        <h3>{{ __('По вопросам дропшиппинга') }}</h3>
                        <p><a href="tel:{{ preg_replace('/[^0-9]/', '', settings('dropshipping_phone')) }}">{{ settings('dropshipping_phone') }}</a></p>
                        <p><a href="mailto:{{ settings('dropshipping_email') }}">{{ settings('dropshipping_email') }}</a></p>
                        <p>
                          <a href="tg://resolve?domain=dropitsellopt" target="_blank" class="social-button telegram"></a>
                          <a href="{{ settings('social_vb_drop') }}" target="_blank" class="social-button viber"></a>
                        </p>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="text-center logo-in-menu">
                      <a href="{{ settings('social_tg') }}" target="_blank" class="">
                        <span class="social-button telegram"></span>
                        <b>Telegram канал, ITsellOPT - Делай, что любишь</b> 💛
                      </a>
                  </div>
                </div>
            </div>
            <!-- #End Header Contacts Block -->

            <!-- Header Navbar Block -->
{{--            <div class="header-navbar-block right-block">--}}
            <div class="header-navbar-block right-block">
                <p class="exchange"><b>{{ __('Курс') }}: {{ settings('dollar') }} грн</b></p>
                <ul>
                    <li><a href="{{ custom_route('static_page', ['delivery']) }}">{{ __('Доставка и оплата') }}</a></li>
                    <li><a href="{{ custom_route('static_page', ['service']) . '#advantages-tab-4' }}">{{ __('Система лояльности') }}</a></li>
                    <li><a href="{{ custom_route('price_list') }}">{{ __('Прайс лист') }}</a></li>
                    <li class="login-link @auth{{ 'profile-menu' }}@endauth">
                        @guest
                        <a href="{{ custom_route('login') }}">{{ __('Вход') }}</a>
                        @endguest

                        @auth
                            <a href="#">{{ __('Профиль') }}</a>
                            <ul class="profile-menu-popup-block header-popup-block clearfix">
                              <li><a href="{{ custom_route('profile') }}">{{ __('Моя информация') }}</a></li>
                              <li><a href="{{ custom_route('profile_orders') }}">{{ __('Мои Заказы') }}</a></li>
                              <li><a href="{{ custom_route('wish_list') }}">{{ __('Лист ожиданий') }}</a></li>
                              <li><a href="{{ custom_route('profile_price') }}">{{ __('Экспорт товаров') }}</a></li>
                              <li><a href="{{ custom_route('recently_viewed') }}">{{ __('Недавно просматривали') }}</a></li>
                              <li><a href="{{ custom_route('logout') }}">{{ __('Выход') }}</a></li>
                            </ul>
                        @endauth
                    </li>
                    <li class="client-link">
                      <a href="#">{{ __('Клиентам') }} @if ($newFunctionality > 0) <span class="text-orange">({{ $newFunctionality }})</span> @endif</a>
                        <ul class="client-popup-block header-popup-block clearfix">
                            <li>
                                <p>itsellopt</p>
                                <a href="{{ custom_route('static_page', ['contacts']) }}">{{ __('Контакты') }}</a>
                                <a href="{{ custom_route('static_page', ['about']) }}">{{ __('О нас') }}</a>
                                <a href="{{ custom_route('static_page', ['mission']) }}">{{ __('Миссия и ценности') }}</a>

                                @if (false)
                                <a href="#">{{ __('Новости') }}</a>
                                @endif
                            </li>
                            <li>
                                <p>{{ __('Клиентам') }}</p>
                                <a href="{{ custom_route('static_page', ['service']) }}">{{ __('ITsellOpt Сервисы') }} <span style="vertical-align: text-top;">&#128077;</span></a>
                                <a href="{{ custom_route('static_page', ['delivery']) }}">{{ __('Доставка и оплата') }}</a>
                                <a href="{{ custom_route('static_page', ['warranty']) }}">{{ __('Гарантия и возврат') }}</a>
                                <a href="#" class="feedback-form-open" data-guest="{{ auth()->guest() }}">{{ __('Написать директору') }}</a>
                            </li>
                            <li>
                                <p>{{ __('Помощь') }}</p>
                                <a href="{{ custom_route('static_page.new') }}"
                                   @if ($newFunctionality > 0) class="text-orange" @endif>
                                   {{ __('Новый функционал') }} @if ($newFunctionality > 0) ({{ $newFunctionality }}) @endif
                                </a>
                                <a href="{{ custom_route('static_page', ['features-faq']) }}">{{ __('FAQ по функциям сайта') }}</a>
                                <a href="{{ custom_route('static_page', ['features-faq']) . '#7.%20Дропшиппинг' }}">{{ __('FAQ Дропшиппинг') }}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #End Header Navbar Block -->

            <div class="mobile-btn-menu">
                <div class="mobile-btn-menu__img"></div>
            </div>
        </div>
    </header>
    <!-- #End Header -->

    <!-- Header Menu Block -->
    <div class="header-menu-block">
        <div class="wrapper clearfix">
            <!-- Menu -->
            <menu>
                <ul>
                    <li class="menu-title-block"><span>{{ __('Каталог товаров') }}</span>
                        <ul class="submenu-block">
                            @foreach ($layoutCategories->where('vertical_menu', true) as $layoutCategory)
                            @if ($loop->index < 7 && $layoutCategory->sectors)
                            <li>
                                @if ($layoutCategory->id === 1767 || $layoutCategory->id === 1781)
                                    <a ref="#">
                                        <img src="{{ config('custom.content_server') . $layoutCategory->icon }}">{{ $layoutCategory->title }}
                                    </a>
                                @else
                                    <a href="{{ custom_route('category', ['others', $layoutCategory->name]) }}">
                                        <img src="{{ config('custom.content_server') . $layoutCategory->icon }}">{{ $layoutCategory->title }}
                                    </a>
                                @endif
                                <div class="submenu-category-block clearfix {{ $layoutCategory->mainMenuBanner ? 'with-banner' : '' }}">
                                    @if ($layoutCategory->mainMenuBanner)
                                    <div class="submenu-banner-block">
                                        @if ($layoutCategory->mainMenuBanner->link)
                                        <a href="{{ $layoutCategory->mainMenuBanner->link }}" target="_blank">
                                        @endif

                                            <img src="{{ config('custom.content_server') . $layoutCategory->mainMenuBanner->image }}"
                                                 alt="{{ $layoutCategory->mainMenuBanner->alt }}"
                                                  style="max-width: 230px">

                                        @if ($layoutCategory->mainMenuBanner->link)
                                        </a>
                                        @endif
                                    </div>
                                    @endif

                                    <ul>
                                        @if ($layoutCategory->id === 1767)
                                        <li class="submenu-category-title">{{ 'Принты' }} <span style="text-transform: lowercase!important;">its</span>P<span style="text-transform: lowercase!important;">rint</span> <span style="color: red">(New !)</span></li>
                                          <LI>
                                            <a href="{{ custom_route('printcover') }}">{{ __('Свой дизайн чехлов') }}</a>
                                          </LI>
                                          <li>
                                            <a href="{{ custom_route('category', ['phones', $layoutCreativeIPrintUrl]) }}">{{ __('Креативы') }} ({{ $layoutCreativeIPrintAmount }})</a>
                                          </li>
                                          <li>
                                            <a href="{{ custom_route('products_series', [$layoutSeriesIPrintName]) }}">{{ __('Модельный ряд') }} ({{ $layoutSeriesIPrintAmount }})</a>
                                          </li>
                                        @endif
                                        @foreach ($layoutCategory->sectors as $layoutSector)
                                        @if ($layoutSector->subcategories->count())
                                            <li class="submenu-category-title">{{ $layoutSector->title }}</li>

                                            @foreach ($layoutSector->subcategories as $layoutSubcategory)
                                            <li>
                                                <a href="{{ custom_route('category', ['others', $layoutSubcategory->name]) }}">
                                                    {{ trim(str_replace($layoutCategory->title, '', $layoutSubcategory->title)) }}
                                                    ({{ $layoutSubcategory->products_count}})

                                                    @if ($layoutSubcategory->products_count_new)
                                                    <span class="text-green">+{{ $layoutSubcategory->products_count_new }}</span>
                                                    @endif
                                                  @if ($layoutSubcategory->products_count_coming)
                                                    <span class="text-orange">+{{ $layoutSubcategory->products_count_coming }}</span>
                                                  @endif
                                                </a>
                                            </li>
                                            @endforeach
                                        @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                            @endif
                            @endforeach
                            <li>
                                <a href="{{ custom_route('brands') }}"><img src="/template/images/catalog_icons/brands_icon.svg">{{ __('Бренды') }}</a>
                                <div class="submenu-category-block">
                                    <ul>
                                        @foreach ($layoutBrands as $layoutBrand)
                                        <li>
                                            <a href="{{ custom_route('brand', [$layoutBrand->name]) }}">
                                                {{ $layoutBrand->title }}
                                                ({{ $layoutBrand->products_count }})

                                                @if ($layoutBrand->products_count_new)
                                                <span class="text-green">+{{ $layoutBrand->products_count_new }}</span>
                                                @endif

                                                @if ($layoutBrand->products_count_coming)
                                                  <span class="text-orange">+{{ $layoutBrand->products_count_coming }}</span>
                                                @endif
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </menu>
            <!-- #End Menu -->

            <!-- Header Cart Block -->
            <div class="header-cart-block right-block">
                <a href="{{ custom_route('cart') }}">
                    {{ __('Корзина') }}

                    @if (Cart::count() > 0)
                    <span>{{ Cart::count() }}</span>
                    @endif
                </a>
            </div>
            <!-- #End Header Cart Block -->

            <!-- Header Fast Link -->
            <div class="header-fast-link right-block">
                <a href="{{ custom_route('section', ['soon']) }}" class="soon-link">{{ __('Скоро') }}</a>
                <a href="{{ custom_route('section', ['novelties']) }}" class="new-link">{{ __('Новинки') }}</a>
                <a href="{{ custom_route('section', ['coming']) }}" class="coming-link">{{ __('Поступления') }}</a>
                <a href="{{ custom_route('section', ['sale']) }}" class="sale-link">{{ __('Sale до') }} -70%</a>
            </div>
            <!-- #End Header Fast Link -->

            <!-- Menu Models Block -->
            <div class="menu-models-block">
                <ul class="clearfix">
                    <li></li>
                    @foreach ($layoutCategories->where('vertical_menu', false) as $layoutCategory)
                    @if ($loop->index < 8)
                    <li class="flex flex-direction-row">
                        <a href="{{ custom_route('category', [ 'phones', $layoutCategory->name]) }}">
                            {{ $layoutCategory->title }}

                          <div class="amount-content">
                            @if ($layoutCategory->products_count_new > 0)
                              <span class="top">{{ $layoutCategory->products_count_new }}</span>
                            @endif

                            @if ($layoutCategory->products_count_coming > 0)
                              <span class="bottom">{{ $layoutCategory->products_count_coming }}</span>
                            @endif
                          </div>
                            <div class="arrow"></div>
                        </a>

                        <div class="submenu-category-block clearfix {{ $layoutCategory->mainMenuBanner ? 'with-banner' : '' }}">
                            @if ($layoutCategory->mainMenuBanner)
                            <div class="submenu-banner-block">
                                @if ($layoutCategory->mainMenuBanner->link)
                                <a href="{{ $layoutCategory->mainMenuBanner->link }}" target="_blank">
                                @endif

                                    <img src="{{ config('custom.content_server') . $layoutCategory->mainMenuBanner->image }}"
                                         alt="{{ $layoutCategory->mainMenuBanner->alt }}"
                                          style="max-width: 230px">

                                @if ($layoutCategory->mainMenuBanner->link)
                                </a>
                                @endif
                            </div>
                            @endif

                            <ul>
                                @foreach ($layoutCategory->sectors as $layoutSector)
                                @if ($layoutSector->subcategories->count())
                                    <li class="submenu-category-title">{{ $layoutSector->title }}</li>

                                    @foreach ($layoutSector->subcategories as $layoutSubcategory)
                                    <li>
                                        <a href="{{ custom_route('category', ['phones', $layoutSubcategory->name]) }}">
                                            {{ trim(str_replace($layoutCategory->title, '', $layoutSubcategory->title)) }}
                                            ({{ $layoutSubcategory->products_count}})

                                            @if ($layoutSubcategory->products_count_new)
                                              <span class="text-green">+{{ $layoutSubcategory->products_count_new }}</span>
                                            @endif

                                            @if ($layoutSubcategory->products_count_coming)
                                              <span class="text-orange">+{{ $layoutSubcategory->products_count_coming }}</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endforeach
                                @endif
                                @endforeach

                                @if (( ! $layoutCategory->sectors || ! $layoutCategory->sectors->count()) && $layoutCategory->subcategories)
                                @foreach ($layoutCategory->subcategories as $layoutSubcategory)
                                <li>
                                    <a href="{{ custom_route('category', ['phones', $layoutSubcategory->name]) }}">
                                        {{ trim(str_replace($layoutCategory->title, '', $layoutSubcategory->title)) }}
                                        ({{ $layoutSubcategory->products_count}})

                                        @if ($layoutSubcategory->products_count_new)
                                          <span>+{{ $layoutSubcategory->products_count_new }}</span>
                                        @endif

                                        @if ($layoutSubcategory->products_count_coming)
                                          <span class="text-orange">+{{ $layoutSubcategory->products_count_coming }}</span>
                                        @endif
                                    </a>
                                </li>
                                @endforeach
                                @endif

                                {{-- Другие модели --}}
                                @if ($layoutCategory->subcategories !== null && $layoutCategory->sectors->count())
                                  <li class="submenu-category-title">{{ __('Другие') }}</li>
                                  @foreach ($layoutCategory->subcategories as $item)
                                    <li>
                                      <a href="{{ custom_route('category', ['phones', $item->name]) }}">
                                        {{ trim(str_replace($layoutCategory->title, '', $item->title)) }} ({{ $item->products_count }})

                                        @if ($item->products_count_new)
                                          <span>+{{ $item->products_count_new }}</span>
                                        @endif

                                        @if ($item->products_count_coming)
                                          <span class="text-orange">+{{ $item->products_count_coming }}</span>
                                        @endif
                                      </a>
                                    </li>
                                  @endforeach
                                @endif
                            </ul>
                        </div>
                    </li>
                    @endif
                    @endforeach

                    <li class="flex flex-direction-row">
                        <a class="show-more">{{ __('Другие') }} <span>{{ $layoutCategories->where('products_count', '>', 0)->count() - 6 }}</span></a>
                        <div class="submenu-category-block clearfix">
                            <ul>
                                @foreach ($layoutCategories->where('vertical_menu', false) as $layoutCategory)
                                @if ($loop->index > 7 && $layoutCategory->products_count > 0 && $layoutCategory->id != config('custom.accessories_category_id'))
                                  <li><a href="{{ custom_route('category', ['phones', $layoutCategory->name]) }}">{{ $layoutCategory->title }}</a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- #End Menu Models Block -->
        </div>
    </div>
    <!-- #End Header Menu Block -->

    @include('partials.breadcrumbs')

    @yield('content')

    <!-- Footer -->
    <footer>
        <div class="wrapper clearfix">
            <div class="footer-contacts-block left-block">
                <a href="#" class="footer-logo-block"><img src="/template/images/white_logo.png" alt="Itsell OPT"></a>
                <a href="tel:{{ preg_replace('/[^0-9]/', '', settings('wholesale_phone')) }}">{{ settings('wholesale_phone') }}</a>
                <p>{{ __('Звоните нам') }} {{ settings('contacts_work_time') }}</p>
                <p>{{ settings('contacts_work_days') }}</p>
                <a href="mailto:{{ settings('wholesale_email') }}">{{ settings('wholesale_email') }}</a>
                <p>{{ __('Пишите нам в любое время.') }}</p>
                <p>{{ __('Мы ответим вам в кратчайшие сроки.') }}</p>
            </div>
            <div class="footer-links-block left-block">
                <h3>{{ __('Каталог товаров') }}</h3>
                @foreach ($layoutCategories->where('vertical_menu', true) as $layoutCategory)
                <a href="{{ custom_route('category', ['others', $layoutCategory->name]) }}">{{ $layoutCategory->title }}</a>
                @endforeach
            </div>
            <div class="footer-links-block footer-accessories-links left-block">
                <h3>{{ __('Аксессуары') }}</h3>
                @foreach ($layoutCategories as $layoutCategory)
                    @if ($loop->index < 5)
                    <a href="{{ custom_route('category', ['phones', $layoutCategory->name]) }}">{{ $layoutCategory->title }}</a>
                    @endif
                @endforeach
            </div>
            <div class="footer-links-block footer-service-links left-block">
                <h3>Itsell OPT</h3>
                <a href="{{ custom_route('static_page', ['contacts']) }}">{{ __('О компании') }}</a>
                <a href="{{ custom_route('static_page', ['mission']) }}">{{ __('Миссия и ценности') }}</a>
                @if (false)
                <a href="{{ custom_route('static_page', ['contacts']) }}">{{ __('Новости') }}</a>
                <a href="{{ custom_route('static_page', ['contacts']) }}">{{ __('Видео обзоры') }}</a>
                @endif
                <a href="{{ custom_route('brands') }}">{{ __('Наши бренды') }}</a>
                <a href="{{ custom_route('static_page', ['contacts']) }}">{{ __('Контакты') }}</a>
            </div>
            <div class="footer-links-block footer-clients-links left-block">
                <h3>{{ __('Клиентам') }}</h3>
                <a href="{{ custom_route('static_page', ['services']) }}">{{ __('Сервисы') }}</a>
                <a href="{{ custom_route('static_page', ['delivery']) }}">{{ __('Доставка и оплата') }}</a>
                <a href="{{ custom_route('static_page', ['warranty']) }}">{{ __('Гарантия и возврат') }}</a>
                <a href="{{ custom_route('static_page', ['features-faq']) }}">FAQ</a>
            </div>
            <div class="mobile-footer-block">
                <div class="title-block"><b>{{ __('Поддержка') }}</b></div>
                <p>{{ __('Звоните нам') }} {{ settings('contacts_work_time') }}</p>
                <p>{{ settings('contacts_work_days') }}</p>
                <a href="tel:{{ preg_replace('/[^0-9]/', '', settings('wholesale_phone')) }}" class="phone-icon">{{ settings('wholesale_phone') }}</a>
                <a href="mailto:{{ settings('wholesale_email') }}">{{ settings('wholesale_email') }}</a>
            </div>
        </div>
    </footer>
    <div class="copyright-block clearfix">
        <div class="footer-social-block">
            <span>{{ __('Канал') }} </span>
            @if (settings('social_tg'))
            <a href="{{ settings('social_tg') }}" target="_blank" class="social-button telegram"></a>
            @endif

            <a href="tg://resolve?domain=Ksenia_Itsellopt" target="_blank" class="social-button telegram"></a>
            @if (settings('social_vb'))
            <a href="{{ settings('social_vb') }}" target="_blank" class="social-button viber"></a>
            @endif

            @if (settings('social_ig'))
            <a href="{{ settings('social_ig') }}" target="_blank" class="social-button instagram"></a>
            @endif

            @if (settings('social_yt'))
            <a href="{{ settings('social_yt') }}" target="_blank" class="social-button youtube"></a>
            @endif

            @if (settings('social_fb'))
            <a href="{{ settings('social_fb') }}" target="_blank" class="social-button fb"></a>
            @endif
        </div>
        <div class="footer-payment-block">
            <img src="/template/images/mastercard_icon.svg">
            <img src="/template/images/visa_icon.svg">
        </div>
        <span>Copyright © {{ date('Y') }}. ITsell. All rights reserved.</span>
    </div>
    <!-- Footer -->

    <!-- Mobile Sibebar Menu -->
    <div class="mobile-sidebar-menu">
        <div class="mobile-sidebar-header">
            <a class="mobile-logo-block"><img src="/template/images/logo.png" alt="ITsell OPT"></a>
            <div class="btn-close"></div>
            <div class="btn-back hidden">
                <div class="btn-back__img"></div>
            </div>
        </div>
        <div class="mobile-sidebar-content">
            <div class="mobile-sidebar-content-inner">
                <div class="mobile-content-block">
                    <p>
                      <b>{{ __('Курс') }}: {{ settings('dollar') }} грн</b> /
                      <span> {{ __('Канал') }}
                        <a href="{{ settings('social_tg') }}" target="_blank" class="social-button telegram"></a>
                      </span>
                    </p>
                    <div>
                        @guest
                        <a href="{{ custom_route('login') }}">{{ __('Вход') }} </a> /
                        <a href="{{ custom_route('register') }}"> {{ __('Регистрация') }}</a>
                        @endguest

                        @auth
{{--                          <a href="{{ custom_route('profile_orders') }}">{{ __('Профиль') }} </a>--}}
                          <div class="bg-gray cursor_p p-5 flex flex-direction-row align-center arrow_right_icon"
                               style="height: 28px;" id="profile-menu">
                            {{ __('Профиль') }}
                          </div>

                          <ul class="mobile-profile-menu bg-gray clearfix" style="display: none">
                            <li style="padding: 4px 0 4px" class="ml-10 border-top border-bottom"><a href="{{ custom_route('profile') }}">{{ __('Моя информация') }}</a></li>
                            <li style="padding: 4px 0 4px" class="ml-10 border-bottom"><a href="{{ custom_route('profile_orders') }}">{{ __('Мои Заказы') }}</a></li>
                            <li style="padding: 4px 0 4px" class="ml-10 border-bottom"><a href="{{ custom_route('wish_list') }}">{{ __('Лист ожиданий') }}</a></li>
                            <li style="padding: 4px 0 4px" class="ml-10 border-bottom"><a href="{{ custom_route('profile_price') }}">{{ __('Экспорт товаров') }}</a></li>
                            <li style="padding: 4px 0 4px" class="ml-10 border-bottom"><a href="{{ custom_route('recently_viewed') }}">{{ __('Недавно просматривали') }}</a></li>
                            <li style="padding: 4px 0 4px" class="ml-10 border-bottom"><a href="{{ custom_route('logout') }}">{{ __('Выход') }}</a></li>
                          </ul>
                        @endauth
                    </div>
                    @if ($newFunctionality > 0)
                    <div>
                        <a href="{{ custom_route('static_page.new') }}" class="text-orange">{{ __('Новый функционал') }} ({{ $newFunctionality }})</a>
                    </div>
                    @endif
                    <div>
                        <a href="tel:{{ preg_replace('/[^0-9]/', '', settings('wholesale_phone')) }}">{{ settings('wholesale_phone') }}</a>

                        <a href="tg://resolve?domain=Ksenia_Itsellopt" target="_blank" class="social-button telegram"></a>
                        <a href="{{ settings('social_vb') }}" class="social-button viber"></a>
                    </div>
                    <a href="mailto:{{ settings('wholesale_email') }}">{{ settings('wholesale_email') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="btn-page-up"></div>
    <div class="popup-bg"></div>
    <div class="header-popup-bg"></div>

    <div class="feedback-popup popup-block popup-block-sm">
        <div class="popup-title">{{ __('Написать директору') }}</div>
        <form class="feedback-form contact-director">
            @include('elements.customer-data', ['titleText' => __('Сообщение')])
            <button class="btn-orange" type="submit" id="send-form-director">{{ __('Отправить') }}</button>
            <input type="image" id="loader1" class="hidden" src="/template/images/ajax-loader.gif" />
        </form>

        <a href="#" class="btn-close"></a>
    </div>

    @if (!auth()->guest())
      <div class="subscribeModal hidden" id="subscribeModal">
        <div class="title">{{ __('Выберите способ оповещения') }}</div>
        <i class="info">{{ __('В дальнейшем вы сможете поменять способ оповещения в личном кабинете') }}</i>

        <div class="body">
          <div class="subscribe">
            <a href="viber://pa?chatURI={{ config('custom.viber_bot_url') }}&context={{ auth()->user()->id }}&text=Отправьте это сообщение чтоб подписаться" target="_blank" data-event="viber" id="hrefViber">{{ __('Получать уведомления по Viber') }}</a>
          </div>
          <div class="subscribe">
            <a href="https://telegram.me/{{ config('custom.telegram_bot_url') }}?start={{ auth()->user()->id }}" target="_blank" data-event="telegram" id="hrefTelegram">{{ __('Получать уведомления по Telegram') }}</a>
          </div>
          <div class="subscribe">
            <a data-event="email" id="hrefEmail">{{ __('Получать уведомления только по email') }}</a>
          </div>
        </div>
      </div>
    @endif

    <script>var appLocale = "{{ app()->getLocale() }}";</script>
    <script src="/template/js/dist/core.min.js?v=1.1.9"></script>

{{--    @stack('footer-scripts')--}}

    <script type="text/javascript">
        /* <![CDATA[ */
        const google_conversion_id = 979911955;
        const google_custom_params = window.google_tag_params;
        const google_remarketing_only = true;
        const getLocaleCity = @json(app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua');
        const priceTypeSiteShow = @json($priceTypeSite === 'disable');
        {{--const priceTypeSite = @json($priceTypeSite ?? 'opt_price');--}}
        {{--const priceRangeForPrint = @json($priceRangeForPrint ?? null);--}}
        /* ]]> */
    </script>

    @stack('footer-scripts')
    <script defer type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/979911955/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
</body>
</html>
