@extends('layout')

@section('content')
<!-- Слайдер -->
<div class="main-banner-block">
    <div class="wrapper">
        <div class="main-banner-content">
          @foreach ($banners as $banner)
            @if($banner->isVideo())
              <a rel="" class="youtube-block"
                   data-video-url="{{ $banner->getVideoUrl() }}"
                   data-youtube-id="{{ $banner->getYoutubeCode() }}">
                <picture>
                  <img class="youtube__cover loading" alt=""
                       data-src="{{ $banner->getCover() }}"
                       data-src-mobile="{{ $banner->getMobileCover() }}"/>
                </picture>
                <button class="youtube__button" aria-label="{{ __('Запустить видео') }}">
                  <svg width="68" height="48" viewBox="0 0 68 48">
                    <path class="youtube__button-shape" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path>
                    <path class="youtube__button-icon" d="M 45,24 27,14 27,34"></path>
                  </svg>
                </button>

              </a>
            @else
              <a href="{{ $banner->getLink() }}" target="_blank">
                <picture>
                  <img src="/template/images/ajax-loader.gif"
                       data-src="{{ $banner->getImage() }}"
                       data-src-mobile="{{ $banner->getMobileImage() }}" class="loading">
                </picture>
              </a>
            @endif
            @endforeach
        </div>
        <div class="main-banner-dots"></div>
    </div>
</div>

<div class="gradient-block increased" style="margin-top: 39px;">
    <div class="wrapper">
        <!-- Новинки -->
        @if ($newProducts->count())
        <div class="product-list-block product-list-big" style="padding: 0px">
            <div class="title-block">{{ __('Новинки') }}</div>
            <div class="product-list-wrapper clearfix">

                <div class="block" style="display: none"></div>
                @each('partials.list-product', $newProducts, 'product')

            </div>
            <div class="btn-show-more"></div>
        </div>
        @endif


        @if ($productsBanner)
        <!-- Баннер -->
        <div class="page-banner-block">
            @if ($productsBanner->link) <a href="{{ $productsBanner->link }}" target="_blank"> @endif
            <img src="{{ config('custom.content_server') . $productsBanner->image }}" alt="{{ $productsBanner->alt }}">
            @if ($productsBanner->link) </a> @endif
        </div>

        <div class="page-mobile-banner">
            @if ($productsBanner->link) <a href="{{ $productsBanner->link }}" target="_blank"> @endif
            <img src="{{ config('custom.content_server') . $productsBanner->image_mobile }}" alt="{{ $productsBanner->alt }}">
            @if ($productsBanner->link) </a> @endif
        </div>
        @endif

        <!-- Скоро в продаже -->
        @if ($waitingProducts->count())
        <div class="product-list-block product-list-big oneline">
            <div class="title-block">{{ __('Скоро в продаже') }}</div>
            <div class="product-list-wrapper clearfix">

                <div class="block" style="display: none"></div>
                @each('partials.list-product', $waitingProducts, 'product')

            </div>
            <div class="btn-show-more"></div>
        </div>
        @endif

        <!-- Аутлет -->
        @if ($saleProducts->count())
        <div class="product-list-block product-list-big product-sale-list oneline">
            <div class="title-block">Itsell OPT — {{ __('Аутлет') }} <span>{{ __('до') }} 70%</span></div>
            <div class="product-list-wrapper clearfix">

                <div class="block" style="display: none"></div>
                @each('partials.list-product', $saleProducts, 'product')

            </div>
            <div class="btn-show-more"></div>
        </div>
        @endif

    </div>
</div>

<div class="wrapper">
    <!-- Преимущества -->
    <div class="advantages-block">
        <div class="title-block">{{ __('Сервисы') }} Itsell Opt</div>
        <div class="advantages-tabs-block tabs-wrapper-block">
            <div class="tabs-title-block">
                <ul>
                    <li class="advantages-icon-1 active">
                        <a href="#advantages-tab-1">{{ __('Системная работа с шириной') }}</a>
                    </li>
                    <li class="advantages-icon-2">
                        <a href="#advantages-tab-2">{{ __('Скорость') }}</a>
                    </li>
                    <li class="advantages-icon-3">
                        <a href="#advantages-tab-3">{{ __('Новый магазин') }}</a>
                    </li>
                    <li class="advantages-icon-4">
                        <a href="#advantages-tab-4">{{ __('Система лояльности') }}</a>
                    </li>
                    <li class="advantages-icon-5">
                        <a href="#advantages-tab-5">{{ __('Отсрочка') }}</a>
                    </li>
                    <li class="advantages-icon-6">
                        <a href="#advantages-tab-6">{{ __('XML и контент') }}</a>
                    </li>
                    <li class="advantages-icon-7">
                        <a href="#advantages-tab-7">{{ __('Персональный менеджер') }}</a>
                    </li>
                </ul>
            </div>
            <div class="tabs-content-block">
                @include('partials.main-page-advantages')
        </div>
    </div>
</div>

<!-- Просмотренные товары -->
@include('partials.viewed-products')

<div class="wrapper clearfix">
    <!-- Видео -->
    <div class="video-reviews-block clearfix">
        <div class="title-block">ITsell - {{ __('info зона') }}</div>
        <div class="video-wrapper-block left-block clearfix">
            <div class="full-video-block left-block">
                <iframe width="560" height="315" frameborder="0"
                        data-src="https://www.youtube.com/embed/{{ $videos[0]['code'] }}"
                        class="lazy"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="video-list-block right-block clearfix">

                @foreach ($videos as $video)
                @if ($loop->index)
                <div class="video-list-item clearfix" data-code="{{ $video['code'] }}">
                    <div class="video-preview-block left-block">
                        <a href="#">
                            <img class="lazy" data-src="https://img.youtube.com/vi/{{ $video['code'] }}/hqdefault.jpg" alt="{{ $video['name'] }}">
                        </a>
                    </div>
                    <div class="video-description-block right-block">
                        <a href="#">{{ $video['name'] }}</a>
                        <p><b>{{ $video['date'] }}</b></p>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>

        <!-- Социальные сети -->
        <div class="social-widget-block">
            @if (false)
            <iframe class="telegram-widget" src="https://xn--r1a.website/s/Itsellopt"></iframe>
            @endif
        </div>
    </div>
</div>

@if ($seoDescription)
    <div class="wrapper">
        <div class="seo-description seo-description-hidden static-page" style="padding-top: 80px">
            {!! $seoDescription !!}
        </div>
        <div class="text-center">
            <button class="btn-white btn-seo-readmore">{{ __('Показать полностью') }}</button>
        </div>
    </div>
@endif

<div class="advantages-popup popup-block"></div>
@endsection

@push('footer-scripts')
<script>
    app.initMainPage();
</script>
@endpush
