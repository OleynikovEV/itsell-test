@extends('layout')

@section('content')
@php
    $showPrice = \Illuminate\Support\Facades\Auth::check() || !$product->show_price;
@endphp


@if ($showPrice)
  @php $priceRange = $product->getPriceRange() @endphp

  @push('json-ld')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Product",
    "name": "{{ $product->title }}",
    "image": [
      @if ($product->getBoxImage())
        "{{ $product->getBoxImage() }}",
      @endif
      "{{ config('custom.content_server') . $product->getTitleImage() }}"
    ],
    "description": "{{ $product->title }}",
    "sku": "{{ $product->id }}",
    "mpn": "{{ $product->id }}",
    "offers": {
      "@type": "AggregateOffer",
      "url": "{{ URL::current() }}",
      "priceCurrency": "USD",
      "lowPrice": "{{ $priceRange[0] }}",
      "highPrice": "{{ isset($priceRange[1]) ? $priceRange[1] : $priceRange[0] }}",
      "itemCondition": "{{ $product->isNovelty() ? 'https://schema.org/NewCondition' : 'https://schema.org/UsedCondition' }}",
      "availability": "{{ $product->isAvailability() ? 'https://schema.org/InStock' : 'https://schema.org/OutOfStock' }}"
    }
  }
  </script>
  @endpush
@endif

<div class="wrapper" >
    <!-- Product Content Block -->
    <div class="product-content-block clearfix">
        <!-- Product Photo Block Desktop -->
        <div class="product-photo-block left-block clearfix">
            <div class="product-big-photo right-block">
                <div class="product-big-photo-content product-big-photo-content-top">
                    @if ($productImages->count())
                        @foreach ($productImages as $num => $productImage)
                        <div data-product-id="{{ $productImage['id'] }}">
                            <img @if ($num <= 5) src="{{ config('custom.content_server') . $productImage['image'] }}" @else src="/template/images/ajax-loader.gif" class="opacity width-a" @endif
                                 data-src="{{ config('custom.content_server') . $productImage['image'] }}" alt="{{ $productImage['alt'] }}">
                        </div>
                        @endforeach
                    @else
                    <div>
                        <img src="/template/images/noimagelarge.jpg" alt="{{ $product->title . ($product->color_name ? ' (' . $product->color_name . ')' : '') }}">
                    </div>
                    @endif
                </div>
                <div class="product-show-all-photos product-show-all-photos-no-modal btn-red hidden"
                     data-price-opt-range="<b>$</b> {!! implode(' - <b>$</b> ', $product->getPriceRange('all', false)) !!}"
                     data-price-default="{{ $product->getPrice($priceTypeSite) }}"
                     data-price-default-uah="{{ round($product->getPrice($priceTypeSite) * settings('dollar')) }}"
                     data-price-sale="{{ ($salePrice > 0) ? '$ ' . $salePrice : null }}"
                     data-price-opt="{{ $product->getPrice() }}"
                     data-price-vip="{{ $product->getPrice('opt_vip_usd') }}"
                     data-price-dealer="{{ $product->getPrice('opt_dealer_usd') }}"
                     data-price-drop-uah="{{ $product->getPrice('drop_uah') }}"
                     data-price-ppc="{{ $product->getPrice('price') }}"
                     data-price-old="{{ $product->getPrice('old_opt_price') }}"
                     data-wish="{{ in_array($product->id, $wishList) ? 'true' : 'false' }}"
                     data-parent-id="{{ $product->id }}">{{ __('Общие фото') }}</div>
            </div>
            <div class="mobile-photo-dots"></div>
            <div class="product-photo-preview left-block">
                <div class="btn-slider-switch btn-switch-up"></div>
                <div class="product-preview-wrapper clearfix">
                    <div class="product-preview-slider product-preview-slider-top">
                        @foreach ($productImages as $num => $productImage)
                        <a href="{{ config('custom.content_server') . $productImage['image'] }}" data-product-id="{{ $productImage['id'] }}">
                            <img @if ($num <= 5) src="{{ config('custom.content_server') . $productImage['image'] }}" @else src="/template/images/ajax-loader.gif" @endif
                                 data-src="{{ config('custom.content_server') . $productImage['image'] }}" alt="{{ $productImage['alt'] }}">
                        </a>
                        @endforeach
                    </div>
                </div>
                <div class="btn-slider-switch btn-switch-down"></div>
            </div>

{{--            <div class="product-label-block product-label-big {{ $product->getLabel() }}"></div>--}}
            @php $label = $product->getLabelNew(); @endphp
            <div class="{{ $label['class'] }}" style="top: 0px">{{ $label['text'] }}</div>
        </div>
        <!-- #End Product Photo Block -->

        <!-- Product Info Block -->
        <div class="product-info-block left-block">
            <h1 class="product-title-block">{{ $product->title }}</h1>
            <div class="product-code-block">
                <div class="product-label-grey">#{{ ltrim(($product->parent && $product->parentProduct ? $product->parentProduct : $product)->baseid, '0') }}</div>

                @if ($product->qty > 0)
                  @if ($product->showLabelOneTwoDays())
                  <div class="product-label-orange product__availability" style="position: relative; padding: 4px 7px 4px 25px; border-radius: 2px; text-transform: uppercase;">{{ __('1-2 дня') }}</div>
                  @else
                    <div class="product-availability product-availability-block">{{ __('в наличии') }}</div>
                  @endif
                @else
                  @if ($product->when_appear)
                    <div class="product-availability product-availability-block soon-available" data-when-appear="{{ $product->when_appear }}">{{ __('ожидается') }}</div>
                  @else
                    <div class="product-availability product-availability-block not-available">{{ __('нет в наличии') }}</div>
                  @endif
                @endif

                @php $isWish = in_array($product->id, $wishList); @endphp
                <div class="product-availability-block cursor_p {{ $isWish ? 'wish-button-inform__select' : 'wish-button-inform' }} {{ ($product->qty > 0) ? 'hidden' : null }}"
                     data-product-id="{{ $product->id }}" data-wish="{{ $isWish ? 'true' : 'false' }}"
                     id="wish">
                  {{ $isWish ? __('Сообщим о наличии') : __('Сообщить, когда появится') }}
                </div>
            </div>

            @if ($banner->count())
            <div class="product-banner-block">
                @if ($banner->get(0)->link) <a href="{{ $banner->get(0)->link }}" id="banners_link" target="_blank"> @endif
                <img src="{{ config('custom.content_server') . $banner->get(0)->image }}" alt="{{ $banner->get(0)->alt }}"
                      id="banners">
                @if ($banner->get(0)->link) </a> @endif
            </div>
            @endif

            <div class="product-price-block">
              {{--    Вывод цен    --}}
              @include('elements.price-product-block', [
                'priceTypeSite' => $priceTypeSite,
                'product' => $product,
                'showPrice' => $showPrice,
                'salePrice' => $salePrice,
                'isModal' => false,
              ])
            </div>
            <div class="info-blocks clearfix">
                <div class="cheaper-block bordered-block left-block">
                    <p class="cheaper-icon" style="padding-left: 30px;">{{ __('Отзывы / пожелания') }}?</p>
                    <span><a href="#">{{ __('SEND') }}</a><span style="padding-left: 20px;">{{ __('Мы стремимся стать лучшими для Вас') }}!</span> </span>
                </div>

                <div class="bordered-block left-block">
                    <a href="#" class="warranty-button page-button" data-page-name="warranty">
                        <p class="exchange-icon">{{ __('Обменяем товар') }}</p>
                        <span>{{ __('Если он Вам не подошел или обнаружились неисправности') }}</span>
                    </a>
                </div>
            </div>

            @if (!auth()->guard()->guest() && (auth()->user()->isAdmin() || auth()->user()->isCopywriter()) && !$product->isPrint())
              <div><a href="{{ route('product-color.update', ['id' => $product->id]) }}" target="_blank">Определить цвет</a></div>
            @endif

            <!-- Product Colors Block -->
            @if (!$product->isPrint())
            <div class="product-colors-block product-colors-block-top clearfix">
                @foreach ($product->groupProductsList as $groupProduct)
{{--                    <div class="{{ $product->old_opt_price > $product->opt_price ? 'color-sale'--}}
{{--                        : ($groupProduct->qty > 0 && $groupProduct->qty < 10 ? 'color-last-items'--}}
{{--                        : ($groupProduct->qty <= 0 && $groupProduct->when_appear ? 'color-soon' : '')) }}">--}}
                  <div class="{{ getColorBorderBottom($product, $groupProduct) }}">
                    <a href="#" style="background-color: #{{ $groupProduct->color }};" class="cart-color {{ colorWhite($groupProduct->color) ? 'color-white' : '' }}"
                       @if ($showPrice)
                       data-price-default="{{ $groupProduct->getPrice($priceTypeSite) }}"
                       data-price-default-uah="{{ round($groupProduct->getPrice($priceTypeSite) * settings('dollar')) }}"
                       data-price-opt-range="<b>$</b> {!! implode(' - <b>$</b> ', $groupProduct->getPriceRange()) !!}"
                       data-price-opt="{{ $groupProduct->getPrice() }}"
                       data-price-vip="{{ $groupProduct->getPrice('opt_vip_usd') }}"
                       data-price-dealer="{{ $groupProduct->getPrice('opt_dealer_usd') }}"
                       data-price-drop-uah="{{ $groupProduct->getPrice('drop_uah') }}"
                       data-price-ppc="{{ $groupProduct->getPrice('price') }}"
                       data-price-old="{{ $groupProduct->getPrice('old_opt_price') }}"
                       @endif
                       data-id="{{ $groupProduct->id }}"
                       data-parent="{{ $groupProduct->parent }}"
                       data-wish="{{ in_array($groupProduct->id, $wishList) ? 'true' : 'false' }}"
                        {!! $groupProduct->color == 'ffffff' ? 'class="color-white"' : '' !!} title="{{ $groupProduct->color_name }}"
                        {!! $groupProduct->qty <= 0 ? 'data-when-appear="' . $groupProduct->when_appear . '"' : '' !!}>
                        @if ($groupProduct->qty <= 0)
                        <span></span>
                        @endif
                    </a>
                    <input type="number" class="product-color-qty {{ ($groupProduct->opt_price > 0 && $showPrice) ? null : 'hidden' }}"
                           data-id="{{ $groupProduct->id }}" value="0" tabindex="1" inputmode="numeric" autocomplete="off">
                </div>
                @endforeach
            </div>
            @endif
            <!-- #End Product Colors Block -->

            <!-- #Begin Select Material For Product -->
            @if ($product->isPrint())
            <div class="print-select-block">
              <div class="flex justify-between align-center" style="width: 95%">
                <div class="flex justify-between align-center cursor_p page-button" data-page-name="material_chehla_print">
                  <span class="m-5">{{ __('Материал чехла') }}</span>
                  <img class="tooltip-event" src="/template/images/hint_icon.svg" title="{{ __('Описание материалов') }}"/>
                </div>

                <div>
                  <a href="{{ custom_route('printcover') }}" class="orange-link">{{ __('Создай свой уникальный чехол') }}</a>
                </div>
              </div>

              <div class="flex">
                <select id="material" class="select-block material" style="width: 195px" required>
                  <option value="" style="display: none">{{ __('Выберите материал') }}</option>
                  @foreach ($product->materialSelect() as $key => $item)
                    <option value="{{ $key }}" @if( $product->countMaterial() === 1) selected @endif>{{ strtoupper($item->name_ru) }}</option>
                  @endforeach
                </select>

                <span class="product-qty-block">
                  <input type="number" id="product-qty" class="input-bottom product-qty"
                         style="text-align: center; width: 30px;height: 20px; margin: 7px 0 10px 5px;"
                         data-id="{{ $product->id }}"
                         data-is-print="{{ $product->isPrint() }}"
                         value="0" tabindex="1" inputmode="numeric">
                </span>
              </div>
            </div>
            @endif
            <!-- #End Select Material For Product -->

            @if ($showPrice)
            <a href="#" class="btn-buy add-to-cart add-to-cart-product-page"><span>{{ __('В корзину') }}</span></a>
            @endif
        </div>
        <!-- #End Product Info Block -->
    </div>
   <!-- Product Content Block -->
</div>

{{-- Быстрые ссылки на серии по брендам --}}
<x-series-short-links :series="$product->series"></x-series-short-links>

{{-- Блок Недавно просматривали --}}
<x-recently-viewed :productId="$product->id"></x-recently-viewed>

{{-- Характеристики, описание  --}}
<div class="product-description-block tabs-wrapper-block clearfix">
    <div class="tabs-title-block">
        <ul class="wrapper clearfix">
            <li ><a href="#all">{{ __('Все') }}</a></li>
            <li class="active"><a href="#characteristics">{{ __('Характеристики') }}</a></li>
            <li><a href="#review">{{ __('Описание') }}</a></li>

            @if ($product->brandData && $product->brandData->short_description)
            <li><a href="#brand-description">{{ __('Описание бренда') }}</a></li>
            @endif

{{--            <li><a href="#delivery">{{ __('Доставка и оплата') }}</a></li>--}}
        </ul>
    </div>
    <div class="tabs-content-block wrapper">

        @if ($filters->count() || $product->brandData)
        <div id="characteristics" class="tabs-info-block clearfix">

            @if ($product->brandData)
            <p><b>{{ __('Бренд') }}:</b> <span>{{ $product->brandData->title }}</span></p>
            @endif

            @foreach ($filters as $filter)
            <p><b>{{ $filter->name }}:</b> <span>{{ $filter->product_values->pluck('name')->join(', ') }}</span></p>
            @endforeach

        </div>
        @endif

          <div id="review" class="tabs-info-block text-block">
            <div class="clearfix">
              @if (!$product->full_description)
                <div class="left-block">
                  {!! $product->price_lists_characteristics !!}
                </div>
                <div class="left-block">
                  {!! $product->price_lists_review !!}
                </div>
              @else
                <div class="">
                  {!! $product->getDescription() !!}
                </div>
              @endif
            </div>

            @if ($product->videos)
              <br>
              <div class="text-center">{!! $product->videos !!}</div>
            @endif
        </div>

        @if ($product->brandData && $product->brandData->short_description)
        <div id="brand-description" class="tabs-info-block text-block">
            {!! $product->brandData->short_description !!}
        </div>
        @endif

{{--        <div id="delivery" class="tabs-info-block text-block">--}}
{{--            {!! settings('delivery_and_payment') !!}--}}
{{--        </div>--}}
    </div>
</div>

{{-- Блок Также интересуются --}}
<x-also-interested :filtersValues="$product->filters_values"
                   :filters="$filters"
                   :productId="$product->id"
                   :menu="$product->menu"></x-also-interested>

<div class="tabs-info-block text-block clearfix">
  <div class="wrapper">
    {!! settings('delivery_and_payment') !!}
  </div>
</div>

{{-- Модальное окно блок Отзывы/пожеланий --}}
<div class="found-cheaper-popup popup-block popup-block-sm">
    <div class="popup-title">{{ __('Отзывы / пожелания') }}</div>
    <form class="feedback-form">
        <input type="hidden" name="productTitle" value="{{ $product->title }}" />
        <input type="hidden" name="productUrl" value="{{ $product->url }}" />
        <input type="hidden" name="productId" value="{{ $product->id }}" />

        @include('elements.customer-data', ['titleText' => __('Ваши отзывы/пожелания. (Информация попадает напрямую управленцу)')])

        <button class="btn-orange" type="submit" id="send-form">{{ __('Отправить') }}</button>
        <input type="image" id="loader" class="hidden" src="/template/images/ajax-loader.gif" />
    </form>

    <a href="#" class="btn-close"></a>
</div>

<div class="static-page-popup popup-block"></div>

<div class="photo-viewer-block" id="modal-photo-viewer" >
    <div class="photo-viewer-wrapper wrapper">
        <!-- Photo Viewer Info Block -->
        <div class="photo-viewer-description product-info-block-modal clearfix">
            <div class="title-big-block">
                <div class="title-block">{{ $product->title }}</div>
                <div class="close-block">
                    <div class="btn-close "></div>
                </div>
            </div>

        </div>
        <!-- #End Photo Viewer Info Block -->
        <!-- Photo Block Modal -->
        <div class="photo-viewer-content position-r clearfix" >
            <div class="product-big-photo product-big-photo-bottom right-block">
                <div class="photo-viewer-image clearfix position-relative">
                    <div class="left-arrow-block">
                        <div class="btn-switch btn-switch-left"></div>
                    </div>
                    <div class="product-big-photo-content product-big-photo-content-bottom">
                        @if ($productImages->count())
                            @foreach ($productImages as $productImage)
                                <div data-product-id="{{ $productImage['id'] }}">
                                    <img src="/template/images/ajax-loader.gif" class="width-a"
                                         data-src="{{ config('custom.content_server') . $productImage['image'] }}"
                                         alt="{{ $productImage['alt'] }}">
                                </div>
                            @endforeach
                        @else
                            <div>
                                <img src="/template/images/noimagelarge.jpg"
                                     alt="{{ $product->title . ($product->color_name ? ' (' . $product->color_name . ')' : '') }}">
                            </div>
                        @endif
                    </div>
                    <div class="product-show-all-photos product-show-all-photos-modal btn-red hidden"
                         data-price-opt-range="<b>$</b> {!! implode(' - <b>$</b> ', $product->getPriceRange('all', false)) !!}"
                         data-price-default="{{ $product->getPrice($priceTypeSite) }}"
                         data-price-default-uah="{{ round($product->getPrice($priceTypeSite) * settings('dollar')) }}"
                         data-price-sale="{{ ($salePrice > 0) ? '$ ' . $salePrice : null }}"
                         data-price-opt="{{ $product->getPrice() }}"
                         data-price-vip="{{ $product->getPrice('opt_vip_usd') }}"
                         data-price-dealer="{{ $product->getPrice('opt_dealer_usd') }}"
                         data-price-drop-uah="{{ $product->getPrice('drop_uah') }}"
                         data-price-ppc="{{ $product->getPrice('price') }}"
                         data-price-old="{{ $product->getPrice('old_opt_price') }}"
                         data-wish="{{ in_array($product->id, $wishList) ? 'true' : 'false' }}"
                         data-parent-id="{{ $product->id }}">{{ __('Общие фото') }}</div>
                    <div class="right-arrow-block">
                        <div class="btn-switch btn-switch-right"></div>
                    </div>
                </div>
            </div>
            <div class="mobile-photo-dots"></div>
            <div class="product-photo-preview product-photo-preview-bottom">
                <div class="btn-slider-switch btn-switch-up"></div>
                <div class="product-preview-wrapper clearfix">
                    <div class="product-preview-slider product-preview-slider-bottom">
                        @foreach ($productImages as $productImage)
                            <a href="{{ config('custom.content_server') . $productImage['image'] }}"
                               data-product-id="{{ $productImage['id'] }}">
                                <img src="/template/images/ajax-loader.gif"
                                     data-src="{{ config('custom.content_server') . $productImage['image'] }}"
                                     alt="{{ $productImage['alt'] }}">
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="btn-slider-switch btn-switch-down btn-switch-down-modal"></div>
            </div>
        </div>

        <div class="photo-viewer-right-block clearfix">
            @if (!$product->isPrint())
            <div class="product-colors-block mobile-colors-block clearfix">
                @foreach ($product->groupProductsList as $groupProduct)
                    <div class="{{ getColorBorderBottom($product, $groupProduct) }}">
                        <a href="#"
                           data-price-default="{{ $groupProduct->getPrice($priceTypeSite) }}"
                           data-price-default-uah="{{ round($product->getPrice($priceTypeSite) * settings('dollar')) }}"
                           data-price-opt-range="<b>$</b> {!! implode(' - <b>$</b> ', $groupProduct->getPriceRange()) !!}"
                           data-price-opt="{{ $groupProduct->getPrice() }}"
                           data-price-vip="{{ $groupProduct->getPrice('opt_vip_usd') }}"
                           data-price-dealer="{{ $groupProduct->getPrice('opt_dealer_usd') }}"
                           data-price-drop-uah="{{ $groupProduct->getPrice('drop_uah') }}"
                           data-price-ppc="{{ $groupProduct->getPrice('price') }}"
                           data-price-old="{{ $groupProduct->getPrice('old_opt_price') }}"
                           data-id="{{ $groupProduct->id }}"
                           data-parent="{{ $groupProduct->parent }}"
                           style="background-color: #{{ $groupProduct->color }};"
                           class="modal-color {{ $groupProduct->color == 'ffffff' ? 'color-white' : '' }}"
                           {!! $groupProduct->color == 'ffffff' ? 'class="color-white"' : '' !!} title="{{ $groupProduct->color_name }}"
                            {!! $groupProduct->qty <= 0 ? 'data-when-appear="' . $groupProduct->when_appear . '"' : '' !!}>
                            @if ($groupProduct->qty <= 0)
                                <span></span>
                            @endif
                        </a>
                        <input type="number" class="product-color-qty product-color-qty-modal {{ ($groupProduct->opt_price > 0 && $showPrice) ? null : 'hidden' }}"
                               data-id="{{ $groupProduct->id }}" value="0" tabindex="1" inputmode="numeric" autocomplete="off">
                    </div>
                @endforeach
            </div>
            @endif

            <!-- #Begin Select Material For Product -->
            @if ($product->isPrint())
              <div style="text-align: center;"><div style="display: inline-block;">
                <div class="flex justify-left align-center cursor_p page-button" data-page-name="material_chehla_print">
                  <div class="m-5">{{ __('Материал чехла') }}</div>
                  <img class="tooltip-event" src="/template/images/hint_icon.svg" title="{{ __('Описание материалов') }}"/>
                </div>

                <div class="flex">
                  <select id="material2" class="select-block material" style="width: 230px" required>
                    <option value="" style="display: none">{{ __('Выберите материал') }}</option>
                    @foreach ($product->materialSelect() as $key => $item)
                      <option value="{{ $key }}" @if( $product->countMaterial() === 1) selected @endif>{{ strtoupper($item->name_ru) }}</option>
                    @endforeach
                  </select>

                  <span class="product-qty-block" title="{{ __('Описание материала') }}">
                    <input type="number" id="product-qty2" class="input-bottom product-qty"
                           style="width: 30px; height: 20px; margin: 7px 0 10px 5px;text-align: center;"
                           data-id="{{ $product->id }}" value="0" tabindex="1" inputmode="numeric" />
                  </span>
                </div>

                </div></div>
          @endif
          <!-- #End Select Material For Product -->

            <div class="cart-block">
                <div class="product-price-block product-price-block-modal position-r">
                  {{--    Вывод цен    --}}
                  @include('elements.price-product-block', [
                    'priceTypeSite' => $priceTypeSite,
                    'product' => $product,
                    'showPrice' => $showPrice,
                    'salePrice' => $salePrice,
                    'isModal' => true,
                  ])
                </div>

                @if ($showPrice)
                  <a href="#" class="btn-buy add-to-cart add-to-cart-product-page-modal" data-type="modal"><span>{{ __('В корзину') }}</span></a>
                @endif
            </div>

            <!-- Product Colors Block -->

            <!-- #End Product Colors Block -->
        </div>
    </div>
</div>

@endsection

@push('footer-scripts')

<script>
  const bannersList = @if($banner->count() > 1) @json($banner) @else null @endif;
  const content_server = @json(config('custom.content_server'));
</script>

<script src="/template/js/dist/product.min.js?v=1.0.64"></script>

@guest
<script>
  $(function() {
    const field = @json(app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua');
    $('.cheaper-block a').on('click', function() {
      $.ajax({
        url: '/v1/cities',
        dataType: 'json',
        data: {search: ''},
        success: function(data) {
          const selectBlock = $('.city-select');
          let selectList = selectBlock.find('.custom-select-list');

          for (let i in data) {
            const option = $('<div>').data('value', data[i].id).html(data[i][field]);
            selectList.append(option);
          }
        }
      });
    });
  });
</script>
@endguest
@endpush
