<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = collect([
          ['code' => 'ua', 'phone' => '380', 'rules' => '/^\+380\d{9}$/', 'mask' => '+38 (999)-999-99-99', 'name_ru' => 'Украина', 'name_ua' => 'Україна'],
          ['code' => 'ru', 'phone' => '7', 'rules' => '/^\+7\d{10}$/', 'mask' => '+7 99-999-99-999', 'name_ru' => 'Россия', 'name_ua' => 'Росія'],
          ['code' => 'md', 'phone' => '373', 'rules' => '/^\+373\d{8}$/', 'mask' => '+373 99-99-99-99', 'name_ru' => 'Молдавия', 'name_ua' => 'Молдавія'],
          ['code' => 'by', 'phone' => '375', 'rules' => '/^\+375\d{9}$/', 'mask' => '+375 99-999-99-99', 'name_ru' => 'Белоруссия', 'name_ua' => 'Білорусь'],
        ]);

        Country::insert($countries->toArray());
    }
}
