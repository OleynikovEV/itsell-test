<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      factory(\App\Models\NewFunctionality::class, 30)->create();
//        $this->call(NewFunctionalitySeeder::class);
    }
}
