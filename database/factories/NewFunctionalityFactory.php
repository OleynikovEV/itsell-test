<?php

/** @var Factory $factory */
use App\Models\NewFunctionality;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(NewFunctionality::class, function (Faker $faker) {
    return [
      'title' => $faker->sentence(),
      'content' => $faker->text(2000),
      'dateStartFunctionality' => $faker->dateTimeBetween('-2 month', '-1 month'),
      'show' => 1,
    ];
});
