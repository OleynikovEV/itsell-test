<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateManagersInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('its_managers_informations', function(Blueprint $table) {
        $table->dropColumn('icq');
        $table->dropColumn('image');
        $table->dropColumn('skype');
        $table->dropColumn('worktime');
        $table->dropColumn('rating');
        $table->dropColumn('manager_id');
      });

      Schema::table('its_managers_informations', function(Blueprint $table) {
        $table->string('manager_id', 50)->index('manager_id_index')->after('id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
