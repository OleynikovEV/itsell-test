<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCategorySecondIdBannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banner_category', function(Blueprint $table) {
          $table->integer('category_second_id', false, true)
            ->default(0)->nullable(false)->index();
        });

      \Illuminate\Support\Facades\DB::statement("ALTER TABLE banner_category DROP PRIMARY KEY;");
      \Illuminate\Support\Facades\DB::statement("CREATE INDEX index_banner_id ON banner_category(banner_id);");
      \Illuminate\Support\Facades\DB::statement("CREATE INDEX index_category_id ON banner_category(category_id);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
