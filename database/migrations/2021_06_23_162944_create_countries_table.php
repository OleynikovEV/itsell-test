<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('its_countries', function (Blueprint $table) {
          $table->string('code', 2)->nullable(false)->index();
          $table->integer('phone')->nullable(false)->index();
          $table->string('rules')->nullable(false);
          $table->string('mask')->nullable(false);
          $table->string('name_ru', 100)->nullable(false);
          $table->string('name_ua', 100)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
