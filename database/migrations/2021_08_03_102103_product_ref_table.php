<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductRefTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('its_product_ref', function(Blueprint $table) {
          $table->integer('product_id')->unsigned()->index();
          $table->integer('category_id')->unsigned()->index();
          $table->integer('brand_id')->unsigned()->index();
          $table->integer('sector_id')->unsigned()->index();
          $table->integer('model_id')->unsigned()->index();
          $table->integer('brand_manufacturer_id')->unsigned()->index();
          $table->boolean('novelty');
          $table->boolean('coming');
          $table->float('opt_price')->default(0)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
