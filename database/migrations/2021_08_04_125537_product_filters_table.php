<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('its_product_filter', function (Blueprint $table) {
        $table->unsignedInteger('product_id')->index();
        $table->unsignedInteger('category_id')->index();
        $table->unsignedInteger('filter_value_id')->index();
        $table->boolean('novelty');
        $table->boolean('coming');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
