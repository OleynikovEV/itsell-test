<?php

return [

    // Базовые данные о магазине
    'shop_name' => 'ITsell OPT',

    // URL сервера с статическими файлами
    'content_server' => env('CONTENT_SERVER_URL', ''),
    'edit_product_url' => env('EDIT_PRODUCT_URL', ''),

    'vchehle_secret' => env('VCHEHLE_SECRET', ''),
    'vchehle_token' => env('VCHEHLE_TOKEN', ''),

    'telegram_bot_token' => env('TELEGRAM_BOT_TOKEN', ''),
    'telegram_bot_url' => env('TELEGRAM_BOT_URL', ''),
    'telegram_bot_username' => env('TELEGRAM_BOT_USERNAME', ''),
    'telegram_webhook_url' => env('TELEGRAM_WEBHOOK_URL', ''),
    'telegram_no_price_chat' => env('TELEGRAM_NO_PRICE_CHAT', ''),

    'viber_bot_token' => env('VIBER_BOT_TOKEN', ''),
    'viber_bot_url' => env('VIBER_BOT_URL', ''),
    'viber_bot_logo' => env('VIBER_BOT_LOGO', ''),

    'email_from' => 'drop@itsellopt.com.ua',

    // Категория Общие аксессуары
    'accessories_category_id' => 110,

   // email руководителя
   'director' => 'director@itsellopt.com.ua',

    // Типы и настройки упаковок
    'package_types' => [
        1 => [
            'label' => 'Упаковка для чехлов',
            'filters' => [256]
        ],
        2 => [
            'label' => 'Упаковка для стекол',
            'filters' => [257, 258]
        ]
    ],

    // интервал для расчета рейтинга товаров в днях
    'rating_interval' => 30,

    // типы цен для покупателя
    'prices_type' => ['opt_price', 'opt_minopt_usd', 'drop_uah', 'opt_vip_usd', 'opt_dealer_usd'],

    // список доступных корзин
    'cart_list' => [
      'default' => 'Основная',
      'cart-1' => 'Корзина 1',
      'cart-2' => 'Корзина 2',
      'cart-3' => 'Корзина 3',
      'cart-4' => 'Корзина 4',
      'cart-5' => 'Корзина 5',
    ],

    // Варианты для поля "Как вы нас нашли" при регистрации
    'how_found_options' => [
        'Google, сам нашёл',
        'Google реклама',
        'Bazzilla',
        'Facebook / Instagram',
        'Представитель компании заходил',
        'Viber',
        'Сарафан / Знакомые посоветовали',
    ],

    // Получатели email-сообщений
    'mail_recipients' => [
        'order_created' => ['order@itsellopt.com.ua'],
        'order2_created' => ['Order2@itsellopt.com.ua'],
        'back_manager' => ['backmanager@itsellopt.com.ua'],
        'order_drop' => ['drop.itsellopt@gmail.com'],
        'abandoned_cart' => ['opt.managers@itsellopt.com.ua'],
        'found_cheaper' => ['order@itsellopt.com.ua'],
        'feedback' => ['partner@itsellopt.com.ua', 'ceo@itsellopt.com.ua', 'director@itsellopt.com.ua']
    ]

];
