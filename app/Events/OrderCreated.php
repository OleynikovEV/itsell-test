<?php

namespace App\Events;

use App\Models\Order;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderCreated
{
    use Dispatchable, SerializesModels;

    public $order;
    public $manager;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order, $manager)
    {
        $this->order = $order;
        $this->manager = $manager;
    }

}