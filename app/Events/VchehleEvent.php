<?php

namespace App\Events;

use App\Models\Order;

class VchehleEvent
{
  private Order $order;

  public function __construct(Order $order)
  {
    $this->order = $order;
  }

  public function getOrder(): Order
  {
    return $this->order;
  }
}
