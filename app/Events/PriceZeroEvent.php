<?php

namespace App\Events;

use App\Models\Product;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PriceZeroEvent
{
    use Dispatchable, SerializesModels;

    private $products = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($productIds)
    {
        $this->products = Product::whereIn('id', $productIds)->get(['title', 'id', 'url']);
    }

    public function getProducts()
    {
      return $this->products;
    }
}
