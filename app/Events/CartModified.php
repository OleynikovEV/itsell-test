<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CartModified
{
    
    use Dispatchable, SerializesModels;

}
