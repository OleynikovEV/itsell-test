<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'its_product_category';

    // relationships
    public function category()
    {
      return $this->hasMany(Category::class, 'id', 'category_id');
    }
}
