<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    const TYPE_MAIN_SLIDER = 1;
    const TYPE_MAIN_MENU = 2;
    const TYPE_CATEGORY_PAGE = 3;
    const TYPE_PRODUCT_PAGE = 4;
    const TYPE_HEADER_BAR = 5;
    const TYPE_SEARCH_PAGE = 6;
    const TYPE_MAIN_PAGE = 7;

    protected $casts = ['meta' => 'object'];

    public function isVideo(): bool
    {
      return $this->isVideo;
    }

    public function getCover(): string
    {
      return ($this->image)
        ? config('custom.content_server') . $this->image
        : sprintf('https://i.ytimg.com/vi/%s/hqdefault.jpg', $this->link);
    }

    public function getMobileCover(): string
    {
      return ($this->image_mobile)
        ? config('custom.content_server') . $this->image_mobile
        : sprintf('https://i.ytimg.com/vi/%s/hqdefault.jpg', $this->link);
    }

    public function getImage(): string
    {
      return config('custom.content_server') . $this->image;
    }

    public function getMobileImage(): string
    {
      return config('custom.content_server') . $this->image_mobile;
    }

    public function getYoutubeCode()
    {
      return ($this->isVideo == 2) ? $this->link : null;
    }

    public function getVideoUrl()
    {
      return ($this->isVideo == 1) ? 'https://itsellopt.com.ua/uploads/video1.mp4' : null; //config('custom.content_server') . $this->link : null;
    }

    public function getLink()
    {
      return  ($this->link) ? $this->link : '#';
    }
}