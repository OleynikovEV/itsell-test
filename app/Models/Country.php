<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'its_countries';

    protected $fillable = ['code', 'phone', 'rules', 'mask', 'name_ru', 'name_ukr'];

    public $timestamps = false;
}
