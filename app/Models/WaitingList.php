<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaitingList extends Model
{
  protected $table = 'its_waiting_list';

  protected $fillable = ['user_id', 'product_id', 'comment'];

  /** relationships */
  public function user()
  {
    return $this->hasOne(User::class, 'id', 'user_id');
  }

  public function product()
  {
    return $this->hasOne(Product::class, 'id', 'product_id');
  }
}
