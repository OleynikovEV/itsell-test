<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoActions extends Model
{
    protected $table = 'its_promo_actions';

    // Getters
    public function getSale(): float
    {
        return $this->sale;
    }

    /**
     * проверяем действует ли промокод
     */
    public function isActive(): bool
    {
        return ($this->time_start <= now()->timestamp && $this->time_end > now()->timestamp );
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'promo_code', 'code');
    }
}
