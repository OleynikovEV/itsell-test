<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'its_orders';

    protected $fillable = ['fio', 'company', 'email', 'firstname', 'lastname', 'phone', 'delivery_type_id', 'city_id',
        'user_comment', 'dropshipping', 'total', 'user_id', 'ip', 'time', 'declaration', 'comment', 'manager', 'country',
        'address', 'delivery_comment', 'street', 'street_building', 'status_code_np', 'price_type', 'from_cart'];

    public $timestamps = false;

    /**
     * Товары в заказе
     */
    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    /**
     * Город для доставки
     */
    public function cityData()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    /**
     * Отделение, на которое выбрана доставка
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * Улица, на которую выбрана доставка
     */
    public function streetData()
    {
        return $this->belongsTo(Street::class, 'street_id', 'id');
    }

    /**
     * ТТН, привязанные к текущему заказу
     */
    public function waybills()
    {
        return $this->hasMany(Waybill::class);
    }

    public function waybill()
    {
        return $this->hasOne(Waybill::class);
    }

    /**
     * Пользователь, который оформил текущий заказ
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получить тип цен
     */
    public function getPriceType()
    {

      return PriceListType::getItem($this->price_type);
    }

    public function hesPriceType(): bool
    {
      return !empty($this->price_type);
    }

    public function isDrop()
    {
      return $this->dropshipping;
    }
}
