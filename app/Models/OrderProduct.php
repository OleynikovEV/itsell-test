<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{

    protected $table = 'its_order_products';

    protected $fillable = ['product_id', 'count', 'qty_with_delay', 'qty_shortage', 'discount', 'price', 'comment',
      'promo_code', 'print', 'material', 'full_border'];

    public $timestamps = false;

    protected $casts = [
      'full_border' => 'boolean',
    ];

    /**
     * Стоимость покупки товара
     */
    public function getPurchasePrice()
    {
        return $this->price - $this->price * $this->discount / 100;
    }

    /**
     * Купленный товар
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productCover()
    {
      return $this->belongsTo(PrintCover::class, 'product_id', 'uuid');
    }

    /**
     * Разница после скидки
     */
    public function getSalePrice()
    {
        return $this->price * $this->discount / 100;
    }

    public function getPrice()
    {
        return $this->price;
    }
}
