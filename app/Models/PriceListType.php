<?php

namespace App\Models;

class PriceListType
{
  // доступные типы цен
  static public function get()
  {
    return [
      'opt_price' => __('Опт'),
      'opt_vip_usd' => __('VIP опт'),
      'opt_dealer_usd' => __('Дилер опт'),
      'opt_minopt_usd' => __('Мин опт'),
      'drop_uah' => __('Дроп'),
      'price' => __('РРЦ'),
    ];
  }

  static public function getItem($index)
  {
    $type = self::get();

    if (isset($type[$index])) {
      return $type[$index];
    }

    return null;
  }
}