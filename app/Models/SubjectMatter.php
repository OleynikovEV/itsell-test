<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectMatter extends Model
{
    protected $table = 'its_subject_matters';

    protected $fillable = ['name'];

    public $timestamps = false;
}
