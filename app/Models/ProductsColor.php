<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsColor extends Model
{
    protected $table = 'its_products_colors';

    protected $fillable = ['id', 'name', 'color', 'priority', 'parent_id', 'parent', 'children', 'full_name'];

    protected $casts = [
        'children' => 'array',
    ];

    public function getColorName()
    {
      return strlen($this->full_name) > 0 ? $this->full_name : $this->name;
    }
}
