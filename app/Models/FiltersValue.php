<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FiltersValue extends Model
{
    protected $table = 'its_filters_values';

    protected $fillable = ['name', 'filter_id', 'url'];

    public $timestamps = false;

    /**
     * Фильтр, к которому относится текущее значение
     */
    public function filter()
    {
        return $this->belongsTo(Filter::class);
    }
}
