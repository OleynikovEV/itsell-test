<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $table = 'its_filters';

    protected $fillable = ['code', 'name', 'url'];

    public $timestamps = false;

    /**
     * Значения текущего фильтра
     */
    public function values()
    {
        return $this->hasMany(FiltersValue::class);
    }

    public function categoryValues()
    {
      return $this->hasMany(FiltersValue::class, 'filter_id', 'id');
    }



    /**
     * Преобразование списка родительских значений текущего фильтра в массив
     */
    public function getParentFiltersValuesAttribute($value = '')
    {
        return $value ? explode(',', $value) : false;
    }
}