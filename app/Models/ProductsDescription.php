<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsDescription extends Model
{
    protected $table = 'its_products_descriptions';

    /**
     * Преобразование списка значений фильтров товара в массив
     */
    public function getFiltersValuesAttribute($value = '')
    {
        return $value ? explode(',', $value) : [];
    }

    public function setFiltersValuesAttribute($value = '')
    {
        $this->attributes['filters_values'] = $value && count($value) ? implode(',', $value) : '';
    }
}