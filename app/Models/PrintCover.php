<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrintCover extends Model
{
  protected $table = 'its_print_cover';

  protected $fillable = [
    'uuid', 'image', 'price', 'qty', 'category_id', 'material_id', 'show_price', 'brand'
  ];

  public function insert(string $uuid, string $title, string $image, int $qty, float $price, int $category_id,
                         string $material_id, int $brand = 323)
  {
    $this->uuid = $uuid;
    $this->title = $title;
    $this->image = $image;
    $this->qty = $qty;
    $this->price = $price;
    $this->category_id = $category_id;
    $this->material_id = $material_id;
    $this->brand = $brand;

    $this->save();

    return $this;
  }

  public function getPrice(): float
  {
    return $this->price;
  }

  public function getLabel(): string
  {
    return '';
  }

  public function isPrint(): bool
  {
    return true;
  }

  // relationships

  public function brandData()
  {
    return $this->belongsTo(Brand::class, 'brand');
  }

  public function categoryVchehle()
  {
    return $this->hasOne(VchehleCategories::class, 'vchehle_id', 'category_id');
  }
}