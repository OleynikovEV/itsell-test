<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // Количество дней, в течение которых товар считается новинкой
    const NOVELTY_DAYS = 60;
    const NOVELTY_DAYS_ORDER = 14;
    const COMING_DAYS = 14;

    const IMAGE_SIZE = 1500;
    const IMAGE_THUMB_MAIN = 250;
    const IMAGE_THUMB_SECONDARY = 100;

    const IMAGE_FIELDS = ['img', 'img_1', 'img_2', 'img_3', 'img_4', 'img_5', 'img_6', 'img_7'];

    protected $table = 'its_products';

    protected $fillable = [
        'code', 'baseid', 'colorCode', 'title', 'menu', 'url', 'active', 'type', 'parent', 'color', 'color_name',
        'price_modify_time', 'full_description', 'filters_values', 'videos', 'temp_photos', 'price', 'has_package',
        'opt_price', 'price_types', 'qty', 'qty_today', 'old_price', 'old_opt_price', 'qty_with_delay', 'liquidity',
        'when_appear', 'last_modified', 'added_time', 'box', 'box_image', 'show_price', 'created_at', 'priority',
        'description_for_client', 'promo_code', 'brand', 'img', 'temporary_photo', 'addit_menus', 'product_type',
        'products_series_id', 'group_products', 'from', 'product_subtype', 'date_coming', 'markdown', 'tech_pack'
    ];

    protected $casts = [
        'price_types' => 'array',
        'group_products' => 'array',
        'onec_images_sizes' => 'array'
    ];

    public $timestamps = false;

    protected $isNovelty;

  /**
   * Показать метку "1-2 дня"
   */
  public function showLabelOneTwoDays(): bool
  {
    return $this->from === 1 || $this->from === 2;
  }

  /**
   * Защитная гидрогелевая пленка SKLO
   */
  public function isHydrogel(): bool
  {
    return $this->from === 2;
  }

    /**
     * Флаг статуса Новинка у текущего товара
     * @return bool
     */
    public function isNovelty(): bool
    {
        if ( ! app('settings')->temp('products_novelty_time')) {
            app('settings')->temp('products_novelty_time', today()->subDays(self::NOVELTY_DAYS)->timestamp);
        }

        return ($this->added_time > app('settings')->temp('products_novelty_time')) && ($this->qty > 0);
//        return ($this->added_time > app('settings')->temp('products_novelty_time'))
//          ;
    }

  /**
   * Уценка или нет
   * @return bool
   */
  public function isMarkdown(): bool
  {
    return $this->markdown === 1;
  }

  /**
   * Есть скидка или нет
   * @return bool
   */
  public function isSale(): bool
  {
    return ($this->old_opt_price > $this->opt_price)
      && ($this->price_modify_time >= today()->subWeek(4)->timestamp)
      && $this->qty > 0;
  }

  /**
   * Новые поступления ?
   * @return bool
   */
  public function isComing(): bool
  {
    return ($this->qty > 0 && $this->date_coming > today()->subDays(Product::COMING_DAYS)->format('Y-m-d'));
  }

  /**
   * товар в ожидании?
   * @return bool
   */
  public function isSoon(): bool
  {
    return (!empty($this->when_appear) && $this->qty <= 0);
  }

  /*
   * rating = (sum_of_rating * 5)/sum_of_max_rating_of_user_count
   */
  public function calcRating($max, $amountPoints)
  {
    $ration = ceil( ($this->priority * $amountPoints / $max) );
    return ($ration > $amountPoints) ? $amountPoints : $ration;
  }

  /**
   * Получение определенного типа цены
   * @param string $type
   * @return false|mixed
   */
    public function getPrice($type = 'opt_price', $convertToUsd = false, bool $groupBySeries = false, bool $convertToUah = false)
    {
      if ($groupBySeries) {
        $type .= '_range';

        $priceMin = min($this->$type ?? 0);
        $priceMax = max($this->$type ?? 0);

        if ($convertToUsd && (stripos($type, 'uah') !== false || $type == 'price_range') && !$convertToUah) {
          $priceMin = round($priceMin / settings('dollar'), 2);
          $priceMax = round($priceMax / settings('dollar'), 2);
        }

        if ($convertToUah && stripos($type, 'uah') === false && $type != 'price_range') {
          $result = $priceMin < $priceMax
            ? [ round($priceMin * settings('dollar')), round($priceMax * settings('dollar')) ]
            : [ round($priceMin * settings('dollar')) ];
        } else {
          $result = $priceMin < $priceMax ? [$priceMin, $priceMax] : [$priceMin];
        }

        return implode(' - ', $result);
      }
      else {
        $price = isset($this->attributes[$type]) ? $this->{$type} : ($this->price_types && isset($this->price_types[$type])
          ? $this->price_types[$type] : false);

        if ($price === false) {
          return 0;
        }

        if ($convertToUsd === false) {
          return $price;
        }

        if (stripos($type, 'uah') !== false || $type === 'price') {
          $price = round($price / settings('dollar'), 2);
        }

        return $price;
      }
    }

    /**
     * Получение диапазона цен для товара
     * @param string $type
     * @param bool $groupBySeries
     * @return array|false[]
     */
    public function getPriceRange($type = 'all', bool $groupBySeries = false)
    {
      // если сгруппирован по сериям
      if ($groupBySeries && isset($this->opt_price_range) && isset($this->opt_vip_usd_range) && isset($this->opt_dealer_usd_range)) {
        $range = array_merge($this->opt_price_range, $this->opt_vip_usd_range, $this->opt_dealer_usd_range);
        $priceMin = min($range);
        $priceMax = max($range);

//        return $priceMin < $priceMax ? [$priceMin, $priceMax] : [$priceMin];
        return min_max_price($priceMin, $priceMax);
      } else { // если выводить по СКУ
        if ($type == 'all') {
          if ($this->getPrice('min_price') === 0) {
            $priceMin = $this->opt_price;
            $priceMax = $this->opt_price;
            foreach (['opt_vip_usd', 'opt_dealer_usd'] as $priceType) {
              $price = $this->getPrice($priceType);
              if ($price && $price < $priceMin) $priceMin = $price;
              if ($price && $price > $priceMax) $priceMax = $price;
            }

            return min_max_price($priceMin, $priceMax);
          } else { // min_price и max_price есть у принтов - TODO@ добавить и для других товаров
            return min_max_price($this->getPrice('min_price'), $this->getPrice('max_price'));
          }
        } else {
          $priceMin = $priceMax = $this->getPrice($type); // TODO: сравнивать цены всех цветов
          return [$priceMin];
        }
      }
    }

    /** тип валюты */
    public function getCurrencyType($type)
    {
      if ($type === 'drop_uah' || $type === 'price') {
        return [
          'currency' => 'uah',
          'symbol' => '₴',
        ];
      } else {
        return [
          'currency' => 'usd',
          'symbol' => '$',
        ];
      }
    }
    /**
     *  Получить картинку коробки
     */
    public function getBoxImage(): ?string
    {
      if (empty($this->box_image)) {
        return null;
      }

      return config('custom.content_server') . $this->box_image;
    }

    /**
     * титульная картинка продукта
     */
    public function getDefaultTitleImage(): string
    {
      return $this->img
        ? config('custom.content_server') . thumb_name($this->img)
        : '/template/images/noimage.jpg';
    }

  /**
   * Возвращает массив с доступным материалом для производства чехлов
   * @return array
   */
    public function materialSelect()
    {
       return $this->group_products[0]->material ?? [];
    }

    public function countMaterial()
    {
      return count( isset($this->group_products[0]->material) ? (array)$this->group_products[0]->material : []);
    }

  /**
   * доступен товар или нет
   */
  public function isAvailability(): bool
  {
    return $this->active && $this->qty > 0;
  }
  // not available

  /**
   * получить метку товару
   */
  public function getLabel(): string
  {
    if ($this->isMarkdown()) return 'label-markdown'; // 3
    if ($this->qty <= 0 && $this->when_appear) return 'label-soon'; // 0
    if ($this->old_opt_price > $this->opt_price) return 'label-sale'; // 2
    if ($this->isNovelty() && $this->qty > 0) return 'label-new'; // 1

    return '';
  }

  public function getLabelNew()
  {
    if ($this->isMarkdown()) return [
      'class' => 'marker marker-markdown',
      'text' => __('Уценка'),
    ];
    if ($this->qty <= 0 && $this->when_appear) return [
      'class' => 'marker marker-soon',
      'text' => __('Скоро'),
    ];
    if ($this->old_opt_price > $this->opt_price) return [
      'class' => 'marker marker-sale',
      'text' => 'Sale',
    ];
    if ($this->isNovelty() && $this->qty > 0) return [
      'class' => 'marker marker-new',
      'text' => 'New',
    ];
    if ($this->isComing()) return [
      'class' => 'marker marker-coming',
      'text' => __('Приход'),
    ];

    return [
      'class' => '',
      'text' => '',
    ];
  }

  /**
   * получить номера метки товару
   */
  public function getLabelNum(): int
  {
    if ($this->isMarkdown()) return 4;
    if ($this->old_opt_price > $this->opt_price) return 2;
    if ($this->isNovelty() && $this->qty > 0) return 1;
    if ($this->qty <= 0 && $this->when_appear) return 3;

    return 0;
  }

    /**
     * получаем титульную картинку
     */
    public function getTitleImage(): ?string
    {
        return $this->img;
    }

    /**
     * цвет продукта
     */
    public function getColorName(): ?string
    {
        return $this->color_name;
    }

  /**
   * это принт или нет (из прайса Вчехле)
   */
  public function isPrint(): bool
  {
    return $this->from === 1;
  }

  /**
   * материал чехла
   */
  public function getMaterial(): string
  {
    $filter = implode(',', $this->filters_values);

    if (mb_strpos($filter, 236) !== false || mb_strpos($filter, 337) !== false) return 'silicone';
    if (mb_strpos($filter, 237) !== false) return 'plastic';

    return '';
  }

  public function getAvailableMaterialsAttribute()
  {
    $material = [];
    if (isset($this->group_products[0]->material)) {
      foreach ($this->group_products[0]->material as $key => $item) {
        $material[$key] = $item->name_ru;
      }
    } else {
      $material = null;
    }

    return $material;
  }


  /**
   * Получение списка изображений товара
   * @param bool $withGroupProducts
   * @return \Illuminate\Support\Collection
   */
    public function getImages($withGroupProducts = false)
    {
        $images = collect();

        $variables = ['title' => $this->title . ($this->color_name ? ' (' . $this->color_name . ')' : '')];

        for ($i = 0; $i < 8; $i++)
        {
            $field = 'img' . ($i ? '_' . $i : '');
            $thumb = str_replace('/', '', $this->{$field});

            if ($this->{$field})
            {
                $images->push([
                    'id' => $this->id,
                    'image' => $this->{$field},
                    'image_thumb' => str_replace('.', '_thumb.', $thumb),
                    'alt' => text_template('products.alt_' . ($i + 1), $this->id, $variables)
                ]);
            }
        }

        // Если задан параметр получения фотографий для всех цветов - добавляем их в массив
        if ($withGroupProducts)
        {
            foreach ($this->groupProductsList as $groupProduct)
            {
                if ($groupProduct->id != $this->id) $images = $images->merge($groupProduct->getImages());
            }
        }

        return $images;
    }

    /**
     * Родительский товар
     */
    public function parentProduct()
    {
        return $this->belongsTo(Product::class, 'parent');
    }

    /**
     * Доступные цвета товара
     */
    public function groupProductsList()
    {
        if ($this->from === 1) {
          return $this->hasMany(Product::class, 'id');
        } else if ( ! $this->parent || ! $this->parentProduct)
        {
            return $this->hasMany(Product::class, 'parent')
                ->whereRaw('(qty > 0 OR when_appear)')
                ->orderBy('qty', 'DESC');
        } else  return $this->parentProduct->groupProductsList();
    }

    /**
     * Категория
     */
    public function categoryData()
    {
        return $this->belongsTo(Category::class, 'menu');
    }

    /**
     * Описание товара
     */
    public function getDescription(): ?string
    {
        if ($this->full_description && $this->full_description != '0' && $this->full_description != '<p>0</p>') {
          if (strpos($this->full_description, 'http') === false) {
            return str_replace('/uploads/', config('custom.content_server'), $this->full_description);
          }

          return $this->full_description;
        }

        return $this->price_lists_review;
    }

    /**
     * Дополнительные категории, к которым привязан текущий товар
     */
    public function additionalCategories()
    {
        return $this->belongsToMany(Category::class, 'its_product_category');
    }

    /**
     * Бренд товара
     */
    public function brandData()
    {
        return $this->belongsTo(Brand::class, 'brand');
    }

    /**
     * Серия товара
     */
    public function series()
    {
        return $this->belongsTo(ProductsSeries::class, 'products_series_id');
    }

  /**
   * Базовые условия активности для отображения товаров на сайте
   * @param null $query
   * @param bool $onlyParentProducts
   */
    public function scopeActive($query = null, $onlyParentProducts = true)
    {
        if ($onlyParentProducts) $query->where('parent', 0);

        $query->where('active', true)
            ->whereRaw('(qty > 0 OR when_appear)');

        return;
    }

  /**
   * Старая цена товара
   * @param $value
   * @return int
   */
    public function getOldOptPriceAttribute($value)
    {
        return $value > $this->opt_price && $this->price_modify_time > today()->subWeek(4)->timestamp ? $value : 0;
        return $value > $this->opt_price && $this->price_modify_time > today()->subWeek(4)->timestamp ? $value : 0;
    }

  /**
   * Преобразование списка значений фильтров товара в массив
   * @param string $value
   * @return array|false|string[]
   */
    public function getFiltersValuesAttribute($value = '')
    {
        return $value ? explode(',', $value) : [];
    }

    public function setFiltersValuesAttribute($value = '')
    {
        $this->attributes['filters_values'] = $value && count($value) ? implode(',', $value) : '';
    }

  /**
   * Преобразование краткой информации о доступных цветах перед ее получением
   * @param $value
   * @return array|mixed
   */
    public function getGroupProductsAttribute($value)
    {
        $value = json_decode($value);

        // Если задан список цветов - обрабатываем их для получения названий для текущей локализации
        if ($value && is_array($value))
        {
            $locale = app()->getLocale();

            foreach ($value as $item)
            {
                // Если в качестве названия задан объект - получаем из него значение для текущей локализации (либо
                // для локализации по умолчанию)
                if (isset($item->name) && is_object($item->name))
                {
                    $item->name = isset($item->name->{$locale}) ? $item->name->{$locale} : reset($item->name);
                }
            }
        }

        return $value;
    }

    public function setGroupProductsAttribute($value)
    {
      $this->attributes['group_products'] = json_encode($value);
    }

    /** relationships */
    public function promoaction()
    {
        return $this->hasOne(PromoActions::class, 'code', 'promo_code');
    }
}
