<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagersInformations extends Model
{
    protected $table = 'its_managers_informations';

    protected $fillable = ['manager_id', 'fio', 'email', 'phone'];

    public $timestamps = false;

    // relationships
    public function users()
    {
      return $this->hasMany('App\Models\User', 'manager', 'id');
    }
}
