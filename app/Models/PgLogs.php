<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PgLogs extends Model
{
  protected $table = 'its_pg_logs';
  protected $fillable = [
    'user_id', 'price', 'timeExecuted', 'memoryUsed', 'created_at'
  ];
  public $timestamps = false;

  public function add()
  {

  }
}