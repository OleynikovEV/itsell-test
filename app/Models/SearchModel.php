<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchModel extends Model
{
    protected $table = 'its_search';

    public $timestamps = false;

    protected $fillable = ['query', 'count'];
}
