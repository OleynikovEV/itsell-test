<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class NewFunctionality extends Model
{
    protected $table = 'its_new_functionality';

  protected $dates = ['dateStartFunctionality'];

  public function getPreviewAttribute()
  {
    $content = strip_tags($this->content);
    $content = str_replace('&nbsp; ', '', $content);
    $content = str_replace('&nbsp;', '', $content);
    $content = str_replace('&quot;', '', $content);
    return Str::limit($content, 200, '...');
  }

  public function getCover(): ?string
  {
    if (Str::contains($this->content, 'src="')) {
      return Str::before(Str::after($this->content, 'src="'), '"');
    }

    return null;
  }
}