<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceRange extends Model
{
  protected $table = 'its_price_range';

  protected $fillable = ['amount', 'price_type', 'price'];

  public $priceMin = 999;
  public $priceMax = 0;
  public $range = [];

  public function priceRange()
  {
    $tmp = [
      1 => [
        "price" => 299,
        "drop_uah" => 120,
        "opt_vip_usd" => 4.11,
        "opt_dealer_usd" => 4.04,
        "opt_price" => 4.21,
        "min_opt_usd" => 3.91,
      ]
    ];

    $prices = $this->get()->groupBy('amount');

    foreach ($prices as $amount => $price) {
      $tmp[$amount] = [];
      foreach ($price as $item) {
        $tmp[$amount][$item->price_type] = $item->price;

        if ($item->price_type == 'price' || $item->price_type == 'opt_minopt_usd'
          || \Illuminate\Support\Str::contains($item->price_type, 'uah')) { continue; }
        if ($item->price < $this->priceMin) {
          $this->priceMin = $item->price;
        }
        if ($item->price > $this->priceMax) {
          $this->priceMax = $item->price;
        }
      }
    }

    return $tmp;
  }

  public function priceRangeByType($type = null, $revers = false)
  {
    $tmp = [];

    $prices = $this->get();

    $this->range = array_keys( $prices->groupBy('amount')->toArray() );
    $prices = $prices->groupBy('price_type');

    foreach ($prices as $key => $price) {
      $tmp[$key] = [];
      foreach ($price as $num => $item) {
        $tmp[$key][$item->amount] = $item->price;
      }
    }

    if ($type !== null) {
      return $tmp[$type];
    }

    if ($revers) {
      $ordered = ['opt_price', 'opt_vip_usd', 'opt_dealer_usd', 'drop_uah', 'price', 'opt_minopt_usd'];
    } else {
      $ordered = ['price', 'drop_uah', 'opt_dealer_usd', 'opt_vip_usd', 'opt_price', 'opt_minopt_usd'];
    }

    $data = [];
    foreach ($ordered as $item) {
      if (isset($tmp[$item])) {
        $data[$item] = $tmp[$item];
      }
    }

    return $data;
  }
}
