<?php


namespace App\Models;


class BannerCategory extends \Illuminate\Database\Eloquent\Model
{
  protected $table = 'banner_category';

  public function banners()
  {
    return $this->hasOne(Banner::class, 'id', 'banner_id');
  }
}