<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsSeries extends Model
{
    protected $table = 'its_products_series';

    protected $fillable = ['name', 'url', 'products_count'];

    public $timestamps = false;

    /**
     * Товары, принадлежащие к текущей серии
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
