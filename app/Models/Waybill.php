<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Waybill extends Model
{
    protected $table = 'its_waybills';

    protected $fillable = ['user_id', 'order_id', 'waybill', 'scheduled_delivery_date',
        'date_return_cargo', 'recipient_date_time', 'status', 'status_code'];

    protected $dates = ['scheduled_delivery_date', 'date_return_cargo', 'recipient_date_time'];

    /** Дата возврата посылки */
    public function dateBoxBack()
    {
        return $this->scheduled_delivery_date;
    }

    /** Получил ли получатель посылку */
    public function parcelReceived(): bool
    {
        return $this->status == 9;
    }
}
