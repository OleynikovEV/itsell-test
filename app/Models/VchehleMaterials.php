<?php

namespace App\Models;

class VchehleMaterials extends \Illuminate\Database\Eloquent\Model
{
  protected $table = 'its_vchehle_materials';
  protected $fillable = ['category_id', 'material_id', 'qty'];
  public $timestamps = false;
}