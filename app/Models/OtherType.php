<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OtherType extends Model
{
    protected $table = 'its_other_types';
}
