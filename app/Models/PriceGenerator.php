<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PriceGenerator extends Model
{
  protected $table = 'its_price_generator';

  protected $fillable = [
    'user_id', 'price_name', 'slug', 'currency', 'price_type', 'file_type',
    'category_type', 'tab', 'conditions', 'ids', 'download', 'notAvailable', 'main_photo'
  ];

  protected $casts = [
    'conditions' => 'array',
    'ids' => 'array',
  ];

  // getters
  public function getUserId(): int
  {
    return $this->user_id;
  }

  public function getSlug(): string
  {
    return $this->slug;
  }

  public function mainPhoto(): bool
  {
    return $this->main_photo === 1;
  }

  /**
   * Определяем можно ли скачать прайс
   * @return bool
   */
  public function canDownload(): bool
  {
    if (empty($this->download)) return true;

    $date = Carbon::parse($this->download);
    return $date->diffInMinutes(Carbon::now()->format('Y-m-d H:i')) >= 15;
  }

  // relationships
  public function user()
  {
    return $this->hasOne(User::class, 'id', 'user_id');
  }
}
