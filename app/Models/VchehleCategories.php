<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VchehleCategories extends Model
{
  protected $table = 'its_vchehle';

  // relationship
  public function category()
  {
    return $this->hasOne(Category::class, 'id', 'menu_id');
  }
}
