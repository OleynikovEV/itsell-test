<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'its_menu';

    protected $fillable = ['code', 'title', 'parent_id', 'name'];

    public $timestamps = false;

    /**
     * Родительская категория
     */
    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * Подкатегории
     */
    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function sectors()
    {
      return $this->hasMany(CategorySector::class, 'id', 'menu_sector');
    }

    /**
     * Баннеры для отображения на странице категории
     */
    public function banners()
    {
        return $this->belongsToMany(Banner::class)
            ->where('type', Banner::TYPE_CATEGORY_PAGE)
            ->orderBy('priority', 'ASC');
    }

    public function parentBanners()
    {
      return $this->hasManyThrough(Category::class, BannerCategory::class, 'category_id', 'filter_categories_parent');
    }

    /**
     * Баннеры для отображения в главном меню у текущей категории
     */
    public function mainMenuBanners()
    {
        return $this->belongsToMany(Banner::class)
            ->where('type', Banner::TYPE_MAIN_MENU)
            ->orderBy('priority', 'ASC');
    }

    /**
     * Баннеры для отображения на страницах товаров из текущей категории
     */
    public function productsPageBanners()
    {
        return $this->belongsToMany(Banner::class)
            ->where('type', Banner::TYPE_PRODUCT_PAGE)
            ->orderBy('priority', 'ASC');
    }

    // Getters
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    public function hasParent(): bool
    {
        return $this->parent_id > 0;
    }
}
