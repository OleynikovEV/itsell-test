<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NPStatus extends Model
{
    protected $table = 'its_np_status';

    protected $fillable = ['ru', 'ukr', 'status_code'];
}
