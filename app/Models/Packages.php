<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    protected $table = 'its_packages';

    protected $fillable = ['product_id', 'product_series_id'];

    public $timestamps = false;
}
