<?php

namespace App\Models;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'its_users';

    protected $fillable = ['name', 'email', 'login', 'password', 'company', 'city', 'fio', 'phone', 'sale_type', 'how_sell',
        'retail_type', 'wholesale_type', 'additional_info', 'details', 'permissions', 'confirmed', 'registered',
        'code', 'basket', 'logo', 'unactive', 'unactive_weeks', 'comment', 'session_data', 'debt', 'last_payment_date',
        'activation_log', 'created_at', 't1', 'telegram_chat_id', 'viber_chat_id', 'first_touch', 'prices_type',
        'price_default', 'price_site_default', 'default_cart', 'country'];

    protected $hidden = ['password', 'remember_token',];

    protected $casts = [
        'details' => 'array',
        'prices_type' => 'array',
        'email_verified_at' => 'datetime',
    ];

    public $timestamps = false;

    public function isBanned()
    {
      return $this->unactive === 1;
    }
    /**
     * роль пользователя
     *  1 - администратор, 2 - менеджер, 3 - пользователь,
     * 4 - непроверенный пользователь, 5 - логист, 6 - клиент-предзаказ,
     * 7 - копирайтер
     */
    public function getRole(): int
    {
      return $this->permissions;
    }

    public function isAdmin(): bool
    {
      return $this->getRole() === 1;
    }

    public function isCopywriter(): bool
    {
      return $this->getRole() === 7;
    }

    /** relationships */
    public function managerOpt()
    {
      return $this->belongsTo('App\Models\ManagersInformations', 'manager', 'id');
    }

    /**
     * Заказы текущего пользователя
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /** получить дефалтную цену - использую для корзины */
    public function getPriceDefault(): string
    {
      if (! empty($this->price_default)) {
        return $this->price_default;
      }

      $type = $this->prices_type[0] ?? 'opt_price';

      return in_array($type, config('custom.prices_type')) ? $type : 'opt_price';
    }

    /** получить дефолтную цену - для отображения на сайте */
    public function getPriceSiteDefault()
    {
      if (! empty($this->price_site_default)) {
        return $this->price_site_default;
      }

      return  null;
    }

    /**
     * Подсвечиваем цену которая есть у пользователя в договоре
     */
    public function getPriceInContract(string $type): bool
    {
      return in_array($type, $this->prices_type ?? []);
    }

    /** список доступных типов цен */
    public function priceTypeList()
    {
      if ($this->isAdmin()) {
        return PriceListType::get();
      } else {
        return array_intersect_key(PriceListType::get(), array_flip($this->prices_type ?? []));
      }
    }

    /**
     * ТТН текущего пользователя, не привязанные ни к какому заказу
     */
    public function waybills()
    {
        return $this->hasMany(Waybill::class)->where('order_id', 0);
    }

    public function waitingList()
    {
      return $this->hasMany(WaitingList::class, 'user_id', 'id');
    }

  public function sendPasswordResetNotification($token)
  {
    $user = $this;
    Mail::send('emails.reset_password', ['token' => $token], function ($m) use ($user) {
      $m->from(config('custom.email_from'), config('custom.shop_name'));
      $m->to($user->email, $user->fio)->subject(__('Сброс пароля'));
    });
  }
}
