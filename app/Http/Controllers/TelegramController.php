<?php

namespace App\Http\Controllers;

use Telegram;
use App\Models\User;
use Telegram\Bot\Api;
use Illuminate\Support\Facades\DB;

class TelegramController extends Controller
{
  public function getMe()
  {
    return Telegram::getMe();
  }

  public function webhook()
  {
    // получаем новые данные
    $telegram = new Api(config('custom.telegram_bot_token'));
    $updates = $telegram->getWebhookUpdate();

    $message = $updates->getMessage();

    // если данные есть
    if ($message->count() > 0) {
      // получаем текст сообщения
      $text = $message->get('text');

      // записываем лог
      DB::table('its_tg')->insert([
        'data' => json_encode($text)
      ]);

      // если это старт
      if (strstr($text, '/start') !== false) {
        $textStrings = explode(' ', $text);

        // есть ли токен
        if (isset($textStrings[1])) {
          $token = $textStrings[1];

          // записываем лог
          DB::table('its_tg')->insert([
            'data' => json_encode($updates->getChat())
          ]);

          if ($token) {
            $user = User::find($token);

            if ($user !== null) {
              $chatId = $updates->getChat()->get('id');
              $user->telegram_chat_id = $chatId;

              DB::table('its_tg')->insert([
                'data' => 'save ' . json_encode($user->save())
              ]);
            }
          }
        }
      }
    }

    return response()->json(['status' => true], 200);
  }
}