<?php

namespace App\Http\Controllers;

use App\Exports\OrdersExport;
use App\Http\Requests\ProfileRequest;
use App\Models\City;
use App\Models\NPStatus;
use App\Models\PriceRange;
use App\Models\Product;
use App\Models\User;
use App\Models\WaitingList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use function Deployer\has;

class ProfileController extends Controller
{
    /**
     * Профиль
     */
    public function index()
    {
        // Получение текущего пользователя
        $user = auth()->user();
        $details = $user->details;
        $purchase_type = []; // способ закупки

        //$cartList = config('custom.cart_list');

        // доступные цены клиенту
        $price_type_site = [
          'price' => __('РРЦ'),
          'disable' => __('Скрыть цены'),
          '' => __('По умолчанию'),
        ];

        $price_type = $user->priceTypeList();
        $price_type_default = $user->price_default;

        if ( in_array('opt_minopt_usd', array_keys($price_type)) ) {
          $price_type_site = array_merge(['opt_minopt_usd' => __('Мин опт')], $price_type_site);
        }

        $price_type_site_default = $user->price_site_default;

        if (isset($details['purchase_types']) && !empty($details['purchase_types'])) {
          $purchase_type = $details['purchase_types'];
        }

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Личный кабинет'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Личный кабинет') . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Личный кабинет') . ' | ITsell ОПТ');

        // Создание отображения
        view()->share('activeProfileSubmenu', 'profile');
        return view('profile', compact('user', 'price_type', 'breadcrumbs', 'purchase_type',
          'price_type_site', 'price_type_default', 'price_type_site_default'));
    }

    public function price()
    {
      date_default_timezone_set('Europe/Kiev');

      // Получение текущего пользователя
      $user = auth()->user();
      $title = __('Экспорт товаров');

      // Формирование хлебных крошек
      $breadcrumbs = [];
      $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
      $breadcrumbs[] = [$title, ''];

      // Формирование мета-данных
      view()->share('layoutMetaTitle',$title . ' | ITsell ОПТ');
      view()->share('layoutMetaDescription', $title . ' | ITsell ОПТ');

      // Создание отображения
      view()->share('activeProfileSubmenu', 'price');

      $prices = \App\Models\PriceGenerator::where('user_id', $user->id);
      $prices = $prices->count() > 0 ? $prices->get() : [];

      return view('profile-price', compact('user', 'breadcrumbs', 'prices'));
    }

    public function profile($id)
    {
        // Получение текущего пользователя
        $user = User::find($id);

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Личный кабинет'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Личный кабинет') . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Личный кабинет') . ' | ITsell ОПТ');

        // Создание отображения
        view()->share('activeProfileSubmenu', 'profile');
        return view('profile', compact('user', 'breadcrumbs'));
    }

    /**
     * недавно просмотренные товары
     */
    public function recentlyViewed()
    {
      $products = collect();
      $productsIds = session('viewed_products');

      if ($productsIds) {
        $products = Product::whereIn('id', $productsIds)
          ->with('series')
          ->get()
          ->sortBy(function ($product) {
            return array_search($product->id, session('viewed_products'));
          });

        if ($products->search(fn($item) => $item->from === 1) !== false) {
          $priceRange = new PriceRange();
          view()->share('priceRangeForPrint', $priceRange->priceRangeByType());
          view()->share('amountRange', $priceRange->range);
        }
      }

      // Формирование хлебных крошек
      $breadcrumbs = [];
      $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
      $breadcrumbs[] = [__('Недавно просматривали'), ''];

      // Формирование мета-данных
      view()->share('layoutMetaTitle', __('Личный кабинет') . ' | ITsell ОПТ');
      view()->share('layoutMetaDescription', __('Личный кабинет') . ' | ITsell ОПТ');

      // Создание отображения
      view()->share('activeProfileSubmenu', 'recently_viewed');

      return view('profile-viewed', compact('products', 'breadcrumbs'));
    }

    /**
     * Изменение общей информации в профиле пользователя
     */
    public function update(ProfileRequest $request)
    {
        // Обновление данных пользователя
        $user = auth()->user();
        $user->email = request('email');
        $user->login = request('email');
        $user->fio = request('fio');
//        $user->default_cart = request('default_cart');
        $user->price_site_default = request('price_type_site');
        $user->price_default = request('price_type');
        $details = $user->details;

        if (!empty(request('purchase_type'))) {
          $details['purchase_types'] = request('purchase_type');
          $user->details = $details;
        }

        $user->phone = preg_replace('/[^0-9+]/', '', request('phone'));

        // Если указан новый пароль - изменяем его
        if (request('password')) $user->password = md5(config('auth.salt') . md5(request('password')));

        // Сохранение изменений
        $user->save();

        // Возврат успешного статуса
        session()->flash('successMessage', __('Ваш профиль успешно изменен'));
        return redirect(custom_route('profile'));
    }

    /**
     * Список заказов текущего пользователя
     */
    public function orders(Request $request, $page = null, $id = null)
    {
        date_default_timezone_set('Europe/Kiev');
        $page = $page ? $page : $request->page;

        // Установка текущей страницы пагинации
        Paginator::currentPageResolver(fn() => $page);

        // Получение списка заказов текущего пользователя
        $filters = collect($request->only([
          'start_date', 'end_date', 'fio', 'phone', 'id', 'status', 'branch',
          'delivery_type_id', 'user_comment'
        ]))->whereNotNull();

        if ($id == null) {
            $user = auth()->user();
        } else {
            $user = User::find($id);
        }

        $phone = '';

        if ($filters->has('phone')) {
          $phone = str_replace('+', '', $filters->get('phone'));
          $phone = str_replace('(', '', $phone);
          $phone = str_replace(')', '', $phone);
          $phone = str_replace('-', '', $phone);
          $phone = str_replace(' ', '', $phone);
          $phone = str_replace('_', '', $phone);
        }

        $orders = $user->orders();

        $orders = $orders
            ->when(($filters->has('start_date') && $filters->has('end_date')), function($query) use($filters) {
                $startDate = \Carbon\Carbon::createFromFormat('Y-m-d',$filters->get('start_date'))->startOfDay()->timestamp;
                $endDate = \Carbon\Carbon::createFromFormat('Y-m-d',$filters->get('end_date'))->endOfDay()->timestamp;
                return $query->whereBetween('time',[$startDate, $endDate]);
            })->when(($filters->has('start_date') && !$filters->has('end_date')), function($query) use($filters) {
                $startDate = \Carbon\Carbon::createFromFormat('Y-m-d',$filters->get('start_date'))->startOfDay()->timestamp;
                return $query->where('time', '>=', $startDate);
            })->when((!$filters->has('start_date') && $filters->has('end_date')), function($query) use($filters) {
                $endDate = \Carbon\Carbon::createFromFormat('Y-m-d',$filters->get('end_date'))->endOfDay()->timestamp;
                return $query->where('time', '<=', $endDate);
            })->when($filters->has('fio'), function($query) use($filters) {
                $fio = explode(" ", $filters->get('fio'));
                return $query->where(function($query)use($fio){
                    foreach($fio as $value) {
                        $query->where('firstname', 'LIKE', $value);
                        $query->orWhere('lastname', 'LIKE', $value);
                    }
                });
            })->when($filters->has('phone'), function($query) use($filters, $phone) {
//                return $query->where("phone", 'LIKE', '%'.$filters->get('phone').'%');
                return $query->where("phone", 'LIKE', '%'.$phone.'%');
            })->when($filters->has('id'), function($query) use($filters) {
                return $query->where("its_orders.id", 'LIKE', '%'.$filters->get('id').'%');
            })->when($filters->has('status'), function($query) use($filters) {
                return $query->whereIn('status_code_np', explode(',', $filters->get('status')));
            })->when($filters->has('branch'), function($query) use($filters) {
                return $query->whereHas("warehouse", function ($q) use ($filters){
                    $q->where('address_ru', 'LIKE', '%'.$filters->get('branch').'%')
                        ->orWhere('address_ua', 'LIKE', '%'.$filters->get('branch').'%');
                });
            })->when($filters->has('delivery_type_id'), function($query) use($filters) {
                return $query->where("delivery_type_id", $filters->get('delivery_type_id').'%');
            })->when($filters->has('user_comment'), function($query) use($filters) {
                return $query->where("user_comment", 'LIKE', '%'.$filters->get('user_comment').'%');
            })
            ->orderBy('its_orders.id', 'DESC')
            ->with('products.product', 'waybills')
            ->paginate(100)
            ->setPath(route('profile_orders', [], false));

        // Получение списка ТТН пользователя, не привязанных ни к одному заказу
        $waybills = auth()->user()->waybills()->latest('id')->limit(10)->orderBy('id', 'DESC')->get();

        // Список статусов посылки НП
        $status = NPStatus::orderBy('ru', 'ASC')->get()->toArray();
        $status[] = [
          'status_code' => null,
          'ru' => 'Все',
          'ukr' => 'Все',
        ];

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Мои заказы'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Мои заказы') . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Мои заказы') . ' | ITsell ОПТ');

        // Создание отображения
        view()->share('activeProfileSubmenu', 'orders');
        return view('profile-orders', compact('user', 'orders', 'waybills', 'breadcrumbs', 'status'));
    }

    /**
     * Список ожидания клиента
     */
    public function wishList(Request $request, $page = null)
    {
      date_default_timezone_set('Europe/Kiev');
      $page = $page ? $page : $request->page;

      // Установка текущей страницы пагинации
      Paginator::currentPageResolver(fn() => $page);

      $user = auth()->user();
      $wishList = DB::table('its_waiting_list as w')
        ->select('w.id as item_id', 'p.id', 'p.qty', 'p.when_appear', 'p.title', 'p.img', 'p.url', 'w.comment')
        ->leftJoin('its_products as p', 'p.id', '=', 'w.product_id')
        ->where('user_id', $user->id)
        ->orderBy('p.qty', 'DESC')
        ->paginate(40)
        ->setPath(route('wish_list', [], false));
      // Формирование хлебных крошек
      $breadcrumbs = [];
      $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
      $breadcrumbs[] = [__('Лист ожиданий'), ''];

      // Формирование мета-данных
      view()->share('layoutMetaTitle', __('Мой список ожидания') . ' | ITsell ОПТ');
      view()->share('layoutMetaDescription', __('Мой список ожидания') . ' | ITsell ОПТ');

      // Создание отображения
      view()->share('activeProfileSubmenu', 'wish_list');
      return view('profile-wish-list', compact('wishList', 'user', 'breadcrumbs'));
    }

    /**
     * Экспорт в excel список заказов
     */
    public function export()
    {
      return \Maatwebsite\Excel\Facades\Excel::download(new OrdersExport(auth()->user()->id, \request('start'), \request('end')), 'orders.xlsx');
    }
}
