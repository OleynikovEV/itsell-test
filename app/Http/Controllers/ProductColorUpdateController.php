<?php

namespace App\Http\Controllers;

use \App\Models\ProductsColor;
use \App\Models\Product;

class ProductColorUpdateController extends Controller
{
  public function update($id)
  {
    $colorsNames = ProductsColor::where('parent', 0)
      ->orderBy('priority', 'ASC')
      ->get()
      ->mapWithKeys(fn($item) => [mb_strtolower($item->name) => $item->color]);

    Product::whereRaw('(qty > 0 OR when_appear)')->where('parent', $id)->chunk(100, function($products) use ($colorsNames) {
      $i = 0;
      $parentId = null;
      $updateColor = [];
      foreach ($products as $product) {
        $parentId = $product->parent;
        $oldColor = $product->color;
        $isUpdate = false;
        foreach ($colorsNames as $key => $value) {
          if (strpos(mb_strtolower($product->color_name), $key) !== false) {
            $product->color = $value;
            break;
          }
        }
        $bgColor = ($product->color !== $oldColor) ? 'red' : '#000';
        if ($product->color !== $oldColor) {
          $product->save();
          $updateColor[$product->id] = $product->color;
          $isUpdate = true;
        }

        echo sprintf('<span style="color: %s"># %s Старый цвет
              <span style="background-color: #%s">&nbsp;&nbsp;&nbsp;&nbsp;</span>, новый цвет
               <span style="background-color: #%s">&nbsp;&nbsp;&nbsp;&nbsp;</span> %s %s</div><br>',
          $bgColor, ++$i, $oldColor, $product->color, $product->color_name, ($isUpdate) ? ' - обновлен' : '');
      }

      if (count($updateColor) > 0) {
        $parent = Product::where('id', $parentId)->get()->first();

        $group_products = $parent->group_products;
        foreach ($group_products as &$item) {
          if (isset($updateColor[$item->id])) {
            $item->color = $updateColor[$item->id];
          }
        }
        $parent->group_products = $group_products;
        if ($parent->save()) {
          echo 'Родитель обновлен <br>';
        }
      }
    });
  }
}
