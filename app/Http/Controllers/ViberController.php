<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class ViberController extends Controller
{
  public function webhook()
  {
    $chatId = null;
    $request = request()->all();

    DB::table('its_tg')->insert([
      'data' => 'viber ' . json_encode($request)
    ]);

    if (isset($request['event'])) { // если есть параметр собития
      // получаем событие
      $event = $request['event'];

       // определяем событие
      switch ($event) {
        case 'subscribed':
        case 'conversation_started':
          // обрабатываем данные для первого контакта с ботом
          $this->subscribed($request);
          break;
        case 'message':
          // получение сообщение от пользователя
          break;
      }
    }

    return response()->json(['success' => true], 200);
  }

  // подписка на чат бота
  private function subscribed($request): bool
  {
    $chatId = null;

    if (isset($request['user']['id'])) {
      $chatId = $request['user']['id']; // получаем ИД чата
    }

    if ($chatId === null) return false;

    if (isset($request['context'])) { // получаем ИД пользователя который передали по ссылке
      $userId = $request['context'];

      $user = User::find($userId);

      if ($user === null) return false;

       $user->viber_chat_id = $chatId;
       return $user->save();
    }

    return false;
  }
}