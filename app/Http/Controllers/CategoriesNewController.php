<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Traits\ProductsLis;
use Illuminate\Support\Facades\DB;

class CategoriesNewController extends Controller
{
  use ProductsLis;

  /**
   * Отображение страницы категории
   */
  public function show($section = '', $url = '', $params = '')
  {
    // Получение информации о категории по ссылке
    $category = Category::select(['id', 'parent_id', 'name', 'title', 'meta_title', 'meta_description', 'filter_categories'])
      ->firstWhere('name', $url);

    abort_if(! $category, 404);

    $baseUrl = custom_route('new_category', [$section, $category->name]);

    // получение всех товаров и его связей
    $products = DB::table('its_product_ref')
      ->where('brand_id', $category->id)
      ->get();

    // @TODO: Вынести в отдельный компонент - хлебные крошки и метаданные
    // Формирование хлебных крошек
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];

    if ( isset($category->parentCategory) ) {
      $breadcrumbs[] = [$category->parentCategory->title, ''];
    }
    $breadcrumbs[] = [$category->title, ''];
    view()->share('breadcrumbs', $breadcrumbs);

    // Формирование мета-данных
    $variables = ['name' => $category->title];
    if (! $category->meta_title) {
      $category->meta_title = text_template('category.title', $category->id, $variables);
    }

    if (! $category->meta_description) {
      $category->meta_description = text_template('category.description', $category->id, $variables);
    }

    view()->share('layoutMetaTitle', $category->meta_title);
    view()->share('layoutMetaDescription', $category->meta_description);

    view()->share('h1Text', $category->h1_text ?: text_template('category.h1', $category->id, $variables));

    view()->share('baseUrl', $baseUrl);

    //** TRAIT */
    return $this->display($products, $section, $url, $params);
  }

  public function filters(Request $request)
  {
    $products = DB::table('its_product_relationship');

    if ($request->has('categoriya')) {
      $products->orWhere('model_id', $request->get('categoriya'));
    }
    $products = $products->orderBy('product_id')->get();

    $viewFlters = new FiltersPanelComponent($products, $request->all());

    return response()->json([
      'filters' => $viewFlters->resolveView()->with($viewFlters->data())->render(),
    ]);
  }
}
