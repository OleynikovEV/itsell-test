<?php

namespace App\Http\Controllers;

use App\Models\NewFunctionality;
use App\Models\Page;
use Illuminate\Pagination\Paginator;

class PagesController extends Controller
{
    /**
     * Отображение статической страницы
     */
    public function show($pageUrl = '')
    {
        // Получение информации о странице
        $page = Page::where('name', $pageUrl)->first();
        if ( ! $page) abort(404);

        // Если данные запрошены в JSON-формате - возвращаем результат
        if (request('type') == 'json')
        {
            return response()->json([
                'title' => $page->title,
                'content' => $page->content
            ]);
        }

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [$page->title, ''];
        
        // Формирование мета-данных
        view()->share('layoutMetaTitle', $page->meta_title);
        view()->share('layoutMetaDescription', $page->meta_description);

        // Создание отображения
        $viewName = view()->exists('static-pages.' . $pageUrl) ? 'static-pages.' . $pageUrl : 'static-page';
        return view($viewName, compact('page', 'breadcrumbs'));
    }

    public function functionality($num = 0)
    {
      $curPage = new \stdClass;
      $curPage->name = 'new';

      Paginator::currentPageResolver(fn() => $num);

      $pages = NewFunctionality::where('show', true)
        ->orderBy('dateStartFunctionality', 'desc')
        ->paginate(20)
        ->setPath(route('static_page.new', [], false));

      // Формирование хлебных крошек
      $breadcrumbs = [];
      $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
      $breadcrumbs[] = [__('Новый функционал'), ''];

      // Формирование мета-данных
      view()->share('layoutMetaTitle', '');
      view()->share('layoutMetaDescription', '');

      return view('static-pages.functionality-pages', compact(['curPage', 'pages', 'breadcrumbs']));
    }

    public function page($num)
    {
      $curPage = new \stdClass;
      $curPage->name = 'new';

      $page = NewFunctionality::find($num);

      if ($page === null) {
        abort(404);
      }

      // Формирование хлебных крошек
      $breadcrumbs = [];
      $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
      $breadcrumbs[] = [__('Новый функционал'), ''];

      // Формирование мета-данных
      view()->share('layoutMetaTitle', '');
      view()->share('layoutMetaDescription', '');

      return view('static-pages.page', compact(['page', 'curPage']));
    }
}