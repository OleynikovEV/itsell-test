<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Brand;
use App\Models\NewFunctionality;
use App\Models\Product;
use App\Models\Category;
use App\Models\CategorySector;
use App\Models\ProductsSeries;
use App\Models\WaitingList;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $dataCacheTime = 900;

    /**
     * Constructor
     */
    public function __construct()
    {
        // Получение категорий и брендов для главного меню
        view()->share('layoutCategories', $this->getLayoutCategories());
        view()->share('layoutBrands', $this->getLayoutBrands());

        $SeriesIPrintName = 'cexol-nabereznaya-mecty'; // модельный ряд
        view()->share('layoutSeriesIPrintName', $SeriesIPrintName);
        view()->share('layoutSeriesIPrintAmount', $this->getAmountSeriesIPrint($SeriesIPrintName));

        $creativeUrlIprint = 'apple-iphone-12-pro-61/filter/brand=323'; // креативы
        view()->share('layoutCreativeIPrintUrl', $creativeUrlIprint);
        view()->share('layoutCreativeIPrintAmount', $this->getAmountCreativeIPrint(2043, 323));

        view()->share('newFunctionality', $this->getNewFunctionality());

        // Получение верхней маркетинговой плашки
        $headerBanner = Banner::where('type', Banner::TYPE_HEADER_BAR)->orderBy('priority', 'ASC')->first();
        view()->share('layoutHeaderBanner', $headerBanner);

        $priceTypeSite = null;
        view()->share('priceTypeSite', $priceTypeSite);

        // Получение списка родительских товаров в корзине и их количества
        $this->middleware(function ($request, $next) {
          $priceTypeSite = null;
          $selectedCart = getSelectedCart();
          // если пользователь залогинен
          if (! auth()->guest()) {
            $priceTypeSite = auth()->user()->getPriceSiteDefault();
          }
          view()->share('priceTypeSite', $priceTypeSite);

          $wishList = [];

          if (!auth()->guest() && in_array(Route::currentRouteName(), ['main_page', 'product', 'category', 'search', 'section', 'brand'])) {
            $user = auth()->user();
            $wishList = WaitingList::where('user_id', $user->id)->get(['product_id'])->pluck('product_id')->toArray();
          }

            $products = Product::select('id', 'parent')
                ->whereIn('id', Cart::instance($selectedCart)->content()->pluck('id'))
                ->get()
                ->pluck('parent', 'id');

            $cartProducts = Cart::instance($selectedCart)->content()->map(function($item) use ($products) {
              if ($item->options->print == true) {
                return ['parent' => $item->id, 'qty' => $item->qty];
              } else {
                return ['parent' => $products->get($item->id), 'qty' => $item->qty];
              }
            })
                ->groupBy('parent')
                ->map->sum('qty');

            view()->share('layoutCartProducts', $cartProducts);
            view()->share('wishList', $wishList);

            return $next($request);
        });
    }

    private function getAmountCreativeIPrint($menuID, $brandId)
    {
      return cache()->remember('layout_amount_creative_iprint_' . app()->getLocale(), $this->dataCacheTime, function () use ($menuID, $brandId) {
        $products = Product::where('qty', '>', 0)->where('menu', $menuID)->where('brand', $brandId)->get();
        return $products->count() ?? 0;
      });
    }

    // подсчет количества серий по принтам
    private function getAmountSeriesIPrint(string $url)
    {
      return cache()->remember('layout_amount_series_iprint_' . app()->getLocale(), $this->dataCacheTime, function () use ($url) {
        $result = ProductsSeries::where('url', $url)->get(['products_count']);


        if ($result->count() > 0) {
          return $result->first()->products_count;
        }

        return 0;
      });
    }

  /**
   * новый функционал
   */
  private function getNewFunctionality()
  {
    return  cache()->remember('getNewFunctionality' . app()->getLocale(), $this->dataCacheTime, function () {
      $newFunctionality = NewFunctionality::where('show', 1)
        ->where('dateStartFunctionality', '>', now()->subDays(30))
        ->get();

        return ($newFunctionality->count() === 0) ? 0 : $newFunctionality->count();
    });
  }

    /**
     * Получение категорий для главного меню
     */
    protected function getLayoutCategories()
    {
      // Laravel observers(наблюдатели)
       return cache()->remember('layout_categories_' . app()->getLocale(), $this->dataCacheTime, function () {
            // Получение списка родительских категорий
            $categories = Category::where('parent_id', 0)
                ->where('active', true)
                ->where('has_products', true)
                ->orderBy('priority_phones', 'DESC')
                ->orderBy('priority', 'DESC')
                ->with('mainMenuBanners')
                ->get(['id', 'code', 'parent_id', 'active', 'has_products', 'priority_phones',
                  'priority', 'name', 'title', 'menu_sector', 'vertical_menu', 'active', 'icon',
                  'menu_sector', 'products_count', 'products_count_new', 'products_count_coming'
                ]);

        // Получение списка секторов и подкатегорий для первых категорий из списка
            $sectors = CategorySector::whereIn('menu_id', $categories->pluck('id'))
                ->orderBy('priority', 'DESC')
                ->get()
                ->groupBy('menu_id');

            $subcategories = Category::whereIn('parent_id', $categories->pluck('id'))
                ->where('active', true)
                ->where('has_products', true)
                ->orderBy('priority_phones', 'DESC')
                ->orderBy('priority', 'DESC')
                ->get(['id', 'code', 'parent_id', 'active', 'has_products', 'priority_phones',
                  'priority', 'name', 'title', 'menu_sector', 'vertical_menu', 'active', 'icon',
                  'menu_sector', 'products_count', 'products_count_new', 'products_count_coming'
                ])
                ->groupBy('parent_id')
                ->map->groupBy('menu_sector');

            $categories->map(function ($category) use ($sectors, $subcategories) {
                if ($subcategories->has($category->id)) {
                    $category->sectors = $sectors->get($category->id) ?: collect();

                    $category->sectors->map(function ($sector) use ($subcategories) {
                        $sector->subcategories = $subcategories->get($sector->menu_id)->get($sector->id) ?: collect();
                        return $sector;
                    });

                    $category->subcategories = $subcategories->get($category->id)->get(0);
                }

                $category->mainMenuBanner = $category->mainMenuBanners->first();
                unset($category->mainMenuBanners);

                return $category;
            });

            return $categories;
        });
    }

    /**
     * Получение списка брендов для главного меню
     */
    protected function getLayoutBrands()
    {
        return cache()->remember('layout_brands_' . app()->getLocale(), $this->dataCacheTime, function()
        {
            $brands = Brand::where('active', true)
                ->where('products_count', '>', 0)
                ->orderBy('priority', 'ASC')
                ->get();

            return $brands;
        });
    }

    /**
     * Склейка дубликатов URL'ов
     *
     * Некоторые страницы можно открыть по разным версиям написания URL'а, но правильной является только одна версия.
     * Метод сравнивает текущий URL с правильной версией (переданной в параметре) и, в случае отличия, делает редирект.
     */
    protected function matchUrlDuplicates($mainUrl = '')
    {
        $mainUrl = trim($mainUrl);

        if ($mainUrl != request()->getPathInfo())
        {
            if (app()->getLocale() != config('app.default_locale')) $mainUrl = '/' . app()->getLocale() . $mainUrl;
            redirect(url($mainUrl), 301)->send();
        }
    }

}
