<?php

namespace App\Http\Controllers;

use App\Events\VchehleEvent;
use App\Models\Order;
use App\Models\Waybill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
  const order_cancelled = 'order_cancelled';
  const order_shipped = 'order_shipped';
  const order_processing_started = 'order_processing_started';

  const status_cancelled = -1;
  const status_shipped = 1;
  const status_in_processing= 0;

  private $order;

  public function index(Request $request)
  {
    $secret = config('custom.vchehle_secret');

    // если отсутствует подпись запроса
    if (!$request->headers->has('Signature')) {
      Log::error('Отсутствует подписка signature');
      return response('Bad request signature', 400);
    }

    // если отсутствуют обязательные данные в запросе
    if (!$request->has('type') || !$request->has('data')) {
      Log::error('В запросе отсутствует Type или data');
      return response('В запросе отсутствует Type или data', 400);
    }

    $data = $request->all();
    $getSignature = $request->headers->get('Signature');

    $payload = json_encode($data);
    $signature = hash_hmac('sha256', $payload, $secret);

    // сравниваем сигнатуры
    if ($signature != $getSignature) {
      Log::error('Хэш запроса не совпал');
      return response('Bad request hash', 400);
    }

    $this->order = Order::where('id', $data['data']['partner_code']);

    switch ($data['type']) {
      case self::order_processing_started:
        $this->orderInProcessing();
        break;
      case self::order_cancelled:
        if (!isset($data['data']['partner_code'])) {
          Log::error('Отсутствует partner_code');
          return response('Bad request', 400);
        }
        if (!isset($data['data']['reason'])) {
          Log::error('Отсутствует reason');
          return response('Bad request', 400);
        }
        $this->orderCancelled($data['data']['reason'], $data['data']['comment']);
        break;
      case self::order_shipped:
        if (!isset($data['data']['partner_code'])) {
          Log::error('Отсутствует partner_code или waybill');
          return response('Bad request', 400);
        }
        $this->orderShipped($data['data']['waybill'] ?: null);
        break;
    }

    event(new VchehleEvent($this->order->first()));

    return response('Success', 200);
  }

  /**
   * Событие начала производства заказа
   */
  private function orderInProcessing(): bool
  {
    if ($this->order->count() === 0) return false;

    $this->order->update([
      'comment' => 'Заказ взят в работу',
      'vchehle_order_status' => self::status_in_processing,
    ]);

    return true;
  }

  /**
   * помечаем заказ как отменен производством
   * @param string $message
   */
  private function orderCancelled(string $message, $comment): void
  {
    $this->order->update([
      'order_cancelled' => $message . ($comment) ? ' ' . $comment : '',
      'vchehle_order_status' => self::status_cancelled,
    ]);
  }

  /**
   * помечаем заказа как отправлен
   * @param string|null $ttn
   */
  private function orderShipped(?string $ttn)
  {
    $this->order->update([
      'vchehle_order_status' => self::status_shipped,
      'comment' => !empty($ttn) ? 'TTH ' . $ttn : '',
    ]);

    if (!empty($ttn) ) {
      $order = $this->order->first();

      if (isset($order->user_id)) {
        $waybill = new Waybill();
        $waybill->user_id = $order->user_id;
        $waybill->order_id = $order->id;
        $waybill->waybill = $ttn;
        $waybill->created_at = time();
        $waybill->updated_at = time();
        $waybill->updated_at = time();
        $waybill->save();
      }
    }
  }
}
