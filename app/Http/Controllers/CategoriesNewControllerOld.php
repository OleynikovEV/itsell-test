<?php

namespace App\Http\Controllers;

use App\Http\Models\Filters\Filters;
use App\Http\Models\UrlParser;
use App\Models\Category;
use App\Http\Models\ProductsRelationshipsCollection;
use App\View\Components\FiltersPanelComponent;
use App\View\Components\ProductCardComponent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesNewController extends Controller
{
  /**
   * Отображение страницы категории
   */
  public function show($section = '', $url = '', $params = '')
  {
//    dump( $section, $url, $params );die;

    // Получение информации о категории по ссылке
    $category = Category::select(['id', 'parent_id', 'name', 'title', 'meta_title', 'meta_description', 'filter_categories'])
      ->firstWhere('name', $url);

    abort_if(! $category, 404);

    $baseUrl = custom_route('new_category', [$section, $category->name]);
    $parseUrl = new UrlParser($section, $url, $params);

    // получение всех товаров и его связей
    $products = DB::table('its_product_relationship')
      ->where('menu_id', $category->id)
      ->orWhere('category_id', $category->id)
      ->orWhere('model_id', $category->id)
      //->orderBy('product_id')
      ->get();

    //$products = new ProductsRelationshipsCollection($products);
    //$productIds = $products->getProductIds()->toArray();
    $productIds = $products->pluck('product_id')->toArray();

    $filterCategories = Filters::categories($productIds, $parseUrl->categoriya);
    $filterCategories = $filterCategories->get(Filters::FILTER_CATEGORIES);

    // если в блоке категория только один фильтр, делаем его активным
    if ($parseUrl->categoriya === null && $filterCategories->valuesCount === 1) {
      $parseUrl->setParameters([
        'categoriya' => [$filterCategories->values->get(0)->id]
      ]);
    }

    // если есть выбранные фильтры - отбираем товары по выбранным фильтрам
    if ($parseUrl->hasFilters()) {

      $exclude = [];
      if (! $parseUrl->selectedCategoriesOnly()) {
        $exclude = ['categoriya'];
      }

      $productFilter = DB::table('its_product_filter')
        ->whereIn('filter_value_id', $parseUrl->getActiveFiltersArray($exclude))
        ->whereIn('product_id', $productIds)
        ->pluck('product_id');

      $productIds = $productFilter->toArray();
    }

    $filters = collect();
    if ($parseUrl->categoriya !== null) {
      $filters = Filters::filters($productIds, $parseUrl);
    }

    if (request()->has('type') && request()->get('type') === 'json') {
      $componentFilters = new FiltersPanelComponent($filterCategories, $filters);
      $componentProducts = new ProductCardComponent($productIds, $parseUrl);

      return response()->json([
        'filtersHtml' => $componentFilters->resolveView()->with($componentFilters->data())->render(),
        'productsHtml' => $componentProducts->resolveView()->with($componentProducts->data())->render(),
      ]);
    }

//    dd($category->parent_id, $category->filter_categories, ($category->parent_id !== 0 && $category->filter_categories !== 1));
    $showGadgetsPanel = ($category->parent_id !== 0 && $category->filter_categories !== 1);
    $showSubcategoriesPanel = true;

//    dump( $productCategories->countBy('menu_id') );
//    dump( $productCategories->countBy('category_id') );
//    dump( $productCategories->countBy('model_id') );

//    $productIds = $productCategories->pluck('product_id');

    // Фильтры
//    $productCategoriesFilters = DB::table('its_product_filter')
//      ->whereIn('product_id', $productIds)
//      ->where('filter_id', 84)
//      ->get();

//    dump( $productCategoriesFilters->countBy('filter_id') );
//    dump( $productCategoriesFilters->countBy('filter_value_id') );


//    $prices = DB::table('its_products')
//      ->selectRaw('min(opt_price) as minPrice, max(opt_price) as maxPrice')
//      ->whereIn('id', $productIds)
//      ->get();

    //  Цена
//    dump( $prices->get(0)->minPrice );
//    dump( $prices->get(0)->maxPrice );

    // Бренды
//    $brands = DB::table('its_products')
//      ->selectRaw('COUNT(its_products.id) as amount, its_products.brand, its_brands.our')
//      ->leftJoin('its_brands', 'its_brands.id', '=', 'its_products.brand')
//      ->whereIn('its_products.id', $productIds)
//      ->groupBy('its_products.brand')
//      ->orderByRaw('our DESC, amount DESC')
//      ->get()
//      ->groupBy('our');
//      ->pluck('amount', 'brand');

//    dump( $brands );

    // Цвет

    // Другое

    // Серии + группы (техничка)

    // @TODO: Вынести в отдельный компонент - хлебные крошки и метаданные
    // Формирование хлебных крошек
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];

    if ( isset($category->parentCategory) ) {
      $breadcrumbs[] = [$category->parentCategory->title, ''];
    }
    $breadcrumbs[] = [$category->title, ''];
    view()->share('breadcrumbs', $breadcrumbs);

    // Формирование мета-данных
    $variables = ['name' => $category->title];
    if (! $category->meta_title) {
      $category->meta_title = text_template('category.title', $category->id, $variables);
    }

    if (! $category->meta_description) {
      $category->meta_description = text_template('category.description', $category->id, $variables);
    }

    view()->share('layoutMetaTitle', $category->meta_title);
    view()->share('layoutMetaDescription', $category->meta_description);

    view()->share('h1Text', $category->h1_text ?: text_template('category.h1', $category->id, $variables));

    return view('shop', compact('productIds', 'filterCategories', 'filters', 'parseUrl', 'baseUrl',
      'showGadgetsPanel')
    );
  }

  public function filters(Request $request)
  {
    $products = DB::table('its_product_relationship');

    if ($request->has('categoriya')) {
      $products->orWhere('model_id', $request->get('categoriya'));
    }
    $products = $products->orderBy('product_id')->get();

    $viewFlters = new FiltersPanelComponent($products, $request->all());

    return response()->json([
      'filters' => $viewFlters->resolveView()->with($viewFlters->data())->render(),
    ]);
  }
}
