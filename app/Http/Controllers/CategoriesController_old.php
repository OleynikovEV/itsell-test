<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use App\Traits\DisplaysProductsList_old;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class CategoriesController_old extends Controller
{
    use DisplaysProductsList_old;

    /**
     * Отображение страницы категории
     */
    public function show($section = '', $url = '', $params = '')
    {
        if ($url == 'razmernye-plenki') {
            $params = Route::current()->parameter('params');
            $url = 'futlyaru-dlya-naushnikov';
            return redirect()->route('category',
                [
                    'section' => $section,
                    'url' => $url,
                    'params' => $params
                ],
                301
            );
        }

        // Получение информации о категории по ссылке
        if (! $category = Category::firstWhere('name', $url)) {
            abort(404);
        }

        // Получение списка подкатегорий
        $subcategoriesIds = $category->subcategories->merge([$category])
            ->map(function ($subcategory) {
                return array_merge(
                    [$subcategory->id],
                    $subcategory->include_categories ? explode(',', $subcategory->include_categories) : []
                );
            })
            ->flatten()
            ->unique();

        // используем для поиска в дополнительных меню
//        $whereSubcategoriesIds = '[[:<:]](' . implode('|', $subcategoriesIds->toArray()) . ')[[:>:]]';

        // Получение списка товаров в категории
        $products = Product::active()
            //->where('from', 0)
//            ->where(function ($q) use ($whereSubcategoriesIds, $subcategoriesIds) {
            ->where(function ($q) use ($subcategoriesIds) {
                $q->whereIn('menu', $subcategoriesIds)
//                    ->orWhere('addit_menus', 'regexp', $whereSubcategoriesIds)
                    ->orWhereIn('id', DB::table('its_product_category')
                        ->select('product_id')
                        ->whereIn('category_id', $subcategoriesIds)
                    );
            })
            ->orderByRaw('`from` ASC, qty > 0 DESC, added_time >= ' . today()->subDays(Product::NOVELTY_DAYS)->timestamp . ' DESC, '
                . 'liquidity > 0 ASC, arrived_in_stock_at DESC, id DESC')
            ->get(['id', 'baseid', 'url', 'title', 'active', 'qty', 'when_appear', 'opt_price', 'old_opt_price', 'brand',
              'img', 'show_price', 'filters_values', 'group_products', 'price_types', 'box_image', 'menu', 'addit_menus',
              'from', 'products_series_id', 'color', 'price', 'price_modify_time', 'liquidity', 'arrived_in_stock_at', 'added_time']);

        // Получение баннера для отображения на странице категории
        $banner = $category->banners()->first();

        if (! $banner && $category->parentCategory) {
            $banner = $category->parentCategory->banners()->first();
        }

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];

        if ($category->parentCategory)
        {
            $breadcrumbs[] = [
                $category->parentCategory->title,
                ($category->parentCategory->name == 'chehly' || $category->parentCategory->name =='stekla-plenki')
                  ? ''
                  : custom_route('category', ['phones', $category->parentCategory->name])
            ];
        }

        $breadcrumbs[] = [$category->title, ''];

        view()->share('breadcrumbs', $breadcrumbs);

        // Формирование мета-данных
        $variables = ['name' => $category->title];

        if (! $category->meta_title) {
            $category->meta_title = text_template('category.title', $category->id, $variables);
        }

        if (! $category->meta_description) {
            $category->meta_description = text_template('category.description', $category->id, $variables);
        }

        view()->share('layoutMetaTitle', $category->meta_title);
        view()->share('layoutMetaDescription', $category->meta_description);

        view()->share('h1Text', $category->h1_text ?: text_template('category.h1', $category->id, $variables));

        // Отображение стандартного списка товаров
        $filtersActiveParentCategory = null;

        if (! $category->parentCategory) {
            $filtersActiveParentCategory = $category;
        } elseif ($category->filter_categories_parent) {
            $filtersActiveParentCategory = Category::find($category->filter_categories_parent);
        }

        // Если нужно показывать seo родительской категории, раскомментировать код
//        if ($category->hasParent()) {
//            $seoDescription = $category->parentCategory->getSeoDescription();
//        } else {
            $seoDescription = $category->getSeoDescription();
//        }

        // получаем ссылку по серии
        if ($category->parent_id !== 0) {
          // выбираем все бренды, индексируем массив по ИД категорий
          $brands = Category::where('brand', 1)->get(['id', 'title', 'name'])->keyBy('id')->toArray();

          //  если категория ссылается на основной бренд, получаем ссылку
          if (isset($brands[$category->parent_id])) {
            $brands = $brands[$category->parent_id];
            $url = $brands['name'] . '/filter/categories=' .  $category->id;
//            dd($category);
          } else { // проверяем по названию, если зашли с вертикального меню
            $title = $category->title;
            $key = array_filter($brands, function($item) use ($title) {
              return $item['title'] === $title;
            });
            $key = key($key);

            if ($key !== null) {
              $url = $brands[$key]['name'];
            } else {
              $url = '';
            }
          }
        }

        $groupBySeries = (request()->cookie('groupBySeries') === null) ? true : request()->cookie('groupBySeries') == 'true';

        if (! empty($params) ) {
          $url .= '/' . $params;
          $url = removePageNum($url);
        }

        return $this->displayProductsList(
          $products,
          custom_route('category', [$section, $category->name]),
          [
            'banner' => $banner,
            'categories_filter' => ! $category->parentCategory || $category->filter_categories,
            'categories_filter_active_parent' => $filtersActiveParentCategory,
          ],
          $seoDescription,
          $groupBySeries,
          $url
        );
    }

    /**
     * Список категорий общих аксессуаров
     */
    public function accessories()
    {
        // Получение родительской категории общих аксессуаров
        if (! $accessoriesCategory = Category::find(config('custom.accessories_category_id'))) {
            abort(404);
        }

        // Получение списка подкатегорий
        $categories = view()->shared('layoutCategories')
            ->firstWhere('id', config('custom.accessories_category_id'))
            ->subcategories;

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Аксессуары'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', $accessoriesCategory->meta_title);
        view()->share('layoutMetaDescription', $accessoriesCategory->meta_title);

        $h1Text = __('Общие аксессуары');

        // Создание отображения
        return view('categories', compact(['categories', 'h1Text', 'breadcrumbs']));
    }
}
