<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Http\Requests\SendMessageRequest;
use App\Mail\SendMessageMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\FoundCheaperRequest;
use App\Mail\FeedbackMail;
use App\Mail\FoundCheaperMail;
use App\Models\Product;

class FeedbackController extends Controller
{

    /**
     * Отправка формы Написать директору
     */
    public function store(FeedbackRequest $request)
    {
        // Отправка сообщения
        Mail::to(config('custom.mail_recipients.feedback'))->send(new FeedbackMail(request()->all()));

        if (!auth()->guest()) {
          $user = auth()->user();
          if ($user) {
            $details = $user->details;
            $details['purchase_types'] = request('purchase_type');
            $user->details = $details;
            $user->save();
          }
        }

        // Возврат успешного статуса
        return response()->json([
            'success' => true,
            'message' => __('Спасибо за Ваше сообщение. Мы свяжемся с Вами в ближайшее время')
        ]);
    }

    /**
     * Отправка формы Нашли дешевле
     */
    public function foundCheaper(FoundCheaperRequest $request)
    {
        // Получение информации о товаре
        $product = Product::find(request('product_id'));
        if ( ! $product)
        {
            return response()->json([
                'success' => false,
                'messages' => [__('Произошла непредвиденная ошибка. Обновите страницу для продолжения')]
            ]);
        }

        // Отправка сообщения
        Mail::to(config('custom.mail_recipients.found_cheaper'))->send(new FoundCheaperMail(request()->all(), $product));

        // Возврат успешного статуса
        return response()->json([
            'success' => true,
            'message' => __('Спасибо за Ваше сообщение. Мы свяжемся с Вами в ближайшее время')
        ]);
    }

    public function sendMessage(SendMessageRequest $request)
    {
      // Отправка сообщения
      Mail::to(config('custom.director'))->send(new SendMessageMail(request()->all()));

      if (!auth()->guest()) {
        $user = auth()->user();
        if ($user) {
          $details = $user->details;
          $details['purchase_types'] = request('purchase_type');
          $user->details = $details;
          $user->save();
        }
      }

      // Возврат успешного статуса
      return response()->json([
        'success' => true,
        'message' => __('Спасибо за Ваше сообщение.')
      ]);
    }
}
