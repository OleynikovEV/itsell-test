<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Traits\DisplaysProductsList;
use Illuminate\Support\Str;

class BrandsController extends Controller
{

    use DisplaysProductsList;

    /**
     * Список всех брендов
     */
    public function index()
    {
        // Получение списка всех активных брендов
        $brands = Brand::where('active', true)->orderBy('priority', 'ASC')->get();

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Бренды'), ''];
        
        // Формирование мета-данных
        view()->share('layoutMetaTitle', text_template('brands.title'));
        view()->share('layoutMetaDescription', text_template('brands.description'));

        $h1Text = text_template('brands.h1');

        // Создание отображения
        return view('brands', compact('brands', 'h1Text', 'breadcrumbs'));
    }

    /**
     * Отображение страницы бренда
     */
    public function show($url = '', $params = '')
    {
        // Получение информации о бренде
        $brand = Brand::firstWhere('name', $url);
        if ( ! $brand) abort(404);

        // Получение списка товаров текущего бренда
        $products = Product::active()->where('brand', $brand->id);
//          ->orderByRaw( sprintf(config('custom.product_order_by'), today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp) )
          //            ->orderByRaw('qty > 0 DESC, added_time >= ' . today()->subDays(Product::NOVELTY_DAYS)->timestamp . ' DESC, '
//                . 'liquidity > 0 ASC, arrived_in_stock_at DESC, id DESC')
//            ->get(config('custom.product_fields'));

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Бренды'), custom_route('brands')];
        $breadcrumbs[] = [$brand->title, ''];

        view()->share('breadcrumbs', $breadcrumbs);
        
        // Формирование мета-данных
        $variables = ['name' => $brand->title];

        view()->share('layoutMetaTitle', $brand->meta_title ?: text_template('brand.title', $brand->id, $variables));
        view()->share('layoutMetaDescription', $brand->meta_description ?: text_template('brand.description', $brand->id, $variables));

        view()->share('h1Text', text_template('brand.h1', $brand->id, $variables));

       $groupBySeries = (request()->cookie('groupBySeries') === null) ? true : request()->cookie('groupBySeries') == 'true';

      // Отображение стандартного списка товаров
        return $this->displayProductsList(
          $products,
          custom_route('brand', [$brand->name]),
          [
            'banner' => null,
            'categories_filter' => true,
            'categories_filter_active_parent' => null,
            'brands_filter' => false,
            'urlCategory' => removePageNum($params),
            'labelFroGroupClass' => 'text-orange bold',
            'labelFroGroupText' => (!$params) ? $brand->title : $this->getBrand($params),
          ],
          '', $groupBySeries
        );
    }

    protected function getBrand($params): string
    {
      $before = Str::before($params, '/');
      return ($before === 'filter') ? '' : ucfirst($before);
    }
}