<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\SearchModel;
use App\Traits\DisplaysProductsList;
use App\View\Components\FastSearchGroup;
use App\View\Components\FastSearchSingleProduct;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SearchController extends Controller
{
    use DisplaysProductsList;

    private $limit = 1500;

    /**
     * Страница поиска
     */
    public function index($searchPhrase, $params = '')
    {
      return $this->search($searchPhrase, null, $params);
    }

    /**
     * Страница поиска
     */
    public function category($categoryUrl, $searchPhrase, $params = '')
    {
      return $this->search($searchPhrase, $categoryUrl, $params);
    }

    protected function search($searchPhrase, $categoryUrl = null, $params = '')
    {
        $searchPhrase = removeEmoji($searchPhrase);
        $moreLimit = false;

        // Поиск товаров
        if (strlen($searchPhrase) >= 4 && ! preg_match('/[^0-9]/', $searchPhrase)) {
            $products = Product::active()
              ->where('baseid', 'like', '%' . $searchPhrase)
              ->orWhere('id', $searchPhrase);

            if ( ! $products->count()) {
                unset($products);
            }
        }

        if ( ! isset($products)) {
            $products = Product::active()
                ->whereNotIn('menu', Category::select('id')->where('id', 237)->orWhere('parent_id', 237));

            if ($categoryUrl !== null) {
              // Получение информации о категории по ссылке
              if (! $category = Category::firstWhere('name', $categoryUrl)) {
                abort(404);
              }

              $subcategoriesIds = DB::table('its_menu')
                ->where('parent_id', $category->id)
                ->get()->merge([$category])
                ->map(function ($subcategory) {
                  return array_merge(
                    [$subcategory->id],
                    $subcategory->include_categories ? explode(',', $subcategory->include_categories) : []
                  );
                })
                ->flatten()
                ->unique();

              $products = $products->where(function ($q) use ($subcategoriesIds) {
                $q->whereIn('menu', $subcategoriesIds)
                  ->orWhereIn('id', DB::table('its_product_category')
                    ->select('product_id')
                    ->whereIn('category_id', $subcategoriesIds)
                  );
              });
            }

            $queryString = $this->cookPhrase($searchPhrase); // получаем поисковую фразу

            $products = $products
              ->select(DB::raw("SQL_CALC_FOUND_ROWS *"))
              ->whereRaw("MATCH(title) AGAINST('" . $queryString . "' IN BOOLEAN MODE)");
        }

        // Если данные запрошены для окна быстрого поиска - возвращаем результат
        $take = 10;
        if (request('type') == 'preview') {
            $products = $products->get(['id', 'title', 'img', 'opt_price', 'url']);
            $moreLimit = $products->count();
            if ($moreLimit > $take) {
              $verticalMenu = DB::table('its_menu', 'm')
                ->selectRaw('DISTINCT m.title, COUNT(DISTINCT p.id) AS `total`, m.name AS url')
                ->leftJoin('its_menu AS sm', 'sm.parent_id', '=', 'm.id')
                ->leftJoin('its_products AS p', function(Builder $query) {
                  $query->on('p.menu', '=', 'sm.id');
                  $query->orWhereIn('sm.id',
                    DB::table('its_product_category AS pc')->select('pc.category_id')->where('pc.product_id', DB::raw('p.id'))
                  );
                })
                ->where('m.vertical_menu', 1)
                ->where('m.has_products', '>', 0)
                ->whereIn('p.id', $products->pluck('id')->toArray())
                ->groupBy('m.id')
                ->orderByDesc('total')
                ->get();

              $response = new FastSearchGroup($searchPhrase, $moreLimit, $verticalMenu);

            } else {
              $products = $products->take($take)->map(function ($product) {
                return [
                  'title' => $product->title,
                  'image' => $product->img ? config('custom.content_server') . $product->img : '/template/images/noimage.jpg',
                  'url' => custom_route('product', [$product->url, $product->id]),
                  'price' => $product->opt_price,
                  'price_uah' => round($product->opt_price * settings('dollar'))
                ];
              });

              $response = new FastSearchSingleProduct($products);
            }

            return response()->json([
                'success' => true,
                'response' => $response->resolveView()->with($response->data())->render()
            ]);
        }

        $products = $products->limit($this->limit);

        // записываем поисковые фразы
        SearchModel::updateOrCreate(['query' => $searchPhrase])->increment('count');

        // Получение баннера для страницы поиска
        $banner = Banner::where('type', Banner::TYPE_SEARCH_PAGE)->orderBy('priority', 'ASC')->first();

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Поиск'), ''];
        $breadcrumbs[] = ['"' . $searchPhrase . '"', ''];

        view()->share('breadcrumbs', $breadcrumbs);

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Поиск') . ' "' . $searchPhrase . '" | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Поиск') . ' "' . $searchPhrase . '" | ITsell ОПТ');

        if ($moreLimit > $this->limit) {
          view()->share('searchLimitText',
            'Показано ' . $this->limit . ' из ' . $moreLimit . ', пожалуйста введите более низкочастотный запрос, что бы отобразились все товары.'
          );
        }

        view()->share('h1Text', __('Поиск'));

        $groupBySeries = (request()->cookie('groupBySeries') === null) ? true : request()->cookie('groupBySeries') == 'true';

        $custom_route = ($categoryUrl === null)
          ? custom_route('search', [$searchPhrase])
          : custom_route('search_category', [$categoryUrl, $searchPhrase]);

        // Отображение стандартного списка товаров
        return $this->displayProductsList(
          $products,
          $custom_route,
          [
            'banner' => $banner,
            'categories_filter' => true,
            'categories_filter_active_parent' => null,
            'brands_filter' => true,
            'urlCategory' => removePageNum($params),
            'labelFroGroupClass' => 'text-orange bold',
            'labelFroGroupText' => ($params) ? $this->getBrand($params) : Str::limit($searchPhrase, 20) . ((Str::length($searchPhrase) > 20) ? '...' : ''),
          ],
          '',
          $groupBySeries
        );
    }

  /**
   * формируем фразу для поиска
   * @param string $searchPhrase
   * @return string
   */
    protected function cookPhrase(string $searchPhrase): string
    {
      // удаляем ДЛЯ
      $searchPhraseOriginal = $searchPhrase;
      $searchPhraseOriginal = str_replace(['\''], '', $searchPhraseOriginal);
      $searchPhrase = trim(Str::lower($searchPhrase));
      $searchPhrase = str_replace(['для', '\'', '"', '(', ')'], '', $searchPhrase);
      $phrases = explode(' ', $searchPhrase);

      // имеет смысл переводить слова из рус - анг - укр
//      $transliteration = $this->transliteration($searchPhrase);
      $searchPhrase = [];
      $searchPhrase[0] = $phrases;
//      $searchPhrase[1] = explode(' ', $transliteration['en']);
//      $searchPhrase[2] = explode(' ', $transliteration['ru']);

      $queryString = [];
      foreach ($searchPhrase as $phrase) {
        $query = [];
        foreach ($phrase as $item) {
          $len = Str::length($item);
          switch (true) {
            case ($len === 0):
              break;
            case ($len <= 2):
              $query[] = '+' . $item;
              break;
            case ($len === 3):
              $query[] = '+' . $item . "*";
              break;
            case ($len > 3 && $len <= 6):
              $query[] = '+' . mb_substr($item, 0, -1, 'UTF-8') . "*";
              break;
            case ($len > 6 && $len <= 9):
              $query[] = '+' . mb_substr($item, 0, -2, 'UTF-8') . "*";
              break;
            case ($len > 9):
              $query[] = '+' . mb_substr($item, 0, -3, 'UTF-8') . "*";
              break;
            default:
              break;
          }
        }

        $queryString[] = sprintf('<(%s)', implode(' ', $query));
      }

      $result = sprintf('>"%s" %s', $searchPhraseOriginal, implode(' ', $queryString));

      return $result;
    }


    protected function getBrand($params): string
    {
      $before = Str::before($params, '/');
      return ($before === 'filter') ? '' : ucfirst($before);
    }

    private function transliteration(string $string)
    {
      $ru = [
        'й',
        'ц',
        'у',
        'к',
        'е',
        'н',
        'г',
        'ш',
        'щ',
        'з',
        'х',
        'ъ',
        'ф',
        'ы',
        'в',
        'а',
        'п',
        'р',
        'о',
        'л',
        'д',
        'ж',
        'э',
        'я',
        'ч',
        'с',
        'м',
        'и',
        'т',
        'ь',
        'б',
        'ю',
      ];
      $en = [
        'q',
        'w',
        'e',
        'r',
        't',
        'y',
        'u',
        'i',
        'o',
        'p',
        '[',
        ']',
        'a',
        's',
        'd',
        'f',
        'g',
        'h',
        'j',
        'k',
        'l',
        ';',
        '"',
        'z',
        'x',
        'c',
        'v',
        'b',
        'n',
        'm',
        ',',
        '.',
      ];

      return [
        'en' => str_replace($en, $ru, $string),
        'ru' => str_replace($ru, $en, $string),
      ];
    }
}