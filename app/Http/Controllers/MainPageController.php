<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\PriceRange;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class MainPageController extends Controller
{
    /**
     * Отображение главной страницы
     */
    public function index()
    {
        // Склейка дубликатов URL'ов
        $this->matchUrlDuplicates('/');

        // Получение изображений для слайдера
        $banners = Banner::where('type', Banner::TYPE_MAIN_SLIDER)->orderBy('priority', 'ASC')->get();

        // Получение списка новинок
        $locale = app()->getLocale();

        $newProducts = cache()->remember('main_page_new_' . $locale, $this->dataCacheTime, function () {
            return Product::active()
                ->where('brand', '<>', 323)
                ->where('qty', '>', 0)
                ->where('added_time', '>=', today()->subDays(Product::NOVELTY_DAYS)->timestamp)
                //->where('title', 'not like', '%Уценка%')
                ->where('markdown', '<>', 1)
                ->inRandomOrder()
                ->limit(20)
                ->with('series')
                ->get();
        });

        // Получение товаров в ожидании
        $waitingProducts = cache()->remember('main_page_waiting_' . $locale, $this->dataCacheTime, function () {
            return Product::active()
                ->where('qty', '<=', 0)
                ->where('when_appear', '!=', '')
                ->inRandomOrder()
                ->limit(20)
                ->with('series')
                ->get();
        });

        // Получение списка товаров со скидкой
        $saleProducts = cache()->remember('main_page_sale_' . $locale, $this->dataCacheTime, function () {
            $liquidityGroups = [];
            if (settings('products_so_discount')) $liquidityGroups[] = 2;
            if (settings('products_nl_discount')) $liquidityGroups[] = 3;

            return Product::where('parent', 0)->where('active', true)->where('qty', '>', 0)
                ->where(function ($q) use ($liquidityGroups) {
                    $q->where(function ($q) {
                        $q->whereRaw('old_opt_price > opt_price')
                            ->where('price_modify_time', '>=', today()->subWeek(4)->timestamp);
                    })
                    ->orWhereIn('liquidity', $liquidityGroups);
                })
                ->inRandomOrder()
                ->limit(20)
                ->with('series')
                ->get();
        });

        // Получение баннера для отображения между товарами
        $productsBanner = Banner::where('type', Banner::TYPE_MAIN_PAGE)->orderBy('priority', 'ASC')->first();

        // Получение списка видео для страницы
        $videos = [
            [
                'name' => 'Чехол Silicone Cover Full Protective (AA)',
                'code' => 'b7XssS9hieM',
                'date' => '02.04.2020'
            ],
            [
                'name' => 'Чехол Deen CrystalRing под магнитный держатель',
                'code' => 'XSXzf-EgdN8',
                'date' => '23.03.2020'
            ],
            [
                'name' => 'Кожаный чехол книжка Nillkin Qin Series',
                'code' => '3KmaE5354UI',
                'date' => '23.03.2020'
            ],
            [
                'name' => 'Кожаный чехол книжка Art Case с визитницей',
                'code' => 'jOhEMYwaOEw',
                'date' => '19.03.2020'
            ],
        ];

        // Получение списка просмотренных товаров
        if ($viewedProducts = session('viewed_products')) {
            $viewedProducts = Product::whereIn('id', $viewedProducts)
                ->with('series')
                ->get()
                ->sortBy(function ($product) {
                    return array_search($product->id, session('viewed_products'));
                });

            if( $viewedProducts->search(fn($item) => $item->from === 1) !== false ) {
                $priceRange = new PriceRange();
                view()->share('priceRangeForPrint', $priceRange->priceRangeByType());
                view()->share('amountRange', $priceRange->range);
            }
        }

        // Формирование мета-данных
        view()->share('layoutMetaTitle', settings('meta_title'));
        view()->share('layoutMetaDescription', settings('meta_description'));

        // Задание флага главной страницы для общего шаблона
        view()->share('layoutMainPage', true);

        $seoDescription = settings('seo_description');


//        $priceRange = new PriceRange();
//        view()->share('priceRangeForPrint', $priceRange->priceRangeByType());
//        view()->share('amountRange', $priceRange->range);

        // Создание отображения
        return view('main-page', compact([
            'banners',
            'newProducts',
            'waitingProducts',
            'saleProducts',
            'productsBanner',
            'videos',
            'viewedProducts',
            'seoDescription',
        ]));
    }

    /**
     * Страница 404-ошибки
     */
    public function notFound()
    {
        return view('errors.not-found');
    }

    /**
     * Редирект на актуальный файл с прайс-листом (используется для предоставления постоянного URL'а)
     */
    public function priceList()
    {
        return redirect(url('/price_lists/' . (settings('price_list_name') ?: 'price_list.xls')));
    }
}
