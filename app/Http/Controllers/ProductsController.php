<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Banner;
use App\Models\BannerCategory;
use App\Models\Category;
use App\Models\City;
use App\Models\Filter;
use App\Models\PriceRange;
use App\Models\Product;
use App\Models\FiltersValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    /**
     * Страница товара
     */
    public function show($url = '', $id = 0)
    {
        // Получение информации о товаре
        $product = Product::find($id);

        if ( ! $product) {
            Product::where('url', $url)->first();
        }

        if ( ! $product) {
            abort(404);
        }

        // Склейка дубликатов URL'ов
        $this->matchUrlDuplicates(route('product', [$product->url, $product->id], false));

        // Получение изображений товара
        $productImages = $product->getImages(true);

        // Получение списка значений фильтров для товара
        $filters = collect();

        if (count($product->filters_values)) {
            $filtersValues = FiltersValue::whereIn('id', $product->filters_values)
                ->orderBy('priority', 'ASC')
                ->orderBy('id', 'DESC')
                ->get()
                ->groupBy('filter_id');

            $filters = Filter::whereIn('id', $filtersValues->keys())
                ->orderBy('priority', 'ASC')
                ->orderBy('id', 'DESC')
                ->get()
                ->map(function ($filter) use ($filtersValues) {
                    $filter->product_values = $filtersValues->get($filter->id);
                    return $filter;
                });
        }

        // Получение спика похожих товаров
//        $similarProducts = null;

//        if (count(array_intersect($product->filters_values, [256, 257, 258]))) {
//            // Получение списка товаров из текущей категории
//            $similarProducts = Product::active()->where('id', '!=', $product->id)->where('menu', $product->menu)->get();
//
//            // Получение позиций того же типа, что и текущий товар
//            $productType = array_values(array_intersect($product->filters_values, [256, 257, 258]))[0];
//            $similarProducts = $similarProducts->filter(fn($item) => array_search($productType, $item->filters_values));
//
//            // Если у товара задан фильтр по материалу - подбираем товары с такими же материалами
//            $materialFilter = $filters->firstWhere('id', 80);
//            if ($materialFilter) {
//                $productMaterials = $materialFilter->product_values->pluck('id');
//                $similarProducts = $similarProducts->filter(function ($item) use ($productMaterials) {
//                    return $productMaterials->intersect($item->filters_values)->count();
//                });
//            }
//
//            // Выносим в приоритет товары заданных брендов и получаем ограниченное количество первых позиций
//            $similarProducts = $similarProducts->sortBy(function ($item) {
//                return array_search($item->brand, [296, 297]) === false;
//            })->take(10);
//
//            $similarProducts->load('series');
//        }

        // Получение баннера для отображения на странице товара
//        $banner = null;
        $banner = $this->getBanners($product);

//        if ($product->categoryData) {
//            $banner = $product->categoryData->productsPageBanners();
//
//            if ($banner->count() === 0 && $product->categoryData->parentCategory) {
//                $banner = $product->categoryData->parentCategory->productsPageBanners();
//            }
//        }

//        if ($banner === null || $banner->count() === 0) {
//          $menu = [];
//          foreach ($product->additionalCategories as $category) {
//            array_push($menu, $category->id);
//            array_push($menu, $category->parent_id);
//          }
//
//          $menu = array_unique($menu);
//
//          $banner = DB::table('banners')
//            ->join('banner_category', 'banner_category.banner_id', 'banners.id')
//            ->where('banners.type', Banner::TYPE_PRODUCT_PAGE)
//            ->whereIn('banner_category.category_id', $menu);
//        }

//        if ($banner->count() > 0) {
//          $banner = $banner->toArray();
//        }

//      dd($banner);

        // Сохранение товара в истории просмотров
        $viewedProductId = $product->parent ?: $product->id;
        $viewedProducts = session('viewed_products') ?: [];

        $viewedProductIndex = array_search($viewedProductId, $viewedProducts);
        if ($viewedProductIndex !== false) {
            unset($viewedProducts[$viewedProductIndex]);
        }

        // сохраняем в сессию недавно просмотренные товары
        array_unshift($viewedProducts, $viewedProductId);
        session(['viewed_products' => array_slice($viewedProducts, 0, 10)]);

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];

        if ($product->categoryData) {
            if ($product->categoryData->parentCategory) {
                $breadcrumbs[] = [
                    $product->categoryData->parentCategory->title,
                    custom_route('category', ['phones', $product->categoryData->parentCategory->name])
                ];
            }

            $breadcrumbs[] = [
                $product->categoryData->title,
                custom_route('category', ['phones', $product->categoryData->name
            ])];
        }

        $breadcrumbs[] = [$product->title, ''];

        view()->share('breadcrumbs', $breadcrumbs);

        // Формирование мета-данных
        $variables = ['name' => $product->title];

        if (! $product->meta_title) {
            $product->meta_title = text_template('product.title', $product->id, $variables);
        }

        if (! $product->meta_description) {
            $product->meta_description = text_template('product.description', $product->id, $variables);
        }

        $salePrice = salePrice($product->opt_price, $product->old_opt_price, $product->price_modify_time);

        view()->share('layoutMetaTitle', $product->meta_title);
        view()->share('layoutMetaDescription', $product->meta_description);

        if ($product->isPrint()) {
          $priceRange = new PriceRange();
          view()->share('priceRangeForPrint', $priceRange->priceRangeByType(null, true));
          view()->share('amountRange', $priceRange->range);
        }

        // Создание отображения
        return view('product', compact(['product', 'productImages', 'filters', 'banner', 'salePrice'])
        );
    }

    protected function getBanners($product)
    {
      $menu = [];

      array_push($menu, $product->menu);
      array_push($menu, $product->categoryData->parentCategory->id ?? 0);

      $additionalCategories = $product->additionalCategories ?? [];

      foreach ($additionalCategories as $category) {
        array_push($menu, $category->id);
        array_push($menu, $category->parent_id);
      }
      $menu = array_unique($menu);

      $banner = DB::table('banners')
        ->join('banner_category', 'banner_category.banner_id', 'banners.id')
        ->where('banners.type', Banner::TYPE_PRODUCT_PAGE)
        ->whereIn('banner_category.category_id', $menu);

      $banner = $banner->get();
      // исключить из коллекции
      $newCollect = $banner->reject(function($item) use ($menu) {
        if ($item->category_second_id > 0) {
          if (! in_array($item->category_second_id, $menu) || ! in_array($item->category_id, $menu) ) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      });

      return $newCollect->sortBy('priority')->values();
    }

    public function getColors()
  {
    $id = request('id');

    $colors = Product::where('parent', $id)->where('qty', '>', 0)->get(['id', 'opt_price', 'img']);

    foreach ($colors as $color) {
      $color->price_uah = convertToUAH($color->opt_price);
      $color->img = fullUrlImage($color->img);
    }

    return response()->json([
      'success' => true,
      'colors' => $colors
    ]);
  }
}
