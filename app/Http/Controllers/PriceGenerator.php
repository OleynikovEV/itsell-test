<?php

namespace App\Http\Controllers;

//use App\Http\Models\IPrintPriceGenerator;
use App\Http\Models\PriceGeneratorHelper as Helper;
use App\Http\Models\PriceGeneratorLogs;
use App\Http\Requests\PriceGeneratorRequest;
use App\Http\Models\PriceGenerator as PG;
use \App\Models\PriceGenerator as Model;
use App\Models\SubjectMatter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class PriceGenerator extends Controller
{
  private $prices_type = [
    'price' => ['name' => 'РРЦ', 'currency' => 'uah'],
//    'opt_price' => ['name' => 'Опт', 'currency' => 'usd'],
//    'opt_dealer_usd' => ['name' => 'Дилер', 'currency' => 'usd'],
//    'opt_vip_usd' => ['name' => 'Vip', 'currency' => 'usd'],
//    'opt_minopt_usd' => ['name' => 'Мин опт', 'currency' => 'usd'],
    'drop_uah' => ['name' => 'Дропшип', 'currency' => 'uah'],
  ];

  public function index()
  {
    $childrenCategories = Helper::getChildrenCategories();
    $tree = Helper::createCategoriesTree(Helper::getCategories(), $childrenCategories);
    $series = Helper::getProductsSeriesList();
    $brands = Helper::getBrandsList();
    $iPrint = Helper::createCategoriesTree(Helper::getCategoriesIPrint(), Helper::getChildrenCategoriesIPrint());
    $subjectMatters = SubjectMatter::get()->toArray();
    //$hydrogel = Helper::createCategoriesTree(Helper::getHydrogel()->get(), $childrenCategories);

    $models = $tree['models'];
    $categories = $tree['categories'];
    $action = 'create';
    $currency = 'uah';
    $sale = 'true';
    $finish = 'false';
    $notAvailable = 'false';
    $prices_type = $this->prices_type;
    $price_type = 'price';
    $mainPhoto = false;

    // Формирование хлебных крошек
    $title = __('Прайс генератор');
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
    $breadcrumbs[] = [__('Экспорт товаров'), custom_route('profile_price')];
    $breadcrumbs[] = [$title, ''];

    return view('price-generator', compact('brands', 'models', 'categories', 'series', 'subjectMatters',
      'action', 'sale', 'currency', 'breadcrumbs', 'mainPhoto', 'prices_type', 'price_type', 'finish', 'iPrint', 'notAvailable')
    );
  }

  /**
   * сохранение шаблона прайса
   * @param PriceGeneratorRequest $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function create(PriceGeneratorRequest $request)
  {
    $data = $request->all();

    $tab = $request->get('tab');
    $tabName = '';
    switch($tab)  {
      case 6:
      case 5:
      case 3:
      case 1: $tabName = 'categories'; break;
      case 2: $tabName = 'series'; break;
      case 4: $tabName = 'brands'; break;
      case 7: $tabName = 'subjectMatters'; break;
    }

    if ($request->has($tabName)) {
      $ids = $request->get($tabName);
    } else {
      session()->flash('errors', __('Заполните фильтр товаров'));
      return Redirect::back();
    }

    $user = auth()->user();
    $conditions = [];

    if ($request->has('finish')) {
      $conditions['finish'] = $request->get('finish') == 1;
    } else {
      $conditions['finish'] = false;
    }

    if ($request->has('sale')) {
      $conditions['sale'] = $request->get('sale') == 1;
    } else {
      $conditions['sale'] = false;
    }


    if ($request->has('action') && $request->get('action') == 'create') {
      $price = new Model();
    } elseif ($request->has('action') && $request->get('action') == 'update' && $request->has('id')) {
      $price = Model::find($request->get('id'));
    } else {
      abort(404);
    }

    $price->user_id = $user->id;
    $price->price_name = $data['name'];
    $price->slug = Str::slug($data['name']);
    $price->currency = $this->prices_type[$data['price_type']]['currency'];
    $price->price_type = $data['price_type'];
    $price->file_type = $data['format'];
    $price->main_photo = $data['main_photo'] ?? false;
    $price->notAvailable = $data['notAvailable'] ?? 0;
    $price->category_type = $data['category_type'];
    $price->tab = $data['tab'];
    $price->conditions = $conditions;
    $price->ids = $ids;
    $price->save();

    // редирект в личный кабинет
    return redirect()->route('profile_price');
  }

  /**
   * Удалить шаблон
   * @param int $id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function remove(int $id)
  {
    $price = Model::find($id);

    if ($price == null) {
      abort(404);
    }

    $price->delete();

    // редирект в личный кабинет
    return redirect()->route('profile_price');
  }

  /**
   * @param int $id
   * @return \Illuminate\View\View
   */
  public function update(int $id)
  {
    $price = Model::find($id);

    if ($price == null) {
      abort(404);
    }

    $tree = Helper::createCategoriesTree(Helper::getCategories(), Helper::getChildrenCategories());
    $series = Helper::getProductsSeriesList();
    $brands = Helper::getBrandsList();
    $iPrint = Helper::createCategoriesTree(Helper::getCategoriesIPrint(), Helper::getChildrenCategoriesIPrint());
    $subjectMatters = SubjectMatter::get();

    $models = $tree['models'];
    $categories = $tree['categories'];
    $action = 'update';

    $sale = isset($price->conditions['sale']) && $price->conditions['sale'] ? 'true' : 'false';
    $finish = isset($price->conditions['finish']) && $price->conditions['finish'] ? 'true' : 'false';
    $notAvailable = $price->notAvailable ? 'true' : 'false';
    $mainPhoto = $price->mainPhoto();

    $currency = $price->currency;
    $prices_type = $this->prices_type;
    $price_type = $price->price_type;

    switch ($price->tab) {
      case 6:
      case 3:
      case 1:
        foreach ($categories as &$child) {
          foreach ($child['children'] as &$item) {
            if (in_array($item['item']['id'], $price->ids)) {
              $item['item']['selected'] = true;
              $child['item']['opened'] = true;
            }
          }
        }

        foreach ($models as &$child) {
          foreach ($child['children'] as &$item) {
            if (in_array($item['item']['id'], $price->ids)) {
              $item['item']['selected'] = true;
              $child['item']['opened'] = true;
            }
          }
        }
        break;
      case 2:
        foreach ($series as &$item) {
          if (in_array($item->id, $price->ids)) {
            $item->selected = true;
          }
        }
        break;
      case 4:
        foreach ($brands as &$item) {
          if (in_array($item->id, $price->ids)) {
            $item->selected = true;
          }
        }
        break;
      case 5:
        foreach ($iPrint['models'] as &$child) {
          foreach ($child['children'] as &$item) {
            if (in_array($item['item']['id'], $price->ids)) {
              $item['item']['selected'] = true;
              $child['item']['opened'] = true;
            }
          }
        }
        break;
      case 7:
        foreach ($subjectMatters as &$item) {
          if (in_array($item->id, $price->ids)) {
            $item->selected = true;
          }
        }
        break;
    }

    // Формирование хлебных крошек
    $title = __('Прайс генератор');
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
    $breadcrumbs[] = [__('Экспорт товаров'), custom_route('profile_price')];
    $breadcrumbs[] = [$title, ''];

    return view('price-generator', compact('brands', 'models', 'categories', 'series',
      'action', 'currency', 'price', 'sale', 'breadcrumbs', 'prices_type', 'price_type', 'finish', 'iPrint',
      'notAvailable', 'mainPhoto', 'subjectMatters')
    );
  }

  /**
   * Прайс генератор
   * @param $slug
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   */
  public function priceGenerator($slug, $format = null)
  {
    $pgLog = new PriceGeneratorLogs($slug, false);
    $pgLog->start();

    $user = auth()->user();

    $price = Model::where('user_id', $user->id)
      ->where('slug', $slug)
      ->first();

    if ($price === null) {
      abort(404);
    }

    $file_type = ($format === null) ? $price->file_type : $format;
    $pgLog->setPriceName($slug . ' ' . $file_type);

    $fileName = PG::generatePrice(
      $file_type,
      $price->category_type,
      $price->tab,
      $price->ids,
      $price->currency,
      $price->price_type,
      $price->conditions,
      $price->notAvailable,
      $price->mainPhoto()
    );

    $pgLog->finish()->save();

    return response()->download($fileName)->deleteFileAfterSend();
  }

  /*
   * скачать прайс по ссылке
   */
  public function downloadPrice($slug, $userId)
  {
    $pgLog = new PriceGeneratorLogs($slug, true);
    $pgLog->start();

    $price = Model::where('user_id', $userId)
      ->where('slug', $slug)
      ->first();

    if ($price === null) {
      abort(404);
    }

    if (!$price->canDownload()) {
      abort(403);
    }

    $price->download = Carbon::now()->format('Y-m-d H:i:s');
    $price->save();

    $file_type = $price->file_type;

    $pgLog->setPriceName($slug . ' ' . $file_type)
    ->setUserId($userId);

    $fileName = PG::generatePrice(
      $file_type,
      $price->category_type,
      $price->tab,
      $price->ids,
      $price->currency,
      $price->price_type,
      $price->conditions,
      $price->notAvailable,
      $price->mainPhoto(),
    );

    $pgLog->finish()->save();

    return response()->download($fileName)->deleteFileAfterSend();
  }

  /**
   * Предварительный просмотр списка товаров
   * @param Request $request
   * @return
   */
  public function productsList(Request $request)
  {
    $products = collect();
    $conditions = [];

    if ($request->has('sale')) {
      $conditions['sale'] = $request->get('sale') === 'true';
    }
    if ($request->has('finish')) {
      $conditions['finish'] = $request->get('finish') === 'true';
    }

    $notAvailable = $request->has('notAvailable') && $request->get('notAvailable') == 'true';

    switch ($request->get('tab')) {
      case 1: // отбираем по категориям
      case 3: // отбираем по брендам
        $products = Helper::getProductsByMenu($request->get('ids'), false, $conditions, $notAvailable, $request->get('page'));
        break;
      case 2: // отбираем по сериям
        $products = Helper::getProductsBySeries($request->get('ids'), false, $conditions, $notAvailable, $request->get('page'));
        break;
      case 4: // отбираем по бренду
        $products = Helper::getProductsByBrands($request->get('ids'), false, $conditions, $notAvailable, $request->get('page'));
        break;
      case 5:
        $products = Helper::getProductsByiPrint($request->get('ids'), false, $conditions, $notAvailable, $request->get('page'));
        break;
      case 6: // гидрогелевые пленки
        $products = Helper::getProductsHydrogel($request->get('ids'), false, $conditions, $notAvailable, $request->get('page'));
        break;
      case 7: // материалы принтов
        $products = Helper::getPrintSubjectMatters($request->get('ids'), false, $conditions, $notAvailable, $request->get('page'));
        break;
    }

//    return new ProductResourceCollection($products);
    return response()->json($products);
  }
}
