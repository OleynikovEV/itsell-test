<?php

namespace App\Http\Controllers;

//use App\Http\Models\Availability;
use App\Http\Models\Availability;
use App\Models\Brand;
use App\Models\ManagersInformations;
use App\Models\OtherType;
use App\Models\User;
use App\Models\Order;
use SimpleXMLElement;
use App\Models\Filter;
use App\Models\Product;
use App\Models\Waybill;
use App\Models\Category;
use Illuminate\Support\Str;
use App\Models\ProductsColor;
use App\Models\ProductsSeries;
use Illuminate\Support\Facades\DB;
use App\Models\ProductsDescription;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Routing\Controller as BaseController;

class OnecExchangeController extends BaseController
{
    // Подпапка для хранения файлов обмена
    protected $exchangeDir = '1c/';

    protected $categoriesList;
    protected $categoriesCodes;
    protected $colorsTranslations;
    protected $brands;
    protected $subTypes;
    protected $categories;

    // Параметры для отладки
    protected $debugMode = false;
    protected $debugStartTime = 0;
    /**
     * @var array
     */
    private $filtersValues;

    // Коды цен
    private $pricesType = [
      '9156ce86-3d6f-11e1-a775-14dae9f72179' => 'opt_price',
      '084bf293-6fdc-11e9-8d62-002590cad0bf' => 'price',
      '084bf291-6fdc-11e9-8d62-002590cad0bf' => 'opt_vip_usd',
      '084bf292-6fdc-11e9-8d62-002590cad0bf' => 'opt_dealer_usd',
      'a0bb4c5d-ad8b-11e2-bf7f-14dae9f72179' => 'opt_minopt_usd',
      '9156ce87-3d6f-11e1-a775-14dae9f72179' => 'drop_uah',
    ];

  /**
     * Обработка запроса на обмен
     */
    public function index()
    {
        // Общие методы для инициализации обмена
        if (request('debug')) {
            $this->debugMode = true;
            $this->debugStartTime = microtime(true);
        }

        if (
            request('mode') != 'checkauth'
            && ! session('1c_authorized')
            && ! $this->debugMode
            // Метод photos.state не делает авториазцию
            && (request('type') != 'photos' || request('mode') != 'state')
        ) {
            return abort(403);
        }

        switch (request('mode')) {
            case 'checkauth':
                return $this->checkAuth();

            case 'init':
                return $this->initFileUpload();
        }

        // Синхронизация товаров и фотографий
        if (request('type') == 'catalog') {
            switch (request('mode')) {
                case 'file':
                    return $this->uploadFile();

                case 'import':
                    return $this->importFile();

                case 'remove':
                    return $this->deleteImage();
            }
        }

        if (request('type') == 'photos' && request('mode') == 'state') {
            return $this->imagesStatus();
        }

        // Синхронизация заказов
        if (request('type') == 'sale') {
            switch (request('mode')) {
                case 'file':
                    return $this->importOrders();

                case 'query':
                    return $this->exportOrders();

                case 'success':
                    return $this->confirmOrdersExport();
            }
        }

        // Синхронизация ТТН
        if (request('type') == 'waybills' && request('mode') == 'file') {
            return $this->importWaybills();
        }

        // Синхронизация пользователей
        if (request('type') == 'clients' && request('mode') == 'file') {
            return $this->importClients();
        }
    }

    /**
     * Проверка авторизации
     */
    protected function checkAuth()
    {
        if (
            true || // TODO: аутентификатция временно отключена
            isset($_SERVER['PHP_AUTH_USER'])
            && $_SERVER['PHP_AUTH_USER'] == config('services.onec.1c_username')
            && $_SERVER['PHP_AUTH_PW'] == config('services.onec.1c_password')
        ) {
            session(['1c_authorized' => true]);

            echo "success\n";
            echo session()->getName() . "\n";
            echo session()->getId() . "\n";
        }
    }

    /**
     * Подготовка к получению файлов
     */
    protected function initFileUpload()
    {
        // Флаг сжатия файлов обмена
        echo "zip=no\n";

        // Максимальный размер одного запроса (файлы большего размера будут разбиты на части)
        echo "file_limit=20000000\n";
    }

    /**
     * Загрузка файла на сервер
     */
    protected function uploadFile()
    {
        $filename = request('filename');

        // Сохранение файла папке для обмена
        $file = fopen(storage_path('app/' . $this->exchangeDir . basename($filename)), 'ab');
        fwrite($file, file_get_contents('php://input'));
        fclose($file);

        // Если было загружено изображение - сразу же обрабатываем его
        if (mb_strpos($filename, 'import_files/') !== false) {
            $this->importImage(basename($filename));
        }

        // Возврат успешного статуса
        echo "success\n";
    }

    /**
     * Импорт данных из заданного файла
     */
    protected function importFile()
    {
        $filename = basename(request('filename'));
        switch ($filename) {
            case 'import.xml':
                $this->importProducts($filename);
                break;

            case 'offers.xml':
            case 'prices.xml':
                $this->importOffers($filename);
                break;
        }

        echo "success\n";
    }

    /**
     * Импорт общей информации о товарах
     */
    protected function importProducts($filename = '')
    {
        $createdProducts = collect();
        $assignSeriesProducts = collect();

        // Загрузка файла обмена
        if (! Storage::exists($this->exchangeDir . $filename)) {
            return;
        }

        try {
            $xml = new SimpleXMLElement(Storage::get($this->exchangeDir . $filename));
        } catch (\Exception $e) {
            Storage::delete($this->exchangeDir . $filename);
            return;
        }

        // Обработка списка категорий
        $this->importProductsCategories($xml->Классификатор->Группы->Группа);

        // Получение списка названий цветов и их hex-кодов
        $colorsNames = ProductsColor::where('parent', 0)
          ->orderBy('priority', 'ASC')
          ->get(['id', 'name', 'color', 'priority', 'parent', 'parent_id'])
          ->mapWithKeys(fn($item) => [mb_strtolower($item->name) => $item->color]);

        // Получение списка фильтров и их значений
        $filters = Filter::with('values')
            ->get()
            ->keyBy('code')
            ->map(function ($filter) {
                $filter->values = $filter->values->keyBy('name');
                return $filter;
            });

        // получение списка брендов
        $this->brands = Brand::get(['id', 'title'])->pluck('title', 'id')->toArray();
        // подтипы товаров из категории Общие аксесуары
        $this->subTypes = OtherType::get(['id', 'name'])->pluck('name', 'id')->toArray();
        // получаем список категорий
        $this->categories = $this->getCategories();
        $parentCategories = Category::where('parent_id', '>', 0)->get(['id', 'parent_id'])->pluck('parent_id', 'id')->toArray();

        // получаем значения фильтров
        $this->filtersValues = $this->getFiltersValues();

        // Импорт новых фильтров
        if ($xml->Классификатор->Свойства) {
            foreach ($xml->Классификатор->Свойства->СвойствоНоменклатуры as $feedFitler) {
                // Если фильтр с текущим кодом не найден - создаем его
                if ( ! $filters->has((string) $feedFitler->Ид)) {
                    $filter = new Filter([
                        'code' => (string) $feedFitler->Ид,
                        'name' => (string) $feedFitler->Наименование,
                        'url' => Str::slug((string) $feedFitler->Наименование),
                    ]);

                    // Проверка URL на уникальность
                    while (Filter::where('url', $filter->url)->exists()) {
                        $filter->url = increment_string($filter->url);
                    }

                    // Сохранение фильтра
                    $filter->save();
                    $filter->values = collect();

                    $filters->put($filter->code, $filter);
                }
            }
        }

        // Получение списка товаров из файла обмена
        $products = (array) $xml->Каталог->Товары;
        $products = collect($products['Товар']);

        $productTypeArray = [
          1767 => 1,
          1781 => 3,
        ];

        // Обработка списка товаров
        $chunks = 0;
        $addit_menus_insert = [];
        foreach ($products->chunk(200) as $productsChunk) {
            $updateProducts = collect();

            // Получение информации о существующих товарах из БД
            $productsCodes = collect();
            foreach ($productsChunk as $product) {
                if (strlen($product->Ид) == 73) {
                    $codes = explode('#', $product->Ид);
                    $productsCodes->push($codes[1]);

                    if ($productsCodes->search($codes[0]) === false) {
                        $productsCodes->push($codes[0]);
                    }
                } elseif ((string) $product->Ид) {
                    $productsCodes->push((string) $product->Ид);
                }
            }

            $productsData = Product::whereIn('code', $productsCodes)->get(['id', 'code'])->keyBy('code');
            $this->debugMessage('Chunk #' . ($chunks + 1) . ' products obtained...');

            // Обработка данных из файла
            foreach ($productsChunk as $product) {
                // Получение кода товара и его родителя
                if (strlen($product->Ид) == 73) {
                    $codes = explode('#', $product->Ид);
                    $parentCode = $codes[0];
                    $productCode = $codes[1];
                } else {
                    $parentCode = '';
                    $productCode = (string) $product->Ид;
                }

                if ( ! $productCode) {
                    continue;
                }

                // Если задан код родительского товара - получаем его информацию
                $parentProduct = $parentCode ? $productsData->get($parentCode) : null;
                if ($parentCode && ! $parentProduct) {
                    continue;
                }

                // Формирование названия цвета товара
                $colorName = $parentProduct ? $product->ХарактеристикиТовара->ХарактеристикаТовара->Значение : '';

                // Получение значений фильтров товара
                $productFiltersValues = collect();

                if ($product->ЗначенияСвойств) {
                    $productFiltersValues->push(397); // автоматом проставляем товару признак - Техничка
                    foreach ($product->ЗначенияСвойств->ЗначенияСвойства as $value) {
                        if ($filter = $filters->get((string) $value->Ид)) {
                            $valueName = (string) $value->Значение;

                            // Если значение фильтра не найдено - создаем его
                            if (! $filter->values->has($valueName)) {
                                $filterValue = $filter->values()->create([
                                    'name' => $valueName
                                ]);
                                $filter->values->put($valueName, $filterValue);
                            }

                            // Если значение найдено - добавляем его в список значений товара
                            if ($filter->values->has($valueName)) {
                                $productFiltersValues->push($filter->values->get($valueName)->id);
                            }
                        }
                    }
                }

                // Получение основных параметров товара
                $title = (string) $product->Наименование;
                $from = Str::containsAll($title, ['Защитная гидрогелевая пленка SKLO', 'для']) === false ? 0 : 2;
                $data = [
                    'baseid' => $parentProduct ? '' : (string) $product->Код,
                    'title' => $title,
                    'colorCode' => (string) $product->КодЦвета,
                    'menu' => $this->getCategoryIdByCode((string) $product->Группы->Ид),
                    'type' => $parentProduct ? 1 : 2,
                    'parent' => $parentProduct ? $parentProduct->id : 0,
                    'color_name' => $parentProduct ? $colorName : '',
                    'temp_photos' => isset($product->ПримерноеФото) && $product->ПримерноеФото == 'Да' ? 1 : 0,
                    'last_modified' => time(),
                    'markdown' => Str::contains($title, 'Уценка') === false ? 0 : 1,
                    'from' => $from,
                    'tech_pack' => ($from === 2 && Str::contains($title, 'тех.пак')) ? 1 : 0,
                ];

                // Если товар не найден - создаем его, иначе - обновляем данные
                if (! $productsData->has($productCode)) {
                    $productType = $this->getProductType((string) $product->Наименование); // получаем тип продукта

                    $parentId = $parentCategories[$data['menu']] ?? 0;
                    $parentArray = $this->categories[$data['menu']] ?? $this->categories[$parentId] ?? $this->categories[$productType] ?? []; // все категории родителя
                    $product_type = $productTypeArray[$productType] ?? 4;

                    $filtersArray = $productFiltersValues->toArray();
                    $data = array_merge($data, [
                        'code' => $productCode,
                        'url' => Str::slug($product->Наименование . ($parentProduct ? '-' . $colorName : '')),
                        'active' => false,
                        //'full_description' => isset($product->Описание) ? (string) $product->Описание : '',
                        'filters_values' => $filtersArray,
                        'added_time' => time(),
                        'brand' => $this->getBrand((string)$product->НоменклатурнаяГруппа),
                        'product_type' => $product_type,
                        'product_subtype' => ($product_type ===4 ) ? $this->getProductSubType((string)$product->Наименование) : 0,
                        'addit_menus' => $this->getAdditMenus($product->Наименование, $parentArray, $filtersArray),
                    ]);

                    // Проверка URL'а на уникальность
                    while (Product::where('url', $data['url'])->exists()) {
                        $data['url'] = increment_string($data['url']);
                    }

                    // Если для товара задано название цвета - определяем его иконку по названию
                    if ($data['color_name']) {
                        foreach ($colorsNames as $key => $value) {
                            if (strpos(mb_strtolower($data['color_name']), $key) !== false) {
                                $data['color'] = $value;
                                break;
                            }
                        }
                    }

                    // Сохранение товара
                    $productData = Product::create($data);
                    $productsData->put($productCode, $productData);

                    // Добавляем в дополнительные разделы
                    if ($data['addit_menus'] != null && $data['parent'] == 0) {
                      $addMenus = explode(',', $data['addit_menus']);
                      foreach ($addMenus as $menu) {
                        $addit_menus_insert[] = [
                          'product_id' => $productData->id,
                          'category_id' => $menu,
                        ];
                      }
                    }
                    //-- Добавляем в дополнительные разделы

                    $createdProducts->push($productData);

                    if (! $parentProduct) {
                        $assignSeriesProducts->push($productData);
                    }
                }
                else {
                    // Обновление информации о товаре
                    $productData = $productsData->get($productCode);
                    $productData->fill($data);

//                    if (isset($product->Описание)) { // отключил синхронизацию из 1С
//                        $productData->full_description = (string) $product->Описание;
//                    }

                    // Добавление списка новых значений фильтров
                    if ($productFiltersValues->count()) {

                        // Данный код добавляет к существующим фильтрам новые
//                        $existingValues = $productData->filters_values;
//
//                        foreach ($productFiltersValues as $filtersValueId) {
//                            if (array_search($filtersValueId, $existingValues) === false) {
//                                $existingValues[] = $filtersValueId;
//                            }
//                        }

//                        $productData->filters_values = $existingValues;
                        // полностью переписываем существующие фильтры из 1С
                        $productData->filters_values = $productFiltersValues->toArray();
                    }

                     // Если у товара изменилось название цвета - определяем его hex-код
                     if ($productData->color_name != $productData->getOriginal('color_name')) {
                        foreach ($colorsNames as $key => $value) {
                            if (strpos(mb_strtolower($productData->color_name), $key) !== false) {
                                $productData->color = $value;
                                break;
                            }
                        }
                    }

                    // Если у товара изменилось название - добавляем его в список позиций для обновления серии
                    if ($productData->title != $productData->getOriginal('title')) {
                        $assignSeriesProducts->push($productData);
                    }

                    $updateProducts->put($productCode, $productData);
                }
            }

            $this->debugMessage('-- Starting update transaction...');

            // Вносим изменения в одной транзакции
            DB::transaction(fn() => $updateProducts->each->save());

            $chunks++;
            $this->debugMessage('Chunk #' . $chunks . ' is processed...' );
        }

        if( count($addit_menus_insert) > 0) DB::table('its_product_category')->insert($addit_menus_insert);

        // Применение шаблонов описаний к только что созданным товарам
        $this->applyDescriptionTemplates($createdProducts);

        // Обновление привязки к сериям для новых или переименованных товаров
        $this->assignSeries($assignSeriesProducts);

        // Сохранение файла обмена как последнего обработанного
        Storage::delete($this->exchangeDir . 'last_' . $filename);
        Storage::move($this->exchangeDir . $filename, $this->exchangeDir . 'last_' . $filename);
    }

    /**
     * Импорт цен и остатков на товары
     */
    protected function importOffers($filename = '')
    {
        // Загрузка файла обмена
        if (! Storage::exists($this->exchangeDir . $filename)) {
            return;
        }

        try {
            $xml = new SimpleXMLElement(Storage::get($this->exchangeDir . $filename));
        } catch (\Exception $e) {
            Storage::delete($this->exchangeDir . $filename);
            return;
        }

        // Коды цен
        $optPriceCode = '9156ce86-3d6f-11e1-a775-14dae9f72179';
        $retailPriceCode = '084bf293-6fdc-11e9-8d62-002590cad0bf';

        $additionalPricesCodes = [
            '084bf291-6fdc-11e9-8d62-002590cad0bf' => 'opt_vip_usd',
            '084bf292-6fdc-11e9-8d62-002590cad0bf' => 'opt_dealer_usd',
            'a0bb4c5d-ad8b-11e2-bf7f-14dae9f72179' => 'opt_minopt_usd',
            '9156ce87-3d6f-11e1-a775-14dae9f72179' => 'drop_uah'
        ];

        // Обработка списка товаров
        $this->debugMessage('!!! Start updating products list');

        $parentIds = collect();
        $updateData = collect();
        $productsIds = [];

        $i = 0;

        $totalItems = count($xml->ПакетПредложений->Предложения->Предложение);
        foreach ($xml->ПакетПредложений->Предложения->Предложение as $offer) {
            $i++;

            // Обрабатываем только дочерние товары, т.к. у родителей приходит нулевое наличие и цены. Родительские
            // товары просчитываются отдельно на основе данных дочерних позиций после их синхронизации
            if (strlen($offer->Ид) != 73) {
                continue;
            }

            $codes = explode('#', $offer->Ид);
            $productCode = $codes[1];

            $date_coming = (! isset($offer->ДатаПоступленияОПТ) || $offer->ДатаПоступленияОПТ == '00.00.0000')
              ? null
              : date('Y-m-d', strtotime($offer->ДатаПоступленияОПТ));

            $data = [
                'qty' => $offer->Количество + $offer->ОстатокСОтсрочкойДоставки,
                'qty_today' => +$offer->Количество,
                'qty_with_delay' => +$offer->ОстатокСОтсрочкойДоставки,
                'liquidity' => +$offer->Ликвидность,
                'when_appear' => '',
                'price_types' => [],
                'date_coming' => $date_coming,
            ];

            // Получение планируемой даты поступления товара
            if ($offer->ДатаПоступления != '31.12.3999' && strtotime($offer->ДатаПоступления) > time() - 7 * 86400) {
                $data['when_appear'] = (string) $offer->ДатаПоступления;
            }

            // Получение цен на товар
            foreach ($offer->Цены->Цена as $price) {
                $priceCode = (string) $price->ИдТипаЦены;

                if ($priceCode == $retailPriceCode) {
                    $data['price'] = +$price->ЦенаЗаЕдиницу;
                }

                if ($priceCode == $optPriceCode) {
                    $data['opt_price'] = +$price->ЦенаЗаЕдиницу;
                }

                if (isset($additionalPricesCodes[$priceCode])) {
                    $data['price_types'][$additionalPricesCodes[$priceCode]] = +$price->ЦенаЗаЕдиницу;
                }
            }

            $updateData->put($productCode, $data);

            // Если набралось заданное количество товаров или все пройдены товары - сохраняем данные
            if ($updateData->count() == 500 || $i == $totalItems) {
                $this->debugMessage('-- Starting update transaction for chunk #' . ceil($i / 500));

                $products = Product::whereIn('code', $updateData->keys())->get();
                DB::transaction(function () use ($products, $updateData, &$productsIds) {
                    foreach ($products as $product) {
                        $data = $updateData->get($product->code);

                        // если текущее количество товара 0 а новое больше 0, получаем id
                        if ($product->qty === 0 && $data['qty'] > 0) {
                          array_push($productsIds, $product->id);
                        }

                        if (isset($data['opt_price'])) {
                            // Если цена на товар снижена - сохраняем время снижения и старую перечеркнутую цену
                            if ($data['opt_price'] < $product->opt_price && $product->price !== null) {
                                $product->price_modify_time = time();
                                $product->old_price = $product->price;
                                $product->old_opt_price = $product->opt_price;
                            } elseif ($data['opt_price'] >= $product->old_opt_price) {
                                // Если цена на товар повышена и стала больше старой перечеркнутой - снимаем старую цену
                                $product->price_modify_time = 0;
                                $product->old_price = 0;
                                $product->old_opt_price = 0;
                            }
                        }

                        // Изменение товара
                        $product->fill($data);

                        // Если товар впервые появился в наличии - сохраняем время поступления
                        if ($product->qty > 0 && ! $product->arrived_in_stock_at) {
                            $product->arrived_in_stock_at = now();
                        }

                        $product->save();
                    }
                });

                // Добавление родительских товара в очередь для пересчета
                $parentIds = $parentIds->merge($products->pluck('parent')->unique());

                // Обновление параметров стека
                $updateData = collect();
            }
        }

        // Формирование списка типов цен для просчета в родительских товарах
        $priceTypes = ['price', 'opt_price'];
        foreach ($additionalPricesCodes as $priceType) {
            $priceTypes[] = 'price_types.' . $priceType;
        }

        // Пересчет родительских товаров
        $this->debugMessage('!!! Start updating parent products list');

        $parentIds = $parentIds->unique()->sort();
        if ($parentIds->first() == 0) {
            $parentIds->shift();
        }

        foreach ($parentIds->chunk(500) as $index => $chunk) {
            $this->debugMessage('-- Starting update transaction for chunk #' . ($index + 1));

            // Получение списка родительских товаров с их дочерними позициями
            $products = Product::whereIn('id', $chunk)->with('groupProductsList')->get();

            // Обновление родительских товаров в одной транзакции
            DB::transaction(function () use ($products, $priceTypes, &$productsIds) {
                foreach ($products as $product) {
                    $groupProducts = $product->groupProductsList;
                    $groupProductsInStock = $groupProducts->where('qty', '>', 0);

                    $maxDateComing = $groupProducts->max('date_coming');
                    $date_coming = ($product->date_coming === '0000-00-00') ? null : $product->date_coming;
                    $data = [
                        'qty' => $groupProducts->sum('qty'),
                        'qty_today' => $groupProducts->sum('qty_today'),
                        'qty_with_delay' => $groupProducts->sum('qty_with_delay'),
                        'when_appear' => $groupProducts->where('qty', '<=', 0)->max('when_appear') ?: '',
                        'price_modify_time' => $groupProducts->max('price_modify_time') ?: 0,
                        'liquidity' => $groupProducts->max('liquidity') ?: 0,
                        'price_types' => $product->price_types,
                        'date_coming' => ($maxDateComing > $product->date_coming) ? $maxDateComing : $date_coming,
                    ];

                    // если текущее количество товара 0 а новое больше 0, получаем id
                    if ($product->qty === 0 && $data['qty'] > 0) {
                      array_push($productsIds, $product->id);
                    }

                    // Получение цен для всех типов
                    foreach ($priceTypes as $priceType) {
                        // Определение минимальной цены текущего типа среди цветов (в приоритете цвета в наличии)
                        $price = $groupProductsInStock->where($priceType, '>', 0)->min($priceType)
                            ?: $groupProducts->where($priceType, '>', 0)->min($priceType);

                        if ($price) {
                            data_set($data, $priceType, $price);
                        }

                        // Если текущий тип цены дополнительный - прочитываем для него и максимальную цену
                        if (Str::startsWith($priceType, 'price_types.')) {
                            $priceMax = $groupProductsInStock->max($priceType) ?: $groupProducts->max($priceType);
                            if ($priceMax) {
                                data_set($data, $priceType . '_max', $price);
                            }
                        }
                    }

                    // Если цена на товар снижена - сохраняем время снижения и старую перечеркнутую цену
                    if (isset($data['opt_price'])) {
                        if ($data['opt_price'] < $product->opt_price) {
                            $product->price_modify_time = time();
                            $product->old_price = $product->price;
                            $product->old_opt_price = $product->opt_price;
                        }elseif ($data['opt_price'] >= $product->old_opt_price) {
                            // Если цена на товар повышена и стала больше старой перечеркнутой - снимаем старую цену
                            $product->price_modify_time = 0;
                            $product->old_price = 0;
                            $product->old_opt_price = 0;
                        }
                    }

                    $product->fill($data);

                    // Если товар впервые появился в наличии - сохраняем время поступления
                    if ($product->qty > 0 && ! $product->arrived_in_stock_at) {
                        $product->arrived_in_stock_at = now();
                    }

                    // Формирование краткой информации об активных дочерних товарах
                    $groupProducts = [];
                    foreach ($product->groupProductsList as $groupProduct) {

                        // получаем min, max цены
                        $range = $groupProduct->getPriceRange();
                        if (count($range) == 1) {
                            $minPrice = $maxPrice = $range[0];
                        } else {
                            list($minPrice, $maxPrice) = $range;
                        }

                        $colorData = [
                            'id' => $groupProduct->id,
                            'color' => $groupProduct->color,
                            'name' => ['ru' => $groupProduct->color_name],
                            'image' => $groupProduct->img,
                            'min_price' => $minPrice,
                            'max_price' => $maxPrice,
                            'price' => $groupProduct->price,
                            'opt_price' => $groupProduct->opt_price,
                            'opt_minopt_usd' => $groupProduct->getPrice('opt_minopt_usd'),
                            'opt_vip_usd' => $groupProduct->getPrice('opt_vip_usd'),
                            'opt_dealer_usd' => $groupProduct->getPrice('opt_dealer_usd'),
                            'old_opt_price' => salePrice($groupProduct->opt_price, $groupProduct->old_opt_price, $groupProduct->price_modify_time),
                            'drop_uah' => $groupProduct->getPrice('drop_uah'),
                            'qty' => $groupProduct->qty,
                            'when_appear' => $groupProduct->qty <= 0 ? $groupProduct->when_appear : '',
                            'date_coming' => ($product->date_coming === '0000-00-00') ? null : $product->date_coming,
                        ];

                        // foreach (config('app.supported_locales') as $locale)
                        // {
                        //     if ($locale != config('app.locale'))
                        //     {
                        //         $translation = $groupProduct->translate($locale);
                        //         if ($translation && $translation->color_name)
                        //         {
                        //             $colorData['name'][$locale] = $translation->color_name;
                        //         }
                        //     }
                        // }

                        $groupProducts[] = $colorData;
                    }

                    $product->group_products = $groupProducts;
                    $product->save();
                }
            });
        }

        // Сохранение файла обмена как последнего обработанного
        Storage::delete($this->exchangeDir . 'last_' . $filename);
        Storage::move($this->exchangeDir . $filename, $this->exchangeDir . 'last_' . $filename);

        // оповещаем пользователей о поступлении товара из их списка ожиданий
        if (count($productsIds) > 0) (new Availability($productsIds))->notify();
    }

    /**
     * Возврат статуса изображений для их обновления
     */
    public function imagesStatus()
    {
        $xml = new SimpleXMLElement(request()->getContent());

        // Обработка списка переданных фотографий
        $photos = collect();
        foreach ($xml->photo as $photo) {
            $photos->push([
                'code' => substr($photo->name, 0, 36),
                'index' => strlen($photo->name) > 40 ? substr($photo->name, 37, 1) : 0,
                'size' => (int) $photo->size
            ]);
        }

        // Сравнение переданных в обмене размеров фотографий с сохраненными в базе сайта
        $results = collect();
        foreach ($photos->chunk(500) as $chunk) {
            $products = Product::whereIn('code', $chunk->pluck('code')->unique())
                ->get()
                ->keyBy('code');

            foreach ($chunk as $item) {
                $product = $products->get($item['code']);
                $imagesSizes = $product ? $product->onec_images_sizes : false;

                // Возвращаем true для изображений, которые не нужно перезагружать, в следующих случаях:
                // - товар не найден
                // - индекс изображения больше 8 (максимальное количество фото на сайте)
                // - изображение уже загружено и его размер на сайте соответствует размеру в запросе
                $results->push(
                    ! $product
                    || $item['index'] >= 8
                    || (
                        $imagesSizes
                        && isset($imagesSizes[$item['index']])
                        && $imagesSizes[$item['index']] == $item['size'])
                );
            }
        }

        // Формирование и возврат ответа
        $response = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><photos/>');
        foreach ($results as $result) {
            $photo = $response->addChild('photo');
            $photo->addChild('answer', $result ? 'true' : 'false');
        }

        return response($response->asXML(), 200, ['Content-type: text/xml']);
    }

    /**
     * Импорт изображения из 1С
     */
    protected function importImage($filename = '')
    {
        $path = storage_path('app/' . $this->exchangeDir . $filename);

        // Определение кода товара и индекса изображения
        $code = substr($filename, 0, 36);
        $imageIndex = strpos($filename, '_') !== false ? substr($filename, 37, 1) : 0;
        $imageSize = filesize($path);

        // Получение информации о товаре
        $product = Product::firstWhere('code', $code);

        if ($product && $imageIndex < 8) {
            // Создание ресурса изображения для проверки корректности файла
            if ($image = rescue(fn() => Image::make($path), false)) {
                $storage = Storage::disk('content');
                $field = Product::IMAGE_FIELDS[$imageIndex];

                // Если у товара задано старое изображение - удаляем его
                if ($product->{$field}) {
                    if ($storage->exists($product->{$field})) {
                        $storage->delete($product->{$field});
                    }

                    if ($storage->exists(thumb_name($product->{$field}))) {
                        $storage->delete(thumb_name($product->{$field}));
                    }

                    $thumb = str_replace('/', '', $product->{$field});
                    $thumb = str_replace('.', '_thumb.', $thumb);

                    if ($storage->exists($thumb)) {
                        $storage->delete($thumb);
                    }
                }

                // Генерация нового названия файла
                $filename = Str::random(32) . '.' . Str::afterLast($filename, '.');

                // Если в товаре отмечен флаг Временные фото - добавляем водяной знак
                if ($product->temp_photos == 1) {
                    $watermark = Image::make(public_path('template/images/temp_photo_watermark.png'));

                    $width = max($image->width() * 0.4, 150);
                    $height = round($width / $watermark->width() * $watermark->height());

                    $watermark->resize($width, $height);

                    $image->insert($watermark, 'bottom', 0, round($image->height() * 0.08));
                }

                // Уменьшение основного изображения
                $image->resize(Product::IMAGE_SIZE, Product::IMAGE_SIZE, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })
                    ->save();

                // Создание уменьшенной версии изображения
                $thumbName = thumb_name($filename);
                $thumbSize = $imageIndex == 0 ? Product::IMAGE_THUMB_MAIN : Product::IMAGE_THUMB_SECONDARY;

                File::copy($path, storage_path('app/' . $this->exchangeDir . $thumbName));

                Image::make(storage_path('app/' . $this->exchangeDir . $thumbName))
                    ->resize($thumbSize, $thumbSize, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })
                    ->save();

                // Загрузка изображений на сервер с контентом
                $storage->put($filename, File::get($path));
                $storage->put($thumbName, File::get(storage_path('app/' . $this->exchangeDir . $thumbName)));

                // Удаление временного файла уменьшенной версии
                File::delete(storage_path('app/' . $this->exchangeDir . $thumbName));

                // Сохранение изображения в данных товара
                $product->{$field} = $filename;

                // Сохранение размера изображения в кеше
                $imagesSizes = $product->onec_images_sizes ?: [];
                if (count($imagesSizes) < 8) {
                    $imagesSizes = array_fill(count($imagesSizes), 8 - count($imagesSizes), 0);
                }

                $imagesSizes[$imageIndex] = $imageSize;
                $product->onec_images_sizes = $imagesSizes;

                $product->save();
            }
        }

        // Удаление временного файла
        File::delete($path);
    }

    /**
     * Удаление изображения для товара из 1С
     */
    protected function deleteImage()
    {
        $filename = request('filename');
        if (mb_strpos($filename, 'import_files/') !== false) {
            $filename = basename($filename);

            // Определение кода товара и индекса изображения
            $code = substr($filename, 0, 36);
            $imageIndex = strpos($filename, '_') !== false ? substr($filename, 37, 1) : 0;

            $product = Product::firstWhere('code', $code);
            if ($product && $imageIndex < 8) {
                $storage = Storage::disk('content');
                $field = Product::IMAGE_FIELDS[$imageIndex];

                // Если у товара задано изображение - удаляем его
                if ($product->{$field}) {
                    if ($storage->exists($product->{$field})) {
                        $storage->delete($product->{$field});
                    }

                    if ($storage->exists(thumb_name($product->{$field}))) {
                        $storage->delete(thumb_name($product->{$field}));
                    }
                }

                // Обновление данных товара
                $product->{$field} = '';

                // Сохранение размера изображения в хеше
                $imagesSizes = $product->onec_images_sizes ?: [];
                if (count($imagesSizes) < 8) {
                    $imagesSizes = array_fill(count($imagesSizes), 8 - count($imagesSizes), 0);
                }

                $imagesSizes[$imageIndex] = 0;
                $product->onec_images_sizes = $imagesSizes;

                $product->save();
            }
        }

        // Возврат успешного статуса
        echo "success\n";
    }

    /**
     * Импорт списка категорий из файла обмена
     */
    protected function importProductsCategories($categories, $parentCategoryId = 0, $level = 1)
    {
        if (! $categories) {
            return;
        }

        // Если кеш существующих категорий еще не загружен - получаем список всех категорий
        if (! $this->categoriesList) {
            $this->categoriesList = Category::all()->keyBy('code');
        }

        foreach ($categories as $category) {
            // Если категория уже существует - обновляем ее данные, иначе - создаем новую
            if ($categoryData = $this->categoriesList->get((string) $category->Ид)) {
                $categoryData->title = (string) $category->Наименование;

                if ($categoryData->sync_parent && $categoryData->parent_id != $parentCategoryId) {
                    $categoryData->parent_id = $parentCategoryId;
                    $categoryData->menu_sector = 0;
                }

                $categoryData->save();
            } else {
                $data = [
                    'code' => (string) $category->Ид,
                    'title' => (string) $category->Наименование,
                    'parent_id' => $parentCategoryId,
                    'name' => Str::slug($category->Наименование),
                ];

                // Проверка URL'а на уникальность
                while (Category::where('name', $data['name'])->exists()) {
                    $data['name'] = increment_string($data['name']);
                }

                $categoryData = Category::create($data);
                $this->categoriesList->put($categoryData->code, $categoryData);
            }

            // Синхронизация списка подкатегорий
            if ($categoryData && $category->Группы) {
                // В категории Общие аксессуары выносим все подкатегории третьего уровня на второй
                $nextParentId = $parentCategoryId == config('custom.accessories_category_id') && $level == 2
                    ? $parentCategoryId
                    : $categoryData->id;

                $this->importProductsCategories($category->Группы->Группа, $nextParentId, $level + 1);
            }
        }
    }

    /**
     * Получение ID категории по ее коду
     */
    protected function getCategoryIdByCode($code)
    {
        // Если коды категорий пока не заданы - получаем их
        if (! $this->categoriesCodes) {
            $this->categoriesCodes = collect();

            // Получение всех категорий
            $categories = Category::select('id', 'parent_id', 'code', 'vertical_menu')->get();

            // Получение списка подкатегорий 2-го уровня для Общих аксессуаров и вертикального меню
            $accessoriesCategories = collect([config('custom.accessories_category_id')])
                ->merge($categories->where('vertical_menu', true)->pluck('id'));

            $accessoriesSubcategories = $categories->whereIn('parent_id', $accessoriesCategories)->pluck('id');

            // Обработка списка категорий
            foreach ($categories as $category) {
                // Если текущая категория является 3-м уровнем Общих аксессуров - выносим ее товары на второй уровень
                // (привязываем их к родительской категории)
                $categoryId = $accessoriesSubcategories->search($category->parent_id) !== false
                    ? $category->parent_id
                    : $category->id;

                $this->categoriesCodes->put($category->code, $categoryId);
            }
        }

        // Возврат ID категории по ее коду
        return $this->categoriesCodes->get($code) ?: 0;
    }

    /**
     * Применение шаблонов описаний к новым товарам
     */
    protected function applyDescriptionTemplates($products)
    {
        if (! $products->count()) {
            return;
        }

        // Получение списка шаблонов описаний
        $descriptions = ProductsDescription::all();

        // Обработка списка товаров и применение к ним шаблонов
        $updatedProducts = collect();
        $addit_menus = [];
        foreach ($products as $product) {
            $product_addit_menus = explode(',', $product->addit_menus);
            foreach ($descriptions as $description) {
                $description_addit_menus = explode(',', $description->addit_menus);
                if (stripos($product->title, $description->keywords) !== false) {
//                    $product->fill($description->only([
//                        'full_description', 'videos', 'filters_values', 'has_package', 'show_price', 'description_for_client', 'addit_menus'
//                    ]));
                    $product->fill([
                      'full_description' => $description->full_description,
                      'videos' => $description->videos,
                      'filters_values' => $description->filters_values,
                      'has_package' => $description->has_package,
                      'show_price' => $description->show_price,
                      'product_type' => $description->product_type,
                      'product_subtype' => $description->product_subtype,
                      'description_for_client' => $description->description_for_client,
                      'addit_menus' => implode(',',
                        array_unique(
                          array_merge($description_addit_menus, $product_addit_menus)
                        )
                      ),
                    ]);

                    if ($description->addit_menus && $product->parent == 0) {
                      $menus = array_filter($description_addit_menus, fn($item) => array_search($item, $product_addit_menus) === false);

                      foreach ($menus as $item) {
                        $addit_menus[] = [
                          'product_id' => $product->id,
                          'category_id' => $item,
                        ];
                      };
                    }

                    $updatedProducts->push($product);
                    break;
                }
            }
        }

        if (count($addit_menus) > 0) {
          DB::table('its_product_category')->insert($addit_menus);
        }

        // Сохранение изменений товаров в одной транзакции
        DB::transaction(fn() => $updatedProducts->each->save());
    }

    /**
     * Привязка товаров к сериям по их названию
     */
    protected function assignSeries($products)
    {
        if (! $products->count()) {
            return;
        }

        // Получение списка серий товаров
        $seriesList = ProductsSeries::all()->keyBy('name');

        // Обработка списка товаров
        foreach ($products as $product) {
            $product->products_series_id = 0;

            if (! Str::contains($product->title, 'для')) {
                continue;
            }

            // Удаляем слово Уценка из названия
            $seriesName = Str::before($product->title, 'для');
            $seriesName = trim(Str::replaceFirst('Уценка', '', $seriesName));
            if ($seriesList->has($seriesName)) {
                $product->products_series_id = $seriesList->get($seriesName)->id;
                continue;
            }

            // Если серия пока не создана, но в базе есть другие товары с таким же названием серии - создаем ее
//            $seriesProducts = Product::where('id', '!=', $product->id)
//                ->where('parent', 0)
//                ->where('title', 'like %', $seriesName . ' для%')
//                ->get();
//
//            if ($seriesProducts->count()) {
                $series = new ProductsSeries([
                    'name' => $seriesName,
                    'url' => Str::slug($seriesName)
                ]);

                // Проверка URL'а на уникальность
                while ($seriesList->where('url', $series->url)->count()) {
                    $series->url = increment_string($series->url);
                }

                $series->save();
                $seriesList->put($series->name, $series);

                // Привязка товаров к серии
                $product->products_series_id = $series->id;

                $seriesProducts = Product::where('id', '!=', $product->id)
                  ->where('parent', 0)
                  ->where('products_series_id', 0)
                  ->where('title', 'like %', $seriesName . ' для%')
                  ->get();

                if ($seriesProducts->count()) {
                    foreach ($seriesProducts as $seriesProduct) {
                        $seriesProduct->products_series_id = $series->id;
                    }

                    DB::transaction(fn() => $seriesProducts->each->save());
                }
//            }
        }

        // Сохранение изменений в одной транзакции
        DB::transaction(fn() => $products->each->save());
    }

    /**
     * Импорт списка заказов
     */
    protected function importOrders()
    {
        $xml = new SimpleXMLElement(request()->getContent());

        // Получение списка локальных заказов и товаров
        $ordersIds = [];
        $productsCodes = [];

        foreach ($xml->Документ as $item) {
            $ordersIds[] = Str::before($item->Номер, '_');

            foreach ($item->Товары->Товар as $itemProduct) {
                $productCode = Str::contains($itemProduct->Ид, '#')
                    ? Str::after($itemProduct->Ид, '#')
                    : (string) $itemProduct->Ид;

                if (array_search($productCode, $productsCodes) === false) {
                    $productsCodes[] = $productCode;
                }
            }
        }

        $orders = Order::whereIn('id', $ordersIds)->with('products')->get()->keyBy('id');
        $products = Product::whereIn('code', $productsCodes)->get()->keyBy('code');

        // Обработка запроса
        foreach ($xml->Документ as $item) {
            $orderId = Str::before($item->Номер, '_');
            $orderType = Str::after($item->Номер, '_') ?: 1;

            if (! $order = $orders->get($orderId)) {
                continue;
            }

            // Обновление суммы заказа
            $order->total = +$item->Сумма;

            // Проверка пометки на удаление заказа
            foreach ($item->ЗначенияРеквизитов->ЗначениеРеквизита as $property) {
                if ($property->Наименование == 'ПометкаУдаления' && $property->Значение == 'true') {
                    $order->state = 6;
                }
            }

            // Обновление списка товаров в заказе
            $orderProducts = $order->products->keyBy('product_id');

            foreach ($item->Товары->Товар as $itemProduct) {
                $productCode = Str::contains($itemProduct->Ид, '#')
                    ? Str::after($itemProduct->Ид, '#')
                    : (string) $itemProduct->Ид;

                if (! $product = $products->get($productCode)) {
                    continue;
                }

                // Если текущий товар уже есть в заказе - обновляем его данные, иначе - добавляем его к заказу
                if ($orderProduct = $orderProducts->has($product->id)) {
                    $orderProduct->price = +$itemProduct->ЦенаЗаЕдиницу;

                    // Обновление количества товара
                    if ($orderType == 1) {
                        $orderProduct->count = $itemProduct->Количество + $orderProduct->qty_with_delay;
                    } else {
                        $orderProduct->count += $itemProduct->Количество - $orderProduct->qty_with_delay;
                        $orderProduct->qty_with_delay = +$itemProduct->Количество;
                    }
                } else {
                    $order->products()->create([
                        'product_id' => $product->id,
                        'count' => $itemProduct->Количество,
                        'qty_with_delay' => $orderType == 2 ? $itemProduct->Количество : 0,
                        'qty_shortage' => 0,
                        'discount' => 0,
                        'price' => +$itemProduct->ЦенаЗаЕдиницу,
                        'comment' => ''
                    ]);
                }
            }

            DB::transaction(fn() => $orderProducts->each->save());

            $order->save();
        }

        // Возврат успешного статуса
        echo "success\n";
    }

    /**
     * Экспорт списка заказов
     */
    protected function exportOrders()
    {
        // Формирование XML-документа
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><КоммерческаяИнформация/>');
        $xml->addAttribute('ВерсияСхемы', '2.05');
        $xml->addAttribute('ДатаФормирования', date('c'));
        $xml->addAttribute('ФорматДаты', 'ДФ=yyyy-MM-dd; ДЛФ=DT');
        $xml->addAttribute('ФорматВремени', 'ДФ=ЧЧ:мм:сс; ДЛФ=T');
        $xml->addAttribute('РазделительДатаВремя', 'T');
        $xml->addAttribute('ФорматСуммы', 'ЧЦ=18; ЧДЦ=2; ЧРД=.');
        $xml->addAttribute('ФорматКоличества', 'ЧЦ=18; ЧДЦ=2; ЧРД=.');

        // Получение списка заказов для экспорта
        $exportedOrders = [];

        $orders = Order::where('exported', false)
            ->oldest('id')
            ->with('user', 'products.product.parentProduct', 'cityData', 'warehouse', 'streetData')
            ->get();

        // получаем список родительских товаров для принтов
        $iPrintParentProducts = DB::table('its_products')
          ->where('title', 'like', '%OPrint%')
          ->where('parent', 0)
          ->get(['code', 'menu'])
          ->keyBy('menu')
          ->toArray();

        foreach ($orders as $order) {
            if (! $order->user) {
                continue;
            }

            // Разделение заказа на 2 части: с товарами доступными сегодня и с отсрочкой отправки
            $orderProducts = [
                1 => $order->products->filter(fn($item) => $item->count != $item->qty_with_delay),
                2 => $order->products->where('qty_with_delay', '>', 0)
            ];

            // В блоке с отсрочкой доставки дублируем товары по 1 единице столько раз, сколько единиц заказал
            // покупатель (для корректного резерва товаров в 1С)
            if ($orderProducts[2]->count()) {
                $items = collect();

                foreach ($orderProducts[2] as $orderProduct) {
                    for ($i = 0; $i < $orderProduct->qty_with_delay; $i++) {
                        $item = clone $orderProduct;
                        $item->qty_with_delay = 1;
                        $items->push($item);
                    }
                }

                $orderProducts[2] = $items;
            }

            foreach ($orderProducts as $orderType => $products) {
                if (! $products->count()) {
                    continue;
                }

                $productsIds = [];
                $printsIds = [];
                foreach ($products as $product) {
                  if (strlen($product->product_id) > 10) {
                    $printsIds[] = $product->product_id;
                  } else {
                    $productsIds[] = $product->product_id;
                  }
                }

                // список продуктов и их id menu
                $productsMenu = [];
                if (count($productsIds) > 0) {
                  $productsMenu = DB::table('its_products')
                    ->whereIn('id', $productsIds)
                    ->get(['id', 'menu'])
                    ->keyBy('id')
                    ->toArray();
                }

                if (count($printsIds) > 0) {
                  $productsMenu = array_merge($productsMenu, DB::table('its_print_cover')
                    ->leftJoin('its_vchehle', 'its_vchehle.vchehle_id', '=', 'its_print_cover.category_id')
                    ->whereIn('its_print_cover.uuid', $printsIds)
                    ->get(['its_print_cover.uuid as id', 'its_vchehle.menu_id as menu'])
                    ->keyBy('id')
                    ->toArray());
                }
                
               // Просчет стоимости товаров в текущей части
                $productsTotalPrice = $products->sum(function ($item) use ($orderType) {
                    $qty = $orderType == 1 ? $item->count - $item->qty_with_delay : $item->qty_with_delay;
//                    return $qty * $item->getPurchasePrice();
                    return $qty * $item->getPrice();
                });

                // Общие данные заказа
                $item = $xml->addChild('Документ');
                $item->addChild('Ид', $order->id . '_' . $orderType);
                $item->addChild('Номер', $order->id . '_' . $orderType);
                $item->addChild('Доставка', $orderType == 1 ? 'Сегодня' : 'Завтра');
                $item->addChild('Дата', date('Y-m-d', $order->time));
                $item->addChild('Время', date('H:i:s', $order->time));
                $item->addChild('Валюта', '840');
                $item->addChild('Сумма', $productsTotalPrice);
                $item->addChild('КлиентПредзаказ', $order->user->permissions == 6 ? 'true' : 'false');
                $item->addChild('Проведен', $order->state != 6 ? 'true' : 'false');
                $item->addChild('ПометкаУдаления', $order->state == 6 ? 'true' : 'false');
                $item->addChild('Дропшиппинг', $order->dropshipping ? 'да' : 'нет');

                // Контрагент
                $counterparties = $item->addChild('Контрагенты');
                $counterparty = $counterparties->addChild('Контрагент');
                $counterparty->addChild('Ид', $order->user->code);

                // Параметры доставки
                $deliveryTypeName = [
                    'Отделение Новой Почты',
                    'Курьер Новой Почты',
                    'Другое'
                ][$order->delivery_type_id - 1];

                $delivery = $item->addChild('ПараметрыДоставки');
                $delivery->addChild('Фамилия', $order->lastname);
                $delivery->addChild('Имя', $order->firstname);
                $delivery->addChild('Телефон', $order->phone);
                $delivery->addChild('Доставка', $deliveryTypeName);
                $delivery->addChild('Город', optional($order->cityData)->np_code);

                switch ($order->delivery_type_id) {
                    // Отделение НП
                    case 1:
                        $delivery->addChild('Отделение', optional($order->warehouse)->np_code);
                        break;

                    // Курьер НП
                    case 2:
                        $delivery->addChild('Улица', optional($order->streetData)->np_code);
                        $delivery->addChild('Дом', $order->street_building);
                        break;

                    // Другое
                    case 3:
                        $deliveryComment = $order->delivery_comment . "\n\n" . $order->address;
                        $delivery->addChild('КомментарийКДоставке', $deliveryComment);
                        break;
                }

                // Параметры для Дропшиппинг заказов
                if ($order->dropshipping) {
                    $delivery->addChild('Оплата', ['Наложенный платеж', 'Предоплата'][$order->payment_type_id - 1]);
                    $delivery->addChild('СуммаКОплате', +$order->dropshipping_price);
                    $delivery->addChild('СтоимостьДоставки', +$order->delivery_price);
                    $delivery->addChild('ОплатаУпаковки', $order->package_payer == 2 ? 'Покупатель' : 'Контрагент');
                }

                // Список товаров
                $itemProducts = $item->addChild('Товары');


                foreach ($products as $product) {
                    if (! $product->product && strlen($product->product_id) < 10 ) {
                        continue;
                    }

                    if ($product->print === 1) {
                      $menu = $productsMenu[$product->product_id]->menu ?? 0; // получаем ИД модели
                      $productCode = $iPrintParentProducts[$menu]->code ?? '845ea561-413a-11eb-a794-002590cad0bf'; // привязываем к созданному товару в 1С
                    } else {
                      $productCode = implode('#', array_filter([
                        optional($product->product->parentProduct)->code,
                        $product->product->code
                      ]));
                    }

                    $productQty = $orderType == 1
                        ? $product->count - $product->qty_with_delay
                        : $product->qty_with_delay;

                    $itemProduct = $itemProducts->addChild('Товар');
                    $itemProduct->addChild('Ид', $productCode);
//                    $itemProduct->addChild('ЦенаЗаЕдиницу', $product->getPurchasePrice());
                    $itemProduct->addChild('ЦенаЗаЕдиницу', $product->getPrice());
                    $itemProduct->addChild('Количество', $productQty);
//                    $itemProduct->addChild('Сумма', $productQty * $product->getPurchasePrice());
                    $itemProduct->addChild('Сумма', $productQty * $product->getPrice());
                    $itemProduct->addChild('Скидка', $product->discount);

                    $discounts = $itemProduct->addChild('Скидки');
                    $discount = $discounts->addChild('Скидка');
                    if ($product->promo_code != null) {
                        $discount->addChild('Сумма', round($product->getSalePrice() * $productQty, 2));
                        $discount->addChild('Промокод', $product->promo_code);
                        $discount->addChild('УчтеноВСумме', 'false');
                    }
                }

                // Комментарий к заказу
                $comment = 'Клиент: ' . $order->user_comment;

                if ($order->comment) {
                    $comment .= ' (Менеджер: ' . $order->comment . ')';
                }

                $item->addChild('Комментарий', $comment);
            }

            $exportedOrders[] = $order->id;
        }

        // Сохранение списка экспортированных заказов в сессии. В следующем запросе 1С вернет подтверждение успешного
        // импорта и указанные заказы нужно будет отметить как экспортированные
        session(['exported_orders' => $exportedOrders]);

        // Возврат результата
        return response($xml->asXML(), 200, ['Content-type: text/xml; charset=utf-8']);
    }

    /**
     * Подтверждение успешного экспорта заказов в 1С
     */
    protected function confirmOrdersExport()
    {
        $exportedOrders = session('exported_orders');
        if ($exportedOrders && is_array($exportedOrders) && count($exportedOrders)) {
            Order::whereIn('id', $exportedOrders)->update(['exported' => true]);
        }

        echo "success\n";
    }

    /**
     * Импорт списка ТТН
     */
    protected function importWaybills()
    {
        $xml = new SimpleXMLElement(request()->getContent());

        // Получение списка уже загруженных ранее ТТН
        $waybills = [];
        foreach ($xml->Накладные->ТТН as $waybill) {
            $waybills[] = (string) $waybill->НомерТТН;
        }

        $existingWaybills = Waybill::whereIn('waybill', $waybills)->get()->keyBy('waybill');

        // Обработка списка ТТН из запроса
        foreach ($xml->Накладные->ТТН as $waybill) {
            // Если ТТН уже была синхронизирована ранее - пропускаем ее
            $waybillNumber = (string) $waybill->НомерТТН;
            if ($existingWaybills->has($waybillNumber)) {
                continue;
            }

            $userCode = (string) $waybill->Ид;
            $orderId = (string) $waybill->ИдЗаказ;

            // Если указан идентификатор заказа - привязываем ТТН напрямую к нему, иначе привязываем ее к пользователю
            if ($orderId) {
                $orderId = Str::before($orderId, '_');
                if ($order = Order::find($orderId)) {
                    Waybill::create([
                        'user_id' => $order->user_id,
                        'order_id' => $order->id,
                        'waybill' => $waybillNumber
                    ]);
                }
            } else {
                if ($user = User::firstWhere('code', $userCode)) {
                    Waybill::create([
                        'user_id' => $user->id,
                        'waybill' => $waybillNumber
                    ]);
                }
            }
        }

        // Сохранение текста запроса в файл для возможности отладки
        $file = fopen(storage_path('app/' . $this->exchangeDir . 'last_' . basename(request('filename'))), 'wb');
        fwrite($file, file_get_contents('php://input'));
        fclose($file);

        // Возврат успешного статуса
        echo "success\n";
    }

    /**
     * Импорт списка клиентов (пользователей)
     */
    protected function importClients()
    {
        $xml = new SimpleXMLElement(request()->getContent());

        // Получение списка пользователей по их логинам
        $logins = [];
        foreach ($xml->Клиенты->Клиент as $client) {
            $logins[] = (string) $client->Логин;
        }

        $users = User::whereIn('login', $logins)->get()->keyBy('login');
        $managers = ManagersInformations::get()->keyBy('manager_id');

        // Обновление данных
        foreach ($xml->Клиенты->Клиент as $client) {
            $login = (string) $client->Логин;
            if (! $user = $users->get($login)) {
                continue;
            }

            $user->code = (string) $client->Ид;
            $user->debt = (float) str_replace([' ', ' '], '', str_replace(',', '.', $client->Долг));
            $user->last_payment_date = (string) $client->ДатаОплаты;

            // получаем ID менеджера
            if (isset($client->Менеджер) && $managers->has((string) $client->Менеджер)) {
              $user->manager = $managers->get((string) $client->Менеджер)->id;
            }

            $priceType = [];
            foreach ($client->ТипыЦен->ИдТипЦен as $type) {
              $type = (string) $type;
              if ( isset($this->pricesType[$type]) ) {
                $priceType[$this->pricesType[$type]] = 0;
              }
            }
            $user->prices_type = array_keys($priceType);
        }

        DB::transaction(fn() => $users->each->save());

        // Сохранение текста запроса в файл для возможности отладки
        $file = fopen(storage_path('app/' . $this->exchangeDir . 'last_' . basename(request('filename'))), 'wb');
        fwrite($file, file_get_contents('php://input'));
        fclose($file);

        // Возврат успешного статуса
        echo "success\n";
    }

    /**
     * Вывод сообщения отладки
     */
    protected function debugMessage($message = '')
    {
        if (! $this->debugMode) {
            return;
        }

        $elapsedTime = microtime(true) - $this->debugStartTime;
        $hours = floor($elapsedTime / 3600);
        $minutes = floor($elapsedTime % 3600 / 60);
        $seconds = floor($elapsedTime % 60);

        echo '[' . ($hours < 10 ? '0' : '') . $hours . ':' . ($minutes < 10 ? '0' : '') . $minutes . ':' .
            ($seconds < 10 ? '0' : '') . $seconds . '] ' . $message . PHP_EOL;
    }

  /**
   * Получаем ИД бренда по названию
   * @param string $param
   * @return int|null
   */
  private function getBrand(string $param): ?int
  {
    $param = Str::lower($param);

    foreach ($this->brands as $key => $brand) {
      $brand = Str::lower($brand);
      if (mb_strpos($param, $brand) !== false) {
        return $key;
      }
    }

    return null;
  }

  /**
   * определяет ИД типа товара
   * @param string $title
   * @return int
   */
  private function getProductType(string $title): int
  {
    $title = mb_strtolower($title);
    // @TODO: массив получать из БД
    $filter = [
      'чехлы' => 1767, 'чехол' => 1767,
      'стекла' => 1781,'стекло' => 1781,
      'пленки' => 1781, 'пленка' => 1781,
      'накладка' => 1767, 'накладки' => 1767
    ];
    foreach ($filter as $item => $id) {
      if (mb_strpos($title, $item) !== false) {
        return $id;
      }
    }

    return 4; // общие аксессуары
  }

  /**
   * Получаем подтип продукта
   */
  private function getProductSubType(string $title): int
  {
    $title = Str::lower($title);

    foreach ($this->subTypes as $key => $type) {
      $type = Str::lower($type);
      $type = mb_substr($type, 0, -2, 'UTF-8');
      if (mb_strpos($title, $type) !== false) {
        return $key;
      }
    }

    return 6;
  }

  /**
   * Создаем список категорий
   */
  private function getCategories(): array
  {
    $categories = DB::table('its_menu as p')->where('p.vertical_menu', 1)
      ->leftJoin('its_menu as sub', 'sub.parent_id', '=', 'p.id')
      ->select('p.id', 'p.title', 'p.vertical_menu', 'sub.title as subTitle', 'sub.id as subId')
      ->get();

    $result= [];
    foreach ($categories as $category) {
      if ($category->subTitle === 'Silicone case') {
        $result[$category->id][$category->subId] = 'Silicon';
        continue;
      }
      $item = explode(' /', $category->subTitle);
      $result[$category->id][$category->subId] = $item[0];
    }

    return $result;
  }

  /**
   * получение списка значений фильтров
   */
  private function getFiltersValues(): array
  {
    $result = DB::table('its_filters_values')->get(['id', 'name'])->pluck('name', 'id')->toArray();
    foreach ($result as $id => $item) {
      if ($item === 'Siliconcase') {
        $result[$id] = 'Silicon';
        continue;
      }
      $text = explode(' -', $item);
      $result[$id] = str_replace('"', '', $text[0]);
    }

    return $result;
  }

  /**
   * Определяем дополнительные разделы
   * @param string $title
   * @param array $parentArray
   * @return string|null
   */
  private function getAdditMenus(string $title, array $parentArray, array $filtersArray): ?string
  {
    $title = mb_strtolower($title);
    $result = [];

    // добавление доп. категорий из названия
    if (count($parentArray) > 0) {
      foreach ($parentArray as $id => $item) {
        if (mb_stripos($title, $item) !== false) {
          $result[] = $id;
        }
      }
    }

    // добавление доп. категорий из фильтров
    if (count($filtersArray) > 0) {
      $filterValueText = [];
      foreach ($filtersArray as $item) {
        if (isset($this->filtersValues[$item])) $filterValueText[] = $this->filtersValues[$item];
      }

      if (count($filterValueText) > 0) {
        foreach ($filterValueText as $text) {;
          foreach ($parentArray as $id => $item) {
            if (mb_stripos($text, $item) !== false) {
              $result[] = $id;
            }
          }
        }
      }
    }

    return (count($result) > 0 ) ? implode(',',  array_unique($result)) : null;
  }
}
