<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use App\Traits\DisplaysProductsList;
use Illuminate\Support\Facades\Route;

class CategoriesController extends Controller
{
    use DisplaysProductsList;

    /**
     * Отображение страницы категории
     */
    public function show($section = '', $url = '', $params = '')
    {
        if ($url == 'razmernye-plenki') {
            $params = Route::current()->parameter('params');
            $url = 'futlyaru-dlya-naushnikov';
            return redirect()->route('category',
                [
                    'section' => $section,
                    'url' => $url,
                    'params' => $params
                ],
                301
            );
        }

        // Получение информации о категории по ссылке
        $category = Category::firstWhere('name', $url);
        abort_if(!$category, 404);

        // Получение списка подкатегорий
        $subcategoriesIds = DB::table('its_menu')
          ->where('parent_id', $category->id)
          ->get()->merge([$category])
          ->map(function ($subcategory) {
            return array_merge(
              [$subcategory->id],
              $subcategory->include_categories ? explode(',', $subcategory->include_categories) : []
            );
          })
          ->flatten()
          ->unique();

        // используем для поиска в дополнительных меню
        // @TODO: по принтам перенести addit_menus в таблицу и убрать этот код
        if ($subcategoriesIds->count() === 1) {
          $whereSubcategoriesIds = '%' . implode(' %', $subcategoriesIds->toArray()) . '%';
        } else {
          $whereSubcategoriesIds = null;
        }

        // Получение списка товаров в категории
        $products = Product::active()
            ->where(function ($q) use ($subcategoriesIds, $whereSubcategoriesIds) {
              $q->whereIn('menu', $subcategoriesIds);
              if ($whereSubcategoriesIds !== null) {
//                $q->orWhere('addit_menus', 'LIKE', $whereSubcategoriesIds);
              }
                $q->orWhereIn('id', DB::table('its_product_category')
                  ->select('product_id')
                  ->whereIn('category_id', $subcategoriesIds)
                );

              if ($whereSubcategoriesIds !== null) {
                $q->orWhere('addit_menus', 'LIKE', $whereSubcategoriesIds);
              }
            });

        if (in_array($url, ['chehly-tpu', 'chehly-print'])) {
          $products->where('from', '!=', 1);
        }
//            ->orderByRaw( sprintf(
//              config('custom.product_order_by'),
//              today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp)
//            );
//            ->orderByRaw('`from` ASC, qty > 0 DESC, added_time >= ' . today()->subDays(Product::NOVELTY_DAYS)->timestamp . ' DESC, '
//                . 'liquidity > 0 ASC, arrived_in_stock_at DESC, id DESC')
//            ->get(config('custom.product_fields'));

        // Получение баннера для отображения на странице категории
        $banner = $category->banners()->first();

        if (! $banner && $category->parentCategory) {
            $banner = $category->parentCategory->banners()->first();
        }

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];

        if ($category->parentCategory) {
            $breadcrumbs[] = [
                $category->parentCategory->title,
                ($category->parentCategory->name == 'chehly' || $category->parentCategory->name =='stekla-plenki')
                  ? ''
                  : custom_route('category', ['phones', $category->parentCategory->name])
            ];
        }

        $breadcrumbs[] = [$category->title, ''];

        view()->share('breadcrumbs', $breadcrumbs);

        // Формирование мета-данных
        $variables = ['name' => $category->title];

        if (! $category->meta_title) {
            $category->meta_title = text_template('category.title', $category->id, $variables);
        }

        if (! $category->meta_description) {
            $category->meta_description = text_template('category.description', $category->id, $variables);
        }

        view()->share('layoutMetaTitle', $category->meta_title);
        view()->share('layoutMetaDescription', $category->meta_description);

        view()->share('h1Text', $category->h1_text ?: text_template('category.h1', $category->id, $variables));

        // Отображение стандартного списка товаров
        $filtersActiveParentCategory = null;

        if (! $category->parentCategory) {
            $filtersActiveParentCategory = $category;
        } elseif ($category->filter_categories_parent) {
            $filtersActiveParentCategory = Category::find($category->filter_categories_parent);
        }

        // Если нужно показывать seo родительской категории, раскомментировать код
//        if ($category->hasParent()) {
//            $seoDescription = $category->parentCategory->getSeoDescription();
//        } else {
            $seoDescription = $category->getSeoDescription();
//        }

        $titleCategory = $category->title;
        $groupBySeries = (request()->cookie('groupBySeries') === null) ? true : request()->cookie('groupBySeries') == 'true';

        // получаем ссылку по серии
        if ($category->parent_id !== 0) {
          // выбираем все бренды, индексируем массив по ИД категорий
          $brands = Category::where('brand', 1)->get(['id', 'title', 'name'])->keyBy('id')->toArray();

          //  если категория ссылается на основной бренд, получаем ссылку
          if (isset($brands[$category->parent_id])) {
            $brands = $brands[$category->parent_id];
            $titleCategory = $brands['title'];
            $url = $brands['name'] . '/filter/categories=' .  $category->id;
            if (request()->cookie('groupBySeries') === null) {
              $groupBySeries = false;
            }
          } else { // проверяем по названию, если зашли с вертикального меню
            $title = $category->title;
            $key = array_filter($brands, function($item) use ($title) {
              return $item['title'] === $title;
            });
            $key = key($key);

            if ($key !== null) {
              $url = $brands[$key]['name'];
            } else {
              $url = '';
            }
          }
        }

        if (! empty($params) ) {
          $url .= '/' . $params;
          $url = removePageNum($url);
        }

        return $this->displayProductsList(
          $products,
          custom_route('category', [$section, $category->name]),
          [
            'banner' => $banner,
            'categories_filter' => ! $category->parentCategory || $category->filter_categories,
            'categories_filter_active_parent' => $filtersActiveParentCategory,
            'urlCategory' => removePageNum($url),
            'labelFroGroupClass' => 'text-orange bold',
            'labelFroGroupText' => $titleCategory,
          ],
          $seoDescription,
          $groupBySeries
        );
    }

    /**
     * Список категорий общих аксессуаров
     */
    public function accessories()
    {
        // Получение родительской категории общих аксессуаров
        if (! $accessoriesCategory = Category::find(config('custom.accessories_category_id'))) {
            abort(404);
        }

        // Получение списка подкатегорий
        $categories = view()->shared('layoutCategories')
            ->firstWhere('id', config('custom.accessories_category_id'))
            ->subcategories;

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Аксессуары'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', $accessoriesCategory->meta_title);
        view()->share('layoutMetaDescription', $accessoriesCategory->meta_title);

        $h1Text = __('Общие аксессуары');

        // Создание отображения
        return view('categories', compact(['categories', 'h1Text', 'breadcrumbs']));
    }
}
