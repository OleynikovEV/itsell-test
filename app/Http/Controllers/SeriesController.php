<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductsSeries;
use App\Traits\DisplaysProductsList;

class SeriesController extends Controller
{

    use DisplaysProductsList;

    /**
     * Серия товаров
     */
    public function show($seriesUrl = '')
    {
        // Получение информации о серии
        $series = ProductsSeries::firstWhere('url', $seriesUrl);
        if ( ! $series) abort(404);

        // Получение списка товаров в серии
        $products = Product::active()->where('products_series_id', $series->id);
//            ->orderBy('arrived_in_stock_at', 'DESC')
//            ->orderByRaw( sprintf(config('custom.product_order_by'), today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp) )
//            ->get(config('custom.product_fields'));

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Серия') . ' ' . $series->name, ''];

        view()->share('breadcrumbs', $breadcrumbs);

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Серия') . ' ' . $series->name . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Серия') . ' ' . $series->name . ' | ITsell ОПТ');

        view()->share('h1Text', __('Серия') . ' ' . $series->name);

        // Отображение стандартного списка товаров
        return $this->displayProductsList($products, custom_route('products_series', [$seriesUrl]));
    }

//    public function series($url = '')
//    {
//      $params = Route::current()->parameter('params');
//
//      if ($url == 'razmernye-plenki') {
//        $url = 'futlyaru-dlya-naushnikov';
//        return redirect()->route('series',
//          [
//            'url' => $url,
//            'params' => $params,
//          ],
//          301
//        );
//      }
//
//      // Получение информации о категории по ссылке
//      if (! $category = Category::firstWhere('name', $url)) {
//        abort(404);
//      }
//
//      // Получение списка подкатегорий
//      $subcategoriesIds = $category->subcategories->merge([$category])
//        ->map(function ($subcategory) {
//          return array_merge(
//            [$subcategory->id],
//            $subcategory->include_categories ? explode(',', $subcategory->include_categories) : []
//          );
//        })
//        ->flatten()
//        ->unique();
//
//      // Получение списка товаров в категории
//      $products = Product::active()
//        ->where(function ($q) use ($subcategoriesIds) {
//          $q->whereIn('menu', $subcategoriesIds)
//            ->orWhereIn('id', DB::table('its_product_category')
//              ->select('product_id')
//              ->whereIn('category_id', $subcategoriesIds)
//            );
//        })
//        ->orderByRaw( sprintf(config('custom.product_order_by'), today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp) )
//        ->get(config('custom.product_fields'));
//
//      $temp = $products->groupBy('products_series_id');
//      $products = new \Illuminate\Database\Eloquent\Collection();
//
//      foreach ($temp as $series => $product) {
//        if ($series === 0) {
//          foreach ($product as $item) {
//            $products->push($item);
//          }
//        } else {
//          $tmp = $product->get(0);
//          $tmp->title = __('Серия') . ' ' . Str::before($tmp->title, 'для');
//          $tmp->title = trim(Str::replaceFirst('Уценка', '', $tmp->title));
//          $products->push($tmp);
//        }
//      }
//
//      // Формирование мета-данных
//      $variables = ['name' => $category->title];
//      view()->share('h1Text', $category->h1_text ?: text_template('category.h1', $category->id, $variables));
//
//      return $this->displayProductsList(
//        $products,
//        custom_route('series', ['url' => $url, 'params' => $params])
//      );
//    }

    public function group($groupUrl = '')
  {
    // Получение информации о серии
    $category = Category::firstWhere('name', $groupUrl);
    if ( ! $category) abort(404);

    // Получение списка товаров в серии
    $products = Product::active()->where('menu', $category->id);
//      ->orderByRaw(sprintf(config('custom.product_order_by'), today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp))
//      ->get(config('custom.product_fields'));

    // Формирование хлебных крошек
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
    $breadcrumbs[] = [__('Группа товаров') . ' ' . $category->title, ''];

    view()->share('breadcrumbs', $breadcrumbs);

    // Формирование мета-данных
    view()->share('layoutMetaTitle', __('Группа товаров') . ' ' . $category->title . ' | ITsell ОПТ');
    view()->share('layoutMetaDescription', __('Группа товаров') . ' ' . $category->seo_description . ' | ITsell ОПТ');

    view()->share('h1Text', __('Группа товаров') . ' ' . $category->title);

    // Отображение стандартного списка товаров
    return $this->displayProductsList($products, custom_route('group', [$groupUrl]));
  }
}
