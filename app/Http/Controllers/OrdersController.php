<?php

namespace App\Http\Controllers;

use App\Events\PriceZeroEvent;
use App\Http\Models\CartContent;
use App\Jobs\PriceZeroNotificationJob;
use App\Models\City;
use App\Models\Order;
use App\Models\PriceRange;
use App\Models\PrintCover;
use App\Models\Street;
use App\Models\Product;
use App\Models\Warehouse;
use App\Events\CartModified;
use App\Events\OrderCreated;
use App\Services\Novaposhta;
use App\Http\Requests\OrderRequest;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    /**
     * Создание заказа
     */
    public function storeOld(OrderRequest $request)
    {
        // Получение данных пользователя
        $user = auth()->user();

        // если корзина пустая
        if (Cart::content()->count() === 0) {
          Log::error('Корзина пуста. Заказчик ' .  $user->email);
          return response()->json([
            'success' => true,
            'redirect_url' => custom_route('cart.empty')
          ]);
        }

        $dropshipping = (bool) request('dropshipping');

        $simpleOrder = collect();
        $hydrogel = collect();
        $hydrogelCount = 0;
        $hydrogelTotal = 0;
        $cart = new CartContent;

        $hydrogelOrder = null;

        // настройка заказа
        $orderField = [
          'fio' => $user->fio,
          'company' => $user->company,
          'email' => $user->email,

          'firstname' => request('firstname'),
          'lastname' => request('lastname'),
          'phone' => preg_replace('/[^0-9+]/', '', request('phone')),

          'delivery_type_id' => request('delivery_type_id'),

          'city' => '',
          'city_id' => request('city_id'),
          'user_comment' => request('comment') ?: '',
          'dropshipping' => $dropshipping,

          //'total' => Cart::total(),
          'total' => 0,

          'user_id' => $user->id,
          'ip' => request()->ip(),
          'time' => time(),

          'declaration' => '',
          'comment' => '',
          'manager' => 0,
          'country' => 'ua',
          'address' => '',
          'delivery_comment' => '',
          'street' => '',
          'street_building' => '',
          'price_type' => request('price-type') ?? null,
        ];

        $priceTypeDefault = 'opt_price';
        // проверяем выбранный из списка тип цен присутствует в доступном диапазоне клиенту
        if (request()->cookie('price_type') && in_array(request()->cookie('price_type'), array_keys($priceList))) {
          $priceTypeDefault = request()->cookie('price_type');
        } elseif (! auth()->guest()) {
          $priceTypeDefault = auth()->user()->getPriceDefault();
        }

        // разделение заказов, если дропшип
        if (! $dropshipping) {
          $hydrogel = $cart->content()->filter(fn($item) => $item->from === 2);
          foreach ($hydrogel as $item) {
            $hydrogelTotal += $item->getPrice($priceTypeDefault);
            $hydrogelCount += $item->qty;
          }

          $hydrogelOrder = new Order($orderField);
          $hydrogelOrder['total'] = $hydrogelTotal;
        }

        if (Cart::count() !== $hydrogelCount) {
          // Подготовка основных данных заказа
          $order = new Order($orderField);
          $order['total'] = Cart::total() - $hydrogelTotal;
          $simpleOrder = $cart->content()->filter(fn($item) => $item->from !== 2);
        } else {
          $order = $hydrogelOrder;
          $simpleOrder = $hydrogel;
        }

        // Получение параметров доставки
        switch ($order->delivery_type_id) {
            case 1:
                $order->warehouse_id = request('warehouse_id');
                if ($hydrogelOrder !== null) $hydrogelOrder->warehouse_id = request('warehouse_id');
                break;

            case 2:
                $order->street_id = request('street_id');
                $order->street_building = request('street_building');
                if ($hydrogelOrder !== null) {
                  $hydrogelOrder->street_id = request('street_id');
                  $hydrogelOrder->street_building = request('street_building');
                }
                break;

            case 3:
                $order->address = request('address') ?: '';
                $order->delivery_comment = request('delivery_comment') ?: '';
                if ($hydrogelOrder !== null) {
                  $hydrogelOrder->address = request('address') ?: '';
                  $hydrogelOrder->delivery_comment = request('delivery_comment') ?: '';
                }
                break;
        }

        // Получение названия выбранного города
        if ($order->city_id) {
            $city = City::find($order->city_id);
            if ($city) {
              $order->city = $city->{app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua'};
              $hydrogelOrder->city = $city->{app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua'};
            }
        } else {
            $order->city = ' ';
            if ($hydrogelOrder !== null) $hydrogelOrder->city = ' ';
        }

        // Получение названия выбранной улицы
        if ($order->street_id) {
            $street = Street::find($order->street_id);
            if ($street) {
              $order->street = $street->name;
              if ($hydrogelOrder !== null) $hydrogelOrder->street = $street->name;
            }
        }

        // Получение дополнительных параметров для дропшиппинг-заказов
        if ($order->dropshipping) {
            $order->payment_type_id = request('payment_type_id');
            $order->dropshipping_price = (int) request('dropshipping_price') ?: 0;
            $order->package_payer = $order->payment_type_id == 1 ? request('package_payer') : 1;

            if ($hydrogelOrder !== null) {
              $hydrogelOrder->payment_type_id = request('payment_type_id');
              $hydrogelOrder->dropshipping_price = (int) request('dropshipping_price') ?: 0;
              $hydrogelOrder->package_payer = $hydrogelOrder->payment_type_id == 1 ? request('package_payer') : 1;
            }

            // Расчет стоимости доставки заказа
            $deliveryPrice = $this->deliveryPrice($order->city_id, $order->delivery_type_id, $order->payment_type_id == 1
                ? $order->dropshipping_price : 0)->getData();
            if ($deliveryPrice->success) {
              $order->delivery_price = $deliveryPrice->response;
              if ($hydrogelOrder !== null) $hydrogelOrder->delivery_price = $deliveryPrice->response;
            }
        }

        // Сохранение заказа
        $order->save();
        if ($hydrogelOrder !== null) $hydrogelOrder->save();

        if ($hydrogelOrder !== null) session(['last_order' => $hydrogelOrder->id]);
        else session(['last_order' => $order->id]);

        $products = collect();
        $cover = collect();

        $productsIds = [];
        $coversIds = [];
        foreach (Cart::content()->pluck('id') as $item) {
          if (strlen($item) > 10) {
            $coversIds[] = $item;
          } else {
            $productsIds[] = $item;
          }
        }

        // Получение данных товаров в заказе
        if (count($productsIds) > 0) {
          $products = Product::whereIn('id', $productsIds)->get([
            'id', 'id as uuid', 'qty', 'qty_today', 'qty_with_delay', 'promo_code'
          ])->keyBy('id');
        }

        if (count($coversIds) > 0) {
          $cover = PrintCover::whereIn('uuid', $coversIds)
            ->with('brandData', 'categoryVchehle')
            ->get(['uuid', 'image', 'price', 'qty', 'material_id', 'category_id', 'show_price', 'brand'])
          ->keyBy('uuid');

          foreach ($cover as $item) {
            $products->put($item->uuid, $item);
          }
        }

//        $products = $products->merge($cover)->keyBy('uuid');

        //$hydrogel = $cart->content()->filter(fn($item) => $item->from === 2);

        if ($hydrogelOrder !== null) {
          foreach ($hydrogel as $item) {
            if ( ! $products->has($item->id)) continue;

            $order->products()->create($this->getProduct($products[$item->id], $item, $priceTypeDefault));
          }
        }

        // Сохранение списка товаров в заказе
        if (Cart::count() !== $hydrogelCount) {
//        foreach (Cart::content() as $item) {
          foreach ($simpleOrder as $item) {
            if (!$products->has($item->id)) continue;

//            $qty_with_delay = $products[$item->id]->qty_today < $item->qty ?
//              min($item->qty - $products[$item->id]->qty_today, $products[$item->id]->qty_with_delay) : 0;
//
//            $qty_shortage = $products[$item->id]->material_id !== null
//              ? 0
//              : max($item->qty - $products[$item->id]->qty_today - $products[$item->id]->qty_with_delay, 0);
//
//            $orderProductData = [
//              'product_id' => $item->id,
//              'count' => $item->qty,
//              'qty_with_delay' => $qty_with_delay ?? 0,
//              'qty_shortage' => $qty_shortage,
//              'discount' => $item->discountRate, // процент скидки
//              'price' => $item->price,
//              'comment' => '',
//              'material' => $item->options->material ?? null,
//              'full_border' => $item->options->fullBorder ?? false,
//              'promo_code' => ($item->discountRate > 0) ? $products[$item->id]->promo_code : '',
//              'print' => (isset($item->options['print']) && isset($item->options['print']) == 1),
//            ];
            $order->products()->create($this->getProduct($products[$item->id], $item));
          }
        }

        // Очистка корзины
        Cart::destroy();

        // Вызов события изменения корзины для ее сохранения профиле пользователя
        event(new CartModified);

        // Вызов события создания заказа
        if (Cart::count() !== $hydrogelCount) {
          event(new OrderCreated($order));
        }

        if ($hydrogelOrder !== null) event(new OrderCreated($hydrogelOrder));

        // Возврат успешного статуса
        return response()->json([
            'success' => true,
            'redirect_url' => custom_route('order_success')
        ]);
    }

    /**
     * Создание заказа
     */
    public function store(OrderRequest $request)
    {
      // Получение данных пользователя
      $user = auth()->user();
      $manager = $user->managerOpt;
      $selectedCart = getSelectedCart();

      // если корзина пустая
      if (Cart::instance($selectedCart)->content()->count() === 0) {
        Log::error('Корзина пуста. Заказчик ' .  $user->email);
        return response()->json([
          'success' => true,
          'redirect_url' => custom_route('cart.empty')
        ]);
      }

      $dropshipping = (bool) request('dropshipping');

      // настройка заказа
      $fields = [
        'fio' => $user->fio,
        'company' => $user->company,
        'email' => $user->email,

        'firstname' => request('firstname'),
        'lastname' => request('lastname'),
        'phone' => preg_replace('/[^0-9+]/', '', request('phone')),

        'delivery_type_id' => request('delivery_type_id'),

        'city' => '',
        'city_id' => request('city_id'),
        'user_comment' => request('comment') ?: '',
        'dropshipping' => $dropshipping,

        //'total' => Cart::total(),
        'total' => 0,

        'user_id' => $user->id,
        'ip' => request()->ip(),
        'time' => time(),

        'declaration' => '',
        'comment' => '',
        'manager' => 0,
        'country' => 'ua',
        'address' => '',
        'delivery_comment' => '',
        'street' => '',
        'street_building' => '',
        'price_type' => request('price-type') ?? null,
      ];

      // получаем тип цены
      $priceTypeDefault = getPriceTypeDefault();

      $cart = new CartContent($priceTypeDefault, $selectedCart);
      $cart->setPriceRange((new PriceRange)->priceRangeByType($priceTypeDefault));
      // разделяем заказы на две группы: гидрогелевые стекла и все остальное
      $orders = $cart->content()->groupBy(fn($item) => ($item->from === 2 && !$dropshipping) );

      $howSaveOrderId = false;
      // если в заказе только гидрогелевые пленки
      if ($orders->count() === 1 && $orders->has(1)) {
        $howSaveOrderId = true;
      }

      $products = [];
      $productZeroIds = [];
      $productZeroAction = false;

      foreach ($orders as $key => $orderItem) {
        $total = $orderItem->sum(fn($item) => $item->getPrice($priceTypeDefault) * $item->qty);

        if ($total == 0) {
          $productZeroAction = true;
          if ($key === 0) array_push($productZeroIds, $orderItem->id);

          continue;
        }

        $orderFields = $fields;
        $orderFields['total'] = $total;
        if ($key === 1) $orderFields['comment'] = 'Гидрогель поштучно';

        // Подготовка основных данных заказа
        $order = new Order($orderFields);
        $order->from_cart = $selectedCart;
        $this->setDeliveryType($order); // записываем параметры доставки
        $this->setCity($order);// Получение названия выбранного города
        $this->setStreet($order);// Получение названия выбранной улицы
        $this->setDropshipping($order);// Получение дополнительных параметров для дропшиппинг-заказов

        // Сохранение заказа
        $order->save();

        if ($howSaveOrderId || $key === 0) {
          session(['last_order' => $order->id]);
        }

        // Сохранение списка товаров в заказе
        foreach ($orderItem as $product) {
          if ($product->getPrice($priceTypeDefault) == 0) {
            $productZeroAction = true;
            if ($key === 0) array_push($productZeroIds, $product->id);

            continue;
          }
          array_push($products, $product->rowId); // добавляем для удаления из корзины
          $order->products()->create($this->getProduct($product, $priceTypeDefault));
        }

        // Вызов события создания заказа
        event(new OrderCreated($order, $manager));
      }

      // Очистка корзины
      if (! $productZeroAction || count($products) === 0) {
        Cart::instance($selectedCart)->destroy();

        // Вызов события изменения корзины для ее сохранения профиле пользователя
        event(new CartModified);

        // Возврат успешного статуса
        return response()->json([
          'success' => true,
          'redirect_url' => custom_route('order_success')
        ]);
      } else {
        foreach ($products as $productId) {
          Cart::instance($selectedCart)->remove($productId);
        }

        // Вызов события изменения корзины для ее сохранения профиле пользователя
        event(new CartModified);

        // Отправить уведомление менеджерам в чат о товарах с ценой = 0
        event( new PriceZeroEvent($productZeroIds) );

        Session::flash('message', __('Данная часть заказа, не может быть оформлена, так как на товары нету цены.'));

        return response()->json([
          'success' => true,
          'redirect_url' => custom_route('cart')
        ]);
      }
    }

    // записываем параметры доставки
    protected function setDeliveryType(&$order)
    {
      switch ($order->delivery_type_id) {
        case 1:
          $order->warehouse_id = request('warehouse_id');
          break;

        case 2:
          $order->street_id = request('street_id');
          $order->street_building = request('street_building');
          break;

        case 3:
          $order->address = request('address') ?: '';
          $order->delivery_comment = request('delivery_comment') ?: '';
          break;
      }
    }

    // записываем название города
    protected function setCity(&$order)
    {
      $city = null;
      if ($order->city_id > 0) {
        $city = City::find($order->city_id);
      } elseif ($order->warehouse_id > 0) {
        $warehouse = Warehouse::find($order->warehouse_id);
        if (isset($warehouse->city_id)) {
          $city = City::find($warehouse->city_id);
        }
      } elseif ($order->street_id > 0) {
        $street = Street::find($order->street_id);
        if (isset($street->city_id)) {
          $city = City::find($street->city_id);
        }
      }

      if ($city) {
        $order->city = $city->{app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua'};
        $order->city_id = $city->id;
      }
      else {
        $order->city = ' ';
      }
    }

    // записываем название улицы
    protected function setStreet(&$order)
    {
      if ($order->street_id) {
        $street = Street::find($order->street_id);
        if ($street) {
          $order->street = $street->name;
        }
      }
    }

    // записываем данные по дропу
    protected function setDropshipping(&$order)
    {
      if ($order->dropshipping) {
        $order->payment_type_id = request('payment_type_id');
        $order->dropshipping_price = (int) request('dropshipping_price') ?: 0;
        $order->package_payer = $order->payment_type_id == 1 ? request('package_payer') : 1;

        // Расчет стоимости доставки заказа
        $deliveryPrice = $this->deliveryPrice($order->city_id, $order->delivery_type_id, $order->payment_type_id == 1
          ? $order->dropshipping_price : 0)->getData();
        if ($deliveryPrice->success) {
          $order->delivery_price = $deliveryPrice->response;
        }
      }
    }

    protected function getProduct($product, $priceTypeDefault)
    {
      $qty_with_delay = $product->qty_today < $product->qty ?
        min($product->qty - $product->qty_today, $product->qty_with_delay) : 0;

      $qty_shortage = ($product->from === 1 || $product->from === 2)
        ? 0
        : max($product->qty - $product->qty_today - $product->qty_with_delay, 0);

      return [
        'product_id' => $product->id,
        'count' => $product->qty,
        'qty_with_delay' => $qty_with_delay ?? 0,
        'qty_shortage' => $qty_shortage,
        'discount' => $product->discountRate, // процент скидки
        'price' => $product->getPrice($priceTypeDefault),
        'comment' => '',
        'material' => $product->options->material ?? null,
        'full_border' => $product->options->fullBorder ?? false,
        'promo_code' => ($product->discountRate > 0) ? $product->promo_code : '',
        'print' => (isset($product->options['print']) && isset($product->options['print']) == 1),
      ];
    }

    /**
     * Страница успешного заказа
     */
    public function success()
    {
        // Получение информации о последнем заказа
        $order = session('last_order') ? Order::find(session('last_order')) : false;
        if ( ! $order) return redirect(custom_route('main_page'));

        // Получение списка товаров с недостаточным количеством в наличии
        $order->load('products.product');
        $shortageProducts = $order->products->where('qty_shortage', '>', 0);

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Заказ оформлен'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Спасибо за Ваш заказ!') . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Спасибо за Ваш заказ!') . ' | ITsell ОПТ');

        // Создание отображения
        return view('order-success', compact('order', 'shortageProducts', 'breadcrumbs'));
    }

    /**
     * Список отделений определенного способа доставки в заданном городе
     */
    public function warehouses()
    {
        // Получение данных о выбранном городе
        $city = City::find(request('city_id'));
        if ( ! $city) return response()->json([]);

        // Получение списка складов в городе
        $warehouses = Warehouse::where('city_id', $city->id)
            ->orderBy('number', 'ASC')
            ->get()
            ->map(function($item)
            {
                return [
                    'id' => $item->id,
                    'name' => app()->getLocale() == 'ru' && $item->address_ru ? $item->address_ru : $item->address_ua
                ];
            });

        // Возврат результата
        return response()->json([
            'success' => true,
            'response' => $warehouses
        ]);
    }

    /**
     * Список улиц для курьерской доставки в заданном городе
     */
    public function streets()
    {
        // Получение данных о выбранном городе
        $city = City::find(request('city_id'));
        if ( ! $city) return response()->json([]);

        // Получение списка улиц в городе
        $streets = Street::where('city_id', $city->id)
            ->orderBy('name', 'ASC')
            ->get()
            ->map->only('id', 'name');

        // Возврат результата
        return response()->json([
            'success' => true,
            'response' => $streets
        ]);
    }

    public function deliveryPrice($cityId = 0, $deliveryTypeId = 0, $paymentAmount = 0)
    {
        // Базовые настройки
		    $senderCityCode = 'db5c88d0-391c-11dd-90d9-001a92567626';
		    $weight = 0.5;
		    $cost = 200;

        // Проверка способа доставки
        if (array_search($deliveryTypeId, [1, 2]) === false) {
            return response()->json(['success' => false, 'message' => __('Указанный способ доставки не найден')]);
        }

		    // Получение города получателя
        $recipientCity = City::find($cityId);
        if ( ! $recipientCity) {
          return response()->json(['success' => false, 'message' => __('Указанный город не найден')]);
        }

        // Подготовка параметров запроса
        $request = [
          'CitySender' => $senderCityCode,
          'CityRecipient' => $recipientCity->np_code,
          'Weight' => $weight,
          'ServiceType' => ['WarehouseWarehouse', 'WarehouseDoors'][$deliveryTypeId - 1],
          'Cost' => $cost,
          'CargoType' => 'Cargo',
          'SeatsAmount' => 1,
        ];

        // Если задана сумма для оплаты - учитываем стоимость наложенного платежа
        if ((int) $paymentAmount) {
          $request['RedeliveryCalculate'] = [
            'CargoType' => 'Money',
            'Amount' => (int) $paymentAmount
          ];
        }

        // Запрос к API для просчета стоимости доставки
        $api = new Novaposhta();
        $response = $api->query('InternetDocument', 'getDocumentPrice', $request);

        if ( ! $response || ! $response->success || ! isset($response->data[0])) {
          return response()->json(['success' => false, 'message' => __('Не удалось рассчитать стоимость доставки')]);
        }

        $deliveryPrice = round( $response->data[0]->Cost + ($response->data[0]->CostRedelivery ?? 0) );

        // прибавить 2% от заявленной суммы - если наложный платеж
//        if ((int) $paymentAmount) {
//          $deliveryPrice = $deliveryPrice + round(($paymentAmount * 2) / 100);
//        }

        // Возврат результата
        return response()->json([
            'success' => true,
            'response' => $deliveryPrice
        ]);
    }

}
