<?php

namespace App\Http\Controllers;

class PrintCoverController extends Controller
{
  public function cover()
  {
    // Формирование хлебных крошек
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
    $breadcrumbs[] = [__('Свой дизайн чехлов'), ''];

    // Формирование мета-данных
    view()->share('layoutMetaTitle', __('Свой дизайн чехлов') . ' | ITsell ОПТ');
    view()->share('layoutMetaDescription', __('Создание своего уникального дизайна чехла для телефона') . ' | ITsell ОПТ');

    return view('printcover', compact('breadcrumbs'));
  }
}