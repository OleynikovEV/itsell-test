<?php

namespace App\Http\Controllers;

use App\Models\User;

class Notification extends \Illuminate\Routing\Controller
{
  public function unsubscribe($userId, $messenger)
  {
    $user = User::find($userId);

    if ($user !== null) {
      if ($messenger === 'telegram') $user->telegram_chat_id = 0;
      if ($messenger === 'viber') $user->viber_chat_id = null;

      $user->save();
    }

    return redirect()->back();
  }
}