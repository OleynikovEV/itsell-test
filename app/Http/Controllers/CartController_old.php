<?php

namespace App\Http\Controllers;

use App\Events\CartModified;
use App\Models\City;
use App\Models\Order;
use App\Models\PrintCover;
use App\Models\Product;
use App\Models\PromoActions;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{
  /**
   * Отображение страницы корзины
   */
  public function index()
  {
    $updateCart = false;
    // Получение информации о товарах в корзине
    $items = Cart::content();

    $productsIds = [];
    $coversIds = [];
    foreach ($items->pluck('id') as $item) {
      if (strlen($item) > 10) {
        $coversIds[] = $item;
      } else {
        $productsIds[] = $item;
      }
    }

    $products = collect();
    $cover = collect();

    if (count($productsIds) > 0) {
      $products = Product::whereIn('id', $productsIds)
        ->with('parentProduct.brandData', 'categoryData', 'promoaction')
        ->get(['id', 'baseid', 'id as uuid', 'title', 'qty', 'img', 'title', 'parent', 'url', 'from',
          'color_name', 'when_appear', 'opt_price', 'old_opt_price', 'group_products', 'added_time', 'price_modify_time',
          'color', 'menu', 'show_price', 'brand'])
        ->keyBy('id');
    }

    if (count($coversIds) > 0) {
      $cover = PrintCover::whereIn('uuid', $coversIds)
        ->selectRaw('uuid, title, image, price, qty, material_id, category_id, 
            category_id as parent, show_price, brand, 1 as `from`')
        ->with('brandData', 'categoryVchehle')
        ->get()
        ->keyBy('uuid');

      foreach ($cover as $item) {
        $products->put($item->uuid, $item);
      }
    }

    // Определение списка товаров, требующих упаковки
    $requiredPackages = [];

    foreach ($items as $item) {
      $product = $products->get($item->id);
//            $product = $products[$item->id];

      if ( ! $product) continue;

      // добавляем необходимые поля для отображения
      if (! isset($product->id)) { // если это чехлы Свой дизайн
        $product->url = null;
        $product->baseid = null;
      } else {
        $product->url = custom_route('product', [
          optional($product->parentProduct)->url ?: $product->url, optional($product->parentProduct)->id ?: $item->id
        ]);
        $product->image = $product->img ? config('custom.content_server') . thumb_name($product->img) : '/template/images/noimage.jpg';
        $product->baseid = ltrim(optional($products->get($item->id)->parentProduct)->baseid ?? $products->get($item->id)->baseid, '0');
      }

      $product->rowId  = $item->rowId;
      $product->label = $product->getLabel();
      $product->orderInfo = $item;
      $product->outOfStock = $product->qty == 0 && !$product->when_appear;
      $product->colorBorder = colorWhite($product->color ?? '');
      $product->selectedMaterial = $item->options->material ?? null;
      $product->material = $product->availableMaterials;
      $product->materialName = ($product->selectedMaterial === null )
        ? null
        : ['silicone' => __('тпу'), 'plastic' => __('3D пластик')][$product->selectedMaterial];
      $product->brandName = ($product->parentProduct)
        ? optional($product->parentProduct->brandData)->title
        : optional($product->brandData)->title;
      $product->modelName = ($product->categoryData)
        ? $product->categoryData->title
        : $product->categoryVchehle->category->title ?? null;
      $product->modelUrl = ($product->categoryData)
        ? custom_route('category', ['phones', $product->categoryData->name])
        : null;
      $product->price = calcSale($item->price, $item->discount); // цена за единицу $
      $product->price_uah = round(calcSale($item->price, $item->discount) * settings('dollar')); // цена за единицу в грн
      $product->total_uah = round($item->price * $item->qty * settings('dollar')); // общая стоимость кол-во * цена в грн
      $product->old_opt_price_total_uah = round($products->get($item->id)->old_opt_price * settings('dollar')); // общая стоимость кол-во * старая цена в грн
      $product->qty_ordered = $item->qty;
      $product->subtotal = $item->subtotal();
      $product->subtotal_uah = round($item->subtotal() * settings('dollar'));
      $product->status = ($product->qty > 0)
        ? 'доступен'
        : (($product->when_appear)
          ? 'ожидается'
          : 'нет в наличии');

      // если акция закончилась и промокод не работает, обнуляем скидку
      if ($item->discount > 0 && ($product->promoaction == null || !$product->promoaction->isActive())) {
        Cart::setDiscount($item->rowId, 0);
        $updateCart = true;
      }

      if ($product->parentProduct) {
        $parentProduct = $product->parentProduct;
        $product->has_package = $parentProduct->has_package;
        $product->filters_values = $parentProduct->filters_values;
      }

      // Если товар продается без упаковки и сам не является упаковкой - определяем тип упаковки, который ему требуется
      if ( ! $product->has_package && ! $product->is_package_for) {
        foreach (config('custom.package_types') as $packageTypeId => $packageType) {
          if (isset($product->filters_values) && count(array_intersect($packageType['filters'], $product->filters_values))) {
            $product->need_package = true;
            if (array_search($packageTypeId, $requiredPackages) === false) $requiredPackages[] = $packageTypeId;
          }
        }
      }
    }

    // Вызов события изменения корзины для ее сохранения в профиле пользователя
    if ($updateCart) {
      event(new CartModified);
    }

    // Получение списка упаковок
    $packages = count($requiredPackages) ? Product::active()->whereIn('is_package_for', $requiredPackages)->get() : null;

    // Получение списка доступных для доставки городов
    $cities = null;
    $citiesNameField = app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua';
//        $cities = City::where($citiesNameField, '!=', '')->orderBy($citiesNameField, 'ASC')->get()->pluck($citiesNameField, 'id');
//        $cities = City::whereIn('id', [1561, 926, 2337, 640])->orderBy($citiesNameField, 'ASC')->get()->pluck($citiesNameField, 'id');

    // Получение последнего заказа текущего пользователя не отмеченного как Дропшиппинг
    if (auth()->check())
    {
      $lastOrder = Order::where('user_id', auth()->id())
        ->where('state', '!=', 6)
        ->where('dropshipping', false)
        ->orderBy('id', 'DESC')
        ->first();
    }
    else $lastOrder = null;

    // Получение списка товаров со скидкой
    $saleProducts = cache()->remember('main_page_sale_' . app()->getLocale(), $this->dataCacheTime, function()
    {
      $liquidityGroups = [];
      if (settings('products_so_discount')) $liquidityGroups[] = 2;
      if (settings('products_nl_discount')) $liquidityGroups[] = 3;

      return Product::active()
        ->where('price_modify_time', '>=', today()->subWeek(4)->timestamp)
        ->orWhereIn('liquidity', $liquidityGroups)
        ->inRandomOrder()
        ->limit(20)
        ->with('series')
        ->get();
    });

    // Формирование хлебных крошек
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
    $breadcrumbs[] = [__('Корзина'), ''];

    // Формирование мета-данных
    view()->share('layoutMetaTitle', __('Корзина') . ' | ITsell ОПТ');
    view()->share('layoutMetaDescription', __('Корзина') . ' | ITsell ОПТ');

//        $products = array_values($products->toArray());
    // Создание отображения
//        return view('cart', compact('items', 'products', 'packages', 'cities', 'lastOrder', 'saleProducts', 'breadcrumbs'));
    return view('cart-new', compact('items', 'products', 'packages', 'cities', 'lastOrder', 'saleProducts', 'breadcrumbs'));
//        return view('cart-vue', compact('items', 'products', 'packages', 'cities', 'lastOrder', 'saleProducts', 'breadcrumbs'));
  }

  /**
   * представление - клиент делает заказ а корзина пуста
   */
  public function cartEmpty()
  {
    date_default_timezone_set('Europe/Kiev');

    $user = auth()->user();
    $lastOrder = Order::where('user_id', $user->id)->with('products')->latest('time')->first();
    $products = collect();

    if ($lastOrder->products->count() > 0) {
      $productsIds = $lastOrder->products->pluck('product_id')->toArray();
      $products = Product::whereIn('id', $productsIds)->get(['id', 'title'])->keyBy('id');
    }

    // Формирование хлебных крошек
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
    $breadcrumbs[] = [__('Корзина'), ''];

    // Формирование мета-данных
    view()->share('layoutMetaTitle', __('Корзина') . ' | ITsell ОПТ');
    view()->share('layoutMetaDescription', __('Корзина') . ' | ITsell ОПТ');

    return view('cart-empty', compact('lastOrder', 'products'));
  }

  /**
   * Добавление товаров в корзину
   */
  public function store()
  {
    // Получение параметров запроса
    $productsQty = request('products');

    if ( !$productsQty || !is_array($productsQty) || !count($productsQty)) return response()->json(['success' => false]);

    // Получение информации о товарах
    $products = Product::whereIn('id', array_keys($productsQty))->get()->keyBy('id');

    // Добавление товаров в корзину
    foreach ($productsQty as $productId => $qty)
    {
      $product = $products->get($productId);
      if ( ! $product) continue;

      $options = [];
      if (isset($product->from) && $product->from == 1) { // если заказ - это чехол принт
        $options = [
          'print' => true,
          'material' => request('material') ?? $product->getMaterial(),
        ];
      }
      Cart::add($product->id, 'product_' . $product->id, $qty, $product->getPrice(), 0, $options);
    }

    // Вызов события изменения корзины для ее сохранения профиле пользователя
    event(new CartModified);

    // Возврат успешного статуса
    return $this->successState();
  }

  /**
   * добавляем в корзину принты из конструктора
   */
  public function storeCover()
  {
    $printCover = request('cover', false); // проверяем пришел ли товар из конструктора

    if ($printCover == null || count($printCover) == 0) return response()->json(['success' => false]);

    $data = request('cover');
    $product = (new PrintCover())->insert(
      $data['uuid'],
      __('Свой дизайн') . ' ' . $data['category'],
      $data['image'],
      1,
      4.21, //$data['price'],
      $data['category_id'],
      $data['material_id'] ?? 'silicone',
    );

    $options = [
      'print' => true,
      'material' => $data['material_id'] ?? 'silicone',
    ];

    // добавляем в корзину
    Cart::add($product->uuid, 'product_' . $product->uuid, $product->qty, $product->getPrice(), 0, $options);

    // Вызов события изменения корзины для ее сохранения профиле пользователя
    event(new CartModified);

    // Возврат успешного статуса
    return $this->successState();
  }

  /**
   * добавить к продукту промокод
   */
  public function updatePromocode()
  {
    $update = false;
    $promocode = request('promocode');

    if ($promocode) {
      $promo = PromoActions::where('code', $promocode);

      if ($promo->count() == 0) { // промокод не существует
        return response()->json('промокод не существует');
      }

      $promo = $promo->first();

      // промокод еще не действует
      if (now()->timestamp < $promo->time_start) {
        return response()->json('промокод еще не действует');
      }

      // срок дкйствия промокода истек
      if (now()->timestamp > $promo->time_end) {
        return response()->json('срок дкйствия промокода истек');
      }

      $cart = Cart::content(); // получаем список из корзины

      // получаем ИД и промокод товара
      $products = Product::find($cart->pluck('id'))->where('promo_code', $promocode)->keyBy('id');

      // если такие товары есть, добавляем им промокод
      if (count($products) > 0) {
        foreach($products as $product) {
          $cart->search(function($cartItem, $rowId) use ($product, $promocode, &$update) {
            // скидка сработает только один раз
            if ($product->id === $cartItem->id && $cartItem->discountRate == 0) {
              $update = true;
              Cart::setDiscount($rowId, $product->promoaction->getSale());
            }
          });
        }

        if ($update === true) {
          // Вызов события изменения корзины для ее сохранения профиле пользователя
          event(new CartModified);
          return $this->successState();
        }
        return  response()->json('Промокод уже применен ранее');
      } else {
        return response()->json('Нет товаров к которым можно пременить данный промокдо');
      }
    }

    return response()->json(false);
  }

  // смена цвета
  public function changeColor() {
    $rowId = request('rowId');
    $newId = request('newId');

    Cart::update($rowId, ['id' => $newId]);

    // Вызов события изменения корзины для ее сохранения профиле пользователя
    event(new CartModified);

    // Возврат успешного статуса
    return response()->json([
      'success' => true,
    ]);
  }

  // обновляем материал
  public function updateMaterial()
  {
    $material = request('material');
    $rowId = request('rowId');

    $options = [
      'material' => $material ?? 'silicone',
    ];

    Cart::update($rowId, ['options' => $options]);

    event(new CartModified);

    return response()->json([
      'success' => true
    ]);
  }

  /**
   * Изменение товаров в корзине
   */
  public function update()
  {
    // Получение параметров запроса
    $productsQty = request('products');
    if ( ! $productsQty || ! is_array($productsQty) || ! count($productsQty)) return response()->json(['success' => false]);

    // Обновление данных о товарах
    foreach ($productsQty as $rowId => $qty)
    {
      // Обновление количества товара
      $qty = (int) $qty;
      if ( ! $qty || $qty < 0) continue;

      Cart::update($rowId, ['qty' => $qty]);
    }

    // Вызов события изменения корзины для ее сохранения профиле пользователя
    event(new CartModified);

    // Возврат успешного статуса
    return $this->successState();
  }

  /**
   * Удаление товара из корзины
   */
  public function destroy($rowId = '')
  {
    if (Cart::content()->has($rowId)) Cart::remove($rowId);

    // Вызов события изменения корзины для ее сохранения профиле пользователя
    event(new CartModified);

    // Возврат успешного статуса
    return $this->successState();
  }

  /**
   * Возврат успешного статуса с базовой информацией о состоянии корзины
   */
  protected function successState()
  {
    return response()->json([
      'success' => true,
      'response' => [
        'count' => Cart::count(),
        'count_sku' => Cart::content()->count(),
        'total' => Cart::total(),
        'items' => Cart::content()->map(function($item)
        {
          return [
            'qty' => $item->qty,
//                        'total' => round($item->price * $item->qty, 2),
            'promocode' => $item->discount > 0,
            'price' => calcSale($item->price, $item->discount),
            'price_uah' => round(calcSale($item->price, $item->discount) * settings('dollar')),
            'discountRate' => $item->discountRate,
            'total' => $item->subtotal(),
            'total_uah' => round($item->price * $item->qty * settings('dollar')),
          ];
        })
      ]
    ]);
  }

}
