<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use App\Models\Product;
use App\Models\User;
use App\Models\WaitingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Api extends Controller
{
    public function getCities(Request $request)
    {
      // Получение списка доступных для доставки городов
      $citiesNameField = app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua';
      $cities = City::where($citiesNameField, '!=', '');

      $search = $request->get('search');

      if ($search != null) {
        $cities = $cities->where($citiesNameField, 'like', '%' . $search . '%');
      }

      $cities = $cities->orderBy($citiesNameField, 'ASC')->get();

      return response()->json($cities);
    }

    public function updateCommentInWishList(Request $request)
    {
      if (auth()->guest()) {
        return response()->json(false, 401);
      }

      if (!$request->has('itemId')) {
        return response()->json(false, 400);
      }

      $itemId = $request->post('itemId');
      $comment = $request->post('comment') ?? '';

      $waitingItem = WaitingList::find($itemId);
      $waitingItem->comment = $comment;
      $waitingItem->save();

      return response()->json(true, 200);
    }

    public function addToWishllist(Request $request)
    {
      if (auth()->guest()) {
        return response()->json(false, 401);
      }

      if (!$request->has('productId')) {
        return response()->json(false, 400);
      }

      $user = auth()->user();
      $productId = $request->post('productId');

      $list = WaitingList::where(['user_id' => $user->id, 'product_id' => $productId]);

      if ($list->count() === 0) {
        WaitingList::create([
          'user_id' => $user->id,
          'product_id' => $productId,
          'comment' => $request->post('comment') ?? null,
        ]);
      }

      return response()->json(json_encode(['first_touch' => $user->first_touch]), 200);
    }

    public function deleteItemsFromWishList(Request $request)
    {
      if (auth()->guest()) {
        return response()->json(false, 401);
      }

      if (!$request->has('ids')) {
        return response()->json(false, 400);
      }

      WaitingList::whereIn('id', $request->post('ids'))->delete();

      return response()->json(true, 200);
    }

    public function deleteFromWishllist(Request $request)
    {
      if (auth()->guest()) {
        return response()->json(false, 401);
      }

      if (!$request->has('productId')) {
        return response()->json(false, 400);
      }

      $user = auth()->user();
      $productId = $request->post('productId');

      WaitingList::where(['user_id' => $user->id, 'product_id' => $productId])->delete();

      return response()->json(true, 200);
    }

    public function subscribe(Request $request)
    {
      if (auth()->guest()) {
        return response()->json(false, 401);
      }

      if (!$request->has('method')) {
        return response()->json(false, 400);
      }

      $user = User::find(auth()->user()->id);
      $user->first_touch = $request->get('method');

      return response()->json(['success' => $user->save()], 200);
    }
}
