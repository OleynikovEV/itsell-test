<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/login';

    public function __construct()
    {
      parent::__construct();

      $this->middleware('guest');
    }

  protected function resetPassword($user, $password)
  {
    $this->setUserPassword($user, $password);

    //$user->setRememberToken(Str::random(60));

    $user->save();

    event(new PasswordReset($user));

    return redirect()->route('login');
    //$this->guard()->login($user);
  }

  protected function setUserPassword($user, $password)
  {
    $user->password = md5(config('auth.salt') . md5($password));
  }
}
