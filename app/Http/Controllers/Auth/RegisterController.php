<?php

namespace App\Http\Controllers\Auth;

use App\Mail\WorkingConditionsMail;
use App\Models\City;
use App\Models\Country;
use App\Models\User;
use App\Events\CartModified;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile?register_success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $NameField = app()->getLocale() == 'ru' ? 'name_ru' : 'name_ua';
        $countries = Country::orderBy($NameField, 'ASC')->get(['code', 'mask', $NameField])->keyBy('code');

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Регистрация'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Регистрация') . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Регистрация') . ' | ITsell ОПТ');

        // Создание отображения
        return view('auth.register', compact('countries', 'NameField', 'breadcrumbs'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'company' => 'required',
            'country' => 'required',
            'city_id' => 'required_if:country,ua',
            'city' => 'required_unless:country,ua',
            'products_type' => 'required',
            'sales_type' => 'required',
            'retail_type' => 'exclude_unless:sales_type,1|required',
            'website_url' => 'exclude_unless:retail_type,1|required',
            'stores_number' => 'exclude_unless:retail_type,2|required',
            'purchase_type' => 'required',
            'how_found' => 'required',
            'how_found_custom' => 'exclude_unless:how_found,custom|required',
            'fio' => 'required',
            'phone' => 'required|phone_number:12,380',
            'email' => 'required|string|email|max:255|unique:its_users',
            'password' => 'required|string|min:8|confirmed',
        ], [
            'company.required' => __('Укажите название Вашей компании'),
            'country.required' => __('Укажите Вашу страну'),
            'city_id.required_if' => __('Укажите Ваш город'),
            'city.required_unless' => __('Укажите Ваш город'),
            'products_type.required' => __('Укажите что Вы продаёте'),
            'sales_type.required' => __('Укажите каким образом Вы продаете товар'),
            'retail_type.required' => __('Укажите Ваш тип розницы'),
            'website_url.required' => __('Укажите адрес Вашего онлайн магазина'),
            'stores_number.required' => __('Укажите количество Ваших физических магазинов'),
            'purchase_type.required' => __('Укажите Ваш способ закупки'),
            'how_found.required' => __('Укажите как Вы нас нашли'),
            'how_found_custom.required' => __('Укажите как Вы нас нашли'),
            'fio.required' => __('Укажите ФИО контактного лица'),
            'phone.required' => __('Введите номер телефона контактного лица'),
            'phone.phone_number' => __('Введите корректный телефон контактного лица'),
            'email.required' => __('Введите Ваш email-адрес'),
            'email.email' => __('Введите корректный email-адрес'),
            'email.unique' => __('Пользователь с указанным email-адресом уже зарегистрирован'),
            'password.required' => __('Введите Ваш пароль'),
            'password.min' => __('Пароль должен содержать не менее 8 символов'),
            'password.confirmed' => __('Введенные пароли не совпадают')
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // Подготовка данных пользователя
        $userData = [
            'email' => $data['email'],
            'login' => $data['email'],
            'password' => md5(config('auth.salt') . md5($data['password'])),
            
            'company' => $data['company'],
            'country' => $data['country'],
            'city' => '',
            'fio' => $data['fio'],
            'phone' => preg_replace('/[^0-9+]/', '', $data['phone']),
            
            'sale_type' => implode('|', $data['products_type']), // TODO: move params into details field
            'how_sell' => implode('|', $data['sales_type']),
            'retail_type' =>isset($data['retail_type']) ? implode('|', $data['retail_type']) : '',
            'wholesale_type' => isset($data['wholesale_type']) ? implode('|', $data['wholesale_type']) : '',
            'additional_info' => $data['additional_info'] ?: '',

            'details' => [
                'how_found' => $data['how_found'] != 'custom' ? $data['how_found'] : $data['how_found_custom'],
                'stores_number' => isset($data['retail_type']) && array_search('physical', $data['retail_type']) !== false 
                    ? $data['stores_number'] : 0,
                'website_url' => isset($data['retail_type']) && array_search('internet', $data['retail_type']) !== false 
                    ? $data['website_url'] : '',
                'purchase_types' => $data['purchase_type']
            ],

            'permissions' => 3,
            'confirmed' => 1,
            'registered' => time(),
            'created_at' => now(),

            'code' => '', // TODO: modify DB columns and set default values
            'basket' => '',
            'logo' => '',
            'unactive' => 0,
            'unactive_weeks' => 0,
            'comment' => '',
            'session_data' => '',
            'debt' => 0,
            'last_payment_date' => '',
            'activation_log' => ''
        ];

        // Получение названия города по его ID
        if (isset($data['city_id']) && $data['city_id'] > 0) {
          $city = City::find($data['city_id']);
          if ($city) $userData['city'] = $city->name_ru;
        } else {
          $userData['city'] = $data['city'];
        }

        // Создание пользователя
        $user = User::create($userData);

        //if ( validator(['email' => $user->email], ['email' => 'email'])->fails() );
      try {
        Mail::to($user->email)->send(new WorkingConditionsMail($user->email));
      } catch (\Exception $e) {

      }

		    return $user;
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        // Если в корзине есть товары - сохраняем их в профиле пользователя
        if (Cart::instance('default')->content()->count()) event(new CartModified);

        // Если задан URL для возврата - редиректим пользователя на него
        if (request('return_url') && substr(request('return_url'), 0, 1) == '/') 
        {
            $this->redirectTo = request('return_url') . '?register_success';
        }

        // Добавление в сессию флага успешной регистрации
        session()->flash('register_success', true);
    }

}