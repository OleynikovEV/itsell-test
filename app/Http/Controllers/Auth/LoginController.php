<?php

namespace App\Http\Controllers\Auth;

use App\Events\CartModified;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest')->except('logout');
    }

    /**
     * Страница авторизации
     */
    public function showLoginForm()
    {
        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Вход'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Вход') . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Вход') . ' | ITsell ОПТ');

        // Создание отображения
        return view('auth.login', compact('breadcrumbs'));
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ], [
            $this->username() . '.required' => __('Введите Ваш email-адрес'),
            'password.required' => __('Введите Ваш пароль')
        ]);
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        // проверяем был ли забанен пользователь
        if ($user->isBanned()) {
          // разлогиниваем пользователя и выводим сообщение
          Auth::guard()->logout();
          return redirect()->route('login')
            ->withErrors([__('Ваша учетная запись заблокирована. Обратитесь к своему менеджеру. <a href="pages/contacts">Контакты</a>')]);
        }

        // Если в текущей корзине есть товары - объединяем их с корзиной из профиля пользователя
        if (Cart::instance('default')->content()->count()) {
            $userDataCart = unserialize($user->cart);
            if ($userDataCart && isset($userDataCart['default']) && $userDataCart['default']->count()) {
                $sessionCart = session('cart');

                foreach ($userDataCart as $cartId => $cart) {
                  foreach ($cart as $rowId => $item) {
                    if (! isset($sessionCart[$cartId])) {
                      $sessionCart[$cartId] = collect();
                    }

                    if ($sessionCart[$cartId]->has($rowId)) {
                      $item->qty += $sessionCart[$cartId]->get($rowId)->qty;
                    }

                    $sessionCart[$cartId]->put($rowId, $item);
                  }
                }

                session(['cart' => $sessionCart]);
            }

            event(new CartModified);
        }

        // Если задан URL для возврата - редиректим пользователя на него
        if (request('return_url') && substr(request('return_url'), 0, 1) == '/') $this->redirectTo = request('return_url');
    }

}
