<?php

namespace App\Http\Controllers;

use App\Events\CartModified;
use App\Http\Models\CartContent;
use App\Models\Order;
use App\Models\Packages;
use App\Models\PriceRange;
use App\Models\PrintCover;
use App\Models\Product;
use App\Models\ProductsSeries;
use App\Models\PromoActions;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Str;

class CartController extends Controller
{
    private $selectedCart = 'default';

    public function __construct()
    {
      parent::__construct();

      $this->middleware(function ($request, $next) {
        $this->selectedCart = getSelectedCart();
        return $next($request);
      });
    }

  /**
     * Отображение страницы корзины
     */
    public function index()
    {
        // Определение списка товаров, требующих упаковки
        $requiredPackages = [];
        $priceList = [];

        // получаем список доступных клиенту типов цен
        if (!auth()->guest()) {
          $priceList = auth()->user()->priceTypeList();
          $cartsList = config('custom.cart_list');
        } else {
          $priceList = [];
          $cartsList = ['default' => __('Основная')];
        }

       // проверяем выбранный из списка тип цен присутствует в доступном диапазоне клиенту
        $priceTypeDefault = getPriceTypeDefault();
        $changePrice = false;

        $cart = new CartContent($priceTypeDefault, $this->selectedCart);
        $cart->setPriceRange((new PriceRange)->priceRangeByType($priceTypeDefault));
        $items = $cart->getCartInstance();
        // Получение информации о товарах в корзине
        $products = $cart->content();

        $updateCart = true;

        foreach ($products as $item) {
            $cart->setItemPrice($item->rowId, $item->getPrice($priceTypeDefault));

            // если акция закончилась и промокод не работает, обнуляем скидку
            if ($item->discountRate > 0 && ($item->getPromoaction() == null || !$item->getPromoaction()->isActive())) {
                $cart->setItemDiscount($item->rowId, 0);
                $updateCart = true;
            }

            // Если товар продается без упаковки и сам не является упаковкой - определяем тип упаковки, который ему требуется
            if ( ! $item->has_package && ! $item->is_package_for && $item->filters_values !== null ) {
              foreach (config('custom.package_types') as $packageTypeId => $packageType) {
                $inArray = array_uintersect($packageType['filters'], $item->filters_values, function($v1, $v2) {
                  return $v1 == $v2;
                });

                if ( isset($item->filters_values) && $inArray ) {
                  $item->need_package = true;
                  if (array_search($packageTypeId, $requiredPackages) === false) $requiredPackages[] = $packageTypeId;
                }
              }
            }
        }

        // обновляем корзину
        if ($updateCart) {
          $cart->updateCart();
        }

        // Получение списка упаковок
        $series_id = $products->pluck('products_series_id'); // получаем список серий с проставленными упаковками
        $series = ProductsSeries::whereIn('id', $series_id)->get()->keyBy('id');

        $OnlyPackagesSelected = $series->every(fn($item) => $item->only_select_packages === 1); // проверяем у серий уникальные упаковки или нет

        $packagesIds = Packages::whereIn('product_series_id', $series_id)->get(['product_id'])->pluck('product_id'); // вытаскиваем все упаковки
        $packages = count($packagesIds) ? Product::active()->whereIn('id', $packagesIds)->get() : collect();

        if (! $OnlyPackagesSelected || $packagesIds->count() === 0) {
          $packages = $packages->merge(
            count($requiredPackages) ? Product::active()->whereIn('is_package_for', $requiredPackages)->get() : collect()
          );
        }

        // Получение последнего заказа текущего пользователя не отмеченного как Дропшиппинг
        if (auth()->check()) {
            $lastOrder = Order::where('user_id', auth()->id())
                ->where('state', '!=', 6)
                ->where('from_cart', $this->selectedCart)
                ->where('dropshipping', false)
                ->orderBy('id', 'DESC')
                ->first();
        }
        else $lastOrder = null;

        // Получение списка товаров со скидкой
        $saleProducts = cache()->remember('main_page_sale_' . app()->getLocale(), $this->dataCacheTime, function() {
            $liquidityGroups = [];
            if (settings('products_so_discount')) $liquidityGroups[] = 2;
            if (settings('products_nl_discount')) $liquidityGroups[] = 3;

            return Product::active()
                ->where('price_modify_time', '>=', today()->subWeek(4)->timestamp)
                ->orWhereIn('liquidity', $liquidityGroups)
                ->inRandomOrder()
                ->limit(20)
                ->with('series')
                ->get();
        });

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__('Корзина'), ''];

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __('Корзина') . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __('Корзина') . ' | ITsell ОПТ');

        $cartDefault = $this->selectedCart;

        // Создание отображения
        return view('cart-new',
          compact('items', 'cart','priceTypeDefault', 'products', 'packages', 'lastOrder',
            'saleProducts', 'breadcrumbs', 'priceList', 'cartsList', 'cartDefault')
        );
    }

  /**
   * представление - клиент делает заказ а корзина пуста
   */
  public function cartEmpty()
  {
    date_default_timezone_set('Europe/Kiev');

    $user = auth()->user();
    $lastOrder = Order::where('user_id', $user->id)->with('products')->latest('time')->first();
    $products = collect();

    if ($lastOrder->products->count() > 0) {
      $productsIds = $lastOrder->products->pluck('product_id')->toArray();
      $products = Product::whereIn('id', $productsIds)->get(['id', 'title'])->keyBy('id');
    }

    // Формирование хлебных крошек
    $breadcrumbs = [];
    $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
    $breadcrumbs[] = [__('Корзина'), ''];

    // Формирование мета-данных
    view()->share('layoutMetaTitle', __('Корзина') . ' | ITsell ОПТ');
    view()->share('layoutMetaDescription', __('Корзина') . ' | ITsell ОПТ');

    return view('cart-empty', compact('lastOrder', 'products'));
  }

    /**
     * Добавление товаров в корзину
     */
    public function store()
    {
        // Получение параметров запроса
        $productsQty = request('products');
        $priceType = 'opt_price';

        // если пользователь авторизирован,
        // получаем его типы цен
        if (! auth()->guest()) {
          $user = auth()->user();
          $priceType = $user->getPriceDefault();
        }

        if ( !$productsQty || !is_array($productsQty) || !count($productsQty)) return response()->json(['success' => false]);

        // Получение информации о товарах
        $products = Product::whereIn('id', array_keys($productsQty))->get()->keyBy('id');

        // Добавление товаров в корзину
        foreach ($productsQty as $productId => $qty) {
            $product = $products->get($productId);
            if ( ! $product) continue;

            $options = [];
            if (isset($product->from) && $product->from == 1) { // если заказ - это чехол принт
              $options = [
                'print' => true,
                'material' => request('material') ?? $product->getMaterial(),
                'fullBorder' => false,
              ];

            }

            Cart::instance($this->selectedCart)->add($product->id, 'product_' . $product->id, $qty, $product->getPrice($priceType, true), 0, $options);
        }

        // Вызов события изменения корзины для ее сохранения профиле пользователя
        event(new CartModified);

        // Возврат успешного статуса
        return $this->successState();
    }

  /**
   * добавляем в корзину принты из конструктора
   */
    public function storeCover()
    {
      $printCover = request('cover', false); // проверяем пришел ли товар из конструктора

      if ($printCover == null || count($printCover) == 0) return response()->json(['success' => false]);

      $priceRange = (new PriceRange)->priceRange();

      $data = request('cover');
      $product = (new PrintCover())->insert(
        $data['uuid'],
        __('Свой дизайн') . ' ' . $data['category'],
        $data['image'],
        1,
        $priceRange[1]['opt_price'] ?? 4.21,
        $data['category_id'],
        $data['material_id'] ?? 'silicone',
      );

      $options = [
        'print' => true,
        'material' => $data['material_id'] ?? 'silicone',
      ];

      // добавляем в корзину
      Cart::instance($this->selectedCart)->add($product->uuid, 'product_' . $product->uuid, $product->qty, $product->getPrice(), 0, $options);

      // Вызов события изменения корзины для ее сохранения профиле пользователя
      event(new CartModified);

      // Возврат успешного статуса
      return $this->successState();
    }

    /**
     * добавить к продукту промокод
     */
    public function updatePromocode()
    {
        $update = false;
        $promocode = request('promocode');

        if ($promocode) {
            $promo = PromoActions::where('code', $promocode);

            if ($promo->count() == 0) { // промокод не существует
                return response()->json(__('промокод не существует'));
            }

            $promo = $promo->first();

            // промокод еще не действует
            if (now()->timestamp < $promo->time_start) {
                return response()->json(__('промокод еще не действует'));
            }

            // срок дкйствия промокода истек
            if (now()->timestamp > $promo->time_end) {
                return response()->json(__('срок дкйствия промокода истек'));
            }

            $cart = Cart::instance($this->selectedCart)->content(); // получаем список из корзины

            // получаем ИД и промокод товара
            $products = Product::find($cart->pluck('id'))->where('promo_code', $promocode)->keyBy('id');

            // если такие товары есть, добавляем им промокод
            if (count($products) > 0) {
                foreach($products as $product) {
                    $cart->search(function($cartItem, $rowId) use ($product, &$update) {
                        // скидка сработает только один раз
                        if ($product->id === $cartItem->id && $cartItem->discountRate == 0) {
                            $update = true;
                          Cart::instance($this->selectedCart)->setDiscount($rowId, $product->promoaction->getSale());
                        }
                    });
                }

                if ($update === true) {
                    // Вызов события изменения корзины для ее сохранения профиле пользователя
                    event(new CartModified);
                    return $this->successState();
                }
                return  response()->json(__('Промокод уже применен ранее'));
            } else {
                return response()->json(__('Нет товаров к которым можно пременить данный промокдо'));
            }
        }

        return response()->json(false);
    }

    // смена цвета
    public function changeColor() {
      $rowId = request('rowId');
      $newId = request('newId');

      Cart::instance($this->selectedCart)->update($rowId, ['id' => $newId]);

      // Вызов события изменения корзины для ее сохранения профиле пользователя
      event(new CartModified);

      // Возврат успешного статуса
      return response()->json([
        'success' => true,
      ]);
    }

  // обновляем материал
  public function updateMaterial()
  {
    $material = request('material');
    $rowId = request('rowId');

    $row = Cart::instance($this->selectedCart)->get($rowId);

    $options = [
      'print' => true,
      'material' => $material ?? 'silicone',
      'fullBorder' => false,
    ];

    Cart::instance($this->selectedCart)->update($rowId, ['options' => $options]);

    event(new CartModified);

    return response()->json([
      'success' => true
    ]);
  }

  /**
   * С закрашенными бортами или нет
   * @return \Illuminate\Http\JsonResponse
   */
  public function setFullBorder()
  {
    $value = request('value');
    $rowId = request('rowId');

    $row = Cart::instance($this->selectedCart)->get($rowId);

    $options = [
      'print' => true,
      'material' => $row->options['material']  ?? 'silicone',
      'fullBorder' => filter_var($value, FILTER_VALIDATE_BOOLEAN),
    ];

    Cart::instance($this->selectedCart)->update($rowId, ['options' => $options]);

    event(new CartModified);

    return response()->json([
      'success' => true
    ]);
  }

    /**
     * Изменение товаров в корзине
     */
    public function update()
    {
        // Получение параметров запроса
        $productsQty = request('products');
        if ( ! $productsQty || ! is_array($productsQty) || ! count($productsQty)) return response()->json(['success' => false]);

        $priceTypeDefault = 'opt_price';

        // проверяем выбранный из списка тип цен присутствует в доступном диапазоне клиенту
        if (request()->cookie('price_type')) {
          $priceTypeDefault = request()->cookie('price_type');
        } elseif (! auth()->guest()) {
          $priceTypeDefault = auth()->user()->getPriceDefault();
        }

        $priceRange = (new PriceRange)->priceRangeByType($priceTypeDefault);
        krsort($priceRange);

        // Обновление данных о товарах
        foreach ($productsQty as $rowId => $qty) {
            // Обновление количества товара
            $qty = (int) $qty;
            if ( ! $qty || $qty < 0) continue;

            // если принт, пересчитываем стоимость по матрице цен
            $row = Cart::instance($this->selectedCart)->get($rowId);
            $price = $newPrice = $row->price;

            if ( isset($row->options['print']) ) {
              foreach ($priceRange as $point => $item) {
                if ($point === $qty) {
                  $newPrice = $item;
                  break;
                }

                if ($qty > $point) {
                  $newPrice = $item;
                  break;
                }
              }
            }

            if ($price !== $newPrice) {
              if (Str::contains($priceTypeDefault, 'uah') || $priceTypeDefault === 'price') {
                $newPrice = $newPrice / settings('dollar');
              }
              Cart::instance($this->selectedCart)->update($rowId, ['price' => $newPrice, 'qty' => $qty]);
            } else {
              Cart::instance($this->selectedCart)->update($rowId, ['qty' => $qty]);
            }
        }

        // Вызов события изменения корзины для ее сохранения профиле пользователя
        event(new CartModified);

        // Возврат успешного статуса
        return $this->successState();
    }

    /**
     * Удаление товара из корзины
     */
    public function destroy($rowId = '')
    {
        if (Cart::instance($this->selectedCart)->content()->has($rowId)) {
          Cart::instance($this->selectedCart)->remove($rowId);
        }

        // Вызов события изменения корзины для ее сохранения профиле пользователя
        event(new CartModified);

        // Возврат успешного статуса
        return $this->successState();
    }

    /**
     * Возврат успешного статуса с базовой информацией о состоянии корзины
     */
    protected function successState()
    {
        return response()->json([
            'success' => true,
            'response' => [
                'count' => Cart::instance($this->selectedCart)->count(),
                'count_sku' => Cart::instance($this->selectedCart)->content()->count(),
                'total' => Cart::instance($this->selectedCart)->total(),
                'items' => Cart::instance($this->selectedCart)->content()->map(function($item)
                {
                    return [
                        'qty' => $item->qty,
                        'promocode' => $item->discount > 0,
                        'price' => calcSale($item->price, $item->discount),
                        'price_uah' => round(calcSale($item->price, $item->discount) * settings('dollar')),
                        'discountRate' => $item->discountRate,
                        'total' => $item->subtotal(),
                        'total_uah' => round($item->price * $item->qty * settings('dollar')),
                    ];
                })
            ]
        ]);
    }

}
