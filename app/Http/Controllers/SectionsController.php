<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Traits\DisplaysProductsList;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SectionsController extends Controller
{

    use DisplaysProductsList;

    /**
     * Отображении секции товаров
     */
    public function index($section = '', $params = '')
    {
        // Если запрошенная секция есть в списке доступных - вызываем ее метод
        $availableSections = [
          'novelties' => [
            'title' => 'Новинки',
            'route' => 'novelties',
            'labels' => 'label bg-in-stock border-radius',
          ],
          'sale'      => [
            'title' => 'Sale',
            'route' => 'sale',
            'labels' => 'label bg-out-stock border-radius',
          ],
          'soon'      => [
            'title' => 'Скоро',
            'route' => 'soon',
            'labels' => 'label bg-soon border-radius',
          ],
          'coming'    => [
            'title' => 'Поступления',
            'route' => 'coming',
            'labels' => 'label bg-soon border-radius',
          ]
        ];

        if (! isset($availableSections[$section])) {
          // Если секция не была найдена - возвращаем 404-ошибку
          abort(404);
        }

        $products = $this->{$section}();

        // Формирование хлебных крошек
        $breadcrumbs = [];
        $breadcrumbs[] = [__('Главная'), custom_route('main_page')];
        $breadcrumbs[] = [__($availableSections[$section]['title']), ''];
        view()->share('breadcrumbs', $breadcrumbs);

        // Формирование мета-данных
        view()->share('layoutMetaTitle', __($availableSections[$section]['title']) . ' | ITsell ОПТ');
        view()->share('layoutMetaDescription', __($availableSections[$section]['title']) . ' | ITsell ОПТ');

        view()->share('h1Text', __($availableSections[$section]['title']));

        $groupBySeries = request()->cookie('groupBySeries') === null || request()->cookie('groupBySeries') == 'true';
        if (Str::contains($params, 'categories') && request()->cookie('groupBySeries') === null) {
          $groupBySeries = false;
        }

        // Отображение стандартного списка товаров
        return $this->displayProductsList(
          $products,
          custom_route('section', [$availableSections[$section]['route']]),
          [
            'banner' => null,
            'categories_filter' => true,
            'categories_filter_active_parent' => null,
            'brands_filter' => true,
            'urlCategory' => removePageNum($params),
            'labelFroGroupClass' => $availableSections[$section]['labels'],
            'labelFroGroupText' => $this->getBrand($params),
          ],
          '', $groupBySeries
        );
    }

    /**
     * новые поступления
     */
    public function coming()
    {
      return Product::active()
        ->where('date_coming', '>', today()->subDays(Product::COMING_DAYS)->format('Y-m-d'));
//        ->orderByRaw('priority DESC, date_coming ASC, arrived_in_stock_at DESC')
//        ->get(config('custom.product_fields'));
    }

    /**
     * Новинки
     */
    public function novelties()
    {
        return Product::active()
            ->where('added_time', '>', today()->subDays(Product::NOVELTY_DAYS)->timestamp)
            ->where('qty', '>', 0)
            ->where('img', '!=', '')
            ->where('from', '!=', 1)
            ->where('title', 'not like', 'Уценка%')
//            ->orderByRaw('priority DESC, arrived_in_stock_at DESC')
            ->limit(1000);
//            ->get(config('custom.product_fields'));
    }

    /**
     * Распродажа
     */
    public function sale()
    {
        // Получение списка товаров в разделе
        $liquidityGroups = [];
        if (settings('products_so_discount')) $liquidityGroups[] = 2;
        if (settings('products_nl_discount')) $liquidityGroups[] = 3;

        return Product::where('parent', 0)
          ->where('active', true)
          ->where('qty', '>', 0)
          ->where(function($q) use ($liquidityGroups)
            {
                $q->where(function($q)
                {
                    $q->whereRaw('old_opt_price > opt_price')
                        ->where('price_modify_time', '>=', today()->subWeek(4)->timestamp);
                })
                //->orWhere('title', 'like', 'Уценка%')
                ->orWhere('markdown', 1)
                ->orWhereIn('liquidity', $liquidityGroups);
            });
//            ->orderByRaw('priority DESC, arrived_in_stock_at DESC, id')
//          ->get(config('custom.product_fields'));
    }

    /**
     * Ожидаемые товары
     */
    public function soon()
    {
       return  Product::active()
            ->where('qty', '<=', 0)
            ->where('when_appear', '!=', '');
//            ->orderBy('id', 'DESC')
//            ->get(config('custom.product_fields'));
    }

    protected function getBrand($params): string
    {
      $before = Str::before($params, '/');
      return ($before === 'filter') ? '' : ucfirst($before);
    }
}
