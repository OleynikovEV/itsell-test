<?php

namespace App\Http\Requests;

class FeedbackRequest extends Request
{

    protected $jsonResponse = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|phone_number',
            'email' => 'required|email',
            'message' => 'required',
            'city' => 'required',
            'purchase_type' => 'required',
            'company' => 'required',
        ];
    }

    /**
     * Get custom validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Введите Ваше имя'),
            'phone.required' => __('Введите Ваш номер телефона'),
            'phone.phone_number' => __('Введите корректный номер телефона'),
            'email.required' => __('Введите Ваш email-адрес'),
            'email.email' => __('Введите корректный email-адрес'),
            'message.required' => __('Введите Ваше сообщение'),
            'city.required' => __('Введите Ваш Город'),
            'purchase_type.required' => __('Введите Ваше способ закупки'),
            'company.required' => __('Введите название Вашей компании'),
        ];
    }

}
