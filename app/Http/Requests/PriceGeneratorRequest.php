<?php

namespace App\Http\Requests;

class PriceGeneratorRequest extends Request
{
    protected $jsonResponse = true;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:1|max:255',
            'format' => 'required|in:xml,csv,yml',
            'category_type' => 'required|in:1,2,3,4,5,6,7',
            'tab' => 'required|in:1,2,3,4,5,6,7',
            'action' => 'required',
//            'id' => 'exclude_if:action,update|required',
        ];
    }

    public function messages()
    {
      $messages = [
          'name.required' => __('Введите название прайса'),
      ];

      return parent::messages();
    }
}
