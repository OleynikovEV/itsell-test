<?php 

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class Request extends FormRequest
{

    protected $jsonResponse = false;

    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $noAjax = $this->input('no_ajax');

        if ($this->jsonResponse && ! $noAjax)
        {
            // Put error messages into one array
            $messages = [];
            foreach ($errors as $error) $messages = array_merge($messages, array_values($error));

            // Return result
            return response()->json([
                'success' => false,
                'messages' => $messages
            ]);
        }
        else
        {
            return parent::response($errors);
        }
    }

    protected function failedValidation(Validator $validator)
    {
        $noAjax = $this->input('no_ajax');

        if ($this->jsonResponse && ! $noAjax)
        {
            // Формирование единого массива со списком ошибок
            $messages = [];
            foreach ($validator->errors()->messages() as $error) $messages = array_merge($messages, array_values($error));

            // Возврат результата
            $response = response()->json([
                'success' => false,
                'messages' => $messages
            ]);

            throw new ValidationException($validator, $response);
        }

        parent::failedValidation($validator);
    }

}