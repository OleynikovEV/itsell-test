<?php

namespace App\Http\Requests;

class OrderRequest extends Request
{

    protected $jsonResponse = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required',
            'firstname' => 'required',
            'phone' => 'required|phone_number:12,380',
            'city_id' => 'required',
            'warehouse_id' => 'exclude_unless:delivery_type_id,1|required',
            'street_id' => 'exclude_unless:delivery_type_id,2|required',
            'street_building' => 'exclude_unless:delivery_type_id,2|required',
            'delivery_comment' => 'exclude_unless:dropshipping,1|exclude_unless:delivery_type_id,3|required',
            'dropshipping_price' => 'exclude_unless:dropshipping,1|exclude_unless:payment_type_id,1|exclude_if:delivery_type_id,3|required',
        ];
    }

    /**
     * Get custom validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'lastname.required' => __('Введите фамилию получателя'),
            'firstname.required' => __('Введите имя получателя'),
            'phone.required' => __('Введите номер телефона получателя'),
            'phone.phone_number' => __('Введите корректный номер телефона получателя'),
            'city_id.required' => __('Выберите город получателя'),
            'warehouse_id.required' => __('Выберите отделение Новой Почты'),
            'street_id.required' => __('Выберите улицу для доставки'),
            'street_building.required' => __('Введите номер дома для доставки'),
            'dropshipping_price.required' => __('Введите сумму для оплаты получателем'),
            'delivery_comment.required' => __('Введите комментарий к доставке'),
        ];
    }

}
