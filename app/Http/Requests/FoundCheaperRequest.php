<?php

namespace App\Http\Requests;

class FoundCheaperRequest extends Request
{

    protected $jsonResponse = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|integer',
            'company' => 'required',
            'email' => 'required',
            'price' => 'required',
            'supplier' => 'required',
        ];
    }

    /**
     * Get custom validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' => __('Произошла непредвиденная ошибка. Обновите страницу для продолжения'),
            'product_id.integer' => __('Произошла непредвиденная ошибка. Обновите страницу для продолжения'),
            'email.required' => __('Введите Ваш email-адрес'),
            'company.required' => __('Введите название Вашей компании'),
            'price.required' => __('Введите цену, по которой Вы нашли товар'),
            'supplier.required' => __('Введите название поставщика'),
        ];
    }

}
