<?php

namespace App\Http\Requests;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:its_users,email,' . auth()->id(),
            'phone' => 'required|phone_number:12,380',
            'fio' => 'required',
            'password' => 'nullable|min:8|confirmed'
        ];
    }

    /**
     * Get custom validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => __('Введите Ваш email-адрес'),
            'email.email' => __('Введите корректный email-адрес'),
            'email.unique' => __('Пользователь с указанным email-адресом уже зарегистрирован'),
            'phone.required' => __('Введите номер телефона контактного лица'),
            'phone.phone_number' => __('Введите корректный телефон контактного лица'),
            'fio.required' => __('Укажите ФИО контактного лица'),
            'password.min' => __('Пароль должен содержать не менее 8 символов'),
            'password.confirmed' => __('Введенные пароли не совпадают')
        ];
    }

}
