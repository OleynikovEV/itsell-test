<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $priceTypeSite = null;
        // если пользователь залогинен
        if (! auth()->guest()) {
          $priceTypeSite = auth()->user()->getPriceSiteDefault();
        }

        $bySeries = $this->bySeries ?? false;
        // Общие данные о товаре
        $data = [
            // проверяем скрыта ли цена. Если цена скрыта, тогда она доступна только для
            // зарегестрированных пользователей
            'id' => $this->id,
            'isPrint' => $this->isPrint(),
            'isAvailability' => $this->isAvailability(),
            'bySeries' => $bySeries,
            'seriesCount' => ($this->seriesCount) ? $this->seriesCount . ' ' . __('СКУ') : 0,
            'show_price' => \Illuminate\Support\Facades\Auth::check() || !$this->show_price,
            'code' => ltrim($this->baseid, '0'),
            'title' => $this->title . ($this->color_name ? ' (' . $this->color_name . ')' : ''),
            'url' => ($this->bySeries === true)
              ? custom_route('products_series', [$this->series->url, $this->urlCategory])
              : custom_route('product', [$this->url, $this->id]),
            'urlSeries' => isset($this->series->url) ? custom_route('products_series', [$this->series->url]) : '',
            'image' => $this->img ? config('custom.content_server') . thumb_name($this->img) : '/template/images/noimage.jpg',
            'price_default_type' => $priceTypeSite,
//            'symbol' => $this->getCurrencyType($priceTypeSite)['symbol'] ?? null,
            'symbol' => '$',
            'showSale' => $priceTypeSite != 'price',
            'price_default' => $this->getPrice($priceTypeSite, true),
            'price_default_type_uah' => round($this->getPrice($priceTypeSite, true) * settings('dollar')),
            'sale' => ($this->old_opt_price > $this->opt_price) ? $this->old_opt_price : 0,
            'prices' => [
                'base' => $this->getPriceRange('all', $bySeries),
                'base_uah' => array_map(function($item) {return round($item * settings('dollar'));}, $this->getPriceRange('all', $bySeries)),
                'price' => $this->getPriceRange('price', $bySeries),
                'drop_uah' => $this->getPriceRange('drop_uah', $bySeries),
                'opt_price' => $this->getPriceRange('opt_price', $bySeries),
                'opt_vip_usd' => $this->getPriceRange('opt_vip_usd', $bySeries),
                'opt_dealer_usd' => $this->getPriceRange('opt_dealer_usd', $bySeries),
                'opt_minopt_usd' => $this->getPriceRange('opt_minopt_usd', $bySeries),
            ],
            'priceColor' => [
              'price' => (!auth()->guest() && auth()->user()->getPriceInContract('price')),
              'drop_uah' => (!auth()->guest() && auth()->user()->getPriceInContract('drop_uah')),
              'opt_price' => (!auth()->guest() && auth()->user()->getPriceInContract('opt_price')),
              'opt_vip_usd' => (!auth()->guest() && auth()->user()->getPriceInContract('opt_vip_usd')),
              'opt_dealer_usd' => (!auth()->guest() && auth()->user()->getPriceInContract('opt_dealer_usd')),
              'opt_minopt_usd' => (!auth()->guest() && auth()->user()->getPriceInContract('opt_minopt_usd')),
            ],
            'old_price' => $this->old_opt_price,
            'group_products' => $this->group_products,
            'series_counter' => $this->series ? $this->series->products_count : 0,
            'label' => $this->getLabelNum(),
            'inWishList' => in_array($this->id, view()->shared('wishList')) ?: 0,
            'when_appear' => $this->qty <= 0 ? substr($this->when_appear, 0, 5) : '',
            'in_cart_qty' => optional(view()->shared('layoutCartProducts'))->get($this->id) ?: 0,
            'box_image' => $this->getBoxImage(),
            'default_title_image' => $this->getDefaultTitleImage(),
        ];

        if ($this->isPrint()) {
          $data['material'] = $this->materialSelect();
          $data['amount_material'] = $this->countMaterial();
        }
        // Формирование полных URL'ов на изображения цветов и сокращение дат ожидания
        if ($data['group_products'])
        {
            foreach ($data['group_products'] as $index => $groupProduct)
            {
                $data['group_products'][$index]->image = $groupProduct->image ? config('custom.content_server')
                    . thumb_name($groupProduct->image) : '/template/images/noimage.jpg';

                $data['group_products'][$index]->inWishList = in_array($groupProduct->id, view()->shared('wishList')) ?: 0;

                if ($groupProduct->when_appear)
                {
                    $data['group_products'][$index]->when_appear = substr($groupProduct->when_appear, 0, 5);
                }

                $data['group_products'][$index]->label = 0;

                if ($this->old_opt_price > $this->opt_price) { // если было понижение цены
                    $data['group_products'][$index]->label = 1;
                } elseif ($groupProduct->qty > 0 && $groupProduct->qty < 10) {
                    $data['group_products'][$index]->label = 2;
                } elseif ($groupProduct->qty <= 0 && $groupProduct->when_appear) {
                    $data['group_products'][$index]->label = 3;
                }

                unset($data['group_products'][$index]->qty);

                $data['group_products'][$index]->opt_range = min_max_price($groupProduct->min_price, $groupProduct->max_price);
                $data['group_products'][$index]->opt_usd = array_map(fn($item) => round($item * settings('dollar')), min_max_price($groupProduct->min_price, $groupProduct->max_price));
            }
        }

        // Добавление лейбла к товару
//        if ($this->isMarkdown()) {
//            $data['label'] = 4;
//        } elseif ($this->isNovelty()) {
//            $data['label'] = 1;
//        } elseif ($this->old_opt_price > $this->opt_price) {
//            $data['label'] = 2;
//        } elseif ($this->qty <= 0 && $this->when_appear) {
//            $data['label'] = 3;
//        }

        return $data;
    }

}
