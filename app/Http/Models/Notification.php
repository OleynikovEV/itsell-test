<?php

namespace App\Http\Models;

class Notification
{
  private $messenger;
  private $chatId;

  public function __construct(Messenger $messenger, $chatId = null)
  {
    $this->messenger = $messenger;
    $this->chatId = $chatId;
  }

  public function setChatId($chatId)
  {
    $this->chatId = $chatId;
    return $this;
  }

  public function sendMessage(string $text)
  {
    if ($this->chatId === null) return false;

    $this->messenger->sendMessage($this->chatId, $text);
  }

  public function sendAvailabilityProducts(array $products)
  {
      $text = $this->messenger->getProductAvailabilityMessage($products);
      $this->sendMessage($text);
  }
}