<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Csv
{
  const ROW_DELIMITER = "\r\n";
  const COL_DELIMITER = ';';

  private string $currency;
  private array $title;

  private array $contents;

  public function __construct(string $currency)
  {
    $this->currency = $currency;

    $this->title = [
      'id', 'available', 'selling_type', 'group_id', 'vendorCode', 'name', 'url', 'price',
      'currencyId', 'categoryId', 'category', 'vendor', 'color',
      'picture_0', 'picture_1', 'picture_2', 'picture_3', 'picture_4', 'picture_5', 'picture_6', 'picture_7',
      'description',
    ];

    $this->contents = [];
  }

  public function setTitle(?array $title = null): void
  {
    if ($title !== null) {
      $this->title = $title;
    }

    $this->addArrayLine($this->title);
  }

  public function insertOffer(array $data): void
  {
    $this->addArrayLine($data);
  }

  public function save(?string $name = null): string
  {
    $this->addStringLine('created: ' . date('d-m-Y H:i'));

    $fileName = $name ?? Str::random();

    $fileName = Storage::disk('price_lists')->path($fileName . '.csv');

    $handle = fopen($fileName,'w+');

    foreach ($this->contents as $item) {
      fputcsv($handle, $item, self::COL_DELIMITER);
    }

    fclose($handle);

    return $fileName;
  }

  private function addStringLine(string $val): void
  {
    $this->contents[] = [$val];
  }

  private function addArrayLine(array $data): void
  {
    $this->contents[] = $data;
  }
}