<?php

namespace App\Http\Models;

use App\Exceptions\Handler;
use App\Models\OtherType;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use App\Http\Models\PriceGeneratorHelper as Helper;
use Illuminate\Support\Str;

class PriceGenerator
{
  private $currency;
  private $price_type;

  /**
   * Получаем список продуктов по menuId
   * @param string $file_type - тип файла
   * @param int $category_type - тип прайса
   * @param int $tab - тип фильтра
   * @param array $ids - ИД для отбора
   * @param string $currency - валюта
   * @param string $price_type
   * @param array $conditions - дополнительные условия отбора товара
   * @return string - имя файла
   */
  static public function generatePrice(string $file_type, int $category_type, int $tab, array $ids, string $currency,
                                       string $price_type, array $conditions, $notAvailable = false, $mainPhoto = false)
  {
    $self = new PriceGenerator();
    $self->setCurrency($currency);
    $self->setPriceType($price_type);
    $result = $self->getProducts($category_type, $self->filterProducts($tab, $ids, $conditions, $notAvailable), $notAvailable, ($tab === 5 || $tab === 7));

    switch ($file_type) {
      case 'xml':
        $categories = $self->getCategories($category_type, $result['categories_ids'], $result['categories_products_types']);
        return $self->saveAsXml($categories, $result['products'], $mainPhoto);
      case 'csv':
        return $self->saveAsCsv($result['products'], $mainPhoto);
    }
  }

  public function setCurrency(string $currency): void
  {
    $this->currency = $currency;
  }

  /**
   * Устанавливаем тип цены
   */
  public function setPriceType($price_type): void
  {
    $this->price_type = $price_type;
  }

    /**
   * Название валюты
   * @return string
   */
  private function getCurrencyName(): string
  {
    return strtoupper($this->currency);
  }

  /**
   * курс
   * @return string
   */
  private function getCurrencyRate(): string
  {
    return '1.0';
  }

  /**
   * Отбираем товары из БД по условиям
   * @param int $tab - по какому типу отбирать продукты
   * @param array $ids
   * @param array $conditions
   * @param bool $notAvailable
   */
  public function filterProducts(int $tab, array $ids, array $conditions, bool $notAvailable)
  {
    $products = null;

    switch ($tab) {
      case 1: // отбираем по категориям
      case 3: // отбираем по брендам
        $products = Helper::getProductsByMenu($ids, true, $conditions, $notAvailable);
        break;
      case 2: // отбираем по сериям
        $products = Helper::getProductsBySeries($ids, true, $conditions, $notAvailable);
        break;
      case 4: // отбираем по бренду
        $products = Helper::getProductsByBrands($ids, true, $conditions, $notAvailable);
        break;
      case 5: // принты iPrint
        $products = Helper::getProductsByiPrint($ids, true, $conditions, $notAvailable);
        break;
      case 6: // гидрогелевые пленки
        $products = Helper::getProductsHydrogel($ids, true, $conditions, $notAvailable);
        break;
      case 7: // выгрузка принтов по тематике
        $products = Helper::getPrintSubjectMatters($ids, true, $conditions, $notAvailable);
        break;
    }

    return $products;
  }

  /**
   * Разносим продукты по категориям
   * @param $type - тип прайса
   * @param $productsQry - список продуктов для прайса
   * @param bool $notAvailable
   * @param bool $noParent
   * @return array
   */
  public function getProducts($type, $productsQry, bool $notAvailable, bool $noParent = false): array
  {
    $productsList = [
      'products' => [],
      'categories_ids' => [],
      'categories_products_types' => [],
    ];

    // Бренды
    $brands = [];

    $brands_list = DB::table('its_brands')->get(['id', 'title']);
    foreach ($brands_list as $brand) {
      $brands[$brand->id] = $brand->title;
    }

    $productsQry->orderBy('id')->chunk(5000, function($products) use ($noParent, $type, &$productsList, $brands, $notAvailable) {
      $categories_ids = [];
      // Получение списка цветов для текущих товаров
      if ($noParent === false) {
        $products_ids = array();
        foreach ($products as $product) $products_ids[] = $product->id;

        if ($notAvailable == false) {
          $products_color = DB::table('its_products')
            ->whereIn('parent', $products_ids)
            ->where('qty', '>', 0)
            ->get(['id', 'parent', 'price_types', 'price', 'opt_price', 'qty', 'colorCode', 'color_name', 'title',
              'img', 'img_1', 'img_2', 'img_3', 'img_4', 'img_5', 'img_6', 'img_7'])->toArray();
        } else {
          $products_color = DB::table('its_products')
            ->whereIn('parent', $products_ids)
            ->where('from', 0)
            ->where('when_appear', '=', '')
            ->where(function ($queryParent) {
              $queryParent->where(function ($query) {
                $query->where('qty', '>', 0);
              })->orWhere(function ($query2) {
                $query2->where('qty', 0)
                  ->whereBetween('last_modified', [today()->subDay(1)->timestamp, today()->timestamp]);
              });
            })
            ->get(['id', 'parent', 'price_types', 'price', 'opt_price', 'qty', 'colorCode', 'color_name', 'title',
              'img', 'img_1', 'img_2', 'img_3', 'img_4', 'img_5', 'img_6', 'img_7'])->toArray();
        }

        $products_colors = [];
        foreach ($products_color as $product) {
          $id = $product->parent;
          if (!isset($products_colors[$id])) {
            $products_colors[$id] = [];
          }
          $products_colors[$id][] = $product;
        }
      }

    // Список исключенных из прайс-листа категорий
    $excluded_categories = [237];

    // Получение списка категорий
    $products_categories = [];
    $general_categories = [];
    $categoriesModel = DB::table('its_menu')->get()->toArray();

    foreach ($categoriesModel as $category) {
      $products_categories[$category->id] = $category;
      if ($category->parent_id == 110) $general_categories[] = $category->id;
      if ($category->parent_id == 237) $excluded_categories[] = $category->id;
    }

    // Обработка товаров
    foreach ($products as $product) {
      // Определение типа товара по фильтрам
      $product_type = 0;
      $filters_values = explode(',', $product->filters_values);
      if (array_search(256, $filters_values) !== false) $product_type = 1; // чехлы
      if (array_search(257, $filters_values) !== false) $product_type = 2; // Защитные стекла
      if (array_search(258, $filters_values) !== false) $product_type = 2; // Защитные пленки
      if (array_search(338, $filters_values) !== false) $product_type = 3; // техничка
      if (array_search(347, $filters_values) !== false) $product_type = 3; // техничка
      if (array_search(397, $filters_values) !== false) $product_type = 3; // техничка

      // Если у товара не задан фильтр по типу - проверяем его категорию на принадлежность к общим аксессуарам
      if ( ! $product_type && array_search($product->menu, $general_categories) !== false) $product_type = 3;

      // Если у товара не задан фильтр по типу, но заполнено старое поле с типом товара - получаем тип из него
      if ( ! $product_type && $product->product_type) $product_type = [1 => 1, 2 => 1, 3 => 2, 4 => 3, 5 => 1][$product->product_type];

      if (array_search($product->menu, $excluded_categories) !== false || ! $product_type) continue;

//      if (($type == 2 || $type == 3) && $product_type == 3 && $product->product_subtype) {
      if (($type == 2 || $type == 3) && $product_type == 3) {
        $category = $product->product_subtype + 10;
      } elseif ($type == 3) {
        if (isset($products_categories[$product->menu]) && $products_categories[$product->menu]->parent_id) {
          $parent_category_id = $products_categories[$product->menu]->parent_id;
          $category = $parent_category_id * 100 + $product_type;
          if (array_search($parent_category_id, $categories_ids) === false) $categories_ids[] = $parent_category_id;
        } else continue;
      } else {
        $category = $product->menu * 100 + $product_type;
        if (array_search($product->menu, $categories_ids) === false) $categories_ids[] = $product->menu;
      }

      if ( ! isset($productsList['categories_products_types'][$product->menu])) $productsList['categories_products_types'][$product->menu] = [];

      if (array_search($product_type, $productsList['categories_products_types'][$product->menu]) === false) {
        $productsList['categories_products_types'][$product->menu][] = $product_type;
      }

      // Добавление в список товара или его цветов
      if ($noParent === false) {
        $list = isset($products_colors[$product->id]) ? $products_colors[$product->id] : [];
      } else {
        $list = [$product];
      }

      foreach ($list as $list_item) {
        // Получение типа цены, заданного в настройках текущего файла
        $list_item->price_types = json_decode($list_item->price_types);

        if (is_array($this->price_type)) {
          $price = [];
          foreach ($this->price_type as $item) {
            $price[$item] = $list_item->price_types->{$item} ?? $list_item->{$item} ?? 0;
          }
        } else {
          $price = $list_item->price_types->{$this->price_type} ?? $list_item->{$this->price_type} ?? 0;
        }

        // Формирование данных товара
        $id = $list_item->colorCode ?? $list_item->baseid;
        $productsList['products'][$id] = [
          'id' => $id,
          'available' => ($product->active = 1 && $list_item->qty > 0) ? 'true' : 'false',
          'selling_type' => 'r',
          'group_id' => ($noParent === false) ? $product->id : $product->products_series_id,
          'vendorCode' => ($noParent === false) ? $this->getVendorCode($product->baseid) : $product->brand,
          'vendor' => isset($brands[$product->brand]) ? strip_tags($brands[$product->brand]) : '',
          'name' => strip_tags($product->title),
          'url' => Url::to('/products/' . $product->url . '/' . $list_item->id),
          'price' => $price, // зависит от выбранного типа цен
          'currencyId' => $this->getCurrencyName(), // зависит от выбранного типа цен
          'categoryId' => $category,
          'category' => isset($products_categories[$product->menu]) ? $products_categories[$product->menu]->title : '',
          'color' => $list_item->color_name,
          'picture' => [],
          'description' => $this->getDescription(
            $product->full_description,
            $product->description_for_client,
            $product->date_of_uploading,
            $list_item->title,
            $list_item->color_name
          ),
        ];

        // добавляем картинки
        $productsList['products'][$id]['main_photo'] = Url::to('/uploads/' . $product->img);
        for ($j = 0; $j <= 7; $j++) {
          $image_field = 'img' . ($j ? '_' . $j : '');
          if ($list_item->{$image_field}) {
            $productsList['products'][$id]['picture'][] = Url::to('/uploads/' . $list_item->{$image_field});
          }
        }
      }
    }

    $productsList['categories_ids'] = array_merge($productsList['categories_ids'], $categories_ids);
    });

    return $productsList;
  }

  /**
   * Возвращает описание для клиентов
   * @param $product
   * @param $list_item
   * @return string
   */
  private function getDescription($fullDescription, $descriptionForClient, $dateOfUploading, $title, $colorName): string
  {
    $description_for_client = '';
    if (strlen(trim($fullDescription)) > 0 && $dateOfUploading != null && strtotime(date('Y-m-d')) >= strtotime($dateOfUploading)) {
      if (strpos($fullDescription, 'http') === false) {
        $description_for_client = str_replace('/uploads/', 'https://itsellopt.com.ua/uploads/', $fullDescription);
      } else {
        $description_for_client = $fullDescription;
      }
    } else {
      $description_for_client = strlen(trim($descriptionForClient)) > 0
        ? strip_tags($descriptionForClient, '<p><b><strong><ul><ol><li><br>')
        : '<p>' . $title . ($colorName ? ' (' . $colorName . ')' : '') . '</p>';
    }

    return $description_for_client;
  }

  /**
   * @param int $type - тип структуры категорий (1, 2, 3)
   * @param array $categories_ids -
   * @param array $categories_products_types -
   * @return array
   */
  public function getCategories(int $type, array $categories_ids, array $categories_products_types): array
  {
    //if ($type > 3) return [];

    // Формирование списка типов товаров
    $products_types = [
      1 => 'Чехлы',
      2 => 'Защита экрана',
      3 => 'Технические и общие товары'
    ];

    $categories = [];
    $parent_categories = null;
    $categories_list = null;

    if ($type == 1 || $type == 2) {
      // Получение списка категорий, товары их которых были добавлены в прайс
      $categories_list = Category::whereIn('id', $categories_ids)->orderBy('parent_id', 'ASC')->get(['id', 'parent_id', 'title']);

      // Получение списка родительских категорий
      $parent_categories_ids = [];
      foreach ($categories_list as $item) {
        if (array_search($item->parent_id, $parent_categories_ids) === false) $parent_categories_ids[] = $item->parent_id;
      }

      $parent_categories = Category::whereIn('id', $parent_categories_ids)->get(['id', 'parent_id', 'title']);
    } elseif ($type == 3) {
      // Получение списка родительских категорий
      $parent_categories = Category::whereIn('id', $categories_ids)->orderby('parent_id', 'ASC')->get(['id', 'parent_id', 'title']);
    }

    // Добавление структуры категорий в зависимости от указанного типа прайс-листа
    if ($type == 1) {
      // Добавление в прайс-лист родительских категорий
      if ($parent_categories->count() > 0) {
        foreach ($parent_categories as $item) {
          $id = $item->id * 100;
          $categories[$id] = $this->addToCategory($id, $item->title);
        }
      }

      // Добавление в список категорий и типов товаров в них
      foreach ($categories_list as $item) {
        $id = $item->id * 100;
        if ($item->parent_id) {
          $parentId = $item->parent_id * 100;
          $categories[$id] = $this->addToCategory($id, $item->title, $parentId);
        } else {
          $categories[$id] = $this->addToCategory($id, $item->title);
        }

        if (isset($categories_products_types[$item->id])) {
          foreach ($categories_products_types[$item->id] as $products_type_id) {
            if (isset($products_types[$products_type_id])) {
              $id = $item->id * 100 + $products_type_id;
              $parentId = $item->id * 100;
              $categories[$id] = $this->addToCategory($id, $products_types[$products_type_id], $parentId);
            }
          }
        }
      }
    } elseif ($type == 2 || $type == 3) {

      foreach ($products_types as $index => $products_type) {
        $categories[$index] = $this->addToCategory($index, $products_type);

        if ($index <= 2) {
          // Добавление в список родительских категорий
          if ($parent_categories->count() > 0) {
            foreach ($parent_categories->all() as $item) {
              $id = $item->id * 100 + $index;
              $parentId = $index;
              $categories[$id] = $this->addToCategory($id, $item->title, $parentId);
            }
          }

          // Добавление в список категорий
          if ($categories_list != null && $categories_list->count() > 0) {
            foreach ($categories_list->all() as $item) {
//              if (isset($categories_products_types[$item->id]) && array_search($index, $categories_products_types[$item->id]) !== false) {
                if ($item->parent_id) {
                  $id = $item->id * 100 + $index;
                  $parentId = $item->parent_id * 100 + $index;
                  $categories[$id] = $this->addToCategory($id, $item->title, $parentId);
                }
//              }
            }
          }
        }
      }

      $general_categories = OtherType::all();
      foreach ($general_categories as $general_category) {
        $id = $general_category->id + 10;
        $categories[$id] = $this->addToCategory($id,  $general_category->name, 3);
      }
    }

    return $categories;
  }

  /**
   * Добавить элемент в категории
   * @param int $id
   * @param string $title
   * @param int|null $parentId
   * @return array
   */
  private function addToCategory(int $id, string $title, ?int $parentId = 0): array
  {
    if ($parentId > 0) {
      $element = [
        'id' => $id,
        'category' => $title,
        'parentId' => $parentId,
      ];
    } else {
      $element = [
        'id' => $id,
        'category' => $title,
      ];
    }

    return $element;
  }

  /**
   * Сохранить данные как xml
   * @param array $categoriesList
   * @param array $products
   * @return string file name
   */
  public function saveAsXml(array $categoriesList, array $products, $maniPhoto = false, string $storage = 'public', ?string $name = null): string
  {
    // Корневой элемент
    $xml = new SimpleXMLExtended('<?xml version="1.0" encoding="utf-8"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd"><yml_catalog/>');
    $xml->addAttribute('date', date('Y-m-d H:i'));

    // Информация о магазине
    $shop = $xml->addChild('shop');
    $shop->addChild('name', 'ITsell Опт');
    $shop->addChild('company', 'ITsell Опт');
    $shop->addChild('url', URL::to('/'));
    $shop->addChild('email', 'partner@itsellopt.com.ua');

    // Валюта магазина
    $currencies = $shop->addChild('currencies');
    $currency = $currencies->addChild('currency');
    $currency->addAttribute('id', $this->getCurrencyName());
    $currency->addAttribute('rate', $this->getCurrencyRate());

    // Плейсхолдер для списка категории
    $categories = $shop->addChild('categories');
    // Товары
    $offers = $shop->addChild('offers');

    // добавляем категории в файл
    foreach ($categoriesList as $item) {
      $category = $categories->addChild('category', str_replace('&', ' ', $item['category']));
      $category->addAttribute('id', $item['id']);
      if (isset($item['parentId'])) {
        $category->addAttribute('parent_id', $item['parentId']);
        $category->addAttribute('parentId', $item['parentId']);
      }
    }

    $mainImage = null;

    // добавляем товары в файл
    foreach ($products as $item) {
      $offer = $offers->addChild('offer');
      $offer->addAttribute('id', $item['id']);
      $offer->addAttribute('available', $item['available']);
      $offer->addAttribute('selling_type', $item['selling_type']);
      $offer->addAttribute('group_id', $item['group_id']);

      $offer->addChild('vendorCode', $item['vendorCode']);
      $offer->addChild('name', htmlspecialchars($item['name']));
      $offer->addChild('url', $item['url']);

      // если передается несколько типов цен
      if (is_array($item['price'])) {
        foreach ($item['price'] as $key => $val) {
          $price = $offer->addChild('price', $val);
          $price->addAttribute('name', $key);
        }
      } else {
        $offer->addChild('price', $item['price']);
      }

      $offer->addChild('currencyId', $item['currencyId']);
      $offer->addChild('categoryId', $item['categoryId']);
      $offer->addChild('category', str_replace('&', ' ', $item['category']));
      $offer->addChild('vendor', htmlspecialchars($item['vendor']));
      $param = $offer->addChild('param', htmlspecialchars($item['color']));
      $param->addAttribute('name',  'Цвет');

      if ($maniPhoto && $mainImage !== $item['main_photo']) {
        $main = $offer->addChild('picture', $item['main_photo']);
        $main->addAttribute('main_photo', true);
        $mainImage = $item['main_photo'];
      }

      for ($i = 0; $i <= 7; $i++) {
        if (isset($item['picture'][$i])) {
          $offer->addChild('picture', $item['picture'][$i]);
        }
      }
      $offer->addCData('description', $item['description']);
    }

    $fileName = $name ?? Str::random();

    $fileName = Storage::disk($storage)->path($fileName . '.xml');
    $xml->asXML($fileName);

    return $fileName;
  }

  /**
   * Сохраняем в csv формате
   * @param array $products
   * @param string $col_delimiter
   * @param string $row_delimiter
   * @return string - имя файла
   */
  public function saveAsCsv(array $products, $maniPhoto = false, $col_delimiter = ';', $row_delimiter = "\r\n"): string
  {
    // строка, которая будет записана в csv файл
    $CSV_str = '';

    $title = [
      'id', 'available', 'selling_type', 'group_id', 'vendorCode', 'vendor', 'name', 'url', 'price',
      'currencyId', 'categoryId', 'category', 'color',
      'picture_0',
      'picture_1', 'picture_2', 'picture_3', 'picture_4', 'picture_5', 'picture_6', 'picture_7',
      'description', 'main_photo'
    ];

    array_unshift($products, $title);

    // перебираем все данные
    foreach($products as $row) {
      $cols = [];

      foreach($row as $key => $col_val) {
        if (gettype($col_val) != 'array') {
          if ($key == 'description') {
            $col_val = str_replace("\n", "", $col_val);
          }
          $cols[] = $this->csvHelper($col_val, $row_delimiter); // добавляем колонку в данные
        } else {
          if ($key == 'picture') {
            for ($i=0; $i <=7; $i++) {
              $cols[] = (isset($col_val[$i])) ? $col_val[$i] : '';
            }
          } else {
            foreach ($col_val as $item) {
              $cols[] = $this->csvHelper($item, $row_delimiter); // добавляем колонку в данные
            }
          }
        }
      }

      $CSV_str .= implode( $col_delimiter, $cols ) . $row_delimiter; // добавляем строку в данные
    }

    $CSV_str = rtrim( $CSV_str, $row_delimiter );
    $CSV_str .= $row_delimiter . 'created: ' . date('d-m-Y H:i');
    $CSV_str .= $row_delimiter . 'exchange rate: ' . $this->getCurrencyRate();

    $fileName = Storage::disk('public')->path(Str::random() . '.csv');
    file_put_contents($fileName, $CSV_str);

    return $fileName;
  }

  private function csvHelper($val, $row_delimiter): string
  {
    // строки должны быть в кавычках ""
    // кавычки " внутри строк нужно предварить такой же кавычкой "
    if ($val && preg_match('/[",;\r\n]/', $val)) {
      // поправим перенос строки
      if ($row_delimiter === "\r\n") {
        $val = str_replace("\r\n", '\n', $val);
        $val = str_replace("\r", '', $val);
      } elseif ($row_delimiter === "\n") {
        $val = str_replace("\n", '\r', $val);
        $val = str_replace("\r\r", '\r', $val);
      }

      return str_replace('"', '""', str_replace(';', ',', $val)); // предваряем "
    }

    return str_replace(';', ',', $val);
  }

  private function getVendorCode(string $baseid): string
  {
    return ltrim(Str::before($baseid, '_'), '0');
  }
}
