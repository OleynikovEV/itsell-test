<?php

namespace App\Http\Models;

use App\Models\FiltersValue;
use App\Models\PriceRange;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductsDescription;
use App\Models\ProductsSeries;
use App\Models\SubjectMatter;
use App\Models\VchehleCategories;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImportPrintPrice
{
  private $priceUrl   = '';
  private $currencies = null;
  private $categories = null;
  private $offers     = null;
  private $filtersSubject = null;
  private $productsSeries = null;
  private $debugMode  = true;
  private $debugStartTime = 0;
  private $storage = null;
  private $storagePath = '';
  private $subjectMatter = null;
  private $newSubjectMatter = null;

  private $chunk = 1000;

  private $brandId = 323;
  private $filter_id = 115;
  private $menu_id = 2111;
  private $case_features_id = 438;

  private $price_range = [];
  private $priceMin = 999;
  private $priceMax = 0;

  /**
   * ImportPrintPrice constructor.
   * @param string $priceUrl
   * @param bool $debugMode
   * @param int $chunk
   * @throws \Exception
   */
  public function __construct(string $priceUrl, bool $debugMode = false, ?int $chunk = null)
  {
    $this->priceUrl = $priceUrl;
    $this->debugMode = $debugMode;
    $this->chunk = $chunk ?? $this->chunk;

    // загружаем прайс
    $this->debugMessage('Load price...');
    if (($data = $this->loadPrice($this->priceUrl)) === false) {
      $this->logError('loadPrice', 'Can`t load price');
    }

    // настройка хранилища
    $this->storage = Storage::disk('content');
//    $this->storage = Storage::disk('local');
    $this->storagePath = storage_path('app/public/');

    // парсим прайс на элементы
    $this->debugMessage('Parse xml...');
    $this->parseXml($data);

    $this->debugMessage('Load filters dat...');
    $this->getFiltersSubject();
    $this->debugMessage('Load products series...');
    $this->getProductsSeries();
    $this->debugMessage('Load the price matrix...');
    $this->getPriceRange();
    $this->debugMessage('Load the price subject matter...');
    $this->subjectMatter = SubjectMatter::get()->keyBy('name');
    $this->newSubjectMatter = collect();

    $this->debugMessage('Convert categories...');
    $this->convertCategories();
//    $this->debugMessage('Convert offers...');
//    $this->convertOffers();
  }

  /**
   * обновляем товары
   */
  public function update(): void
  {
    $this->updateProducts(); // обновляем прайс
    $this->nullify(); // обнуляем все принты

    // сохраняем дополнительные категории
//    $this->updateAdditionalCategories();

    // удаляем доп категории если товар не активен
//    $this->clearAdditionalCategories();
  }

  private function updateAdditionalCategories()
  {
    $this->debugMessage('start update additional categories');
     Product::select(['id', 'addit_menus'])
       ->where('from', 1)
       ->where('qty', '>', 0)
       ->where('active', true)
       ->chunk($this->chunk, function($products) {
       $this->debugMessage('... chunk  update additional categories');
        $data = [];

        foreach ($products as $product) {
          $addit_menus = explode(',', $product->addit_menus);

          if (count($addit_menus) === 0) continue;

          foreach ($addit_menus as $menu) {
            $data[] = [
              'product_id' => $product->id,
              'category_id' => $menu,
            ];
          }
        }

        $this->debugMessage('... chunk clear additional categories');
        DB::table('its_product_category')->whereIn('product_id', $products->pluck('id'))->delete();
        $this->debugMessage('... chunk insert additional categories');
        DB::table('its_product_category')->insert($data);
    });
    $this->debugMessage('finish update additional categories');
  }

  private function clearAdditionalCategories()
  {
      $this->debugMessage('start clear additional categories');
      DB::statement('DELETE FROM its_product_category WHERE product_id IN (SELECT id FROM its_products WHERE `from` = 1 AND qty = 0);');
      $this->debugMessage('finish clear additional categories');
  }

  /**
   * обнуляем все принты
   */
  private function nullify()
  {
    Product::where('from', 1)
      ->where('last_modified', '<', Carbon::now()->subHours(1)->timestamp)
      ->update([
        'qty' => 0,
        'active' => 0,
      ]);
  }

  /**
   * получаем матрицу цен
   */
  private function getPriceRange()
  {
    $price = new PriceRange;

    $this->price_range = $price->priceRange();
    $this->priceMin = $price->priceMin;
    $this->priceMax = $price->priceMax;
  }

  /**
   * Обновляем список продуктов
   */
  private function updateProducts(): void
  {
    if ($this->offers === null) return;

    $chunks = 0;

    foreach ($this->offers->chunk($this->chunk) as $productsChunk) {
      $this->debugMessage('Chunk #' . (++$chunks) . ' products obtained...');

      $this->debugMessage('Convert offers...');
      $productsChunk = $this->convertOffers($productsChunk);

      $updateProducts = collect();
      $createdProducts = collect();

      // Получение информации о существующих товарах из БД
      $productsCodes = $productsChunk->pluck('id')->toArray();

      $productsData = Product::where('from', 1)->whereIn('code', $productsCodes)
        ->get(['id', 'code', 'temporary_photo', 'img', 'title', 'products_series_id', 'price_modify_time'])
        ->keyBy('code');

      // Обработка данных из файла
      foreach ($productsChunk as $product) {
        // Если товар не найден - создаем его, иначе - обновляем данные
        if (! $productsData->has($product['id'])) {
          $data = $this->getProductData($product);
          $data['price_types'] = json_encode($data['price_types']);
          $data['filters_values'] = implode(',', $data['filters_values']);
          $data['group_products'] = json_encode($data['group_products']);

          $createdProducts->push($data);
        }
        else {
          $productData = $productsData->get($product['id']);

          // если картинка изменилась, обновляем ее
          if ($productData->temporary_photo != $product['picture']) {
            $this->deleteImage($productData->img);
            $imageName = $this->importImages($product['picture']);
          } else {
            $imageName = $productData->img;
          }

          // если изменилось название товара
          if ($productData->title != $product['title']) {
             $products_series_id = $this->getProductSeriesId($product['title']);
          } else {
            $products_series_id = $productData->products_series_id;
          }

          // если изменилась цена
          $price_modify_time = $productData->price_modify_time;
          $old_price = 0;
          $old_opt_price = 0;
//          if ($productData->price > $product['price']) { // цена снижена метка sale
//            $price_modify_time = time();
//            $old_price = $productData->price;
//            $old_opt_price = $productData->opt_price;
//          } elseif ($productData->price < $product['price']) { // цену подняли
//            $price_modify_time = 0;
//            $old_price = 0;
//            $old_opt_price = 0;
//          } else {
//            $price_modify_time = $productData->price_modify_time;
//            $old_price = $productData->old_price;
//            $old_opt_price = $productData->old_opt_price;
//          }
          $data = [
            'id' => $productData->id,
            'title' => $product['title'],
            'last_modified' => time(),
            'temporary_photo' => $product['picture'],
            'img' => $imageName,
            'color' => 'fff',
            'color_name' => $this->getColorName($product['params']),
            'qty' => $product['qty'],
            'active' => 1,
            'group_products' => [],
            'products_series_id' => $products_series_id,
            'filters_values' => $this->setOfferParams($product['params'], $product['materials']),
            'menu' => $product['category_id'],
            'addit_menus' => implode(',', $this->addItMenu($product)),
            'price_lists_review' => $product['description'],
            'price_modify_time' => $price_modify_time,
            'old_price' => $old_price,
            'old_opt_price' => $old_opt_price,
            'price' => $this->price_range[1]['price'],
            'opt_price' => $this->price_range[1]['opt_price'],
            'price_types' => [
              'opt_vip_usd_max' => $this->price_range[1]['opt_vip_usd'],
              'opt_vip_usd' => $this->price_range[1]['opt_vip_usd'],
              'opt_dealer_usd_max' => $this->price_range[1]['opt_dealer_usd'],
              'opt_dealer_usd' => $this->price_range[1]['opt_dealer_usd'],
              'opt_minopt_usd_max' => $this->price_range[1]['opt_minopt_usd'],
              'opt_minopt_usd' => $this->price_range[1]['opt_minopt_usd'],
              'drop_uah_max' => $this->price_range[1]['drop_uah'],
              'drop_uah' => $this->price_range[1]['drop_uah'],
              'min_price' => $this->priceMin,
              'max_price' => $this->priceMax,
            ],
          ];

          $data['group_products'] = $this->getGroupProducts($data, $product);

          $productData->fill($data);
          $updateProducts->put($product['id'], $productData);
        }
      }

      if ($createdProducts->count() > 0) {
        Product::insert($createdProducts->toArray());
      }

      if ($updateProducts->count() > 0) {
        DB::transaction(fn() => $updateProducts->each->save());
      }

      if ($this->newSubjectMatter->count() > 0) {
        DB::transaction(fn() => $this->newSubjectMatter->each->save());
      }
      // Применение шаблонов описаний к только что созданным товарам
//      $this->applyDescriptionTemplates($createdProducts);
    }
  }

  /**
   * загрузка прайса по ссылке
   * @param $priceUrl
   * @return false|string
   */
  private function loadPrice($priceUrl)
  {
    return file_get_contents($priceUrl);
  }

  /**
   * Парсим xml
   * @param string $data
   * @throws \Exception
   */
  private function parseXml(string $data): void
  {
    $xml = null;
    try {
      $xml = new \SimpleXMLElement($data);
    } catch (\Exception $ex) {
      $this->logError('parseXml', $ex->getMessage());
    }

    if ($xml->count() === 0 || !isset($xml->shop)) {
      $this->logError('parseXml', 'Xml is empty');
    }

    if (isset($xml->shop->currencies->currency) && $xml->shop->currencies->currency->count() > 0) {
      $this->currencies = $xml->shop->currencies->currency;
    } else {
      $this->logError('parseXml', 'Currencies are empty');
    }

    if (isset($xml->shop->categories->category) && $xml->shop->categories->category->count() > 0) {
      $this->categories = $xml->shop->categories->category;
    } else {
      $this->logError('parseXml', 'Categories are empty');
    }

    if (isset($xml->shop->offers->offer) && $xml->shop->offers->offer->count() > 0) {
      $this->offers = collect();

      foreach ($xml->shop->offers->offer as $offer) {
        $this->offers->push($offer);
      }
    } else {
      $this->logError('parseXml', 'Offers are empty');
    }
  }

  /**
   * Преобразуем категории из xml в массив
   * @throws \Exception
   */
  private function convertCategories(): void
  {
    if ($this->categories === null) {
      $this->logError('convertCategories', 'Categories are empty');
    }

    $categories = VchehleCategories::with('category')->get()->keyBy('vchehle_id')->toArray();
    $this->categories = $categories;
  }

  /**
   * преобразование offers
   * @throws \Exception
   */
  private function convertOffers($offers)
  {
//    if ($this->offers === null) {
    if ($offers === null) {
      $this->logError('convertCategories', 'Offers are empty');
    }

    $products = collect();

//    foreach ($this->offers as $offer) {
    foreach ($offers as $offer) {
      $attributes = $offer->attributes();

      if (!isset($attributes->id)) continue;

      $id = (int)$attributes->id;

      // параметры
      $params = [];
      foreach ($offer->param as $param) {
        $attributes = $param->attributes();

        $params[] = [
            'name' => Str::lower($attributes->name),
            'value' => Str::lower((string)$param),
        ];
      }

      $qty = 0; // количество
      // материалы
      $materials = [];
      foreach ($offer->material as $material) {
        $attributes = $material->attributes();
        $materials[(string)$attributes->id] = [
          'qty' => (int)$attributes->qty,
          'name_ru' => (string)$material,
        ];
        $qty += (int)$attributes->qty;
      }

      $title = (string)$offer->name;
      $products_series_id = $this->getProductSeriesId($title);
      $categoryId = (int)$offer->categoryId;
      $menu = $this->getCategoryId($categoryId);

      $productId = $products_series_id . $menu; // так как объединяем чехлы по моделям, чтоб не дублировались
      // Например у нас 11 и 11+ - это одна группа а вчехле это разные. Этот код объединяет их и не дает дубли
//      $productId = $id;
      $data = [
        'id' => $id,
        'title' => $this->getTitle($title, $categoryId),
        'price' => (float)$offer->price,
        'vendor' => (string)$offer->vendor,
        'picture' => (string)$offer->picture, // ссылка изначального фото
        'url' => Str::slug((string)$offer->name),
        'category_id' => $menu,
        'products_series_id' => $products_series_id,
        'qty' => $qty,
        'description' => (string)$offer->description,
        'params' => $params,
        'materials' => $materials,
      ];

      if (!$products->has($productId)) $products->put($productId, $data);
    }
//    $this->offers = $products;
    return $products;
  }

  /**
   * записываем дополнительные параметры в товар
   * @param array $params
   * @return array
   */
  private function setOfferParams(array $params, $materials): array
  {
    $filters_values = [];
    foreach ($params as $param) {
      switch ($param['name']) {
        case 'категория': array_push($filters_values, 256); break; // чехлы
        case 'форм-фактор': array_push($filters_values, $this->getFormFactorId($param['value'])); break;
        case 'тип аксессуара': array_push($filters_values, $this->getAccessoryTypeId($param['value'])); break;
        //case 'материал': array_push($filters_values, $this->getMaterialId($param['value'])); break;
        case 'особенности чехла': array_push($filters_values, $this->getCaseFeaturesId($param['value'])); break;
        case 'тематика': array_push($filters_values, $this->getSubjectId($param['value'])); break;
      }
    }

    foreach ($materials as $material) {
      array_push($filters_values, $this->getMaterialId(Str::lower($material['name_ru'])));
    }

    return $filters_values;
  }

  /**
   * @param $product
   * @return array
   */
  private function getProductData($product): array
  {
    $image = $this->importImages($product['picture']);
    $colorName = $this->getColorName($product['params']);

    $data = [
      'baseid' => $product['id'],
      'code' => $product['id'],
      'brand' => $this->getVendorId(),
      'title' => $product['title'],
      'added_time' => time(),
      'last_modified' => time(),
      'temp_photos' => 0,
      'temporary_photo' => $product['picture'], // ссылка изначального фото
      'img' => $image,
      'url' => $product['url'] . '_' . $product['id'],
      'qty' => $product['qty'],
      'qty_today' => $product['qty'],
      'qty_with_delay' => 0,
      'active' => 1,
      'product_type' => 1, //Тип товара чехол
      'type' => 1,
      'parent' => 0,
      'liquidity' => 0,
      'color_name' => $colorName,
      'color' => 'fff',
      'price' => $this->price_range[1]['price'],
      'opt_price' => $this->price_range[1]['opt_price'],
      'price_types' => [
        'opt_vip_usd_max' => $this->price_range[1]['opt_vip_usd'],
        'opt_vip_usd' => $this->price_range[1]['opt_vip_usd'],
        'opt_dealer_usd_max' => $this->price_range[1]['opt_dealer_usd'],
        'opt_dealer_usd' => $this->price_range[1]['opt_dealer_usd'],
        'opt_minopt_usd_max' => $this->price_range[1]['opt_minopt_usd'],
        'opt_minopt_usd' => $this->price_range[1]['opt_minopt_usd'],
        'drop_uah_max' => $this->price_range[1]['drop_uah'],
        'drop_uah' => $this->price_range[1]['drop_uah'],
        'min_price' => $this->priceMin,
        'max_price' => $this->priceMax,
      ],
      'products_series_id' => $product['products_series_id'],
      'filters_values' => $this->setOfferParams($product['params'], $product['materials']),
      'menu' => $product['category_id'],
      'addit_menus' => implode(',', $this->addItMenu($product)),
      'group_products' => [],
      'price_lists_characteristics' => '',
      'price_lists_review' => $product['description'],
      'has_package' => 0,
      'is_package_for' => 0,
      'show_price' => 0,
      'from' => 1, // продукт с прайса ВЧехле
    ];

    $data['group_products'] = $this->getGroupProducts($product, $data);

    return $data;
  }

  /**
   * @param $product
   * @param bool $save
   * @return array[]
   */
  private function getGroupProducts(array $product, array $data = null)
  {
    $tmp = [[
      'id' => $product['id'] ?? '',
      'color' => 'fff',
      'name' => [
        'ru' => $product['color_name'] ?? $data['color_name'],
      ],
      'material' => [],
      'image' => $product['img'] ?? $data['img'] ?? null,
      'min_price' => $this->priceMin,
      'max_price' => $this->priceMax,
      'price' => $this->price_range[1]['price'],
      'opt_price' => $this->price_range[1]['opt_price'],
      'opt_vip_usd' => $this->price_range[1]['opt_vip_usd'],
      'opt_dealer_usd' => $this->price_range[1]['opt_dealer_usd'],
      'drop_uah' => $this->price_range[1]['drop_uah'],
      'old_opt_price' => false, //$product->price,
      'qty' => $product['qty'],
      'when_appear' => '',
    ]];

    $materials = $product['materials'] ?? $data['materials'];
    foreach ($materials as $index => $material) {
      $tmp[0]['material'][$index]['qty'] = $material['qty'];
      if ($index === 'silicone') {
        $tmp[0]['material'][$index]['name_ru'] = 'TPU';
      } elseif ($index === 'plastic') {
        $tmp[0]['material'][$index]['name_ru'] = '3D пластик';
      } else {
        $tmp[0]['material'][$index]['name_ru'] = $material['name_ru'];
      }
    }

    return $tmp;
  }

  /**
   * получение цвета
   * @param array $params
   * @return string
   */
  private function getColorName(array $params): string
  {
    $find  = array_search('тематика', array_column($params, 'name'));
    if($find === false) {
      return '';
    }

    $subjectMatter = $params[$find]['value'];

    if (! $this->subjectMatter->has($subjectMatter) ) {
      $this->subjectMatter->put($subjectMatter, 0);
      $this->newSubjectMatter->push(new SubjectMatter([
        'name' => $subjectMatter
      ]));
    }

    return $subjectMatter;
  }

  /**
   * Получаем список фильтров Тематика
   */
  private function getFiltersSubject(): void
  {
    $this->filtersSubject = FiltersValue::where('filter_id', $this->filter_id)->get(['id', 'filter_id', 'name']);
  }

  /**
   * получение серий товаров
   */
  private function getProductsSeries(): void
  {
    $this->productsSeries = ProductsSeries::all(['id', 'name'])->pluck('name', 'id')->toArray();
  }

  /**
   * получаем ID категории
   * @param int $id
   * @return int
   */
  private function getCategoryId(int $id): int
  {
    return $this->categories[$id]['menu_id'] ?? $this->menu_id;
  }

  /**
   * получение родительского ID
   * @param int $id
   * @return intf
   */
  private function getParentId(int $id): int
  {
    return isset($this->categories[$id]['category']['parent_id']) ? $this->categories[$id]['category']['parent_id'] : 0;
  }

  /**
   * Получаем ID поставщика
   * @param string $vendor
   * @return int
   */
  private function getVendorId(): int
  {
    return $this->brandId;
  }

  /**
   * записываем ошибки в лог
   * @param string $methodName
   * @param string $message
   * @throws \Exception
   */
  private function logError(string $methodName, string $message): void
  {
    throw new \Exception(sprintf('Import print price - method %s %s', $methodName, $message));
  }

  // Получение значений для filters_values
  /**
   * получить ID форм-фактора
   * @param string $name
   * @return int
   */
  private function getFormFactorId(string $name): int
  {
    switch (Str::lower($name)) {
      case 'накладка': return 219;
      default: return 0;
    }
  }

  /**
   * получить ID тип аксессуара
   * @param string $name
   * @return int
   */
  private function getAccessoryTypeId(string $name): int
  {
    switch (Str::lower($name)) {
      case 'принт': return 226;
      default: return 0;
    }
  }

  /**
   * получить ID материала
   * @param string $name
   * @return int
   */
  private function getMaterialId(string $name): int
  {
    switch (Str::lower($name)) {
      case 'силикон':
      case 'tpu': return 236;
      case 'tpu (термополиуретан)': return 240;
      case 'tpu+pc': return 245;
      case 'натуральная кожа': return 238;
      case 'искусственная кожа': return 239;
      case 'пластик': return 237;
//      case 'силикон': return 337;
      case 'ткань': return 274;
      default: return 0;
    }
  }

  /**
   * получить ID особенности чехла (фильтр)
   * @param string $name
   * @return int
   */
  private function getCaseFeaturesId(string $name): int
  {
    switch ($name) {
      case 'индивидуальная печать': return $this->case_features_id;
      default: return 0;
    }
  }

  /**
   * получить ID тематики
   * @param string $name
   * @return int
   */
  private function getSubjectId(string $name): int
  {
    $filter = $this->filtersSubject->where('name', $name);

    if ($filter->count() > 0) {
      return $filter->first()->id;
    } else {
      $filter = new FiltersValue([
        'filter_id' => $this->filter_id,
        'name' => $name,
        'url' => Str::slug('subject-'.$name),
      ]);

      // Проверка URL на уникальность
      while (FiltersValue::where('url', $filter->url)->exists()) {
        $filter->url = increment_string($filter->url);
      }

      $filter->save();

      $this->filtersSubject->push($filter);
      return $filter->id;
    }
  }

  /**
   * получение ID серии
   * @param string $title
   * @return int
   */
  private function getProductSeriesId(string $title): int
  {
    $title = $this->removeIprintInTitle($title);
    if (! Str::contains($title, 'для')) {
      return 0;
    }

    $seriesName = Str::before($title, ' для');
    $find = array_search($seriesName, $this->productsSeries);
    if ($find !== false) {
      return $find;
    } else {
      $series = new ProductsSeries([
        'name' => $seriesName,
        'url' => Str::slug($seriesName),
        'products_count' => 1,
      ]);

      // Проверка URL на уникальность
      while (ProductsSeries::where('url', $series->url)->exists()) {
        $series->url = increment_string($series->url);
      }

      $series->save();
      $this->productsSeries[$series->id] = $series->name;

      return $series->id;
    }
  }

  /**
   * добавляем дополнительные меню
   * @param array $product
   * @return array
   */
  private function addItMenu(array $product): array
  {
    $menu = [
      1767, // Чехлы
      1780, // Принт
    ];

    foreach ($product['params'] as $param) {
      if ($param['name'] == 'материал') {
        $material = 0;

        switch (Str::lower($param['value'])) {
          case 'TPU':
          case 'tpu': $material = 1773;break;
//        case 'tpu (термополиуретан)': $material = 0; break;
//        case 'tpu+pc': $material = 0; break;
//        case 'натуральная кожа': $material = 0; break;
//        case 'искусственная кожа': $material = 0; break;
          case 'пластик': $material = 1826;break;
          case 'силикон': $material = 1772; break;
//        case 'ткань': $material = 0; break;
        }
        array_push($menu, $material);
      }
    }

    if (isset($product['title'])) {
      $title = mb_strtolower($product['title'], 'UTF-8');
      if (mb_strpos($title, 'apple') !== false || mb_strpos($title, 'iphone') !== false) array_push($menu, 1768);
      elseif (mb_strpos($title, 'samsung') !== false || mb_strpos($title, 'galaxy') !== false) array_push($menu, 1769);
      elseif (mb_strpos($title, 'xiaomi') !== false) array_push($menu, 1776);
      elseif (mb_strpos($title, 'motorola') !== false) array_push($menu, 2068);
      elseif (mb_strpos($title, 'realme') !== false) array_push($menu, 1816);
      elseif (mb_strpos($title, 'huawei') !== false) array_push($menu, 1817);
      elseif (mb_strpos($title, 'vivo') !== false) array_push($menu, 1955);
      elseif (mb_strpos($title, 'oppo') !== false) array_push($menu, 1976);
      elseif (mb_strpos($title, 'zte') !== false) array_push($menu, 2049);
      elseif (mb_strpos($title, 'motorola') !== false) array_push($menu, 224);
      elseif (mb_strpos($title, 'infinix') !== false) array_push($menu, 2176);
      elseif (mb_strpos($title, 'tecno') !== false) array_push($menu, 2168);
    }

    return $menu;
  }

  /**
   * Применение шаблонов описаний к новым товарам
   * @param $products
   */
  private function applyDescriptionTemplates($products): void
  {
    if (! $products->count()) {
      return;
    }

    // Получение списка шаблонов описаний
    $descriptions = ProductsDescription::all();

    // Обработка списка товаров и применение к ним шаблонов
    $updatedProducts = collect();

    foreach ($products as $product) {
      foreach ($descriptions as $description) {
        if (stripos($product->title, $description->keywords) !== false) {
          $product->fill($description->only([
            'full_description', 'videos', 'filters_values', 'has_package', 'show_price', 'description_for_client'
          ]));

          $updatedProducts->push($product);
          break;
        }
      }
    }

    // Сохранение изменений товаров в одной транзакции
    DB::transaction(fn() => $updatedProducts->each->save());
  }

  /**
   * импорт картинок
   * @param string $url
   * @return string
   */
  private function importImages(string $url): string
  {
    return $url;
    try {
      $image = Image::make($url);
    } catch (\Exception $ex) {
      return '';
    }
    // Создание ресурса изображения для проверки корректности файла
    if ($image) {
      // Генерация нового названия файла
      $filename = Str::beforeLast( Str::afterLast($url, '/'), '.');
      $filename = $filename . '.jpg';

      $image->resize(Product::IMAGE_SIZE, Product::IMAGE_SIZE, function($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      })->save($this->storagePath . $filename);

      // Создание уменьшенной версии изображения
      $thumbName = thumb_name($filename);
      $thumbSize = Product::IMAGE_THUMB_MAIN;

      File::copy($this->storagePath . $filename, $this->storagePath . $thumbName);
      Image::make($this->storagePath . $thumbName)
        ->resize($thumbSize, null, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        })
        ->save($this->storagePath . $thumbName);

      // Загрузка изображений на сервер с контентом
      $this->storage->put($filename, File::get($this->storagePath . $filename));
      $this->storage->put($thumbName, File::get($this->storagePath . $thumbName));

      File::delete($this->storagePath . $filename);
      File::delete($this->storagePath . $thumbName);

      return $filename;
    }
  }

  /**
   * удаление картинки
   * @param $filename
   */
  private function deleteImage($filename)
  {
    $thumbName = thumb_name($filename);

    $this->storage->delete($filename);
    $this->storage->delete($thumbName);
  }

  /**
   * Вывод сообщения отладки
   * @param string $message
   */
  private function debugMessage($message = '')
  {
    if (!$this->debugMode) {
      return;
    }

    $elapsedTime = microtime(true) - $this->debugStartTime;
    $hours = floor($elapsedTime / 3600);
    $minutes = floor($elapsedTime % 3600 / 60);
    $seconds = floor($elapsedTime % 60);

    echo '[' . ($hours < 10 ? '0' : '') . $hours . ':' . ($minutes < 10 ? '0' : '') . $minutes . ':' .
      ($seconds < 10 ? '0' : '') . $seconds . '] ' . $message . PHP_EOL;
  }

  /**
   * создаем название для модели
   * @param $title
   * @param $category_id
   * @return string
   */
  private function getTitle(string $title, int $category_id): string
  {
    if (isset($this->categories[$category_id]['category'])) {
      $title = Str::before($title, ' для');
      $categoriesName = $this->categories[$category_id];
      $title .= ' для ';
      $title .= $categoriesName['category']['title'];
    }

    $title = $this->addIprintInTitle($title);

    return $title;
  }

  /**
   * добавляем iPrint в название
   * @param string $title
   * @return string
   */
  private function addIprintInTitle(string $title): string
  {
    return str_ireplace('Чехол', 'Чехол itsPrint', $title);
  }

  /**
   * удаляем iPrint из названия
   * @param string $title
   * @return string
   */
  private function removeIprintInTitle(string $title): string
  {
    return str_ireplace('Чехол itsPrint', 'Чехол', $title);
  }
}
