<?php

namespace App\Http\Models;

use App\Events\CartModified;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartContentItem
{
  public $rowId;
  public $id;
  public $parent;
  public $baseid;
  public $url;
  public $title;
  public $image;
  public $color;
  public $color_name;
  public $isPrint;
  public $label;
  public $when_appear;
  public $outOfStock;
  public $colorBorder;
  public $selectedMaterial;
  public $fullBorder;
  public $material;
  public $material_id;
  public $materialName;
  public $brandName;
  public $modelName;
  public $modelUrl;
  public $products_series_id;

  public $discountRate;
  public $price;
  public $opt_price;
  public $opt_vip_usd;
  public $opt_dealer_usd;
  public $drop_uah;
  public $opt_minopt_usd;
  public $old_opt_price;

  public $has_package;
  public $filters_values;
  public $need_package;
  public $is_package_for;
  public $requiredPackages = [];

  public $qty;
  public $qty_today;
  public $qty_with_delay;

  public $priceRange;
  public $pointers;

  private $promoaction;

  public function __construct($product, $item)
  {
    $this->rowId = $item->rowId;
    $this->id = $product->id ?? $product->uuid;
    $this->parent = $product->parent;
    $this->title = $product->title;
    $this->from = $product->from;
    $this->baseid = $this->getBaseId($product);
    $this->url = $this->getUrl($product);
    $this->image = $this->getImageUrl($product);
    $this->color = $product->color;
    $this->color_name = $product->color_name;
    $this->isPrint = $product->isPrint();
    $this->products_series_id = $product->products_series_id;

    $this->label = $product->getLabel();
    $this->outOfStock = $product->qty == 0 && !$product->when_appear;
    $this->when_appear = $product->when_appear;
    $this->colorBorder = colorWhite($product->color ?? '');
    $this->selectedMaterial = $item->options->material ?? null;
    $this->fullBorder = $item->options->fullBorder ?? false;
    $this->material = $product->availableMaterials;
    $this->material_id = $product->material_id ?? null;
    $this->materialName = $this->getMaterialName($this->selectedMaterial);
    $this->brandName = $this->getBrandName($product);
    $this->modelName = $this->getModelName($product);
    $this->modelUrl = $this->getModelUrl($product);

    $this->qty = $item->qty;
    $this->qty_product = $product->qty;
    $this->qty_today = $product->qty_today;
    $this->qty_with_delay = $product->qty_with_delay;

    $this->discountRate = $item->discountRate;
    $this->price = 0;
    $this->price_rrc = $product->getPrice('price');
    $this->opt_price = $product->getPrice('opt_price');
    $this->opt_vip_usd = $product->getPrice('opt_vip_usd');
    $this->opt_dealer_usd = $product->getPrice('opt_dealer_usd');
    $this->drop_uah = $product->getPrice('drop_uah');
    $this->opt_minopt_usd = $product->getPrice('opt_minopt_usd');
    $this->old_opt_price = $product->getPrice('old_opt_price');

    $this->is_package_for = $product->is_package_for;
    if ($product->parentProduct) {
      $parentProduct = $product->parentProduct;
      $this->has_package = $parentProduct->has_package;
      $this->filters_values = $parentProduct->filters_values;
    }

    $this->requiredPackages = $this->getPackage($product);

    $this->promoaction = $product->promoaction;

    $this->priceRange = [];
    $this->pointers = [];

    $this->options = $item->options;
  }

  // GETTER PRIVATE
  protected function getPriceFromRange($amount)
  {
    foreach ($this->pointers as $item) {
      if ($item === $amount) {
        return $this->priceRange[$item];
      }

      if ($amount > $item) {
        return $this->priceRange[$item];
      }
    }

    return 0;
  }

  // GETTER PUBLIC
  /**
   * статус заказа
   */
  public function status(): string
  {
    return ($this->qty > 0)
      ? 'доступен'
      : (($this->when_appear)
        ? 'ожидается'
        : 'нет в наличии');
  }

  /**
   * промоакция
   */
  public function getPromoaction()
  {
    return $this->promoaction;
  }

  /**
   * Получить цену товара
   * @param string $type
   * @param bool $withDiscount - с учетом скидки или без
   * @return float
   */
  public function getPrice(string $type = 'opt_price', bool $withDiscount = false, array $priceRange = null): float
  {
    $type = ($type === 'price') ? 'price_rrc' : $type;
    // если тип цены в грн и это не принт - переводим цену в доллары
    if ($this->isPrint) {
      $price = $this->getPriceFromRange((int)$this->qty);
      if ((stripos($type, 'uah') !== false || $type === 'price_rrc')) {
        $price = $price / settings('dollar');
      }
    } else {
      $price = ((stripos($type, 'uah') !== false || $type === 'price_rrc') && $this->id !== null)
        ? $this->{$type} / settings('dollar')
        : $this->{$type};
    }

    if ($withDiscount === true) {
      $discount = round( ($price * $this->discountRate / 100), 2);
      return round( $price - $discount, 2);
    }

    return round( $price, 2);
  }

  /**
   * конвертируем цену в грн
   * @param string $type
   * @param bool $withDiscount
   * @return int
   */
  public function getPriceUAH(string $type = 'opt_price', bool $withDiscount = false): int
  {
    $type = ($type === 'price') ? 'price_rrc' : $type;
    // если тип цены в грн и это не принт - переводим цену в доллары
    $price =  ((stripos($type, 'uah') !== false || $type === 'price_rrc') && $this->id !== null)
      ? $this->{$type}
      : $this->{$type} * settings('dollar');

    if ($withDiscount === true) {
      $discount = round( ($price * $this->discountRate / 100), 2);
      return round($price - $discount);
    }

    return round($price);
  }

  /**
   * общая стоимость товара с кол-вом
   * @param string $type
   * @param bool $convertToUAH
   * @return float
   */
  public function priceTotal(string $type = 'opt_price', bool $convertToUAH = false): float
  {
    if ($convertToUAH) {
      return $this->price * settings('dollar') * $this->qty;
    } else {
      return $this->price * $this->qty;
    }
  }

  /**
   * скидка от цены
   * @param string $priceType
   * @return float
   */
  public function discount($priceType = 'opt_price')
  {
    return round( ($this->getPrice($priceType) * $this->discountRate / 100), 2);
  }

  /**
   *  суммарная скидка
   */
  public function discountTotal()
  {
    return $this->discount() * $this->qty;
  }

  /**
   * общая стоимость товара с учетом скидки
   * @param string $type
   * @param bool $convertToUAH
   * @return mixed
   */
  public function subtotal(string $type = 'opt_price', bool $convertToUAH = false)
  {
    $decimals = 2;
    if ($convertToUAH) $decimals = 0;

//    return max(round($this->priceTotal($type, $convertToUAH) - $this->discountTotal(), $decimals), 0);
    return max(round($this->priceTotal($type, $convertToUAH) - $this->discountTotal(), $decimals), 0);
  }

  // SETTER PUBLIC

  /**
   * обновляем скидку
   * @param $discount
   */
  public function setDiscount($discount): void
  {
    //Cart::setDiscount($this->rowId, 0);
    $this->discountRate = $discount;
    //if ($update) event(new CartModified);
  }

  /**
   * изменить цену в корзине
   * @param $price
   */
  public function setPrice($price): void
  {
    //Cart::update($this->rowId, ['price' => $price]);
    $this->price = $price;

    if ($this->id === null) {
      $this->drop_uah = $price;
      $this->price_rrc = $price;
    }
    //if ($update) event(new CartModified);
  }

  /**
   * Получаем массив с таблицей цен
   */
  public function setPriceRange($data)
  {
    $this->pointers = array_keys($data);
    rsort($this->pointers);
    $this->priceRange = $data;
  }

  // GETTER PRIVATE
  private function getImageUrl($product) {
    return isset($product->id)
      ? $product->img ? config('custom.content_server') . thumb_name($product->img) : '/template/images/noimage.jpg'
      : $product->image;
  }
  private function getPackage(): array
  {
    $requiredPackages = [];
    // Если товар продается без упаковки и сам не является упаковкой - определяем тип упаковки, который ему требуется
    if ( ! $this->has_package && ! $this->is_package_for) {
      foreach (config('custom.package_types') as $packageTypeId => $packageType) {
        if (isset($this->filters_values) && count(array_intersect($packageType['filters'], $this->filters_values))) {
          $this->need_package = true;
          if (array_search($packageTypeId, $requiredPackages) === false) $requiredPackages[] = $packageTypeId;
        }
      }
    }

    return $requiredPackages;
  }
  private function getModelUrl($product)
  {
    return ($product->categoryData)
      ? custom_route('category', ['phones', $product->categoryData->name])
      : null;
  }
  private function getModelName($product)
  {
    return ($product->categoryData)
      ? $product->categoryData->title
      : $product->categoryVchehle->category->title ?? null;
  }
  private function getBrandName($product)
  {
    return ($product->parentProduct)
      ? optional($product->parentProduct->brandData)->title
      : optional($product->brandData)->title;
  }
  private function getMaterialName($selectedMaterial)
  {
    return ($selectedMaterial === null )
      ? null
      : ['silicone' => __('тпу'), 'plastic' => __('3D пластик')][$selectedMaterial];
  }
  private function getBaseId($product): ?string
  {
    return isset($product->id) ? ltrim(($product->parentProduct->baseid ?? $product->baseid), '0') : null;
  }
  private function getUrl($product): ?string
  {
    return isset($product->id) ? custom_route('product', [optional($product->parentProduct)->url ?: $product->url,
      optional($product->parentProduct)->id ?: $product->id]) : null;
  }
}