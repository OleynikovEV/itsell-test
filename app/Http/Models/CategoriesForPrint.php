<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class CategoriesForPrint
{
  private int $type;
  private $data;

  private bool $showCovers;
  private bool $showScreenProtection;
  private bool $showTechnicalGoods;

  private array $categories = [];

  public function __construct(int $type, bool $showCovers, bool $showScreenProtection, bool $showTechnicalGoods)
  {
    $this->type = $type;
    $this->showCovers = $showCovers;
    $this->showScreenProtection = $showScreenProtection;
    $this->showTechnicalGoods = $showTechnicalGoods;

    $this->data = null;
  }

  public function setData($data)
  {
    $this->data = $data;
  }

  public function getCategoriesType3()
  {
    if ($this->data !== null) {
      return $this->data;
    }

    $parentCategories = $this->getParentCategories();

    // Формирование списка типов товаров
    if ($this->showCovers) {
      $this->categories[] = $this->addToCategory(1, 'Чехлы');
      $this->categories1($this->categories, $parentCategories, 1);
    }
    if ($this->showScreenProtection) {
      $this->categories[] = $this->addToCategory(2, 'Защита экрана');
      $this->categories1($this->categories, $parentCategories, 2);
    }
    if ($this->showTechnicalGoods) {
      $this->categories[] = $this->addToCategory(3, 'Технические и общие товары');
      $this->categories = array_merge($this->categories, $this->getOtherCategories());
    }

    return $this->categories;
  }

  /*
   * первый тип категорий категория + бренд
   */
  private function categories1(&$categories, $parentCategories, $parent)
  {
    foreach ($parentCategories as $item) {
      $categories[] = $this->addToCategory($item->id * 100 + $parent, $item->title, $parent);
    }
  }

  private function getParentCategories()
  {
    return DB::table('its_menu')
      ->where('parent_id', 0)
      ->where('brand', 1)
      ->get(['id', 'parent_id', 'title']);
  }

  private function getOtherCategories(): array
  {
    $categories = [];

    $generalCategories = DB::table('its_other_types')->get(['id', 'name']);

    foreach ($generalCategories as $item) {
      $id = $item->id + 10;
      $categories[$id] = $this->addToCategory($id,  $item->name, 3);
    }

    return $categories;
  }

  /**
   * Добавить элемент в категории
   * @param int $id
   * @param string $title
   * @param int|null $parentId
   * @return array
   */
  private function addToCategory(int $id, string $title, ?int $parentId = 0): array
  {
    if ($parentId > 0) {
      $element = [
        'id' => $id,
        'category' => $title,
        'parentId' => $parentId,
      ];
    } else {
      $element = [
        'id' => $id,
        'category' => $title,
      ];
    }

    return $element;
  }
}