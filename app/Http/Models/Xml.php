<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class Xml
{
  private $xml;
//  private $shop;
//  private $currencies;
  private $categories;
  private $offers;
  private $currency;

  public function __construct(string $currency)
  {
    $this->currency = $currency;

    // Корневой элемент
    $this->xml = new SimpleXMLExtended('<?xml version="1.0" encoding="utf-8"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd"><yml_catalog/>');
    $this->xml->addAttribute('date', date('Y-m-d H:i'));

    // Информация о магазине
    $shop = $this->xml->addChild('shop');
    $shop->addChild('name', 'ITsell Опт');
    $shop->addChild('company', 'ITsell Опт');
    $shop->addChild('url', URL::to('/'));
    $shop->addChild('email', 'partner@itsellopt.com.ua');

    // Валюта магазина
    $currencies = $shop->addChild('currencies');
    $currency = $currencies->addChild('currency');
    $currency->addAttribute('id', $this->getCurrencyName());
    $currency->addAttribute('rate', $this->getCurrencyRate());

    // Плейсхолдер для списка категории
    $this->categories = $shop->addChild('categories');

    // Товары
    $this->offers = $shop->addChild('offers');
  }

  public function getOffers()
  {
    return $this->offers;
  }

  /*
   * добавление категорий
   */
  public function insertCategories(array $categories): void
  {
    // добавляем категории в файл
    foreach ($categories as $item) {
      $category = $this->categories->addChild('category', str_replace('&', '', $item['category']));
      $category->addAttribute('id', $item['id']);
      if (isset($item['parentId']) && $item['parentId'] > 0) {
        $category->addAttribute('parent_id', $item['parentId']);
        $category->addAttribute('parentId', $item['parentId']);
      }
    }
  }

  /*
   * добавление товаров
   */
  public function insertOffer(callable $function): void
  {
    $offer = $this->offers->addChild('offer');
    $function($offer);
  }

  /**
   * сохраняем файл
   * @param string|null $name
   * @return string
   */
  public function save(?string $name = null): string
  {
    $fileName = $name ?? Str::random();
    $fileName = Storage::disk('price_lists')->path($fileName . '.xml');
    $this->xml->asXML($fileName);

    return $fileName;
  }

  /**
   * Название валюты
   * @return string
   */
  private function getCurrencyName(): string
  {
    return strtoupper($this->currency);
  }

  /**
   * курс
   * @return string
   */
  private function getCurrencyRate(): string
  {
    return '1.0';
  }
}