<?php
/**
 * https://developers.viber.com/docs/api/rest-bot-api/
 */


namespace App\Http\Models;

use GuzzleHttp\Client;

class ViberMessanger implements Messenger
{
  public function sendMessage($chatId, $text)
  {
    $url = 'https://chatapi.viber.com/pa/send_message';

    $params = [
      'receiver' => $chatId,
      'type' => 'text',
      'sender' => [
        'name' => config('custom.shop_name'),
        'avatar' => config('custom.viber_bot_logo'),
      ],
      'text' => $text,
      'auth_token' => config('custom.viber_bot_token'),
    ];

    $headers = [
      'Cache-Control' => 'no-cache',
      'Content-Type' => 'application/JSON',
      'X-Viber-Auth-Token' => config('custom.viber_bot_token'),
    ];

    $client = new Client();

    $response = $client->request('POST', $url, [
      'header' => $headers,
      'json' => $params
    ]);

    return true;
  }

  public function getMessage()
  {
    // TODO: Implement getMessage() method.
  }

  public function getProductAvailabilityMessage(array $products): string
  {
    $text = '';

    if (count($products) > 0) {
      $text = 'Поступление товаров itsellopt.com.ua' . PHP_EOL;
      $text .= 'Здравствуйте, на сайте поступление товаров которые вы ожидаете' . PHP_EOL;
      $text .= PHP_EOL;

      foreach ($products as $product) {
        $text .= $product->title . PHP_EOL;
        $text .= custom_route('product', [$product->url, $product->id]) . PHP_EOL;
        if ($product->comment) {
          $text .= 'Комментарий: ' . $product->comment . PHP_EOL;
        }
        $text .= PHP_EOL;
      }
    }

    return $text;
  }
}