<?php

namespace App\Http\Models;

use Illuminate\Support\Collection;

class ProductsRelationshipsCollection
{
  private Collection $collect;

  public function __construct(Collection $collection)
  {
    $this->collect = $collection;
  }

  public function getProductIds(): Collection
  {
    return $this->collect->pluck('product_id');
  }

  public function get(): Collection
  {
    return $this->collect;
  }
}