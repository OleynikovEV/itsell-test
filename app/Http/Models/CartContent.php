<?php

namespace App\Http\Models;

use App\Events\CartModified;
use App\Models\PrintCover;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartContent
{
  private $content;
  private $cartInstance;
  private string $priceType;
  private $priceRange;
  private $pointers;

  public function __construct(string $priceTypeDefault = 'opt_price', $instance = 'default')
  {
    $this->priceRange = null;
    $this->pointers = null;
    $this->content = collect();
    $this->priceType = $priceTypeDefault;
    $this->cartInstance = Cart::instance($instance);
  }

  // PUBLIC
  /**
   * Вызов события изменения корзины для ее сохранения профиле пользователя
   */
  public function updateCart()
  {
    event(new CartModified);
  }

  // GETTER
  /**
   * Если нужно дополнительный список цен
   */
  public function setPriceRange($data)
  {
    $this->pointers = array_keys($data);
    rsort($this->pointers);
    $this->priceRange = $data;
  }

  /**
   * Все товары в корзине
   * @return \Illuminate\Support\Collection
   */
  public function content()
  {
    $this->getProductsCollection();
    return $this->content;
  }

  /**
   * сумма заказа
   * @param string $priceType
   * @return int
   */
  public function total($priceType = 'opt_price')
  {
    if ($this->content->count() === 0) return 0;

    $sum = 0;

    foreach ($this->content as $item) {
      $sum += $item->subtotal($priceType);
    }

    return $sum;
  }

  /**
   * Ссылка на корзину
   */
  public function getCartInstance()
  {
    return $this->cartInstance;
  }

  /**
   * суммарное количество товара
   */
  public function count()
  {
    return $this->content->count();
  }

  // SETTER
  public function setPriceType(string $priceTypeDefault)
  {
    $this->priceType = $priceTypeDefault;
  }

  /**
   * Обновляем цену товара
   * @param $itemId
   * @param $price
   */
  public function setItemPrice($itemId, $price): void
  {
    if ($this->content->count() === 0) return;

    $item = $this->content->firstWhere('rowId', $itemId);
    if ($item !== null) {
      $item->setPrice($price);
      $this->cartInstance->update($itemId, ['price' => $price]);
    }
  }

  /**
   * Обновляем скидку товару
   * @param $itemId
   * @param $price
   */
  public function setItemDiscount($itemId, $discount): void
  {
    if ($this->content->count() === 0) return;

    $item = $this->content->firstWhere('rowId', $itemId);
    if ($item !== null) {
      $item->setDiscount($discount);
      $this->cartInstance->setDiscount($itemId, $discount);
    }
  }

  // PROTECTED
  protected function createCartContentItem($product, $cartItem): CartContentItem
  {
    $item = new CartContentItem($product, $cartItem);
    if ($this->priceRange !== null) {
      $item->setPriceRange($this->priceRange);
    }

    return $item;
  }

  protected function getProductsCollection()
  {
    $this->content = collect();

    $productsIds = [];
    $coversIds = [];

    $items = $this->cartInstance->content();

    foreach ($items->pluck('id') as $item) {
      if (strlen($item) > 10) { $coversIds[] = $item; }
      else { $productsIds[] = $item; }
    }

    $products = collect();
    $cover = collect();

    if (count($productsIds) > 0) {
      $products = Product::whereIn('id', $productsIds)
        ->with('parentProduct.brandData', 'categoryData', 'promoaction')
        ->get(['id', 'baseid', 'id as uuid', 'title', 'qty', 'img', 'title', 'parent', 'url', 'from',
          'opt_price', 'old_opt_price', 'price', 'price_types', 'promo_code', 'qty_today', 'qty_with_delay',
          'color_name', 'when_appear', 'group_products', 'added_time', 'price_modify_time',
          'color', 'menu', 'show_price', 'brand', 'products_series_id'])
        ->keyBy('id');
    }

    if (count($coversIds) > 0) {
      $cover = PrintCover::whereIn('uuid', $coversIds)
        ->selectRaw('uuid, title, image, price, qty, material_id, category_id, 
            category_id as parent, show_price, brand, 1 as `from`')
        ->with('brandData', 'categoryVchehle')
        ->get()
        ->keyBy('uuid');

      foreach ($cover as $item) {
        $products->put($item->uuid, $item);
      }
    }

    foreach ($items as $item) {
      $product = $products->get($item->id);

      if ( ! $product) continue;

      $this->content->push(
        $this->createCartContentItem($product, $item)
      );
    }
  }


}