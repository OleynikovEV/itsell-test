<?php

namespace App\Http\Models;

use App\Mail\AvailabilityNotification;
use App\Models\Product;
use App\Models\User;
use App\Models\WaitingList;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Availability
{
  /**
   * При экспорте из 1С
   * 1) получить ids товаров которые пришли из 1с с реальным количеством +
   * 2) получить список товаров из таблицы "в ожидании" +
   *  2.1) получить список уникальных ids товаров +
   *  2.2) получить список уникальных пользователей +
   * 3.1) из списка 2.1 получить список товаров с их доп. полями +
   * 3.2) из списка 2.2 получить информацию о способе оповещения +
   * 4) из списка 2 получить массив, где ключ - это id пользователя и вложить те товары, которые он ожидает
   * 5) пройтись по массиву и разослать уведомления
   */

  /**
   * Клиент заходит в список ожиданий
   * 1) подгрузить все товары и описания
   * 2) пометить как "архивные" - все товары которые отключены
   */

  private $productsIds = [];

  /**
   * Availability constructor.
   * @param array $productsIds
   */
  public function __construct(array $productsIds)
  {
      if (count($productsIds) === 0) {
        throw new \ValueError('productsIds is empty');
      }

      // удаляем дубликаты из массива
      $this->productsIds = $this->deleteDuplicate($productsIds);
  }

  /**
   * Разослать уведомления клиентам
   */
  public function notify()
  {
    $notifiTelegram = new Notification(new TelegramMessanger());
    $notifiViber = new Notification(new ViberMessanger());

    // получаем список пользователей
    $users = $this->getUsers($this->productsIds);
    $productsInfo = $this->getProductsInfo($this->productsIds);

    foreach ($users as $user) {
      if (empty($user->email) && $user->telegram_chat_id === 0) continue;

      $productsIds = explode(',' ,$user->productsIds);
      $products = [];
      foreach ($productsIds as $productId) {
        $product = $productsInfo[$productId];
        $products[] = $product;
      }

      try {
        Mail::to($user->email)->send(new AvailabilityNotification($user, $products));
      } catch (\Throwable $ex) {
        Log::error(sprintf('Рассылка уведомлений. Пользователь %s %s %s', $user->fio, $user->email, $ex->getMessage()));
      }

      if ($user->telegram_chat_id > 0) {
        try {
          $notifiTelegram->setChatId($user->telegram_chat_id)
          ->sendAvailabilityProducts($products);
        } catch (\Throwable $ex) {
          Log::error(sprintf('Рассылка уведомлений по telegram. Пользователь %s %s %s', $user->fio, $user->telegram_chat_id, $ex->getMessage()));
        }
      }

      if ($user->viber_chat_id) {
        try {
          $notifiViber->setChatId($user->viber_chat_id)
            ->sendAvailabilityProducts($products);
        } catch (\Throwable $ex) {
          Log::error(sprintf('Рассылка уведомлений по viber. Пользователь %s %s %s', $user->fio, $user->viber_chat_id, $ex->getMessage()));
        }
      }
    }
  }

  /**
   * Удаление дубликатов
   * @param array $productIds
   * @return array
   */
  private function deleteDuplicate(array $productIds): array
  {
    return array_unique($productIds);
  }

  /**
   * Список товаров с доп. полями
   * @param array $productIds
   * @return array
   */
  private function getProductsInfo(array $productIds): array
  {
    return DB::table('its_products')
      ->whereIn('its_products.id', $productIds)
      ->leftJoin('its_waiting_list', 'its_waiting_list.product_id', '=', 'its_products.id')
      ->get(['its_products.id', 'its_products.title', 'its_products.url', 'its_products.img', 'its_waiting_list.comment'])
      ->keyBy('id')
      ->toArray();
  }

  /**
   * список пользователей у которых в листе ожиданий есть данный товар
   * @param array $productsIds
   * @return array
   */
  private function getUsers(array $productsIds): array
  {
    return DB::table('its_waiting_list')
      ->select('user_id', 'fio', 'email', 'telegram_chat_id', 'viber_chat_id', DB::raw('GROUP_CONCAT(product_id) AS productsIds'))
      ->whereIn('product_id', $productsIds)
      ->leftJoin('its_users AS user', 'user.id', '=', 'user_id')
      ->groupBy('user_id')
      ->get()
      ->keyBy('user_id')
      ->toArray();
  }
}
