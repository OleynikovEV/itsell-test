<?php

namespace App\Http\Models;

use Telegram;

class TelegramMessanger implements Messenger
{
  public function sendMessage($chatId, $text)
  {
    return Telegram::sendMessage([
      'chat_id' => $chatId,
      'text' => $text,
      'parse_mode' => 'HTML',
    ]);
  }

  public function getMessage()
  {
    // TODO: Implement getMessage() method.
  }

  public function getProductAvailabilityMessage(array $products): string
  {
    $text = '';

    if (count($products) > 0) {
      $text = '<b>Поступление товаров itsellopt.com.ua</b>' . PHP_EOL;
      $text .= '<b>Здравствуйте, на сайте поступление товаров которые вы ожидаете</b>' . PHP_EOL;
      $text .= PHP_EOL;



      foreach ($products as $num => $product) {
        $text .= '<b>' . ++$num . ')</b> <a href="' . custom_route('product', [$product->url, $product->id]) . '">' . $product->title . '</a>' . PHP_EOL;
        if ($product->comment) {
          $text .= '<b>Комментарий</b>: ' . $product->comment . PHP_EOL;
        }
        $text .= PHP_EOL;
      }
    }

    return $text;
  }
}