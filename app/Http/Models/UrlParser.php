<?php

namespace App\Http\Models;

use Illuminate\Support\Str;

class UrlParser
{
  protected $url;
  protected $section;
  protected $parameters;
  protected $page;
  protected $hasFilters;

  public function __construct($section, $url, $parameters)
  {
    $this->section = $section;
    $this->url = $url;

    $parameters = explode('/', $parameters);

    // проверяем есть ли фильтры
    $this->hasFilters = (isset($parameters[0]) && $parameters[0] === 'filter');
    // какой номер страницы выбран
    $this->page = (isset($parameters[2]) && (int)$parameters[2]) ? (int)$parameters[2] : 0;
    $this->parameters = (isset($parameters[1])) ? explode(';', $parameters[1]) : null;

    // разбираем строку с параметрами
    $tmp = [];
    if ($this->parameters !== null) {
      foreach ($this->parameters as $item) {
        $key = Str::before($item, '=');
        $tmp[$key] = explode(',', Str::after($item, '='));
      }
    }

    $this->parameters = $tmp;
  }

  public function getSection()
  {
    return $this->section;
  }

  public function getMenu()
  {
    return $this->url;
  }

  public function hasFilters(): bool
  {
    return $this->hasFilters;
  }

  public function amount()
  {
    return count($this->parameters);
  }

  public function currentPage(): int
  {
    return $this->page;
  }

  public function getAllParams(): ?array
  {
    return $this->parameters;
  }

  /**
   * Получаем массив активных фильтров
   */
  public function getActiveFiltersArray(array $exclude = []): array
  {
    $activeFilters = [];
    $filters = $this->parameters;

    //unset($filters['categories']); // удаляем фильтры по моделям телефонов
    if (count($filters) > 0) {
      foreach ($filters as $key => $items) {
        if (in_array($key, $exclude)) continue;
        foreach ($items as $item) {
          $activeFilters[] = $item;
        }
      }
    }

    return $activeFilters;
  }

  /**
   * Выбранные фильтры из категорий
   * @return array|null
   */
  public function getCategories(): ?array
  {
    return $this->categoriya;
  }

  /**
   * Выбранные фильтры по моделям
   * @return array|null
   */
  public function getModels(): ?array
  {
    return $this->categories;
  }

  /**
   * Выбираем фильтры по бренду
   * @return array|null
   */
  public function getBrands(): ?array
  {
    return $this->brand;
  }

  public function selectedCategoriesOnly(): bool
  {
    return ($this->hasFilters() && $this->amount() === 1 && $this->categoriya !== null);
  }

  public function __get($name)
  {
    // преобразуем строку formFaktor в form-faktor
    $name = Str::replaceArray('_',  ['-'], Str::snake($name));
    if ( isset($this->parameters[$name]) ) {
      return $this->parameters[$name];
    }
  }

  public function setParameters(array $parameters)
  {
    $this->parameters = $parameters;
    $this->hasFilters = true;
  }
}