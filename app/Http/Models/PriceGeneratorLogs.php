<?php

namespace App\Http\Models;

use App\Models\PgLogs;

class PriceGeneratorLogs
{
  private string $priceName;
  private bool $autogenerate;
  private int $userID;
  private float $timeStart; // в секундах
  private float $timeEnd; // в секундах
  private float $memoryStartPosition; // в байтах
  private float $memoryUse; // в байтах

  public function __construct(string $priceName, bool $autogenerate)
  {
    $this->priceName = $priceName;
    $this->autogenerate = $autogenerate;
    $this->userID = auth()->guest() ? 1 : auth()->user()->id; // 1 - system
  }

  // SETTER
  public function setUserId(int $userId): PriceGeneratorLogs
  {
    $this->userID = $userId;

    return $this;
  }

  public function setPriceName(string $priceName): PriceGeneratorLogs
  {
    $this->priceName = $priceName;

    return $this;
  }

  // GETTER
  public function getPriceName(): string
  {
    return $this->priceName;
  }

  public function start(): void
  {
    $this->timeStart = microtime(true);
    $this->memoryStartPosition = memory_get_usage();
  }

  public function finish(): PriceGeneratorLogs
  {
    $this->timeEnd = microtime(true) - $this->timeStart;
    $this->memoryUse = memory_get_usage() - $this->memoryStartPosition;

    return $this;
  }

  public function save(): bool
  {
    $new = new PgLogs();
    $new->user_id = $this->userID;
    $new->price = $this->priceName;
    $new->autogenerate = $this->autogenerate;
    $new->timeExecuted = round($this->timeEnd, 2);
//    $new->memoryUsed = round($this->convertToMb($this->memoryUse), 2);
    $new->memoryUsed = $this->memoryUse;
    $new->created_at = now();
    return $new->save();
  }

  private function convertToMb($value)
  {
    return $value / 1024 / 1024;
  }
}