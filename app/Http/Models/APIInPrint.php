<?php

/**
 * Модуль по работ с АПИ и получению количества товаров и доступного материала
 * для производства чехлов
 * url https://prints.vchehle.ua/api/categories/materials
 * doc: https://prints.vchehle.ua/api-docs.html #3 - Наличие материалов по категориям
 */
namespace App\Http\Models;

use App\Models\VchehleMaterials;
use Illuminate\Support\Facades\DB;

class APIInPrint
{
  private $url = 'https://prints.vchehle.ua/api/';
  private $api = [
    'availableMaterialsByCategory' => 'categories/materials',
  ];
  private $token = null;

  public function __construct()
  {
    $this->token = config('custom.vchehle_token');
  }

  public function availableMaterialsByCategory()
  {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $this->url . $this->api['availableMaterialsByCategory']);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-type: application/json',
      'Authorization: Bearer ' . $this->token,
    ]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    try {
      $response = curl_exec($ch);

      $this->updateCategories(json_decode($response, true));
    } catch (\Exception $e) {

    } finally {
      curl_close($ch);
    }
  }

  private function updateCategories(array $data)
  {
    if (isset($data['data']) && count($data['data']) > 0) {
      $categories = $this->getCategories();

      foreach ($data['data'] as $item) {
        echo $item['name'] . PHP_EOL;
        foreach ($item['materials'] as $material) {

          if ($material['qty'] > 0) {
            $category = $categories->where('category_id', $item['id'])->where('material_id', $material['id']);

            if ($category->count() > 0) {
              DB::table('its_vchehle_materials')
                ->where('category_id', $item['id'])->where('material_id', $material['id'])
                ->update(['qty' => $material['qty']]);
            } else {
              $category = new VchehleMaterials();
              $category->category_id = $item['id'];
              $category->material_id = $material['id'];
              $category->qty = $material['qty'];
              $category->save();
            }
          }
        }
      }
    }
  }

  private function getCategories()
  {
    return VchehleMaterials::all(['category_id', 'material_id', 'qty']);

    $categories = DB::table('its_vchehle_materials')->get();

    if ($categories->count() > 0) {
      $categories = $categories->toArray();

      $tmp = [];
      foreach ($categories as $item) {
        if (!isset($item->category_id)) $tmp[$item->category_id] = [];

        $tmp[$item->category_id][$item->material_id] = $item->qty;
      }

      return $tmp;
    }

    return null;
  }
}