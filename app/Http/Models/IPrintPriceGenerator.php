<?php

namespace App\Http\Models;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class IPrintPriceGenerator
{
  private $currency; // курс валют
  private $priceType; // тип цены
  private $query; // запрос для формирования прайса
  private $categoryType; // тип (вид) категории

  public function __construct(string $currency, string $priceType, Builder $query, $categoryType = 1)
  {
    $this->currency = strtoupper($currency);
    $this->query = $query;
    $this->priceType = $priceType;
    $this->categoryType = $categoryType;
  }

  /*
   * сохранить как xml
   */
  public function saveAsXml(?string $fileName = null): string
  {
    $categoryType = $this->categoryType;
    $categoriesData = $this->getCategories();

    $categories = new CategoriesForPrint($categoryType, true, false, false);

    $xml = new Xml($this->currency);
    $xml->insertCategories($categories->getCategoriesType3());

    $this->query->orderBy('id')->chunk(5000, function($products) use ($xml, $categoryType, $categoriesData) {
      foreach ($products as $product) {
        $xml->insertOffer(function ($offer) use ($product, $categoryType, $categoriesData) {
          $offer->addAttribute('id', $product->id);
          $offer->addAttribute('available', $product->qty > 0 ? 'true' : 'false');
          $offer->addAttribute('selling_type', 'r');
          $offer->addChild('vendorCode', $product->brand);
          $offer->addChild('vendor', 'itsPrint');
          $offer->addChild('name', htmlspecialchars($product->title));
          $offer->addChild('url', Url::to('/products/' . $product->url . '/' . $product->id));
          $offer->addChild('picture', full_path_picture($product->img));
          $offer->addChild('currencyId', $this->currency);
          $offer->addChild('price', $product->{$this->priceType} ?? $product->price);

          if ($categoryType == 3) {
            if (isset($categoriesData[$product->menu]['parentId'])) {
              $categoryId = $this->getCategoryId($categoriesData[$product->menu]['parentId'], 1);
            } else {
              $categoryId = $product->menu;
            }

            $offer->addChild('categoryId', $categoryId);
          }

          $offer->addChild('category', $categoriesData[$product->menu]['name'] ?? '');

          $param = $offer->addChild('param', 'Накладка');
          $param->addAttribute('name',  'Форм-фактор');

          $param = $offer->addChild('param', 'Принт');
          $param->addAttribute('name', 'Тип аксессуара');

          $param = $offer->addChild('param', 'TPU');
          $param->addAttribute('name', 'Материал');

          $param = $offer->addChild('param', 'Индивидуальная печать');
          $param->addAttribute('name', 'Особенности чехла');

          $param = $offer->addChild('param', $product->color_name);
          $param->addAttribute('name',  'Тематика');

          $groups = json_decode($product->group_products, true);

          foreach ($groups[0]['material'] as $key => $group) {
            $material = $offer->addChild('material', $group['name_ru']);
            $material->addAttribute('id',  $key);
            $material->addAttribute('qty',  $group['qty']);
          }
        });
      }
    });

    return $xml->save($fileName);
  }

  private function getCategoryId($categoryId, $parent)
  {
    return  $categoryId * 100 + $parent;
  }

  /*
   * сохранить как csv
   */
  public function saveAsCsv(?string $fileName = null): string
  {
    $categoryType = $this->categoryType;
    $categoriesData = $this->getCategories();

    $csv = new Csv($this->currency);
    $csv->setTitle([
      'id', 'available', 'selling_type', 'vendorCode', 'vendor', 'name', 'url', 'picture', 'currencyId', 'price',
      'categoryId', 'category', 'form_factor', 'accessory_type', 'case_features', 'subject',
      'material', 'qty'
    ]);

    $this->query->orderBy('id')->chunk(5000, function($products) use ($csv, $categoryType, $categoriesData) {
      foreach ($products as $product) {
        if ($categoryType == 3) {
          $categoryId  = $categoriesData[$product->menu]['parentId'] ?? $product->menu;
        } else {
          $categoryId = $product->menu;
        }

        $groups = json_decode($product->group_products, true);

        $data = [];
        foreach ($groups[0]['material'] as $key => $group) {
          $data = [
            $product->id,
            $product->active ? 'true' : 'false',
            'r',
            $product->brand,
            'itsPrint',
            $product->title,
            Url::to('/products/' . $product->url . '/' . $product->id),
            full_path_picture($product->img),
            $this->currency,
            $product->{$this->priceType} ?? $product->price,
            $categoryId,
            $categoriesData[$product->menu]['name'] ?? '',
            'Накладка',
            'Принт',
            'Особенности чехла',
            $product->color_name,
            $group['name_ru'],
            $group['qty'],
          ];

          $csv->insertOffer($data);
        }
      }
    });

    return $csv->save($fileName);
  }

  /**
   * Категории
   * @return array
   */
  private function getCategories(): array
  {
    $data = DB::table('its_vchehle as v')
      ->select(['m.id', 'm.parent_id', 'm.title'])
      ->leftJoin('its_menu as m', 'm.id', '=', 'v.menu_id')
      ->orderBy('m.parent_id')
      ->get()
      ->toArray();

    $categories = [];
    foreach ($data as $item) {
      $categories[$item->id] = [
        'parentId' => $item->parent_id,
        'name' => $item->title,
      ];
    }

    return $categories;
  }
}