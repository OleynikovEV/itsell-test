<?php

namespace App\Http\Models\PriceGenerator;

use App\Models\Category;

class CategoriesType
{
  static function getType4()
  {
//    $categoriesIds = [1911, 683, 1767, 682, 1781, 1907, 1786];
    $categoriesIds = [1911, 683, 682, 1907];

    $categories = Category::whereIn('id', $categoriesIds)
      ->where('products_count', '>', 0)
      ->orWhere(function($q) {
        $q->where('parent_id', 1786)->where('products_count', '>', 0);
      })
      ->orderBy('parent_id')
      ->get(['id', 'title', 'parent_id']);

    $data = [
      1 => [
        'id' => 1,
        'category' => 'Чехлы',
        'parentId' => 0,
        'childrenIds' => [],
      ],
      2 => [
        'id' => 2,
        'category' => 'Защитные стекла',
        'parentId' => 0,
        'childrenIds' => [],
      ],
      3 => [
        'id' => 3,
        'category' => 'Защитные пленки',
        'parentId' => 0,
        'childrenIds' => [],
      ],
    ];

    foreach ($categories as $item) {
      if (!isset($data[$item->id])) {
        $data[$item->id] = [
          'id' => $item->id,
          'category' => $item->title,
          'parentId' => 0,
          'childrenIds' => [],
        ];
      }

      $data[$item->id]['childrenIds'][] = $item->id;
    }

    return $data;
  }
}