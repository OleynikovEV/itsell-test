<?php

namespace App\Http\Models;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductsSeries;
use App\Models\SubjectMatter;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class PriceGeneratorHelper
{
  private static $columns = ['id', 'active', 'qty', 'parent', 'filters_values', 'menu', 'product_type', 'product_subtype',
    'products_series_id', 'baseid', 'brand', 'url', 'title', 'full_description', 'markdown',
    'description_for_client', 'date_of_uploading', 'from', 'group_products', 'price_types', 'img'];
  /*
   * основной конструктор запросов
   */
  static private function getProductsQuery(bool $notAvailable = false, int $subDay = 1): Builder
  {
    if ($notAvailable == false) {
      return DB::table('its_products')
        ->select(self::$columns)
        ->where('parent', 0)
        ->where('active', true)
        ->where('qty', '>', 0)
        ->where('menu', '<>', 237)
        ->where('from', 0);
    } else {
      return DB::table('its_products')
        ->select(self::$columns)
        ->where('parent', 0)
        ->where('from', 0)
        ->where('menu', '<>', 237)
        ->where(function ($queryParent) use ($subDay) {
          $queryParent->where(function ($query) {
            $query->where('active', true)
              ->where('qty', '>', 0);
          })->orWhere(function ($query2) use ($subDay) {
            $query2->where('active', 0)
              ->whereBetween('last_modified', [today()->subDay($subDay)->timestamp, today()->timestamp]);
          });
        });
    }
  }

  /*
   * гиброгелевые пленки
   */
  static public function getHydrogel(): Builder
  {
    return DB::table('its_products')
      ->select(self::$columns)
      ->where('parent', 0)
      ->where('active', true)
      ->where('qty', '>', 0)
      ->where('menu', '<>', 237)
      ->where('from', 2)
      ->where('tech_pack', 0);
  }

  static public function getAllPrint(bool $notAvailable = false)
  {
    if ($notAvailable == false) {
      return DB::table('its_products')
        ->where('from', 1)
        ->where('active', true)
        ->where('qty', '>', 0);
    } else {
      return DB::table('its_products')
        ->where('from', 1);
    }
  }

  /**
   * Список всех товаров
   * @return Builder
   */
  static public function getAllProducts(bool $notAvailable = false, int $subDay = 1)
  {
    return self::getProductsQuery($notAvailable, $subDay);
  }

  /**
   * Получаем список продуктов по menuId
   * @param array $menu - массив из ИД меню
   * @param bool $full - возвращать весь список продуктов или с пагинацией
   * @param array $conditions - дополнительные условия отбора
   * @param int|null $offset - с какой позиции отбирать записи для пагинации
   * @param int|null $limit - сколько записей отобрать для пагинации
   */
  static public function getProductsByMenu(array $menu, bool $full, array $conditions, bool $notAvailable, ?int $offset = 0, ?int $limit = 20)
  {
    $products = self::getProductsQuery($notAvailable);

    self::conditions($products, $conditions);

    $products = $products->where(function ($q) use ($menu) {
        $q->whereIn('menu', $menu)
          ->orWhereIn('id', DB::table('its_product_category')
            ->select('product_id')
            ->whereIn('category_id', $menu)
          );
      })
      ->orderBy('colorCode');

    if ($full === false) {
      $paginationUrl = route('price-generator.products-list');
      $offset = ($offset) ? $offset : 0;
      $limit = ($limit) ? $limit : 0;

      return self::paginationGenerate($products, $offset, $limit, $paginationUrl);
    } else {
      return $products;
    }
  }

  /**
   * Получение гидрогелевых пленок
   * @param array $menu
   * @param bool $full
   * @param array $conditions
   * @param bool $notAvailable
   * @param int|null $offset
   * @param int|null $limit
   * @return Builder|LengthAwarePaginator
   */
  static public function getProductsHydrogel(array $menu, bool $full, array $conditions, bool $notAvailable, ?int $offset = 0, ?int $limit = 20)
  {
    $products = self::getHydrogel();

    self::conditions($products, $conditions);

    $products = $products->where(function ($q) use ($menu) {
      $q->whereIn('menu', $menu)
        ->orWhereIn('id', DB::table('its_product_category')
          ->select('product_id')
          ->whereIn('category_id', $menu)
        );
    })
      ->orderBy('colorCode');

    if ($full === false) {
      $paginationUrl = route('price-generator.products-list');
      $offset = ($offset) ? $offset : 0;
      $limit = ($limit) ? $limit : 0;

      return self::paginationGenerate($products, $offset, $limit, $paginationUrl);
    } else {
      return $products;
    }
  }

  /*
   * получение принтов по материалам
   */
  static public function getPrintSubjectMatters(array $menu, bool $full, array $conditions, bool $notAvailable, ?int $offset = 0, ?int $limit = 20)
  {
    $sm = SubjectMatter::whereIn('id', $menu)->get()->pluck('name')->toArray();

    $products = self::getAllPrint($notAvailable);

    $products->whereIn('color_name', $sm);

    self::conditions($products, $conditions);

    if ($full === false) {
      $paginationUrl = route('price-generator.products-list');
      $offset = ($offset) ? $offset : 0;
      $limit = ($limit) ? $limit : 0;

      return self::paginationGenerate($products, $offset, $limit, $paginationUrl);
    } else {
      return $products;
    }
  }

  /**
   * Получаем список продуктов по menuId
   * @param array $menu - массив из ИД меню
   * @param bool $full - возвращать весь список продуктов или с пагинацией
   * @param array $conditions - дополнительные условия отбора
   * @param bool $notAvailable
   * @param int|null $offset - с какой позиции отбирать записи для пагинации
   * @param int|null $limit - сколько записей отобрать для пагинации
   */
  static public function getProductsByiPrint(array $menu, bool $full, array $conditions, bool $notAvailable, ?int $offset = 0, ?int $limit = 20)
  {
    $products = self::getAllPrint($notAvailable);

    self::conditions($products, $conditions);

    $products = $products->where(function ($q) use ($menu) {
      $q->whereIn('menu', $menu)
        ->orWhereIn('id', DB::table('its_product_category')
          ->select('product_id')
          ->whereIn('category_id', $menu)
        );
    });

    if ($full === false) {
      $paginationUrl = route('price-generator.products-list');
      $offset = ($offset) ? $offset : 0;
      $limit = ($limit) ? $limit : 0;

      return self::paginationGenerate($products, $offset, $limit, $paginationUrl);
    } else {
      return $products;
    }
  }

  /**
   * Получаем список продуктов по menuId
   * @param array $series - массив из ИД серий
   * @param bool $full - возвращать весь список продуктов или с пагинацией
   * @param array $conditions - дополнительныйе условия отбора
   * @param int|null $offset - с какой позиции отбирать записи для пагинации
   * @param int|null $limit - сколько записей отобрать для пагинации
   */
  static public function getProductsBySeries(array $series, bool $full, array $conditions, bool $notAvailable, ?int $offset = 0, ?int $limit = 20)
  {
    $products = self::getProductsQuery($notAvailable);
    self::conditions($products, $conditions);

    $products = $products->whereIn('products_series_id', $series)
      ->orderBy('arrived_in_stock_at', 'DESC')
      ->orderBy('id', 'DESC');

    if ($full === false) {
      $paginationUrl = route('price-generator.products-list');
      $offset = ($offset) ? $offset : 0;
      $limit = ($limit) ? $limit : 0;

      return self::paginationGenerate($products, $offset, $limit, $paginationUrl);
    } else {
      return $products;
    }
  }

  /**
   * @param array $brands
   * @param bool $full
   * @param array $conditions
   * @param int|null $offset
   * @param int|null $limit
   */
  static public function getProductsByBrands(array $brands, bool $full, array $conditions, bool $notAvailable, ?int $offset = 0, ?int $limit = 20)
  {
    $products = self::getProductsQuery($notAvailable);
    self::conditions($products, $conditions);

    $products = $products->whereIn('brand', $brands)
      ->orderBy('arrived_in_stock_at', 'DESC')
      ->orderBy('id', 'DESC');

    if ($full === false) {
      $paginationUrl = route('price-generator.products-list');
      $offset = ($offset) ? $offset : 0;
      $limit = ($limit) ? $limit : 0;

      return self::paginationGenerate($products, $offset, $limit, $paginationUrl);
    } else {
      return $products;
    }
  }

  /**
   * условия отбора товаров
   * @param $products
   * @param array $conditions
   */
  static private function conditions(&$products, array $conditions): void
  {
    if (isset($conditions['finish']) && $conditions['finish'] == true) {
      //
    } else {
      self::whereQtyMoreThen($products, 9);
    }

    if (isset($conditions['sale']) && $conditions['sale'] == false) {
      self::whereSale($products);
    }
  }

  /**
   * фильтр уценка
   */
  static private function whereSale(&$products): void
  {
    $products->whereRaw('(markdown = 0)');
  }

  /**
   * фильтруем по количеству
   * @param $products
   * @param int $amount
   */
  static private function whereQtyMoreThen(&$products, int $amount): void
  {
    $products->where('qty', '>', $amount);
  }

  /**
   * Получаем список продуктов по menuId
   * @param $products - коллекция products Model
   * @param int $offset - с какой позиции отбирать записи для пагинации
   * @param int $limit - сколько записей отобрать для пагинации
   * @param string $paginationUrl - ссылка для пагинации
   * @return LengthAwarePaginator
   */
  static private function paginationGenerate($products, int $offset, int $limit, string $paginationUrl): LengthAwarePaginator
  {
    return $products->paginate($limit);
  }

  /**
   * Возвращает родительские категории
   */
  static public function getCategories()
  {
    return DB::table('its_menu')
      ->where('parent_id', 0)
      ->where('id', '<>', 237)
      ->where('products_count', '>', 0)
      ->orderBy('title', 'asc')
      ->get(['id', 'name', 'title', 'parent_id']);
  }

  /**
   * Возвращает дочерние категории
   * @param bool|null $withSubMenu - подтягивать данные из menu_sectors
   */
  static public function getChildrenCategories(?bool $withSubMenu = false)
  {
    $categories = DB::table('its_menu')
      ->where('parent_id', '>', 0)
      ->where('id', '<>', 237)
      ->where('parent_id', '<>', 237)
      ->where('products_count', '>', 0)
      ->orderBy('title', 'asc');

    return $categories->get(['id', 'name', 'title', 'parent_id']);
  }

  /**
   * Строим двухуровневое дерево из меню
   * @param $parentCategories - коллекция Category Model
   * @param $childrenCategories - коллекция Category Model
   * @return array - массив в виде дерева категорий
   */
  static public function createCategoriesTree($parentCategories, $childrenCategories): array
  {
    $tree = [];

    foreach ($parentCategories as $item) {
      $id = $item->id;

      $element = [
        'id' => $id,
        'name' => $item->name,
        'title' => $item->title,
        'selected' => false,
        'opened' => false,
      ];

      $tree[$id]['item'] = $element;
    }

    foreach ($childrenCategories as $index => $item) {
      $parent_id = $item->parent_id;

      $element = [
        'id' => $item->id,
        'name' => $item->name,
        'title' => $item->title,
        'parent_id' => $item->parent_id,
        'selected' => false,
      ];

      if (isset($tree[$parent_id])) {
        $tree[$parent_id]['children'][$index]['item'] = $element;
      }
    }

    return self::separateCategories(self::orderCategories($tree));
  }

  /**
   * Разделение меню на бренд и категория
   * @param array $data - данные из метода createCategoriesTree
   * @return array - массив с categories и brand
   */
  static private function separateCategories(array $data): array
  {
    $tree = [
      'categories' => [],
      'models' => [],
    ];

    $categories = [1911, 1907, 1786, 1781, 1767, 683, 682, 110];

    foreach ($data as $id => $item) {
      if (in_array($item['item']['id'], $categories)) {
        $type = 'categories';
      } else {
        $type = 'models';
      }

      $tree[$type][$id] = $item;
    }

    return $tree;
  }

  /**
   * Серии товара
   */
  static function getProductsSeriesList()
  {
    return DB::table('its_products_series')
      ->where('products_count', '>', 0)
      ->orderBy('name')->get(['id', 'name']);
  }

  /**
   * Бренд товара
   */
  static function getBrandsList()
  {
    return DB::table('its_brands')
      ->where('active', true)
      ->where('products_count', '>', 0)
      ->orderBy('name')
      ->get(['id', 'title', 'products_count', 'products_count_new']);
  }

  /**
   * Категория моделей по принтам
   */
  static function getChildrenCategoriesIPrint()
  {
    return DB::table('its_vchehle as v')
      ->leftJoin('its_menu as m', 'm.id', '=', 'v.menu_id')
      ->groupBy('v.menu_id')
      ->get(['m.id', 'm.parent_id', 'm.title', 'm.name']);
  }

  /**
   * Категория брендов по принтам
   */
  static function getCategoriesIPrint()
  {
    return DB::table('its_vchehle as v')
      ->leftJoin('its_menu as m', 'm.id', '=', 'v.menu_id')
      ->leftJoin('its_menu as p', 'p.id', '=', 'm.parent_id')
      ->orderBy('p.title')
      ->groupBy('p.id')
      ->get(['p.id', 'p.title', 'p.name']);
  }


  /**
   * Сортировка меню для вывода на сайт
   * @param array $tree
   * @return array
   */
  private static function orderCategories(array $tree)
  {
    $num = 9;
    $temp = [];

    foreach ($tree as $key => $item) {
      switch ($key) {
        case 1767: $temp[0] = $item; break;
        case 1781: $temp[1] = $item; break;
        case 1907: $temp[2] = $item; break;
        case 1911: $temp[4] = $item; break;
        case 683:  $temp[5] = $item; break;
        case 682:  $temp[6] = $item; break;
        case 1786: $temp[7] = $item; break;
        case 110:  $temp[8] = $item; break;
        default:
          $temp[$num] = $item;
          $num++;
      }
    }

    return $temp;
  }
}
