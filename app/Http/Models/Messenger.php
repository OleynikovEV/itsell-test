<?php

namespace App\Http\Models;

interface Messenger
{
  public function sendMessage($chatId, $text);
  public function getMessage();
  public function getProductAvailabilityMessage(array $products): string;
}