<?php

namespace App\Http\Middleware;

use Closure;

class RestoreCartFromUserData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user())
        {
            $cart = unserialize(auth()->user()->cart);
            if ($cart) session(['cart' => $cart]); else session()->forget('cart');
        }

        return $next($request);
    }
}
