<?php

namespace App\Http\Middleware;

use Closure;

class DisableSessionsForBots
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Изменение драйвера сессий для поисковых ботов для отключения их сохранения
        if (\Crawler::isCrawler()) config(['session.driver' => 'array']);
        
        return $next($request);
    }
}
