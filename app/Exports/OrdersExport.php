<?php

namespace App\Exports;

use App\Models\Order;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OrdersExport implements FromArray, WithStrictNullComparison, ShouldAutoSize, WithHeadings, WithStyles// ,WithMapping
{
  private int $userId;
  private int $dateStart;
  private int $dateEnd;

  /**
   * OrdersExport constructor.
   * @param int $userId
   * @param null $dateStart
   * @param null $dateEnd
   */
  public function __construct(int $userId, $dateStart = null, $dateEnd = null)
  {
    $this->userId = $userId;
    $this->dateStart = (empty($dateStart)) ? now()->startOfDay()->timestamp : now()->setTimestamp(strtotime($dateStart . 'T00:00:00'))->timestamp;
    $this->dateEnd = (empty($dateEnd)) ? now()->endOfDay()->timestamp : now()->setTimestamp(strtotime($dateEnd . 'T23:59:59'))->timestamp;
  }

  /**
   * @inheritDoc
   */
  public function array(): array
  {
    $orders = Order::where('user_id', $this->userId)
      ->whereBetween('time', [$this->dateStart, $this->dateEnd])
      ->with('products.product:id,title')
      ->get(['id', 'total', 'time', 'company', 'fio']);

    $collect = [];
    $totalCount = 0;
    $totalSum = 0;

    foreach ( $orders as $order) {
      foreach ($order->products as $product) {
        if ($product->product) {
          $collect[] = [
            [
              $order->id,
              $product->product->id,
              $product->product->title,
              $product->count,
              $product->price,
              round($product->price * $product->count, 2),
              now()->setTimestamp($order->time)->format('d-m-Y'),
            ]
          ];
        } elseif ($product->productCover) {
          $collect[] =[
            [
              $order->id,
              '',
              $product->productCover->title . ' ' . ['silicone' => __('тпу'), 'plastic' => __('3D пластик')][$product->material] ,
              $product->count,
              $product->price,
              round($product->price * $product->count, 2),
              now()->setTimestamp($order->time)->format('d-m-Y'),
            ]
          ];
        }

        $totalCount += $product->count;
        $totalSum += round($product->price * $product->count, 2);
      }
    }

    array_push($collect, [
      '', '',
      __('Итого'),
      $totalCount,
      '',
      $totalSum,
      '',
    ]);

    return $collect;
  }

  /**
   * Заголовки к колонкам
   * @return array
   */
  public function headings(): array
  {
    return [
      ['','',
        sprintf('%s с %s по %s'
        , __('Расходная накладная за период')
        , now()->setTimestamp($this->dateStart)->format('d-m-Y')
        , now()->setTimestamp($this->dateEnd)->format('d-m-Y')
      )],
      ['','',
        sprintf('%s %s', __('Поставщик'), config('custom.shop_name')
      )],
      [''],
      [__('№ заказа'), __('Код товара'), __('Товар'), __('Количество'), __('Цена (usd)'), __('Сумма (usd)'), __('Дата заказа')],
    ];
  }

  /**
   * Стилизация ячеек
   * @param Worksheet $sheet
   */
  public function styles(Worksheet $sheet)
  {
    $sheet->getStyle('C1')->getFont()->setSize(16)->setBold(true);
    $sheet->getStyle('C2')->getFont()->setBold(true);

    $sheet->getStyle('A4')->getFont()->setBold(true);
    $sheet->getStyle('B4')->getFont()->setBold(true);
    $sheet->getStyle('C4')->getFont()->setBold(true);
    $sheet->getStyle('D4')->getFont()->setBold(true);
    $sheet->getStyle('E4')->getFont()->setBold(true);
    $sheet->getStyle('F4')->getFont()->setBold(true);
    $sheet->getStyle('G4')->getFont()->setBold(true);
  }
}