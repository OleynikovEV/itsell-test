<?php

namespace App\Console\Commands;

use App\Http\Models\APIInPrint;
use Illuminate\Console\Command;

class AvailableMaterialsByCategory extends Command
{
  protected $signature = 'print:available';
  protected $description = 'Трекинг посылки по ТТН';

  public function handle()
  {
    $api = new APIInPrint();
    $api->availableMaterialsByCategory();
  }
}