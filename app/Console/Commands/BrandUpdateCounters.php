<?php

namespace App\Console\Commands;

use App\Models\Brand;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BrandUpdateCounters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'brand:update-counters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update products counters for all brands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Получение списка всех брендов
        $brands = Brand::all()->keyBy('id')->map(function($brand)
        {
            $brand->products_count = 0;
            $brand->products_count_new = 0;

            return $brand;
        });

        // Получение счетчиков количества товаров
        $counters = Product::active()
            ->selectRaw('brand, COUNT(*) AS count, SUM(IF(added_time > ' . today()->subDays(Product::NOVELTY_DAYS)->timestamp
                . ', 1, 0)) AS count_new')
            ->groupBy('brand')
            ->get()
            ->keyBy('brand');

        // Обновление данных в брендах
        DB::transaction(function() use ($brands, $counters)
        {
            foreach ($brands as $brand)
            {
                if ($counters->has($brand->id))
                {
                    $brand->products_count += $counters->get($brand->id)->count;
                    $brand->products_count_new += $counters->get($brand->id)->count_new;
                }

                // Сохранение бренда
                $brand->save();
            }
        });

        // Возврат успешного статуса
        $this->info('Completed!');
    }
}
