<?php

namespace App\Console\Commands;

use App\Http\Models\PriceGeneratorLogs;
use Illuminate\Console\Command;
use App\Http\Models\PriceGenerator\CategoriesType;
use App\Http\Models\PriceGeneratorHelper;
use App\Models\Product;
use App\Http\Models\Xml;
use Illuminate\Support\Facades\URL;
use App\Models\Brand;
use App\Models\FiltersValue;
use Illuminate\Support\Str;

class RozetkaPriceGenerator extends Command
{
  protected $signature = 'pg:rozetka';
  protected $description = 'Генератор прайсов для rozetka';

  public function handle()
  {
    $days = 7;

    $pgLog = new PriceGeneratorLogs('pg:rozetka xml', true);
    $pgLog->start();

    $categories = CategoriesType::getType4(); // получаем категории

    // получаем товары
    $query = PriceGeneratorHelper::getAllProducts(true, $days);
    $parentProducts = $query->select(['id', 'active', 'filters_values', 'brand', 'menu',
      'full_description', 'description_for_client', 'date_of_uploading'])->get();
    $productsIds = $parentProducts->pluck('id')->toArray();

    // получаем цвета
    $productsColors = Product::with('categoryData')
      ->whereIn('parent', $productsIds)
      ->where('from', 0)
      ->where('when_appear', '=', '')
      ->where(function ($queryParent) use ($days){
        $queryParent->where(function ($query) {
          $query->where('qty', '>', 0);
        })->orWhere(function ($query2) use ($days) {
          $query2->where('qty', 0)
            ->whereBetween('last_modified', [today()->subDay($days)->timestamp, today()->timestamp]);
        });
      })
      ->get(['id', 'parent', 'active', 'menu', 'price_types', 'old_price', 'price', 'opt_price', 'qty', 'colorCode',
        'color_name', 'title', 'img', 'img_1', 'img_2', 'img_3', 'img_4', 'img_5', 'img_6', 'img_7', 'url'])
      ->groupBy('parent');

    $brand = Brand::all()->pluck('title', 'id'); // бренды
    $filters = FiltersValue::with('filter')->get(['id', 'filter_id', 'name'])->keyBy('id'); // фильтры

    // создаем xml
    $xml = new Xml('uah', );
    $xml->insertCategories($categories);

    // заполняем файл
    foreach ($parentProducts as $product) {
      if (! $productsColors->has($product->id)) continue;

      $list = $productsColors->get($product->id);

      $brandName = htmlspecialchars($brand->get($product->brand)) ?? '';
      $productFiltersValues = [];

      // характеристики товара
      if ($product->filters_values) {
        $productFilters = explode(',', $product->filters_values);
        foreach ($productFilters as $item) {
          if ($filters->has($item)) {
            $filter = $filters->get($item);
            if ($filter->filter->id === 84) continue;
            $productFiltersValues[] = [
              'name' => $filter->filter->alternative_name ?? $filter->filter->name,
              'value' => $filter->name,
            ];
          }
        }
      }

      // определяем категорию
      $categoryId = null;
      if (in_array(256, $productFilters)) { $categoryId = 1; }
      elseif (in_array(257, $productFilters)) { $categoryId = 2; }
      elseif (in_array(258, $productFilters)) { $categoryId = 3; }

      foreach ($list as $color) {
        $xml->insertOffer(function ($offer) use ($color, $product, $brandName, $productFiltersValues, $categoryId) {
          $offer->addAttribute('id', $color->colorCode);
          $offer->addAttribute('available', ($color->qty > 0) ? 'true' : 'false');
          $offer->addChild('name', htmlspecialchars($color->title));
          $offer->addChild('url', Url::to('/products/' . $color->url . '/' . $color->id));
          $offer->addChild('vendor', htmlspecialchars($brandName));
          $offer->addChild('categoryId', $categoryId ??
            (
            ($color->categoryData->parent_id === 1786)
              ? $color->categoryData->id
              : $color->categoryData->parent_id
            )
          );
          $offer->addChild('currencyId', 'UAH');
          $offer->addChild('price', $color->price);
          if ($color->old_price > 0) {
            $offer->addChild('price_old', $color->old_price);
          }
          $offer->addChild('stock_quantity', $color->qty);
          foreach (['img', 'img_1', 'img_2', 'img_3', 'img_4', 'img_5', 'img_6', 'img_7'] as $img) {
            if ($color->{$img}) {
              $offer->addChild('picture', full_path_picture($color->{$img}));
            }
          }

          // совместимость
          // iPhone 12 Pro Max (6.7")
//          $findPos = strripos($color->categoryData->title, '"');
//          if ($findPos !== false) {
//            dump($color->categoryData->title);
//            dd($findPos);
//          }
          $param = $offer->addChild('param', htmlspecialchars($color->categoryData->title));
          $param->addAttribute('name', 'Совместимость');

          // цвет
          $param = $offer->addChild('param', htmlspecialchars($color->color_name));
          $param->addAttribute('name', 'Цвет');

          foreach ($productFiltersValues as $value) {
            $param = $offer->addChild('param', $value['value']);
            $param->addAttribute('name',  $value['name']);
          }

          $description = getDescription(
            $product->full_description,
            $product->description_for_client,
            $product->date_of_uploading,
            $color->title,
            $color->color_name
          );
          $offer->addCData('description', $description);
        });
      }
    }

    $xml->save('rozetka_price'); // сохраняем

    $pgLog->finish()->save();
  }
}