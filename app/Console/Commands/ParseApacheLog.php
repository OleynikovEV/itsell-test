<?php

namespace App\Console\Commands;

use App\Models\SearchModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ParseApacheLog extends Command
{
    protected $symbol = ["%20"=>" ", "%D0%B0"=>"а", "%D0%90"=>"А", "%D0%B1"=>"б", "%D0%91"=>"Б", "%D0%B2"=>"в", "%D0%92"=>"В",
      "%D0%B3"=>"г", "%D0%93"=>"Г", "%D0%B4"=>"д", "%D0%94"=>"Д", "%D0%B5"=>"е", "%D0%95"=>"Е", "%D1%91"=>"ё",
      "%D0%81"=>"Ё", "%D0%B6"=>"ж", "%D0%96"=>"Ж", "%D0%B7"=>"з", "%D0%97"=>"З", "%D0%B8"=>"и", "%D0%98"=>"И",
      "%D0%B9"=>"й", "%D0%99"=>"Й", "%D0%BA"=>"к", "%D0%9A"=>"К", "%D0%BB"=>"л", "%D0%9B"=>"Л", "%D0%BC"=>"м",
      "%D0%9C"=>"М", "%D0%BD"=>"н", "%D0%9D"=>"Н", "%D0%BE"=>"о", "%D0%9E"=>"О", "%D0%BF"=>"п", "%D0%9F"=>"П",
      "%D1%80"=>"р", "%D0%A0"=>"Р", "%D1%81"=>"с", "%D0%A1"=>"С", "%D1%82"=>"т", "%D0%A2"=>"Т", "%D1%83"=>"у",
      "%D0%A3"=>"У", "%D1%84"=>"ф", "%D0%A4"=>"Ф", "%D1%85"=>"х", "%D0%A5"=>"Х", "%D1%86"=>"ц", "%D0%A6"=>"Ц",
      "%D1%87"=>"ч", "%D0%A7"=>"Ч", "%D1%88"=>"ш", "%D0%A8"=>"Ш", "%D1%89"=>"щ", "%D0%A9"=>"Щ", "%D1%8A"=>"ъ",
      "%D0%AA"=>"Ъ", "%D1%8B"=>"ы", "%D0%AB"=>"Ы", "%D1%8C"=>"ь", "%D0%AC"=>"Ь", "%D1%8D"=>"э", "%D0%AD"=>"Э",
      "%D1%8E"=>"ю", "%D0%AE"=>"Ю", "%D1%8F"=>"я", "%D0%AF"=>"Я", "%C2%BB"=>"»", "%C2%AB"=>"«","%d0%b0"=>"а",
      "%d0%90"=>"А", "%d0%b1"=>"б", "%d0%91"=>"Б", "%d0%b2"=>"в", "%d0%92"=>"В", "%22"=>"'", "%E2%80"=>'"',
      "%d0%b3"=>"г", "%d0%93"=>"Г", "%d0%b4"=>"д", "%d0%94"=>"Д", "%d0%b5"=>"е", "%d0%95"=>"Е", "%d1%91"=>"ё",
      "%d0%81"=>"Ё", "%d0%b6"=>"ж", "%d0%96"=>"Ж", "%d0%b7"=>"з", "%d0%97"=>"З", "%d0%b8"=>"и", "%d0%98"=>"И",
      "%d0%b9"=>"й", "%d0%99"=>"Й", "%d0%ba"=>"к", "%d0%9a"=>"К", "%d0%bb"=>"л", "%d0%9b"=>"Л", "%d0%bc"=>"м",
      "%d0%9c"=>"М", "%d0%bd"=>"н", "%d0%9d"=>"Н", "%d0%be"=>"о", "%d0%9e"=>"О", "%d0%bf"=>"п", "%d0%9f"=>"П",
      "%d1%80"=>"р", "%d0%a0"=>"Р", "%d1%81"=>"с", "%d0%a1"=>"С", "%d1%82"=>"т", "%d0%a2"=>"Т", "%d1%83"=>"у",
      "%d0%a3"=>"У", "%d1%84"=>"ф", "%d0%a4"=>"Ф", "%d1%85"=>"х", "%d0%a5"=>"Х", "%d1%86"=>"ц", "%d0%a6"=>"Ц",
      "%d1%87"=>"ч", "%d0%a7"=>"Ч", "%d1%88"=>"ш", "%d0%a8"=>"Ш", "%d1%89"=>"щ", "%d0%a9"=>"Щ", "%d1%8a"=>"ъ",
      "%d0%aa"=>"Ъ", "%d1%8b"=>"ы", "%d0%ab"=>"Ы", "%d1%8c"=>"ь", "%d0%ac"=>"Ь", "%d1%8d"=>"э", "%d0%ad"=>"Э",
      "%d1%8e"=>"ю", "%d0%ae"=>"Ю", "%d1%8f"=>"я", "%d0%af"=>"Я", "%c2%bb"=>"»", "%c2%ab"=>"«", "%e2%80"=>'"',
      "%b3"=>"", "%B3"=>"", "%28"=>"(", "%29"=>")", "%D1%96"=>"i", "%d1%96"=>"i"];

    /**`
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:searchString';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсер apache логов чтоб сохранить логи поисковых запросов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $files = glob('c:\www\log\tmp\itsellopt.com.ua.log.*');

      foreach( $files as $file ) {
        $result = $this->parser($file);

        $this->comment('Начало чтения файла ' . $file);

        $data = $this->readFromDB();

        foreach ($result as $item) {
          if ($data->has($item)) {
            $data->get($item)->count += 1;
          } else {
            $data->put($item, new SearchModel(['query' => $item, 'count' => 1]));
          }
        }

        $this->comment('Записываем в БД');
        DB::transaction(fn() => $data->each->save());
      }

      $this->comment('Готово');
    }

  protected function parser(string $filePath)
  {
    if (! is_file($filePath)) return false;

    $file = @file_get_contents($filePath);

    if (!$file) return false;

    $content = explode('"', $file);

    $result = [];
    $prev = '';

    foreach ($content as $row) {
      if (!Str::contains($row, 'GET /search/')) continue;

      $tmp = Str::between($row, 'GET /search/', 'HTTP');

      if (Str::contains($tmp, '/filter/')) continue;
      if (Str::contains($tmp, '?utm_source')) continue;

      $tmp = $this->url_decode($tmp);

      if (Str::contains($tmp, $prev) && Str::contains($tmp, '?type=preview')) continue;

      $tmp = Str::before($tmp, '?type=preview');
      $tmp = Str::before($tmp, '?type=json');
      $tmp = Str::before($tmp, '?fn=');
      $tmp = trim($tmp);

      $prev = $tmp;

      $result[] = $tmp;
    }

    return $result;
  }

  protected function url_decode(string $string)
  {
//    $string = strtr($string, $this->symbol);
    $string = rawurldecode($string);
    //$string = htmlentities($string);
    //$string = strtr($string, ['\xF0\x9F\x8C\x9F' => '']);
    return trim($string);
  }

  protected function readFromDB()
  {
    return SearchModel::get()->keyBy('query');
  }

  protected function writeToBd(string $searchPhrase)
  {
    SearchModel::updateOrCreate(['query' => $searchPhrase])->increment('count');
  }
}
