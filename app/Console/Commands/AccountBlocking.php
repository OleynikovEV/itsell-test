<?php

namespace App\Console\Commands;

use App\Mail\AccountBlockingMail;
use App\Mail\NotificationUsersBlockMail;
use App\Models\ManagersInformations;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AccountBlocking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:block-by-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Блокировка пользователей по сроку давности';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $dateFilter = now()->subMonths(4)->format('Y-m-d H:i:s');

      $users = User::where('permissions', 3)
        ->where('unactive', 0)
        ->where(fn($q) => $q->where('cart_updated_at', '<', $dateFilter)->orWhere('cart_updated_at', null))
        ->limit(100)
        ->get(['id','permissions', 'unactive', 'created_at', 'cart_updated_at', 'email',
          'login', 'fio', 'phone', 'company', 'manager']);

      $managers = ManagersInformations::get()->keyBy('id');

      foreach( $users as $user ) {
        // блокируем пользователя
        $user->unactive = 1;

        if ( validator(['email' => $user->email], ['email' => 'email'])->fails() ) continue;

        Mail::to($user->email)->send(new AccountBlockingMail($user));
      }

      if ($users->count() > 0) {
        DB::transaction(fn() => $users->each->save());

        $users = $users->groupBy('manager');

        foreach ($users as $id => $items) {
          foreach ($items->chunk(50) as $item) {
            $email = config('custom.mail_recipients.order_created');

            if ($managers->has($id)) {
              $email = $managers->get($id)->email;
            }
//            Mail::to(config('custom.mail_recipients.abandoned_cart'))->send(new NotificationUsersBlockMail($items));
//            Mail::to($email)->cc('deluxe2006@i.ua')->send(new NotificationUsersBlockMail($item));
            Mail::to($email)->send(new NotificationUsersBlockMail($item));
          }
        };
      }

      $this->info('Completed!');
    }
}
