<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\ProductsSeries;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProductsSeriesUpdateCounters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products-series:update-counters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update products counters for all series';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Подсчет количества товаров в каждой серии
        $counters = Product::active()
            ->selectRaw('products_series_id, COUNT(*) AS count')
            ->groupBy('products_series_id')
            ->get()
            ->pluck('count', 'products_series_id');

        // Получение списка самих серий и обновление данных в них
        $seriesList = ProductsSeries::all();

        DB::transaction(function() use ($seriesList, $counters)
        {
            foreach ($seriesList as $series)
            {
                $series->products_count = $counters->get($series->id) ?: 0;
                $series->save();
            }
        });

        // Отображение успешного статуса
        $this->info('Completed!');
    }
}
