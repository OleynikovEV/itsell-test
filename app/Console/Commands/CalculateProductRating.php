<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CalculateProductRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:calculate_rating';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'calculate product rating';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $this->comment('... start ...');
      $startDate = Carbon::now()->subDays(config('custom.rating_interval'))->timestamp;
      $endDate = Carbon::now()->timestamp;

      $products = DB::table('its_orders', 'o')
        ->selectRaw('IF(p.parent = 0, p.id, p.parent) AS parent, COUNT(p.id) AS `rating`')
        ->leftJoin('its_order_products AS op', 'op.order_id', '=', 'o.id')
        ->leftJoin('its_products AS p', 'p.id', '=', 'op.product_id')
        ->whereBetween('o.time', [$startDate, $endDate])
        ->where('p.qty', '>', 0)
        ->groupBy(DB::raw('IF(p.parent = 0, p.id, p.parent)'))
        ->orderBy('parent');

      $productsIds = [];

      $products->chunk(1000, function($items) use (&$productsIds) {
        $this->comment('... get products for update ...');
        $updateProducts = collect();
        $ids = $items->pluck('parent');
        $products = Product::whereIn('id', $ids)->get(['id', 'priority'])->keyBy('id');

        foreach ($items as $product) {
          if ($products->has($product->parent)) {
            $productItem = $products->get($product->parent);
            $productItem->priority = $product->rating;
            $updateProducts->push($productItem);
          }

          $productsIds[] = $product->parent;
        }

        DB::transaction(fn() => $updateProducts->each->save());

        $this->comment('... chunk ...');
      });

      $this->comment('... set priority = 0 for other products ...');
      // обнулить все рейтинги у товаров которые не были куплены за указанный период
      DB::table('its_products')
        ->where('parent', 0)
        ->whereNotIn('id', $productsIds)
        ->update(['priority' => 0]);

      $this->comment('... finish ...');
    }
}
