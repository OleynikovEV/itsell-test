<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Product;
use App\Mail\AbandonedCartMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CartSendAbandonedNotifications extends Command
{

    protected $signature = 'cart:send-abandoned-notifications';
    protected $description = 'Send abandoned cart notifications';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cc = config('custom.mail_recipients.order2_created');

        for ($i = 1; $i <= 2; $i++) {
            // Получение списка пользователей для отправки уведомления
            $periodStart = now()->startOfHour()->subHour($i == 1 ? 25 : 49);
            $periodEnd = $periodStart->copy()->addHour();

            $users = User::where('cart', '!=', '')
                ->where('cart_updated_at', '>=', $periodStart)
                ->where('cart_updated_at', '<', $periodEnd)
//                ->where('id', 14454)
                ->with('managerOpt')
                ->get();

            foreach ($users as $user) {
                $managerEmail = '';

                // Если текущее сообщение отправляется покупателю - проверяем валидность его email'а
                if ($i == 1 && validator(['email' => $user->email], ['email' => 'email'])->fails()) continue;

                if ($i === 2) {
                  $manager = $user->managerOpt;

                  if (empty($manager)) {
                    $managerEmail = config('custom.mail_recipients.order_created');
                  } else {
                    $managerEmail = $manager->email;
                  }
                }

                // Получение корзины пользователя
                $carts = unserialize($user->cart);

                if (empty($carts) || count($carts) === 0) continue;

                // Получение информации о товарах в корзине
                foreach ($carts as $cartId => $cart) {
                  $products = Product::whereIn('id', $cart->pluck('id'))->get()->keyBy('id');

                  $cart = $cart->map(function ($item) use ($products) {
                    $item->product = $products->get($item->id);
                    return $item;
                  });

                  // Просчет итоговой стоимости товаров в корзине
                  $cartTotal = $cart->sum(fn($item) => round($item->price * $item->qty, 2));

                  // Отправка email-уведомления
//                  $recipients = $i == 1 ? $user->email : config('custom.mail_recipients.abandoned_cart');
                  $recipients = $i == 1 ? $user->email : $managerEmail;
                  Mail::to($recipients)->send(new AbandonedCartMail($user, $cart, $cartTotal, $i, $cartId));
                }
            }
        }

        // Отображение успешного статуса
        $this->info('Completed!');
    }
}
