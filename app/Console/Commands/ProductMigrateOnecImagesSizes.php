<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProductMigrateOnecImagesSizes extends Command
{
    protected $signature = 'product:migrate-onec-images-sizes';
    protected $description = 'Migrate Onec images sizes to new format from separate table';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $chunks = 0;

        Product::chunkById(500, function ($products) use (&$chunks) {
            $images = DB::table('its_products_images')
                ->whereIn('product_code', $products->pluck('code'))
                ->get()
                ->keyBy('product_code');
            
            foreach ($products as $product) {
                if ($productImages = $images->get($product->code)) {
                    $sizes = [];

                    for ($i = 0; $i < 8; $i++) {
                        $sizes[] = $productImages->{'img' . ($i > 0 ? '_' . $i : '') . '_size'};
                    }
                    
                    $product->onec_images_sizes = $sizes;
                }
            }

            DB::transaction(fn() => $products->each->save());

            $chunks++;
            $this->comment('Processed chunk #' . $chunks);
        });
    }
}
