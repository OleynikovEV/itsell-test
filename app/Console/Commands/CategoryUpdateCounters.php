<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CategoryUpdateCounters extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'category:update-counters';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update products counters for all categories';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    // Получение списка всех категорий
    $categoriesGroups = Category::all()->groupBy('parent_id')->map(function($categories)
    {
      return $categories->keyBy('id')->map(function($category)
      {
        $category->products_count = 0;
        $category->products_count_new = 0;
        $category->products_count_coming = 0;

        return $category;
      });
    });

    // Получение счетчиков количества товаров
    $counters = Product::active()
      ->selectRaw('menu, COUNT(*) AS count, 
              SUM(IF(added_time > ' . today()->subDays(Product::NOVELTY_DAYS)->timestamp . ', 1, 0)) AS count_new, ' .
        'SUM(IF(date_coming > "' . today()->subDays(Product::COMING_DAYS)->format('Y-m-d') . '", 1, 0)) AS count_coming'
      )
      ->where('from', '<>', 1)
      ->groupBy('menu')
      ->get()
      ->keyBy('menu');

    // Получение количества товаров, подключенных через поле Дополнительные разделы
    $additionalCategories = DB::table('its_product_category')
      ->join('its_products', 'its_products.id', '=', 'its_product_category.product_id')
      ->selectRaw('category_id AS menu, COUNT(*) AS count, 
              SUM(IF(added_time > ' . today()->subDays(Product::NOVELTY_DAYS)->timestamp . ', 1, 0)) AS count_new, ' .
        'SUM(IF(date_coming > "' . today()->subDays(Product::COMING_DAYS)->format('Y-m-d') . '", 1, 0)) AS count_coming'
      )
      ->where('from', '<>', 1)
      ->whereRaw('(its_products.qty > 0 OR its_products.when_appear)')
      ->where('parent', 0)
      ->where('its_products.active', true)
      ->where(function($q)
      {
        $q->where('qty', '>', 0)->orWhere('when_appear', '!=', '');
      })
      ->groupBy('category_id')
      ->get();

    foreach ($additionalCategories as $category) {
//            $counter = $counters->get(+$category->menu);
      $counter = $counters->get($category->menu);
      if ($counter) {
        $counter->count += $category->count;
        $counter->count_new += $category->count_new;
        $counter->count_coming += $category->count_coming;
      }
      else $counter = $category;

      $counters->put($category->menu, $counter);
    }

    // Обновление данных в категориях
    DB::transaction(function() use ($categoriesGroups, $counters) {
      foreach ($categoriesGroups as $parentId => $categories) {
        if ( ! $parentId) continue;

        foreach ($categories as $category) {
          // Получение количества товаров в текущей категории или связанной категории
          $has = false;
          $data = null;
          if ($counters->has($category->id)) {
            $has = true;
            $data = $counters->get($category->id);
          } elseif (!empty($category->include_categories)) {
            $parentCategories = explode(',', $category->include_categories);
            foreach ($parentCategories as $item) {
              if ($counters->has($item)) {
                $has = true;
                $data = $counters->get($item);
                break;
              }
            }
          }

          if ($has) {
            $category->products_count += $data->count;
            $category->products_count_new += $data->count_new;
            $category->products_count_coming += $data->count_coming;

            // Увеличение счетчиков родительской категории
            if ($parentId && $categoriesGroups->get(0)->get($parentId))
            {
              $categoriesGroups->get(0)->get($parentId)->products_count += $category->products_count;
              $categoriesGroups->get(0)->get($parentId)->products_count_new += $category->products_count_new;
              $categoriesGroups->get(0)->get($parentId)->products_count_coming += $category->products_count_coming;
            }
          }

          // Если в категорию включены товары из других категорий - плюсуем их к счетчикам
          if ($category->include_categories)
          {
            foreach (explode(',', $category->included_categories) as $categoryId)
            {
              if ($counters->has($categoryId))
              {
                $category->products_count += $counters->get($categoryId)->count;
                $category->products_count_new += $counters->get($categoryId)->count_new;
                $category->products_count_coming += $counters->get($categoryId)->count_coming;
              }
            }
          }

          // Сохранение категорию
          $category->has_products = $category->products_count > 0;
          $category->save();
        }
      }

      // Сохранение списка родительских категорий
      foreach ($categoriesGroups->get(0) as $category)
      {
        $category->has_products = $category->products_count > 0;
        $category->save();
      }
    });

    // Возврат успешного статуса
    $this->info('Completed!');
  }
}
