<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DumpDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'клонирование БД';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $DB_HOST = config('database.connections.mysql.host');
      $DB_USER = config('database.connections.mysql.username');
      $DB_PASS = config('database.connections.mysql.password');

//      $DB_SRC = config('database.connections.mysql.database');
      $DB_SRC = 'itsellopt_db'; // ОТКУДА КОПИРУЕМ

      $DB_DST = 'itsellopt_test'; // КУДА КОПИРУЕМ

      // MYSQL Connect
      $this->comment('Соединение с БД');
      $mysqli = new \mysqli( $DB_HOST, $DB_USER, $DB_PASS ) or die( $mysqli->error );

      // Create destination database
      //$this->comment('Создание БД');
      //$mysqli->query( "CREATE DATABASE $DB_DST" ) or die( $mysqli->error );

      // Iterate through tables of source database
      $tables = $mysqli->query( "SHOW TABLES FROM $DB_SRC" ) or die( $mysqli->error );

      while( $table = $tables->fetch_array() ):
        $TABLE = $table[0];
        // Copy table and contents in destination database
        $this->comment("Копирование таблицы $DB_DST.$TABLE");
        $mysqli->query( "DROP TABLE IF EXISTS $DB_DST.$TABLE" ) or die( $mysqli->error );
        $mysqli->query( "CREATE TABLE $DB_DST.$TABLE LIKE $DB_SRC.$TABLE" ) or die( $mysqli->error );
        $mysqli->query( "INSERT INTO $DB_DST.$TABLE SELECT * FROM $DB_SRC.$TABLE" ) or die( $mysqli->error );
      endwhile;

      $this->comment('Конец работы клонирования');
    }
}
