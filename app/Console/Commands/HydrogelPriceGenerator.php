<?php

namespace App\Console\Commands;

use App\Http\Models\PriceGenerator as PG;
use App\Http\Models\PriceGeneratorHelper as Helper;
use App\Http\Models\PriceGeneratorLogs;
use Illuminate\Console\Command;

class HydrogelPriceGenerator extends Command
{
  protected $signature = 'pg:hydrogel';
  protected $description = 'Генератор прайсов для Защитная гидрогелевая пленка SKLO';

  public function handle()
  {
    $pgLog = new PriceGeneratorLogs('pg:hydrogel', true);
    $pgLog->start();

    $products = Helper::getHydrogel();
    $generator = new PG();
    $generator->setCurrency('uah');
    $generator->setPriceType(['price']);
    $result = $generator->getProducts(1, $products, false);
    $categories = $generator->getCategories(1, $result['categories_ids'], $result['categories_products_types']);
    $generator->saveAsXml($categories, $result['products'], false, 'price_lists', 'hydrogel');

    $pgLog->finish()->save();
  }
}