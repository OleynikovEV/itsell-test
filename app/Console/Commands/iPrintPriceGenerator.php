<?php


namespace App\Console\Commands;

use App\Http\Models\IPrintPriceGenerator as IPrintPriceGeneratorModel;
use App\Http\Models\PriceGeneratorHelper;
use App\Http\Models\PriceGeneratorLogs;
use Illuminate\Console\Command;

class iPrintPriceGenerator extends Command
{
  protected $signature = 'pg:iprint';
  protected $description = 'Генератор прайсов для iPrint';

  public function handle()
  {
    $this->pgLog = new PriceGeneratorLogs('IPrint', true);
    $products = PriceGeneratorHelper::getAllPrint();

    $this->pgLog->start();
    $iprint = new IPrintPriceGeneratorModel('usd', 'opt_price', $products, 3);
    $iprint->saveAsXml('iPrint_opt_price_t3_usd');
    $iprint->saveAsCsv('iPrint_opt_price_t3_usd');
    $this->pgLog->setPriceName('IPrint opt_price xml');
    $this->pgLog->finish()->save();

    $this->pgLog->start();
    $iprint = new IPrintPriceGeneratorModel('uah', 'price', $products, 3);
    $iprint->saveAsXml('iPrint_ppc_price_t3_uah');
    $this->pgLog->setPriceName('IPrint price xml');
    $iprint->saveAsCsv('iPrint_ppc_price_t3_uah');
    $this->pgLog->finish()->save();
  }
}