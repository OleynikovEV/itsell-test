<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Waybill;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Novaposhta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'np:tracking';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Трекинг посылки по ТТН';

    protected $apiUrl = 'https://api.novaposhta.ua/v2.0/json/';

    public function handle()
    {
        $this->comment('Начало обновления данных __');
        try {
            $client = new GuzzleClient();
            $i = 10;

            Waybill::where('created_at', '>', '2020-09-01')
                ->where(function($q) {
                    $q->where('status_code', '<>',9) // Відправлення отримано
                        ->where('status_code', '<>', 105) // Припинено зберігання
                    ->orWhereRaw('status_code IS NULL');
                })
                ->chunk(50, function ($waybills) use ($client, $i) {
                    $this->comment($i . ' - партия');
                    $i += 10;

                    $data = [
                        'apiKey' => '',
                        'modelName' => 'TrackingDocument',
                        'calledMethod' => 'getStatusDocuments',
                        'methodProperties' => [
                            'Documents' => []
                        ]
                    ];

                    foreach ($waybills as $item) {
                        $TTH = [
                            'DocumentNumber' => $item->waybill,
                            'Phone' => ''
                        ];

                        array_push($data['methodProperties']['Documents'], $TTH);
                    }

                    try {
                        $response = $client->post($this->apiUrl, [
                            'headers' => ['Content-Type' => 'application/json'],
                            'json' => $data
                        ]);

                        $response = json_decode($response->getBody());
                        if ($response && !$response->success && $response->errors) {
                            if ($response->errors[0] == 'API auth fail') $response->errors[0] = 'Ключ API недействителен';
                            if ($response->errors[0] == 'API key expired') $response->errors[0] = 'Истек срок действия ключа API';
                        }

                        if ($response->success == true) {
                            foreach ($response->data as $item) {
                                echo $item->Number;
                                $waybill = Waybill::where('waybill', $item->Number)->first();
                                $waybill->scheduled_delivery_date =  (isset($item->ScheduledDeliveryDate) && strlen(trim($item->ScheduledDeliveryDate)))
                                    ? date('Y-m-d H:i', strtotime($item->ScheduledDeliveryDate)) : null;
                                $waybill->recipient_date_time = (isset($item->RecipientDateTime) && strlen(trim($item->RecipientDateTime)) > 0)
                                    ? date('Y-m-d H:i', strtotime($item->RecipientDateTime)) : null;
                                $waybill->date_return_cargo = (isset($item->DateReturnCargo) && strlen(trim($item->DateReturnCargo)) > 0)
                                    ? date('Y-m-d H:i', strtotime($item->DateReturnCargo)) : null;
                                $waybill->status = $item->Status;
                                $waybill->status_code = $item->StatusCode;
                                if ($waybill->save()) {
                                  // обновляем статус и в заказе
                                  DB::table('its_orders')
                                    ->where('id', $waybill->order_id)
                                    ->update(['status_code_np' => $item->StatusCode]);
                                    echo ' - Update' . PHP_EOL;;
                                } else {
                                    echo ' - Error' . PHP_EOL;
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        logger('An error is occurred while making Novaposhta API call', ['error' => $e->getMessage()]);
                        return false;
                    }
                    $this->comment('конец обработки партии');
                });
            $this->comment('конец ___');
        }catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }
}
