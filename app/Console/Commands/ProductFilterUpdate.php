<?php

namespace App\Console\Commands;

use App\Models\FilterValueRelationships;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductFilterUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:product_filter_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Добавляем в доп таблицу все фильтры';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $this->comment('... Start ... ' . now()->format('H:i:s'));

      $chunk = 0;

      $filters = DB::table('its_filters_values')
        ->select(['id', 'filter_id'])
        ->get()
        ->keyBy('id');

      $categories = DB::table('its_filters_values')
        ->select(['id'])
        ->where('filter_id', 84)
        ->get()
        ->keyBy('id')
        ->toArray();

      DB::table('its_product_filter')->truncate();

      Product::select(['id',  'active', 'qty', 'when_appear', 'filters_values', 'addit_menus',
        'added_time', 'date_coming', 'parent'])
        ->where('active', true)
        ->where('parent', 0)
        ->where(function($q) {
          $q->where('qty', '>', 0)
            ->orWhere('when_appear', '!=', '');
        })
//        ->whereIn('id', [255822,262472,210141])
        ->orderBy('id')
        ->chunk(1000, function($products) use ($filters, $categories, &$chunk) {
          $this->comment('... chunk ' . (++$chunk) . '...');

          $dataUpdate = [];

          foreach ($products as $product) {

            $filterValues = $product->filters_values;

            // объединяем стекла и пленки
            if ( in_array(258, $filterValues) ||
                 in_array(257, $filterValues)
            ) {
              $steklaIndex = array_search(257, $filterValues);
              if ($steklaIndex !== false) unset($filterValues[$steklaIndex]);

              $plenkiIndex = array_search(258, $filterValues);
              if ($plenkiIndex !== false) unset($filterValues[$plenkiIndex]);

              array_push($filterValues, 513);
            }

            $categoryId = 0;

            // к какой категории относится товара (чехлы, стекал, техничка ...)
            // @TODO: переписать поиск
            foreach ($filterValues as $item) {
              if (isset($categories[$item])) {
                $categoryId = $item;
                break;
              }
            }

            // определяем для товара такой блок фильтров как Другое (уценка, скидка, новинка ...)
            $novelty = $product->isNovelty(); // новинки
            $coming = $product->isComing(); // приход

            // другие фильтры
            foreach ($filterValues as $value) {
              if (! $filters->has($value)) continue;

              if ($categoryId == $value) continue;

              $parentFilterId = $filters->get($value)->filter_id;

              $dataUpdate[] = [
                'product_id' => $product->id,
                'category_id' => $categoryId,
                'filter_value_id' => $value,
                'novelty' => $novelty,
                'coming' => $coming,
              ];
            }
          }

          $productIds = $products->pluck('id');

          $this->comment('... clear ...');
          DB::table('its_product_filter')->whereIn('product_id', $productIds)->delete();
          $this->comment('... insert ...');
          DB::table('its_product_filter')->insert($dataUpdate);
        });

      $this->comment('... filnish ... ' . now()->format('H:i:s'));
    }
}
