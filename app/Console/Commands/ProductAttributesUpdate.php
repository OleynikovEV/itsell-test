<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProductAttributesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:product_attribute_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $this->comment('... Start ... ' . now()->format('H:i:s'));

      $chunk = 0;

      $categories = DB::table('its_filters_values')
        ->select(['id'])
        ->where('filter_id', 84)
        ->get()
        ->keyBy('id')
        ->toArray();

      $brands = DB::table('its_menu')
        ->get(['id', 'parent_id', 'menu_sector'])
        ->keyBy('id');

//      DB::table('its_product_ref')->truncate();

      Product::select(['id', 'active', 'qty', 'when_appear', 'brand', 'filters_values', 'menu',
        'addit_menus', 'products_series_id', 'added_time', 'date_coming', 'parent', 'opt_price'])
        ->with('additionalCategories.parentCategory:id,parent_id')
        ->where('active', true)
        ->where('parent', 0)
        ->where(function($q) {
          $q->where('qty', '>', 0)
            ->orWhere('when_appear', '!=', '');
        })
//        ->whereIn('id', [255822,262472,210141])
        ->orderBy('id')
        ->chunk(500, function($products) use ($categories, $brands, &$chunk) {
          $this->comment('... chunk ' . (++$chunk) . '...');

          $dataUpdate = [];

          foreach ($products as $product) {
            $filterValues = $product->filters_values;

            // объединяем стекла и пленки
            if ( in_array(258, $filterValues) ||
                 in_array(257, $filterValues)
            ) {
              $steklaIndex = array_search(257, $filterValues);
              if ($steklaIndex !== false) unset($filterValues[$steklaIndex]);

              $plenkiIndex = array_search(258, $filterValues);
              if ($plenkiIndex !== false) unset($filterValues[$plenkiIndex]);

              array_push($filterValues, 513);
            }

            $categoryId = 0;

            // к какой категории относится товара (чехлы, стекал, техничка ...)
            // @TODO: переписать поиск
            foreach ($filterValues as $item) {
              if (isset($categories[$item])) {
                $categoryId = $item;
                break;
              }
            }

            if (! $brands->has($product->menu)) continue;

            $brand = $brands->get($product->menu);

            // фильтры
            $dataUpdate[] = [
              'product_id' => $product->id,
              'category_id' => $categoryId,
              'brand_id' => $brand->parent_id,
              'sector_id' => $brand->menu_sector,
              'model_id' => $product->menu,
              'brand_manufacturer_id' => $product->brand ?? 0,
              'novelty' => $product->isNovelty(),
              'coming' => $product->isComing(),
              'opt_price' => $product->opt_price,
            ];

            // Составной ключ, для проверки, чтобы не было дублей при сохранении данных из доп. категорий
            $composite_key = $product->id . $brand->parent_id;

            // дополнительные категории
            if ( $product->additionalCategories->count() > 0 ) {
              foreach ($product->additionalCategories as $item) {

                if ($item->parentCategory === null) {
                  $brand_id = ($item->parent_id === 0)
                    ? $item->id
                    : $item->parent_id;
                } else {
                  $brand_id = ($item->parentCategory->parent_id === 0)
                    ? $item->parentCategory->id
                    : $item->parentCategory->parent_id;
                }

                // пропускаем дубли
                if ($composite_key === $product->id . $brand_id) continue;

                $dataUpdate[] = [
                  'product_id' => $product->id,
                  'category_id' => $categoryId,
                  'brand_id' => $brand_id,
                  'sector_id' => $brand->menu_sector,
                  'model_id' => $item->id,
                  'brand_manufacturer_id' => $product->brand ?? 0,
                  'novelty' => $product->isNovelty(),
                  'coming' => $product->isComing(),
                  'opt_price' => $product->opt_price,
                ];
              }
            }
          }

          $productIds = $products->pluck('id');

          $this->comment('... clear ...');
          DB::table('its_product_ref')->whereIn('product_id', $productIds)->delete();
          $this->comment('... insert ...');
          DB::table('its_product_ref')->insert($dataUpdate);
        });

      $this->comment('... filnish ... ' . now()->format('H:i:s'));
    }
}
