<?php

namespace App\Console\Commands;

use App\Http\Models\PriceGenerator as PG;
use App\Http\Models\PriceGeneratorHelper as Helper;
use App\Http\Models\PriceGeneratorLogs;
use Illuminate\Console\Command;

class BazzillaPriceGenerator extends Command
{
  protected $signature = 'pg:bazzilla';
  protected $description = 'Генератор прайсов для Bazzilla';

  public function handle()
  {
    $pgLog = new PriceGeneratorLogs('pg:bazzilla xml', true);
    $pgLog->start();

    $products = Helper::getAllProducts();
    $generator = new PG();
    $generator->setCurrency('uah');
    $generator->setPriceType(['price', 'drop_uah']);
    $result = $generator->getProducts(1, $products, false);
    $categories = $generator->getCategories(1, $result['categories_ids'], $result['categories_products_types']);
    $generator->saveAsXml($categories, $result['products'], false, 'price_lists', 'general_price_bazzilla');

    $pgLog->finish()->save();
  }
}