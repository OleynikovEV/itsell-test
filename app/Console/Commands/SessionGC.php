<?php

namespace App\Console\Commands;

use Illuminate\Support\Arr;
use Illuminate\Console\Command;

class SessionGC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'session:gc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Garbage collector for expired sessions';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $session = $this->getLaravel()->make('session');
        $lifetime = Arr::get($session->getSessionConfig(), 'lifetime') * 60;
        $session->getHandler()->gc($lifetime);

        $this->info('Completed!');
    }
}