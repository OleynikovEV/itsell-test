<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportPrintPrice extends Command
{
  protected $signature = 'import:prints {--debug=false}';
  protected $description = 'Импорт прайса принтов';

//  protected $url = 'https://prints.vchehle.ua/storage/price_lists/prints.yml';
  protected $url = 'https://prints.vchehle.ua/feeds/YZ5asH/8Ky39R954MxopGYM69BrfirHtypjkGgG.yml';
//  protected $url = 'C:/Users/eugen/Desktop/scren/iprint.xml';

  public function handle()
  {
    $debug = $this->option('debug') == 'true';

    $import = new \App\Http\Models\ImportPrintPrice($this->url, $debug);
    $import->update();
  }
}