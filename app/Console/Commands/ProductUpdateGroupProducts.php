<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProductUpdateGroupProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
      protected $signature = 'product:update-group-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update group products cache data in parent products';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunks = 0;

        Product::where('parent', 0)
            ->where('from', 0)
            ->with('groupProductsList')
            ->orderBy('id', 'DESC')
            ->chunk(500, function($products) use (&$chunks) {
                DB::transaction(function() use ($products) {
                    foreach ($products as $product) {
                        $groupProducts = [];
                        $groupProductsList = $product->groupProductsList;
                        $maxDateComing = $groupProductsList->max('date_coming');
                        $maxDateComing = ($maxDateComing === '0000-00-00') ? null : $maxDateComing;

                        foreach ($groupProductsList as $groupProduct) {
                            // получаем min, max цены
                            $range = $groupProduct->getPriceRange();
                            if (count($range) == 1) {
                                $minPrice = $maxPrice = $range[0];
                            } else {
                                list($minPrice, $maxPrice) = $range;
                            }

                            $colorData = [
                                'id' => $groupProduct->id,
                                'color' => $groupProduct->color,
                                'name' => ['ru' => $groupProduct->color_name],
                                'image' => $groupProduct->img,
                                'min_price' => $minPrice,
                                'max_price' => $maxPrice,
                                'price' => $groupProduct->price,
                                'opt_minopt_usd' => $groupProduct->getPrice('opt_minopt_usd'),
                                'opt_price' => $groupProduct->opt_price,
                                'opt_vip_usd' => $groupProduct->getPrice('opt_vip_usd'),
                                'opt_dealer_usd' => $groupProduct->getPrice('opt_dealer_usd'),
                                'old_opt_price' => salePrice($groupProduct->opt_price, $groupProduct->old_opt_price, $groupProduct->price_modify_time),
                                'drop_uah' => $groupProduct->getPrice('drop_uah'),
                                'qty' => $groupProduct->qty,
                                'when_appear' => $groupProduct->qty <= 0 ? $groupProduct->when_appear : '',
                                'date_coming' => ($groupProduct->date_coming === '0000-00-00') ? null : $groupProduct->date_coming,
                            ];

                            // foreach (config('app.supported_locales') as $locale)
                            // {
                            //     if ($locale != config('app.locale'))
                            //     {
                            //         $translation = $groupProduct->translate($locale);
                            //         if ($translation && $translation->color_name)
                            //         {
                            //             $colorData['name'][$locale] = $translation->color_name;
                            //         }
                            //     }
                            // }

                            $groupProducts[] = $colorData;
                        }

                        $date_coming = ($product->date_coming === '0000-00-00') ? null : $product->date_coming;
                        $product->date_coming = ($maxDateComing > $product->date_coming) ? $maxDateComing : $date_coming;
                        $product->group_products = $groupProducts;
                        try {
                          $product->save();
                        } catch (\Exception $ex) {
                          dd($date_coming, $maxDateComing);
                        }
                    }
                });

                $chunks++;
                $this->comment('Processed chunk #' . $chunks);
            });
    }
}
