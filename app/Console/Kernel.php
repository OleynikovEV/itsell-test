<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Предпросчет счетчиков с количеством товаров в категориях, брендах и сериях товаров
        $schedule->command('category:update-counters')->everyFifteenMinutes();

        $schedule->command('brand:update-counters')->hourlyAt(5);
        $schedule->command('brand:update-counters')->hourlyAt(20);
        $schedule->command('brand:update-counters')->hourlyAt(35);
        $schedule->command('brand:update-counters')->hourlyAt(50);

        $schedule->command('products-series:update-counters')->hourlyAt(10);
        $schedule->command('products-series:update-counters')->hourlyAt(25);
        $schedule->command('products-series:update-counters')->hourlyAt(40);
        $schedule->command('products-series:update-counters')->hourlyAt(55);

        // блокировка клиентов
        $schedule->command('users:block-by-time')->daily()->at('12:00');

        // Отправка напоминаний о брошенных корзинах
        $schedule->command('cart:send-abandoned-notifications')->hourly();

        // Сборщик мусора в сессиях
        $schedule->command('session:gc')->twiceDaily();

        // Бекапы
        $schedule->command('backup:clean')->daily()->at('02:50');
        $schedule->command('backup:run')->daily()->at('03:00');

        // Обновление статусов посылок из НП
        $schedule->command('np:tracking')->twiceDaily(13, 19);

        // Импорт принтов
        $schedule->command('import:prints')->daily()->at('8:30');
        $schedule->command('import:prints')->daily()->at('12:30');
        $schedule->command('import:prints')->daily()->at('16:30');
        $schedule->command('import:prints')->daily()->at('20:30');

//         генерация прайсов
        $schedule->command('pg:iprint')->daily()->at('9:25');
        $schedule->command('pg:iprint')->daily()->at('13:25');
        $schedule->command('pg:iprint')->daily()->at('17:25');
        $schedule->command('pg:iprint')->daily()->at('21:25');

        $schedule->command('pg:hydrogel')->hourly();
        $schedule->command('pg:bazzilla')->hourly();
        $schedule->command('pg:evgphones')->hourly();
        $schedule->command('pg:rozetka')->hourly();

        $schedule->command('product:update-group-products')->daily()->at('01:00');
        $schedule->command('products:calculate_rating')->daily()->at('00:15');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
