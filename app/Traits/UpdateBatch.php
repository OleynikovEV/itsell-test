<?php

namespace App\Traits;

trait UpdateBatch
{

  static protected $chunkLimit = 100;

  static public function updateBatch(array $data, ?string $updateKey = null)
  {
    $instance = new static;
    $table = $instance->table;
    $key = ($updateKey !== null) ? $updateKey : $instance->getKeyName();

    $collect = collect($data);

    foreach ($collect->chunk(self::$chunkLimit) as $items) {
      $querys = $instance->getSql($table, $key, $items->toArray());
      $instance->getConnection()->transaction(function() use ($instance, $querys) {
//        dd( get_class_methods( $instance->getConnection() ) );
        $instance->getConnection()->unprepared(
          $instance->getConnection()->raw($querys)
        );
      });
    };
  }

  private function getSql($table, $primaryKey, array $data): string
  {
    $query = [];

    foreach ($data as $key => $items) {
      if (!isset($items[$primaryKey])) throw new \Exception('Column ' . $primaryKey . ' not found Index = ' . $key);

      $values = [];

      foreach ($items as $column => $value) {
        if ($primaryKey === $column) continue;
        $values[] = sprintf('`%s`=%s',
          $column,
          (is_numeric($value) ? $value : '"' . $value . '"')
        );
      }

      $query[$key] = sprintf('UPDATE `%s` SET %s WHERE `%s` = %s;',
        $table,
        implode(', ', $values),
        $primaryKey,
        $items[$primaryKey]
      );
    }

    return implode(' ', $query);
  }
}