<?php

namespace App\Traits;

use App\Http\Models\UrlParser;
use App\View\Components\FiltersPanelComponent;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait ProductsListOld
{
  public function display(Collection $products)
  {
    $start = microtime(true);
    $globalFilters = [
      FiltersPanelComponent::FILTER_CATEGORY,
      FiltersPanelComponent::FILTER_OTHER,
      FiltersPanelComponent::FILTER_MODEL,
      FiltersPanelComponent::FILTER_OUR_BRANDS,
      FiltersPanelComponent::FILTER_BRANDS,
    ];

    // парсим url
    $parseUrl = new UrlParser($this->section, $this->url, $this->params);

    $productsFiltersQuery = DB::table('its_product_filter')
      ->whereIn('product_id', $products->pluck('product_id')->unique()->toArray());

     // если не выбран ни один фильтр, отбираем данные по блоку Категория
    if (! $parseUrl->hasFilters()) {
      $productsFiltersQuery->whereIn('filter_id', $globalFilters);
    }

    // если выбран фильтр из блока Категория, отбираем связанные фильтры
    if ($parseUrl->getCategories() !== null) {
      $productsFiltersQuery->where(fn($q) => (
        $q->whereIn('category_id', $parseUrl->getCategories())
          ->orWhere('filter_id', $globalFilters)
      ));
    }

    $productsFilters = $productsFiltersQuery->get();

    // Получение списка доступных значений фильтров по каждому товару
    $filtersValuesIds = $productsFilters->whereNotIn('filter_id', [
      FiltersPanelComponent::FILTER_BRANDS,
      FiltersPanelComponent::FILTER_OUR_BRANDS,
      FiltersPanelComponent::FILTER_MODEL,
    ])
      ->pluck('filter_value_id')->unique()->values();

    // Получаем описание фильтров
    $filtersValues = DB::table('its_filters_values')
      ->whereIn('id', $filtersValuesIds)
      ->orderBy('priority')
      ->get(['id', 'name', 'url', 'filter_id']
      )->keyBy('id');

    $brandsIds = $productsFilters->whereIn('filter_id', [
      FiltersPanelComponent::FILTER_OUR_BRANDS,
      FiltersPanelComponent::FILTER_BRANDS
    ])->pluck('filter_value_id')->unique()->values();
    $filtersBrands = DB::table('its_brands')
      ->selectRaw('id, name  AS url, title AS name')
      ->whereIn('id', $brandsIds)
      ->orderBy('priority')
      ->get()
      ->keyBy('id');

//    dd( $filtersBrands );

    // помечаем выбранные фильтры
    $this->checkedFiltersValues($filtersValues, $parseUrl->getActiveFiltersArray());

    $selectedFilters = $parseUrl->getActiveFiltersArray(['categoriya']); // списко выбранных фильтров
    if ( count($selectedFilters) > 0 ) {
      $productsFilters = $productsFilters->whereIn('vilter_value_id', $parseUrl->getActiveFiltersArray(['categoriya']));
    }

    // считаем количество товаров в фильтрах
    $counters = $this->countBy($productsFilters, 'filter_value_id');

    // получаем блоки для фильтров
    $filtersBlock = $filtersValues->groupBy('filter_id', true);

    // Получаем информацию по блокам
    $blocks = DB::table('its_filters')
//      ->whereIn('id', $filtersBlock->keys())
      ->whereIn('id', $productsFilters->pluck('filter_id')->unique()->values())
      ->orderBy('priority')
      ->get(['id', 'name', 'url'])
      ->keyBy('id');

    foreach ($filtersBlock as $filters) {
      foreach ($filters as &$filter) {
        if ($counters->has($filter->id) ) {
          $item = $counters->get($filter->id);

          $filter->amount = $item[0];
          $filter->novelty = $item[1];
          $filter->coming = $item[2];
        }
      }
    }

    foreach ($blocks as $id => &$block) {
      $block->values = $filtersBlock->get($id);
    }

    if (request()->has('type') && request()->get('type') === 'json') {
      $componentFilters = new FiltersPanelComponent($blocks);
//      $componentProducts = new ProductCardComponent($productIds, $parseUrl);

      return response()->json([
        'filtersHtml' => $componentFilters->resolveView()->with($componentFilters->data())->render(),
//        'productsHtml' => $componentProducts->resolveView()->with($componentProducts->data())->render(),
      ]);
    }

//    dump(round(( microtime(true) - $start ), 3));

    return view('shop', compact('blocks', 'products'));
  }

  /**
   * Подсчет товаров по фильтрам
   * @param $products
   * @return array
   */
  protected function countBy(Collection $products, $field): Collection
  {
    $counter = [];

    foreach ($products as $product) {
      $id = $product->{$field};
      if (! isset($counter[$id]) ) $counter[$id] = [0, 0, 0];

      $counter[$id][0] ++;
      if ($product->novelty) $counter[$id][1] ++;
      if ($product->coming)  $counter[$id][2] ++;
    }

    return collect($counter);
  }

  protected function checkedFiltersValues(Collection &$collection, array $activeFilters)
  {
    if (count($activeFilters) === 0) return;

    foreach ($activeFilters as $item) {
      if(! $collection->has($item)) continue;

      $collection->get($item)->selected = 'checked';
    }
  }

  protected function filterBy($products)
  {
//    foreach ($products as $product) {
//
//    }
  }
}