<?php

namespace App\Traits;

use App\Models\Brand;
use App\Models\Filter;
use App\Models\Category;
use App\Models\FiltersValue;
use App\Models\Product;
use App\Models\ProductsColor;
use Illuminate\Support\Facades\Route;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Resources\ProductResourceCollection;
use App\Models\CategorySector;
use Illuminate\Support\Str;

trait DisplaysProductsList_old
{

protected $productsPerPage = 40;

protected $displayParamsDefaults = [
  'banner' => null,
  'categories_filter' => true,
  'categories_filter_active_parent' => null,
  'brands_filter' => true
];

/**
 * Отображение страницы со списком товаров
 * @param null $products
 * @param string $baseUrl
 * @param array $displayParams
 * @param string $seoDescription
 */
public function displayProductsList($products = null, $baseUrl = '', $displayParams = [], $seoDescription = '', ?bool $bySeries = false, string $urlCategory = '')
{
  $groupBySeries = $bySeries;
//    $start = microtime(true);

  /*$prints = $products->filter(fn($value) => $value->from === 1);
  $products = $products->filter(fn($value) => $value->from === 0);
  $productsCount = $products->count();

  if ($productsCount === 0) {
    $products = $prints;
  } else {
    if ($prints->count() > 0) {
      $key = 5;
      $series = 0;
      foreach ($prints as $item) {
        if ($key > $productsCount || $series == $item->products_series_id) {
          $products->push($item);
        } else {
          $series = $item->products_series_id;
          $products->splice($key, 0, [$item]);
          $key += 5;
        }
      }
    }
  }*/

  // Объединение параметров для текущего отображения с параметрами по-умолчанию
  $displayParams = array_merge($this->displayParamsDefaults, $displayParams);

  // Получение параметров из роута
  $page = 0;
  $filtersString = '';
  $parentCategoryUrl = '';
  $gadgetFilter = false;

  $params = Route::current()->parameter('params');
  $products_series = Route::current()->getName() === 'products_series';

  if ($params) { // @TODO: вынести в отдельный метод
    $gadgetFilter = true;

    $params = explode('/', $params);

    if (count($params) && ! (int) $params[0] && $params[0] != 'filter') $parentCategoryUrl = array_shift($params);
    if (count($params) > 1 && $params[0] == 'filter') $filtersString = $params[1];
    if (count($params) == 1 && ! $filtersString && (int) $params[0]) $page = (int) $params[0];
    if (count($params) == 3 && $filtersString && (int) $params[2]) $page = (int) $params[2];

    // Если в URL'е указана первая страница - делаем редирект на URL без номера страницы
    if ($page == 1) return redirect($baseUrl . ($parentCategoryUrl ? '/' . $parentCategoryUrl : '')
      . ($filtersString ? '/' . $filtersString : ''), 301);
  }

  if (!empty($filtersString)) {
//      $bySeries = false;
  }

  if ( ! $page) $page = 1;

  // Разбор строки с фильтрами из URL'а
  $activeFilters = $filtersString ? $this->parseFiltersString($filtersString) : collect();

  // Если выбрана подстраница родительской категории - получаем ее данные и добавляем ее самым первым фильтром
  if ($parentCategoryUrl && ! $displayParams['categories_filter_active_parent'])
  {

    $isAccessoriesCategory = substr($parentCategoryUrl, 0, 7) === 'others-';

    $activeParentCategory = Category::firstWhere('name', $isAccessoriesCategory ? substr($parentCategoryUrl, 7) : $parentCategoryUrl);
    if ($activeParentCategory)
    {
      $subcategoriesIds = $isAccessoriesCategory ? collect([$activeParentCategory->id])
        : Category::select('id')->where('parent_id', $activeParentCategory->id)->get()->pluck('id');

      $activeFilters->prepend($subcategoriesIds, 'parent_category');
    }
  }
  else $activeParentCategory = $displayParams['categories_filter_active_parent'];

  // Загрузка дополнительных данных для товаров
  $products->load('series', 'additionalCategories:id');

  // Фиксация общего количества товаров в списке
  $totalProductsBeforeFilters = $products->count();

  // Получение списка доступных значений фильтров
  $filtersValues = $products->pluck('filters_values')->flatten()->unique()->values();
  $filtersValues = FiltersValue::whereIn('id', $filtersValues)->get()->groupBy('filter_id');

  $filters = Filter::whereIn('id', $filtersValues->keys())->orderBy('priority', 'ASC')->get()->keyBy('id');

  // Получение списка значений, у которых есть дочерние фильтры
  $filtersValuesWithChildren = $filters->pluck('parent_filters_values')->flatten()->filter()->unique();

  // Поочередное применение выбранных фильтров и просчет количества товаров для каждого из их значений
  $categoriesCounters = $brandsCounters = null;
  $filtersCounters = collect();

  // фильтрация по цветам
  $colorFilters = collect([]);

  foreach ($activeFilters as $filterUrl => $activeValues) {
    $filter = $filters->where('url', $filterUrl)->first();

    if ($filter) {
      $counters = $this->getFiltersCounters($products);

      $filterValues = $filtersValues->get($filter->id)->keyBy('id');
      $filtersCounters = $filtersCounters->union($counters->intersectByKeys($filterValues));
    }

//      if ($displayParams['categories_filter']) {
//        if ($filterUrl == 'parent_category') $parentCategoriesCounters = $this->getFiltersCounters($products, 'category');
//        if ($filterUrl == 'categories') $categoriesCounters = $this->getFiltersCounters($products, 'category');
//      }

    $locActiveValues = clone $activeValues;
    if ($displayParams['categories_filter']) {
//        if ($filterUrl == 'parent_category') $parentCategoriesCounters = $this->getFiltersCounters($products, 'category');
      if ($filterUrl == 'parent_category') $parentCategoriesCounters = $this->getFiltersCounters($products, 'parent_category');
      if ($filterUrl == 'categories') $categoriesCounters = $this->getFiltersCounters($products, 'category');

      // получаем список категорий из include_categories
      $categoriesIds = $activeValues->toArray();
      $categoriesInclude = Category::whereIn('id', $categoriesIds)
        ->whereRaw('LENGTH(include_categories) > 0')
        ->get(['include_categories'])
        ->pluck('include_categories')
        ->toArray();

      if (!empty($categoriesInclude)) {
        foreach ($categoriesInclude as $item) {
          $item = explode(',', $item);
          foreach ($item as $id) {
            $locActiveValues->push($id);
          }
        }
      }
    }

    if ($displayParams['brands_filter'] && $filterUrl == 'brand') {
      $brandsCounters = $this->getFiltersCounters($products, 'brand');
    }

    if (($filterUrl == 'price_from' || $filterUrl == 'price_to') && ! isset($filterPriceMin)) {
      $filterPriceMin = $products->min('opt_price');
      $filterPriceMax = $products->max('opt_price');
    }

    // фильтр по родительскому цвету
    if ($filterUrl === 'color') {
      if ($activeFilters->has('color')) {
        $colorFilters->push($activeFilters->get('color'));
        $products = $this->filterColor($products, $activeFilters->get('color'));
      }
    }

    // фильтр по дочернему цвету
    if ($filterUrl === 'color-child') {
      if ($activeFilters->has('color-child')) {
        $products = $this->filterColor($products, $activeFilters->get('color-child'), true);
        $colorFilters->push($activeFilters->get('color-child'));
      }
    }

    if ($filterUrl === 'color' || $filterUrl === 'color-child') continue;

//      $products = $this->filterProducts($products, collect([$filterUrl => $activeValues]));
    $products = $this->filterProducts($products, collect([$filterUrl => $locActiveValues]));
    if ($filterUrl == 'parent_category') $totalProductsBeforeFilters = $products->count();
  }

  // отбираем цвета для вывода в фильтры
  $parentColors = collect(); // родительские цвета для основного фильтра
  $childrenColorsFilter = collect(); // дочерние цвета выбранных родительских фильтров - для фильтрации
  $childrenColors = collect(); // дочерние цвета выбранных родительских фильтров - для фильтрации
  $fullColors = $this->getColors($products); // получаем список всех доступных цветов по выбранным товарам
  if ($fullColors !== null) {
    $parentColors = $this->getColorsForFilter($fullColors); // получаем список всех доступных родительских цветов с учетом выбранных товаров
    $colorFilters = $colorFilters->flatten()->toArray();

    // если был выбран родительский цвет в фильтре, отбираем все его дочерние цвета
    if ($activeFilters->has('color')) {
      $childrenColorsFilter = $parentColors->whereIn('color', $colorFilters)->map(function($item) {
        return $item->children;
      });

      $childrenColorsFilter = $childrenColorsFilter->flatten()->unique()->all(); // полученные дочерние цвета преобразуем в массив отбирая уникальные
      $childrenColorsFilter = array_intersect($childrenColorsFilter, $fullColors); // удаляем дочерние цвета которых нет в выбранных продуктах
      $childrenColorsFilter = $this->getColorsForFilter($childrenColorsFilter, true, $activeFilters->get('color')->toArray());
    }

    // получаем все дочерние цвета для определения нового
    if ($parentColors !== null && $parentColors->count() > 0) {
      $childrenColors = $parentColors->map(function ($item) {
        return $item->children;
      });
      $childrenColors = $childrenColors->flatten()->unique()->all();
      $childrenColors = array_intersect($childrenColors, $fullColors);
      $childrenColors = $this->getColorsForFilter($childrenColors, true);
    }
  }

  // Просчет количества товаров для оставшихся фильтров и исключение значений без товаров
  $counters = $this->getFiltersCounters($products);
  foreach ($filtersValues as $filterId => $values) {
    if ( ! $activeFilters->has($filters->get($filterId)->url)) {
      $filtersCounters = $filtersCounters->union($counters->intersectByKeys($values->keyBy('id')));
    }

    $values = $values->keyBy('id')->intersectByKeys($filtersCounters);
    $filtersValues->put($filterId, $values->count() ? $values : false);
  }

  if ($displayParams['categories_filter']) {
//      if ( ! isset($parentCategoriesCounters)) $parentCategoriesCounters = $this->getFiltersCounters($products, 'category');
    if ( ! isset($parentCategoriesCounters)) $parentCategoriesCounters = $this->getFiltersCounters($products, 'parent_category');
//      if ( ! isset($categoriesCounters)) $categoriesCounters = $this->getFiltersCounters($products, 'category');
    if ( ! isset($categoriesCounters)) $categoriesCounters = $categoriesCounters = $this->getFiltersCounters(
      $products, 'category',
      $this->needAddItMenu($displayParams['categories_filter_active_parent'] ?? null)
    );

    if ($displayParams['categories_filter_active_parent']) $parentCategoriesCounters = $categoriesCounters;
  }

  if ($displayParams['brands_filter'] && ! isset($brandsCounters)) {
    $brandsCounters = $this->getFiltersCounters($products, 'brand');
  }

  if ( ! isset($filterPriceMin)) {
    $filterPriceMin = $products->min('opt_price');
    $filterPriceMax = $products->max('opt_price');
  }

  // подсчет количества уцененного товара
  $ucenennye_tovary = $this->getFiltersCounters($products, 'ucenka');
  $ucenennye_tovary = $ucenennye_tovary->toArray();

  // Сортировка списка товаров
  $productsSort = '';

  if (request()->cookie('products_sort')
    && array_search(request()->cookie('products_sort'), ['discounts', 'cheap', 'expensive']) !== false
    && $products->count() > 0) {
    $productsSort = request()->cookie('products_sort');

    $products = $products->groupBy('from');
    $productsPrint = $products->get(1)  ?? collect();
    $products = $products->get(0) ?? $products->get(1);

    if ($productsSort == 'discounts') {
      $products = $products->sortBy(function($product)
      {
        return ($product->old_opt_price > $product->opt_price) ? 0 : 1;
      });
    }

    if ($productsSort == 'cheap') $products = $products->sortBy('opt_price'); // опускаем принты в самый низ всегда
    if ($productsSort == 'expensive') $products = $products->sortByDesc('opt_price'); // опускаем принты в самый низ всегда

    $products = $products->merge($productsPrint);
  }

  //преобразуем в серии
//    dd($groupBySeries);
  if ($bySeries) {
    $products = $this->groupBySeries($products, $urlCategory);
//      dd($products);
  }

  // Формирование пагинации
  $totalProducts = $products->count();

  $perPage = request()->cookie('products_per_page');
  if ( ! $perPage || array_search($perPage, [40, 60, 80]) === false) $perPage = $this->productsPerPage;

  $paginationUrl = $baseUrl . ($filtersString ? '/filter/' . $filtersString : '');

  // пагинация
  $products = new LengthAwarePaginator(
    $products->slice(($page - 1) * $perPage, $perPage),
    $totalProducts,
    $perPage,
    $page,
    ['path' => $paginationUrl, 'query' => request()->query()]
  );
//dd($products);
  // Если данные запрошены в JSON-формате - возвращаем результат
  if (request('type') == 'json') {
    return new ProductResourceCollection($products);
  }

  // Исключение фильтров без доступных значений
  $filters = $filters->intersectByKeys($filtersValues->filter());

  // если в категории фильтр только техничка, чехлы или стекла-пленкт, показываем все техн. фильтры, иначе - скрываем
  // id = 397 - tehnichka(Техничка) - табл. its_filters_values
  // id = 256 - чехлы
  // id = 257, 258 - стекла-пленки

  $technichkaOnlyOne = (
    ($filtersValuesWithChildren->count() == 1 && ($filtersValuesWithChildren->search(397) !== false || $filtersValuesWithChildren->search(256) !== false || $filtersValuesWithChildren->search(257) !== false || $filtersValuesWithChildren->search(258) !== false))
    ||
    ($filtersValuesWithChildren->count() == 2 && ($filtersValuesWithChildren->search(257) !== false && $filtersValuesWithChildren->search(258) !== false))
  );

  // если в категориях фильтр один, делаем его активным
  if ($filtersValues->has(84) && $filtersValues->get(84) !== false) {
    $filtersCetagories = $filtersValues->get(84);
    if ($filtersCetagories->count() == 1 || ($filtersCetagories->count() == 2 && $filtersCetagories->has(257) && $filtersCetagories->has(258))) {
      $id = $filtersCetagories->first()->id;
      $activeFilters->put('categoriya', collect($id));
    }
  }

  if (!$technichkaOnlyOne) {
    // Исключение фильтров, являющихся дочерними, если их родитель пока не выбран
    $filters = $filters->filter(function ($filter, $key) use ($activeFilters) {
      return !$filter->parent_filters_values || $activeFilters->except('brand')->flatten()
          ->intersect($filter->parent_filters_values)->count();
    });
  }

  // Добавление в детали каждого фильтра списка его значений
  $filters = $filters->map(function($filter) use ($filtersValues, $filtersValuesWithChildren) {
    $filter->availableValues = $filtersValues->get($filter->id)
      ->map(function($value) use ($filtersValuesWithChildren) {
        $value->has_children = $filtersValuesWithChildren->search($value->id) !== false;
        return $value;
      });

    return $filter;
  })
    ->keyBy('url');

  // Получение списка доступных категорий
  $categories = $categoriesSectors = $parentCategories = $accessoriesCategories = null;

  if ($displayParams['categories_filter']) {
    $categories = Category::whereIn('id', $parentCategoriesCounters->keys())
      ->orderBy('priority_phones', 'DESC')
      ->orderBy('priority', 'DESC')
      ->get()
      ->groupBy('parent_id');

    if ( ! $displayParams['categories_filter_active_parent']) {
      $accessoriesCategories = $categories->pull(config('custom.accessories_category_id'));

      $parentCategories = Category::whereIn('id', $categories->keys())
        ->orderBy('priority_phones', 'DESC')
        ->orderBy('priority', 'DESC')
        ->get()
        ->keyBy('id');
      $parentCategories->pull(config('custom.accessories_category_id'));

      // Суммирование количества дочерних категорий для получения количества в родителях
      if ($parentCategories) {
        foreach ($parentCategories as $parentCategory) {
          $counter = [0, 0];
          foreach ($categories->get($parentCategory->id) as $category) {
            $counter[0] += $parentCategoriesCounters[$category->id][0];
            $counter[1] += $parentCategoriesCounters[$category->id][1];
          }

          $categoriesCounters->put($parentCategory->id, $counter);
        }
      }

      // Перенос счетчиков товаров по категориям общих аксессуаров в единый массив
      if ($accessoriesCategories) {
        foreach ($accessoriesCategories as $accessoriesCategory) {
          $categoriesCounters->put($accessoriesCategory->id, $parentCategoriesCounters->get($accessoriesCategory->id));
        }
      }
    }

    // Если выбрана родительская категория - отображаем фильтры по подкатегориям
    if ($activeParentCategory && $categories->has($activeParentCategory->id)) {
      $categories = $categories->get($activeParentCategory->id)
        ->keyBy('id')
        ->intersectByKeys($categoriesCounters)
        ->groupBy('menu_sector');

      // Получение списка секторов для категорий
      $categoriesSectors = CategorySector::whereIn('id', $categories->keys())
        ->orderBy('priority', 'DESC')
        ->get()
        ->keyBy('id');

      if ($categories->has(0)) $categoriesSectors->put(0, (object) ['id' => 0, 'title' => __('Другие модели')]);

      // проверяем была ли модель случайно добавлена в другую категорию
//        $_menus = $products->pluck('menu');
//        $_parentMenu = Category::whereIn('id', $_menus)->get()->groupBy('parent_id')->all();
//        unset($_parentMenu[$activeParentCategory->id]);

      // если есть такие товары
//        if ( count($_parentMenu) > 0) {
//          foreach ($_parentMenu as $key => $_menu) {
//            $first = $_menu->first();
//            $categoriesSectors->put($key, $first);
//            if ($_menu->count() > 0) {
//              if (!$categories->has($first->id)) {
//                $categories->put($first->id, collect());
//              }
//
//              foreach ($_menu->all() as $item) {
//                $categories->get($first->id)->push($item);
//              }
//            }
//          }
//        }
    }
    else $categories = null;
  }
//    dd($categoriesSectors);
  // Получение списка доступных брендов
  if ($displayParams['brands_filter']) {
    $brands = Brand::whereIn('id', $brandsCounters->keys())->get(['id', 'title', 'our'])->keyBy('id');
  }
  else $brands = null;

  // Получение баннера для отображения на странице
  $banner = $displayParams['banner'];

  // Задание флага страницы с товарами для общего шаблона
  view()->share('layoutProductsListPage', true);

  // Задание canonical-ссылки для страниц пагинации
  if ($page > 1) view()->share('layoutCanonicalLink', $baseUrl . ($parentCategoryUrl ? '/' . $parentCategoryUrl : '')
    . ($filtersString ? '/' . $filtersString : ''));

  // Получение строки фильтров без фильтра по категориям для формирования ссылок на родительские категории
  $filtersStringWithoutCategories = $filtersString ? rtrim(preg_replace('/(categories=[^;]+(;|$))/', '', $filtersString), ';') : '';

  // Группировака категорий фильтров Стекла и Пленки
  // получаем активные ibs фильтров - Категория
  $maxFiltersCategories = null;
  if ($filters->has('categoriya')) {
    $this->groupCategories($filtersCounters, $filters, $activeFilters);

    // выборка топ два фильтра
    $ids = $filters->get('categoriya')->availableValues->map(function($item) {
      return $item->id;
    });

    $activeFilterIds = array_flip($ids->flatten()->all()); // ids выносим как ключи массива
    // filtersCounters - агрегация кол-ва товаров по фильтрам, отбираем только те которые из фильтра Категория
    $intersect = $filtersCounters->intersectByKeys($activeFilterIds);
    // отбираем два фильтра с мак кол-вом товара
    $maxFiltersCategories = $intersect->sortByDesc(0)->take(2)->all();
  }
  //Конец - Группировака категорий фильтров Стекла и Пленки

  // сортируем фильтры Бренд по кол-ву товаров
  $brandsFilter = null;
  if ($brands !== null) {
    $brandsFilter = $this->brandOrder($brands, $brandsCounters);
  }

//    echo 'Время выполнения скрипта: '.round(microtime(true) - $start, 4).' сек.';

  // сортировка цвета
  if ($childrenColorsFilter->count() > 0) {
    $childrenColorsFilter = $childrenColorsFilter->sortBy(function($item){
        $item->new = today()->diffInDays($item->created_at) <= 30;
      return $item->color;
    });
    // обнуляем индексы
    $childrenColorsFilter = $childrenColorsFilter->values();
  }

  if ($parentColors !== null) {

    $colorNew = [];

    if ($childrenColors->count()) {
      $childrenColors->map(function($item) use (&$colorNew) {
        if (today()->diffInDays($item->created_at) <= 30) {
          $colorNew[$item->parent_id] = true;
        }
      });
    }

    $parentColors = $parentColors->sortBy(function ($item) use ($colorNew) {
      $item->new = today()->diffInDays($item->created_at) <= 30 || isset($colorNew[$item->id]);
      return $item->color;
    });
    // обнуляем индексы
    $parentColors = $parentColors->values();
  }

//    dd($products->get(1)->getLabel());
  // Создание отображения
  return view('products-list_old', compact([
    'products', 'baseUrl', 'paginationUrl', 'filters', 'activeFilters', 'filtersString', 'filtersCounters',
    'activeParentCategory', 'parentCategoryUrl', 'parentCategories', 'categories', 'categoriesSectors',
    'accessoriesCategories', 'categoriesCounters', 'brands', 'brandsFilter', 'brandsCounters', 'filterPriceMin',
    'filterPriceMax', 'totalProducts', 'totalProductsBeforeFilters', 'perPage', 'productsSort',
    'filtersStringWithoutCategories', 'banner', 'ucenennye_tovary', 'maxFiltersCategories', 'groupBySeries',
    'parentColors', 'colorFilters', 'childrenColorsFilter', 'seoDescription', 'gadgetFilter', 'products_series'
  ]));
}

/**
 * группируем по сериям
 */
protected function groupBySeries($products, $urlCategory)
{
  $items = $products->groupBy('products_series_id');

  $temp = new \Illuminate\Database\Eloquent\Collection();
//dd($items->get(814));
  foreach ($items as $series => $product) {
    if ($series === 0 || $product->count() === 1) {
      foreach ($product as $item) {
        $temp->push($item);
      }
    } else {
        $ucenca = true;
        $priceRange = [];
        $optPriceRange = [];
        $optPrice = [];
        $priceOptVipRange = [];
        $priceOptDealerRange = [];
        $priceDropRange = [];
        $priceOptMinOptRange = [];

        foreach ($product as $item) {
          $item->opt_vip_usd = $item->getPrice('opt_vip_usd');
          $item->opt_dealer_usd = $item->getPrice('opt_dealer_usd');
          $item->drop_uah = $item->getPrice('drop_uah');
          $item->opt_minopt_usd = $item->getPrice('opt_minopt_usd');
          if ($item->isMarkdown() === false && $ucenca) {
            $ucenca = false;
          }
        }

        $optPriceRange = [
          $product->min('opt_price'),
          $product->max('opt_price')
        ];
        $priceRange = [
          $product->min('price'),
          $product->max('price')
        ];
        $priceOptVipRange = [
          $product->min('opt_vip_usd'),
          $product->max('opt_vip_usd')
        ];
        $priceOptDealerRange = [
          $product->min('opt_dealer_usd'),
          $product->max('opt_dealer_usd')
        ];
        $priceDropRange = [
          $product->min('drop_uah'),
          $product->max('drop_uah')
        ];
        $priceOptMinOptRange = [
          $product->min('opt_minopt_usd'),
          $product->max('opt_minopt_usd')
        ];

        $tmp = $product->first();

        $tmp->opt_price_range = $optPriceRange;
        $tmp->price_range = $priceRange;
        $tmp->opt_vip_usd_range = $priceOptVipRange;
        $tmp->opt_dealer_usd_range = $priceOptDealerRange;
        $tmp->drop_uah_range = $priceDropRange;
        $tmp->opt_minopt_usd_range = $priceOptMinOptRange;

        $tmp->markdown = $tmp->isMarkdown();
        $tmp->title = __('Серия') . ' ' . Str::before($tmp->title, 'для');
        $tmp->title = trim(Str::replaceFirst('Уценка', '', $tmp->title));
        $tmp->bySeries = true;
        $tmp->seriesCount = $product->count();
        $tmp->urlCategory = $urlCategory;
        if (! Str::contains($urlCategory, 'filter') && $ucenca) {
          $tmp->urlCategory .= 'filter/';
        }
        $tmp->urlCategory .= ($ucenca) ? ';ucenka=true' : ''; // если весь товар уценен, прописываем фильтр
        $temp->push($tmp);

//        dd($tmp);
    }
  }
//die;
  return $temp;
}

/**
 * Нужно ли добавлять фильтры из addit_menus
 * @param $category
 * @return bool
 */
private function needAddItMenu($category): bool
{
  if (!isset($category->id)) return true;

  return !in_array($category->id, [1767,1781,1907,1911,683,682,1786]);
}

/**
 * Группируем категории стекла - пленки
 * @param $filtersCounters
 * @param $filters
 * @param $activeFilters
 */
protected function groupCategories(&$filtersCounters, &$filters, &$activeFilters): void
{
  $stekla = 257;
  $plenki = 258;
  $stekla_plenki = $stekla . ',' . $plenki;
  if ($filtersCounters->has($stekla) && $filtersCounters->has($plenki)) {

    // суммируем количество
    $counter = [0, 0, 0];
    $tmp = $filtersCounters->get($stekla);
    $counter[0] += $tmp[0];
    $counter[1] += $tmp[1];
    $counter[2] += $tmp[2] ?? 0;
    $filtersCounters->forget($stekla);

    $tmp = $filtersCounters->get($plenki);
    $counter[0] += $tmp[0];
    $counter[1] += $tmp[1];
    $counter[2] += $tmp[2] ?? 0;
    $filtersCounters->forget($plenki);

    $filtersCounters->put($stekla_plenki, $counter);

    // удаляем фильтры 'Защитные стекла' и 'Защитные пленки'
    $filters->get('categoriya')->availableValues->forget($stekla);
    $filters->get('categoriya')->availableValues->forget($plenki);

    // создаем новый агрегированный фильтр
    $filters->get('categoriya')->availableValues->push((object)[
      'id' => $stekla_plenki,
      'filter_id' => 84,
      'url' => 'stekla-plenki',
      'name' => 'Стекла / Пленки',
      'code' => null,
      'priority' => 1,
      'has_children' => true,
    ]);

    if ($activeFilters->has('categoriya')
      && $activeFilters->get('categoriya')->search($stekla) !== false
      && $activeFilters->get('categoriya')->search($plenki) !== false
    ) {
      $activeFilters->get('categoriya')->forget(
        $activeFilters->get('categoriya')->search($stekla)
      );
      $activeFilters->get('categoriya')->forget(
        $activeFilters->get('categoriya')->search($plenki)
      );
      $activeFilters->get('categoriya')->push($stekla_plenki);
    }
  }
}

/**
 * Сортировка брендов по количеству товаров
 */
protected function brandOrder($brands, $brandsCounters)
{
  $data = collect([]);
  $our = collect([]);
  foreach ($brands as $brand) {
    if ($brandsCounters->has($brand->id)) {
      $amount = $brandsCounters->get($brand->id);

      if ($brand->our === 1) {
        $our->put($brand->id, (object)[
          'id' => $brand->id,
          'title' => $brand->title,
          'our' => $brand->our,
          'amount' => $amount[0],
          'amountNew' => $amount[1],
        ]);
      } else {
        $data->put($brand->id, (object)[
          'id' => $brand->id,
          'title' => $brand->title,
          'our' => $brand->our,
          'amount' => $amount[0],
          'amountNew' => $amount[1],
        ]);
      }
    }
  }

  if ($data->count() > 0) {
    $data = $data->sortByDesc('amount', SORT_NUMERIC);
  }
  if ($our->count() > 0) {
    $our = $our->sortByDesc('amount', SORT_NUMERIC);
  }

  return  collect(['our' => $our, 'data' => $data]);
}

/**
 * Разбор строки с фильтрами из URL'а
 */
protected function parseFiltersString($filtersString = '')
{
  $filters = collect();
  foreach (explode(';', $filtersString) as $index => $value)
  {
    $value = explode('=', $value);
    if (is_array($value) && count($value) == 2)
    {
      $filters->put($value[0], collect(explode(',', $value[1])));
    }
  }

  return $filters;
}

/**
 * Просчет количества товаров для каждого значения фильтра
 */
protected function getFiltersCounters($products = null, $filtersType = 'custom', bool $addItMenu = false)
{
  $filtersCounters = [];

  foreach ($products as $product)
  {
    if ($filtersType == 'category' && $product->from === 1 && Route::getCurrentRoute()->getName() !== 'products_series') continue;

    // Получение списка всех значений фильтров для текущего товара
    $productValues = [];

    if ($filtersType != 'custom')
    {
      if ($filtersType == 'brand') $productValues = [$product->brand];
      if ($filtersType == 'category') {
        $productValues = [$product->menu];
//          $addItMenu = true;
//          if ($product->addit_menus && $addItMenu) { // если у товара есть дополнительные категории
//          if ($product->addit_menus) { // если у товара есть дополнительные категории
//            $addit_menus = explode(',', $product->addit_menus);
//            foreach ($addit_menus as $menu) {
//              $productValues[] = $menu;
//            }
//          }

        if ($product->additionalCategories->count() > 0) {
          $additionalCategories = $product->additionalCategories->pluck('id')->all();
          foreach ($additionalCategories as $menu) {
            $productValues[] = $menu;
          }
        }
      }
      if ($filtersType == 'parent_category') $productValues = [$product->menu];
      if ($filtersType == 'ucenka' && strpos(mb_strtolower($product->title), 'уценка') !== false) {
        $productValues = ['ucenka'];
      }
    }
    else $productValues = $product->filters_values;

    // Увеличение счетчиков для всех значений, к которым относится текущий товар
    foreach ($productValues as $filtersValue) {
      if ( ! isset($filtersCounters[$filtersValue])) $filtersCounters[$filtersValue] = [0, 0];

      $filtersCounters[$filtersValue][0]++;
      if ($product->isNovelty()) $filtersCounters[$filtersValue][1]++;
    }
  }

  foreach ($filtersCounters as &$item) {
    $item[2] = $item[0] + $item[1];
  }

  // отображение категорий из include_categories
  if ($filtersType == 'category' && !empty($filtersCounters)) {
    $keys = array_keys($filtersCounters); // получаем список ид категорий
    // формируем строку запроса
    $amount = count($keys) - 1;
    $whereLike = '';
    foreach ($keys as $num => $key) {
      $whereLike .= ' include_categories LIKE "%' . $key . '%" ';
      if ($num < $amount) $whereLike .= 'OR';
    }

    // получаем категории которые включают в себя другие категории
    $includeCategories = Category::whereRaw($whereLike)->get(['id', 'include_categories'])->toArray();

    if ($includeCategories != null) {
      foreach ($includeCategories as $categories) {
        if (!isset($filtersCounters[$categories['id']])) { // если категория еще не просчитана
          $arrCategories = explode(',', $categories['include_categories']);
          foreach ($arrCategories as $item) { // проходимся по массиву include_categories и проверяем что есть чтоб взять данные по кол-ву
            if (isset($filtersCounters[$item])) {
              $filtersCounters[$categories['id']] = $filtersCounters[$item];
              break;
            }
          }
        }
      }
    }
  }

  return collect($filtersCounters);
}

/**
 * Фильтрация списка товаров
 */
protected function filterProducts($products = null, $activeFilters = [])
{
  $filterPriceTypes = ['opt_price', 'opt_vip_usd', 'opt_vip_usd_max', 'opt_dealer_usd', 'opt_dealer_usd_max'];

  return $products->filter(function($product) use ($activeFilters, $filterPriceTypes)
  {
    $flag = true;

    $productFiltersValues = collect($product->filters_values);

    foreach ($activeFilters as $name => $values)
    {
      switch ($name)
      {
        case 'sale':
          if ($product->liquidity < 2 || $product->qty <= 0) $flag = false;
          break;

        case 'price_from':
          $flag = false;

          foreach ($filterPriceTypes as $priceType)
          {
            $price = $product->getPrice($priceType);
            if ($price && $price >= $values[0])
            {
              $flag = true;
              break;
            }
          }

          break;

        case 'price_to':
          $flag = false;

          foreach ($filterPriceTypes as $priceType)
          {
            $price = $product->getPrice($priceType);
            if ($price && $price <= $values[0])
            {
              $flag = true;
              break;
            }
          }

          break;

        case 'parent_category':
        case 'categories':
          $productCategories = array_merge([$product->menu], $product->additionalCategories->pluck('id')->toArray());
          if ( ! $values->intersect($productCategories)->count()) $flag = false;
          break;

        case 'brand':
          if ($values->search($product->brand) === false) $flag = false;
          break;

        case 'price_currency':
          break;

        case 'ucenka':
          if (strpos(mb_strtolower($product->title), 'уценка') === false) $flag = false;
          break;

        default:
          if ( ! $productFiltersValues->intersect($values)->count()) $flag = false;
      }

      if ( ! $flag) break;
    }

    return $flag;
  });
}

/**
 * Фильтруем товар по цветам
 * @param $products - список продуктов которые нужно отфильтровать
 * @param $colors - цвета по которым нужно отобрать
 * @param $getChildrenOnly - true - фильтровать только по дочерним цветам,
 *                          false - фильтровать по агрегации дочерних цветов
 * @return mixed
 */
protected function filterColor($products, $colors, $getChildrenOnly = false)
{
  $colors = $colors->toArray();
  $parentColor = $colors;
  if ($getChildrenOnly === false) {
    $childrenColor = ProductsColor::whereIn('color', $colors)->where('parent_id', 0)->get();
    if ($childrenColor->count() === 0) {
      return $products;
    }

    $colors = $childrenColor->pluck('children')->filter()->flatten()->toArray();
    foreach ($parentColor as $item) {
      array_push($colors, $item);
    }
  }

  $products = $products->filter(function($product) use ($colors) {
    foreach($product->group_products as $item) {
      if (in_array($item->color, $colors)) {
        return true;
      }
    }
  });

  return $products;
}

/**
 * получить список цветов для фильтра
 * @param $colors -  доступные цвета в отобранных продуктах
 * @param $getChildrenOnly - true - фильтровать только по дочерним цветам,
 *                          false - фильтровать по агрегации дочерних цветов
 * @return |null
 */
protected function getColorsForFilter(array $colors, $getChildrenOnly = false, $parentColors = [])
{
  if ($getChildrenOnly === false) {
    $parentColors = ProductsColor::whereIn('color', $colors)->get();

    if ($parentColors->count() === 0) {
      return null;
    }

    $parentColors = $parentColors->map(function($color) {
      return ($color->parent_id === 0) ? $color->id : $color->parent_id;
    });

    return ProductsColor::whereIn('id', $parentColors->unique())->get();
  } else {
    $result = ProductsColor::whereIn('color', $colors)
      ->where('parent_id', '>', 0)
      ->whereNotIn('color', $parentColors)
      ->orderBy('parent_id')
      ->get();

    return $result->unique('color');
  }
}

/**
 * Список всех цветов в товаре
 * @param Product collect
 * @return array|null
 */
protected function getColors($products)
{
  $colors = $products->map(function($product) {
    if ($product->group_products !== null)
      return array_column($product->group_products, 'color');
  });

  if ($colors->count() == 0) {
    return null;
  }

  $colors =  array_unique($colors->flatten()->toArray());
  return $colors;
}
}