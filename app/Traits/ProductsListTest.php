<?php

namespace App\Traits;

use App\Http\Models\UrlParser;
use App\View\Components\FiltersPanelComponent;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait ProductsListTest
{
  public function display(Collection $products)
  {
    $start = microtime(true);

    $globalFilters = [
      FiltersPanelComponent::FILTER_CATEGORY,
      FiltersPanelComponent::FILTER_OTHER,
      FiltersPanelComponent::FILTER_OUR_BRANDS,
      FiltersPanelComponent::FILTER_BRANDS,
    ];

    // парсим url
    $parseUrl = new UrlParser($this->section, $this->url, $this->params);

    $activeFilters = $parseUrl->getAllParams();

    // Получаем все доступные фильтры
    $productIds = $products->pluck('product_id')->unique()->values();
    $productsFiltersQuery = DB::table('its_product_filter')
      ->whereIn('product_id', $productIds);

    // если не выбран ни один фильтр, отбираем данные по блоку Категория
    if (! $parseUrl->hasFilters()) {
      $productsFiltersQuery->whereIn('filter_id', $globalFilters);
    }

    $productsFilters = $productsFiltersQuery->get();

    // Получение списка доступных значений фильтров
    $filtersValuesIds = $products->pluck('filter_values_id')->unique()->values();
    $filtersValues = DB::table('its_filters_values')
      ->whereIn('id', $filtersValuesIds)
      ->orderBy('priority')
      ->get(['id', 'filter_id', 'name', 'url'])
    ->groupBy('filter_id');

    // получаем родительские блоки фильтров
    $filtersIds = $filtersValues->keys();
    $filters = DB::table('its_filters')
      ->whereIn('id', $filtersIds)
      ->orderBy('priority')
      ->get(['id', 'name', 'url'])
      ->keyby('id');

    // Поочередное применение выбранных фильтров и просчет количества товаров для каждого из их значений
//    foreach ($activeFilters as $filter) {
//      dd($filter);
//    }


    return view('shop', compact('products'));
  }

  /**
   * Подсчет товаров по фильтрам
   * @param $products
   * @return array
   */
  protected function countBy(Collection $products, $field): Collection
  {
    $counter = [];

    foreach ($products as $product) {
      $id = $product->{$field};
      if (! isset($counter[$id]) ) $counter[$id] = [0, 0, 0];

      $counter[$id][0] ++;
      if ($product->novelty) $counter[$id][1] ++;
      if ($product->coming)  $counter[$id][2] ++;
    }

    return collect($counter);
  }

  protected function checkedFiltersValues(Collection &$collection, array $activeFilters)
  {
    if (count($activeFilters) === 0) return;

    foreach ($activeFilters as $item) {
      if(! $collection->has($item)) continue;

      $collection->get($item)->selected = 'checked';
    }
  }

  protected function filterBy($products)
  {
//    foreach ($products as $product) {
//
//    }
  }
}