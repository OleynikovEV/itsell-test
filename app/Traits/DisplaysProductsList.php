<?php

namespace App\Traits;

use App\Models\Brand;
use App\Models\Filter;
use App\Models\Category;
use App\Models\FiltersValue;
use App\Models\PriceRange;
use App\Models\Product;
use App\Models\ProductsColor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Resources\ProductResourceCollection;
use App\Models\CategorySector;
use Illuminate\Support\Str;

trait DisplaysProductsList
{

protected $productsPerPage = 40;

protected $displayParamsDefaults = [
  'banner' => null,
  'categories_filter' => true,
  'categories_filter_active_parent' => null,
  'brands_filter' => true,
  'urlCategory' => '',
  'labelFroGroupClass' => '',
];

  /**
   * Отображение страницы со списком товаров
   * @param null $products - список отобранных товаров
   * @param string $baseUrl - базовый url
   * @param array $displayParams - параметры для отображения
   * 'banner' => null, - показывать банер
   * 'categories_filter' => true, - фильтр категорий
   * 'categories_filter_active_parent' => null, - активный родительский фильтр категорий
   * 'brands_filter' => true - фильтр брендов
   * 'urlCategory' => '' - для карточек сгруппированных
   * 'labelFroGroupClass' => '', - класс для ссылки на серии
   * 'labelFroGroupText' => '', - текст для подсказки на серии
   *
   * @param string $seoDescription - вывод seo на странице
   * @param bool|null $bySeries - группировать по сериям или нет
   */
public function displayProductsList($products = null, $baseUrl = '', $displayParams = [], $seoDescription = '', ?bool $bySeries = false)
{
  $groupBySeries = $bySeries;
  $start = microtime(true);
  getMicrotime(1, $start);

  $orderList = [
    'novelty' => __('Новинки'),
    'rating' => __('Популярные'),
    'coming' => __('Приход'),
    'discounts' => __('Акции'),
    'cheap' => __('Дешевые'),
    'expensive' => __('Дорогие'),
  ];

  /*$prints = $products->filter(fn($value) => $value->from === 1);
  $products = $products->filter(fn($value) => $value->from === 0);
  $productsCount = $products->count();

  if ($productsCount === 0) {
    $products = $prints;
  } else {
    if ($prints->count() > 0) {
      $key = 5;
      $series = 0;
      foreach ($prints as $item) {
        if ($key > $productsCount || $series == $item->products_series_id) {
          $products->push($item);
        } else {
          $series = $item->products_series_id;
          $products->splice($key, 0, [$item]);
          $key += 5;
        }
      }
    }
  }*/

  // Объединение параметров для текущего отображения с параметрами по-умолчанию
  $displayParams = array_merge($this->displayParamsDefaults, $displayParams);

  // Получение параметров из роута
  $page = 0;
  $filtersString = '';
  $parentCategoryUrl = '';
  $gadgetFilter = false;

  $params = Route::current()->parameter('params');
  $products_series = Route::current()->getName() === 'products_series';

  if ($params) { // @TODO: вынести в отдельный метод
    $gadgetFilter = true;
    $params = explode('/', $params);
//    dd($params);

    if (count($params) && ! (int) $params[0] && $params[0] != 'filter') $parentCategoryUrl = array_shift($params);
    if (count($params) > 1 && $params[0] == 'filter') $filtersString = $params[1];
    if (count($params) == 1 && ! $filtersString && (int) $params[0]) $page = (int) $params[0];
    if (count($params) == 3 && $filtersString && (int) $params[2]) $page = (int) $params[2];

    // получаем номер страницы из GET строки - если ссылка из гугл аналитики
    if ($page === 0 && request()->has('page')) {
      $page = request('page');
    }

    // Если в URL'е указана первая страница - делаем редирект на URL без номера страницы
    if ($page == 1) return redirect($baseUrl . ($parentCategoryUrl ? '/' . $parentCategoryUrl : '')
      . ($filtersString ? '/' . $filtersString : ''), 301);
  }

  if ( ! $page) $page = 1;

  // Разбор строки с фильтрами из URL'а
  $activeFilters = $filtersString ? $this->parseFiltersString($filtersString) : collect();

  // Блоки новинки, скидки, поступления и тд - выбран фильтр в Гаджеты
  // Если выбрана подстраница родительской категории - получаем ее данные и добавляем ее самым первым фильтром
  if ($parentCategoryUrl && ! $displayParams['categories_filter_active_parent']) {
    $isAccessoriesCategory = substr($parentCategoryUrl, 0, 7) === 'others-';

    $activeParentCategory = Category::firstWhere('name', $isAccessoriesCategory ? substr($parentCategoryUrl, 7) : $parentCategoryUrl);
    if ($activeParentCategory) {
      $subcategoriesIds = $isAccessoriesCategory ? collect([$activeParentCategory->id])
        : DB::table('its_menu')->where('parent_id', $activeParentCategory->id)->get(['id'])->pluck('id'); //Category::select('id')->where('parent_id', $activeParentCategory->id)->get()->pluck('id');

      $activeFilters->prepend($subcategoriesIds, 'parent_category');
    }
  } else $activeParentCategory = $displayParams['categories_filter_active_parent'];


  // сортировка товара
  $productsSort = '';

  $products_sort = request()->cookie('products_sort') ?? 'novelty';
  if ($products_sort) {
    $productsSort = $products_sort;

    // новинки
    if ($productsSort == 'novelty') $products->orderByRaw('`from` = 0 DESC, qty > 0 DESC, added_time >= '
      . today()->subDays(Product::NOVELTY_DAYS)->timestamp . ' DESC, markdown ASC, '
      . 'liquidity > 0 ASC, arrived_in_stock_at DESC, id DESC');
    // сортировка товара со скидкой
    if ($productsSort == 'discounts') $products->orderByRaw('`from` = 0 DESC, CASE WHEN old_opt_price > opt_price THEN 1 ELSE 0 END DESC, id DESC');
    // по продажам
    if ($productsSort == 'rating') $products->orderByRaw('`from` = 0 DESC, CASE WHEN added_time >= '
      . today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp
      . ' AND qty > 0 AND markdown = 0 AND `from` = 0 THEN 1 ELSE 0 END DESC, priority DESC, qty DESC, id DESC');
    // опускаем принты в самый низ всегда
    if ($productsSort == 'coming') $products->orderByRaw('`from` = 0 DESC, date_coming DESC, id DESC');
    // опускаем принты в самый низ всегда
    if ($productsSort == 'cheap') $products->orderByRaw('`from` = 0 DESC, opt_price ASC, id DESC');
    // опускаем принты в самый низ всегда
    if ($productsSort == 'expensive') $products->orderByRaw('`from` = 0 DESC, opt_price DESC, id DESC');
  }

  $products = $products->with('series', 'additionalCategories:id,parent_id', 'categoryData:id,title,name')
    ->get(['id', 'baseid', 'url', 'title', 'active', 'qty', 'when_appear', 'opt_price', 'old_opt_price', 'brand',
      'img', 'show_price', 'filters_values', 'group_products', 'price_types', 'box_image', 'menu', 'addit_menus',
      'from', 'products_series_id', 'color', 'price', 'price_modify_time', 'liquidity', 'arrived_in_stock_at',
      'added_time', 'date_coming', 'markdown', 'priority']);
  // Загрузка дополнительных данных для товаров
//  $products->load('series', 'additionalCategories:id,parent_id', 'categoryData:id,title,name');

  // Фиксация общего количества товаров в списке
  $totalProductsBeforeFilters = $products->count();

  // Получение списка доступных значений фильтров
  $filtersValuesIds = $products->pluck('filters_values')->flatten()->unique()->values();
  $filtersValues = FiltersValue::whereIn('id', $filtersValuesIds)->get()->groupBy('filter_id');

  $filters = Filter::whereIn('id', $filtersValues->keys())->orderBy('priority', 'ASC')->get()->keyBy('id');

  // Получение списка значений, у которых есть дочерние фильтры
  $filtersValuesWithChildren = $filters->pluck('parent_filters_values')->flatten()->filter()->unique();

  // Поочередное применение выбранных фильтров и просчет количества товаров для каждого из их значений
  $categoriesCounters = $brandsCounters = null;
  $filtersCounters = collect();

  // фильтрация по цветам
  $colorFilters = collect([]);
  getMicrotime(2, $start);

  foreach ($activeFilters as $filterUrl => $activeValues) {
    $filter = $filters->where('url', $filterUrl)->first();

//    dump($activeValues);

    if ($filter && !request()->has('type')) {
      $counters = $this->getFiltersCounters($products);
      $filterValues = $filtersValues->get($filter->id)->keyBy('id');
      $filtersCounters = $filtersCounters->union($counters->intersectByKeys($filterValues));
    }

    $locActiveValues = clone $activeValues;
    if ($displayParams['categories_filter']) {
      if ($filterUrl == 'parent_category' && !request()->has('type')) {
        $parentCategoriesCounters = $this->getFiltersCounters($products, 'parent_category');
      }
      if ($filterUrl == 'categories' && !request()->has('type')) {
        $categoriesCounters = $this->getFiltersCounters($products, 'category');
      }

      // получаем список категорий из include_categories
      $categoriesIds = $activeValues->toArray();
      $categoriesInclude = DB::table('its_menu')
          ->whereIn('id', $categoriesIds)
          ->whereRaw('LENGTH(include_categories) > 0')
          ->get(['include_categories'])
          ->pluck('include_categories')
          ->toArray();

//        Category::whereIn('id', $categoriesIds)
//        ->whereRaw('LENGTH(include_categories) > 0')
//        ->get(['include_categories'])
//        ->pluck('include_categories')
//        ->toArray();

      if (!empty($categoriesInclude)) {
        foreach ($categoriesInclude as $item) {
          $item = explode(',', $item);
          foreach ($item as $id) {
            $locActiveValues->push($id);
          }
        }
      }
    }

    if ($displayParams['brands_filter'] && $filterUrl == 'brand' && !request()->has('type')) {
      $brandsCounters = $this->getFiltersCounters($products, 'brand');
    }

    if (($filterUrl == 'price_from' || $filterUrl == 'price_to') && ! isset($filterPriceMin)) {
      $filterPriceMin = $products->min('opt_price');
      $filterPriceMax = $products->max('opt_price');
    }

    // фильтр по родительскому цвету
    if ($filterUrl === 'color') {
      if ($activeFilters->has('color')) {
        $colorFilters->push($activeFilters->get('color'));
        $products = $this->filterColor($products, $activeFilters->get('color'));
      }
    }

    // фильтр по дочернему цвету
    if ($filterUrl === 'color-child') {
      if ($activeFilters->has('color-child')) {
        $products = $this->filterColor($products, $activeFilters->get('color-child'), true);
        $colorFilters->push($activeFilters->get('color-child'));
      }
    }

    if ($filterUrl === 'color' || $filterUrl === 'color-child') continue;

    $products = $this->filterProducts($products, collect([$filterUrl => $locActiveValues]));
    if ($filterUrl == 'parent_category') $totalProductsBeforeFilters = $products->count();
  }
//dd($filtersCounters);
  getMicrotime(3, $start);
  // отбираем цвета для вывода в фильтры
  $parentColors = collect(); // родительские цвета для основного фильтра
  $childrenColorsFilter = collect(); // дочерние цвета выбранных родительских фильтров - для фильтрации
  $childrenColors = collect(); // дочерние цвета выбранных родительских фильтров - для фильтрации
  $fullColors = $this->getColors($products); // получаем список всех доступных цветов по выбранным товарам
  if ($fullColors !== null) {
    $parentColors = $this->getColorsForFilter($fullColors); // получаем список всех доступных родительских цветов с учетом выбранных товаров
    $colorFilters = $colorFilters->flatten()->toArray();

    // если был выбран родительский цвет в фильтре, отбираем все его дочерние цвета
    if ($activeFilters->has('color')) {
      $childrenColorsFilter = $parentColors->whereIn('color', $colorFilters)->map(function($item) {
        return $item->children;
      });

      $childrenColorsFilter = $childrenColorsFilter->flatten()->unique()->all(); // полученные дочерние цвета преобразуем в массив отбирая уникальные
      $childrenColorsFilter = array_intersect($childrenColorsFilter, $fullColors); // удаляем дочерние цвета которых нет в выбранных продуктах
      $childrenColorsFilter = $this->getColorsForFilter($childrenColorsFilter, true, $activeFilters->get('color')->toArray());
    }

    // получаем все дочерние цвета для определения нового
    if ($parentColors !== null && $parentColors->count() > 0) {
      $childrenColors = $parentColors->map(function ($item) {
        return $item->children;
      });
      $childrenColors = $childrenColors->flatten()->unique()->all();
      $childrenColors = array_intersect($childrenColors, $fullColors);
      $childrenColors = $this->getColorsForFilter($childrenColors, true);
    }
  }

  getMicrotime(4, $start);

  // Просчет количества товаров для оставшихся фильтров и исключение значений без товаров
  if (!request()->has('type')) {
    getMicrotime('4-1', $start);
    $counters = $this->getFiltersCounters($products);
    foreach ($filtersValues as $filterId => $values) {
      if (!$activeFilters->has($filters->get($filterId)->url)) {
        $filtersCounters = $filtersCounters->union($counters->intersectByKeys($values->keyBy('id')));
      }

      $values = $values->keyBy('id')->intersectByKeys($filtersCounters);
      $filtersValues->put($filterId, $values->count() ? $values : false);
    }
  }

  if (!request()->has('type')) {
    getMicrotime('4-2', $start);
    if ($displayParams['categories_filter']) {
      if ( ! isset($parentCategoriesCounters)) {
        $parentCategoriesCounters = $this->getFiltersCounters($products, 'parent_category');
      }

      if ( ! isset($categoriesCounters)) {
        $categoriesCounters = $categoriesCounters = $this->getFiltersCounters(
          $products, 'category',
          $this->needAddItMenu($displayParams['categories_filter_active_parent'] ?? null)
        );
      }

      if ($displayParams['categories_filter_active_parent']) {
        $parentCategoriesCounters = $categoriesCounters;
      }
    }

    if ($displayParams['brands_filter'] && !isset($brandsCounters)) {
      $brandsCounters = $this->getFiltersCounters($products, 'brand');
    }

    getMicrotime('4-3', $start);

    if (!isset($filterPriceMin)) {
      $filterPriceMin = $products->min('opt_price');
      $filterPriceMax = $products->max('opt_price');
    }

    getMicrotime(5, $start);

    // подсчет количества уцененного товара
    $list_counters = $this->getFiltersCountersNew($products, ['ucenka', 'sale', 'soon', 'novelty', 'coming']);
    $ucenennye_tovary = $list_counters['ucenka'] ?? [];

    // подсчет количества товаров со скидкой
    $sale_tovary = $list_counters['sale'] ?? [];
    // подсчет количества товаров в ожидании
    $soon_tovary = $list_counters['soon'] ?? [];

    // подсчет количества товаров новинки
    $novelty_tovary = $list_counters['novelty'] ?? [];
    $coming_tovary = $list_counters['coming'] ?? [];

    getMicrotime(6, $start);

    $filter_other = false;
    if ($ucenennye_tovary || $sale_tovary || $soon_tovary || $novelty_tovary) {
      $filter_other = true;
    }
  }
  // Сортировка списка товаров
//  $productsSort = '';
//

//  if (request()->cookie('products_sort')
//    && isset($orderList[$products_sort]) !== false
//    && $products->count() > 0) {
//    $productsSort = $products_sort;

//    $products = $products->groupBy('from');
//    $productsPrint = $products->get(1) ?? collect();

    // @TODO: переписать для разных from
//    getMicrotime('6-1', $start);
//    if (!$products->has(0) && !$products->has(2)) {
//      $products = $products->get(1);
//    } else {
//      $productsTmp = collect();
//      $productsTmp = $productsTmp->merge( $products->get(0) ?? collect());
//      $products = $productsTmp->merge( $products->get(2) ?? collect());
//    }
//    getMicrotime('6-2', $start);
//    $products = $products->get(0) ?? $products->get(1);

//    if ($productsSort == 'discounts') {
//      $products = $products->sortBy(function($product)
//      {
//        return ($product->old_opt_price > $product->opt_price) ? 0 : 1;
//      });
//    }
//    if ($productsSort == 'rating') {
//
//    }
//    if ($productsSort == 'coming') $products = $products->sortByDesc('date_coming'); // опускаем принты в самый низ всегда
//    if ($productsSort == 'cheap') $products = $products->sortBy('opt_price'); // опускаем принты в самый низ всегда
//    if ($productsSort == 'expensive') $products = $products->sortByDesc('opt_price'); // опускаем принты в самый низ всегда

//    $products = $products->merge($productsPrint);
//  }

  // для вывода количества
  $totalProducts = $totalProductsForPagination = $products->count();

  //преобразуем в серии
  if ($bySeries) {
    $products = $this->groupBySeries(
      $products,
      $displayParams['urlCategory'],
      $displayParams['labelFroGroupClass'],
      $displayParams['labelFroGroupText']
    );

    $products_sort = request()->cookie('products_sort');
    if ($productsSort == 'rating') { //todo
//      $newUp = today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp;
      $products = $products->sortByDesc('rating');
    }
    // Формирование новой пагинации
    $totalProductsForPagination = $products->count();
  }

  $perPage = request()->cookie('products_per_page');
  if ( ! $perPage || array_search($perPage, [40, 60, 80]) === false) $perPage = $this->productsPerPage;

  $paginationUrl = $baseUrl . ($filtersString ? '/filter/' . $filtersString : '');

  // пагинация
  $products = new LengthAwarePaginator(
    $products->slice(($page - 1) * $perPage, $perPage),
    $totalProductsForPagination,
    $perPage,
    $page,
    ['path' => $paginationUrl, 'query' => request()->query()]
  );

  if (request()->has('type')) {
    $priceRange = new PriceRange();
    view()->share('priceRangeForPrint', $priceRange->priceRangeByType());
    view()->share('amountRange', $priceRange->range);

    if (request()->get('type') == 'html') {
      $html = view('partials.products-loader', compact('products'))->render();

      return response()->json([
        'success' => true,
        'html' => $html,
        'total' => $totalProducts,
        'perPage' => $perPage,
        'page' => $page,
        'current_page' => $products->currentPage(),
        'to' => $products->lastItem(),
        'path' => $paginationUrl,
        'query' => request()->query()
      ], 200, [
        'Content-Type' => 'application/json',
      ]);
    }

    // Если данные запрошены в JSON-формате - возвращаем результат
    if (request()->get('type') == 'json') {
      return new ProductResourceCollection($products);
    }
  }

  // Исключение фильтров без доступных значений
  $filters = $filters->intersectByKeys($filtersValues->filter());

  // если в категории фильтр только техничка, чехлы или стекла-пленкт, показываем все техн. фильтры, иначе - скрываем
  // id = 397 - tehnichka(Техничка) - табл. its_filters_values
  // id = 256 - чехлы
  // id = 257, 258 - стекла-пленки

  $technichkaOnlyOne = (
    ($filtersValuesWithChildren->count() == 1 && ($filtersValuesWithChildren->search(397) !== false || $filtersValuesWithChildren->search(256) !== false || $filtersValuesWithChildren->search(257) !== false || $filtersValuesWithChildren->search(258) !== false))
    ||
    ($filtersValuesWithChildren->count() == 2 && ($filtersValuesWithChildren->search(257) !== false && $filtersValuesWithChildren->search(258) !== false))
  );

  // если в категориях фильтр один, делаем его активным
  if ($filtersValues->has(84) && $filtersValues->get(84) !== false) {
    $filtersCetagories = $filtersValues->get(84);
    if ($filtersCetagories->count() == 1 || ($filtersCetagories->count() == 2 && $filtersCetagories->has(257) && $filtersCetagories->has(258))) {
      $id = $filtersCetagories->first()->id;
      $activeFilters->put('categoriya', collect($id));
    }
  }

  if (!$technichkaOnlyOne) {
    // Исключение фильтров, являющихся дочерними, если их родитель пока не выбран
    $filters = $filters->filter(function ($filter, $key) use ($activeFilters) {
      return !$filter->parent_filters_values || $activeFilters->except('brand')->flatten()
          ->intersect($filter->parent_filters_values)->count();
    });
  }

  // Добавление в детали каждого фильтра списка его значений
  $filters = $filters->map(function($filter) use ($filtersValues, $filtersValuesWithChildren) {
    $filter->availableValues = $filtersValues->get($filter->id)
      ->map(function($value) use ($filtersValuesWithChildren) {
        $value->has_children = $filtersValuesWithChildren->search($value->id) !== false;
        return $value;
      });

    return $filter;
  })
    ->keyBy('url');

  // Получение списка доступных категорий
  $categories = $categoriesSectors = $parentCategories = $accessoriesCategories = null;
//dd($parentCategoriesCounters);
  if ($displayParams['categories_filter']) {
    $categories = DB::table('its_menu')
        ->whereIn('id', $parentCategoriesCounters->keys())
        ->orderBy('priority_phones', 'DESC')
        ->orderBy('priority', 'DESC')
        ->get()
        ->groupBy('parent_id');

//      Category::whereIn('id', $parentCategoriesCounters->keys())
//      ->orderBy('priority_phones', 'DESC')
//      ->orderBy('priority', 'DESC')
//      ->get()
//      ->groupBy('parent_id');

    if ( ! $displayParams['categories_filter_active_parent']) {
      $accessoriesCategories = $categories->pull(config('custom.accessories_category_id'));

      $parentCategories = DB::table('its_menu')
        ->whereIn('id', $categories->keys())
        ->orderBy('priority_phones', 'DESC')
        ->orderBy('priority', 'DESC')
        ->get()
        ->keyBy('id');
//        Category::whereIn('id', $categories->keys())
//        ->orderBy('priority_phones', 'DESC')
//        ->orderBy('priority', 'DESC')
//        ->get()
//        ->keyBy('id');
      $parentCategories->pull(config('custom.accessories_category_id'));

      // Суммирование количества дочерних категорий для получения количества в родителях
      if ($parentCategories) {
        foreach ($parentCategories as $parentCategory) {
          $counter = [0, 0, 0];
          foreach ($categories->get($parentCategory->id) as $category) {
            $counter[0] += $parentCategoriesCounters[$category->id][0];
            $counter[1] += $parentCategoriesCounters[$category->id][1];
            $counter[2] += $parentCategoriesCounters[$category->id][2];
          }

          $categoriesCounters->put($parentCategory->id, $counter);
        }
      }

      // Перенос счетчиков товаров по категориям общих аксессуаров в единый массив
      if ($accessoriesCategories) {
        foreach ($accessoriesCategories as $accessoriesCategory) {
          $categoriesCounters->put($accessoriesCategory->id, $parentCategoriesCounters->get($accessoriesCategory->id));
        }
      }
    }

    // Если выбрана родительская категория - отображаем фильтры по подкатегориям
    if ($activeParentCategory && $categories->has($activeParentCategory->id)) {
      $categories = $categories->get($activeParentCategory->id)
        ->keyBy('id')
        ->intersectByKeys($categoriesCounters)
        ->groupBy('menu_sector');

      // Получение списка секторов для категорий
      $categoriesSectors = CategorySector::whereIn('id', $categories->keys())
        ->orderBy('priority', 'DESC')
        ->get()
        ->keyBy('id');

      if ($categories->has(0)) $categoriesSectors->put(0, (object) ['id' => 0, 'title' => __('Другие модели')]);

      // проверяем была ли модель случайно добавлена в другую категорию
//        $_menus = $products->pluck('menu');
//        $_parentMenu = Category::whereIn('id', $_menus)->get()->groupBy('parent_id')->all();
//        unset($_parentMenu[$activeParentCategory->id]);

      // если есть такие товары
//        if ( count($_parentMenu) > 0) {
//          foreach ($_parentMenu as $key => $_menu) {
//            $first = $_menu->first();
//            $categoriesSectors->put($key, $first);
//            if ($_menu->count() > 0) {
//              if (!$categories->has($first->id)) {
//                $categories->put($first->id, collect());
//              }
//
//              foreach ($_menu->all() as $item) {
//                $categories->get($first->id)->push($item);
//              }
//            }
//          }
//        }
    }
    else $categories = null;
  }

  // Получение списка доступных брендов
  if ($displayParams['brands_filter']) {
    $brands = Brand::whereIn('id', $brandsCounters->keys())->get(['id', 'title', 'our'])->keyBy('id');
  }
  else $brands = null;

  // Получение баннера для отображения на странице
  $banner = $displayParams['banner'];

  // Задание флага страницы с товарами для общего шаблона
  view()->share('layoutProductsListPage', true);

  // Задание canonical-ссылки для страниц пагинации
  if ($page > 1) view()->share('layoutCanonicalLink', $baseUrl . ($parentCategoryUrl ? '/' . $parentCategoryUrl : '')
    . ($filtersString ? '/' . $filtersString : ''));

  // Получение строки фильтров без фильтра по категориям для формирования ссылок на родительские категории
  $filtersStringWithoutCategories = $filtersString ? rtrim(preg_replace('/(categories=[^;]+(;|$))/', '', $filtersString), ';') : '';

  // Группировака категорий фильтров Стекла и Пленки
  // получаем активные ibs фильтров - Категория
  $maxFiltersCategories = null;
  if ($filters->has('categoriya')) {
    $this->groupCategories($filtersCounters, $filters, $activeFilters);

    // выборка топ два фильтра
    $ids = $filters->get('categoriya')->availableValues->map(function($item) {
      return $item->id;
    });

    $activeFilterIds = array_flip($ids->flatten()->all()); // ids выносим как ключи массива
    // filtersCounters - агрегация кол-ва товаров по фильтрам, отбираем только те которые из фильтра Категория
    $intersect = $filtersCounters->intersectByKeys($activeFilterIds);
    // отбираем два фильтра с мак кол-вом товара
    $maxFiltersCategories = $intersect->sortByDesc(0)->take(2)->all();
  }
  //Конец - Группировака категорий фильтров Стекла и Пленки

  // сортируем фильтры Бренд по кол-ву товаров
  $brandsFilter = null;
  if ($brands !== null) {
    $brandsFilter = $this->brandOrder($brands, $brandsCounters);
  }

  // сортировка цвета
  if ($childrenColorsFilter->count() > 0) {
    $childrenColorsFilter = $childrenColorsFilter->sortBy(function($item){
        $item->new = today()->diffInDays($item->created_at) <= 30;
      return $item->color;
    });
    // обнуляем индексы
    $childrenColorsFilter = $childrenColorsFilter->values();
  }

  if ($parentColors !== null) {

    $colorNew = [];

    if ($childrenColors->count()) {
      $childrenColors->map(function($item) use (&$colorNew) {
        if (today()->diffInDays($item->created_at) <= 30) {
          $colorNew[$item->parent_id] = true;
        }
      });
    }

    $parentColors = $parentColors->sortBy(function ($item) use ($colorNew) {
      $item->new = today()->diffInDays($item->created_at) <= 30 || isset($colorNew[$item->id]);
      return $item->color;
    });
    // обнуляем индексы
    $parentColors = $parentColors->values();
  }

   if ($brands === null || $brands->has(323)) {
    $priceRange = new PriceRange();
     view()->share('priceRangeForPrint', $priceRange->priceRangeByType());
     view()->share('amountRange', $priceRange->range);
   }

//   $maxPriority = $products->max('priority');

  // Создание отображения
  return view('products-list', compact([
    'products', 'baseUrl', 'paginationUrl', 'filters', 'activeFilters', 'filtersString', 'filtersCounters',
    'activeParentCategory', 'parentCategoryUrl', 'parentCategories', 'categories', 'categoriesSectors',
    'accessoriesCategories', 'categoriesCounters', 'brands', 'brandsFilter', 'brandsCounters', 'filterPriceMin',
    'filterPriceMax', 'totalProducts', 'totalProductsBeforeFilters', 'perPage', 'productsSort',
    'filtersStringWithoutCategories', 'banner', 'maxFiltersCategories', 'groupBySeries', 'orderList',
    'parentColors', 'colorFilters', 'childrenColorsFilter', 'seoDescription', 'gadgetFilter', 'products_series',
    'filter_other', 'ucenennye_tovary', 'sale_tovary', 'soon_tovary', 'novelty_tovary', 'coming_tovary'
  ]));
}

  /**
   * группируем по сериям
   * @param $products
   * @param string $urlCategory
   * @param string $labelFroGroupClass
   * @param string $labelFroGroupText
   */
protected function groupBySeries($products, string $urlCategory, string $labelFroGroupClass = '', string $labelFroGroupText = '')
{
  $newUp = today()->subDays(Product::NOVELTY_DAYS_ORDER)->timestamp;
  $products->each(function($item) use ($newUp) {
  // проверяем является ли товар Чехол или Стекла\Пленки и исключаем их из группировки для "Группа товаров ..."
    $parentId = $item->additionalCategories->search(
      fn($category) => (($category->parent_id === 1767 || $category->parent_id === 1781) && $item->from === 0)
    );
    $item->rating = $item->priority + (($item->from === 1)
      ? 1
      : (($item->added_time >= $newUp && $item->markdown !== 1) ? 10000 : 10)); //todo

    if ( ($parentId === false && $item->from === 0) || $item->products_series_id === 0 )  {
      $item->products_series_id = '__' . $item->menu;
      $item->categoryTitle = $item->categoryData->title;
      $item->categoryUrl = $item->categoryData->name;
      $item->series = null;
      $item->technical = true;
    }
  });

  $items = $products->groupBy('products_series_id');
  // если получается только одна группа, сразу раскрываем ее
  if ($items->count() === 1) {
    return $products;
  }
  $temp = new \Illuminate\Database\Eloquent\Collection();
  $hasFilter = Str::contains($urlCategory, 'filter');

  if ($hasFilter) {
    $url = $urlCategory;
  } else {
    $url = $urlCategory . '/filter/';
  }

  foreach ($items as $series => $product) {
    if ($series === 0 || $product->count() === 1) {
      foreach ($product as $item) {
        $temp->push($item);
      }
    } else {
        $saleLabel = false;
        $saleAmount = 0;
        $soonLabel = false;
        $soonAmount = 0;
        $noveltyLabel = false;
        $noveltyAmount = 0;
        $markdownLabel = false;
        $markdownAmount = 0;
        $comingLabel = false;
        $comingAmount = 0;

        $optPriceRange = [9999, 0];
        $priceRange = [9999, 0];
        $priceOptVipRange = [9999, 0];
        $priceOptDealerRange = [9999, 0];
        $priceDropRange = [9999, 0];
        $priceOptMinOptRange = [9999, 0];

        $groupProducts = [];
        $groupProductsBase = [];
        $groupProductsMax = 0;

        foreach ($product as $item) {
          $groupProducts = array_merge($groupProducts, $item->group_products);

          $item->opt_vip_usd = $item->getPrice('opt_vip_usd');
          $item->opt_dealer_usd = $item->getPrice('opt_dealer_usd');
          $item->drop_uah = $item->getPrice('drop_uah');
          $item->opt_minopt_usd = $item->getPrice('opt_minopt_usd');
          $item->min_price = $item->getPrice('min_price');
          $item->max_price = $item->getPrice('max_price');

          if ($item->isNovelty()) {
            $noveltyLabel = true;
            $noveltyAmount ++;
          }
          if ($item->isSale()) {
            $saleLabel = true;
            $saleAmount ++;
          }
          if ($item->isSoon()) {
            $soonLabel = true;
            $soonAmount ++;
          }
          if ($item->isMarkdown()) {
            $markdownLabel = true;
            $markdownAmount ++;
          }
          if ($item->isComing()) {
            $comingLabel = true;
            $comingAmount ++;
          }

          // считаем мин-макс цены в серии
          $this->minMaxPrice($optPriceRange, $item->opt_price, $item->min_price, $item->max_price);
          $this->minMaxPrice($priceRange, $item->price);
          $this->minMaxPrice($priceOptVipRange, $item->opt_vip_usd);
          $this->minMaxPrice($priceOptDealerRange, $item->opt_dealer_usd);
          $this->minMaxPrice($priceDropRange, $item->drop_uah);
          $this->minMaxPrice($priceOptMinOptRange, $item->opt_minopt_usd);
        }

        $count = $product->count(); // количество товаров в серии

        // если только одна уценка
        if ($markdownAmount === $count) {
          $tmp = $product->first();
        } else { // если есть другие товары, берем первый но не Уценка
          $tmp = $product->first(function ($value) {
              return $value->markdown !== 1;
          });
        }

        $tmp->groupProducts = $this->mergeColors($tmp->group_products, $groupProducts);
        $tmp->priority = $product->sum('priority');
        $tmp->rating = $tmp->priority + (($tmp->from === 1)
          ? 1
          : 10); //todo

        $tmp->opt_price_range = $optPriceRange;
        $tmp->price_range = $priceRange;
        $tmp->opt_vip_usd_range = $priceOptVipRange;
        $tmp->opt_dealer_usd_range = $priceOptDealerRange;
        $tmp->drop_uah_range = $priceDropRange;
        $tmp->opt_minopt_usd_range = $priceOptMinOptRange;

        if (! $item->technical) {
          $tmp->title = '<span class="text-orange bold">' . __('Серия') . '</span> ' . Str::before($tmp->title, 'для');
        } else {
          $tmp->title = '<span class="text-orange bold">' . __('Группа товаров') . '</span> ' . $tmp->categoryTitle;
        }
        $tmp->title = trim(Str::replaceFirst('Уценка', '', $tmp->title));

        $tmp->bySeries = true;
        $tmp->seriesCount = $count;
        if (isset($tmp->series->url)) {
          $tmp->urlCategory = custom_route('products_series', [$tmp->series->url, $urlCategory]); // основная ссылка для перехода на серию
        } else {
          $tmp->urlCategory =  custom_route('group', [$tmp->categoryUrl, $urlCategory]);
        }

        if (Str::contains($tmp->urlCategory, 'categories')) {
          $tmp->urlSeries = $tmp->urlCategory;
        } else {
          if (isset($tmp->series->url) && !$item->technical) {
            $tmp->urlSeries = custom_route('products_series', [$tmp->series->url]);
          } else {
            $tmp->urlSeries =  custom_route('group', [$tmp->categoryUrl, $urlCategory]);
          }
        }

        if (isset($tmp->series->url) && !$item->technical) {
          $urlBySeries = custom_route('products_series', [$tmp->series->url, $url]);
          $urlBySeries = Str::replaceFirst('//filter', '/filter', $urlBySeries);
        } else {
          $urlBySeries =  custom_route('group', [$tmp->categoryUrl, $url]);
          $urlBySeries = Str::replaceFirst('//filter', '/filter', $urlBySeries);
        }

//        $tmp->labelFroGroupClass = $labelFroGroupClass;
//        $tmp->labelFroGroupText = sprintf('%s %s %s',  $count, word_declension('модел', $count), $labelFroGroupText);

        // урл для перехода на серии
        $labels = [];
        $labelText = '';

        $labelText = ($tmp->technical) ? $tmp->categoryTitle : $labelFroGroupText;

        if ($count > $noveltyAmount && $count > $soonAmount && $count > $saleAmount && $count > $markdownAmount) {
          array_push($labels, [
            'class' => 'marker marker-standard',
            'text' => word_declension('модел', $count),
            'url' => $tmp->urlCategory,
            'amountText' => sprintf('%s %s %s', 'Моделей', $count, $labelText),
            'amount' => $count,
          ]);
        }

        // метки лейблы
        if ($noveltyLabel) {
          array_push($labels, [
            'class' => 'marker marker-new',
            'text' => 'New',
            'url' => sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'novelty=true'),
            'amountText' => sprintf('%s %s %s', 'Новинки', $noveltyAmount, $labelText),
            'amount' => $noveltyAmount,
          ]);

          if ($noveltyAmount === $count && ! Str::contains($urlCategory, 'novelty=true')) {
            $tmp->urlCategory = sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'novelty=true');
          }
        }

        if ($soonLabel) {
          array_push($labels, [
            'class' => 'marker marker-soon',
            'text' => 'Скоро',
            'url' => sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'soon=true'),
            'amountText' => sprintf('%s %s %s', 'Ожидается', $soonAmount, $labelText),
            'amount' => $soonAmount,
          ]);

          if ($soonAmount === $count && ! Str::contains($urlCategory, 'soon=true')) {
            $tmp->urlCategory = sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'soon=true');
          }
        }

        if ($saleLabel) {
          array_push($labels, [
            'class' => 'marker marker-sale',
            'text' => 'Sale',
            'url' => sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'sale=true'),
            'amountText' => sprintf('%s %s %s', 'Со скидкой', $saleAmount, $labelText),
            'amount' => $saleAmount,
          ]);
          // если весь товар со скидкой
          if ($saleAmount === $count && ! Str::contains($urlCategory, 'sale=true')) {
            $tmp->urlCategory = sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'sale=true');
          }
        }

        if ($markdownLabel) {
          array_push($labels, [
            'class' => 'marker marker-markdown',
            'text' => 'Уценка',
            'url' => sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/',
              (! Str::contains($urlCategory, 'ucenka=true')) ? 'ucenka=true' : ''),
            'amountText' => sprintf('%s %s %s', 'Уцененных', $markdownAmount, $labelText),
            'amount' => $markdownAmount,
          ]);

          // если весь товар уценен, прописываем фильтр
          if ($markdownAmount === $count) {
            $tmp->labelFroGroupClass = 'label bg-ucenka border-radius';
            if (! Str::contains($urlCategory, 'ucenka=true')) {
              $tmp->urlCategory = sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/',
                (! Str::contains($urlCategory, 'ucenka=true')) ? 'ucenka=true' : '');
            }
            $tmp->labelFroGroupText = sprintf('%s %s %s %s',
              $count, word_declension('модел', $count), word_declension('уцененн', $count), $labelText);
          }
        }

        if ($comingLabel) {
          array_push($labels, [
            'class' => 'marker marker-coming',
            'text' => __('Приход'),
            'url' => sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'coming=true'),
            'amountText' => sprintf('%s %s %s', 'Поступления', $comingAmount, $labelFroGroupText),
            'amount' => $comingAmount,
          ]);
          // если весь товар со скидкой
          if ($comingAmount === $count && ! Str::contains($urlCategory, 'coming=true')) {
            $tmp->urlCategory = sprintf('%s%s%s', $urlBySeries, ($hasFilter) ? ';' : '/', 'sale=true');
          }
        }

        $tmp->labels = $labels;

        $temp->push($tmp);
    }
  }
  return $temp;
}

private function minMaxPrice(&$data, $price, $min = 0, $max = 0)
{
  if ($min > 0) {
    $data[0] = $min;
  }
  if ($max > 0) {
    $data[1] = $max;
  }

  if ($price < $data[0]) {
    $data[0] = $price;
  } elseif ($price > $data[1]) {
    $data[1] = $price;
  }
}

private function mergeColors($base, $addColors)
{
  $colors = array_column($base, 'color');
  foreach ($addColors as $item) {
    if (!in_array($item->color, $colors)) {
      $base[] = $item;
      $colors[] = $item->color;
    }
  }

  return $base;
}

/**
 * Нужно ли добавлять фильтры из addit_menus
 * @param $category
 * @return bool
 */
private function needAddItMenu($category): bool
{
  if (!isset($category->id)) return true;

  return !in_array($category->id, [1767,1781,1907,1911,683,682,1786]);
}

/**
 * Группируем категории стекла - пленки
 * @param $filtersCounters
 * @param $filters
 * @param $activeFilters
 */
protected function groupCategories(&$filtersCounters, &$filters, &$activeFilters): void
{
  $stekla = 257;
  $plenki = 258;
  $stekla_plenki = $stekla . ',' . $plenki;
  if ($filtersCounters->has($stekla) && $filtersCounters->has($plenki)) {

    // суммируем количество
    $counter = [0, 0, 0];
    $tmp = $filtersCounters->get($stekla);
    $counter[0] += $tmp[0];
    $counter[1] += $tmp[1];
    $counter[2] += $tmp[2] ?? 0;
    $filtersCounters->forget($stekla);

    $tmp = $filtersCounters->get($plenki);
    $counter[0] += $tmp[0];
    $counter[1] += $tmp[1];
    $counter[2] += $tmp[2] ?? 0;
    $filtersCounters->forget($plenki);

    $filtersCounters->put($stekla_plenki, $counter);

    // удаляем фильтры 'Защитные стекла' и 'Защитные пленки'
    $filters->get('categoriya')->availableValues->forget($stekla);
    $filters->get('categoriya')->availableValues->forget($plenki);

    // создаем новый агрегированный фильтр
    $filters->get('categoriya')->availableValues->push((object)[
      'id' => $stekla_plenki,
      'filter_id' => 84,
      'url' => 'stekla-plenki',
      'name' => 'Стекла / Пленки',
      'code' => null,
      'priority' => 1,
      'has_children' => true,
    ]);

    if ($activeFilters->has('categoriya')
      && $activeFilters->get('categoriya')->search($stekla) !== false
      && $activeFilters->get('categoriya')->search($plenki) !== false
    ) {
      $activeFilters->get('categoriya')->forget(
        $activeFilters->get('categoriya')->search($stekla)
      );
      $activeFilters->get('categoriya')->forget(
        $activeFilters->get('categoriya')->search($plenki)
      );
      $activeFilters->get('categoriya')->push($stekla_plenki);
    }
  }
}

/**
 * Сортировка брендов по количеству товаров
 */
protected function brandOrder($brands, $brandsCounters)
{
  $data = collect([]);
  $our = collect([]);
  foreach ($brands as $brand) {
    if ($brandsCounters->has($brand->id)) {
      $amount = $brandsCounters->get($brand->id);

      if ($brand->our === 1) {
        $our->put($brand->id, (object)[
          'id' => $brand->id,
          'title' => $brand->title,
          'our' => $brand->our,
          'amount' => $amount[0],
          'amountNew' => $amount[1],
          'amountComing' => $amount[2],
        ]);
      } else {
        $data->put($brand->id, (object)[
          'id' => $brand->id,
          'title' => $brand->title,
          'our' => $brand->our,
          'amount' => $amount[0],
          'amountNew' => $amount[1],
          'amountComing' => $amount[2],
        ]);
      }
    }
  }

  if ($data->count() > 0) {
    $data = $data->sortByDesc('amount', SORT_NUMERIC);
  }
  if ($our->count() > 0) {
    $our = $our->sortByDesc('amount', SORT_NUMERIC);
  }

  return  collect(['our' => $our, 'data' => $data]);
}

/**
 * Разбор строки с фильтрами из URL'а
 */
protected function parseFiltersString($filtersString = '')
{
  $filters = collect();
  foreach (explode(';', $filtersString) as $index => $value)
  {
    $value = explode('=', $value);
    if (is_array($value) && count($value) == 2)
    {
      $filters->put($value[0], collect(explode(',', $value[1])));
    }
  }

  return $filters;
}

  protected function getFiltersCountersNew($products = null, $filtersType = 'custom', bool $addItMenu = false)
  {
    $filtersCounters = [];

    $filtersType = (!is_array($filtersType)) ? [$filtersType] : $filtersType;

    foreach ($products as $product) {
      if ($product->from === 1 && Route::getCurrentRoute()->getName() !== 'products_series') continue;

      $productValues = [];

      // уценка
      if (in_array('ucenka', $filtersType) && $product->isMarkdown()) {
        $productValues[] = 'ucenka';
      }
      // скидка
      if (in_array('sale', $filtersType) && $product->isSale()) {
        $productValues[] = 'sale';
      }
      // скоро
      if (in_array('soon', $filtersType) && $product->isSoon()) {
        $productValues[] = 'soon';
      }
      // новинки
      if (in_array('novelty', $filtersType) && $product->isNovelty()) {
        $productValues[] = 'novelty';
      }
      // приход
      if (in_array('coming', $filtersType) && $product->isComing()) {
        $productValues[] = 'coming';
      }

      // Увеличение счетчиков для всех значений, к которым относится текущий товар
      foreach ($productValues as $filtersValue) {
        if (! isset($filtersCounters[$filtersValue])) $filtersCounters[$filtersValue] = [0, 0];

        $filtersCounters[$filtersValue][0]++;
        if ($product->isNovelty()) $filtersCounters[$filtersValue][1]++;
      }
    }

    return $filtersCounters;
  }

/**
 * Просчет количества товаров для каждого значения фильтра
 */
protected function getFiltersCounters($products = null, $filtersType = 'custom', bool $addItMenu = false)
{
  $filtersCounters = [];

  foreach ($products as $product) {
    if ($filtersType == 'category' && $product->from === 1 && Route::getCurrentRoute()->getName() !== 'products_series') continue;

    // Получение списка всех значений фильтров для текущего товара
    $productValues = [];

    if ($filtersType != 'custom') {
      if ($filtersType == 'brand') $productValues = [$product->brand];
      if ($filtersType == 'category') {
        $productValues = [$product->menu];
//          $addItMenu = true;
//          if ($product->addit_menus && $addItMenu) { // если у товара есть дополнительные категории
//          if ($product->addit_menus) { // если у товара есть дополнительные категории
//            $addit_menus = explode(',', $product->addit_menus);
//            foreach ($addit_menus as $menu) {
//              $productValues[] = $menu;
//            }
//          }

        if ($product->additionalCategories->count() > 0) {
          $additionalCategories = $product->additionalCategories->pluck('id')->all();
          foreach ($additionalCategories as $menu) {
            $productValues[] = $menu;
          }

        }
      }

      if ($filtersType == 'parent_category') {
        $productValues = [$product->menu];
      }
    }
    else $productValues = $product->filters_values;

    // Увеличение счетчиков для всех значений, к которым относится текущий товар
    foreach ($productValues as $filtersValue) {
      if ( ! isset($filtersCounters[$filtersValue])) $filtersCounters[$filtersValue] = [0, 0, 0];

      $filtersCounters[$filtersValue][0]++;
      if ($product->isNovelty()) $filtersCounters[$filtersValue][1]++;
      if ($product->isComing()) $filtersCounters[$filtersValue][2]++;
    }
  }

  // отображение категорий из include_categories
  if ($filtersType == 'category' && !empty($filtersCounters)) {
    $keys = array_keys($filtersCounters); // получаем список ид категорий
    // формируем строку запроса
    $amount = count($keys) - 1;
    $whereLike = '';
    foreach ($keys as $num => $key) {
      $whereLike .= ' include_categories LIKE "%' . $key . '%" ';
      if ($num < $amount) $whereLike .= 'OR';
    }

    // получаем категории которые включают в себя другие категории
    $includeCategories = DB::table('its_menu')->whereRaw($whereLike)->get(['id', 'include_categories'])->toArray();
//    $includeCategories = Category::whereRaw($whereLike)->get(['id', 'include_categories'])->toArray();

    if ($includeCategories != null) {
      foreach ($includeCategories as $categories) {
        if (!isset($filtersCounters[$categories->id])) { // если категория еще не просчитана
          $arrCategories = explode(',', $categories->include_categories);
          foreach ($arrCategories as $item) { // проходимся по массиву include_categories и проверяем что есть чтоб взять данные по кол-ву
            if (isset($filtersCounters[$item])) {
              $filtersCounters[$categories->id] = $filtersCounters[$item];
              break;
            }
          }
        }
      }
    }
  }

  return collect($filtersCounters);
}

/**
 * Фильтрация списка товаров
 */
protected function filterProducts($products = null, $activeFilters = [])
{
  $filterPriceTypes = ['opt_price', 'opt_vip_usd', 'opt_vip_usd_max', 'opt_dealer_usd', 'opt_dealer_usd_max'];

  return $products->filter(function($product) use ($activeFilters, $filterPriceTypes)
  {
    $flag = true;

    $productFiltersValues = collect($product->filters_values);

    foreach ($activeFilters as $name => $values)
    {
      switch ($name)
      {
        case 'sale': return $product->isSale();
        case 'soon': return $product->isSoon();
        case 'novelty': return $product->isNovelty();
        case 'coming': return $product->isComing();

        case 'price_from':
          $flag = false;

          foreach ($filterPriceTypes as $priceType)
          {
            $price = $product->getPrice($priceType);
            if ($price && $price >= $values[0])
            {
              $flag = true;
              break;
            }
          }

          break;

        case 'price_to':
          $flag = false;

          foreach ($filterPriceTypes as $priceType)
          {
            $price = $product->getPrice($priceType);
            if ($price && $price <= $values[0])
            {
              $flag = true;
              break;
            }
          }

          break;

        case 'parent_category':
        case 'categories':
          $productCategories = array_merge([$product->menu], $product->additionalCategories->pluck('id')->toArray());
          if ( ! $values->intersect($productCategories)->count()) $flag = false;
          break;

        case 'brand':
          if ($values->search($product->brand) === false) $flag = false;
          break;

        case 'price_currency':
          break;

        case 'ucenka':
          if ($product->markdown !== 1) $flag = false;
          break;

        default:
          if ( ! $productFiltersValues->intersect($values)->count()) $flag = false;
      }

      if ( ! $flag) break;
    }

    return $flag;
  });
}

/**
 * Фильтруем товар по цветам
 * @param $products - список продуктов которые нужно отфильтровать
 * @param $colors - цвета по которым нужно отобрать
 * @param $getChildrenOnly - true - фильтровать только по дочерним цветам,
 *                          false - фильтровать по агрегации дочерних цветов
 * @return mixed
 */
protected function filterColor($products, $colors, $getChildrenOnly = false)
{
  $colors = $colors->toArray();
  $parentColor = $colors;
  if ($getChildrenOnly === false) {
    $childrenColor = ProductsColor::whereIn('color', $colors)->where('parent_id', 0)->get();
    if ($childrenColor->count() === 0) {
      return $products;
    }

    $colors = $childrenColor->pluck('children')->filter()->flatten()->toArray();
    foreach ($parentColor as $item) {
      array_push($colors, $item);
    }
  }

  $products = $products->filter(function($product) use ($colors) {
    foreach($product->group_products as $item) {
      if (in_array($item->color, $colors)) {
        return true;
      }
    }
  });

  return $products;
}

/**
 * получить список цветов для фильтра
 * @param $colors -  доступные цвета в отобранных продуктах
 * @param $getChildrenOnly - true - фильтровать только по дочерним цветам,
 *                          false - фильтровать по агрегации дочерних цветов
 * @return |null
 */
protected function getColorsForFilter(array $colors, $getChildrenOnly = false, $parentColors = [])
{
  if ($getChildrenOnly === false) {
    $parentColors = ProductsColor::whereIn('color', $colors)->get();

    if ($parentColors->count() === 0) {
      return null;
    }

    $parentColors = $parentColors->map(function($color) {
      return ($color->parent_id === 0) ? $color->id : $color->parent_id;
    });

    return ProductsColor::whereIn('id', $parentColors->unique())->get();
  } else {
    $result = ProductsColor::whereIn('color', $colors)
      ->where('parent_id', '>', 0)
      ->whereNotIn('color', $parentColors)
      ->orderBy('parent_id')
      ->get();

    return $result->unique('color');
  }
}

/**
 * Список всех цветов в товаре
 * @param Product collect
 * @return array|null
 */
protected function getColors($products)
{
  $colors = $products->map(function($product) {
    if ($product->group_products !== null)
      return array_column($product->group_products, 'color');
  });

  if ($colors->count() == 0) {
    return null;
  }

  $colors =  array_unique($colors->flatten()->toArray());
  return $colors;
}
}