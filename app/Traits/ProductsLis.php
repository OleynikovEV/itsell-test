<?php

namespace App\Traits;

use App\Http\Models\UrlParser;
use App\View\Components\FilterBrandsComponent;
use App\View\Components\FilterCategoryComponent;
use App\View\Components\FilterPricesComponent;
use App\View\Components\FiltersPanelComponent;
use App\View\Components\ModelPanelComponent;
use App\View\Components\ProductCardComponent;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait ProductsLis
{
  protected $products;
  protected $queryParams;

  // @TODO: , $section, $url, $params - переделать в модель и передавать как модель
  protected function display(Collection $products, $section, $url, $params)
  {
    $start = microtime(true);

    $this->products = $products;

    $this->queryParams = new UrlParser($section, $url, $params);

    // ФИЛЬТРЫ - МОДЕЛИ, БРЕНДЫ
    // фильтруем товар по моделям
    $this->products = $this->products->when($this->queryParams->getModels() !== null, fn($q) =>
      $q->whereIn('model_id', $this->queryParams->getModels())
    )->when( $this->queryParams->getBrands() !== null, fn($q) =>
      $q->whereIn('brand_manufacturer_id', $this->queryParams->getBrands())
    );

    // получаем фильтры блока Категории
    $categories = DB::table('its_filters_values')
      ->select(['id', 'name', 'url'])
      ->orderBy('priority');
    $categories = $this->helper($this->products, 'category_id', $categories);

    // ФИЛЬТРЫ
    // применяем другие фильтры
    $this->products = $this->products->when($this->queryParams->getCategories() !== null, fn($q) =>
      $q->whereIn('category_id', $this->queryParams->getCategories())
    );

    // НАЧАЛО ПРИМЕНЕНИЕ ОБЩИХ ФИЛЬТРОВ
    // Получаем дополнительные фильтры
    $filters = collect();
    if ($this->queryParams->getCategories() !== null) {
      // получаем массив выбранных фильтров, исключая фильтры по категориям, моделям, брендам
      $selectedFilters = $this->queryParams->getActiveFiltersArray(['categoriya', 'categories', 'brand']);

      // получаем список товаров с учетом выбранных фильтров
      $productsIdsSelected = null;
      if (count($selectedFilters) > 0) {
        $productsIdsSelected = DB::table('its_product_filter')
          ->select('product_id')
          ->whereIn('filter_value_id', $selectedFilters)
          ->groupBy('product_id')
          ->pluck('product_id');
      }

      // отбираем фильтры по товарам
      $productIds = $this->products
        ->when($productsIdsSelected !== null, fn($q) => $q->wherein('product_id', $productsIdsSelected))
        ->pluck('product_id')->unique();

      // согласно выбранным фильтрам, отбираем товары для других блоков фильтров
      $this->products = $this->products->when($productsIdsSelected !== null, fn($q) =>
        $q->wherein('product_id', $productsIdsSelected)
      );

      // Слабое место +2 сек к скрипту
//      dump( microtime(true) - $start );
      $productsFilters = DB::table('its_product_filter')
        ->selectRaw('filter_value_id, COUNT(product_id) AS amount,
          SUM(novelty) AS novelty, SUM(coming) AS coming')
        ->whereIn('product_id', $productIds)
        ->whereIn('category_id', $this->queryParams->getCategories())
        ->groupBy('filter_value_id')
        ->get()
        ->keyBy('filter_value_id');
//      dump( microtime(true) - $start );

      // Названия дополнительных фильтров
      $filterValueId = $productsFilters->pluck('filter_value_id')->unique();
      $filtersValues = DB::table('its_filters_values')
        ->select(['id', 'name', 'url', 'filter_id'])
        ->whereIn('id', $filterValueId)
        ->get()
        ->keyBy('id');

      // сливаем счетчики и названия фильтров
      foreach ($filtersValues as &$item) {
        $itemCounter = $productsFilters->get($item->id);
        $item->amount = $itemCounter->amount;
        $item->novelty = $itemCounter->novelty;
        $item->coming = $itemCounter->coming;
      }

      // помечаем выбранные фильтры
      $this->selectedFiltersValues($filtersValues, $this->queryParams->getActiveFiltersArray());

      // группируем фильтры по родительским блокам
      $filtersValues = $filtersValues->groupBy('filter_id');

      // получаем родительские блоки для фильтров
      $filtersIds = $filtersValues->keys();
      $filters = DB::table('its_filters')
        ->whereIn('id', $filtersIds)
        ->orderBy('priority')
        ->get(['id', 'name', 'url']);

      // объединяем родительский блок и их значения
      foreach ($filters as &$block) {
        $block->values = $filtersValues->get($block->id);
      }
    }
    // КОНЕЦ ПРИМЕНЕНИЕ ОБЩИХ ФИЛЬТРОВ

    // получаем фильтр по цене
    $prices = collect([
      'price_from' => $this->products->where('opt_price', '>', 0)->min('opt_price'),
      'price_to' => $this->products->max('opt_price'),
    ]);

    // получаем фильтры блока Модели
    $models = DB::table('its_menu')
      ->selectRaw('id, name AS url, title AS name, menu_sector AS sector_id')
      ->orderByDesc('priority');

    // если выбраны фильтры только по моделям, отображаем все модели и их счетчики,
    // в противном случае отображаем модель согласно выбранным фильтрам
    $productsForModels = ($this->queryParams->amount() === 1 && $this->queryParams->getModels() !== null)
      ? $products
      : $this->products;

    $models = $this->helper($productsForModels,'model_id', $models);

    // @TODO: привязать к родителю
//    $models = $models->map(function($item) {
//      // удаляем дренд из названия модели
//      $name = explode(' ', $item->name);
//      unset($name[0]);
//      $item->name = implode(' ', $name);
//      return $item;
//    });

    // получаем фильтры блока Категории
    $brands = DB::table('its_brands')
      ->selectRaw('id, name AS url, title AS name, our')
      ->orderBy('priority');
    $brands = $this->helper($this->products, 'brand_manufacturer_id', $brands, true);

    // Получаем Ids уже отфильтрованных товаров для отбора карточек товара
    $productIds = $this->products->pluck('product_id') ;

    // если ajax запрос
    if (request()->has('type') && request()->get('type') === 'json') {
      $componentCategories = new FilterCategoryComponent($categories);
      $componentModels = new ModelPanelComponent($models);
      $componentPrices = new FilterPricesComponent($prices);
      $componentFilters = new FiltersPanelComponent($filters, $prices);
      $componentProducts = new ProductCardComponent($productIds);
      $componentBrands = new FilterBrandsComponent($brands);

      return response()->json([
        'categoriesHtml' => $componentCategories->resolveView()->with($componentCategories->data())->render(),
        'modelsHtml' => $componentModels->resolveView()->with($componentModels->data())->render(),
        'pricesHtml' => $componentPrices->resolveView()->with($componentPrices->data())->render(),
        'filtersHtml' => $componentFilters->resolveView()->with($componentFilters->data())->render(),
        'brandsHtml' => $componentBrands->resolveView()->with($componentBrands->data())->render(),
        'productsHtml' => $componentProducts->resolveView()->with($componentProducts->data())->render(),
      ]);
    }

    return view('shop', compact('categories', 'models', 'brands',
      'filters', 'productIds', 'prices')
    );
  }

  /**
   * helper - для удобного получения данныхдля вывода
   */
  protected function helper(Collection $products, string $fieldForCount, $query, bool $orderByAmount = false): Collection
  {
    $counter = $this->counterBy($products, $fieldForCount);

    // получаем фильтры блока Категории
    $categories = $query->whereIn('id', $counter->keys())->get()->keyBy('id');

    // соединяем фильтры и счетчики
    $this->mergeFiltersWithCounters($categories, $counter);

    // если выбраны фильтры, помечаем их
    $this->selectedFiltersValues($categories, $this->queryParams->getActiveFiltersArray());

    if ($orderByAmount){
      return $categories->sortByDesc('amount');
    }

    return $categories;
  }

  /**
   * Подсчет товаров, а также новионк и поступлеий
   * @param Collection $data - соллекция товаров для подсчета
   * @param string $field - по какому полю считать
   * @return Collection
   */
  protected function counterBy(Collection $data, string $field): Collection
  {
    $counter = [];

    foreach ($data as $item) {
      $id = $item->{$field};

      if (! isset($counter[$id]) ) $counter[$id] = [0, 0, 0];

      $counter[$id][0] ++;
      if ($item->novelty) $counter[$id][1] ++;
      if ($item->coming)  $counter[$id][2] ++;
    }

    return collect($counter);
  }

  /**
   * Пл выбранным фильтрам помесаем позиции в коллекции
   * @param Collection $collection - коллекция с фильтрами
   * @param array $activeFilters - массив с выбранными фильрами
   */
  protected function selectedFiltersValues(Collection &$collection, array $activeFilters)
  {
    if (count($activeFilters) === 0) return;

    foreach ($activeFilters as $item) {
      if(! $collection->has($item)) continue;

      $collection->get($item)->selected = 'checked';
    }
  }

  /**
   * Соединяем значения фильтров и счетчики
   * @param Collection $collection - коллекция фильтров
   * @param Collection $counters - коллекция счетчиков
   */
  protected function mergeFiltersWithCounters(Collection &$collection, Collection $counters)
  {
    foreach ($collection as $key => $item) {
      // если в коллекци фильтров нет товара, удаляем такой фильтр
      if (! $counters->has($key)) $collection->forget($key);

      $itemCounter = $counters->get($key);

      $item->amount = $itemCounter[0];
      $item->novelty = $itemCounter[1];
      $item->coming = $itemCounter[2];
    }
  }
}