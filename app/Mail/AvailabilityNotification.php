<?php


namespace App\Mail;


class AvailabilityNotification extends \Illuminate\Mail\Mailable
{
  public $products;
  public $user;

  public function __construct($user, $products)
  {
    $this->user = $user;
    $this->products = $products;
  }

  public function build()
  {
    return $this->subject(__('Поступление товаров itsellopt.com.ua'))->view('emails.availability');
  }
}
