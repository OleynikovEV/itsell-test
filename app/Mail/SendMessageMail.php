<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMessageMail extends Mailable
{
    use Queueable, SerializesModels;

    public $formData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($formData = [])
    {
        $this->formData = $formData;

      if (isset($this->formData['purchase_type'])) {
        $purchase_type = implode(',', $this->formData['purchase_type']);
        $purchase_type = str_replace('wholesale', 'Опт', $purchase_type);
        $purchase_type = str_replace('dropshipping', 'Дропшиппинг', $purchase_type);
        $this->formData['purchase_type'] = $purchase_type;
      }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->subject('Отзывы / пожелания')->view('emails.message');
        return $this->subject('Отзывы / пожелания')->view('emails.feedback');
    }
}
