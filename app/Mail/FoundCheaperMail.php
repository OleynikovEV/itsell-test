<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FoundCheaperMail extends Mailable
{
    use Queueable, SerializesModels;

    public $formData;
    public $product;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($formData = [], $product = null)
    {
        $this->formData = $formData;
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Нашли товар дешевле')->view('emails.found-cheaper');
    }
}
