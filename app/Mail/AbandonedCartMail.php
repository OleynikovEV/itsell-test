<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AbandonedCartMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $cart;
    public $cartTotal;

     // Тип сообщения (1 - для покупателя, 2 - для менеджеров)
    public $mailType;

    public $cartName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $cart, $cartTotal, $mailType = 1, $cartName = 'default')
    {
        $this->user = $user;
        $this->cart = $cart;
        $this->cartTotal = $cartTotal;
        $this->mailType = $mailType;

        $cart_list = config('custom.cart_list') ?? [];
        $this->cartName = isset($cart_list[$cartName]) ? $cart_list[$cartName] : __('Основная');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Формирование темы сообщения в зависимости от типа сообщения
        $subject = $this->mailType == 1 ? trim($this->user->fio) . ', у Вас незавершенная покупка!' 
            : 'У пользователя незавершенная покупка (' . ($this->user->city ?: 'Город не указан') . ')';

        // Подключение сообщения
        return $this->subject($subject)->view('emails.abandoned-cart');
    }
}
