<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    // Тип сообщения (1 - для покупателя, 2 - для менеджеров)
    public $mailType;

    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order, $mailType = 1, $subject = null)
    {
        $this->order = $order;
        $this->mailType = $mailType;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Загрузка списка товаров в заказе
        $this->order->load('products.product');

        // Формирование темы сообщения в зависимости от типа сообщения
        $subject = $this->mailType == 1 ? __('Заказ') . ' №' . $this->order->id . ' ' . __('успешно оформлен')
            : 'Заказ в интернет-магазине ' . config('app.name') . ' (' . ($this->order->dropshipping ? 'Дропшиппинг' : 'Опт') . ')';

        $subject = $this->subject ?? $subject;

        if ($this->mailType === 2) {
          $hasPrint = $this->order->products->where('print', 1);
          if ($hasPrint->count() > 0) {
            $subject = 'ПРИНТЫ ' . $subject;
          }
        }

        // Подключение сообщения
        return $this->subject($subject)->view('emails.order-created');
    }
}
