<?php

namespace App\Exceptions;

use App\Http\Models\TelegramMessanger;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Throwable;
use App\Http\Controllers\MainPageController;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    protected $exceptClassError = [
      'Illuminate\Validation\ValidationException',
      'Illuminate\Auth\AuthenticationException',
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof NotFoundHttpException || $exception instanceof MethodNotAllowedHttpException)
        {
            $view = app(MainPageController::class)->notFound();
            return response()->make($view, 404);
        }

//        if (config('app.env') === 'local') {
        if (config('app.env') === 'production') {
          // перехватываем ошибки и отправляем с tg чат
          $e = FlattenException::create($exception);
          $statusCode = $e->getStatusCode($e);
          $class = $e->getClass();
//          dd($class);
          if ($statusCode === 500 && !in_array($class, $this->exceptClassError)) {
            try {
              $formatString = 'Ошибка %s <a href="%s">по ссылке</a>'
                . PHP_EOL . 'Remote IP: %s' . PHP_EOL . '%s' . PHP_EOL . 'файл: %s' . PHP_EOL . 'строка: %s';

              (new TelegramMessanger())->sendMessage(
                746276963,
                sprintf($formatString,
                  $statusCode,
                  $request->fullUrl(),
                  implode(',', $request->ips()),
                  $e->getMessage(),
                  $e->getFile(),
                  $e->getLine()
                )
              );
            } catch (\Exception  $e) {
            }
          }
        }
        
        return parent::render($request, $exception);
    }
}
