<?php

namespace App\Providers;

use App\Models\Country;
use App\Models\Setting;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      // @TODO: сбросить кэш по событию изменения данных
        Cache::forever('settings', Setting::all());

        Validator::extend('phone_number', function ($attribute, $value, $parameters, $validator) 
        {
            $flag = false;

            $rules = Country::get('rules')->pluck('rules')->toArray();

            $value = '+' . preg_replace('/[^0-9]/', '', $value);

            foreach ($rules as $pattern) {
              if (preg_match($pattern, $value) != false) {
                return true;
              }
            }

//            $value = preg_replace('/[^0-9]/', '', $value);
//            if (isset($parameters[0]) && strlen($value) != $parameters[0]) $flag = false;
//            if (isset($parameters[1]) && substr($value, 0, strlen($parameters[1])) !== $parameters[1]) $flag = false;

            return $flag;
        });
    }
}
