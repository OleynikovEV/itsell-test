<?php

namespace App\Providers;

use App\Events\CartModified;
use App\Events\OrderCreated;
use App\Events\PriceZeroEvent;
use App\Events\VchehleEvent;
use App\Listeners\CartStoreInUserData;
use App\Listeners\OrderChangeStatusSendNotify;
use App\Listeners\OrderSendCreatedNotification;
use App\Listeners\PriceZeroNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OrderCreated::class => [OrderSendCreatedNotification::class],
        CartModified::class => [CartStoreInUserData::class],
        VchehleEvent::class => [OrderChangeStatusSendNotify::class],
        PriceZeroEvent::class => [PriceZeroNotification::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
