<?php

namespace App\Providers;

use App\Services\TextTemplates;
use Illuminate\Support\ServiceProvider;

class TextTemplatesServiceProvider extends ServiceProvider
{
    
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('text-templates', function()
        {
            return new TextTemplates();
        });
    }

    public function provides()
    {
        return ['text-templates'];
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}
