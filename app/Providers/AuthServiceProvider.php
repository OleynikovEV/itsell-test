<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use App\Services\Auth\MD5UserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Провайдер для авторизации пользователей со старой версии сайта
        Auth::provider('md5-pass', function($app, array $config)
        {
            return new MD5UserProvider();
        });
    }
}
