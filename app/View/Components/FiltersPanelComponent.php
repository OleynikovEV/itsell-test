<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Collection;

class FiltersPanelComponent extends Component
{
  const FILTER_CATEGORY = 84;
  const FILTER_OTHER = 119;
  const FILTER_MODEL = 120;
  const FILTER_OUR_BRANDS = 121;
  const FILTER_BRANDS = 122;

  public $filters;

  public function __construct(Collection $filters)
  {
    $this->filters = $filters;
  }

  public function render()
  {
    return view('components.filters-panel');
  }
}
