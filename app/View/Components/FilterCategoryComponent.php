<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class FilterCategoryComponent extends Component
{
  public $categories;

  public function __construct(Collection $categories)
  {
    $this->categories = $categories;
  }

  public function render()
  {
    return view('components.filter-category');
  }
}
