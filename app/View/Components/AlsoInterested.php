<?php

namespace App\View\Components;

use App\Models\Product;
use Illuminate\View\Component;

class AlsoInterested extends Component
{
  public $products;

  private $filresArray = [256, 257, 258];

  public function __construct(array $filtersValues, int $productId, int $menu, $filters)
  {
    if (count(array_intersect($filtersValues, $this->filresArray))) {
      // Получение списка товаров из текущей категории
      $this->products = Product::active()->where('id', '!=', $productId)
        ->where('menu', $menu)
        ->get();

      // Получение позиций того же типа, что и текущий товар
      $productType = array_values(array_intersect($filtersValues, $this->filresArray))[0];
      $this->products = $this->products->filter(fn($item) => array_search($productType, $item->filters_values));

      // Если у товара задан фильтр по материалу - подбираем товары с такими же материалами
      $materialFilter = $filters->firstWhere('id', 80);
      if ($materialFilter) {
        $productMaterials = $materialFilter->product_values->pluck('id');
        $this->products = $this->products->filter(function ($item) use ($productMaterials) {
          return $productMaterials->intersect($item->filters_values)->count();
        });
      }

      // Выносим в приоритет товары заданных брендов и получаем ограниченное количество первых позиций
      $this->products = $this->products->sortBy(function ($item) {
        return array_search($item->brand, [296, 297]) === false;
      })->take(10);

      $this->products->load('series');
    }
  }

    public function render()
    {
        return view('components.also-interested');
    }
}
