<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PopupPrice extends Component
{
    public $product;
    public $bySeries;
    public $ru = [];
    public $class = [];

    /**
     * Всплывающее окно с дополнительными ценами
     *
     * @return void
     */
    public function __construct($product, $bySeries = false)
    {
        $this->product = $product;
        $this->bySeries = $bySeries ?? false;

        $this->ru = [
          'price' => __('РРЦ'),
          'drop_uah' => __('Дроп'),
          'opt_price' => __('Опт'),
          'opt_vip_usd' => 'VIP',
          'opt_dealer_usd' => __('Дилер'),
        ];

        $this->class = [
          'price' => '',
          'drop_uah' => '',
          'opt_price' => '',
          'opt_vip_usd' => '',
          'opt_dealer_usd' => '',
        ];

        if (! auth()->guest() ) {
          $user = auth()->user();
          $this->class = [
            'price' => $user->getPriceInContract('price') ? 'text-orange' : '',
            'drop_uah' => $user->getPriceInContract('drop_uah') ? 'text-orange' : '',
            'opt_price' => $user->getPriceInContract('opt_price') ? 'text-orange' : '',
            'opt_vip_usd' => $user->getPriceInContract('opt_vip_usd') ? 'text-orange' : '',
            'opt_dealer_usd' => $user->getPriceInContract('opt_dealer_usd') ? 'text-orange' : '',
          ];
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        if ($this->product->isPrint()) {
          return view('components.popup-price-for-print');
        }

        return view('components.popup-price');
    }
}
