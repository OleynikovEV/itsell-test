<?php

namespace App\View\Components;

use App\Models\PriceRange;
use App\Models\Product;
use Illuminate\View\Component;

class RecentlyViewed extends Component
{
  public $products;

  public function __construct(?int $productId = null)
  {
    $this->products = collect();

    $productsIds = session('viewed_products');

    if ($productId !== null && $productsIds) {
      unset($productsIds[array_search($productId, $productsIds)]);
    }

    if ($productsIds) {
      $this->products = Product::whereIn('id', $productsIds)
        ->with('series')
        ->get()->sortBy(function ($product) {
          return array_search($product->id, session('viewed_products'));
        });

      if ($this->products->search(fn($item) => $item->from === 1) !== false) {
        $priceRange = new PriceRange();
        view()->share('priceRangeForPrint', $priceRange->priceRangeByType());
        view()->share('amountRange', $priceRange->range);
      }
    }
  }

  public function render()
  {
    return view('components.recently-viewed');
  }
}
