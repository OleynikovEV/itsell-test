<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FilterBlock extends Component
{
    public $values;
    public $parentId;
    public $parentUrl;
    public $showMore;

    public function __construct($values, $parentUrl, $parentId)
    {
        $this->values = $values;
        $this->parentId = $parentId;
        $this->parentUrl = $parentUrl;
        $this->showMore = ($values->count() > 5);
    }

    public function render()
    {
        return view('components.filter-block');
    }
}
