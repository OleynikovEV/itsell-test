<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class FilterPricesComponent extends Component
{
  public $prices;

  public function __construct(Collection $prices)
  {
    $this->prices = ( $prices->count() > 0 ) ? $prices : null;
  }

  public function render()
  {
    return view('components.filter-prices');
  }
}
