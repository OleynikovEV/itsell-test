<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FastSearchSingleProduct extends Component
{
    public $products = null;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($products = null)
    {
      $this->products = $products;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
      if($this->products !== null) {
        return view('components.fast-search-single-products');
      }

      return null;
    }
}
