<?php

namespace App\View\Components;

use Illuminate\Support\Facades\DB;
use Illuminate\View\Component;

class SeriesShortLinks extends Component
{
  public $productSeries;
  public $seriesCounters;

  public function __construct($series)
  {
    $this->seriesCounters = collect();
    $this->productSeries = $series;

    if ($series) {
      $this->seriesCounters = $series->products()
        ->active()
        ->select('menu')
        ->with('categoryData')
        ->get()
        ->groupBy('categoryData.parent_id')
        ->map->count();
    }
  }

  public function render()
  {
    return view('components.series-short-links');
  }
}
