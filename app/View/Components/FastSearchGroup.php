<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FastSearchGroup extends Component
{
    public $products = null;
    public $searchPhrase = '';
    public $total = '';

    /**
     * Create a new component instance.
     * @return void
     */
    public function __construct($searchPhrase, $total, $products)
    {
      $this->searchPhrase = $searchPhrase;
      $this->products = $products;
      $this->total = $total;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
      if ($this->products !== null) {
        return view('components.fast-search-group');
      }

      return null;
    }
}
