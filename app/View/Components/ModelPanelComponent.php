<?php

namespace App\View\Components;

use App\Models\CategorySector;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Component;

class ModelPanelComponent extends Component
{
    public $sectors;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Collection $models)
    {
      // серии моделей
      $sectorsIds = $models->pluck('sector_id')->unique()->toArray();

      $this->sectors = DB::table('its_menu_sectors')
        ->select(['id', 'title'])
        ->whereIn('id', $sectorsIds)
        ->orderBy('priority', 'DESC')
        ->get()
        ->keyBy('id');

      // добавляем руппу для моделе без серии
      if (isset($sectorsIds[0]) && $sectorsIds[0] === 0) {
        $this->sectors->put(0, (object)[
          'id' => 0,
          'title' => __('Другие модели'),
        ]);
      }

      // объединяем серии и модели
      $models = $models->groupBy('sector_id');
      foreach ($this->sectors as $id => &$sector) {
        $sector->models = $models->get($id);
      }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.model-panel');
    }
}
