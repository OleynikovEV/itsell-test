<?php

namespace App\View\Components;

use App\Models\PriceRange;
use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Component;

class ProductCardComponent extends Component
{
  public $products;

  public function __construct(Collection $productIds)
  {
    $this->products = Product::whereIn('id', $productIds)
      ->with('series', 'additionalCategories:id,parent_id', 'categoryData:id,title,name')
      ->limit(40)
      ->get();

    $priceRange = new PriceRange();
    view()->share('priceRangeForPrint', $priceRange->priceRangeByType());
    view()->share('amountRange', $priceRange->range);
  }

  public function render()
  {
    return view('components.product-card');
  }
}
