<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class FilterBrandsComponent extends Component
{
  public $brands;
  public $ourBrands;

  public function __construct(Collection $brands)
  {
    $brands = $brands->groupBy('our');
    $this->ourBrands = $brands->has(1) ? $brands->get(1) : null;
    $this->brands = $brands->has(0) ? $brands->get(0) : null;
  }

  public function render()
  {
    return view('components.filter-brands');
  }
}
