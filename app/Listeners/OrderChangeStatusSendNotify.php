<?php

namespace App\Listeners;

use App\Events\VchehleEvent;
use App\Mail\OrderCreatedMail;
use Illuminate\Support\Facades\Mail;

class OrderChangeStatusSendNotify
{
  const status_cancelled = -1;
  const status_shipped = 1;
  const status_in_processing= 0;

  public function handle(VchehleEvent $event)
  {
    $subject = 'Заказ №%s %s';
    $order = $event->getOrder();

    if ($order->vchehle_order_status === self::status_shipped) $subject = sprintf($subject, $order->id, 'изготовлен');
    if ($order->vchehle_order_status === self::status_cancelled) $subject = sprintf($subject, $order->id, 'отменен');
    if ($order->vchehle_order_status === self::status_in_processing) $subject = sprintf($subject, $order->id, 'заказ принят в работу');

    Mail::to(config('custom.mail_recipients.order_created'))->send(new OrderCreatedMail($order, 2, $subject));
  }
}
