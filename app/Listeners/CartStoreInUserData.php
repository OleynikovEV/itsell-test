<?php

namespace App\Listeners;

class CartStoreInUserData
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = auth()->user();
        if ( ! $user) return;

        $user->cart = serialize(session('cart'));
        $user->cart_updated_at = now();
        $user->save();
    }
}
