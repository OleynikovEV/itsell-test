<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use App\Mail\OrderCreatedMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderSendCreatedNotification
{

    /**
     * Handle the event.
     *
     * @param  OrderCreated  $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        // Отправка email-уведомления покупателю
        if ($event->order->email && validator(['email' => $event->order->email], ['email' => 'email'])->passes()) {
          try {
            Mail::to($event->order->email)->send(new OrderCreatedMail($event->order));
          } catch (\Exception $e) {
            Log::error('Order ' . $event->order->email . $e->getMessage());
          }
        }

        if ($event->order->isDrop()) {
          $email = config('custom.mail_recipients.order_drop');
        } else {
          $manager = $event->manager;

          if (empty($manager)) {
            $email = config('custom.mail_recipients.order_created');
          } else {
            $email = $manager->email;
          }
        }

        // Отправка email-уведомления менеджерам
        $cc = array_merge(
          config('custom.mail_recipients.order2_created'),
          config('custom.mail_recipients.back_manager')
        );

        try {
          Mail::to($email)->cc($cc)->send(
            new OrderCreatedMail($event->order, 2)
          );
        } catch (\Exception $e) {
          Log::error('Order ' . implode(',', $email) . $e->getMessage());
        }
    }
}
