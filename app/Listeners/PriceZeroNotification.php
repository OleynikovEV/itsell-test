<?php

namespace App\Listeners;

use App\Events\PriceZeroEvent;
use App\Http\Models\TelegramMessanger;

class PriceZeroNotification
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PriceZeroEvent $event)
    {
      $products = $event->getProducts();
      try {
        $tg = new TelegramMessanger;

        foreach ($products as $product) {
          $tg->sendMessage(config('custom.telegram_no_price_chat'),
            'Нет цены <a href="' . custom_route('product', [$product->url, $product->id]) .'">' . $product->title . '</a>'
          );
        }
      } catch (\Exception $ex) {
        dd($ex->getMessage());
        // @TODO: добавить какое-то логирование
      }
    }
}
