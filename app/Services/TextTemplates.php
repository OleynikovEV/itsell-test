<?php

namespace App\Services;

class TextTemplates
{

    /**
     * Генерация текста согласно заданному шаблону
     */
    public function get($templateName = '', $generationSeed = 0, $variables = [])
    {
        // Получение заданного шаблона
        if ( ! $templateName) return false;

        $template = config('text-templates.' . app()->getLocale() . '.' . $templateName);
        if ( ! $templateName) return false;

        if ( ! is_array($template)) $template = [$template];

        // Преобразование переменных для поддежки разных регистров
        $vars = array();
        foreach ($variables as $index => $value)
        {
            $vars['{%' . $index . '%}'] = $value;

            $firstChar = mb_substr($value, 0, 1);
            $vars['{%' . $index . '[lowercase]%}'] = (ctype_alpha($firstChar) ? $firstChar : mb_strtolower($firstChar))
                . mb_substr($value, 1);
            $vars['{%' . $index . '[uppercase]%}'] = mb_strtoupper($value);
        }

        // Генерация текста
        $result = [];

        mt_srand($generationSeed);
        foreach ($template as $section)
        {
            if (is_array($section))
            {
                $result[] = $section[mt_rand(0, count($section) - 1)];
            }
            else
            {
                $result[] = preg_replace('/\s+/', ' ', trim(str_replace(array_keys($vars), array_values($vars), $section)));
            }
        }

        return implode(' ', $result);
    }

}