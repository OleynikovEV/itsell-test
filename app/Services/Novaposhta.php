<?php

namespace App\Services;

use GuzzleHttp\Client as GuzzleClient;

class Novaposhta
{

    // Настройки доступа к API
    protected $apiUrl = 'https://api.novaposhta.ua/v2.0/json/';
    protected $apiKey;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apiKey = config('services.novaposhta.key');
    }

    /**
     * Выполнение запроса к API
     */
    public function query($model = '', $method = '', $properties = [])
    {
        // Подготовка параметров запроса
        $data = [
            'modelName' => $model,
            'calledMethod' => $method,
            'apiKey' => $this->apiKey,
            'language' => 'ru'
        ];

        if (count($properties)) $data['methodProperties'] = $properties;

        // Отправка запроса к API
        $client = new GuzzleClient();
        try
        {
            $response = $client->post($this->apiUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => $data
            ]);

            $response = json_decode($response->getBody());
            if ($response && ! $response->success && $response->errors)
            {
                if ($response->errors[0] == 'API auth fail') $response->errors[0] = 'Ключ API недействителен';
                if ($response->errors[0] == 'API key expired') $response->errors[0] = 'Истек срок действия ключа API';
            }

            return $response;
        }
        catch (\Exception $e)
        {
            logger('An error is occurred while making Novaposhta API call', ['error' => $e->getMessage()]);
            return false;
        }
    }

}
