<?php

namespace App\Services;

use App\Models\Setting;

class Settings
{

    protected $settings;
    protected $temp;

    /**
     * Получение всех настроек при инициализации
     */
    public function __construct()
    {
        $this->settings = Setting::all()->pluck('value', 'name');
        $this->temp = collect();
    }

    /**
     * Получение значения настройки по его имени
     */
    public function get($name = '')
    {
        return $this->settings->get($name) ?: false;
    }

    /**
     * Получение либо установка временного значения на время текущего запроса
     */
    public function temp($name = '', $value = null)
    {
        if ($value !== null)
        {
            $this->temp->put($name, $value);
        }
        else return $this->temp->get($name) ?: false;
    }

}