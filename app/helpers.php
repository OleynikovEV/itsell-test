<?php

if ( ! function_exists('custom_route')) {

    /**
     * Переопределение стандартного хелпера routes для добавления текущей локализации в URL
     */
    function custom_route($name, $parameters = [], $absolute = true)
    {
        $route = app('url')->route($name, $parameters, false);
        // if (app()->getLocale() != config('app.default_locale')) $route = '/' . app()->getLocale() . $route; // TODO: активировать после внедрения мультиязычности

        if ($absolute) $route = url($route);

        return $route;
    }

}

if ( ! function_exists('settings')) {

    /**
     * Вспомогательная функция для быстрого доступа к сервису настроек
     */
    function settings($name = '')
    {
      return Cache::get('settings')->where('name', $name)->first()->value;
//        return app('settings')->get($name);
    }

}

if ( ! function_exists('text_template')) {

    /**
     * Вспомогательная функция для быстрого доступа к сервису генерации текстов по шаблонам
     */
    function text_template($templateName = '', $seed = 0, $variables = [])
    {
        return app('text-templates')->get($templateName, $seed, $variables);
    }

}

if ( ! function_exists('full_path_picture'))
{
  function full_path_picture(string $pictureName): string
  {
    return config('custom.content_server') . $pictureName;
  }
}

if ( ! function_exists('thumb_name'))
{

    /**
     * Получение названия для уменьшенной копии изображения
     */
    function thumb_name($filename = '', $size = 0)
    {
        $dot_index = strrpos($filename, '.');
        return substr($filename, 0, $dot_index) . (substr($filename, $dot_index - 6, 6) != '_thumb' ? '_thumb' : '')
            . ($size ? '_' . $size : '') . substr($filename, $dot_index);
    }

}

if ( ! function_exists('word_declension'))
{

    /**
     * Склонение слова в зависимости от количества, к которуму оно применяется
     */
    function word_declension($word = '', $count = 0)
    {
        $endings = [
            'модел' => ['ь', 'и', 'ей'],
            'цвет' => ['', 'а', 'ов'],
            'наименовани' => ['е', 'я', 'й'],
            'единиц' => ['а', 'ы', ''],
            'товар' => ['', 'а', 'ов'],
            'сери' => ['я', 'и', 'ий'],
            'телефон' => ['а', 'ов', 'ов'],
            'уцененн' => ['ая', 'ые', 'ых'],
            'новин' => ['ка', 'ки', 'ок'],
        ];

        $wordEngings = isset($endings[mb_strtolower($word)]) ? $endings[mb_strtolower($word)] : false;
        if ( ! $wordEngings) return $word;

        $ending = $wordEngings[2];

        if ($count < 10 || $count > 20)
        {
            $count = $count % 10;
            if ($count == 1) $ending = $wordEngings[0];
            if ($count > 1 && $count < 5) $ending = $wordEngings[1];
        }

        return $word . $ending;
    }

}

if ( ! function_exists('increment_string'))
{

    /**
     * Увеличение (добавление) цифрового индекса в конце строки
     */
    function increment_string($str = '', $separator = '-', $first = 1)
    {
        preg_match('/(.+)' . $separator . '([0-9]+)$/', $str, $match);
        return isset($match[2]) ? $match[1] . $separator . ($match[2] + 1) : $str . $separator . $first;
    }

}

if ( ! function_exists('min_max_price'))
{
    /**
     * для отображения интервала цен в представлении
     */
    function min_max_price($min, $max): array
    {
        return $min < $max ? [$min, $max] : [$min];
    }
}

if (!function_exists('removeEmoji')) {
    /**
     * удаление Emoji из текста
     */
    function removeEmoji(string $data): string
    {
        $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clear_string = preg_replace($regex_emoticons, '', $data);

        $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clear_string = preg_replace($regex_symbols, '', $clear_string);

        $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clear_string = preg_replace($regex_transport, '', $clear_string);

        $regex_misc = '/[\x{2600}-\x{26FF}]/u';
        $clear_string = preg_replace($regex_misc, '', $clear_string);

        $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
        $clear_string = preg_replace($regex_dingbats, '', $clear_string);

        return $clear_string;
    }
}

if (!function_exists('salePrice')) {
    // возвращает цену скидки
    function salePrice(float $optPrice, float $oldOptPrice, int $startTime, $endWeek = 4): float
    {
        return ($oldOptPrice > $optPrice
            && $startTime >= today()->subWeek($endWeek)->timestamp )
            ? $oldOptPrice : 0;
    }
}

if (!function_exists('calcSale')) {
    // расчет скидки
    function calcSale(float $price, $discount, $round = 2)
    {
        return ($discount === 0) ? $price : round(($price - round($discount, $round)), $round);
    }
}

if (!function_exists('colorWhite')) {
    // каким цветам делать рамку
    function colorWhite(string $color): string
    {
        return in_array($color, ['fff', 'ffffff', 'f8f8ff', 'f5f5f5', 'e2eaed', 'f5f5dc']) ? 'color-white' : '';
    }
}

if (!function_exists('fullUrlImage')) {
  // каким цветам делать рамку
  function fullUrlImage(?string $image): string
  {
    return $image ? config('custom.content_server') . thumb_name($image) : '/template/images/noimage.jpg';
  }
}

if (!function_exists('convertToUAH')) {
  // переводим в грн
  function convertToUAH($price): int
  {
    return round($price * settings('dollar'));
  }
}

if (!function_exists('getSql')) {
  /*
   *  returns SQL with values in it
   */
  function getSql($model)
  {
    $replace = function ($sql, $bindings)
    {
      $needle = '?';
      foreach ($bindings as $replace){
        $pos = strpos($sql, $needle);
        if ($pos !== false) {
          if (gettype($replace) === "string") {
            $replace = ' "'.addslashes($replace).'" ';
          }
          $sql = substr_replace($sql, $replace, $pos, strlen($needle));
        }
      }
      return $sql;
    };
    $sql = $replace($model->toSql(), $model->getBindings());

    return $sql;
  }
}

if (!function_exists('getDescription')) {
  /**
   * Возвращает описание для клиентов
   * @param $product
   * @param $list_item
   * @return string
   */
  function getDescription($fullDescription, $descriptionForClient, $dateOfUploading, $title, $colorName): string
  {
    $description_for_client = '';
    if (strlen(trim($fullDescription)) > 0 && $dateOfUploading != null && strtotime(date('Y-m-d')) >= strtotime($dateOfUploading)) {
      if (strpos($fullDescription, 'http') === false) {
        $description_for_client = str_replace('/uploads/', 'https://itsellopt.com.ua/uploads/', $fullDescription);
      } else {
        $description_for_client = $fullDescription;
      }
    } else {
      $description_for_client = strlen(trim($descriptionForClient)) > 0
        ? strip_tags($descriptionForClient, '<p><b><strong><ul><ol><li><br>')
        : '<p>' . $title . ($colorName ? ' (' . $colorName . ')' : '') . '</p>';
    }

    return $description_for_client;
  }
}

if (!function_exists('getColorBorderBottom')) {
  /**
   * цвет линии подчеркивания в input количество
   */
  function getColorBorderBottom($product, $groupProduct): ?string
  {
    if ($product->old_opt_price > $product->opt_price || $product->markdown === 1) return 'color-sale';
    if ($groupProduct->qty > 0 && $groupProduct->qty < 10) return 'color-last-items';
    if ($groupProduct->qty <= 0 && $groupProduct->when_appear) return 'color-soon';
    if ($product->old_opt_price > $product->opt_price
      || \Illuminate\Support\Str::contains($product->title, 'Уценка')) return 'color-sale';
    if ($groupProduct->qty > 0 && $groupProduct->qty < 10) return 'color-last-items';
    if (! empty($groupProduct->date_coming)) {
      $diffDay = now()->diffInDays(\Carbon\Carbon::parse($groupProduct->date_coming));
      if ($diffDay < \App\Models\Product::COMING_DAYS) return 'color-coming-items';
    }

    return null;
  }
}

if (!function_exists('removePageNum')) {
  function removePageNum($string)
  {
    if (is_numeric($string)) {
      return '';
    }

    return preg_replace('/\/\d+$/', '', $string);
  }
}

// определить выбранную корзину
if (!function_exists('getSelectedCart')) {
  function getSelectedCart()
  {
    $selectedCart = 'default';

    if (! auth()->guest()) {
      $selectedCart = auth()->user()->default_cart;
      $cartsList = config('custom.cart_list');

      if (request()->cookie('selected_cart') && isset($cartsList[request()->cookie('selected_cart')]) ) {
        $selectedCart = request()->cookie('selected_cart');
      }
    }

    return $selectedCart;
  }
}

// получить выбранный тип прайса
if (!function_exists('getPriceTypeDefault')) {
  function getPriceTypeDefault()
  {
    $priceTypeDefault = 'opt_price';
    $priceList = [];

    // получаем список доступных клиенту типов цен
    if (!auth()->guest()) {
      $priceList = auth()->user()->priceTypeList();
    } else {
      $priceList = [];
    }

    // проверяем выбранный из списка тип цен присутствует в доступном диапазоне клиенту
    if (request()->cookie('price_type') && in_array(request()->cookie('price_type'), array_keys($priceList))) {
      $priceTypeDefault = request()->cookie('price_type');
    } elseif (! auth()->guest()) {
      $priceTypeDefault = auth()->user()->getPriceDefault();
    }

    return $priceTypeDefault;
  }
}

if (!function_exists('getMicrotime')) {
  function getMicrotime($text, $start)
  {
    //dump( $text . ' ' . round(( microtime(true) - $start ), 3) );
  }
}