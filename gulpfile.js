const gulp = require('gulp');
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const minifyCss = require('gulp-minify-css');
const uglify = require('gulp-uglify-es').default;
const watch = require('gulp-watch');
const gutil = require('gulp-util');

const jsFiles = {
  core: [
    'vendor/jquery-3.4.1.js',
    'vendor/js.cookie-2.2.1.min.js',
    'vendor/jquery.touchSwipe.min.js',
    'vendor/jquery.wmr-slider.js',
    'vendor/jquery.wmr-sliding-content.js',
    'vendor/jquery.inputmask.min.js',
    'vendor/jquery-ui.min.js',
    'vendor/sorting-elements-with-jquery.js',
    'phoneMask.js',
    'app.js',
    'wish.js',
    'lazyload.js',
    'modal.js',
    'priceModel.js',
    'videoBaner.js',
  ],
  'products-list': [
    'vendor/jquery-ui.touch-punch.js',
    'products-list.js',
    'loadProduct.js'
  ],
  'wish-list': ['wishList.js'],
  custom: ['custom.js'],
  product: ['product.js'],
  cart: ['cart.js'],
  order: ['order.js'],
  register: ['register.js']
}

gulp.task('uglify-js', function(done) {
    for (let key in jsFiles) makeJsBundle(key, jsFiles[key]);
    done();
});

gulp.task('dev-js', function(done) {
  for (let key in jsFiles) makeJsBundleDev(key, jsFiles[key]);
  done();
});

gulp.task('minify-css', function() {
    const cssFiles = [
        'variables.css',
        'reset.css',
        'style.css',
        'custom.css',
        'forms.css',
        'elements.css',
        'product_list.css',
        'print_cover.css'
    ];

    return gulp.src(cssFiles.map(function(a) {return 'public/template/css/' + a}))
        .pipe(concat('bundle.min.css'))
        .pipe(gulp.dest('public/template/css'))
        .pipe(minifyCss({keepSpecialComments : 0}))
        .pipe(gulp.dest('public/template/css'));
});

gulp.task('watch', function(){
    gulp.watch(['public/template/js/**/*.js', '!public/template/js/**/*.min.js'], gulp.parallel('uglify-js'));
    gulp.watch(['public/template/css/**/*.css', '!public/template/css/**/*.min.css'], gulp.parallel('minify-css'));
});

gulp.task('vue-cart', function(done) {
  const files = {
    cartApp: ['cart/app.js']
  };
  for (let key in files) makeJsBundle(key, files[key]);
  done();
});

gulp.task('default', gulp.parallel('uglify-js', 'minify-css'));

function makeJsBundle(filename, files) {
    return gulp.src(files.map(function(a) {return 'public/template/js/' + a}))
        .pipe(concat(filename + '.min.js'))
        .pipe(gulp.dest('public/template/js/dist'))
        .pipe(uglify())
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest('public/template/js/dist'));
}

function makeJsBundleDev(filename, files) {
  return gulp.src(files.map(function(a) {return 'public/template/js/' + a}))
    .pipe(concat(filename + '.min.js'))
    .pipe(gulp.dest('public/template/js/dist'))
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(gulp.dest('public/template/js/dist'));
}
